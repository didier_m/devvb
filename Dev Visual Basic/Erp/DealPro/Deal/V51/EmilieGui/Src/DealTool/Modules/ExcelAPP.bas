Attribute VB_Name = "ExcelAPP"
'***********************************************************************
'* Module de gestion des fonctions Excel par import d'un fichier Ascii *
'***********************************************************************

'-> Variable pour collection des formats excel
Public xl_Formats As Collection

'-> Variable de r�cup�ration du fichier d�tail
Private DetailFic As String
Private SepFic As String 'S�parateur du fichier pour r�partition des colonnes
Private nbCol As Variant 'Nombre de colonne pour d�callage du format via Offset (Excel8.OLB)
Private AutoAjust As Boolean 'Indique si ajustement des colonnes par d�faut
Private CellDep As String 'Adresse de la cellule de d�part
Private FormatEntete As String 'Nom du format � appliquer pour l'entete
Private FormatLigne As String 'Nom du format � appliquer pour les lignes
Private NomFeuille As String 'Nom de la feuille
Private Formattage As Boolean 'indique si on applique le format d�sir�
Private ClasseurRef As String 'Nom d'un classeur mod�le
Private FormatCol() As Integer 'Liste des formats � appliquer en colonne
Private AutoFilter As Boolean 'Indique si on doit appliquer un filtre automatique
Private SetName As String 'Indique si diff�rent de "" que l'on doit donner un nom � la plage de donn�es
Private SetCodeLangue As Integer 'Indiquer le code langue � utliser pour les exports
Private FichierLangue As String 'Fichier Langue
Dim KillSpool As Boolean 'Indique si on doit supprimer les spools apr�s traitement
Dim RunMacro As String 'Indique que l'on doit lancer une macro dans le classeur


'-> Indique si on doit se servir de la premi�re ligne de d�tail pour indiquer le nombre de colonnes
Private FindNbCol As Boolean


Public Sub OpenEntete(ByVal NomFichier As String)

'-> Cette proc�dure ouvre le descriptif de fichier Entete et met � jour les
' diff�rents param�tres

Dim Ligne As String
Dim NumFic As Integer
Dim i As Integer
Dim Tempo As String
Dim FindFormatCol As Boolean
Dim CommunPath As String
Dim ToFormatCol As Boolean

'-> Test si fichier existant
If Dir$(NomFichier) = "" Then
    MsgBox "Impossible de trouver le fichier d'entete" & NomFichier, vbCritical + vbOKOnly, "Erreur"
    End
End If

'-> Initialiser la collection des formats de cellule
Set xl_Formats = New Collection

'-> Ouverture du fichier pour lecture
NumFic = FreeFile
Open NomFichier For Input As #NumFic

'-> De base supprimer le fichier detail
KillSpool = True

'-> De base on postionner le codelangue en fran�ais
SetCodeLangue = 1

'-> Lecture du fichier
Do While Not EOF(NumFic)

    '-> Lecture d'une ligne
    Line Input #NumFic, Ligne
    
    If Trim(Ligne) = "" Then
    Else
    
        '- Ne lire que les lignes qui commencent par un "\"
        If Mid$(Ligne, 1, 1) = "\" Then
        
            Select Case UCase$(Entry(1, Ligne, "$"))
                Case "\FORMULAIRE"
                Case "\CLASSEUR"
                    ClasseurRef = Entry(2, Ligne, "$")
                Case "\SETNAME"
                    SetName = Entry(2, Ligne, "$")
                Case "\FORMATAGE"
                    Formattage = CBool(Entry(2, Ligne, "$"))
                Case "\CODELANGUE"
                    SetCodeLangue = CInt(Entry(2, Ligne, "$"))
                Case "\NOMFEUILLE"
                    NomFeuille = Entry(2, Ligne, "$")
                Case "\FICHIERDETAIL"
                    DetailFic = Entry(2, Ligne, "$")
                    If Dir$(DetailFic) = "" Then
                        MsgBox "Impossible de trouver le fichier d�tail :" & DetailFic, vbCritical + vbOKOnly, "Erreur"
                        End
                    End If
                Case "\SEPARATEUR"
                    SepFic = Entry(2, Ligne, "$")
                Case "\NBCOL"
                    nbCol = Entry(2, Ligne, "$")
                    If Trim(nbCol) = "" Or CInt(nbCol) > 255 Then
                        MsgBox "Nombre de colonnes incorrect :" & Ligne, vbCritical + vbOKOnly, "Erreur"
                        End
                    End If
                    nbCol = CInt(nbCol)
                    FindNbCol = True
                    
                Case "\FORMATCOL"
                
                    ReDim FormatCol(1 To nbCol)
                    Ligne = Entry(2, Ligne, "$")
                    For i = 1 To nbCol
                        '-> Recup�ration de la description du format
                        Tempo = Entry(i, Ligne, "|")
                        Select Case Trim(UCase$(Tempo))
                            Case "CHA"
                                FormatCol(i) = 1
                            Case "DAT"
                                FormatCol(i) = 2
                            Case "DEC"
                                FormatCol(i) = 4
                                ToFormatCol = True
                            Case "INT"
                                FormatCol(i) = 3
                                ToFormatCol = True
                            Case Else
                                FormatCol(i) = 1
                        End Select
                    Next
                    FindFormatCol = True
                
                Case "\AUTOAJUST"
                
                    If UCase$(Entry(2, Ligne, "$")) = "VRAI" Then
                        AutoAjust = True
                    Else
                        AutoAjust = False
                    End If
                    
                Case "\FILTRE"
                
                    If UCase$(Entry(2, Ligne, "$")) = "VRAI" Then
                        AutoFilter = True
                    Else
                        AutoFilter = False
                    End If
                
                    
                Case "\CELLULEDEPART"
                
                    CellDep = Entry(2, Ligne, "$")
                    If Trim(CellDep) = "" Then
                        MsgBox "Cellule de d�part incorrecte :" & Ligne, vbCritical + vbOKOnly, "Erreur"
                        End
                    End If
                                
                Case "\FORMATENTETE"
                
                    FormatEntete = Entry(2, Ligne, "$")
                    If Trim(FormatEntete) = "" Then FormatEntete = "EnteteDefaut"
                    AnalyseFormat FormatEntete
                                                    
                Case "\FORMATLIGNE"
                    
                    FormatLigne = Entry(2, Ligne, "$")
                    If Trim(FormatLigne) = "" Then FormatLigne = "LigneDefaut"
                    AnalyseFormat FormatLigne
                    
                Case "\KILL"
                    If Trim(Entry(2, Ligne, "$")) = "0" Then KillSpool = False
                    
                Case "\RUNMACRO"
                    If Trim(Entry(2, Ligne, "$")) <> "" Then
                        RunMacro = Entry(2, Ligne, "$")
                    Else
                        RunMacro = ""
                    End If
            End Select 'selon le type de ligne
            
        End If 'Si ligne commence par "\"
        
    End If 'Si ligne � blanc

Loop 'Boucle d'analyse

'-> Fermer les divers fichiers
Reset

'-> Cr�taion du path du fichier Langue
i = InStr(UCase$(App.Path), "\EMILIEGUI\")
CommunPath = Mid$(App.Path, 1, i + 10) & "Mqt\Dealtool-"

If Dir$(CommunPath & Format(SetCodeLangue, "00") & ".lng") <> "" Then
    FichierLangue = Mid$(App.Path, 1, i + 10) & "Mqt\Dealtool-" & Format(SetCodeLangue, "00") & ".lng"
Else
    FichierLangue = Mid$(App.Path, 1, i + 10) & "Mqt\Dealtool-01.lng"
End If

If Not FindFormatCol Then
    ToFormatCol = False
    ReDim FormatCol(1 To nbCol)
    For i = 1 To nbCol
        FormatCol(i) = 1
    Next
End If

'-> Si on doit , reformatter les colonnes necessaires
If ToFormatCol Then Call FormatNumValue

'-> Supprimer le fichier d'ordre
On Error Resume Next

'-> Supprimer le fichier d'entete
If Trim(NomFichier) <> "" Then
    'THIERRY If KillSpool Then If Dir$(NomFichier, vbNormal) <> "" Then Kill NomFichier
End If

'-> Lancer Excel
LinkExcel

End Sub
Private Sub FormatNumValue()

'---> Cette procedure applique un format � la data dans un fichier d�tail

Dim hdlFileRef As Integer
Dim hdlFile As Integer
Dim TempFileName As String
Dim Ligne As String
Dim i As Integer
Dim LigneDest As String
Dim ValueCell As String

On Error Resume Next

'-> R�cup�rer un fichier Temporaire
TempFileName = GetTempFileNameVB("EXC")

'-> Ouvrir le fichier que l'on vient de cr�er
hdlFile = FreeFile
Open TempFileName For Output As #hdlFile

'-> Ouverture du fichier de r�f�rence
hdlFileRef = FreeFile
Open DetailFic For Input As #hdlFileRef

'-> Retraitement de tout le fichier
Do While Not EOF(hdlFileRef)
    '-> Raz de la ligne de destination
    LigneDest = ""
    '-> Lecture de la ligne
    Line Input #hdlFileRef, Ligne
    '-> Ne pas traiter les lignes � blanc
    If Trim(Ligne) = "" Then GoTo NextRow
    '-> Analyse de la ligne
    For i = 1 To nbCol
        '-> Get de la valeur
        ValueCell = Entry(i, Ligne, SepFic)
        '-> Ne retraiter que si format d�cimal
        If FormatCol(i) = 3 Or FormatCol(i) = 4 Then
            '-> Convertir la valeur
            ValueCell = Trim(Convert(ValueCell))
        End If
        '-> Rajouter dans la ligne en cours
        If Trim(LigneDest) = "" Then
            LigneDest = ValueCell
        Else
            LigneDest = LigneDest & SepFic & ValueCell
        End If
    Next
    '-> Imprimer dans le fichier destination
    Print #hdlFile, LigneDest
NextRow:
Loop 'Pour tous le fichier d'origine

'-> Fermer les 2 fichiers
Close #hdlFile
Close #hdlFileRef

'-> supprimer l'ancien fichier d�tail
Kill DetailFic

'-> Copier le nouveau fichier
Name TempFileName As DetailFic

End Sub


Public Sub AnalyseFormat(ByVal NomFormat As String)

'---> Proc�dure qui recherche et cr�� la classe format en fonction de son param�trage

Dim ListeCle As String
Dim xlF As Xl_Format
Dim Res As Long
Dim i As Integer
Dim DefCle As String
Dim NomCle As String
Dim ValueCle As String


Dim TempFileName As String

On Error GoTo ErrorFormat

'-> Il faut obtenir un nom de fihcier temporaire pour ouverture du fichier .ini PB WIN95
TempFileName = GetTempFileNameVB("LIB")
Res = CopyFile(App.Path & "\DealTool1.lib", TempFileName, 0)

'-> Initialisation du buffer
ListeCle = Space$(1000)
Res = GetPrivateProfileSection(NomFormat, ListeCle, Len(ListeCle), TempFileName)

If Res = 0 Then
    MsgBox "Impossible de lire le format sp�cifi� : " & NomFormat & ".Aucun format ne sera appliqu�.", vbCritical + vbOKOnly, "Erreur de format"
Else
    '-> Cr�er une nouvelle classe
    Set xlF = New Xl_Format
    xlF.Name = NomFormat
    '-> Formater la liste
    ListeCle = Mid$(ListeCle, 1, Res - 1)
    If Trim(ListeCle) <> "" Then
        '-> r�cup�ration des diff�rentes cl�s
        For i = 1 To NumEntries(ListeCle, Chr(0))
            '-> R�cup�ration de la d�finition de la cle
            DefCle = Entry(i, ListeCle, Chr(0))
            NomCle = Entry(1, DefCle, "=")
            ValueCle = Entry(2, DefCle, "=")
            '-> Cr�ation de la valeur
            Select Case UCase$(NomCle)
                    
                Case "TYPEBORDURE"
                
                    xlF.TypeBordure = CInt(ValueCle)
                    
                Case "COULEURBORDURE"
                    
                    xlF.ColorBordure = CInt(ValueCle)
                    
                Case "BACKCOLOR"
                    
                    xlF.BackColor = CInt(ValueCle)
                    
                Case "FONTNAME"
                    
                    xlF.FontName = ValueCle
                    
                Case "FONTSIZE"
                
                    xlF.FontSize = CInt(ValueCle)
                    
                Case "FONTCOLOR"
                
                    xlF.FontColor = CInt(ValueCle)
                
                Case "FONTBOLD"
                    If UCase$(Trim(ValueCle)) = "FAUX" Then
                        xlF.FontBold = False
                    Else
                        xlF.FontBold = True
                    End If
                                    
                Case "FONTITALIC"
                
                    If UCase$(Trim(ValueCle)) = "FAUX" Then
                        xlF.FontItalic = False
                    Else
                        xlF.FontItalic = True
                    End If
                
                Case "FONTUNDERLINE"
                
                    If UCase$(Trim(ValueCle)) = "FAUX" Then
                        xlF.FontUnderline = False
                    Else
                        xlF.FontUnderline = True
                    End If
                    
                
                Case "ALIGNEMENT"
                
                    xlF.AlignementH = CInt(ValueCle)
            
            End Select 'selon la nature de la cl�
            
        Next 'pour toutes les cl�s
    End If 'si valeur <> ""
    
    '-> Ajouter le nouveau format
    xl_Formats.Add xlF, UCase$(NomFormat)
    
    '-> Lib�rer le pointeur
    Set xlF = Nothing
    
End If 'Si on trouve la rubrique

If Dir$(TempFileName, vbNormal) <> "" Then Kill TempFileName

Exit Sub

ErrorFormat:

    MsgBox "Erreur durant la lecture des diff�rents formats." & vbClrf & Err.Number & " -> " & Err.Description, vbCritical + vbOKOnly, "Erreur fatale"
    End

End Sub

Private Sub SetSheetName(aFeuille As Object, strName As String)

On Error Resume Next

aFeuille.Name = Trim(strName)


End Sub

Private Sub RunMacroInExcelApp(ExcelApp As Object)

'---> Cette procedure lance une macro dans un classeur Excel

On Error GoTo GestError

'-> Essayer de lancer la macro
ExcelApp.Run RunMacro

Exit Sub


GestError:
    

End Sub

Private Sub LinkExcel()

'-> Proc�dure qui r�alise le lien avec Excel
Dim ExcelApp As Object
Dim Classeur As Object
Dim Feuille As Object
Dim aRange As Object
Dim xlF As Xl_Format
Dim Res As Long
Dim lpbuffer As String
Dim ActiveCol As Integer
Dim aRange2 As Object
Dim NbLigne As Long
Dim FindSheet As Boolean
Dim HideSheet As Integer
Dim hdlFile As Integer
Dim Ligne As String
Dim RowPos As Integer
Dim ColPos As Integer
Dim i As Integer
Dim ValueField As String
Dim nbDec As String
Dim iPos As Integer

On Error GoTo EndError

'-> Afficher la feuille de temporisation
DealAnim.Caption = GetIniFileValue("LANGUE", "1", FichierLangue)
DealAnim.Label1 = Replace(GetIniFileValue("LANGUE", "2", FichierLangue), "$FILE$", DetailFic)
DealAnim.Show
DoEvents

'-> Ouverture d'excel et cr�ation des objets
Set ExcelApp = CreateObject("Excel.Application")
ExcelApp.DisplayAlerts = False
DoEvents

If Trim(ClasseurRef) = "" Or Dir$(ClasseurRef) = "" Then
    Set Classeur = ExcelApp.Workbooks.Add
    DealAnim.Label1.Caption = GetIniFileValue("LANGUE", "4", FichierLangue)
    DoEvents
    '-> Suppression des feuilles en trop
    Set Feuille = Classeur.Worksheets(2)
    Feuille.Delete
    DoEvents
    Set Feuille = Classeur.Worksheets(2)
    Feuille.Delete
    DoEvents
    '-> S�lectionner la feuille Feuil1
    Set Feuille = Classeur.Worksheets(1)
    '-> Maj du nom
    If Trim(NomFeuille) <> "" Then SetSheetName Feuille, NomFeuille
Else
    '-> Ouverture du classeur de r�f�rence
    Set Classeur = ExcelApp.Workbooks.Open(ClasseurRef)
    
    '-> Si le nom de la feuille est sp�cifi�e
    If Trim(NomFeuille) <> "" Then
        FindSheet = False
        '-> V�rifier si on a trouv� la feuille sp�cifi�e
        For Each Feuille In Classeur.Worksheets
            '-> Tester si correspondance
            If UCase$(Trim(NomFeuille)) = UCase$(Trim(Feuille.Name)) Then
                FindSheet = True
                Exit For
            End If
        Next
        If Not FindSheet Then
            '-> Si on n' pas trouv� la feuille sp�cifi�e, ajouter une feuille et leui donn� le nom
            Set Feuille = Classeur.Worksheets.Add
            SetSheetName Feuille, NomFeuille
        End If
    End If
    
End If

'-> V�rification si feuille masqu�e ou non
HideSheet = Feuille.Visible
Feuille.Visible = -1
Feuille.Activate

'-> S�lection de la cellule active
If Trim(CellDep) = "" Then CellDep = "A1"

'-> S�lectionner la cellule de d�part
ExcelApp.Application.Range(CellDep).Select

'-> R�cup�rer la ligne de d�part
RowPos = ExcelApp.Application.ActiveCell.Row
ColPos = ExcelApp.Application.ActiveCell.Column

'-> Ouverture du fichier
hdlFile = FreeFile
Open DetailFic For Input As #hdlFile

'-> Lecture s�quentielle du fichier
Do While Not EOF(hdlFile)
    '-> Mettre � jour le libelle�
    DealAnim.Label1 = Replace(GetIniFileValue("LANGUE", "8", FichierLangue), "$FILE$", DetailFic) & " " & NbLigne
    DoEvents
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> Redispatcher les colonnes
    For i = 1 To nbCol
        '-> get de la valeur
        ValueField = Entry(i, Ligne, SepFic)
        '-> rajout de ' ' si commence par '=' Didier
        If Len(ValueField) > 2 Then
            If Mid(ValueField, 1, 1) = "=" Then ValueField = " " & ValueField
        End If
        '-> Poser le Format
        If FormatCol(i) = 3 Or FormatCol(i) = 4 Then
            If IsNumeric(ValueField) Then
                If FormatCol(i) = 4 Then
                    '-> R�cup�rer la partie d�cimale
                    iPos = InStr(1, ValueField, SepDec)
                    If iPos <> 0 Then
                        iPos = Len(ValueField) - iPos
                        If iPos <> 0 Then
                            nbDec = "0." & String(iPos, "0")
                        Else
                            nbDec = "0"
                        End If
                    Else
                        nbDec = "0"
                    End If
                Else
                    nbDec = "0"
                End If
                Feuille.Cells(RowPos + NbLigne, ColPos + i - 1) = CDec(Entry(i, Ligne, SepFic))
                Feuille.Cells(RowPos + NbLigne, ColPos + i - 1).NumberFormat = nbDec
            Else
                Feuille.Cells(RowPos + NbLigne, ColPos + i - 1) = Entry(i, Ligne, SepFic)
            End If
        Else
            If FormatCol(i) = 2 Then
                If IsDate(ValueField) Then
                    Feuille.Cells(RowPos + NbLigne, ColPos + i - 1) = CDate(Trim(Entry(i, Ligne, SepFic)))
                Else
                    Feuille.Cells(RowPos + NbLigne, ColPos + i - 1) = Entry(i, Ligne, SepFic)
                End If
            Else
                'Feuille.Cells(RowPos + NbLigne, ColPos + i - 1) = Entry(i, Ligne, SepFic)
                Feuille.Cells(RowPos + NbLigne, ColPos + i - 1) = ValueField 'Entry(i, Ligne, SepFic) didier
            End If
        End If
    Next 'Pour toutes les colonnes
    '-> Incr�menter le compteur de ligne
    NbLigne = NbLigne + 1
Loop

'-> Fermer le fichier
Close #hdlFile

'-> Si on doit donner un nom
If SetName <> "" Then
    '-> S�lectionner la plage entiere des donn�es
    Set aRange = ExcelApp.Application.Range(ExcelApp.Application.ActiveCell, ExcelApp.Application.ActiveCell.Offset(ExcelApp.Application.Selection.Areas.Item(1).Rows.Count - 1, nbCol - 1))
    aRange.Select
    '-> Donner un nom
    SetGoodName aRange
End If

DoEvents

If Formattage Then
    '-> Faire la mise en forme
    DealAnim.Label1.Caption = GetIniFileValue("LANGUE", "7", FichierLangue)

    'D�callage de la cellule active pour formattage des lignes de d�tail
    Set aRange = ExcelApp.Application.ActiveCell.CurrentRegion
    Set aRange = aRange.Offset(1, 0).Resize(aRange.Rows.Count - 1, aRange.Columns.Count)
    
    'Formattage des cellules de d�tail
    If Trim(FormatLigne) <> "" Then
        Set xlF = xl_Formats(UCase$(FormatLigne))
        Call ApplicFormat(xlF, aRange)
    End If
    
    '-> Formattage de la ligne d'entete
    Set aRange = Feuille.Range(CellDep)
    aRange.Activate
    
    '-> S�lectionner l'entete
    Set aRange = ExcelApp.Application.ActiveCell.CurrentRegion
    Set aRange = aRange.Resize(1, aRange.Columns.Count)

    '-> Appliquer le tri automatique des donn�es si necessaire
    If AutoFilter Then aRange.AutoFilter

    'Formattage des cellules de l'entete si on a sp�cifi� ou non une value
    If FormatEntete <> "" Then
        Set xlF = xl_Formats(UCase$(FormatEntete))
        Call ApplicFormat(xlF, aRange)
    End If
End If 'Si on doit formatter

'-> Redimensionnement automatique des colonnes
If AutoAjust Then
    If Trim(CellDep) <> "" Then Feuille.Range(CellDep).Activate
    ActiveCol = ExcelApp.Application.ActiveCell.Column
    For i = ActiveCol To ActiveCol + nbCol
        ExcelApp.Application.Columns(i).Activate
        ExcelApp.Application.Columns(i).EntireColumn.AutoFit
    Next
End If

'-> S�lectionner la cellule de d�part
If Trim(CellDep) <> "" Then Feuille.Range(CellDep).Activate

'-> Ex�cuter une macro si necessaire
If RunMacro <> "" Then RunMacroInExcelApp ExcelApp

'-> Masquer la feuille si necessaire
Feuille.Visible = HideSheet
    
'-> Rendre Excel visible
ExcelApp.Visible = True
ExcelApp.DisplayAlerts = True

'-> Supprimer le fichier des donn�es
If Trim(DetailFic) <> "" Then
    'THIERRY If KillSpool Then If Dir$(DetailFic, vbNormal) <> "" Then Kill DetailFic
End If

EndError:

    If Err.Number <> 0 Then MsgBox Err.Number & " -> " & Err.Description

    '-> Lib�rer les pointeurs
    Set Feuille = Nothing
    Set Classeur = Nothing
    Set ExcelApp = Nothing
    Set aRange = Nothing
    
    '-> d�charger la feuille tempo
    Unload DealAnim


End Sub

Private Function SetGoodName(aRange As Object)

'---> Cette proc�dure d�termine si le nom est bon ou non

On Error GoTo NameError

aRange.Name = SetName

Exit Function

NameError:


End Function

Private Sub ApplicFormat(ByVal xlF As Xl_Format, ByVal aRange As Object)

On Error Resume Next

    'Bordures
    If xlF.xlLineStyle = -4142 Then
    Else
        aRange.Borders.LineStyle = xlF.xlLineStyle
        aRange.Borders.Weight = xlF.XlBorderWeight
        aRange.Borders.ColorIndex = xlF.ColorBordure
    End If
    'Int�rieur
    aRange.Interior.ColorIndex = xlF.BackColor
    aRange.Interior.Pattern = 1
    
    If aRange.Font.Name <> xlF.FontName Then aRange.Font.Name = xlF.FontName
    aRange.Font.ColorIndex = xlF.FontColor
    If aRange.Font.Size <> xlF.FontSize Then aRange.Font.Size = xlF.FontSize
    If aRange.Font.Bold <> xlF.FontBold Then aRange.Font.Bold = xlF.FontBold
    If aRange.Font.Italic <> xlF.FontItalic Then aRange.Font.Italic = xlF.FontItalic
    If aRange.Font.Underline <> xlF.FontUnderline Then aRange.Font.Underline = xlF.FontUnderline
    'If aRange.HorizontalAlignment <> xlF.AlignementH Then aRange.HorizontalAlignment = xlF.AlignementH


Err.Number = 0

End Sub

Public Sub GenerateTemplate()

'-> Fonction qui g�n�re un exemple pour tester un format de cellule

Dim ExcelApp As Object
Dim Classeur As Object
Dim Feuille As Object
Dim aRange As Object

'-> Cr�ation du lien avec Excel
Set ExcelApp = CreateObject("Excel.Application")
ExcelApp.Visible = False
Set Classeur = ExcelApp.Workbooks.Add
Set Feuille = Classeur.Worksheets("Feuil1")
Feuille.Range("B3").Activate

'-> Copie de la chaine test dans la m�moire
Clipboard.Clear
Clipboard.SetText "Cellule1|Cellule2|Cellule3" & Chr(13) + Chr(10) & "Cellule1|Cellule2|Cellule3"

'-> Collage des donn�es
Feuille.Paste

'-> R�partition des donn�es
ExcelApp.Application.Selection.TextToColumns DataType:=xlDelimited, _
    ConsecutiveDelimiter:=False, Other:=True, OtherChar:="|"

'-> S�lection des cellules
Set aRange = Feuille.Range("B3:D4")
aRange.Activate

'-> R�cup�ration du format de cellule
Set xlFormat = xl_Formats(UCase$(EditLib.List2.Text))
Call ApplicFormat(xlFormat, aRange)

'-> S�lectionner la premi�re cellule
Feuille.Range("A1").Activate
 
ExcelApp.Visible = True


Set ExcelApp = Nothing

End Sub
