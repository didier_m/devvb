VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   4710
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7200
   LinkTopic       =   "Form1"
   ScaleHeight     =   4710
   ScaleWidth      =   7200
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim hdlMenu As Long
Dim aPoint As POINTAPI
Dim aMsg As MSG

If Button = vbRightButton Then

    '-> Cr�ation d'un handle de menu
    hdlMenu = CreatePopupMenu()
    
    '-> Ajouter les entr�es du menu
    AppendMenu hdlMenu, MFT_STRING, 1, "&Test Option"
    AppendMenu hdlMenu, MFT_STRING, 2, "&Test Option1"
    AppendMenu hdlMenu, MFT_STRING, 3, "&Test Option2"
    
    '-> R�cup�rer la position du pointeur de la souris
    GetCursorPos aPoint
    
    '-> Afficher le menu contextuel
    TrackPopupMenu hdlMenu, TPM_RIGHTBUTTON Or TPM_LEFTALIGN Or TPM_TOPALIGN, aPoint.x, aPoint.y, 0, Me.hWnd, 0
    
    '-> R�cup�rer le dernier evenement re�u par la feuille
    PeekMessage aMsg, Me.hWnd, 0, 0, PM_NOREMOVE
    
    If aMsg.message = WM_COMMAND& Then MsgBox aMsg.message & "   " & aMsg.wParam
    
    '-> d�truire le menu
    DestroyMenu (hdlMenu)
    
End If


End Sub
