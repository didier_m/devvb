VERSION 5.00
Begin VB.Form frmPrint 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   3255
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6915
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3255
   ScaleWidth      =   6915
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Aide 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   6120
      Picture         =   "frmPrint.frx":0000
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   12
      Top             =   2640
      Width           =   630
   End
   Begin VB.Frame Frame2 
      Height          =   1455
      Left            =   3480
      TabIndex        =   5
      Top             =   1680
      Width           =   1935
      Begin VB.TextBox NbCopies 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1080
         TabIndex        =   11
         Text            =   "1"
         Top             =   720
         Width           =   375
      End
      Begin VB.VScrollBar VScroll1 
         Height          =   285
         Left            =   1440
         Max             =   1
         Min             =   50
         TabIndex        =   6
         Top             =   720
         Value           =   1
         Width           =   255
      End
      Begin VB.Image Image1 
         Height          =   480
         Left            =   240
         Picture         =   "frmPrint.frx":1042
         Top             =   600
         Width           =   480
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1455
      Left            =   120
      TabIndex        =   1
      Top             =   1680
      Width           =   3255
      Begin VB.OptionButton Option3 
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   1080
         Width           =   2295
      End
      Begin VB.TextBox Text2 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2400
         TabIndex        =   4
         Top             =   720
         Width           =   615
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1320
         TabIndex        =   3
         Top             =   720
         Width           =   615
      End
      Begin VB.OptionButton Option2 
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   720
         Width           =   975
      End
      Begin VB.OptionButton Option1 
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   360
         Width           =   2295
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Height          =   255
         Left            =   1920
         TabIndex        =   9
         Top             =   840
         Width           =   375
      End
   End
   Begin VB.CommandButton cmdPrint 
      Height          =   375
      Left            =   5520
      TabIndex        =   7
      Top             =   240
      Width           =   1335
   End
   Begin VB.ListBox List1 
      Height          =   1425
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5295
   End
End
Attribute VB_Name = "frmPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Aide_Click()
    Shell App.Path & "\HelpDeal.exe DEAL|EMILIE|1|TURBO-PRINTER", vbMaximizedFocus
End Sub

Private Sub cmdPrint_Click()

Dim x As Printer
Dim i As Integer
'Dim ExcelApp As Excel.Application


'On Error Resume Next

'-> Afficher le sablier pour impression
Me.MousePointer = 11

'-> Tester l'imprimante OutLook
'If Me.List1.Text = "Exporter vers Messagerie OutLook" Then
'    frmOutLook.Show vbModal
'    PrintOk = False
'    Unload Me
'    Exit Sub
'End If

'-> Tester si imprimante HTML
'If Me.List1.Text = "Exporter au format HTML" Then
'    '-> Lancer Turbo-Web
'    Shell App.Path & "\TurboWeb.exe C:\Travailv51\BS002698.fxh|DUMEZ-GTM.HTM"
'    '-> Lancer le navigateur
'    Shell "C:\Program Files\Plus!\Microsoft Internet\IEXPLORE.EXE C:\travailv51\DUMEZ-GTM.HTM", vbNormalFocus
'    PrintOk = False
'    Unload Me
'    Exit Sub
'End If

'If Me.List1.Text = "Exporter vers Tableau Excel" Then
'    '-> Cr�er un lien m�moire vers Excel
'    Set ExcelApp = New Excel.Application
'    ExcelApp.Workbooks.Open "C:\travailv51\Tableau_Bord.xls"
'    ExcelApp.Application.Visible = True
'    PrintOk = False
'    Unload Me
'    Exit Sub
'End If


'If UCase$(Me.List1.Text) = "MICROSOFT WORD" Then
'    Unload Me
'    PrintPageWord 1
'    Exit Sub
'End If


'-> Faire setting de l'imprimante
For Each x In Printers
    If x.DeviceName = Me.List1.Text Then
        Set Printer = x
        Set TempPrinter = x
        Exit For
    End If
Next

'-> Indiquer que l'on a cliqu� sur Imprim�
PrintOk = True

'-> Nombre de copies
nCopies = CInt(Me.NbCopies.Text)

'-> Indiquer le mode d'impression retenu
If Me.Option1.Value Then
    PrintMode = 1
    PageMin = PageCours
    PageMax = PageCours
ElseIf Me.Option2.Value Then
    If Me.Text1.Text = "" Or Not IsNumeric(Text1.Text) Then
        MsgBox "Veuillez s�lectionner la page mini.", vbCritical + vbOKOnly, "Erreur"
        Me.Text1.SetFocus
        Exit Sub
    Else
        PageMin = CInt(Me.Text1.Text)
    End If
    If Me.Text2.Text = "" Or Not IsNumeric(Text2.Text) Then
        MsgBox "Veuillez s�lectionner la page maxi.", vbCritical + vbOKOnly, "Erreur"
        Me.Text2.SetFocus
        Exit Sub
    Else
        PageMax = CInt(Me.Text2.Text)
    End If
    PrintMode = 2
Else
    PrintMode = 3
    PageMin = 1
    PageMax = NbPage
End If

Unload Me

End Sub

Private Sub Form_Load()

Dim x As Printer
Dim aLb As Libelle

'-> Ajouter les imprimantes dans la liste
For Each x In Printers
    Me.List1.AddItem x.DeviceName
    '-> S�lectionner dans la liste si c'est l'imprimante par d�faut
    If x.DeviceName = Printer.DeviceName Then _
                      Me.List1.Selected(Me.List1.ListCount - 1) = True
Next

'-> Ajouter une imprimant vers Outlook
'Me.List1.AddItem "Exporter vers Messagerie OutLook"

'-> Ajouter une imprimante vers Internet
'Me.List1.AddItem "Exporter au format HTML"

'-> Ajouter une imprimante Export Vers Excel
'Me.List1.AddItem "Exporter vers Tableau Excel"

'-> Ajouter l'imprimante WORD
'Me.List1.AddItem "Microsoft Word"

'-> Gestion des libelles
Set aLb = Libelles("FRMPRINT")

Me.Caption = aLb.GetCaption(1)
Me.Frame1.Caption = aLb.GetCaption(2)
Me.Option1.Caption = aLb.GetCaption(3)
Me.Option2.Caption = aLb.GetCaption(4)
Me.Label1.Caption = aLb.GetCaption(5)
Me.Option3.Caption = aLb.GetCaption(6)
Me.Frame2.Caption = aLb.GetCaption(7)
Me.cmdPrint.Caption = aLb.GetCaption(8)

Set aLb = Nothing

'-> Bloquer les options
Me.Option3.Value = True

End Sub

Private Sub Option1_Click()

    '---> Page en cours

    SelPrint = 0
    Me.Text1.Enabled = False
    Me.Text2.Enabled = False


End Sub

Private Sub Option2_Click()

    '---> Page born�e

    SelPrint = 1
    Me.Text1.Enabled = True
    Me.Text2.Enabled = True
    Me.Text1.SetFocus

End Sub

Private Sub Option3_Click()

    '---> Le fichier en entier

    SelPrint = 2
    Me.Text1.Enabled = False
    Me.Text2.Enabled = False

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)


    Select Case KeyAscii
    
    
        Case Is = 8, 48 To 57
        
        Case Else
        
            KeyAscii = 0
            
    End Select

End Sub


Private Sub VScroll1_Change()
    Me.NbCopies.Text = Me.VScroll1.Value
End Sub
