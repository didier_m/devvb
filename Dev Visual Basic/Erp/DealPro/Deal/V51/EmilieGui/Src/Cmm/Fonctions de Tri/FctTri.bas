Attribute VB_Name = "FctTri"
Option Compare Text
Public Function Tri(strToTri As String, SepEntry As String, SepRowId As String) As String

Dim nT As ObjTri
Dim T As ObjTri
Dim CurElement As ObjTri
Dim i As Long
Dim DefEntry As String
Dim Tries As Collection
Dim NonTries As Collection


'-> Initialiser les collections de tri
Set Tries = New Collection
Set NonTries = New Collection

'-> Cr�er la collection des �l�ments non tries
For i = 1 To NumEntries(strToTri, SepEntry)
    '-> R�cup�ration de l'entr�e
    DefEntry = Entry(i, strToTri, SepEntry)
    '-> Cr�er un objet non tri�
    Set nT = New ObjTri
    nT.Value = Entry(1, DefEntry, SepRowId)
    nT.ReturnValue = Entry(2, DefEntry, SepRowId)
    nT.Key = "ITEM" & i
    NonTries.Add nT, nT.Key
Next

'-> Tri des �l�ments dans l'ordre croissant
Do While NonTries.Count <> 0
    '-> Pointer sur le premier �l�ment de la collection
    Set CurElement = NonTries(1)
    '-> Ajout dans la collection si c'est le premier �l�ment que l'on trie
    If Tries.Count = 0 Then
        Tries.Add CurElement, CurElement.Key
        NonTries.Remove (CurElement.Key)
    Else
        Do
            For Each T In Tries
                If CurElement.Value < T.Value Then
                    '-> Ajouter dans la collection des �l�ments tri�s
                    Tries.Add CurElement, CurElement.Key, T.Key
                    '-> Supprimer de la collection des �l�ments non tri�s
                    NonTries.Remove (CurElement.Key)
                    Exit Do
                Else
                    '-> Si on a analys� le dernier �l�ment sortir
                    If Tries(Tries.Count).Key = T.Key Then
                        '-> Ajouter dans la collection des �l�ments tri�s
                        Tries.Add CurElement, CurElement.Key, , T.Key
                        '-> Supprimer de la collection des �l�ments non tri�s
                        NonTries.Remove (CurElement.Key)
                        Exit Do
                    End If
                End If
            Next
        Loop
    End If 'Si premier �l�ment tri�
Loop 'Pour tous les �l�ments non tri�s

'-> Cr�ation de la chaine de retour de tri
For Each T In Tries
    If Tri = "" Then
        Tri = T.ReturnValue
    Else
        Tri = Tri & SepEntry & T.ReturnValue
    End If
Next 'Pour tous les elements

End Function

