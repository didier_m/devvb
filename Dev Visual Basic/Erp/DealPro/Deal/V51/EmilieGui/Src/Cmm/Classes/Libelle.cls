VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Libelle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'***************************************************************
'*                                                             *
'* Cette classe sert � g�rer les lib�ll�s en multi-langue      *
'*                                                             *
'***************************************************************

'-> Variable de type private qui sert � stoocker les libelles lus dans le fichier ini
Private Captions() As String
Private ToolTips() As String
Public NbKey As Integer 'nombre de cl�


Public Sub SetKeys(ByVal KeysDef As String)

    '---> Procedure charg�e de cr�er les diff�rentes matrices. KeyDefs contient _
    la liste des cl�s avec un s�parateur chr(0)
    
    Dim KeyDef As String 'd�finition d'une cle
    Dim KeyValue As String 'valeur de la cl�
    Dim i As Integer
    
    '-> Nb de cle
    NbKey = NumEntries(KeysDef, Chr(0))
    '-> Redimension des matrices
    ReDim Captions(1 To NbKey)
    ReDim ToolTips(1 To NbKey)
    
    For i = 1 To NbKey
    
        '-> Intialisation
        KeyDef = Entry(i, KeysDef, Chr(0))
        KeyValue = Entry(2, KeyDef, "=")
        
        '-> Sauvegarde du libelle
        Captions(i) = Entry(1, KeyValue, "�")
            
        '-> Analyse de la valeur
        If InStr(1, KeyValue, "�") <> 0 Then ToolTips(i) = Entry(2, KeyValue, "�")
    
    Next 'pour toutes les cl�s

End Sub

Public Function GetCaption(ByVal NumCaption As Integer) As String

    '---> Affiche un caption selon son numero
    
    On Error GoTo GestError
    
    GetCaption = Captions(NumCaption)
    Exit Function
    
GestError:
        GetCaption = ""
    

End Function

Public Function GetToolTip(ByVal NumTip As Integer) As String

    On Error GoTo GestError
    
    '---> Affiche un caption selon son numero
    GetToolTip = ToolTips(NumTip)
    Exit Function
    
GestError:
        GetToolTip = ""


End Function

