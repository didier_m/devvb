Attribute VB_Name = "fctB3D_Bas"
'-> Collection qui stocke l'acc�s au catalogue
Public Catalogue As Collection

'-> Collection des FNAME
Public fNames As Collection

'-> Collection des idents
Public Idents As Collection

'-> Variable qui indique le code langue s�lectionn� par l'utilisateur Initialis� � 1
Public CurCodeLangue As Integer

'-> Path du fichier ASCII du catalogue "EXT-DICT.DAT
Public FichierCatalogue As String

'-> Path du fichier ASCII des liens Catalogue/B3D "EXT-DICT.LNK"
Public LienCatalogueB3D As String

'-> Path du fichier Ascii du dictionnaire
Public FichierDictionnaire As String

'-> Indique le path du fichier B3D.INI
Public PathB3DIni As String

'-> Indique l'ident en cours
Public CurIdent As String
Public Sub Initialisation_B3D()


Dim Res As Long
Dim lpBuffer As String

'-> Initialiser les collections
Set Catalogue = New Collection
Set fNames = New Collection
Set Idents = New Collection

'-> Initialiser le code langue en cours
CurCodeLangue = 1

'-> R�cup�ration des ini
PathB3DIni = App.Path & "\B3D.ini"

'-> R�cup�ration des informations sur les fichiers
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("FILE", "CATALOGUE", "", lpBuffer, Len(lpBuffer), PathB3DIni)
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    FichierCatalogue = lpBuffer
End If

lpBuffer = Space$(2000)
Res = GetPrivateProfileString("FILE", "B3D", "", lpBuffer, Len(lpBuffer), PathB3DIni)
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    FichierDictionnaire = lpBuffer
End If

lpBuffer = Space$(2000)
Res = GetPrivateProfileString("FILE", "LINK", "", lpBuffer, Len(lpBuffer), PathB3DIni)
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    LienCatalogueB3D = lpBuffer
End If


End Sub


Public Sub LoadCatalogue()

Dim Ligne As String
Dim i As Integer
Dim aEmilieObj As EmilieObj
Dim hdlFichier As Integer
Dim Ident As String
Dim hdlFile As Long


'-> V�rifier si on trouve le fichier des liens
If Trim(FichierCatalogue) = "" Then Exit Sub
If Dir$(FichierCatalogue, vbNormal) = "" Then Exit Sub

'-> R�cup�ration d'u handle de fichier
hdlFichier = FreeFile

'-> Ouverture du fichier
Open FichierCatalogue For Input As #hdlFichier

'-> Boucle d'analyse
Do While Not EOF(hdlFichier)
    '-> Lecture de la ligne
    Line Input #hdlFichier, Ligne
    If Trim(Ligne) <> "" Then
        '-> Tester si mot cle = ""
        If Trim(Entry(2, Entry(1, Ligne, "�"), "|")) <> "" Then
            '-> Cr�ation d'un objet catalogue
            Set aEmilieObj = New EmilieObj
            '-> Charger la ligne dans l'objet
            SetLigne aEmilieObj, Ligne
            '-> Enregistrer l'objet dans la collection
            Ident = Trim(Entry(2, Entry(1, Ligne, "�"), "|"))
            Ident = Trim(Ident & "|" & Entry(2, Entry(2, Ligne, "�"), "|"))
            Catalogue.Add aEmilieObj, UCase$(Ident)
            Ident = ""
        End If
    End If ' Si ligne � blanc
    Ligne = ""
Loop

'-> Fermer le fichier
Close #hdlFichier

End Sub
Public Sub SetLigne(aEmilieObj As EmilieObj, LigneDef As String)

Dim i As Integer

Dim ValeurZone As String

'-> R�cup�ration des zones
aEmilieObj.Ident = Trim(Entry(1, LigneDef, "�"))
aEmilieObj.MotCle = Trim(Entry(2, LigneDef, "�"))
aEmilieObj.Progiciel = Trim(Entry(3, LigneDef, "�"))
aEmilieObj.Image = Trim(Entry(4, LigneDef, "�"))
aEmilieObj.Son = Trim(Entry(5, LigneDef, "�"))
aEmilieObj.Mot_Master = Trim(Entry(6, LigneDef, "�"))
aEmilieObj.TypeWidget = Trim(Entry(7, LigneDef, "�"))
aEmilieObj.DataType = Trim(Entry(8, LigneDef, "�"))
aEmilieObj.WidgetFormat = Trim(Entry(9, LigneDef, "�"))
aEmilieObj.WidgetWidth = Trim(Entry(10, LigneDef, "�"))
aEmilieObj.WidgetHeight = Trim(Entry(11, LigneDef, "�"))
aEmilieObj.Filled_Type = Trim(Entry(12, LigneDef, "�"))
aEmilieObj.Filled_Ascii = Trim(Entry(13, LigneDef, "�"))
aEmilieObj.c_Sensitive = Trim(Entry(14, LigneDef, "�"))
aEmilieObj.Defaut = Trim(Entry(15, LigneDef, "�"))
aEmilieObj.Valeur = Trim(Entry(16, LigneDef, "�"))
aEmilieObj.Link_Input = Trim(Entry(17, LigneDef, "�"))
aEmilieObj.Link_Output = Trim(Entry(18, LigneDef, "�"))
aEmilieObj.Link_Doc = Trim(Entry(19, LigneDef, "�"))

'-> Nom par langue
For i = 1 To 10
    '-> Positionner le code langue
    aEmilieObj.CodeLangue = i
    '-> Charger la valeur de la zone
    aEmilieObj.Fill_Label = Trim(Entry(i + 19, LigneDef, "�"))
Next

'-> Label colonne
For i = 1 To 10
    '-> Positionner le code langue
    aEmilieObj.CodeLangue = i
    '-> Charger la valeur de la zone
    aEmilieObj.Col_Label = Trim(Entry(i + 29, LigneDef, "�"))
Next

'-> FamDict
aEmilieObj.FamDict = Trim(Entry(40, LigneDef, "�"))
'-> Type de Controle
aEmilieObj.Type_Control = Trim(Entry(41, LigneDef, "�"))
'-> Table de correspondance
aEmilieObj.Table_Correspondance = Trim(Entry(42, LigneDef, "�"))

End Sub

Public Sub LoadCatalogueB3D()

'---> Chargement du fichier des liens entre le B3D et le catalogue

Dim hdlFile As Integer
Dim aEmilieObj As EmilieObj
Dim Ligne As String
Dim aLinkFname As LinkFname
Dim aLinkIname As LinkIname
Dim aLinkInterface As LinkInterface
Dim AccesKey As String
Dim OrderKey As String
Dim LnkValue As String
Dim ObjValue As String

'-> V�rifier si on trouve le fichier des liens
If Trim(LienCatalogueB3D) = "" Then Exit Sub
If Dir$(LienCatalogueB3D, vbNormal) = "" Then Exit Sub

'-> Ouverture du fichier
hdlFile = FreeFile
Open LienCatalogueB3D For Input As #hdlFile

'-> Boucle d'analyse du fichier
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    If Trim(Ligne) <> "" Then
        '-> R�cup�rer les valeurs
        AccesKey = Entry(1, Ligne, "�")
        OrderKey = Entry(2, Ligne, "�")
        LnkValue = Entry(3, Ligne, "�")
        '-> Test de valeurs lues
        If Trim(AccesKey) = "" Or Trim(OrderKey) = "" Or Trim(LnkValue) = "" Then
        Else
            '-> Tous les champs sont renseign�s : Charger l'objet Emilie
            Set aEmilieObj = Catalogue(AccesKey)
            
            '-> Selon le type d'ordre
            Select Case UCase$(OrderKey)
                Case "CREATE"
                    '-> Cr�ation du fname
                    Set aLinkFname = New LinkFname
                    aLinkFname.Fname = UCase$(LnkValue)
                    '-> Ajout dans la collection
                    aEmilieObj.fNames.Add aLinkFname, LnkValue
                                        
                Case "DIRECT"
                    '-> Indiquer que c'est un index direct
                    Set aLinkFname = aEmilieObj.fNames(LnkValue)
                    aLinkFname.IsIndexDirect = True
                    
                Case "CHAMPREF"
                    '-> Indiquer que c'est un champ refrence
                    Set aLinkFname = aEmilieObj.fNames(LnkValue)
                    aLinkFname.IsChampRef = True
                                    
                Case "INAME"
                    '-> R�cup�rer le nom de l'index
                    ObjValue = Entry(4, Ligne, "�")
                    If Trim(ObjValue) <> "" Then
                        '-> Pointer sur le Fname associ�
                        Set aLinkFname = aEmilieObj.fNames(LnkValue)
                        
                        '-> Ajouter un lien vers le Iname
                        Set aLinkIname = New LinkIname
                        aLinkIname.iName = ObjValue
                        aLinkFname.LinkInames.Add aLinkIname, ObjValue
                    
                    End If 'Si lien renseign�
                                                            
                Case "INTERFACE"
                    '-> R�cup�rer le nom de l'interafce
                    ObjValue = Entry(4, Ligne, "�")
                    If Trim(ObjValue) <> "" Then
                        '-> Pointer sur le Fname associ�
                        Set aLinkFname = aEmilieObj.fNames(LnkValue)
                        
                        '-> Ajouter un interface vers le fname
                        Set aLinkInterface = New LinkInterface
                        aLinkInterface.Name = ObjValue
                        aLinkFname.LinkInterfaces.Add aLinkInterface, ObjValue
                    
                    End If 'Si lien renseign�
            End Select
        End If 'Si valeur = ""
    End If 'Si ligne = ""
Loop 'Boucle d'analyse du fichier

'-> Fermer le fichier
Close #hdlFile

End Sub

Public Sub OpenDictionnaireB3BD()

'---> Fonction qui ouvre le Dico B3D

Dim aFname As Fname
Dim aField As Field
Dim aIname As iName
Dim aIField As IFIELD
Dim aIntField As IntField
Dim aInt As Interface
'-> Pour ajouter dans les Inames
Dim aLinkInt As LinkInterface
Dim i As Integer
Dim TempoStr As String
Dim Res As Long
Dim Ligne As String
Dim hdlFile As String

'-> V�rifier si on trouve le fichier
If Trim(FichierDictionnaire) = "" Then Exit Sub
If Dir$(FichierDictionnaire) = "" Then Exit Sub

'-> R�cup�ration d'un hanlde pour ouverture du fichier
hdlFile = FreeFile
Open FichierDictionnaire For Input As #hdlFile
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    Select Case UCase$(Entry(1, Ligne, "�"))
        Case "FNAME" 'Charger un fname
        
            '-> Cr�ation d'un nouvel Fname
            Set aFname = New Fname
            
            '-> Setting des valeurs
            aFname.Name = UCase$(Trim(Entry(2, Ligne, "�")))
            aFname.Ident = UCase$(Trim(Entry(3, Ligne, "�")))
            aFname.IsReplica = CBool(UCase$(Trim(Entry(4, Ligne, "�"))))
            aFname.IsStat = CBool(UCase$(Trim(Entry(5, Ligne, "�"))))
            aFname.IndexDirect = UCase$(Trim(Entry(6, Ligne, "�")))
            aFname.iOrdre = CLng(UCase$(Trim(Entry(7, Ligne, "�"))))
            For i = 1 To 10
                aFname.CodeLangue = i
                aFname.Designation = Trim(Entry(7 + i, Ligne, "�"))
            Next
            aFname.ChampRef = Entry(18, Ligne, "�")
            If NumEntries(Ligne, "�") = 18 Then
                aFname.IsChampRef = False
            Else
                aFname.IsChampRef = CBool(UCase$(Trim(Entry(19, Ligne, "�"))))
            End If
            '-> Ajouter le fname dans la collection
            fNames.Add aFname, aFname.Ident & "|" & aFname.Name
            
        Case "FIELD" 'Champ associ� � un fname
        
            '-> Cr�ation d'un nouveau champ
            Set aField = New Field
            
            '-> Setting des valeurs
            aField.Name = UCase$(Trim(Entry(2, Ligne, "�")))
            aField.Ident = UCase$(Trim(Entry(3, Ligne, "�")))
            aField.Ordre = CLng(UCase$(Trim(Entry(4, Ligne, "�"))))
            aField.IsReplica = CBool(UCase$(Trim(Entry(5, Ligne, "�"))))
            aField.IsCryptage = CBool(UCase$(Trim(Entry(6, Ligne, "�"))))
            aField.IsHisto = CBool(UCase$(Trim(Entry(7, Ligne, "�"))))
            aField.IsIndexDirect = CBool(UCase$(Trim(Entry(8, Ligne, "�"))))
            aField.Fname = UCase$(Trim(Entry(9, Ligne, "�")))
            
            '-> Ajouter le champ dans les fname
            aFname.Fields.Add aField, aField.Name
        
        Case "INAME" 'Index associ� � un fname
        
            '-> Cr�ation d'un nouveau Iname
            Set aIname = New iName
        
            '-> Setting des propri�t�s
            aIname.Code = UCase$(Trim(Entry(2, Ligne, "�")))
            aIname.Ident = UCase$(Trim(Entry(3, Ligne, "�")))
            aIname.Fname = UCase$(Trim(Entry(4, Ligne, "�")))
            aIname.iOrdre = CLng(UCase$(Trim(Entry(5, Ligne, "�"))))
            aIname.IsUnique = CBool(UCase$(Trim(Entry(6, Ligne, "�"))))
            For i = 1 To 10
                aIname.CodeLangue = i
                aIname.Designation = Trim(Entry(6 + i, Ligne, "�"))
            Next
            TempoStr = UCase$(Trim(Entry(17, Ligne, "�")))
            '-> Cr�ation des liens dans les champs
            If Trim(TempoStr) <> "" Then
                For i = 1 To NumEntries(TempoStr, "|")
                    Set aLinkInt = New LinkInterface
                    aLinkInt.Name = UCase$(Trim(Entry(1, TempoStr, "|")))
                    '-> Ajouter le lien dans l'index
                    aIname.LinkInterfaces.Add aLinkInt, aLinkInt.Name
                Next
            End If
            '-> Ajouter l'index dans le fname
            aFname.Inames.Add aIname, aIname.Code
                        
        Case "IFIELD" 'Champ associ� a un index
        
            '-> Cr�ation d'un nouveau champ d'index
            Set aIField = New IFIELD
            
            '-> Setting des propri�t�s
            aIField.Name = UCase$(Entry(2, Ligne, "�"))
            aIField.Ident = UCase$(Entry(3, Ligne, "�"))
            aIField.Fname = UCase$(Entry(4, Ligne, "�"))
            aIField.iName = UCase$(Entry(5, Ligne, "�"))
            aIField.Ordre = CLng(UCase$(Entry(6, Ligne, "�")))
            aIField.TypeCondition = SetValueProp(UCase$(Entry(7, Ligne, "�")))
            aIField.Cumul = CBool(UCase$(Entry(8, Ligne, "�")))
            aIField.ValMini = UCase$(Entry(9, Ligne, "�"))
            aIField.ValMaxi = UCase$(Entry(10, Ligne, "�"))
            
            '-> Ajouter le champ dans le iname
            aIname.iFields.Add aIField, aIField.Name
            
        Case "INTERFACE" 'Interface associ� � un fname
        
            '-> Cr�er un nouvel Interface
            Set aInt = New Interface
            
            '-> Setting des propri�t�s
            aInt.Name = UCase$(Entry(2, Ligne, "�"))
            aInt.Index = UCase$(Entry(3, Ligne, "�"))
            aInt.Intput_Output = CInt(UCase$(Entry(4, Ligne, "�")))
            aInt.Tolerance = UCase$(Entry(5, Ligne, "�"))
            aInt.Separateur = UCase$(Entry(6, Ligne, "�"))
            For i = 1 To 10
                aInt.CodeLangue = i
                aInt.Designation = Entry(6 + i, Ligne, "�")
            Next
            
            '-> Ajouter l'interface dans le Fname
            aFname.Interfaces.Add aInt, aInt.Name
        
        Case "INTFIELD" 'Champ associ� � un interface

            '-> Cr�er un nouveau Champ d'interface
            Set aIntField = New IntField
            
            '-> Setting des propri�t�s
            aIntField.Field = UCase$(Entry(2, Ligne, "�"))
            aIntField.Position = UCase$(Entry(3, Ligne, "�"))
            aIntField.Longueur = UCase$(Entry(4, Ligne, "�"))
            aIntField.IsModif = CBool(UCase$(Entry(5, Ligne, "�")))
            aIntField.GestionErreur = CInt(UCase$(Entry(6, Ligne, "�")))
            aIntField.Defaut = UCase$(Entry(7, Ligne, "�"))
            aIntField.Table_Correspondance = UCase$(Entry(8, Ligne, "�"))

            '-> Ajouter le champ dans l'interface
            aInt.Fields.Add aIntField, aIntField.Field
            
    End Select

Loop 'Boucle d'analyse principale

Close #hdlFile

End Sub


Public Function SetValueProp(ByVal LibProp As String) As Integer

Select Case UCase$(LibProp)
    Case ""
        SetValueProp = 0
    Case "EQ"
        SetValueProp = 1
    Case "INC"
        SetValueProp = 2
    Case "NIN"
        SetValueProp = 3
    Case "DIF"
        SetValueProp = 4
    Case "LE"
        SetValueProp = 5
    Case "GE"
        SetValueProp = 6
    Case "LT"
        SetValueProp = 7
    Case "GT"
        SetValueProp = 8
    Case "BEG"
        SetValueProp = 9
    Case "MAT"
        SetValueProp = 10
End Select


End Function




Public Function TestCondition(ByRef Id_Fname As String, ByRef strChamp As String, _
                               ByRef strCondition As String, ByRef strValMini As String, _
                               ByRef strValMaxi As String, ByVal Top_Lock As String, _
                               ByVal Num_Enreg As String) As Boolean


Dim boolRetour As Boolean
Dim aRec As Recordset
Dim strTest As String
Dim aEmilieObj As EmilieObj

'-> Pointer sur l'objet du catalogue
Set aEmilieObj = Catalogue(UCase$(Ident) & "|" & UCase$(aIField.Name))

'-> Pointer sur D_DATA
Set aRec = aConnect.OpenRecordset("SELECT [ZON_DATA] WHERE [ID_FNAME] = '" & Id_Fname & "' AND [NUM_ENREG] = '" & Num_Enreg _
                                   & "' AND [FIELD] = '" & strChamp & "'")
                                   
'-> R�cup�rer une valuer differente si il est lock�
strTest = ""
If Top_Lock <> "" Then
    strTest = aRec.Fields("ZON_LOCK")
Else
    strTest = aRec.Fields("ZON_DATA")
End If

boolRetour = True

Select Case UCase$(strCondition)
        
    Case "EQ"
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(strTest) <> CDbl(strValMini) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) <> CInt(strValMini) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest <> strValMini Then boolRetour = False
        
        End Select
        
    Case "INC"
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(strTest) < CDbl(strValMini) Or CDbl(strTest) > CDbl(strValMaxi) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) < CInt(strValMini) Or CInt(strTest) > CInt(strValMaxi) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest < strValMini Or strTest > strValMaxi Then boolRetour = False
        End Select
        
    Case "NIN"
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(strTest) > CDbl(strValMini) Or CDbl(strTest) < CDbl(strValMaxi) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) > CInt(strValMini) Or CInt(strTest) < CInt(strValMaxi) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest > strValMini Or strTest < strValMaxi Then boolRetour = False
        End Select
            
    Case "DIF"
        
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(strTest) = CDbl(strValMini) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) = CInt(strValMini) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest = strValMini Then boolRetour = False
        End Select
        
    Case "LE"
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(strTest) > CDbl(strValMini) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) > CInt(strValMini) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest > strValMini Then boolRetour = False
        
        End Select
        
    Case "GE"
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(strTest) < CDbl(strValMini) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) < CInt(strValMini) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest < strValMini Then boolRetour = False
        
        End Select
        
    Case "LT"
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(strTest) >= CDbl(strValMini) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) >= CInt(strValMini) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest >= strValMini Then boolRetour = False
        
        End Select
        
    Case "GT"
    
        Select Case Left$(aEmilieObj.DataType, 3)
            Case "DEC"
                If CDbl(strTest) <= CDbl(strValMini) Then boolRetour = False
            
            Case "INT"
                If CInt(strTest) <= CInt(strValMini) Then boolRetour = False
            
            Case "DAT", "CHA", "LOG"
                If strTest <= strValMini Then boolRetour = False
        
        End Select
        
    Case "BEG"
    
        If Left$(aEmilieObj.DataType, 3) = "CHA" Then
            If strValMini <> Left$(strTest, Len(strValMini)) Then boolRetour = False
        End If
        
        
    Case "MAT"
        
        If Left$(aEmilieObj.DataType, 3) = "CHA" Then
            If InStr(UCase$(strValMini), UCase$(strTest)) = 0 Then boolRetour = False
        End If

End Select

TestCondition = boolRetour


End Function

