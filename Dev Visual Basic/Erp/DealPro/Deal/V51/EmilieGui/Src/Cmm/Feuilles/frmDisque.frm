VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDisque 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   5820
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6960
   Icon            =   "frmDisque.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5820
   ScaleWidth      =   6960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2760
      Top             =   5160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisque.frx":014A
            Key             =   "LecteurReseau"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisque.frx":059C
            Key             =   "Folder"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisque.frx":06F6
            Key             =   "OpenFolder"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisque.frx":0B48
            Key             =   "DisqueDur"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisque.frx":0F9A
            Key             =   "CdRom"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisque.frx":13EC
            Key             =   "PosteTravail"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisque.frx":183E
            Key             =   "Reseau"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisque.frx":1C90
            Key             =   "Prog"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Ok 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   6120
      Picture         =   "frmDisque.frx":1FAA
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   1
      Top             =   5280
      Width           =   630
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   5175
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6975
      _ExtentX        =   12303
      _ExtentY        =   9128
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      FullRowSelect   =   -1  'True
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      BorderStyle     =   1
      Appearance      =   1
   End
End
Attribute VB_Name = "frmDisque"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Files As String
Private Sub Form_Load()

'ATTENTION : les modules : CommunLib.bas, Messprog.bas et la classe Libelle doivent _
�tre charg�s


Dim aLb As Libelle
Dim LpBuffer As String
Dim Res As Long
Dim n As Integer
Dim NomDisque As String
Dim Image As String
Dim Parent As String


'-> Charger les libelles

Set aLb = Libelles("FRMDISQUE")

Me.Caption = aLb.GetCaption(1)
Me.Ok.ToolTipText = aLb.GetCaption(2)



'-> Ajouter l'icone du Poste de travail
Me.TreeView1.Nodes.Add , , "PosteTravail", aLb.GetCaption(5), "PosteTravail"
Parent = "PosteTravail"

Set aLb = Nothing

'-> charger les Disques  existants
LpBuffer = String$(2048, 0)
Call GetLogicalDriveStrings(2047, LpBuffer)
Do
    n = InStr(LpBuffer, Chr$(0))
    If n > 1 Then
        NomDisque = Left$(LpBuffer, n - 2)
        Res = GetDriveType(NomDisque)
        Select Case Res
            Case 3
                Image = "DisqueDur"
            Case 4
                Image = "LecteurReseau"
            Case 5
                Image = "CdRom"
        End Select
        Me.TreeView1.Nodes.Add Parent, 4, NomDisque, "(" & NomDisque & ")", Image
        LpBuffer = Mid$(LpBuffer, n + 1)
    End If
Loop Until n <= 1


End Sub

Private Sub Ok_Click()

Dim aLb As Libelle
Dim Pathdir As String
Dim i As Integer

On Error Resume Next

'-> Positionner la variable globale sur le chemin

If UCase$(Me.TreeView1.SelectedItem.Key) = "POSTETRAVAIL" Then
    Set aLb = Libelles("FRMDISQUE")
    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Set aLb = Nothing
    Exit Sub
End If


'-> Construire le path � analyser
Pathdir = Me.TreeView1.SelectedItem.FullPath
i = InStr(1, Pathdir, "\")
Pathdir = Mid$(Pathdir, i + 2, Len(Pathdir) - 1)
Pathdir = Replace(Pathdir, ")", "")

If Mid$(Pathdir, Len(Pathdir), 1) <> "\" Then Pathdir = Pathdir & "\"

SelFrmDisque = Pathdir

Unload Me

End Sub

Private Sub TreeView1_Click()


Dim i As Integer
Dim Pathdir As String


'-> Ne rien faire s'il y a d�ja les nodes
If Me.TreeView1.SelectedItem.Children <> 0 Then Exit Sub

Me.TreeView1.MousePointer = 11

'-> Construire le path � analyser
Pathdir = Me.TreeView1.SelectedItem.FullPath
i = InStr(1, Pathdir, "\")
Pathdir = Mid$(Pathdir, i + 2, Len(Pathdir) - 1)
Pathdir = Replace(Pathdir, ")", "\")

If Mid$(Pathdir, Len(Pathdir), 1) <> "\" Then Pathdir = Pathdir & "\"

'-> Afficher les r�pertoires
DisplayDir Pathdir

'-> Afficher les fichiers
If Files <> "" Then DisplayFiles Pathdir


Me.TreeView1.SelectedItem.Expanded = True

Me.TreeView1.MousePointer = 0

Exit Sub


End Sub


Private Sub DisplayDir(Pathdir As String)

Dim IsRep As Integer
Dim Fichier As String

On Error GoTo GestError

'-> Afficher les sous r�pertoires
Fichier = Dir$(Pathdir, vbDirectory)
Do While Fichier <> ""
        '-> V�rifier qu'il s'agit d'un r�pertoire
        If Fichier <> "." And Fichier <> ".." Then
            IsRep = GetAttr(Pathdir & Fichier) And vbDirectory
            If IsRep >= 16 Then
                Me.TreeView1.Nodes.Add Me.TreeView1.SelectedItem.Key, 4, Me.TreeView1.SelectedItem.Key & Fichier, Fichier, "Folder", "OpenFolder"
            End If
        End If
Continue:
        Fichier = Dir$
Loop

Exit Sub

GestError:

    Select Case Err.Number
    
        Case 52
        
            Resume Next
            
        Case Else
        
            GoTo Continue
    
    End Select


End Sub


Private Sub DisplayFiles(Pathdir As String)

Dim IsRep As Integer
Dim Fichier As String

On Error GoTo GestError

'-> Afficher les sous r�pertoires
Fichier = Dir$(Pathdir & Files)
Do While Fichier <> ""
    Me.TreeView1.Nodes.Add Me.TreeView1.SelectedItem.Key, 4, Me.TreeView1.SelectedItem.Key & Fichier, Fichier, "Prog"
Continue:
    Fichier = Dir$
Loop

Exit Sub

GestError:

    Select Case Err.Number
    
        Case 52
        
            Resume Next
            
        Case Else
        
            GoTo Continue
    
    End Select


End Sub

