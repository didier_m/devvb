Attribute VB_Name = "CommunLib"
'***************************************************************
'*                                                             *
'* Contient les fonctions communes � tout les projets          *
'*                                                             *
'***************************************************************

'-> Variables pour mots cl�s de path
Public PathLecteur As String
Public PathVersion As String
Public PathIdent As String

'-> Variables pour la cr�ation des Path d'analyse
Public RessourcePath As String
'-> Indique le path ou l'on peut trouver les maquettes du client et ses samples
Public ClientPath As String
'-> Variable pour la gestion des imprimantes
Public SelPrint As Integer 'Indique le choix de l'utilisteur : _
' 0 -> Page en cours
' 1 -> Page Born�e
' 2 -> Fichier
Public PrintOk As Boolean 'indique que l'utilisateur a cliqu� sur le bouton imprimer
Public nCopies As Integer 'nombre de copies saisies
Public nPrinter As String
Public nRectoVerso As Boolean
Public curPage As Integer
Public PageMin As Integer
Public PageMax As Integer
Public PrintMode As Integer '-> Indique le choix de l'utilisateur
'-> 1 Page en cours
'-> 2 Page Min:Max
'-> 3 Fichier
'-> Variable pour la feuille de recherche frmDisque : On stoque ici le r�pertoire s�lectionn�
'dans cette feuille
Public SelFrmDisque As String

'-> Gestion des s�parateurs d�cimaux
Public SepDec As String
Public SepMil As String

Public tempPathLocal As String

Private Declare Function GetLocaleInfoVB& Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long)

'-> pour la base de registre
Public Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long
Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long

'-> API pour gestion des messages
Public Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal Hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)

'-> Constante pour formattage du listview
Public Const LVM_SETCOLUMNWIDTH = 4126 '( LVM_FIRST + 30 )
'Public Const CSIDL_PERSONAL = &H5


Public Function InitConvertion()

Dim lpBuffer As String
Dim Res As Long
Dim LOCALE_STHOUSAND As Long
Dim LOCALE_USER_DEFAULT As Long
Dim LOCALE_SDECIMAL As Long

'-> S�parateur de millier : gestion des variables d'environnement
LOCALE_STHOUSAND = &HF
LOCALE_USER_DEFAULT& = &H400
LOCALE_SDECIMAL = 14

lpBuffer = Space$(10)
Res = GetLocaleInfoVB(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, lpBuffer, Len(lpBuffer))
SepDec = Mid$(lpBuffer, 1, Res - 1)
lpBuffer = Space$(10)
Res = GetLocaleInfoVB(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
SepMil = Mid$(lpBuffer, 1, Res - 1)


End Function

Public Function Convert(ByVal StrToAnalyse As String) As String

Dim i As Integer
Dim Tempo As String
Dim FindSep As Boolean

For i = Len(StrToAnalyse) To 1 Step -1
    If Mid$(StrToAnalyse, i, 1) = "." Then
        If Not FindSep Then
            Tempo = SepDec & Tempo
            FindSep = True
        End If
    ElseIf Mid$(StrToAnalyse, i, 1) = "," Then
        If Not FindSep Then
            Tempo = SepDec & Tempo
            FindSep = True
        End If
    Else
        Tempo = Mid$(StrToAnalyse, i, 1) & Tempo
    End If
Next 'Pour tous les caract�res � analyser

Convert = Tempo

End Function

Public Sub GetPathMotCles()

'---> R�cup�ration des cl�s $LECTEUR$ , $VERSION$ , $IDENT$ en fonction du proptah

'-> R�cup�ration de l'index de la racine
i = GetEntryIndex(App.Path, "DEALPRO", "\")

'-> R�cup�ration du nom de la version
PathVersion = Entry(i + 2, App.Path, "\")

If IsMouchard Then Trace "PathVersion :" + PathVersion, 1

'-> R�cup�ration du nom de lectuer logique
i = InStr(1, UCase$(App.Path), "V6")
If i <> 0 Then
    PathLecteur = Mid$(App.Path, 1, i - 1)
Else
    '-> on va lire dans le fichier turbomaq.ini
'    PathLecteur = GetIniString("Param", "$ROOT", IniPath, False)
'    If PathLecteur <> "" Then
'        PathLecteur = Replace(PathLecteur, "/", "\")
'        PathLecteur = Mid$(PathLecteur, 1, InStrRev(UCase$(PathLecteur), "\") - 1) & "\"
'    End If
End If
If IsMouchard Then Trace "PathLecteur :" + PathLecteur, 1

End Sub

Public Function ReplacePathMotCles(PathToReplace As String)

'-> V6 : on change le path du param.ini qui est dans le repertoire de l'executable DEALTOOL
'PathToReplace = App.Path & "\param.ini"

'-> Remplacer $LECTEUR$
If Trim(PathLecteur) <> "" Then PathToReplace = Replace((PathToReplace), "$LECTEUR$", PathLecteur)

'-> Remplacer $IDENT$
If Trim(PathIdent) <> "" Then PathToReplace = Replace((PathToReplace), "$IDENT$", PathIdent)

'-> Remplacer $VERSION$
If Trim(PathVersion) <> "" Then PathToReplace = Replace((PathToReplace), "$VERSION$", PathVersion)

End Function

Public Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

If Len(Separateur) = 1 Then
    On Error Resume Next
    Entry = Split(Vecteur, Separateur)(nEntrie - 1)
    Exit Function
End If


'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Long
Dim PosEnCour As Long
Dim i As Long
Dim CHarDeb As Long
Dim CharEnd As Long

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
    

End Function

Public Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Variant

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Long
Dim i As Long
Dim PosAnalyse As Long

On Error GoTo GestError

PosAnalyse = 1

Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1

Exit Function

GestError:

    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
    

End Function

Public Sub AddItemToList(ByVal Vecteur As String, ByVal aList As ListBox, Optional ByVal Selected As Integer)

'---> Cette fonction  alimente une liste avec toutes les entr�es de vecteur _
'S�parateur "|"

Dim i As Integer
Dim NbEntries As Integer

aList.Clear
NbEntries = NumEntries(Vecteur, "|")
For i = 1 To NbEntries
    aList.AddItem Entry(i, Vecteur, "|")
Next

'-> S�lectionner l'entr�e sp�cifi�e
If Selected <> 0 Then aList.Selected(Selected - 1) = True


End Sub

Public Function AddEntryToMatrice(ByVal Matrice As String, ByVal SepMatrice As String, ByVal EntryToAdd As String) As String

'---> cette fonction rajoute une entr�e dans une matrice. Elle renvoie la matrice mise � jour

'-> Ajouter l'entre�e dans la matrice
If Trim(Matrice) = "" Then
    Matrice = EntryToAdd
Else
    Matrice = Matrice & SepMatrice & EntryToAdd
End If

AddEntryToMatrice = Matrice

End Function

Public Function DeleteEntry(ByVal Matrice As String, ByVal EntryToDel As Integer, ByVal SepMatrice As String) As String

'---> Cette fonction supprime une entr�e dans la matrice. Elle renvoie la matrice mise � jour
Dim NbEntries As Integer
Dim i As Integer
Dim j As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> R�cup�rer le nombre d'entr�e
NbEntries = NumEntries(Matrice, SepMatrice)

'-> Renvoyer un chaine vide s'il n'y a qu'une seule entr�e
If NbEntries = 1 Then
    'Sil n'y a qu'une seule entr�e, vider la matrice
    DeleteEntry = ""
    Exit Function
ElseIf EntryToDel > NbEntries Then
    'Si on doit supprimer une entr�e qui n'existe pas -> renvoyer la matrice d'origine
    DeleteEntry = Matrice
    Exit Function
ElseIf EntryToDel = NbEntries Then
    'Si on doit supprimer la derni�re entr�e, chercher le s�parateur n-1
    i = InStrRev(Matrice, SepMatrice)
    DeleteEntry = Mid$(Matrice, 1, i - 1)
    Exit Function
ElseIf EntryToDel = 1 Then
    '-> Si on doit supprimer la premi�re entr�e, chercher le premier s�parateur
    i = InStr(1, Matrice, SepMatrice)
    DeleteEntry = Mid$(Matrice, i + 1, Len(Matrice) - i)
    Exit Function
Else
    '-> Suppression d'une entr�e
    j = 1
    i = 0
    NbEntries = 0
    Do
        i = InStr(j, Matrice, SepMatrice)
        NbEntries = NbEntries + 1
        If NbEntries = EntryToDel - 1 Then
            'Lecture de la position de d�but
            CHarDeb = i
            CharEnd = InStr(i + 1, Matrice, SepMatrice)
            DeleteEntry = Mid$(Matrice, 1, CHarDeb - 1) & Mid$(Matrice, CharEnd, Len(Matrice) - CharEnd + 1)
            Exit Function
        End If
        j = i + 1
    Loop
End If
    

End Function


Public Function GetEntryIndex(ByVal Matrice As String, ByVal EntryToSearch As String, ByVal SepMatrice As String) As Integer

'---> Cette fonction retourne l'index d'une valeur dans une matrice. _
Attention : Cette fonction retourne la premi�re it�ration trouv�e

On Error Resume Next

Dim i As Integer, j As Integer
Dim NbEntries As Integer

'-> Recherche de la pchaine de caract�re
i = InStr(1, UCase$(Matrice), UCase$(EntryToSearch))
j = 1
NbEntries = 1
'-> Analyse
Do While j < i
    j = InStr(j, Matrice, SepMatrice)
    NbEntries = NbEntries + 1
    j = j + 1
Loop

GetEntryIndex = NbEntries


End Function

Public Function LimitSaisie(ByVal KeyAscii As Integer) As Boolean

'---> Fonction qui limitte la saisie � des nombres entiers

Select Case KeyAscii
    Case 8, 48 To 57, 127
        LimitSaisie = True
        
    Case Else
        LimitSaisie = False
End Select

End Function

Public Function IsLegalName(ByVal NewName As String, Optional Tiret As Boolean) As Boolean

'---> Fonction qui v�rifie le contenu d'un nom pour y supprimer tous les caract�res interdits
' Caract�res interdits : \ / : * " < > | -

Dim FindBad As Boolean


If InStr(1, NewName, "\") <> 0 Then FindBad = True
If InStr(1, NewName, "/") <> 0 Then FindBad = True
If InStr(1, NewName, ":") <> 0 Then FindBad = True
If InStr(1, NewName, "*") <> 0 Then FindBad = True
If InStr(1, NewName, """") <> 0 Then FindBad = True
If InStr(1, NewName, "<") <> 0 Then FindBad = True
If InStr(1, NewName, ">") <> 0 Then FindBad = True
If InStr(1, NewName, "|") <> 0 Then FindBad = True
If Tiret And InStr(1, NewName, "-") <> 0 Then FindBad = True

IsLegalName = Not FindBad


End Function



Public Sub FormatListView(List As Object)

Dim i As Long
Dim x As Object

'-> Ne rien faire si pas de colonnes
If List.ColumnHeaders.count = 0 Then Exit Sub

'-> De base toujours cr�er un enregistrement avec les entetes de colonnes
Set x = List.ListItems.add(, "DEALENREGENTETE")

For i = 0 To List.ColumnHeaders.count - 1
    '-> Ajouter le libelle de l'entete de la colonne
    If i = 0 Then
        x.text = List.ColumnHeaders(1).text
    Else
        x.SubItems(i) = List.ColumnHeaders(i + 1).text
    End If
    SendMessage List.Hwnd, LVM_SETCOLUMNWIDTH, i, 0
Next

List.ColumnHeaders(1).width = List.ColumnHeaders(1).width + 250

'-> Supprimer le premier enregistrement
List.ListItems.Remove ("DEALENREGENTETE")

End Sub

Public Function FormatNum(ByVal ToFormat As String, ByVal Msk As String) As String

'---> Cette fonction a pour but d'analyser une chaine de caract�re et de la retouner
'formatt�e en s�parateur de milier et en nombre de d�cimal

Dim strFormat As String
Dim strtempo As String
Dim strPartieEntiere As String
Dim StrPartieDecimale As String
Dim i As Integer, j As Integer
Dim nbDec As Integer
Dim strSep As String
Dim idNegatif As Integer
Dim Masque As String
Dim Tempo As String

'-> Modifier le contenu de la chaine pour le formatter
For i = 1 To Len(ToFormat)
    If Not IsNumeric(Mid$(ToFormat, i, 1)) And (Mid$(ToFormat, i, 1)) <> SepDec Then
    Else
        Tempo = Tempo & (Mid$(ToFormat, i, 1))
    End If
Next

ToFormat = Tempo

'-> Tester si on envoie une zone num�rique
If Not IsNumeric(ToFormat) Then
    FormatNum = ""
    Exit Function
End If
'-> Charger le masque de la cellule
nbDec = CInt(Entry(1, Msk, "�"))
strSep = Entry(2, Msk, "�")
idNegatif = CInt(Entry(3, Msk, "�"))

'-> Analyse si d�cimale
If nbDec = 0 Then
    '-> Arrondir
    strtempo = CStr(CLng(ToFormat))
    Masque = "#########################################0"
Else
    Masque = "#########################################0." & String(nbDec, "0")
    strtempo = ToFormat
End If

'-> Construction d'un masque assez grand pour formatter n'importe qu'elle zone
strtempo = FORMAT(Abs(strtempo), Masque)

'-> Construction de la partie enti�re
If nbDec <> 0 Then
    strPartieEntiere = Mid$(strtempo, 1, InStr(1, strtempo, SepDec) - 1)
    StrPartieDecimale = SepDec & Mid$(strtempo, InStr(strtempo, SepDec) + 1, nbDec)
Else
    strPartieEntiere = strtempo
    StrPartieDecimale = ""
End If

j = 1
For i = Len(strPartieEntiere) To 1 Step -1
    strFormat = Mid$(strtempo, i, 1) & strFormat
    If j = 3 And i <> 1 Then
        strFormat = strSep & strFormat
        j = 1
    Else
        j = j + 1
    End If
Next

FormatNum = strFormat & StrPartieDecimale

End Function

Public Function GetPath(ByVal InitPath As String) As String

'---> Cette fonction renvois le path de \MQT en fonction de l'endroit d'ou _
est appell�e le programme ( utilis� pour faire la diff�rence en DVL et EXE)

'-> La diff�rence se fait par : Si on trouve le fichier VBP : on est en DVL

If Dir$(InitPath & "\*.vbp") = "" Then
    '-> On est en mode exe
    pos = InStrRev(InitPath, "\")
    BasePath = Mid$(InitPath, 1, pos)
    RessourcePath = BasePath & "Mqt\"
Else
    '-> On est en mode interpr�t�
    RessourcePath = InitStringPath(0) & "\"
End If

GetPath = RessourcePath

End Function

Public Function GetTempFileNameVB(ByVal Id As String, Optional Rep As Boolean) As String

Dim TempPath As String
Dim lpBuffer As String
Dim Result As Long
Dim TempFileName As String

'---> Fonction qui d�termine un nom de fichier tempo sous windows

'-> Recherche du r�pertoire temporaire
lpBuffer = Space$(500)
Result = GetTempPath(Len(lpBuffer), lpBuffer)
TempPath = Mid$(lpBuffer, 1, Result)
If tempPathLocal <> "" Then TempPath = tempPathLocal

'-> Si on ne demande que le r�pertoire de windows
If Rep Then
    GetTempFileNameVB = TempPath
    Exit Function
End If

'-> Cr�ation d'un nom de fichier
TempFileName = Space$(1000)
Result = GetTempFileName(TempPath, Id, 0, TempFileName)
TempFileName = Entry(1, TempFileName, Chr(0))

GetTempFileNameVB = TempFileName

End Function

Public Function GetMyDocumentsPathVB() As String
'-> retourne le repertoire mes spools
On Error Resume Next

'-> On accede au repertoire mes spools
MkDir GetSpecialfolder(CSIDL_PERSONAL) & "\mes spools\"
GetMyDocumentsPathVB = GetSpecialfolder(CSIDL_PERSONAL) & "\mes spools\"

End Function

Public Sub GetFileSizeVB(ByVal NomFichier As String)

Dim hdlFile As Long
Dim Res As Long
Dim aOf As OFSTRUCT

'-> R�cup�ration des infos pour ProgressBar
hdlFile = OpenFile(NomFichier, aOf, OF_READ)
If hdlFile <> -1 Then
    Res = GetFileSize(hdlFile, 0)
    Res = CloseHandle(hdlFile)
End If

End Sub


Public Sub CreatePath(IniPath As String)

Dim i As Integer, j As Integer

'-> Construire le propath � partir de l'emplacement actuel "M:\DealPro\Deal\V51\EmilieGUI\Exe" Chez DEAL
IniPath = App.Path
For j = 1 To 2
    '-> R�cup�ration du dernier \
    i = InStrRev(IniPath, "\")
    IniPath = Mid$(IniPath, 1, i - 1)
Next
If Mid$(IniPath, Len(IniPath), 1) <> "\" Then IniPath = IniPath & "\"
IniPath = IniPath & "Turbo"

End Sub



Public Function GetAppNameByIndex(ByVal IndexApp As Integer) As String

Select Case IndexApp

    Case 1
        GetAppNameByIndex = "Editeur de maquettes graphiques"
            
    Case 2
        GetAppNameByIndex = "Moteur d'�ditions graphiques"
    
    Case 3
        GetAppNameByIndex = "Boite � outils Deal Informatique"
    
End Select

End Function
Public Function SetIniString(AppName As String, lpKeyName As String, IniFile As String, Value As String)

Dim Res As Long
Dim lpBuffer As String


Res = WritePrivateProfileString(AppName, lpKeyName, Value, IniFile)

End Function
Public Function GetIniString(AppName As String, lpKeyName As String, IniFile As String, Error As Boolean) As String

Dim Res As Long
Dim lpBuffer As String
Dim nCnt As Integer
Dim cString As String

lpBuffer = Space$(320000)
Res = GetPrivateProfileString(AppName, lpKeyName, "NULL", lpBuffer, Len(lpBuffer), IniFile)
lpBuffer = Entry(1, lpBuffer, Chr(0))
If lpBuffer = "NULL" Then
    If Error Then
        MsgBox "Impossible de trouver la cl� : " & lpKeyName & _
               " dans la section : " & AppName & " du fichier : " & _
                IniFile, vbCritical + vbOKOnly, "Erreur de lecture"
    End If
    GetIniString = ""
Else
    lpBuffer = Mid$(lpBuffer, 1, Res)
    GetIniString = lpBuffer
End If


End Function


Public Function InitStringPath(IsTurbo As Integer) As String

'---> Cette proc�dure retourne un le path de MQT  si isturbo = 0 _
ou le path du r�pertoire turbo = 1

Dim TempPath As String
Dim i As Integer, j As Integer
Dim LastName As String


'-> R�cup�rer le path en cours
TempPath = App.Path

If IsTurbo = 0 Then
    '-> Recherche du path MQT
    If InStr(1, UCase$(App.Path), "EMILIEGUI\SRC") <> 0 Then
        i = InStr(1, UCase$(App.Path), "EMILIEGUI\SRC")
        TempPath = Mid$(TempPath, 1, i - 1) & "EMILIEGUI\MQT"
        InitStringPath = TempPath
    Else
        TempPath = Replace(UCase$(App.Path), "EMILIEGUI\EXE", "EMILIEGUI\MQT")
        InitStringPath = TempPath & "\"
    End If
    RessourcePath = TempPath
Else
    '-> Recherche du path TURBO
    i = InStr(1, UCase$(TempPath), "DEALPRO\")
    TempPath = Mid$(TempPath, 1, i - 1) & "TURBO"
    InitStringPath = TempPath
    
    Exit Function
    
    
    
'    i = NumEntries(App.Path, "\")
'    TempPath = App.Path
'    Do
'        '-> Get du nom du dernier r�pertoire
'        LastName = Entry(i, TempPath, "\")
'        '-> On fait le test
'        If UCase$(Trim(LastName)) = "DEALPRO" Then
'            InitStringPath = TempPath & "\TURBO"
'            Exit Function
'        End If
'
'        '-> Concatainer le nouveau path
'        j = InStrRev(TempPath, "\")
'        TempPath = Mid$(App.Path, 1, j - 1)
'
'        '-> Ne faire une boucle ind�finie
'        If TempPath = "" Then
'            '-> Renvoyer par d�faut le path en cours
'            InitStringPath = App.Path
'            Exit Function
'        End If
'
'        i = i - 1
'
'    Loop

End If


End Function

Public Function ShowOpenFolder(Title As String, hwndOwner As Long) As String
Dim lngRet As Long, sDummy As String
Dim tBrowseInfo As BrowseInfo


With tBrowseInfo
    .hwndOwner = hwndOwner
    .lpszTitle = lstrcat(Title, "")
    .ulFlags = BIF_RETURNONLYFSDIRS + BIF_DONTGOBELOWDOMAIN
End With

lngRet = SHBrowseForFolder(tBrowseInfo)

If lngRet Then
    sDummy = Space(255)
    SHGetPathFromIDList lngRet, sDummy
    sDummy = Left(sDummy, InStr(sDummy, vbNullChar) - 1)
    ShowOpenFolder = sDummy
Else
    ShowOpenFolder = ""
End If

End Function


Public Function FileExist(Fichier As String) As Boolean

On Error GoTo GestError

If Dir$(Fichier) <> "" Then FileExist = True
Exit Function

GestError:
    FileExist = False

End Function

Public Function IsFile(Fichier As String) As Boolean

On Error GoTo GestError

If FileLen(Fichier) <> 0 Then IsFile = True
Exit Function

GestError:
    IsFile = False

End Function

Public Function GetIniEntry(Section As String, Key As String, FileName As String, Optional DefaultValue As Boolean) As String
'-> pour les gros volumes on le fait a la main
    Dim hdlFile As Integer
    hdlFile = FreeFile
    Open FileName For Input As #hdlFile
    '-> Boucle d'analyse du fichier
    Do While Not EOF(hdlFile)
        '-> Lecture de la ligne
        Line Input #hdlFile, Ligne
        '-> Decrypter la source
        If "[" & Section & "]" = Ligne Then
            Exit Do
        End If
    Loop
    Do While Not EOF(hdlFile)
        '-> Lecture de la ligne
        Line Input #hdlFile, Ligne
        '-> Decrypter la source
        If Entry(1, Ligne, "=") = Key Then
            GetIniEntry = Entry(2, Ligne, "=")
            Exit Do
        End If
    Loop
    Close hdlFile
End Function
  

