Attribute VB_Name = "ApiWin32"
'-> R�cup�ration des caract�ristiques d'une fen�tre
Public Declare Function GetSystemMetrics& Lib "user32" (ByVal nIndex As Long)
'-> Constantes pour GetSystemMetrics
Public Const SM_CXEDGE& = 45 'Largeur d'une bordure double

'-> API pour GDI
Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Public Type POINTAPI
    x As Long
    y As Long
End Type

Public Type FILETIME
    dwLowDateTime As Long
    dwHighDateTime As Long
End Type

'-> API de lecture des fichiers au format "*.ini"
Public Declare Function GetPrivateProfileSection& Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)
Public Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal HKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long
Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal HKey As Long) As Long
Public Declare Function RegQueryInfoKey Lib "advapi32.dll" Alias "RegQueryInfoKeyA" (ByVal HKey As Long, ByVal lpClass As String, lpcbClass As Long, ByVal lpReserved As Long, lpcSubKeys As Long, lpcbMaxSubKeyLen As Long, lpcbMaxClassLen As Long, lpcValues As Long, lpcbMaxValueNameLen As Long, lpcbMaxValueLen As Long, lpcbSecurityDescriptor As Long, lpftLastWriteTime As FILETIME) As Long
Public Declare Function RegEnumKeyEx& Lib "advapi32.dll" Alias "RegEnumKeyExA" (ByVal HKey As Long, ByVal dwIndex As Long, ByVal lpName As String, lpcbName As Long, ByVal lpReserved As Long, ByVal lpClass As String, lpcbClass As Long, lpftLastWriteTime As FILETIME)
Public Declare Function RegCreateKey Lib "advapi32.dll" Alias "RegCreateKeyA" (ByVal HKey As Long, ByVal lpSubKey As String, phkResult As Long) As Long
Public Declare Function RegOpenKeyEx& Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal HKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long)
Public Declare Function RegSetValueEx& Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal HKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, lpData As Any, ByVal cbData As Long)
Public Declare Function RegDeleteKey Lib "advapi32.dll" Alias "RegDeleteKeyA" (ByVal HKey As Long, ByVal lpSubKey As String) As Long


'-> API pour gestion d'une fen�tre
Public Declare Function GetClientRect& Lib "user32" (ByVal hwnd As Long, lpRect As RECT)
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function GetWindowRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long
Public Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)
Public Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long) As Long
Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Public Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Public Declare Function IsWindow& Lib "user32" (ByVal hwnd As Long)

'-> API pour gestion des regions
Public Declare Function CreateRectRgn& Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long)
Public Declare Function PtInRegion& Lib "gdi32" (ByVal hRgn As Long, ByVal x As Long, ByVal y As Long)
Public Declare Function InvertRgn& Lib "gdi32" (ByVal hdc As Long, ByVal hRgn As Long)
Public Declare Function OffsetRgn& Lib "gdi32" (ByVal hRgn As Long, ByVal x As Long, ByVal y As Long)
Public Declare Function SelectClipRgn& Lib "gdi32" (ByVal hdc As Long, ByVal hRgn As Long)
Public Declare Function GetRgnBox& Lib "gdi32" (ByVal hRgn As Long, lpRect As RECT)

'-> Api pour Dessin
Public Declare Function CreateSolidBrush& Lib "gdi32" (ByVal crColor As Long)
Public Declare Function CreatePen& Lib "gdi32" (ByVal nPenStyle As Long, ByVal nWidth As Long, ByVal crColor As Long)
Public Declare Function DeleteObject& Lib "gdi32" (ByVal hObject As Long)
Public Declare Function GetStockObject& Lib "gdi32" (ByVal nIndex As Long)
Public Declare Function SelectObject& Lib "gdi32" (ByVal hdc As Long, ByVal hObject As Long)
Public Declare Function Rectangle& Lib "gdi32" (ByVal hdc As Long, ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long)
Public Declare Function LineTo& Lib "gdi32" (ByVal hdc As Long, ByVal x As Long, ByVal y As Long)
Public Declare Function MoveToEx& Lib "gdi32" (ByVal hdc As Long, ByVal x As Long, ByVal y As Long, lpPoint As POINTAPI)
Public Declare Function DrawText& Lib "user32" Alias "DrawTextA" (ByVal hdc As Long, ByVal lpStr As String, ByVal nCount As Long, lpRect As RECT, ByVal wFormat As Long)
Public Declare Function FrameRect& Lib "user32" (ByVal hdc As Long, lpRect As RECT, ByVal hBrush As Long)
Public Declare Function ScrollDC& Lib "user32" (ByVal hdc As Long, ByVal dx As Long, ByVal dy As Long, lprcScroll As RECT, lprcClip As RECT, ByVal hrgnUpdate As Long, lprcUpdate As RECT)

'-> Api gestion des fichiers tempo
Public Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal LpBuffer As String) As Long
Public Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long

'-> Api param�trage system
Public Const LOCALE_SYSTEM_DEFAULT& = &H800
Public Declare Function GetLocaleInfo& Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long)
Public Declare Function SetLocaleInfo& Lib "kernel32" Alias "SetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String)
Public Declare Function GetSystemDirectory& Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal LpBuffer As String, ByVal nSize As Long)

'-> Api pour gestion des fichiers
Public Declare Function CloseHandle& Lib "kernel32" (ByVal hObject As Long)
Public Declare Function OpenFile& Lib "kernel32" (ByVal lpFileName As String, lpReOpenBuff As OFSTRUCT, ByVal wStyle As Long)
Public Declare Function GetFileSize& Lib "kernel32" (ByVal hFile As Long, lpFileSizeHigh As Long)
Public Declare Function CopyFile& Lib "kernel32" Alias "CopyFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Long)
Public Declare Function CreateFile& Lib "kernel32" Alias "CreateFileA" (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, lpSecurityAttributes As SECURITY_ATTRIBUTES, ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long)
Public Declare Function DeleteFile& Lib "kernel32" Alias "DeleteFileA" (ByVal lpFileName As String)

'-> API pour gestion des fonts
Public Declare Function AddFontResource& Lib "gdi32" Alias "AddFontResourceA" (ByVal lpFileName As String)

'-> Api pour d�tection des disques
Public Declare Function GetLogicalDriveStrings& Lib "kernel32" Alias "GetLogicalDriveStringsA" (ByVal nBufferLength As Long, ByVal LpBuffer As String)
Public Declare Function GetDriveType& Lib "kernel32" Alias "GetDriveTypeA" (ByVal nDrive As String)

'-> Constantes de type de disque
Public Const DRIVE_CDROM& = 5
Public Const DRIVE_FIXED& = 3
Public Const DRIVE_REMOTE& = 4
Public Const DRIVE_REMOVABLE& = 2


'-> Pour objets GDI
Public Const GRAY_BRUSH& = 2
Public Const PS_SOLID& = 0
Public Const PS_NULL& = 5

'-> Hwnd du bureau
Public Const HWND_DESKTOP& = 0

'-> Constantes pour modification du style des fen�tres
Public Const SWP_DRAWFRAME = &H20
Public Const SWP_NOMOVE = &H2
Public Const SWP_NOSIZE = &H1
Public Const SWP_NOZORDER = &H4
Public Const SWP_FLAGS = SWP_NOZORDER Or SWP_NOSIZE Or SWP_NOMOVE Or SWP_DRAWFRAME
Public Const GWL_STYLE = (-16)
Public Const WS_THICKFRAME = &H40000
Public Const WS_EX_TRANSPARENT& = &H20&
Public Const GWL_EXSTYLE& = (-20)

'-> Constantes pour Message de Windows
Public Const WM_PAINT& = &HF
Public Const WM_CLOSE& = &H10

'-> Constantes pour DrawText
Public Const DT_SINGLELINE& = &H20
Public Const DT_TOP& = &H0
Public Const DT_VCENTER& = &H4
Public Const DT_BOTTOM& = &H8
Public Const DT_LEFT& = &H0
Public Const DT_CENTER& = &H1
Public Const DT_RIGHT& = &H2
Public Const DT_WORDBREAK& = &H10
Public Const DT_CALCRECT& = &H400


'-> Pour gestion des fichiers
Public Const OFS_MAXPATHNAME& = 128
Public Const OF_EXIST& = &H4000
Public Const OF_READ& = &H0
Public Const FILE_SHARE_READ& = &H1
Public Const FILE_SHARE_WRITE& = &H2
Public Const CREATE_ALWAYS& = 2
Public Const CREATE_NEW& = 1
Public Const FILE_ATTRIBUTE_ARCHIVE& = &H20
Public Const FILE_ATTRIBUTE_COMPRESSED& = &H800
Public Const FILE_ATTRIBUTE_DIRECTORY& = &H10
Public Const FILE_ATTRIBUTE_HIDDEN& = &H2
Public Const FILE_ATTRIBUTE_NORMAL& = &H80
Public Const FILE_ATTRIBUTE_READONLY& = &H1
Public Const FILE_ATTRIBUTE_SYSTEM& = &H4
Public Const FILE_ATTRIBUTE_TEMPORARY& = &H100
Public Const GENERIC_WRITE& = &H40000000
Public Const GENERIC_READ& = &H80000000
Public Const GENERIC_ALL& = &H10000000
Public Const GENERIC_EXECUTE& = &H20000000


Public Type OFSTRUCT
    cBytes As Byte
    fFixedDisk As Byte
    nErrCode As Integer
    Reserved1 As Integer
    Reserved2 As Integer
    szPathName(OFS_MAXPATHNAME) As Byte
End Type

'-> Constantes pour acc�s au registre
Public Const HKEY_CURRENT_USER = &H80000001

