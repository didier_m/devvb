Attribute VB_Name = "Messprog"
Public Libelles As New Collection

'***************************************************************
'*                                                             *
'* Ce module contient les variables utils�es pour l'affichage  *
'* des diff�rents libell�s                                     *
'*                                                             *
'***************************************************************


Public Function LoadMessprog(ByVal FichierMessprog As String, Optional StopMess As Boolean) As Boolean

    '---> Fonction charg�e de loader le fichier des messprog et d'alimenter les variables
    
    'Le fichier Messprog doit se trouver dans le r�pertoire : _
     App.path + ..\Mqt\Messprog-[Code Langue].Lng
    
    Dim lpbuffer As String 'matrice des sections
    Dim SectionName As String 'nomd 'une section
    Dim nEntries As Integer 'nombre de sections
    
    Dim i As Integer, j As Integer
    
    Dim lpKey As String 'matrice des cles d'une section
    Dim nKey As Integer 'nombre de cle
    
    Dim aLb As Libelle
    
    On Error GoTo LoadSectionErr
        
    '-> r�cup�ration de la liste des diff�rentes sections
    lpbuffer = Space$(32765)
    Res = GetPrivateProfileString(vbNullString, "", "", lpbuffer, Len(lpbuffer), FichierMessprog)
    If Res = 0 Then GoTo LoadSectionErr
    
    lpbuffer = Mid$(lpbuffer, 1, Res)
    
    If isUTF8(lpbuffer) Then lpbuffer = Decode_UTF8(lpbuffer)
    
    '-> Charger le tableau des sections
    nEntries = NumEntries(lpbuffer, Chr(0))
    For i = 1 To nEntries
        '-> R�cup�rer le nom de la section
        SectionName = Entry(i, lpbuffer, Chr(0))
        '-> Pb WIN 95 : Gestion des blancs
        If Trim(SectionName) = "" Then Exit For
        '-> Chercher les diff�rentes cl�s
        lpKey = Space$(32765)
        Res = GetPrivateProfileSection(SectionName, lpKey, Len(lpKey), FichierMessprog)
        
        If Res <> 0 Then lpKey = Mid$(lpKey, 1, Res)
        '-> Cr�er les objets libell�s
        If lpKey = "Null" Then
        Else
            '-> Initalisation de l'objet
            Set aLb = New Libelle
            
            '-> Ajout dans la collection des libelle
            Libelles.Add aLb, UCase$(SectionName)
            If isUTF8(lpKey) Then lpKey = Decode_UTF8(lpKey)
            
            '-> Cr�ation des libell�s
            aLb.SetKeys lpKey
            
            '-> Liberrer le pointeur
            Set aLb = Nothing
            
        End If
            
    Next 'Pour toute les sections
        
    LoadMessprog = True
    Exit Function
    
    
    
LoadSectionErr:
    
        If StopMess Then
        Else
            MsgBox "Erreur durant l'analyse des sections", vbCritical + vbOKOnly, "Erreur LoadMessprog"
        End If
        LoadMessprog = False
        Exit Function
        

End Function

Public Function CreateMessProg(ByVal FileToLoad As String, Optional StopMess As Boolean) As Boolean

    Dim NomFic As String
    Dim Res As Boolean
    Dim TempFileName As String
    
    On Error GoTo gestError
    
    '-> V�rifier si on trouve le fichier langue dans le r�pertoire du client
    NomFic = ClientPath & FileToLoad
    If Trim(Dir(NomFic)) = "" Then
        '-> Ligne de reprise
Reprise:
        '-> On n'a pas trouv� le fichier langue du client. Chercher le standard
        NomFic = RessourcePath & FileToLoad
    
        If Trim(Dir$(NomFic, vbNormal)) = "" Then
            MsgBox "Impossible de trouver le fichier langue sp�cifi�.", vbCritical + vbOKOnly, "Erreur fichier"
            GoTo gestError
        End If
    End If
    
    '-> PB WIN95 : L'utilisation des API des fichiers ini pose des pb pour _
    les r�pertoires qui sont en lecture seule; En effet , lorsqu'il lit _
    un fichier INI, WIN95 lock le fichier en �crivant dedans : donc -> Violation _
    des privil�ges
    
    'TempFileName = GetTempFileNameVB("LNG")
    
    '-> Effectuer une copie du fichier ini dans le r�pertoire tempo
    'Res = CopyFile(NomFic, TempFileName, 0)
    
    TempFileName = NomFic
    
    '-> Lancer le load du fichier langue
    If StopMess Then
        Res = LoadMessprog(TempFileName, True)
    Else
        Res = LoadMessprog(TempFileName)
    End If
    If Not Res Then GoTo gestError
        
    '-> Supprimer le fichier langue tempo
    'If Dir$(TempFileName) <> "" Then Kill TempFileName
    
    CreateMessProg = True
    
    
    
    Exit Function
    
gestError:
    
        If Err.Number = 52 Then
            ClientPath = ""
            GoTo Reprise
        End If
        
        If StopMess Then
        Else
            MsgBox "Erreur dans proc�dure du Load des Messprog", vbCritical + vbOKOnly, "Erreur fatale"
        End If
        '-> Supprimer le fichier le temporaire
        'If TempFileName <> "" And Dir$(TempFileName, vbNormal) <> "" Then Kill TempFileName
        CreateMessProg = False

End Function


