VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "mscomctl.ocx"
Begin VB.Form AproposDe 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "A propos de Deal Informatique ..."
   ClientHeight    =   6075
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5055
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6075
   ScaleWidth      =   5055
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3840
      Top             =   2040
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "AproposDe.frx":0000
            Key             =   "SELECTION"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "AproposDe.frx":0EDA
            Key             =   "PRINT"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "AproposDe.frx":3D6C
            Key             =   "MAIL"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "AproposDe.frx":5A46
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Picture3 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   1500
      Left            =   840
      Picture         =   "AproposDe.frx":6320
      ScaleHeight     =   100
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   100
      TabIndex        =   13
      Top             =   240
      Width           =   1500
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Informatique"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   840
      TabIndex        =   0
      Top             =   1680
      Width           =   3975
   End
   Begin VB.Image Image4 
      Height          =   810
      Left            =   3840
      Picture         =   "AproposDe.frx":D892
      Top             =   5160
      Width           =   840
   End
   Begin VB.Image Image3 
      Height          =   600
      Left            =   3120
      Picture         =   "AproposDe.frx":FC44
      Top             =   5400
      Width           =   600
   End
   Begin VB.Image Image2 
      Height          =   600
      Left            =   2400
      Picture         =   "AproposDe.frx":106C6
      Top             =   5400
      Width           =   600
   End
   Begin VB.Image Image1 
      Height          =   600
      Left            =   1680
      Picture         =   "AproposDe.frx":11148
      Top             =   5400
      Width           =   600
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Label2"
      Height          =   615
      Left            =   840
      TabIndex        =   1
      Top             =   2205
      Width           =   3975
   End
   Begin VB.Label Label14 
      BackStyle       =   0  'Transparent
      Caption         =   "https://dealinformatique.sharepoint.com/:f:/s/ExternePublic/Ep4tIIEqpShInRWnVjGi9loBa9263WeCKXaKe90nhBixUQ?e=9FV7uW"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   840
      MouseIcon       =   "AproposDe.frx":1244A
      MousePointer    =   99  'Custom
      TabIndex        =   12
      Top             =   4680
      Width           =   2535
   End
   Begin VB.Label Label13 
      BackColor       =   &H00FFFFFF&
      Caption         =   "TurboGraph : Version 2.1.0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   840
      TabIndex        =   11
      Top             =   5040
      Width           =   4095
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Height          =   255
      Left            =   480
      TabIndex        =   10
      Top             =   3240
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "Maintenance : 05.56.75.15.45"
      Height          =   255
      Left            =   3000
      TabIndex        =   9
      Top             =   1920
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "Commercial : 05.56.75.12.28"
      Height          =   255
      Left            =   3000
      TabIndex        =   8
      Top             =   1680
      Visible         =   0   'False
      Width           =   3135
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "Maintenance : +33 (0) 547 504 001"
      Height          =   255
      Left            =   840
      TabIndex        =   7
      Top             =   3240
      Width           =   3375
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Standard : +33 (0) 547 504 000"
      Height          =   255
      Left            =   840
      TabIndex        =   6
      Top             =   3000
      Width           =   3135
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "maintenance@deal.fr"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   840
      MouseIcon       =   "AproposDe.frx":1259C
      MousePointer    =   99  'Custom
      TabIndex        =   5
      Top             =   3840
      Width           =   2775
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "commercial@deal.fr"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   840
      MouseIcon       =   "AproposDe.frx":126EE
      MousePointer    =   99  'Custom
      TabIndex        =   4
      Top             =   3600
      Width           =   2535
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "http://www.deal.fr"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   840
      MouseIcon       =   "AproposDe.frx":12840
      MousePointer    =   99  'Custom
      TabIndex        =   3
      Top             =   4440
      Width           =   2535
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Visitez nos sites : "
      Height          =   255
      Left            =   840
      TabIndex        =   2
      Top             =   4200
      Width           =   1335
   End
End
Attribute VB_Name = "AproposDe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
frmConfig.Show vbModal
End Sub

Private Sub Form_Load()

On Error GoTo GestError

Me.Label2.Caption = "27, Avenue de la Poterie" & Chr(13) & _
                    "B.P. 100" & Chr(13) & _
                    "33173 GRADIGNAN CEDEX"
Me.Label13.Caption = "Turbograph Version : [" & App.Major & "." & App.Minor & "." & App.Revision & "]"

If VersionTurbo = 0 Then
    Me.Label13.Caption = Me.Label13.Caption & " (E) "
Else
    Command1.Visible = True
End If

'-> on charge l'image de tool
Command1.Picture = AproposDe.ImageList1.ListImages(1).Picture

GestError:
End Sub


Private Sub Image1_Click()
Dim http As Long
http = ShellExecute(Hwnd, "Open", "https://twitter.com/DEALFR", vbNullString, App.Path, 1)

End Sub

Private Sub Image2_Click()
Dim http As Long
http = ShellExecute(Hwnd, "Open", "https://www.facebook.com/dealinformatique", vbNullString, App.Path, 1)

End Sub

Private Sub Image3_Click()
Dim http As Long
http = ShellExecute(Hwnd, "Open", "https://www.linkedin.com/company/deal-informatique", vbNullString, App.Path, 1)

End Sub

Private Sub Image4_Click()
Dim http As Long
http = ShellExecute(Hwnd, "Open", "http://www.key-for-it.com/exposant-hall-solutions-metiers/deal-informatique-196.html", vbNullString, App.Path, 1)

End Sub

Private Sub Label14_Click()
Dim http As Long
http = ShellExecute(Hwnd, "Open", Me.Label14.Caption, vbNullString, App.Path, 1)

End Sub

Private Sub Label4_Click()

Call ShellExecute(0&, vbNullString, "mailto: " & Label4.Caption, vbNullString, vbNullString, vbNormalFocus)

End Sub

Private Sub Label6_Click()

Dim http As Long
http = ShellExecute(Hwnd, "Open", Me.Label6.Caption, vbNullString, App.Path, 1)

End Sub

Private Sub Label7_Click()

Call ShellExecute(0&, vbNullString, "mailto: " & Label7.Caption, vbNullString, vbNullString, vbNormalFocus)

End Sub

Private Sub Label8_Click()

Call ShellExecute(0&, vbNullString, "mailto: " & Label8.Caption, vbNullString, vbNullString, vbNormalFocus)

End Sub

Private Sub Picture3_Click()

Close hdlMouchard
frmEditor.RichTextBox1.LoadFile Mouchard
hdlMouchard = 0
Exit Sub
If Not IsMouchard Then
    IsMouchard = True
    Mouchard = GetTempFileNameVB("DBG")
    hdlMouchard = FreeFile
    Open Mouchard For Output As #hdlMouchard
    MsgBox "Le fichier mouchard " & Mouchard & " est actif"
End If
End Sub

Private Sub Label13_DblClick()
Dim strInfo As String
strInfo = strInfo & "Chemin de l'application :" & vbLf
strInfo = strInfo & App.Path & vbLf
strInfo = strInfo & "Chemin des fichiers ini :" & vbLf
strInfo = strInfo & V6TurboIniPath & vbLf
strInfo = strInfo & "Fichier turbograph.ini :" & vbLf
strInfo = strInfo & TurboGraphIniFile & vbLf
strInfo = strInfo & "Fichier pour les images :" & vbLf
strInfo = strInfo & Tm_PictureIniFile & vbLf
strInfo = strInfo & "Chemin pour les maquettes :" & vbLf
strInfo = strInfo & V6MqtPath & vbLf
strInfo = strInfo & "Fichier pour les maquettes :" & vbLf
strInfo = strInfo & TurboMaqIniFile & vbLf
strInfo = strInfo & "Argument de commande :" & vbLf
strInfo = strInfo & Command$ & vbLf
strInfo = strInfo & "Liste des imprimantes :" & vbLf
For Each aPrinter In Printers
    strInfo = strInfo & aPrinter.DeviceName & ","
Next
strInfo = strInfo & vbLf
MsgBox strInfo
End Sub
