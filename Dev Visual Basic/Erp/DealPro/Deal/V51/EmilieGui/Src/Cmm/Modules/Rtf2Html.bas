Attribute VB_Name = "Rtf2Html"
Option Explicit

'-> Message pour �diteur RTF
Private Const EM_GETLINECOUNT& = &HBA
Private Const EM_LINEFROMCHAR& = &HC9
Private Const EM_GETLINE& = &HC4

'-> Structure de comparaison RTF
Private Type lpRtf
    FontName As String
    FontSize As String
    FontColor As Long
    FontBold As Boolean
    FontItalic As Boolean
    FontUnderline As Boolean
End Type

'-> Handle d'impression du fichier HTML
Dim hdlFile As Integer

'-> Liste des images d�ja utilis�es pour une impression
Public ListeHTMLImages() As String
Public NbImages As Integer

'-> Indique le nom de la palette de naviagtion avec menu BAR de deal
Public Const NavigaBarHtmlFile = "nav_std$CHRONO$.html"
Public Const NavigaSpoolHtml = "nav_spool$CHRONO$.html"

'-> Decallage pour le dessin de la feuille HTML
Const Decalage = 10

'-> Indique si les images de navigation sont pr�sentes dans le cas d'une page unique
Dim IsBackImg As Boolean
Const BackImg = "nav_precedent1.gif"
Dim IsNextImg As Boolean
Const NextImg = "nav_suivant1.gif"
Dim IsPageImg As Boolean
Const PageImg = "nav_spool.gif"

'-> Indique le d�but de la postion de la page en pixels dans le cas d'une impression mono fichier HTML
Dim PositionDebutPage As Long

'-> Variables pour la cr�ation d'une �dition au format HTML
Public OrigineExport As Integer 'Indique la provenance de la demande d'export
'-> Valeurs : 0 -> Export HTML depuis la page de visu _
              1 -> Envoie de mail vers INTERNET depuis visu _
              2 -> Envoie Vers OUTLLOK depuis visu _
              3 -> Export HTML depuis ligne commande _
              4 -> Envoie de mail vers INTERNET depuis ligne commande _
              5 -> Envoie de mail Vers OUTLLOK depuis ligne commande
Public ListeHtmlFile As String 'Indique la liste des fichiers cr��s lors d'une edition au format HTML
Public PathToExport As String 'Path d'export des documents HTML
Public FileToExport As String 'Nom du fichier � cr�er pour l'export HTML
Private PageSpoolToExport As String 'Nom des pages du fichier � cr�er
Public RunAfterExport As Boolean 'Indique si on doit lancer le fichier apr�s export des donn�es
Public IncluseSpoolNumber As Boolean 'Indique si on doit inclure le num�ro des spools dans le nom des pages
Public ExportType As Integer '0 = Page en cours , 1 = Spool en cours , 2 = Fichier en entier
Public NavigationType As Integer '0 = FrametSet de navigation , 1 = Fichier Unique , 2 = Simples Pages
Public SpoolKeyExport As String '-> cl� du spool pour ExportType = 1
Public FileKeyExport As String '-> Cl� du fichier qui contient le spool
Public PageToExport As Integer '-> Num�ro de la page � exporter
Public IsTempo As Boolean '-> Si dessin de temporisation
Public FileToLaunch As String '-> Nom du fichier HTML � ex�cuter
Public ExportBordure As Boolean '-> Quand on exporte une page HTML
Private signature As String

Private Function ConvertColor(Color As Long) As String

'---> Cette fonction convertit une couleur exprim�e en DEC au format HTML

Dim Red As Long
Dim Green As Long
Dim Blue As Long
Dim Couleur As String

'-> formatter la couleur � traduire
Couleur = FixLen(Hex$(Color), "000000")
Red = CInt("&H" & Right$(Couleur, 2))
Green = CInt("&H" & Mid$(Couleur, 3, 2))
Blue = CInt("&H" & Left$(Couleur, 2))

'-> Pour internet, inverser les couleurs
Couleur = Hex$(RevRGB(Red, Green, Blue))

'-> Remplir avec des 0 si necessaire
Do While Len(Couleur) <> 6
    Couleur = "0" & Couleur
Loop
    
ConvertColor = Couleur

End Function
Private Function RevRGB(Red As Long, Green As Long, Blue As Long) As Long
    RevRGB = CLng(Blue + (Green * 256) + (Red * 65536))
End Function

Private Function FixLen(ByVal sIn As String, ByVal sMask As String) As String

    If Len(sIn) < Len(sMask) Then
        FixLen = Left$(sMask, Len(sMask) - Len(sIn)) & sIn
    Else
        FixLen = Right$(sIn, Len(sMask))
    End If
    
End Function


Public Function Rtf2Html(ByRef aRtf As RichTextBox, aRect As RECT, BordureStyle As String, PrintBackColor As Boolean) As String

'---> Cette proc�dure convertit du text RTF en text HTML
Dim NbChar As Long
Dim i As Long, j As Long
Dim RTfRef As lpRtf
Dim RTFCrs As lpRtf
Dim BaseChar As Long
Dim MyText As String
Dim Ligne As String
Dim LenLigne As Long
Dim LigCours As Long
Dim IsNewLigne As Boolean
Dim RowAligne As String
Dim FirstChar As Boolean
Dim strRtf2Html As String


'-> Imprimer les balises d'ouverture
If PrintBackColor Then
    strRtf2Html = "<DIV Style=""" & BordureStyle & "BACKGROUND-COLOR:#" & ConvertColor(aRtf.BackColor) & "; HEIGHT:" & aRect.Bottom & _
                        "px;WIDTH:" & aRect.Right & _
                        "px;LEFT:" & aRect.Left & _
                        "px;TOP:" & aRect.Top & _
                        "px;POSITION:absolute "" >" & Chr(13) & Chr(10)
                        '"CLIP:rect(" & aRect.Top & "px " & aRect.Left + aRect.Right & "px " & aRect.Top + aRect.Bottom & "px " & aRect.Left & "px)"" >" & Chr(13) & Chr(10)
Else
    strRtf2Html = "<DIV Style=""" & BordureStyle & "HEIGHT:" & aRect.Bottom & _
                        "px;WIDTH:" & aRect.Right & _
                        "px;LEFT:" & aRect.Left & _
                        "px;TOP:" & aRect.Top & _
                        "px;POSITION:absolute "" >" & Chr(13) & Chr(10)
                        '"CLIP:rect(" & aRect.Top & "px " & aRect.Left + aRect.Right & "px " & aRect.Top + aRect.Bottom & "px " & aRect.Left & "px)"" >" & Chr(13) & Chr(10)

End If


'-> Quitter si pas de lignes � imprimer
If SendMessage(aRtf.hwnd, EM_GETLINECOUNT, 0, 0) = 0 Then GoTo CloseHTML

'-> R�cup�rer le nombre de caract�re
NbChar = Len(aRtf.Text)
If NbChar = 0 Then GoTo CloseHTML

'-> Indiquer la ligne en cours de lecture
LigCours = 0

'-> Indiquer que l'on lit le premier caract�re de la ligne
FirstChar = True

For i = 0 To NbChar - 1
    '-> R�cup�rer son num�ro de ligne
    If SendMessage(aRtf.hwnd, EM_LINEFROMCHAR, i, 0) <> LigCours Then
        '-> Tester s'il ya quelquechose sur la ligne
        If Trim(Ligne) = "" Then
            '-> Positionner le char dans l'�diteur
            aRtf.SelStart = i
            '-> Rien � imprimer donc saut de ligne
            strRtf2Html = strRtf2Html & "<FONT STYLE=""line-height:100%;font: " & aRtf.SelFontSize & "pt '" & aRtf.SelFontName & "'""><BR></FONT>" & Chr(13) & Chr(10)
        Else
            '-> Imprimer la ligne
            strRtf2Html = strRtf2Html & RowAligne & "" & Ligne & " </FONT></DIV>" & Chr(13) & Chr(10)
        End If
        
        '-> Indiquer que l'on lit le premier caract�re de la ligne
        FirstChar = True
        '-> Remettre la variable de chaine � blanc
        Ligne = ""
        '-> Mettre � jour la variable de ligne
        LigCours = LigCours + 1
    End If 'Si on est sur la m�me ligne
    
    '-> Ne pas traiter chr(13) et chr(10)
    If Asc(Mid$(aRtf.Text, i + 1, 1)) = 13 Or Asc(Mid$(aRtf.Text, i + 1, 1)) = 10 Or Asc(Mid$(aRtf.Text, i + 1, 1)) = 9 Then GoTo NextChar
    
    '-> S�lectionner le caract�re dans l'�diteur
    aRtf.SelStart = i
    aRtf.SelLength = 1
    
    '-> Initialiser la structure du char en cours
    InitlpRTf RTFCrs, aRtf
        
    '-> Si on est sur le premier caract�re
    If FirstChar Then
        '-> Lire l'alignement de la ligne
        RowAligne = GetAlignFromChar(aRtf)
        '-> Initialiser la structure de r�f�rence
        InitlpRTf RTfRef, aRtf
        '-> Construire la chaine � imprimer
        Ligne = GetFontFromlpRtf(RTfRef)
        '-> Indiquer que ce n'est plus le premier caract�re
        FirstChar = False
    Else
    
        '-> Faire la diff�rence entre les deux structures
        If Not DiflpRTF(RTfRef, RTFCrs) Then
            '-> Fermer la balise de font en cours si necessaire
            If Trim(Ligne) <> "" Then Ligne = Ligne & "</FONT>"
            '-> Cr�er la nouvelle balise pour la nouvelle font
            Ligne = Ligne & GetFontFromlpRtf(RTFCrs)
            '-> Faire du lpRTFcrs le lpRTF de r�f
            InitlpRTf RTfRef, aRtf
        End If
    End If 'Si on est sur le premier caract�re
    
    '-> Ajouter � la ligne � imprimer
    Ligne = Ligne & ConvertChar(Asc(aRtf.SelText))
    
NextChar:
Next 'Pour touts les caract�res de l'�diteur RTF

'-> Tester s'il ya quelquechose sur la ligne
If Trim(Ligne) = "" Then
    '-> Rien � imprimer donc saut de ligne
    strRtf2Html = strRtf2Html & "<BR>" & Chr(13) & Chr(10)
Else
    '-> Imprimer la ligne
    strRtf2Html = strRtf2Html & RowAligne & "" & Ligne & " </FONT></DIV>" & Chr(13) & Chr(10)
End If

CloseHTML:

'-> Imprimer les balises de fermeture
strRtf2Html = strRtf2Html & "</DIV>" & Chr(13) & Chr(10)

'-> Renvoyer la valeur
Rtf2Html = strRtf2Html

End Function
Private Function DiflpRTF(lpRTF1 As lpRtf, lpRTF2 As lpRtf) As Boolean

'---> Cette proc�dure indique s'il y a une diff�rence entre 2 structures de formattage

Dim Retour As Boolean


'-> Initialiser la valeur de retour
Retour = True
'-> Faire la comparaison
If lpRTF1.FontBold <> lpRTF2.FontBold Then Retour = False
If lpRTF1.FontColor <> lpRTF2.FontColor Then Retour = False
If lpRTF1.FontItalic <> lpRTF2.FontItalic Then Retour = False
If lpRTF1.FontName <> lpRTF2.FontName Then Retour = False
If lpRTF1.FontSize <> lpRTF2.FontSize Then Retour = False
If lpRTF1.FontUnderline <> lpRTF2.FontUnderline Then Retour = False


DiflpRTF = Retour

End Function


Private Function GetAlignFromChar(aRtf As RichTextBox) As String

'---> Cette proc�dure retourne la chaine d'alignement d'une ligne

Dim Align As Integer

Align = aRtf.SelAlignment

Select Case Align
    Case 0 '-> Left
        GetAlignFromChar = "<DIV ALIGN=LEFT>"
    Case 1 '-> Right
        GetAlignFromChar = "<DIV ALIGN=RIGHT>"
    Case 2 '-> Center
        GetAlignFromChar = "<DIV ALIGN=CENTER>"
End Select

End Function

Private Function InitlpRTf(lpRtf As lpRtf, aRtf As RichTextBox)

With lpRtf
    .FontBold = aRtf.SelBold
    .FontColor = aRtf.SelColor
    .FontItalic = aRtf.SelItalic
    .FontName = aRtf.SelFontName
    .FontSize = aRtf.SelFontSize
    .FontUnderline = aRtf.SelUnderline
End With

End Function

Private Function GetFontFromlpRtf(lpRtf As lpRtf) As String

'---> Cette focntion retourne une chaine de caract�re comportant le TAG html de ref

Dim Temp As String



Temp = "<FONT STYLE=""line-height:100%;font:"
'-> Bold
If lpRtf.FontBold Then Temp = Temp & " bold"
'-> Italic
If lpRtf.FontItalic Then Temp = Temp & " italic"
'-> Size
Temp = Temp & " " & lpRtf.FontSize & "pt"
'-> Face
Temp = Temp & " '" & lpRtf.FontName & "'"
'-> Underline
If lpRtf.FontUnderline Then Temp = Temp & ";TEXT-DECORATION:underline"
'-> Couleur
Temp = Temp & ";COLOR:" & ConvertColor(lpRtf.FontColor)
'-> fermer la balise
Temp = Temp & """>"

'-> Renvoyer la valeur
GetFontFromlpRtf = Temp

End Function

Public Sub CreatePdfToMessagerie(SpoolName As String, StrPrinter As String)
'-> cette proc�dure g�n�re un fichier pdf utilis� pour les fichiers joints de messagerie
Dim CurrentPIDProcess As Long
Dim i As Integer
Dim FilePath As String

If StrPrinter = "" Then StrPrinter = "TURBOPDF"

If StrPrinter = "TURBOPDF" Then
    '-> on g�n�re le fichier avec le turboPDF
    Trace "CreatePdfToMessagerie: " & App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & SpoolName & "�" & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "pdfDirectory=", "pdfDirectoryNo=", , , vbTextCompare) & "�pdfName=" & Dir(SpoolName) & ".pdf" & Chr(34)
    CurrentPIDProcess = Shell(App.Path & "\TurboPdf.exe " & Chr(34) & "�fileToConvert=" & SpoolName & "�" & Replace(Replace(GetIniString("PDF", "CMD", App.Path & "\Turbograph.ini", False), "�sortie=", "�NoSortie=", , , vbTextCompare), "pdfDirectory=", "pdfDirectoryNo=", , , vbTextCompare) & "�pdfName=" & Dir(SpoolName) & ".pdf" & Chr(34))
Else
    CurrentPIDProcess = Shell(App.Path & "\TurboGraph.exe " & StrPrinter & "~DIRECT~1|" & SpoolName, vbNormalFocus)
End If
    
'-> Boucler tant que le processus est actif
Do While IsPidRunning(CurrentPIDProcess)
    '-> Lib�ration de la pile des messages
    DoEvents
    Sleep 1000
Loop
FileKeyExport = SpoolName
'-> Laisser le temps de terminer
Do While Dir$(SpoolName & ".pdf") = ""
    Sleep 1000
    DoEvents
Loop

End Sub

Public Function IsPidRunning(lngPid As Long) As Boolean

'---> Cette proc�dure v�rifie si un process sp�cifi� ( lngPid ) est encore actif

Dim hdlProcess As Long
Dim ExitCode As Long

'-> Ne rien faire si process � 0
If lngPid = 0 Then Exit Function
'-> R�cup�rer le handle du process affect� � ce pide
hdlProcess = OpenProcess(PROCESS_QUERY_INFORMATION, False, lngPid)
'-> V�rifier si le PID est toujours valide
GetExitCodeProcess hdlProcess, ExitCode
If ExitCode = STILL_ACTIVE Then IsPidRunning = True

End Function

Public Sub CreateHTMLToMessagerie(IsOutLook As Boolean, Optional pParam As String)

'---> Cette prco�dure exporte les s�lections au formats HTML avant de les joindre _
� un message interne

Dim aOutLook As Object
Dim aMail As Object
Dim i As Integer
Dim OldPath As String
Dim hdlBody As Integer
Dim Ligne As String
Dim oSess As Object
Dim oDB As Object
Dim oDoc As Object
Dim oItem As Object
Dim direct As Object
Dim Var As Variant
Dim flag As Boolean
Dim richStyle As Object
Dim AttachME As Object

'-> Faire le setting des variables d'export HTML

'-> pParam pour envoie depuis CDE avec
' TO + chr(0) + COPIES + chr(0) + OBJET + chr(0) + BODY + chr(0) + BODYFILE

'On Error GoTo GestError

'-> Path d'enregistrement dans le r�pertire temporaire
OldPath = PathToExport
PathToExport = GetTempFileNameVB("WWW", True)
'-> Nom du Fichier
FileToExport = LCase$(GetSpoolName(FileKeyMail)) & ".html"
'-> Cl� du fichiers
FileKeyExport = FileKeyMail
'-> Spool
SpoolKeyExport = SpoolKeyMail
'-> Num�ro de page
PageToExport = PageNumMail

'-> Si on est en navigation : inclure le num�ro du spool
If NavigationType = 1 Then IncluseSpoolNumber = True

'-> Origine de l'export
Select Case OrigineMail
    Case 0 '-> OutLook visu
        OrigineExport = 2
    Case 1 '-> Internet Visu
        OrigineExport = 1
    Case 2 '-> OutLook Cde
        OrigineExport = 5
    Case 3 '-> Internet Cde
        OrigineExport = 4
End Select

'-> Type d'export
ExportType = ExportMail

'-> Lancer la cr�ation des pages HTMLS
CreateEditionHTML

'-> Restaurer le path d'export
PathToExport = OldPath

'-> Cr�er un mail et l'envoyer si necessaire
If IsLotus Then
    '-> on initialse la session par defaut
    Set oSess = CreateObject("Notes.NotesSession")
    Set oDB = oSess.GETDATABASE("", "")
    Call oDB.OPENMAIL
    flag = True
    If Not (oDB.IsOpen) Then flag = oDB.Open("", "")
    '-> on verifie si c'est bien ouvert
    If Not flag Then
        MsgBox "Can't open mail file: " & oDB.SERVER & " " & oDB.FilePath
        Exit Sub
    End If
    '-> initialisation du message
    Set oDoc = oDB.CREATEDOCUMENT
    Set oItem = oDoc.CREATERICHTEXTITEM("BODY")
    Set richStyle = oSess.CreateRichTextStyle
    richStyle.PassThruHTML = True
    
    '-> pour les envois en copy
    Dim arrNames() As String
    ReDim arrNames(NumEntries(Replace(Entry(2, pParam, Chr(0)), ";", ","), ",") - 1)
    '-> Afficher le corps du message
    If OrigineMail = 0 Then
        '-> Envoie depuis la page de visu
        If frmMail.RichTextBox1.Text <> "" Then oDoc.body = frmMail.RichTextBox1.Text
        '-> Setting des param�tres
        oDoc.Subject = Entry(3, pParam, Chr(0))
        oDoc.sendto = Entry(1, pParam, Chr(0))
        oDoc.body = Entry(4, pParam, Chr(0))
        '-> Pour lotus le separateur est la virgule
        For i = 0 To NumEntries(Replace(Entry(2, pParam, Chr(0)), ";", ","), ",") - 1
            arrNames(i) = Entry(i + 1, Replace(Entry(2, pParam, Chr(0)), ";", ","), ",")
        Next
        oDoc.copyto = arrNames
    Else
        '-> Setting des param�tres
        oDoc.sendto = Entry(1, pParam, Chr(0))
        '-> Pour lotus le separateur est la virgule
        For i = 0 To NumEntries(Replace(Entry(2, pParam, Chr(0)), ";", ","), ",") - 1
            arrNames(i) = Entry(i + 1, Replace(Entry(2, pParam, Chr(0)), ";", ","), ",")
        Next
        oDoc.copyto = arrNames
        oDoc.Subject = Entry(3, pParam, Chr(0))
        '-> Setting du body
        If Trim(Entry(4, pParam, Chr(0))) = "" Then
            '-> V�rifier si un fichier pour body est donn�
            If Trim(Entry(5, pParam, Chr(0))) <> "" Then
                If Dir$(Trim(Entry(5, pParam, Chr(0))), vbNormal) <> "" Then
                    '-> Ouvrir le fichier body
                    hdlBody = FreeFile
                    Open Trim(Entry(5, pParam, Chr(0))) For Input As #hdlBody
                    Do While Not EOF(hdlBody)
                        '-> Lecture de la ligne
                        Line Input #hdlBody, Ligne
                        '-> Ajout dans le body du text
                        If oDoc.body <> "" Then
                            oDoc.body = oDoc.body & Chr(13) + Chr(10) & Ligne
                        Else
                            oDoc.body = Ligne
                        End If
                    Loop
                    '-> Fermer le fichier
                    Close #hdlBody
                End If 'Si fichier body existe
            End If 'Si fichier body sp�cifi�
        Else
            oDoc.body = Entry(4, pParam, Chr(0))
        End If 'Si pas de body sp�cifi�
    End If 'Selon l'origine du mail
    
    '-> Attacher le fichier sp�cifi�
    If ListeHtmlFile <> "" Then
        Set AttachME = oDoc.CREATERICHTEXTITEM("Attachment")
        For i = 1 To NumEntries(ListeHtmlFile, "|")
            AttachME.EMBEDOBJECT 1454, "", Entry(i, ListeHtmlFile, "|"), "Attachment"
        Next
        'oDoc.CREATERICHTEXTITEM ("Attachment")
    End If
    
    '-> Ajouter les images s'il y en a
    For i = 1 To NbImages
        AttachME.EMBEDOBJECT 1454, "", ListeHTMLImages(i), "Attachment"
    Next
    
    '-> si il y a des fichiers joints lorsque l on vient d'un fichier zipp�
    For i = 0 To MDIMain.mnuJoin.Count - 1
        If MDIMain.mnuJoin.Item(i).Visible Then
            If Trim(MDIMain.mnuJoin.Item(i).Tag) <> "" Then aMail.Attachments.add MDIMain.mnuJoin.Item(i).Tag
        End If
    Next
    
    '-> mettre le mail dans la boite d'envoi
    oDoc.PostedDate = Now()
    oDoc.SAVEMESSAGEONSEND = True
    '-> Afficher le mail
    If SendOutLook Then
        oDoc.Send False
    Else
        oDoc.visable = True
        oDoc.Send False
    End If
    
    '-> Lib�rer les pointeurs
    Set oSess = Nothing
    Set oDB = Nothing
    Set oDoc = Nothing
    Set oItem = Nothing
    Exit Sub
End If

If IsOutLook Then

    '-> Obtenir un pointeur vers OutLook
    Set aOutLook = StartOutlook()
    
    '-> Cr�er un nouveau message
    Set aMail = aOutLook.createitem(0)
    
    '-> Afficher le corps du message
    If OrigineMail = 0 Then
        '-> Envoie depuis la page de visu
        If frmMail.RichTextBox1.Text <> "" Then aMail.body = frmMail.RichTextBox1.Text
        '-> Setting des param�tres
        aMail.To = Entry(1, pParam, Chr(0))
        aMail.CC = Entry(2, pParam, Chr(0))
        aMail.Subject = Entry(3, pParam, Chr(0))
        aMail.body = Entry(4, pParam, Chr(0))
    Else
        '-> Setting des param�tres
        aMail.To = Entry(1, pParam, Chr(0))
        aMail.CC = Entry(2, pParam, Chr(0))
        aMail.Subject = Entry(3, pParam, Chr(0))
        '-> Setting du body
        If Trim(Entry(4, pParam, Chr(0))) = "" Then
            '-> V�rifier si un fichier pour body est donn�
            If Trim(Entry(5, pParam, Chr(0))) <> "" Then
                If Dir$(Trim(Entry(5, pParam, Chr(0))), vbNormal) <> "" Then
                    '-> Ouvrir le fichier body
                    hdlBody = FreeFile
                    Open Trim(Entry(5, pParam, Chr(0))) For Input As #hdlBody
                    Do While Not EOF(hdlBody)
                        '-> Lecture de la ligne
                        Line Input #hdlBody, Ligne
                        '-> Ajout dans le body du text
                        If aMail.body <> "" Then
                            aMail.body = aMail.body & Chr(13) + Chr(10) & Ligne
                        Else
                            aMail.body = Ligne
                        End If
                    Loop
                    '-> Fermer le fichier
                    Close #hdlBody
                End If 'Si fichier body existe
            End If 'Si fichier body sp�cifi�
        Else
            aMail.body = Entry(4, pParam, Chr(0))
        End If 'Si pas de body sp�cifi�
    End If 'Selon l'origine du mail
    
    '-> Attacher le fichier sp�cifi�
    If ListeHtmlFile <> "" Then
        For i = 1 To NumEntries(ListeHtmlFile, "|")
            aMail.Attachments.add Entry(i, ListeHtmlFile, "|")
        Next
    End If
    
    '-> Ajouter les images s'il y en a
    For i = 1 To NbImages
        aMail.Attachments.add ListeHTMLImages(i)
    Next
    
    '-> si il y a des fichiers joints lorsque l on vient d'un fichier zipp�
    For i = 0 To MDIMain.mnuJoin.Count - 1
        If MDIMain.mnuJoin.Item(i).Visible Then
            If Trim(MDIMain.mnuJoin.Item(i).Tag) <> "" Then aMail.Attachments.add MDIMain.mnuJoin.Item(i).Tag
        End If
    Next
    
    '-> Afficher le mail
    If SendOutLook Then
        aMail.Send
    Else
        aMail.display
    End If
    
    '-> Lib�rer les pointeurs
    Set aMail = Nothing
    Set aOutLook = Nothing
Else
    '-> Intialiser la session MAPI
    If Not InitMAPISession Then
        '-> Afficher un message si on est en visu
        If OrigineMail = 1 Then
            MsgBox "Impossible d'initialiser la session Internet", vbCritical + vbOKOnly, "Erreur"
            Exit Sub
        End If
    End If
                
    '-> Cr�er un nouveau message
    With frmLib.MAPIMessages1
        If .RecipAddress = "" Then
            .MsgIndex = -1
            .AddressEditFieldCount = 2
            .Compose
            .RecipAddress = Entry(1, pParam, Chr(0))
            .ResolveName
        Else
            .ResolveName
        End If
    End With
    
    '-> Corp du message
    If Trim(Entry(4, pParam, Chr(0))) = "" Then
        '-> V�rifier si un fichier BODY file existe
        If Trim(Entry(5, pParam, Chr(0))) <> "" Then
            '-> V�rifier que le fichier existe
            If Dir$(Trim(Entry(5, pParam, Chr(0))), vbNormal) <> "" Then
                '-> Ouverture du fichier
                hdlBody = FreeFile
                Open Trim(Entry(5, pParam, Chr(0))) For Input As #hdlBody
                Do While Not EOF(hdlBody)
                    Line Input #hdlBody, Ligne
                    If frmLib.MAPIMessages1.MsgNoteText = "" Then
                        frmLib.MAPIMessages1.MsgNoteText = Ligne
                    Else
                        frmLib.MAPIMessages1.MsgNoteText = frmLib.MAPIMessages1.MsgNoteText & Chr(13) + Chr(10) & Ligne
                    End If
                Loop
                '-> Fermer le fichier
                Close #hdlBody
            End If 'Si fichier bodyfile existe
        End If 'Si fichier bodyfile sp�cifi�
    Else
        '-> Setting du body
        frmLib.MAPIMessages1.MsgNoteText = Entry(4, pParam, Chr(0))
    End If
       
    '-> Sujet du message
    frmLib.MAPIMessages1.MsgSubject = Entry(3, pParam, Chr(0))
        
    '-> on laisse la place pour inserer les pieces jointes
    frmLib.MAPIMessages1.MsgNoteText = Space(20) & vbCrLf & frmLib.MAPIMessages1.MsgNoteText
        
    '-> Liste des fichiers joints
    If ListeHtmlFile <> "" Then
        For i = 1 To NumEntries(ListeHtmlFile, "|")
            With frmLib.MAPIMessages1
                .AttachmentIndex = frmLib.MAPIMessages1.AttachmentCount
                .AttachmentPosition = frmLib.MAPIMessages1.AttachmentCount
                .AttachmentPathName = Entry(i, ListeHtmlFile, "|")
                .AttachmentType = 0
            End With
        Next
    End If
    
    '-> Ajouter les images s'il y en a
    For i = 1 To NbImages
        With frmLib.MAPIMessages1
            .AttachmentIndex = frmLib.MAPIMessages1.AttachmentCount
            .AttachmentPathName = ListeHTMLImages(i)
        End With
    Next

'    '-> D�terminer la position des pi�ces jointes
'    If frmLib.MAPIMessages1.MsgNoteText <> "" Then
'        frmLib.MAPIMessages1.AttachmentPosition = Len(frmLib.MAPIMessages1.MsgNoteText) - 1
'    Else
'        frmLib.MAPIMessages1.AttachmentPosition = 0
'    End If
        
    '-> Envoyer le mail
    frmLib.MAPIMessages1.Send False
    
End If 'Si envoie sur internet

End Sub

Public Sub CreateHTMLToBody(IsOutLook As Boolean, Optional pParam As String)

'---> Cette prco�dure exporte les s�lections au formats HTML avant de les joindre _
� un message interne

Dim aOutLook As Object
Dim aMail As Object
Dim i As Integer
Dim OldPath As String
Dim hdlBody As Integer
Dim Ligne As String
Dim oSess As Object
Dim oDB As Object
Dim oDoc As Object
Dim oItem As Object
Dim direct As Object
Dim Var As Variant
Dim flag As Boolean
Dim AttachME As Object
Dim Bodyitem As Object
Dim Mime As Object
Dim MimeChild As Object
Dim Header As Object
Dim ENC_NONE As Integer
Dim Stream As Object

'-> Faire le setting des variables d'export HTML

'-> pParam pour envoie depuis CDE avec
' pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile & Chr(0) & pFormatBody & Chr(0) & pFormat

'On Error GoTo GestError

'-> Path d'enregistrement dans le r�pertire temporaire
If Right$(PathToExport, 1) <> "\" Then PathToExport = PathToExport & "\"
OldPath = PathToExport
PathToExport = GetTempFileNameVB("WWW", True)
'-> Nom du Fichier
FileToExport = LCase$(GetSpoolName(FileKeyMail)) & ".html"
'-> Cl� du fichiers
If FileKeyExport = "" Then FileKeyExport = FileKeyMail
'-> Spool
SpoolKeyExport = SpoolKeyMail
'-> Num�ro de page
PageToExport = PageNumMail
'-> Si on est en navigation : inclure le num�ro du spool
NavigationType = 1
If NavigationType = 1 Then IncluseSpoolNumber = True

'-> Origine de l'export
Select Case OrigineMail
    Case 0 '-> OutLook visu
        OrigineExport = 2
    Case 1 '-> Internet Visu
        OrigineExport = 1
    Case 2 '-> OutLook Cde
        OrigineExport = 5
    Case 3 '-> Internet Cde
        OrigineExport = 4
End Select

'-> Type d'export
ExportType = ExportMail

'-> Lancer la cr�ation des pages HTMLS
If Trim(Entry(6, pParam, Chr(0))) = "1" Then
    If IsOutLook Then
        CreateOutlookHTML
    Else
        CreateEditionHTML
    End If
End If
'-> Restaurer le path d'export
PathToExport = OldPath
'-> Cr�er un mail et l'envoyer si necessaire

If IsLotus Then
    '-> Obtenir un pointeur vers Lotus
    '-> on initialse la session par defaut
    Set oSess = CreateObject("Notes.NotesSession")
    Set oDB = oSess.GETDATABASE("", "")
    Call oDB.OPENMAIL
    flag = True
    If Not (oDB.IsOpen) Then flag = oDB.Open("", "")
    '-> on verifie si c'est bien ouvert
    If Not flag Then
        MsgBox "Can't open mail file: " & oDB.SERVER & " " & oDB.FilePath
        Exit Sub
    End If
    '-> initialisation du message
    Set oDoc = oDB.CREATEDOCUMENT
    
    '-> Afficher le corps du message
    '-> Setting des param�tres
    oDoc.sendto = Entry(1, pParam, Chr(0))
    '-> Pour lotus le separateur est la virgule
    Dim arrNames() As String
    ReDim arrNames(NumEntries(Replace(Entry(2, pParam, Chr(0)), ";", ","), ",") - 1)
    For i = 0 To NumEntries(Replace(Entry(2, pParam, Chr(0)), ";", ","), ",") - 1
        arrNames(i) = Entry(i + 1, Replace(Entry(2, pParam, Chr(0)), ";", ","), ",")
    Next
    oDoc.copyto = arrNames
    oDoc.Subject = Entry(3, pParam, Chr(0))
    '-> Setting du body
    Set Bodyitem = oSess.CreateStream
    '-> Envoie depuis la page de visu
    'If frmMail.RichTextBox1.Text <> "" Then Bodyitem.WriteText frmMail.RichTextBox1.Text
    '-> V�rifier si un fichier pour body est donn�
    If Trim(Entry(4, pParam, Chr(0))) <> "" Then
        Bodyitem.WriteText "<HTML><META http-equiv='Content-Type' content='text/html; charset=UTF-8' /><BODY><P><FONT SIZE=2 FACE='Arial'>"
        '-> Ajout dans le body du text
        Bodyitem.WriteText Replace(Entry(4, pParam, Chr(0)), Chr(13) + Chr(10), "<BR>") & "</FONT></P></BODY>"
    End If 'Si pas de body sp�cifi�
    '-> Setting du bodyfile
    '-> V�rifier si un fichier pour body est donn�
    If Trim(Entry(5, pParam, Chr(0))) <> "" Then
        If Dir$(Trim(Entry(5, pParam, Chr(0)))) <> "" Then
            '-> Ouvrir le fichier body
            hdlBody = FreeFile
            Open Trim(Entry(5, pParam, Chr(0))) For Input As #hdlBody
            Bodyitem.WriteText "<HTML><BODY><P>"
            Do While Not EOF(hdlBody)
                '-> Lecture de la ligne
                Line Input #hdlBody, Ligne
                '-> Ajout dans le body du text
                Bodyitem.WriteText "<BR><FONT SIZE=2 FACE='Arial'>" & Ligne & "</FONT>"
            Loop
            Bodyitem.WriteText "</P></BODY>"
            '-> Fermer le fichier
            Close #hdlBody
        End If 'Si fichier body existe
    End If 'Si pas de body sp�cifi�
    '-> on charge maintenant les fichiers HTML qui ont �t� g�n�r�s
    If Dir$(Trim(Entry(4, ListeHtmlFile, "|")), vbNormal) <> "" And Trim(Entry(4, ListeHtmlFile, "|")) <> "" Then
        '-> Ouvrir le fichier body
        hdlBody = FreeFile
        Ligne = String(65000, " ")
        Open Trim(Entry(4, ListeHtmlFile, "|")) For Binary As #hdlBody
        frmLib.RtfSel.Text = ""
        Do While Not EOF(hdlBody)
            '-> Lecture de la ligne
            'Line Input #hdlBody, Ligne
            Get #hdlBody, , Ligne
            '-> Ajout dans le body du text
            Bodyitem.WriteText Ligne
'            If oDoc.body <> "" Then
'                oDoc.body = oDoc.body & Ligne
'            Else
'                oDoc.body = Ligne
'            End If
        Loop
        '-> Fermer le fichier
        Close #hdlBody
    End If 'Si fichier body existe
    
    '-> on met le corps du texte au bon format
    Set Mime = oDoc.CreateMIMEEntity '("Body")
    '-> On prepare le mime
    Set Header = Mime.CreateHeader("Content-Type")
    Call Header.SetHeaderVal("multipart/mixed")
    
    '-> on prepare le mime pour le texte
    Set MimeChild = Mime.CreateChildEntity
    'MimeChild.SetContentFromText Bodyitem, "text/html", ENC_NONE
    MimeChild.SetContentFromText Bodyitem, "text/HTML;charset=UTF-8", 1729 'modification a cause de problemes de caracteres

    '-> on ferme
    Bodyitem.Close
    Bodyitem.Truncate
    
    
    '-> Ajouter les images s'il y en a
    For i = 1 To NbImages
        '-> on prepare le mime pour les pieces jointes
        Set MimeChild = Mime.CreateChildEntity
        Set Header = MimeChild.CreateHeader("Content-Disposition")
        Call Header.setHeaderValAndParams("attachment; filename=" & GetFileName(ListeHTMLImages(i)))
        Set Stream = oSess.CreateStream
        If Not Stream.Open(ListeHTMLImages(i), "binary") Then
            Exit Sub
        End If
        If Stream.Bytes = 0 Then
            Exit Sub
        End If
        Call MimeChild.SetContentFromBytes(Stream, "application/images", 1730)
        Call Stream.Close
        Call Stream.Truncate
    Next
    
    '-> cas piece jointe au format pdf
    If FileKeyExport <> "" And Trim(Entry(7, pParam, Chr(0))) = "4" Then
        Sleep 9000
        '-> on prepare le mime pour les pieces jointes
        Set MimeChild = Mime.CreateChildEntity
        Set Header = MimeChild.CreateHeader("Content-Disposition")
        Call Header.setHeaderValAndParams("attachment; filename=" & GetFileName(FileKeyExport & ".pdf"))
        Set Stream = oSess.CreateStream
        If Not Stream.Open(FileKeyExport & ".pdf", "binary") Then
            Exit Sub
        End If
        If Stream.Bytes = 0 Then
            Exit Sub
        End If
        Call MimeChild.SetContentFromBytes(Stream, "application/images", 1730)
        Call Stream.Close
        Call Stream.Truncate
    End If
    
    '-> cas piece jointe au format turbo
    If FileKeyExport <> "" And Trim(Entry(7, pParam, Chr(0))) = "0" Then
        '-> on prepare le mime pour les pieces jointes
        Set MimeChild = Mime.CreateChildEntity
        Set Header = MimeChild.CreateHeader("Content-Disposition")
        Call Header.setHeaderValAndParams("attachment; filename=" & GetFileName(FileKeyExport))
        Set Stream = oSess.CreateStream
        If Not Stream.Open(FileKeyExport, "binary") Then
            Exit Sub
        End If
        If Stream.Bytes = 0 Then
            Exit Sub
        End If
        Call MimeChild.SetContentFromBytes(Stream, "application/turbo", 1730)
        Call Stream.Close
        Call Stream.Truncate
    End If
    
    '-> cas de la piece jointe au format HTML
    If FileKeyExport <> "" And Trim(Entry(7, pParam, Chr(0))) = "1" Then
        '-> on cree le html pour les pieces jointes
        PathToExport = GetTempFileNameVB("WWW", True)
        CreateEditionHTML
        '-> Attacher le fichier sp�cifi�
        If ListeHtmlFile <> "" Then
            For i = 1 To NumEntries(ListeHtmlFile, "|")
                If Dir$(Entry(i, ListeHtmlFile, "|")) <> "" Then
                    '-> on prepare le mime pour les pieces jointes
                    Set MimeChild = Mime.CreateChildEntity
                    Set Header = MimeChild.CreateHeader("Content-Disposition")
                    Call Header.setHeaderValAndParams("attachment; filename=" & GetFileName(Entry(i, ListeHtmlFile, "|")))
                    Set Stream = oSess.CreateStream
                    If Not Stream.Open(Entry(i, ListeHtmlFile, "|"), "binary") Then
                        Exit Sub
                    End If
                    If Stream.Bytes = 0 Then
                        Exit Sub
                    End If
                    Call MimeChild.SetContentFromBytes(Stream, "application/html", 1730)
                    Call Stream.Close
                    Call Stream.Truncate
                End If
            Next
        End If
        '-> Ajouter les images s'il y en a
        For i = 1 To NbImages
            If Dir$(ListeHTMLImages(i)) <> "" Then
                '-> on prepare le mime pour les pieces jointes
                Set MimeChild = Mime.CreateChildEntity
                Set Header = MimeChild.CreateHeader("Content-Disposition")
                Call Header.setHeaderValAndParams("attachment; filename=" & GetFileName(ListeHTMLImages(i)))
                Set Stream = oSess.CreateStream
                If Not Stream.Open(ListeHTMLImages(i), "binary") Then
                    Exit Sub
                End If
                If Stream.Bytes = 0 Then
                    Exit Sub
                End If
                Call MimeChild.SetContentFromBytes(Stream, "application/images", 1730)
                Call Stream.Close
                Call Stream.Truncate
            End If
        Next
    End If
    
    '-> si il y a des fichiers joints lorsque l on vient d'un fichier zipp�
    For i = 0 To MDIMain.mnuJoin.Count - 1
        If MDIMain.mnuJoin.Item(i).Visible Then
            If Trim(MDIMain.mnuJoin.Item(i).Tag) <> "" Then
                '-> on prepare le mime pour les pieces jointes
                Set MimeChild = Mime.CreateChildEntity
                Set Header = MimeChild.CreateHeader("Content-Disposition")
                Call Header.setHeaderValAndParams("attachment; filename=" & GetFileName(MDIMain.mnuJoin.Item(i).Tag))
                Set Stream = oSess.CreateStream
                If Not Stream.Open(MDIMain.mnuJoin.Item(i).Tag, "binary") Then
                    Exit Sub
                End If
                If Stream.Bytes = 0 Then
                    Exit Sub
                End If
                Call MimeChild.SetContentFromBytes(Stream, "application/turbo", 1730)
                Call Stream.Close
                Call Stream.Truncate
            End If
        End If
    Next
    
    '-> si le corps du mail est en html on l'envoi directement
    If frmMail.Check3.Value = "1" Then SendOutLook = True
    
    '-> mettre le mail dans la boite d'envoi
    oDoc.PostedDate = Now()
    oDoc.SAVEMESSAGEONSEND = True
    '-> Afficher le mail
    If SendOutLook Then
        oDoc.Send False
    Else
        oDoc.visable = True
        oDoc.Send False
    End If
    
    '-> Lib�rer les pointeurs
    Set oSess = Nothing
    Set oDB = Nothing
    Set oDoc = Nothing
    Set oItem = Nothing
    Set Mime = Nothing
    Set Bodyitem = Nothing
    Exit Sub
End If


If IsOutLook Then

    '-> Obtenir un pointeur vers OutLook
    Set aOutLook = StartOutlook()
    
    '-> Cr�er un nouveau message
    If signature = "" Then
        'Set aMail = aOutLook.createitem(0)
        'aMail.display
        'signature = aMail.htmlbody
        'aMail.Close (1)
    End If
    Set aMail = aOutLook.createitem(0)
    '-> Afficher le corps du message
    '-> Setting des param�tres
    aMail.To = Entry(1, pParam, Chr(0))
    aMail.CC = Entry(2, pParam, Chr(0))
    aMail.Subject = Entry(3, pParam, Chr(0))
    '-> Setting du body
    '-> Envoie depuis la page de visu
    If frmMail.RichTextBox1.Text <> "" Then aMail.body = frmMail.RichTextBox1.Text
    '-> V�rifier si un fichier pour body est donn�
    If Trim(Entry(4, pParam, Chr(0))) <> "" Then
        aMail.htmlbody = "<HTML><BODY><P><FONT SIZE=2 FACE='Arial'>"
        '-> Ajout dans le body du text
        aMail.htmlbody = aMail.htmlbody & Replace(Entry(4, pParam, Chr(0)), Chr(13) & Chr(10), "<BR>") & "</FONT></P></BODY>"
    End If 'Si pas de body sp�cifi�
    '-> Setting du bodyfile
    '-> V�rifier si un fichier pour body est donn�
    If Trim(Entry(5, pParam, Chr(0))) <> "" Then
        If Dir$(Trim(Entry(5, pParam, Chr(0)))) <> "" Then
            '-> Ouvrir le fichier body
            hdlBody = FreeFile
            Open Trim(Entry(5, pParam, Chr(0))) For Input As #hdlBody
            aMail.htmlbody = aMail.htmlbody & "<HTML><BODY><P>"
            Do While Not EOF(hdlBody)
                '-> Lecture de la ligne
                Line Input #hdlBody, Ligne
                '-> Ajout dans le body du text
                aMail.htmlbody = aMail.htmlbody & "<BR><FONT SIZE=2 FACE='Arial'>" & Ligne & "</FONT>"
            Loop
            aMail.htmlbody = aMail.htmlbody & "</P></BODY>"
            '-> Fermer le fichier
            Close #hdlBody
        End If 'Si fichier body existe
    End If 'Si pas de body sp�cifi�
    '-> on charge maintenant les fichiers HTML qui ont �t� g�n�r�s
    If Dir$(Trim(Entry(4, ListeHtmlFile, "|")), vbNormal) <> "" And Trim(Entry(4, ListeHtmlFile, "|")) <> "" Then
        '-> Ouvrir le fichier body
        hdlBody = FreeFile
        Ligne = String(65000, " ")
        Open Trim(Entry(4, ListeHtmlFile, "|")) For Binary As #hdlBody
        frmLib.RtfSel.Text = ""
        Do While Not EOF(hdlBody)
            '-> Lecture de la ligne
            'Line Input #hdlBody, Ligne
            Get #hdlBody, , Ligne
            '-> Ajout dans le body du text
            If aMail.htmlbody <> "" Then
                aMail.htmlbody = aMail.htmlbody & Ligne
            Else
                aMail.htmlbody = Ligne
            End If
        Loop
        '-> Fermer le fichier
        Close #hdlBody
    End If 'Si fichier body existe
    aMail.htmlbody = aMail.htmlbody & vbNewLine & signature
    '-> Ajouter les images s'il y en a
    For i = 1 To NbImages
        aMail.Attachments.add ListeHTMLImages(i)
    Next
    
    '-> on pointe sur les images pour les afficher dans le corps
    aMail.htmlbody = Replace(aMail.htmlbody, "url(.\", "url(cid:")

    '-> cas piece jointe au format pdf
    If FileKeyExport <> "" And Trim(Entry(7, pParam, Chr(0))) = "4" Then
        Sleep 9000
        aMail.Attachments.add FileKeyExport & ".pdf"
    End If
    
    '-> cas piece jointe au format turbo
    If FileKeyExport <> "" And Trim(Entry(7, pParam, Chr(0))) = "0" Then
        aMail.Attachments.add FileKeyExport
    End If
    
    '-> cas de la piece jointe au format HTML
    If FileKeyExport <> "" And Trim(Entry(7, pParam, Chr(0))) = "1" Then
        '-> on cree le html pour les pieces jointes
        PathToExport = GetTempFileNameVB("WWW", True)
        CreateEditionHTML
        '-> Attacher le fichier sp�cifi�
        If ListeHtmlFile <> "" Then
            For i = 1 To NumEntries(ListeHtmlFile, "|")
                If Dir$(Entry(i, ListeHtmlFile, "|")) <> "" Then aMail.Attachments.add Entry(i, ListeHtmlFile, "|")
            Next
        End If
        '-> Ajouter les images s'il y en a
        For i = 1 To NbImages
            If Dir$(ListeHTMLImages(i)) <> "" Then aMail.Attachments.add ListeHTMLImages(i)
        Next
    End If
    
    '-> si il y a des fichiers joints lorsque l on vient d'un fichier zipp�
    For i = 0 To MDIMain.mnuJoin.Count - 1
        If MDIMain.mnuJoin.Item(i).Visible Then
            If Trim(MDIMain.mnuJoin.Item(i).Tag) <> "" Then aMail.Attachments.add MDIMain.mnuJoin.Item(i).Tag
        End If
    Next
    
    '-> si le corps du mail est en html on l'envoi directement
    If frmMail.Check3.Value = "1" Then SendOutLook = True
    
    '-> Afficher le mail
    If SendOutLook Then
        aMail.Send
    Else
        aMail.display
    End If
    
    '-> Lib�rer les pointeurs
    Set aMail = Nothing
    Set aOutLook = Nothing
Else
    '-> Intialiser la session MAPI
    If Not InitMAPISession Then
        '-> Afficher un message si on est en visu
        If OrigineMail = 1 Then
            MsgBox "Impossible d'initialiser la session Internet", vbCritical + vbOKOnly, "Erreur"
            Exit Sub
        End If
    End If
                
    '-> Cr�er un nouveau message
    With frmLib.MAPIMessages1
        .MsgIndex = -1
        .AddressEditFieldCount = 2
        .Compose
        .RecipAddress = Entry(1, pParam, Chr(0))
        .ResolveName
        '-> Message en copie
        If Trim(Entry(2, pParam, Chr(0))) <> "" Then
            .RecipIndex = 1
            .RecipAddress = Trim(Entry(2, pParam, Chr(0)))
            .ResolveName
            .RecipType = 2
        End If
    End With
    
    '-> Corp du message
    If Trim(Entry(4, pParam, Chr(0))) = "" Then
        '-> V�rifier si un fichier BODY file existe
        If Trim(Entry(5, pParam, Chr(0))) <> "" Then
            '-> V�rifier que le fichier existe
            If Dir$(Trim(Entry(5, pParam, Chr(0))), vbNormal) <> "" Then
                '-> Ouverture du fichier
                hdlBody = FreeFile
                Open Trim(Entry(5, pParam, Chr(0))) For Input As #hdlBody
                Do While Not EOF(hdlBody)
                    Line Input #hdlBody, Ligne
                    If frmLib.MAPIMessages1.MsgNoteText = "" Then
                        frmLib.MAPIMessages1.MsgNoteText = Ligne
                    Else
                        frmLib.MAPIMessages1.MsgNoteText = frmLib.MAPIMessages1.MsgNoteText & Chr(13) + Chr(10) & Ligne
                    End If
                Loop
                '-> Fermer le fichier
                Close #hdlBody
            End If 'Si fichier bodyfile existe
        End If 'Si fichier bodyfile sp�cifi�
    Else
        '-> Setting du body
        frmLib.MAPIMessages1.MsgNoteText = Entry(4, pParam, Chr(0))
    End If
       
    '-> Sujet du message
    frmLib.MAPIMessages1.MsgSubject = Entry(3, pParam, Chr(0))
        
    '-> on laisse la place pour inserer les pieces jointes
    frmLib.MAPIMessages1.MsgNoteText = Space(20) & vbCrLf & frmLib.MAPIMessages1.MsgNoteText
        
    '-> Liste des fichiers joints
    If FileKeyExport <> "" And Trim(Entry(7, pParam, Chr(0))) <> "3" Then
        '-> on verifie que le fichier a bien �t� cr��r
        Sleep 9000
        With frmLib.MAPIMessages1
            .AttachmentIndex = frmLib.MAPIMessages1.AttachmentCount
            .AttachmentPosition = frmLib.MAPIMessages1.AttachmentCount
            .AttachmentPathName = FileKeyExport & ".pdf"
            .AttachmentType = 0
        End With
    End If
    
    If ListeHtmlFile <> "" Then
        For i = 1 To NumEntries(ListeHtmlFile, "|")
            With frmLib.MAPIMessages1
                .AttachmentIndex = frmLib.MAPIMessages1.AttachmentCount
                .AttachmentPosition = frmLib.MAPIMessages1.AttachmentCount
                .AttachmentPathName = Entry(i, ListeHtmlFile, "|")
                .AttachmentType = 0
            End With
        Next
    End If
    
    '-> Ajouter les images s'il y en a
    For i = 1 To NbImages
        With frmLib.MAPIMessages1
            .AttachmentIndex = frmLib.MAPIMessages1.AttachmentCount
            .AttachmentPathName = ListeHTMLImages(i)
        End With
    Next

    '-> D�terminer la position des pi�ces jointes
'    If frmLib.MAPIMessages1.MsgNoteText <> "" Then
'        frmLib.MAPIMessages1.AttachmentPosition = Len(frmLib.MAPIMessages1.MsgNoteText) - 1
'    Else
'        frmLib.MAPIMessages1.AttachmentPosition = 0
'    End If
        
    '-> Envoyer le mail
    frmLib.MAPIMessages1.Send False
    
End If 'Si envoie sur internet

End Sub

Public Function CreateEditionHTML()

'---> Cette proc�dure r�alise une impression HTML

Dim i As Integer
Dim SpoolName As String
Dim SpoolNum As String
Dim aLb As Libelle
Dim IsError As Boolean
Dim PrintSpool As Boolean
Dim NbPageToPrint As Integer
Dim DepartPrint As Long
Dim ReturnFile As String
Dim AnchorPos As Long
Dim PgS As Boolean
Dim PgP As Boolean


Dim aSpool As Spool
Dim aFichier As Fichier
Dim SpoolToPrint As Spool

On Error GoTo GestError

'-> Nouvelle edition : remettre � blanc la liste des images
Erase ListeHTMLImages
NbImages = 0

'-> Indiquer si on dessine la temporisation
If OrigineExport = 0 Then IsTempo = True

'-> Remettre � blanc la liste des fichiers touch�s
ListeHtmlFile = ""

'-> R�cup�rer le nom du spool sans l'extension
i = InStrRev(FileToExport, ".")
SpoolName = Mid$(FileToExport, 1, i - 1)
SpoolNum = Entry(2, SpoolKeyExport, "|")

'-> Pointer sur la classe libelle
Set aLb = Libelles("FRMINTERNET")

'-> Pointer sur l'objet fichier
Set aFichier = Fichiers(FileKeyExport)

'-> Inclure les num�ros de spool que si on exporte un fichier et qu'il ya plusieurs spools
If ExportType = 2 And aFichier.Spools.Count <> 1 Then IncluseSpoolNumber = True

'-> Init de l'export HTML
If NavigationType = 0 Then
    '-> Cr�ation du frameSet de Navigation
    CreateSommaire1
    '-> Nom du fichier � lancer en fin de traitement
    FileToLaunch = PathToExport & FileToExport
ElseIf NavigationType = 1 Then
    '-> Initialisation de la page unique HTML
    InitPageHtml PathToExport & FileToExport
    '-> initialiser la navigation
    IsImgNaviga
    '-> Positionnement en pixel du d�part
    If ExportType = 2 Then
        DepartPrint = InitSommaireOnePage(aFichier, -1) + 60
    Else
        '-> Analyse la la recherche du bon spool
        DepartPrint = InitSommaireOnePage(aFichier, CInt(Entry(2, SpoolKeyExport, "|"))) + 60
    End If
    '-> Ajouter � la liste des fichiers HTMLS
    AddToFileHtml PathToExport & FileToExport
    '-> Indiquer le fichier � lancer
    FileToLaunch = PathToExport & FileToExport
End If

'-> Impression de tous les spool dans le fichier
For Each SpoolToPrint In aFichier.Spools
    '-> Tester si on doit imprimer tous les spools
    If ExportType <> 2 Then
        '-> V�rifier si on imprime le spool sp�cifi�
        If SpoolKeyExport = SpoolToPrint.Key Then
            PrintSpool = True
        Else
            PrintSpool = False
        End If
    Else
        PrintSpool = True
    End If
    
    '-> Tester si on doit imprimer le spool
    If PrintSpool Then
        '-> V�rifier qu'il n'y a pas d'erreurs
        If SpoolToPrint.NbError <> 0 Then GoTo NextSpool
        '-> Impression de tous le spool
        For i = 1 To SpoolToPrint.nbPage
            '-> Indiquer la position de la page en cours
            PositionDebutPage = DepartPrint
            '-> Additionner le nombre de pages
            NbPageToPrint = NbPageToPrint + 1
            TailleLue = NbPageToPrint
            '-> Dessiner la temporisation si necessaire
            If IsTempo Then
                frmInternet.Label1.Caption = aLb.GetCaption(9) & i
                DrawTempo frmInternet.Picture1
                DoEvents
            End If
            '-> Tester si on imprime dans des fichiers s�par�e ou dans un seul
            If NavigationType = 1 Then 'Fichier Unique
                AnchorPos = DepartPrint - 50
                If SpoolToPrint.nbPage = 1 Then
                    PgS = False
                    PgP = False
                Else
                    If i = 1 Then
                        PgP = False
                        PgS = True
                    Else
                        PgP = True
                        '-> Tester si on est sur la derni�re page
                        If i = SpoolToPrint.nbPage Then
                            PgS = False
                        Else
                            PgS = True
                        End If
                    End If
                End If
                
                '-> Tester le cas de la page unique
                If ExportType = 0 Then
                    '-> Page en cours
                    PgS = False
                    PgP = False
                End If
                                                            
                '-> Cas d'un export de la page en cours
                If ExportType = 0 Then If PageToExport <> i Then GoTo NextPage
                
                '-> Barre de navigation
                InitNavigaOnePage SpoolToPrint.Num_Spool, PgS, PgP, "sp" & SpoolToPrint.Num_Spool & "PAGE", i, AnchorPos
                '-> Imprimer la page
                CreatePageHTML SpoolToPrint, i, DepartPrint
                '-> Mettre � jour la position d'impression
                DepartPrint = DepartPrint + frmLib.ScaleY(SpoolToPrint.maquette.Hauteur + 2, 7, 3)
            ElseIf NavigationType = 0 Then 'FrameSet
                
                '-> Cas d'un export de la page en cours
                If ExportType = 0 Then If PageToExport <> i Then GoTo NextPage
            
                '-> Initialiser la page
                If IncluseSpoolNumber Then
                    InitPageHtml PathToExport & SpoolName & "_sp" & SpoolToPrint.Num_Spool & "_page" & i & ".html"
                    AddToFileHtml PathToExport & SpoolName & "_sp" & SpoolToPrint.Num_Spool & "_page" & i & ".html"
                Else
                    InitPageHtml PathToExport & SpoolName & "_page" & i & ".html"
                    AddToFileHtml PathToExport & SpoolName & "_page" & i & ".html"
                End If
                
                '-> Imprimer la page
                CreatePageHTML SpoolToPrint, i, 30
                '-> Fermer la page
                ClosePageHTML
            Else 'Simple page
            
                '-> Cas d'un export de la page en cours
                If ExportType = 0 Then If PageToExport <> i Then GoTo NextPage
            
                '-> Initialiser la page
                If IncluseSpoolNumber Then
                    InitPageHtml PathToExport & SpoolName & "_sp" & SpoolToPrint.Num_Spool & "_page" & i & ".html"
                    FileToLaunch = PathToExport & SpoolName & "_sp" & SpoolToPrint.Num_Spool & "_page" & i & ".html"
                Else
                    InitPageHtml PathToExport & SpoolName & "_page" & i & ".html"
                    FileToLaunch = PathToExport & SpoolName & "_page" & i & ".html"
                End If
                '-> Imprimer la page
                CreatePageHTML SpoolToPrint, i, 30
                '-> Fermer la page
                ClosePageHTML
                '-> Ajouter dans la liste des fichiers � joindre
                AddToFileHtml FileToLaunch
            End If
NextPage:
        Next 'POur toutes les pages
    End If
NextSpool:
Next 'Pour tous les spools dans le fichier
        
If NavigationType = 1 Then
    '-> Si on est mode unique fermer le fichier HTML
    ClosePageHTML
End If

'-> Lancer si necessaire
If RunAfterExport Then ShellExecute MDIMain.hwnd, "Open", FileToLaunch, vbNullString, App.Path, 1

Exit Function

GestError:
    CreateEditionHTML = ""

End Function

Public Function CreateOutlookHTML()

'---> Cette proc�dure r�alise une impression HTML

Dim i As Integer
Dim SpoolName As String
Dim SpoolNum As String
Dim aLb As Libelle
Dim IsError As Boolean
Dim PrintSpool As Boolean
Dim NbPageToPrint As Integer
Dim DepartPrint As Long
Dim ReturnFile As String
Dim AnchorPos As Long
Dim PgS As Boolean
Dim PgP As Boolean


Dim aSpool As Spool
Dim aFichier As Fichier
Dim SpoolToPrint As Spool

On Error GoTo GestError

'-> Nouvelle edition : remettre � blanc la liste des images
Erase ListeHTMLImages
NbImages = 0

'-> Indiquer si on dessine la temporisation
If OrigineExport = 0 Then IsTempo = True

'-> Remettre � blanc la liste des fichiers touch�s
ListeHtmlFile = ""

'-> R�cup�rer le nom du spool sans l'extension
i = InStrRev(FileToExport, ".")
SpoolName = Mid$(FileToExport, 1, i - 1)
SpoolNum = Entry(2, SpoolKeyExport, "|")

'-> Pointer sur la classe libelle
Set aLb = Libelles("FRMINTERNET")

'-> Pointer sur l'objet fichier
Set aFichier = Fichiers(FileKeyExport)

'-> Inclure les num�ros de spool que si on exporte un fichier et qu'il ya plusieurs spools
If ExportType = 2 And aFichier.Spools.Count <> 1 Then IncluseSpoolNumber = True

'-> Init de l'export HTML
If NavigationType = 0 Then
    '-> Cr�ation du frameSet de Navigation
    CreateSommaire1
    '-> Nom du fichier � lancer en fin de traitement
    FileToLaunch = PathToExport & FileToExport
ElseIf NavigationType = 1 Then
    '-> Initialisation de la page unique HTML
    InitPageHtml PathToExport & FileToExport
    '-> initialiser la navigation
    'IsImgNaviga
    ''-> Positionnement en pixel du d�part
    'If ExportType = 2 Then
    '    DepartPrint = InitSommaireOnePage(aFichier, -1) + 60
    'Else
    '    '-> Analyse la la recherche du bon spool
    '    DepartPrint = InitSommaireOnePage(aFichier, CInt(Entry(2, SpoolKeyExport, "|"))) + 60
    'End If
    '-> Ajouter � la liste des fichiers HTMLS
    DepartPrint = 300
    AddToFileHtml PathToExport & FileToExport
    '-> Indiquer le fichier � lancer
    FileToLaunch = PathToExport & FileToExport
End If

'-> Impression de tous les spool dans le fichier
For Each SpoolToPrint In aFichier.Spools
    '-> Tester si on doit imprimer tous les spools
    If ExportType <> 2 Then
        '-> V�rifier si on imprime le spool sp�cifi�
        If SpoolKeyExport = SpoolToPrint.Key Then
            PrintSpool = True
        Else
            PrintSpool = False
        End If
    Else
        PrintSpool = True
    End If
    
    '-> Tester si on doit imprimer le spool
    If PrintSpool Then
        '-> V�rifier qu'il n'y a pas d'erreurs
        If SpoolToPrint.NbError <> 0 Then GoTo NextSpool
        '-> Impression de tous le spool
        For i = 1 To SpoolToPrint.nbPage
            '-> Indiquer la position de la page en cours
            PositionDebutPage = DepartPrint
            '-> Additionner le nombre de pages
            NbPageToPrint = NbPageToPrint + 1
            TailleLue = NbPageToPrint
            '-> Dessiner la temporisation si necessaire
            If IsTempo Then
                frmInternet.Label1.Caption = aLb.GetCaption(9) & i
                DrawTempo frmInternet.Picture1
                DoEvents
            End If
            '-> Tester si on imprime dans des fichiers s�par�e ou dans un seul
            If NavigationType = 1 Then 'Fichier Unique
                AnchorPos = DepartPrint - 50
                If SpoolToPrint.nbPage = 1 Then
                    PgS = False
                    PgP = False
                Else
                    If i = 1 Then
                        PgP = False
                        PgS = True
                    Else
                        PgP = True
                        '-> Tester si on est sur la derni�re page
                        If i = SpoolToPrint.nbPage Then
                            PgS = False
                        Else
                            PgS = True
                        End If
                    End If
                End If
                
                '-> Tester le cas de la page unique
                If ExportType = 0 Then
                    '-> Page en cours
                    PgS = False
                    PgP = False
                End If
                                                            
                '-> Cas d'un export de la page en cours
                If ExportType = 0 Then If PageToExport <> i Then GoTo NextPage
                
                '-> Barre de navigation
                'InitNavigaOnePage SpoolToPrint.Num_Spool, PgS, PgP, "sp" & SpoolToPrint.Num_Spool & "PAGE", i, AnchorPos
                '-> Imprimer la page
                CreatePageHTML SpoolToPrint, i, DepartPrint
                '-> Mettre � jour la position d'impression
                DepartPrint = DepartPrint + frmLib.ScaleY(SpoolToPrint.maquette.Hauteur + 2, 7, 3)
            ElseIf NavigationType = 0 Then 'FrameSet
                
                '-> Cas d'un export de la page en cours
                If ExportType = 0 Then If PageToExport <> i Then GoTo NextPage
            
                '-> Initialiser la page
                If IncluseSpoolNumber Then
                    InitPageHtml PathToExport & SpoolName & "_sp" & SpoolToPrint.Num_Spool & "_page" & i & ".html"
                    AddToFileHtml PathToExport & SpoolName & "_sp" & SpoolToPrint.Num_Spool & "_page" & i & ".html"
                Else
                    InitPageHtml PathToExport & SpoolName & "_page" & i & ".html"
                    AddToFileHtml PathToExport & SpoolName & "_page" & i & ".html"
                End If
                
                '-> Imprimer la page
                CreatePageHTML SpoolToPrint, i, 30
                '-> Fermer la page
                ClosePageHTML
            Else 'Simple page
            
                '-> Cas d'un export de la page en cours
                If ExportType = 0 Then If PageToExport <> i Then GoTo NextPage
            
                '-> Initialiser la page
                If IncluseSpoolNumber Then
                    InitPageHtml PathToExport & SpoolName & "_sp" & SpoolToPrint.Num_Spool & "_page" & i & ".html"
                    FileToLaunch = PathToExport & SpoolName & "_sp" & SpoolToPrint.Num_Spool & "_page" & i & ".html"
                Else
                    InitPageHtml PathToExport & SpoolName & "_page" & i & ".html"
                    FileToLaunch = PathToExport & SpoolName & "_page" & i & ".html"
                End If
                '-> Imprimer la page
                CreatePageHTML SpoolToPrint, i, 30
                '-> Fermer la page
                ClosePageHTML
                '-> Ajouter dans la liste des fichiers � joindre
                AddToFileHtml FileToLaunch
            End If
NextPage:
        Next 'POur toutes les pages
    End If
NextSpool:
Next 'Pour tous les spools dans le fichier
        
If NavigationType = 1 Then
    '-> Si on est mode unique fermer le fichier HTML
    ClosePageHTML
End If

'-> Lancer si necessaire
If RunAfterExport Then ShellExecute MDIMain.hwnd, "Open", FileToLaunch, vbNullString, App.Path, 1

Exit Function

GestError:
    CreateEditionHTML = ""

End Function

Private Sub ClosePageHTML()

'-> Imprimer les  baslises de fermeture
Print #hdlFile, "</BODY></HTML>"

'-> Fermer le fichier
Close #hdlFile

End Sub

Private Function InitPageHtml(FileName As String) As Integer

'---> Cette pro�cude initialise le fichier HTML de d�part

'-> Ouverture du fichier sp�cifi�
hdlFile = FreeFile
Open FileName For Output As #hdlFile

'-> Imprimer les balises d'ouverture du fichier
Print #hdlFile, "<HTML><BODY Style=""font-size:1"">"

'-> Renvoyer le handle du fichier
InitPageHtml = hdlFile

End Function

Private Sub IsImgNaviga()

'---> Cette proc�sure indique si les images de navigation sont pr�sentes

'-> Pr�cedent
If Dir$(App.Path & "\internet\" & BackImg) <> "" Then
    IsBackImg = True
    '-> Copy dans le r�pertoire
    If Dir$(PathToExport & BackImg) = "" Then _
        FileCopy App.Path & "\internet\" & BackImg, PathToExport & BackImg
    '-> Ajouter le fichier � la liste des fichiers � d�ployer
    AddToFileHtml PathToExport & BackImg
Else
    IsBackImg = False
End If


'-> Suivant
If Dir$(App.Path & "\internet\" & NextImg) <> "" Then
    IsNextImg = True
    '-> Copy dans le r�pertoire
    If Dir$(PathToExport & NextImg) = "" Then _
        FileCopy App.Path & "\internet\" & NextImg, PathToExport & NextImg
    '-> Ajouter le fichier � la liste des fichiers � d�ployer
    AddToFileHtml PathToExport & NextImg
Else
    IsNextImg = False
End If

'-> Num�ro de la page
If Dir$(App.Path & "\internet\" & PageImg) <> "" Then
    IsPageImg = True
    '-> Copy dans le r�pertoire
    If Dir$(PathToExport & PageImg) = "" Then _
        FileCopy App.Path & "\internet\" & PageImg, PathToExport & PageImg
    '-> Ajouter le fichier � la liste des fichiers � d�ployer
    AddToFileHtml PathToExport & PageImg
Else
    IsPageImg = False
End If

End Sub


Private Function InitSommaireOnePage(aFichier As Fichier, Num_Spool As Integer) As Long

'---> Proc�dure initialise le sommaire pour la navigation page � page

Dim aSpool As Spool
Dim SpoolPrint As Boolean
Dim TopPos As Long

'-> Position d'impression de d�part
TopPos = 10

'-> Positionner l'ancre du sommaire
Print #hdlFile, "<A NAME=SOMMAIRE></A>"

For Each aSpool In aFichier.Spools
    If Num_Spool = -1 Then
        '-> il faut imprimer tous les spools
        SpoolPrint = True
    Else
        '-> Faire un sommaire pour le spool en cours
        If aSpool.Num_Spool = Num_Spool Then
            SpoolPrint = True
        Else
            SpoolPrint = False
        End If
    End If
    
    '-> Impression du sommaire
    If SpoolPrint Then
        If IsPageImg Then
            Print #hdlFile, "<DIV Style=""LEFT:10px;Top:" & TopPos & "px;WIDTH:150px;HEIGHT:32px;POSITION:ABSOLUTE;BACKGROUND-IMAGE:url(.\" & PageImg & ")"">"
        Else
            Print #hdlFile, "<DIV Style=""LEFT:10px;Top:" & TopPos & "px;WIDTH:150px;HEIGHT:32px;POSITION:ABSOLUTE;"">"
        End If
            
        '-> Imprimer le num�ro du spool
        Print #hdlFile, "<TABLE WIDTH=""100%"" HEIGHT=""50px""><TR>"
    
        '-> Num�ro du spool
        Print #hdlFile, "<TD ALIGN=CENTER VALIGN=CENTER ><FONT STYLE=""line-height:100%;font: 12pt 'Comic Sans MS';COLOR:blue""><A STYLE=""color:white"" HREF=""" & FileToExport & "#SP" & aSpool.Num_Spool & """>Spool n� : " & aSpool.Num_Spool & "</A></FONT></TD>"
            
        '-> Fermer ce tableau
        Print #hdlFile, "</TR></TABLE></DIV>"
        
        '-> Hauteur du sommaire
        TopPos = TopPos + 50
    End If
        
Next 'Pour tous les spools

'-> Renvoyer la hauter du sommaire pour le d�but de l'impression
InitSommaireOnePage = TopPos

End Function

Private Sub InitNavigaOnePage(isSpool As Integer, PageSuivante As Boolean, PagePrecedente As Boolean, AnchorName As String, NumPage As Integer, PositionDepart As Long)

'---> Cette proc�dure cr�er une ancre pour la page en cours et met les images de navigation
'page pr�c�dente et page suivante

Dim LeftPos As Long

'-> Positionnement du num�ro du spool
If isSpool <> 0 Then
    If IsPageImg Then
        Print #hdlFile, "<DIV Style=""LEFT:10px;Top:" & PositionDepart - 6 & "px;WIDTH:150px;HEIGHT:32px;POSITION:ABSOLUTE;BACKGROUND-IMAGE:url(.\" & PageImg & ")"">"
    Else
        Print #hdlFile, "<DIV Style=""LEFT:10px;Top:" & PositionDepart - 6 & "px;WIDTH:150px;HEIGHT:32px;POSITION:ABSOLUTE;"">"
    End If
    '-> Positionnement de l'ancre de la page
    Print #hdlFile, "<A NAME=SP" & isSpool & " ></A>"
    
    '-> Imprimer le num�ro de la page
    Print #hdlFile, "<TABLE WIDTH=""100%"" HEIGHT=""50px""><TR>"
    
    '-> Num�ro de la page
    If Not IsPageImg Then
        Print #hdlFile, "<TD ALIGN=CENTER VALIGN=CENTER ><FONT STYLE=""line-height:100%;font: 12pt 'Comic Sans MS';COLOR:000000""><A STYLE=""color:white"" HREF=""" & FileToExport & "#SOMMAIRE"">Sommaire</A>&nbsp;&nbsp;&nbsp;</FONT></TD>"
    Else
        Print #hdlFile, "<TD ALIGN=CENTER VALIGN=CENTER ><FONT STYLE=""line-height:100%;font: 12pt 'Comic Sans MS';COLOR:white""><A STYLE=""color:white"" HREF=""" & FileToExport & "#SOMMAIRE"">Sommaire </A></FONT></TD>"
    End If
    
    '-> Fermer ce tableau
    Print #hdlFile, "</TR></TABLE></DIV>"
    
    LeftPos = 160
    '-> Posiionner la balise suivante
Else
    LeftPos = 10
End If
    
'-> Positionnement du num�ro de la page
If IsPageImg Then
    Print #hdlFile, "<DIV Style=""LEFT:" & LeftPos & "px;Top:" & PositionDepart - 6 & "px;WIDTH:150px;HEIGHT:32px;POSITION:ABSOLUTE;BACKGROUND-IMAGE:url(.\" & PageImg & ")"">"
Else
    Print #hdlFile, "<DIV Style=""LEFT:" & LeftPos & "px;Top:" & PositionDepart - 6 & "px;WIDTH:150px;HEIGHT:32px;POSITION:ABSOLUTE;"">"
End If
'-> Positionnement de l'ancre de la page
Print #hdlFile, "<A NAME=" & AnchorName & NumPage & " ></A>"

'-> Imprimer le num�ro de la page
Print #hdlFile, "<TABLE WIDTH=""100%"" HEIGHT=""50px""><TR>"

'-> Num�ro de la page
If Not IsPageImg Then
    Print #hdlFile, "<TD ALIGN=CENTER VALIGN=CENTER ><FONT STYLE=""line-height:100%;font: 12pt 'Comic Sans MS';COLOR:000000"">Sp" & isSpool & " Page n� " & NumPage & "&nbsp;&nbsp;&nbsp;</FONT></TD>"
Else
    Print #hdlFile, "<TD ALIGN=CENTER VALIGN=CENTER ><FONT STYLE=""line-height:100%;font: 12pt 'Comic Sans MS';COLOR:white"">Sp" & isSpool & " Page n� " & NumPage & "</FONT></TD>"
End If

'-> Fermer ce tableau
Print #hdlFile, "</TR></TABLE></DIV>"
    
'-> Positionner la valeur left de la balise DIV qui suit
LeftPos = LeftPos + 160
    
'-> Page pr�c�dente
If PagePrecedente Then
    If IsBackImg Then
        Print #hdlFile, "<DIV Style=""LEFT:" & LeftPos & "px;Top:" & PositionDepart & "px;WIDTH:70px;HEIGHT:40px;POSITION:ABSOLUTE"">"
        Print #hdlFile, "<A HREF=""" & FileToExport & "#" & AnchorName & NumPage - 1 & """ ><IMG BORDER=0 SRC="".\" & BackImg & """ALT=""Page pr�c�dente""></A>"
        Print #hdlFile, "</DIV>"
        '-> Repostionner la balise DIV suivante
        LeftPos = LeftPos + 70
    Else
        Print #hdlFile, "<DIV Style=""LEFT:" & LeftPos & "px;Top:" & PositionDepart & "px;WIDTH:170px;HEIGHT:40px;POSITION:ABSOLUTE"">"
        Print #hdlFile, "<TABLE><TR><TD ALIGN=CENTER VALIGN=CENTER>"
        Print #hdlFile, "<A HREF=""" & FileToExport & "#" & AnchorName & NumPage - 1 & """ ><FONT STYLE=""line-height:100%;font: 12pt 'Comic Sans MS';COLOR:000000"">Page Pr�c�dente</A> &nbsp;&nbsp;&nbsp;</FONT>"
        Print #hdlFile, "</TD></TR></TABLE>"
        Print #hdlFile, "</DIV>"
        '-> Repostionner la balise DIV suivante
        LeftPos = LeftPos + 170
    End If
End If

'-> Page suivante
If PageSuivante Then
    If IsNextImg Then
        Print #hdlFile, "<DIV Style=""LEFT:" & LeftPos & "px;Top:" & PositionDepart & "px;WIDTH:70px;HEIGHT:40px;POSITION:ABSOLUTE"">"
        Print #hdlFile, "<A HREF=""" & FileToExport & "#" & AnchorName & NumPage + 1 & """ ><IMG BORDER=0 SRC="".\" & NextImg & """ALT=""Page suivante""></A>"
        Print #hdlFile, "</DIV>"
    Else
        Print #hdlFile, "<DIV Style=""LEFT:" & LeftPos & "px;Top:" & PositionDepart & "px;WIDTH:170px;HEIGHT:40px;POSITION:ABSOLUTE"">"
        Print #hdlFile, "<TABLE><TR><TD ALIGN=CENTER VALIGN=CENTER>"
        Print #hdlFile, "<A HREF=""" & FileToExport & "#" & AnchorName & NumPage + 1 & """ >Page Suivante</A>&nbsp;&nbsp;&nbsp;</FONT>"
        Print #hdlFile, "</TD></TR></TABLE>"
        Print #hdlFile, "</DIV>"
    End If
End If


End Sub

Public Function CreatePageHTML(aSpool As Spool, pageToPrint As Integer, PositionDepart As Long)

'---> Cette focntion cr�er un fichier HTML
Dim i As Integer, j As Integer
Dim PositionX As Long
Dim PositionY As Long
Dim IsPaysage As Boolean
Dim Ligne As String
Dim TypeObjet As String
Dim NomObjet As String
Dim TypeSousObjet As String
Dim NomSousObjet As String
Dim Param As String
Dim DataFields As String
Dim FirstObj As Boolean
Dim aNode As Node
Dim ErrorCode As Integer
Dim aSection As Section
Dim Tempo As String

Dim aRect As RECT

'-> Setting du rectangle de dessin
aRect.Left = Decalage
aRect.Top = PositionDepart
aRect.Right = frmLib.ScaleX(aSpool.maquette.Largeur, 7, 3)
aRect.Bottom = frmLib.ScaleY(aSpool.maquette.Hauteur, 7, 3)

'-> Setting des contours
Tempo = "BORDER-LEFT:1pt SOLID;" & _
        "BORDER-TOP:1pt SOLID;" & _
        "BORDER-BOTTOM:1pt SOLID;" & _
        "BORDER-RIGHT:1pt SOLID;"

'-> Imprimer l'ombre
If ExportBordure Then
    Print #hdlFile, "<DIV Style=""" & Tempo & "BACKGROUND-COLOR:black;Left:" & aRect.Left + 3 & "px;TOP:" & aRect.Top + 3 & "px;WIDTH:" & aRect.Right + 3 & "px;HEIGHT:" & aRect.Bottom + 3 & "px;POSITION:Absolute""></DIV>"
    '-> Imprimer le contour de la page
    Print #hdlFile, "<DIV Style=""" & Tempo & "BACKGROUND-COLOR:white;Left:" & aRect.Left & "px;TOP:" & aRect.Top & "px;WIDTH:" & aRect.Right & "px;HEIGHT:" & aRect.Bottom & "px;POSITION:Absolute""></DIV>"
End If

'-> Initialiser la position X , y du pointeur sur les marges du document
PositionY = PositionDepart + Sortie.ScaleY(aSpool.maquette.MargeTop, 7, 3)
PositionX = Sortie.ScaleX(aSpool.maquette.MargeLeft, 7, 3)

'-> Indiquer l'�tat du premier objet que l'on trouve
FirstObj = True

'-> Lecture des lignes de la page
For i = 1 To NumEntries(aSpool.GetPage(pageToPrint), Chr(0))
    
    '-> R�cup�ration de la ligne en cours
    Ligne = Entry(i, aSpool.GetPage(pageToPrint), Chr(0))

    '-> Ne rien imprimer si page � blanc
    If Trim(Ligne) = "" Then GoTo NextLigne
    
    If pageToPrint = 0 Then
        frmLib.Rtf(aSection.IdRTf).Text = frmLib.Rtf(aSection.IdRTf).Text & Chr(13) & Chr(10) & Ligne
    Else
        If InStr(1, Ligne, "[") = 1 Then
            '-> r�cup�ration des param�tres
            AnalyseObj Ligne, TypeObjet, NomObjet, TypeSousObjet, NomSousObjet, Param, DataFields
            '-> Selon le type d'objet
            If UCase$(TypeObjet) = "ST" Then
                PrintSectionHTML NomObjet, Param, DataFields, FirstObj, PositionX, PositionY, aSpool
                FirstObj = False
            ElseIf UCase$(TypeObjet) = "TB" Then
                PrintTableauHTML NomObjet, Param, DataFields, PositionX, PositionY, NomSousObjet, FirstObj, aSpool
                FirstObj = False
            End If
        End If 'Si premier caract�re = "["
    End If 'Si on imprime la page de s�lection
NextLigne:

Next 'Pour toutes les lignes de la page

Exit Function
           
GestError:
        
End Function
Private Sub GetBordureStyle(aObject As Object, BordureStyle As String)

'---> Cette proc�dure cr�er le style HTML des bordures

Dim Tempo As String

'-> Initialiser � blanc
BordureStyle = ""

'-> Selon le type d'objets
If TypeOf aObject Is Section Or TypeOf aObject Is ImageObj Then
    If aObject.Contour Then
        Tempo = "BORDER-LEFT:1pt SOLID;" & _
                "BORDER-TOP:1pt SOLID;" & _
                "BORDER-BOTTOM:1pt SOLID;" & _
                "BORDER-RIGHT:1pt SOLID;"
               
    End If 'Si brd sur la section
Else '-> c'est un cadre
    If aObject.Haut Then
        If Tempo = "" Then
            Tempo = "BORDER-TOP:" & aObject.LargeurTrait & "pt solid"
        Else
            Tempo = Tempo & ";BORDER-TOP:" & aObject.LargeurTrait & "pt solid"
        End If
    End If
    
    If aObject.Bas Then
        If Tempo = "" Then
            Tempo = "BORDER-BOTTOM:" & aObject.LargeurTrait & "pt solid"
        Else
            Tempo = Tempo & ";BORDER-BOTTOM:" & aObject.LargeurTrait & "pt solid"
        End If
    End If
    
    If aObject.Gauche Then
        If Tempo = "" Then
            Tempo = "BORDER-LEFT:" & aObject.LargeurTrait & "pt solid"
        Else
            Tempo = Tempo & ";BORDER-LEFT:" & aObject.LargeurTrait & "pt solid"
        End If
    End If
    
    If aObject.Droite Then
        If Tempo = "" Then
            Tempo = "BORDER-RIGHT:" & aObject.LargeurTrait & "pt solid"
        Else
            Tempo = Tempo & ";BORDER-RIGHT:" & aObject.LargeurTrait & "pt solid"
        End If
    End If
End If
    
If Tempo = "" Then
    BordureStyle = ""
Else
    BordureStyle = Tempo & ";"
End If

End Sub


Private Function PrintSectionHTML(ByVal SectionName As String, ByRef Param As String, _
                         ByRef DataFields As String, ByRef FirstObj As Boolean, _
                         ByRef PositionX As Long, ByRef PositionY As Long, ByRef aSpool As Spool) As Boolean


'---> Cette proc�dure imprime dans une page HTML une section des texte

Dim aSection As Section
Dim i As Integer
Dim NomObjet As String
Dim aCadre As Cadre
Dim aBmp As ImageObj

'-> Pointer sur la section � imprimer
Set aSection = aSpool.maquette.Sections(UCase$(SectionName))

'-> positionner la section dans la page
Select Case aSection.AlignementLeft
    Case 2 'Marge gauche
        PositionX = Sortie.ScaleX(aSpool.maquette.MargeLeft, 7, 3)
    Case 4 'Centr�
        PositionX = Sortie.ScaleX((aSpool.maquette.Largeur - aSection.Largeur) / 2, 7, 3)
    Case 3 'Marge Droite
        PositionX = Sortie.ScaleX(aSpool.maquette.Largeur - aSection.Largeur - aSpool.maquette.MargeLeft, 7, 3)
    Case 5 'Sp�cifi�
        PositionX = Sortie.ScaleX(aSection.Left, 7, 3)
End Select
Select Case aSection.AlignementTop
    Case 1 'Libre
        'PositionY = Sortie.ScaleY(PositionY, 3, 3)
    Case 2 'Marge haut
        PositionY = Sortie.ScaleY(aSpool.maquette.MargeTop, 7, 3) + PositionDebutPage
    Case 4 'Centr�
        PositionY = Sortie.ScaleY((aSpool.maquette.Hauteur - aSection.Hauteur) / 2, 7, 3) + PositionDebutPage
    Case 3 'Marge bas
        PositionY = PositionDebutPage - Sortie.ScaleY(aSpool.maquette.Hauteur - aSection.Hauteur - aSpool.maquette.MargeTop, 7, 3)
    Case 5 'Sp�cifi�
        PositionY = Sortie.ScaleY(aSection.Top, 7, 3) + PositionDebutPage
End Select

'-> Imprimer la section
Print #hdlFile, "<!-- Section de texte : " & aSection.Nom & " -->"
PrintObjRTF2HTML PositionX, PositionY, aSection, DataFields, PositionX, PositionY, aSpool

'-> Impmrimer les objets affect�s � cette section
For i = 1 To aSection.nEntries - 1
    NomObjet = aSection.GetOrdreAffichage(i)
    If UCase$(Entry(1, NomObjet, "-")) = "CDR" Then
        '-> Pointer sur le cadre
        Set aCadre = aSection.Cadres(UCase$(Entry(2, NomObjet, "-")))
        '-> Imprimer l'objet
        Print #hdlFile, "<!--  Cadre : " & aCadre.Nom & " -->"
        PrintObjRTF2HTML PositionX, PositionY, aCadre, DataFields, PositionX, PositionY, aSpool, aCadre.MargeInterne
        '-> Liberer le pointeur
        Set aCadre = Nothing
    ElseIf UCase$(Entry(1, NomObjet, "-")) = "BMP" Then
        Set aBmp = aSection.Bmps(UCase$(Entry(2, NomObjet, "-")))
        Print #hdlFile, "<!-- Image " & aBmp.Nom & " -->"
        PrintBmpHTML aBmp, PositionX, PositionY, DataFields, aSpool
        Set aBmp = Nothing
    End If
Next

'-> Mettre � jour la position d'impression apr�s dessin de la bordure
PositionY = PositionY + frmLib.ScaleY(aSection.Hauteur, 7, 3)

End Function

Private Function PrintBmpHTML(aBmp As ImageObj, DebutRangX As Long, DebutRangY As Long, ByVal DataFields As String, ByRef aSpool As Spool) As Boolean

'---> Impression d'un BMP dans le cadre d'une page HTML

Dim apic As PictureBox
Dim Champ As String
Dim NomFichier As String
Dim aRect As RECT
Dim i As Integer
Dim BordureStyle As String
Dim IsOk As Boolean
Dim NomImage As String
Dim Balise As String

'-> Initialiser le pictureBox
Set apic = frmLib.PicObj(aBmp.IdPic)

'-> Impression du BMP
If aBmp.isVariable Then
    '-> Venir charg� le BMP associ�
    For i = 2 To NumEntries(DataFields, "^")
        Champ = Entry(i, DataFields, "^")
        If UCase$(aBmp.Fichier) = "^" & UCase$(Mid$(Champ, 1, 4)) Then
            '-> R�cup�ration du nom du fichier
            NomFichier = RTrim(Mid$(Champ, 5, Len(Champ) - 1))
            '-> Test de l'utilisation de l'association
            If aBmp.UseAssociation Then
                '-> R�cup�rer le nom du fichier dans le fichier ini
                NomFichier = GetPictureAssociation(NomFichier)
            End If
            Exit For
        End If
    Next
Else
    '-> Le bmp n'est pas variable : venir charger le fichier
    NomFichier = aBmp.Fichier
End If

'-> Dessiner la bordure si necessaire
GetBordureStyle aBmp, BordureStyle

'-> Gestion du nom du fichier
If Trim(NomFichier) <> "" Then
    '-> V�rifier que l'on trouve le fichier sp�cifi� sur le disque
    If Dir$(NomFichier, vbNormal) <> "" Then
        '-> R�cup�rer le nom de l'image
        NomImage = Entry(NumEntries(NomFichier, "\"), NomFichier, "\")
        '-> V�rifier que l'on n' a pas d�ja charge fichier pour ne pas le dupliquer
        If Not IsImageHTML(PathToExport & NomImage) Then
            '-> La copier dans le r�pertoire Internet
            FileCopy NomFichier, PathToExport & NomImage
            '-> Mettre � jour la matrice des images et son compteur
            ReDim Preserve ListeHTMLImages(NbImages + 1)
            ListeHTMLImages(NbImages + 1) = PathToExport & NomImage
            NbImages = NbImages + 1
            '-> Indiquer que l'image est ok
            IsOk = True
        Else
            '-> L'image existe d�ja , ne cr�er que la balise
            IsOk = True
        End If
    End If 'Si on trouve le fichier
End If

'-> Appliquer le redimenionnement auto
apic.AutoSize = aBmp.IsAutosize

'-> Imprimer la balise DIV si n�cessaire
If IsOk Then
    '-> Charger l'image dans le picturebox
    Set apic.Picture = LoadPicture(PathToExport & NomImage)

    '-> Mise � ajour de la trusture RECT
    aRect.Left = DebutRangX + frmLib.ScaleX(aBmp.Left, 7, 3) + Decalage
    aRect.Top = DebutRangY + frmLib.ScaleY(aBmp.Top, 7, 3) + Decalage
    aRect.Right = frmLib.ScaleX(apic.width, 1, 3)
    aRect.Bottom = frmLib.ScaleY(apic.height, 1, 3)
    
    '-> Imprimer la balise
    If BordureStyle <> "" Then
        Balise = "<DIV Style=""" & BordureStyle & ";BACKGROUND-IMAGE:url(.\" & NomImage & ");LEFT:" & aRect.Left & "px;TOP:" & aRect.Top & "px;WIDTH:" & aRect.Right & "px;HEIGHT:" & aRect.Bottom & "px;POSITION:absolute""></DIV>"
    Else
        Balise = "<DIV Style=""BACKGROUND-IMAGE:url(.\" & NomImage & ");LEFT:" & aRect.Left & "px;TOP:" & aRect.Top & "px;WIDTH:" & aRect.Right & "px;HEIGHT:" & aRect.Bottom & "px;POSITION:absolute""></DIV>"
    End If
    
    Print #hdlFile, Balise
    
End If

'-> Lib�rer le pointeur
Set apic = Nothing


    

End Function
Private Function IsImageHTML(NomFichier As String) As Boolean

'---> Cette fonction analyse la matrice des images pour indiquer s'il elle existe d�ja

Dim i As Integer

If NbImages = 0 Then
    IsImageHTML = False
    Exit Function
End If

'-> Analyse de la matrice
For i = 1 To UBound(ListeHTMLImages())
    If Trim(UCase$(NomFichier)) = Trim(UCase$(ListeHTMLImages(i))) Then
        '-> Image existe d�ja
        IsImageHTML = True
        Exit Function
    End If
Next 'POur toutes les images

End Function

Private Function PrintObjRTF2HTML(PositionX As Long, PositionY As Long, _
                        aObject As Object, ByVal DataFields As String, _
                        DebutRangX As Long, DebutRangY As Long, ByRef aSpool As Spool, Optional MargeInterne As Single) As Boolean

'---> Cette proc�dure imprime un objet RTf au format HTML

Dim aRtf As RichTextBox
Dim aSection As Section
Dim RTFValue As String
Dim i As Integer
Dim NbChamp As Long
Dim Champ As String
Dim aField() As String
Dim aRect As RECT
Dim PosX As Long
Dim PosY As Long
Dim BordureStyle As String
Dim PrintBackColor As Boolean

'-> Mettre l'�diteur RTF aux bonnes dimensions
Set aRtf = frmLib.Rtf(aObject.IdRTf)
aRtf.width = frmLib.ScaleX(aObject.Largeur, 7, 1)
aRtf.height = frmLib.ScaleY(aObject.Hauteur, 7, 1)
aRtf.Visible = True

'-> R�cup�ration du code RTF
RTFValue = aRtf.TextRTF


'-> Cr�er une base de champs
If Trim(DataFields) = "" Then
Else
    '-> R�cup�ration du nombre de champs
    NbChamp = NumEntries(DataFields, "^")
    For i = 2 To NbChamp
        Champ = Entry(i, DataFields, "^")
        ReDim Preserve aField(1 To 2, i - 1)
        aField(1, i - 1) = "^" & Mid$(Champ, 1, 4)
        aField(2, i - 1) = RTrim(Mid$(Champ, 5, Len(Champ) - 1))
    Next
    '-> Faire un remplacement des champs par leur valeur
    For i = 1 To NbChamp - 1
        aRtf.TextRTF = Replace(aRtf.TextRTF, aField(1, i), aField(2, i))
    Next
    '-> Indiquer que l'on imprimer le fond
    PrintBackColor = True
End If

'-> D�finition de l'emplacement d'impression de la zone de texte
If TypeOf aObject Is Section Then
    aRect.Left = PositionX + Decalage
    aRect.Top = PositionY + Decalage
    aRect.Bottom = frmLib.ScaleY(aObject.Hauteur, 7, 3)
    aRect.Right = frmLib.ScaleY(aObject.Largeur, 7, 3)
ElseIf TypeOf aObject Is Cadre Then
    '-> Il faut modifier la position X et Y du cadre
    PosX = DebutRangX + Sortie.ScaleX(aObject.Left, 7, 3)
    PosY = DebutRangY + Sortie.ScaleY(aObject.Top, 7, 3)
    '-> Setting du rect de dessin
    aRect.Left = PosX + Decalage
    aRect.Top = PosY + Decalage
    aRect.Right = Sortie.ScaleX(aObject.Largeur, 7, 3)
    aRect.Bottom = Sortie.ScaleY(aObject.Hauteur, 7, 3)
End If

'-> D�tection des bordures
GetBordureStyle aObject, BordureStyle

'-> Attention pour les cadres, il faut d'abord le contour dans une balise � part
If TypeOf aObject Is Cadre Then
    '-> Imprimer le contour avec la couleur
    If BordureStyle = "" Then
        Print #hdlFile, "<DIV Style=""BACKGROUND-COLOR:#" & ConvertColor(aRtf.BackColor) & "; HEIGHT:" & aRect.Bottom & _
                            "px;WIDTH:" & aRect.Right & _
                            "px;LEFT:" & aRect.Left & _
                            "px;TOP:" & aRect.Top & _
                            "px;POSITION:absolute""></DIV>"
        
    Else
        Print #hdlFile, "<DIV Style=""" & BordureStyle & "BACKGROUND-COLOR:#" & ConvertColor(aRtf.BackColor) & "; HEIGHT:" & aRect.Bottom & _
                            "px;WIDTH:" & aRect.Right & _
                            "px;LEFT:" & aRect.Left & _
                            "px;TOP:" & aRect.Top & _
                            "px;POSITION:absolute""></DIV>"
    End If
    
    '-> Cr�er un nouveau rectangle avec les les marges internes
    aRect.Left = aRect.Left + frmLib.ScaleX(aObject.MargeInterne, 7, 3)
    aRect.Top = aRect.Top + frmLib.ScaleY(aObject.MargeInterne, 7, 3)
    aRect.Right = aRect.Right - frmLib.ScaleX(aObject.MargeInterne, 7, 3)
    aRect.Bottom = aRect.Bottom - frmLib.ScaleY(aObject.MargeInterne, 7, 3)
    
    '-> Ne pas imprimer les bordures
    BordureStyle = ""
    
    '-> Indiquer que l'on n'imprime pas le fond
    PrintBackColor = False
    
End If 'Si on impprime un cadre

'-> il faut convertir le format RTF en HML
Print #hdlFile, Rtf2Html(aRtf, aRect, BordureStyle, PrintBackColor)

'-> Resiturer le contenu du RTF
aRtf.TextRTF = RTFValue

End Function

Private Function PrintTableauHTML(ByRef NomObjet As String, ByRef Param As String, _
                         ByRef DataFields As String, ByRef PositionX As Long, _
                         ByRef PositionY As Long, ByRef NomSousObjet As String, ByRef FirstObj As Boolean, ByRef aSpool As Spool) As Boolean
                         
Dim aTb As Tableau
Dim nLig As Integer

'-> Pointer sur le tableau pass� en argument
Set aTb = aSpool.maquette.Tableaux(UCase$(NomObjet))

'-> R�cup�ration de la ligne de tableau � imprimer
nLig = CInt(Entry(2, Param, "\"))

'-> Imprimer le block de ligne
PositionY = PrintBlockHTML(NomSousObjet, Param, DataFields, PositionX, PositionY, aTb, nLig, aSpool)

End Function

Private Function PrintBlockHTML(ByRef NomSousObjet As String, ByRef Param As String, _
                        ByRef DataFields As String, ByVal PosX As Long, _
                        ByRef PosY As Long, ByRef aTb As Tableau, ByVal Ligne As Integer, ByRef aSpool As Spool) As Long

'---> Cette Proc�dure imprime un block de ligne
Dim aBlock As Block
Dim Champ As String
Dim aField() As String
Dim i As Long
Dim aCell As Cellule
Dim HauteurLigPix As Long
Dim ListeField() As String
Dim Tempo

'-> Pointer sur le block de tableau � �diter
Set aBlock = aTb.Blocks("BL-" & UCase$(NomSousObjet))

'-> Initaliser le block
InitBlock aBlock, aSpool

'-> Calculer la modification des positions X et Y des blocks de ligne. _
Les coordonn�es de chaque cellule �tant calcul�e sur un X et Y = 0 de base

Select Case aBlock.AlignementLeft
    Case 2 '-> Marge gauche
        PosX = Sortie.ScaleX(aSpool.maquette.MargeLeft, 7, 3)
    Case 3 '-> Centr�
        PosX = Sortie.ScaleX((aSpool.maquette.Largeur - aBlock.Largeur) / 2, 7, 3)
    Case 4 '-> Marge droite
        PosX = Sortie.ScaleX(aSpool.maquette.Largeur - aBlock.Largeur - aSpool.maquette.MargeLeft, 7, 3)
    Case 5 '-> Sp�cifi�
        PosX = Sortie.ScaleX(aBlock.Left, 7, 3)
End Select


If aBlock.AlignementTop = 5 And Ligne <> 1 Then
Else
    Select Case aBlock.AlignementTop
        Case 1 '-> Libre
            '-> RAS le pointeur est bien positionn�
        Case 2 '-> Marge haut
            PosY = Sortie.ScaleY(aSpool.maquette.MargeTop, 7, 3) + PositionDebutPage
        Case 3 '-> Centr�
            PosY = Sortie.ScaleY((aSpool.maquette.Hauteur - aBlock.Hauteur) / 2, 7, 3) + PositionDebutPage
        Case 4 '-> Marge Bas
            PosY = PositionDebutPage - Sortie.ScaleY(aSpool.maquette.Hauteur - aBlock.Hauteur, 7, 3)
        Case 5 '-> Sp�cifi�
            PosY = Sortie.ScaleY(aBlock.Top, 7, 3) + PositionDebutPage
    End Select
End If

'-> Impression du block de ligne
HauteurLigPix = DrawBlockHTML(aBlock, PosX, PosY, aSpool, aTb.Nom, Ligne, DataFields)

'If Ligne = aBlock.NbLigne Then PosY = PosY + 1
PrintBlockHTML = PosY + HauteurLigPix
        

End Function

Public Function DrawBlockHTML(ByRef aBlock As Block, ByVal PosX As Long, ByVal PosY As Long, ByRef aSpool As Spool, ByRef NomTb As String, Ligne As Integer, DataFields As String) As Long

'---> Fonction qui dessine une cellule

Dim aRect As RECT
Dim aFt As FormatedCell
Dim PrintSigne As Boolean
Dim alpRtf As lpRtf
Dim Temp As String
Dim FontBalise As String
Dim aCell As Cellule
Dim i As Integer, j As Integer
Dim IsBordureGauche As Boolean
Dim ALignCell As String
Dim ColorCell As String
Dim DrawInRed As Boolean
Dim cvChar As String

Dim x1 As Long, x2 As Long, y1 As Long, y2 As Long

For i = 1 To aBlock.NbCol
    '-> Pointer sur la cellule � dessiner
    Set aCell = aBlock.Cellules("L" & Ligne & "C" & i)
    '-> Remplacer les champs par leur valeur
    aCell.ReplaceField DataFields
    '-> Ne pas ecrire en rouge si erreur
    DrawInRed = False
    '-> premi�re cellule mettre une bordure gauiche si necessaire
    If i = 1 Then
        If aCell.BordureGauche Then
            IsBordureGauche = True
        Else
            IsBordureGauche = False
        End If
    Else
        '-> Ne pas mettre de bordure gauche s'il y en a une car elle sera mise � droite sur la pr�cedente
        IsBordureGauche = False
    End If
        
    '-> Positionnement du rectangle de sortie
    x1 = aCell.x1 + PosX
    x2 = aCell.x2 + PosX
    y1 = PosY
    y2 = aCell.y2 + PosY - aCell.y1
    
    aRect.Left = x1 + Decalage
    aRect.Top = y1 + Decalage
    aRect.Right = aCell.x2 - aCell.x1
    aRect.Bottom = aCell.y2 - aCell.y1
    
    Temp = "LEFT:" & aRect.Left & "px;TOP:" & aRect.Top & "px;WIDTH:" & aRect.Right & "px;HEIGHT:" & aRect.Bottom & "px"
    
    '-> Dessiner le contour de la cellule
    If aCell.BordureHaut Then Temp = Temp & ";BORDER-TOP:1pt solid"
    If IsBordureGauche Then Temp = Temp & ";BORDER-LEFT:1pt solid"
    If aCell.BordureBas Then Temp = Temp & ";BORDER-BOTTOM:1pt solid"
    If aCell.BordureDroite Then Temp = Temp & ";BORDER-RIGHT:1pt solid"
        
    '-> Gestion des formats num�rics
    Select Case aCell.TypeValeur
        Case 0 '-> RAS
            PrintSigne = False
        Case 1 '-> Caract�re
            '-> Faire un susbstring
            aCell.ContenuCell = Mid$(aCell.ContenuCell, 1, CInt(aCell.Msk))
            PrintSigne = False
        Case 2 '-> Numeric
            '-> V�rifier s'il y a quelquechose � formater
            If Trim(aCell.ContenuCell) = "" Then
                PrintSigne = False
            Else
                aFt = FormatNumTP(Trim(aCell.ContenuCell), aCell.Msk, aBlock.Nom, "L" & aCell.Ligne & "C" & aCell.Colonne)
                '-> Rajouter Trois blancs au contenu de la cellule
                aFt.strFormatted = aFt.strFormatted
                If aFt.Ok Then
                    '-> Mettre e rouge si necessaire
                    If aFt.Value < 0 And aFt.idNegatif > 1 Then DrawInRed = True
                    '-> affecter le contenu de la cellule
                    aCell.ContenuCell = aFt.strFormatted
                    '-> Si c'est un 0 mettre � blanc si pas imprimer
                    If CDbl(aCell.ContenuCell) = 0 And Not (aCell.PrintZero) Then
                        aCell.ContenuCell = ""
                    Else
                        If aFt.Value < 0 Then
                            If aFt.idNegatif = 0 Or aFt.idNegatif = 3 Then
                                aCell.ContenuCell = "- " & aCell.ContenuCell
                                PrintSigne = False
                            ElseIf aFt.idNegatif = 1 Or aFt.idNegatif = 4 Then
                                PrintSigne = True
                            End If
                        End If
                    End If
                Else
                    '-> Ne pas imprimer  de signe
                    PrintSigne = False
                End If
            End If
    End Select
    
    '-> si on doi imprimer le signe
    If PrintSigne Then aCell.ContenuCell = "- " & aCell.ContenuCell
    
    '-> Gestion des alignements
    Select Case aCell.CellAlign
        Case 1, 4, 7
            ALignCell = "ALIGN=LEFT "
        Case 2, 5, 8
            ALignCell = "ALIGN=CENTER "
        Case 3, 6, 9
            ALignCell = "ALIGN=RIGHT "
    End Select
    
    '-> Appliquer les options de font au format RTF
    alpRtf.FontName = aCell.FontName
    alpRtf.FontSize = aCell.FontSize
    alpRtf.FontBold = aCell.FontBold
    alpRtf.FontItalic = aCell.FontItalic
    alpRtf.FontUnderline = aCell.FontUnderline
    If Not DrawInRed Then
        alpRtf.FontColor = aCell.FontColor
    Else
        alpRtf.FontColor = rgb(255, 0, 0)
    End If
    
    FontBalise = GetFontFromlpRtf(alpRtf)

    If aCell.FondTransparent Then
        '-> Fond transparent
        Print #hdlFile, "<DIV " & ALignCell & "Style=""" & Temp & ";POSITION:ABSOLUTE""></DIV>"
    Else
        '-> Imprimer la balise
        Print #hdlFile, "<DIV " & ALignCell & "Style=""BACKGROUND-COLOR:#" & ConvertColor(aCell.BackColor) & ";" & Temp & ";POSITION:ABSOLUTE""></DIV>"
    End If
    
    '-> Balise de font si necessaire
    If Trim(aCell.ContenuCell) <> "" Then
        '-> Mettre � blanc
        cvChar = ""
        '-> Remplacer les caract�res
        For j = 1 To Len(aCell.ContenuCell)
            cvChar = cvChar & ConvertChar(Asc(Mid$(aCell.ContenuCell, j, 1)))
        Next
        
        Print #hdlFile, "<DIV " & ALignCell & "Style=""" & Temp & ";POSITION:ABSOLUTE"">" & FontBalise & cvChar & "</FONT></DIV>"
    End If
Next 'Pour toutes les colonnes

DrawBlockHTML = aCell.y2 - aCell.y1

Exit Function


End Function

Private Function CreateSommaire1() As Boolean

'---> Cette proc�dure cr�er un fichier sommaire
Dim hdlHtml As Integer
Dim Ligne As String
Dim Fichier As String
Dim IsLoadingFile As Boolean
Dim NomFichier As String
Dim NomSpool As String
Dim aFichier As Fichier
Dim SpoolToPrint As Spool
Dim PrintSpool As Boolean
Dim i As Integer
Dim Res As Long

'-> Gestion des erreurs
On Error GoTo GestError

'-> Faire un compteur
Res = GetTickCount()

'-> Pointer sur l'objet fichier
Set aFichier = Fichiers(UCase$(Trim(FileKeyExport)))

'-> R�cup�rer le nom du fichier
NomFichier = GetSpoolName(aFichier.FileName)

'-> Ouvrture du FrameSet de Destination
hdlHtml = FreeFile
Open PathToExport & FileToExport For Output As #hdlHtml

'-> Ecrire le FrameSet
Print #hdlHtml, "<HTML>"

Print #hdlHtml, "<FRAMESET COlS=""200px,*"">"
Print #hdlHtml, "<FRAME NORESIZE=NORESIZE  NAME=SPOOLS SRC="".\" & Replace(NavigaSpoolHtml, "$CHRONO$", Res) & """>"
Print #hdlHtml, "<FRAMESET ROWS=""80px,*"">"
Print #hdlHtml, "<FRAME NORESIZE=NORESIZE  NAME=NAVIGA SCROLLING=NO SRC="".\" & Replace(NavigaBarHtmlFile, "$CHRONO$", Res) & """ >"
Print #hdlHtml, "<FRAME NORESIZE=NORESIZE  NAME=TURBO >"
Print #hdlHtml, "</FRAMESET>"
Print #hdlHtml, "</FRAMESET>"
Print #hdlHtml, "</HTML>"

'-> Fermer le fichier
Close #hdlHtml

'-> Ajouter ce fichier � la liste des �l�ments
AddToFileHtml PathToExport & FileToExport

'-> Cr�er la feuille HTML de navigation des spools
hdlFile = FreeFile
Open PathToExport & Replace(NavigaSpoolHtml, "$CHRONO$", Res) For Output As #hdlFile

'-> Impression de l'ent�te HTML
Print #hdlFile, "<HTML>"
Print #hdlFile, "<SCRIPT LANGUAGE=JAVASCRIPT>"
Print #hdlFile, "function loadspool ( myspool )"
Print #hdlFile, "{"
Print #hdlFile, "parent.NAVIGA.document.forms[0].FICHIER.value =  myspool ;"
Print #hdlFile, "parent.TURBO.location.href= myspool + String(1) + "".html"" ;"
Print #hdlFile, "}"
Print #hdlFile, "</SCRIPT>"
Print #hdlFile, "<BODY>"
Print #hdlFile, "<TABLE>"

i = 1
'-> Imprimer autant de balise IMG qu'il y a de spools
For Each SpoolToPrint In aFichier.Spools
    If ExportType = 1 Then '-> Export Spool
        '-> Tester la cl� du spool
        If SpoolKeyExport = SpoolToPrint.Key Then
            PrintSpool = True
            NomSpool = "sp" & SpoolToPrint.Num_Spool
            PageMin = 1
            PageMax = SpoolToPrint.nbPage
        Else
            PrintSpool = False
        End If
    ElseIf ExportType = 2 Then 'Export Fichier
        '-> On est en multispool ; tout imprimer
        PrintSpool = True
        If i = 1 Then
            NomSpool = "sp" & SpoolToPrint.Num_Spool
            PageMin = 1
            PageMax = SpoolToPrint.nbPage
        End If
    Else 'Export Page Simple
        '-> R�cup�rer le nom du spool
        NomSpool = "sp" & Entry(2, SpoolKeyExport, "|")
        PageMin = PageToExport
        PageMax = PageToExport
        If SpoolKeyExport = SpoolToPrint.Key Then
            PrintSpool = True
        Else
            PrintSpool = False
        End If
    End If
    i = i + 1
    '-> Imprimer l'image du spool
    If PrintSpool Then
        If IncluseSpoolNumber Then
            Print #hdlFile, "<TR><TD ALIGN=CENTER WIDTH=""150px"" HEIGHT=""50px"" BACKGROUND="".\nav_spool.gif""><A HREF=javascript:loadspool(""" & NomFichier & "_sp" & SpoolToPrint.Num_Spool & "_page"") ><FONT COLOR=yellow>Spool N� : " & SpoolToPrint.Num_Spool & "</FONT></A></TD></TR>"
        Else
            Print #hdlFile, "<TR><TD ALIGN=CENTER WIDTH=""150px"" HEIGHT=""50px"" BACKGROUND="".\nav_spool.gif""><A HREF=javascript:loadspool(""" & NomFichier & "_page"") ><FONT COLOR=yellow>Spool N� : " & SpoolToPrint.Num_Spool & "</FONT></A></TD></TR>"
        End If
    End If
Next 'Pour tous les spools

'-> Fermer le fichier
Print #hdlFile, "</TABLE></BODY></HTML>"
Close #hdlFile

'-> Ajouter le fichier � la ,liste des fichiers � copier
AddToFileHtml PathToExport & Replace(NavigaSpoolHtml, "$CHRONO$", Res)

'-> Transf�rer la palette de navigation dans le r�pertyoire du client
FileCopy App.Path & "\internet\" & Replace(NavigaBarHtmlFile, "$CHRONO$", ""), PathToExport & Replace(NavigaBarHtmlFile, "$CHRONO$", Res)

'-> Ajouter ce fichier � la liste des fichiers HTMLS
AddToFileHtml PathToExport & Replace(NavigaBarHtmlFile, "$CHRONO$", Res)

'-> Mettre � jour le fichier la palette de naviagation
hdlFile = FreeFile
Open PathToExport & Replace(NavigaBarHtmlFile, "$CHRONO$", Res) For Input As #hdlFile

'-> D�terminer ici le num�ro du spool � initialiser
' si multispool -> sp1
' si spool en cours -> num du spool
' si page en cours -> num du spool

Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    
    '-> Traitement des fichiers � copier
    If Trim(UCase$(Ligne)) = "[FILETOLOAD]" Then
        '-> Indiquer que l'on va lire les fichiers � transf�rer
        IsLoadingFile = True
        '-> Aller � la ligne suivante
        GoTo NextLigne
    End If
    
    If IsLoadingFile Then
        '-> Tester si on atteint la fin de la liste des fichiers � copier
        If UCase$(Ligne) = "[/FILETOLOAD]" Then
            '-> Indiquer que l'on a fini la lecture des fichiers
            IsLoadingFile = False
            '-> Aller � la ligne suivante
            GoTo NextLigne
        End If
    
        '-> Nom d'un fichier � copier
        If Trim(Ligne) = "" Then GoTo NextLigne
        '-> V�rifier si le fichier existe
        If Dir$(App.Path & "\internet\" & Trim(Ligne), vbNormal) <> "" Then
            '-> Faire une copie du fichier
            CopyFile App.Path & "\internet\" & Trim(Ligne), PathToExport & Trim(Ligne), 0
            '-> Ajouter � la liste des fichiers
            AddToFileHtml PathToExport & Trim(Ligne)
        End If
    Else
        If InStr(1, Ligne, "%NOMFICHIER%") <> 0 Then
            If IncluseSpoolNumber Then
                Ligne = Replace(Ligne, "%NOMFICHIER%", NomFichier & "_" & NomSpool)
            Else
                Ligne = Replace(Ligne, "%NOMFICHIER%", NomFichier)
            End If
        End If
            
        If InStr(1, Ligne, "%PAGECOURS%") <> 0 Then _
            Ligne = Replace(Ligne, "%PAGECOURS%", PageMin)
    
        If InStr(1, Ligne, "%PAGEMIN%") <> 0 Then _
            Ligne = Replace(Ligne, "%PAGEMIN%", PageMin)
    
        If InStr(1, Ligne, "%PAGEMAX%") <> 0 Then _
            Ligne = Replace(Ligne, "%PAGEMAX%", PageMax)
            
        Fichier = Fichier & Chr(13) & Chr(10) & Ligne
    End If
NextLigne:
Loop

'-> Fermer le fichier
Close #hdlFile

'-> Le r�ouvrir
hdlFile = FreeFile
Open PathToExport & Replace(NavigaBarHtmlFile, "$CHRONO$", Res) For Output As #hdlFile

'-> Imprimer le fichier
Print #hdlFile, Fichier

'-> Fermer le fichier
Close #hdlFile

'-> Renvoyer une valeur de succ�s
CreateSommaire1 = True

Exit Function

GestError:

    If Err.Number = 75 Then
        Resume Next
    Else
        CreateSommaire1 = False
        Reset
    End If

End Function

Private Sub AddToFileHtml(FileName As String)

If Trim(ListeHtmlFile) = "" Then
    ListeHtmlFile = FileName
Else
    ListeHtmlFile = ListeHtmlFile & "|" & FileName
End If

End Sub

Public Function ConvertChar(intChar As Integer) As String

'---> Cette fonction convertit un caract�re interdit en caract�re  HTML

Select Case intChar

    Case 32 ' Espace
        ConvertChar = "&nbsp;"
    Case 163 '�
        ConvertChar = "&pound;"
    Case 164 '�
        ConvertChar = "&curren;"
    Case 167 '�
        ConvertChar = " &sect;"
    Case 176 '�
        ConvertChar = "&deg;"
    Case 181 '�
        ConvertChar = "&micro;"
    Case 192 '�
        ConvertChar = "&Agrave;"
    Case 193 '�
        ConvertChar = "&Aacute;"
    Case 194 '�
        ConvertChar = "&Acirc;"
    Case 195 '�
        ConvertChar = "&Atilde;"
    Case 196 '�
        ConvertChar = "&Auml"
    Case 198 '�
        ConvertChar = "&AElig;"
    Case 200 '�
        ConvertChar = "&Egrave;"
    Case 201 '�
        ConvertChar = "&Eacute;"
    Case 202 '�
        ConvertChar = "&Ecirc;"
    Case 203 '�
        ConvertChar = "&Euml;"
    Case 210 '�
        ConvertChar = "&Ograve;"
    Case 211 '�
        ConvertChar = "&Oacute;"
    Case 212 '�
        ConvertChar = "&Ocirc;"
    Case 213 '�
        ConvertChar = "&Otilde;"
    Case 214 '�
        ConvertChar = "&Ouml;"
    Case 216 '�
        ConvertChar = "&Oslash;"
    Case 217 '�
        ConvertChar = "&Ugrave;"
    Case 218 '�
        ConvertChar = "&Uacute;"
    Case 219 '�
        ConvertChar = "&Ucirc;"
    Case 220 '�
        ConvertChar = "&Uuml;"
    Case 224 '�
        ConvertChar = "&agrave;"
    Case 225 '�
        ConvertChar = "&aacute;"
    Case 226 '�
        ConvertChar = "&acirc;"
    Case 227 '�
        ConvertChar = "&atilde;"
    Case 228 '�
        ConvertChar = "&auml;"
    Case 229 '�
        ConvertChar = "&aring;"
    Case 230 '�
        ConvertChar = "&aelig;"
    Case 231 '�
        ConvertChar = "&ccedil;"
    Case 232 '�
        ConvertChar = "&egrave;"
    Case 233 '�
        ConvertChar = "&eacute;"
    Case 234 '�
        ConvertChar = "&ecirc;"
    Case 235 '�
        ConvertChar = "&euml"
    Case 242 '�
        ConvertChar = "&ograve;"
    Case 243 '�
        ConvertChar = "&oacute;"
    Case 244 '�
        ConvertChar = "&ocirc;"
    Case 245 '�
        ConvertChar = "&otilde;"
    Case 246 '�
        ConvertChar = "&ouml;"
    Case 248 '�
        ConvertChar = "&oslash;"
    Case 249 '�
        ConvertChar = "&ugrave;"
    Case 250 '�
        ConvertChar = "&uacute;"
    Case 251 '�
        ConvertChar = "&ucirc;"
    Case 252 '�
        ConvertChar = "&uuml"
    Case Else
        ConvertChar = Chr(intChar)
End Select

End Function

Public Function GetSpoolName(FileName As String) As String

'-> Cette fonction retourne le nom du fichier sans l'extension

Dim i As Integer
Dim Temp As String

On Error GoTo GestError

Temp = Entry(NumEntries(FileName, "\"), FileName, "\")
i = InStrRev(Temp, ".")
If i <> 1 Then
    Temp = Mid$(Temp, 1, i - 1)
End If

If NumEntries(Temp, "/") <> 1 Then
    Temp = Entry(NumEntries(Temp, "/"), Temp, "/") + ".turbo"
    i = InStrRev(Temp, ".")
    If i <> 1 Then
        Temp = Mid$(Temp, 1, i - 1)
    End If
End If

GetSpoolName = Temp

Exit Function

GestError:

    GetSpoolName = FileName

End Function

Public Function GetFileName(MyFile As String) As String

Dim i As Integer
MyFile = Replace(MyFile, "/", "\")
i = InStrRev(MyFile, "\")
If i = 0 Then
    GetFileName = MyFile
Else
    GetFileName = Mid$(MyFile, i + 1, Len(MyFile) - i)
End If


End Function

