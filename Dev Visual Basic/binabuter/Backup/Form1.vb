Option Strict On

Imports System.IO

Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnYes As System.Windows.Forms.Button
    Friend WithEvents btnNo As System.Windows.Forms.Button
    Friend WithEvents lblQuestion As System.Windows.Forms.Label
    Friend WithEvents btnCrash As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnYes = New System.Windows.Forms.Button
        Me.lblQuestion = New System.Windows.Forms.Label
        Me.btnNo = New System.Windows.Forms.Button
        Me.btnCrash = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'btnYes
        '
        Me.btnYes.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnYes.Location = New System.Drawing.Point(56, 80)
        Me.btnYes.Name = "btnYes"
        Me.btnYes.Size = New System.Drawing.Size(176, 48)
        Me.btnYes.TabIndex = 0
        Me.btnYes.Text = "Yes !"
        '
        'lblQuestion
        '
        Me.lblQuestion.BackColor = System.Drawing.SystemColors.Info
        Me.lblQuestion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblQuestion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQuestion.Location = New System.Drawing.Point(32, 16)
        Me.lblQuestion.Name = "lblQuestion"
        Me.lblQuestion.Size = New System.Drawing.Size(224, 56)
        Me.lblQuestion.TabIndex = 1
        Me.lblQuestion.Text = "Have you ever seen a dummy application?"
        Me.lblQuestion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnNo
        '
        Me.btnNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNo.Location = New System.Drawing.Point(56, 136)
        Me.btnNo.Name = "btnNo"
        Me.btnNo.Size = New System.Drawing.Size(176, 48)
        Me.btnNo.TabIndex = 2
        Me.btnNo.Text = "No !"
        '
        'btnCrash
        '
        Me.btnCrash.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCrash.Location = New System.Drawing.Point(56, 192)
        Me.btnCrash.Name = "btnCrash"
        Me.btnCrash.Size = New System.Drawing.Size(176, 48)
        Me.btnCrash.TabIndex = 3
        Me.btnCrash.Text = "Make me crash!"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 246)
        Me.Controls.Add(Me.btnCrash)
        Me.Controls.Add(Me.btnNo)
        Me.Controls.Add(Me.lblQuestion)
        Me.Controls.Add(Me.btnYes)
        Me.Name = "Form1"
        Me.Text = "UTMag Demo"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNo.Click
        MessageBox.Show("So here is your first one!", _
                        "UTMag demo", _
                        MessageBoxButtons.OK, _
                        MessageBoxIcon.Exclamation)
    End Sub

    Private Sub btnYes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnYes.Click
        MessageBox.Show("So here is another one!", _
                        "UTMag demo", _
                        MessageBoxButtons.OK, _
                        MessageBoxIcon.Exclamation)
    End Sub

    Private Sub btnCrash_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCrash.Click
        File.Copy(Application.StartupPath & "\test.txt", "c:\temp\test.txt", True)
        MessageBox.Show("File has been copied successfully", "File copy", _
                        MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
End Class
