VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form PontBascule 
   BorderStyle     =   0  'None
   Caption         =   "Deal Informatique"
   ClientHeight    =   4305
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5535
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   Moveable        =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   4305
   ScaleWidth      =   5535
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   4200
      Top             =   3000
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Timer Timer1 
      Interval        =   500
      Left            =   1320
      Top             =   2880
   End
   Begin MSCommLib.MSComm MSComm1 
      Left            =   4800
      Top             =   3000
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      CommPort        =   3
      DTREnable       =   -1  'True
      RThreshold      =   1
      RTSEnable       =   -1  'True
      BaudRate        =   1200
      ParitySetting   =   2
      DataBits        =   7
      SThreshold      =   1
   End
   Begin VB.Label Label5 
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   3480
      Width           =   4695
   End
   Begin VB.Label Label4 
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   3840
      Width           =   495
   End
   Begin VB.Label Label3 
      Caption         =   "Quitter"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      MousePointer    =   1  'Arrow
      TabIndex        =   2
      Top             =   3840
      Width           =   975
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   3000
      Width           =   5295
      WordWrap        =   -1  'True
   End
   Begin VB.Image Image1 
      Height          =   2115
      Left            =   120
      Picture         =   "PontBascule.frx":0000
      Stretch         =   -1  'True
      Top             =   120
      Width           =   5295
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Waiting data..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   2400
      Width           =   5295
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "PontBascule"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)
Private Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Private Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long
Private Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
'-> API de temporisation
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Dim sBuffer
Public strFile As String
Public strEthernet As String
Public strEthernetPort As String

Dim hdlFile As Integer
Dim TempFileName As String
Dim topLecture As Boolean
Public protocole As String
Public strTempo As String
Public ReponseFaite As Boolean

Private Function GetTempFileNameVB(ByVal Id As String, Optional Rep As Boolean) As String

Dim TempPath As String
Dim lpBuffer As String
Dim result As Long
Dim TempFileName As String

'---> Fonction qui d�termine un nom de fichier tempo sous windows

'-> Recherche du r�pertoire temporaire
lpBuffer = Space$(500)
result = GetTempPath(Len(lpBuffer), lpBuffer)
TempPath = Mid$(lpBuffer, 1, result)

'-> Si on ne demande que le r�pertoire de windows
If Rep Then
    GetTempFileNameVB = TempPath
    Exit Function
End If

'-> Cr�ation d'un nom de fichier
TempFileName = Space$(1000)
result = GetTempFileName(TempPath, Id, 0, TempFileName)
TempFileName = Entry(1, TempFileName, Chr(0))

GetTempFileNameVB = TempFileName

End Function

Private Sub Form_Load()
Init
End Sub

Public Sub Init()
    Dim cmdline As String
    Dim comport As Integer
    Dim setg As String
    Dim Settings
    Dim CommPort
    Dim Handshaking
    Dim Echo
    Dim EOFEnable
    
    On Error GoTo GestError
    '-> on cr�e le fichier temporaire de travail
    TempFileName = GetTempFileNameVB("TMP")
    hdlFile = FreeFile
    Settings = GetSetting(App.Title, "Properties", "Settings", "") ' frmTerminal.MSComm1.Settings]\
    If Settings <> "" Then
        MSComm1.Settings = Settings
        If Err Then
            MsgBox Error$, 48
            Exit Sub
        End If
    End If
    strEthernet = GetSetting(App.Title, "Properties", "Ethernet", "")
    strEthernetPort = GetSetting(App.Title, "Properties", "EthernetPort", "")
    protocole = GetSetting(App.Title, "Properties", "Protocole", "")
    If InStr(1, Command$, "console", vbTextCompare) = 0 Then
        Me.Show
        Me.Visible = True
    End If
        
    If strEthernet <> "" Then  ' frmTerminal.MSComm1.CommPort
        '-> on se connecte au serveur
        Label5.Caption = "Initialize"
        
        'If Winsock1.State <> sckConnected Then
        
            Winsock1.Close
            
            Me.Label5.Caption = "..."
            ReponseFaite = False
            Winsock1.Connect Trim(strEthernet), Val(strEthernetPort)
            
            '-> on met une attente pour les antispam (en principe pas d'attente)
            Sleep 500
            Me.Label5.Caption = "Connect..."
            While ReponseFaite = False
                DoEvents
            Wend
            
            While Winsock1.State <> sckConnected
                DoEvents
            Wend
            
            Me.Label5.Caption = "Connected on " + strEthernet + ":" + strEthernetPort
        'End If
    
    Else
        CommPort = GetSetting(App.Title, "Properties", "CommPort", "") ' frmTerminal.MSComm1.CommPort
        If CommPort <> "" Then MSComm1.CommPort = CommPort
        
        Handshaking = GetSetting(App.Title, "Properties", "Handshaking", "") 'frmTerminal.MSComm1.Handshaking
        If Handshaking <> "" Then
            MSComm1.Handshaking = Handshaking
            If Err Then
                MsgBox Error$, 48
                Exit Sub
            End If
        End If
        
        Echo = GetSetting(App.Title, "Properties", "Echo", "") ' Echo
        
        EOFEnable = GetSetting(App.Title, "Properties", "EOFEnable", "") ' frmTerminal.MSComm1.EOFEnable
        If CommPort <> "" Then MSComm1.EOFEnable = EOFEnable
                
        MSComm1.PortOpen = True
    End If
    strFile = GetSetting(App.Title, "Properties", "File", "")
    If strFile = "" Then strFile = "c:\poids.txt"
    
        
    Open TempFileName For Binary Access Write As hdlFile
    
    Exit Sub
    
GestError:
    
End Sub

Private Sub Label3_Click()
    '-> on ferme le programme
    End

End Sub

Private Sub Label4_Click()
frmProperties.Show
End Sub

Private Sub MSComm1_OnComm()
    Dim receivedbyte As String
    Dim ptr
    Dim EVMsg$
    Dim ERMsg$
    
    Select Case MSComm1.CommEvent
        ' Event messages.
        Case comEvReceive
            receivedbyte = MSComm1.Input
            'Label1.Caption = "->" & StrConv(receivedbyte, vbUnicode)
            If topLecture Then Exit Sub
            sBuffer = sBuffer & StrConv(receivedbyte, vbUnicode)
            If protocole = "Eric" And Len(sBuffer) > 77 Then
                    writePoids
                    sBuffer = ""
            Else
                If InStr(1, StrConv(sBuffer, vbUnicode), Chr(10)) And Len(sBuffer) > 30 Then
                    writePoids
                    sBuffer = ""
                End If
            End If
            Put #hdlFile, , receivedbyte
        Case comEvSend
        Case comEvCTS
            EVMsg$ = "Change in CTS Detected"
        Case comEvDSR
            EVMsg$ = "Change in DSR Detected"
        Case comEvCD
            EVMsg$ = "Change in CD Detected"
        Case comEvRing
            EVMsg$ = "The Phone is Ringing"
        Case comEvEOF
            EVMsg$ = "End of File Detected"

        ' Error messages.
        Case comBreak
            ERMsg$ = "Break Received"
        Case comCDTO
            ERMsg$ = "Carrier Detect Timeout"
        Case comCTSTO
            ERMsg$ = "CTS Timeout"
        Case comDCB
            ERMsg$ = "Error retrieving DCB"
        Case comDSRTO
            ERMsg$ = "DSR Timeout"
        Case comFrame
            ERMsg$ = "Framing Error"
        Case comOverrun
            ERMsg$ = "Overrun Error"
        Case comRxOver
            ERMsg$ = "Receive Buffer Overflow"
        Case comRxParity
            ERMsg$ = "Parity Error"
        Case comTxFull
            ERMsg$ = "Transmit Buffer Full"
        Case Else
            ERMsg$ = "Unknown error or event"
    End Select
    
    If Len(EVMsg$) Then
        Me.Label5.Caption = EVMsg$
        Sleep 2000
        Me.Label5.Caption = ""
        ' Enable timer so that the message in the status bar
        ' is cleared after 2 seconds
        
    ElseIf Len(ERMsg$) Then
        Me.Label5.Caption = ERMsg$
        Sleep 2000
        Me.Label5.Caption = ""
        Beep
        'Dim Ret
        'Ret = MsgBox(ERMsg$, 1, "Click Cancel to quit, OK to ignore.")
        
        ' If the user clicks Cancel (2)...
        'If Ret = 2 Then
        '    MSComm1.PortOpen = False    ' Close the port and quit.
        'End If
        
        ' Enable timer so that the message in the status bar
        ' is cleared after 2 seconds
        
    End If
End Sub

Private Sub writePoids()
    Dim strTemp As String
    Dim strCle As String
    Dim i As Integer
    Dim startBloc As Boolean
    Dim sBloc As String
    Dim hdlFile2 As Integer
    Dim ValueEnreg As Byte
    sBloc = sBuffer
    SetIniString "POIDS", "DEBUG", strFile, sBloc
    Close hdlFile
    hdlFile = FreeFile
    topLecture = True
    Open TempFileName For Binary As #hdlFile
    
    '-> Lecture s�quentielle du fichier
    Do While Not EOF(hdlFile)
        '-> Get du code de l'enregistrement
        Get #hdlFile, , ValueEnreg
        If ValueEnreg = 13 Then startBloc = True
        If protocole = "Eric" Then
            If startBloc Then
                        'statut
                        Get #hdlFile, , ValueEnreg
                        strTemp = Chr(ValueEnreg)
                        strCle = "STATUT"
                        SetIniString "POIDS", strCle, strFile, strTemp
                        If Trim(strTemp) = "" Then SetIniString "POIDS", strCle, strFile, "INSTABLE"
                        'signe
                        Get #hdlFile, , ValueEnreg
                        strTemp = Chr(ValueEnreg)
                        strCle = "BRUTSIGNE"
                        SetIniString "POIDS", strCle, strFile, strTemp
                        strTemp = ""
                        For i = 1 To 5
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "BRUT"
                        SetIniString "POIDS", strCle, strFile, strTemp
                        strTemp = ""
                        For i = 1 To 1
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "TARESIGNE"
                        SetIniString "POIDS", strCle, strFile, strTemp
                        strTemp = ""
                        For i = 1 To 5
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "TARE"
                        SetIniString "POIDS", strCle, strFile, strTemp
                        strTemp = ""
                        For i = 1 To 1
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "NETSIGNE"
                        SetIniString "POIDS", strCle, strFile, strTemp
                        strTemp = ""
                        For i = 1 To 5
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "NET"
                        SetIniString "POIDS", strCle, strFile, strTemp
                        
                        Me.Label1.Caption = Now
                        Me.Label2.Caption = strTemp
                        
                        strTemp = ""
                        For i = 1 To 6
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "TICKET"
                        SetIniString "POIDS", strCle, strFile, strTemp
                        strTemp = ""
                        For i = 1 To 6
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "DATE"
                        SetIniString "POIDS", strCle, strFile, strTemp
                        strTemp = ""
                        For i = 1 To 6
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "HEURE"
                        SetIniString "POIDS", strCle, strFile, strTemp
                        strTemp = ""
                        For i = 1 To 1
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "CKS"
                        SetIniString "POIDS", strCle, strFile, strTemp
            
            End If
        Else
            '-> Selon le code trait�
            Select Case ValueEnreg
                Case 1
                    '-> c'est le d�but!! SOH
                Case 11
                    '-> protocole maitre
                Case 2
                    '-> STX juste avant chaque debut de bloc
                    startBloc = True
            End Select
            If startBloc Then
                Get #hdlFile, , ValueEnreg
                sBloc = Chr(ValueEnreg)
                Get #hdlFile, , ValueEnreg
                sBloc = sBloc + Chr(ValueEnreg)
                Select Case sBloc
                    Case "01" '-> poids brut sur 7 caracteres (5caracteres puis 0 � 9 puis point)
                        strTemp = ""
                        For i = 1 To 6
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "BRUT"
                    Case "02" '-> poids tare
                        strTemp = ""
                        For i = 1 To 6
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "TARE"
                    Case "03" '-> poids net
                        strTemp = ""
                        For i = 1 To 6
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "NET"
                    Case "04"
                        strTemp = ""
                        For i = 1 To 4
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "STATUT"
                    Case "06" '-> numero ticket
                        strTemp = ""
                        For i = 1 To 8
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "TICKET"
                    Case "98" '-> numero identification dsd
                        strTemp = ""
                        For i = 1 To 2
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "IDSD"
                    Case "99" '-> numero enregistrement dsd
                        strTemp = ""
                        For i = 1 To 6
                            Get #hdlFile, , ValueEnreg
                            strTemp = strTemp + Chr(ValueEnreg)
                        Next
                        strCle = "NDSD"
                    Case Else
                        startBloc = False
                End Select
                '-> on �crit la lecture
                If strCle <> "" And startBloc Then
                    'MsgBox strCle & strTemp & strFile
                    SetIniString "POIDS", strCle, strFile, strTemp
                    If strCle = "NET" Then
                        Me.Label1.Caption = Now
                        Me.Label2.Caption = strTemp
                    End If
                    DoEvents
                    '-> on continue l'analyse
                    startBloc = False
                    strCle = ""
                End If
            End If
        End If
    Loop
    Close hdlFile
    FileCopy TempFileName, TempFileName + ".old"
    Kill TempFileName
    hdlFile = FreeFile
    Open TempFileName For Binary Access Write As hdlFile
    topLecture = False
End Sub

Public Function GetIniString(AppName As String, lpKeyName As String, IniFile As String, Error As Boolean) As String

Dim Res As Long
Dim lpBuffer As String


lpBuffer = Space$(2000)
Res = GetPrivateProfileString(AppName, lpKeyName, "NULL", lpBuffer, Len(lpBuffer), IniFile)
lpBuffer = Entry(1, lpBuffer, Chr(0))
If lpBuffer = "NULL" Then
    If Error Then
        MsgBox "Impossible de trouver la cl� : " & lpKeyName & _
               " dans la section : " & AppName & " du fichier : " & _
                IniFile, vbCritical + vbOKOnly, "Erreur de lecture"
    End If
    GetIniString = ""
Else
    lpBuffer = Mid$(lpBuffer, 1, Res)
    GetIniString = lpBuffer
End If


End Function

Public Function SetIniString(AppName As String, lpKeyName As String, IniFile As String, Value As String)

Dim Res As Long
Dim lpBuffer As String


Res = WritePrivateProfileString(AppName, lpKeyName, Value, IniFile)

End Function

Private Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Integer
Dim PosEnCour As Integer
Dim i As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
End Function

Private Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Integer

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Integer
Dim i As Integer
Dim PosAnalyse As Integer

On Error GoTo GestError

'-< Analyse du vecteur
PosAnalyse = 1
Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1
'-> Quitter la fonction
Exit Function

GestError:
    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
End Function

Private Sub Timer1_Timer()
On Error Resume Next

If protocole = "Eric" And Not topLecture And GetIniString("POIDS", "ASK", strFile, False) = "TRUE" Then
    SetIniString "POIDS", "ASK", strFile, ""
    SetIniString "POIDS", "TARE", strFile, ""
    SetIniString "POIDS", "TICKET", strFile, ""
    SetIniString "POIDS", "BRUT", strFile, ""
    SetIniString "POIDS", "NET", strFile, ""
    SetIniString "POIDS", "STATUT", strFile, ""
    If strEthernet <> "" Then
        If Winsock1.State <> sckConnected Then
            Winsock1.Close
            Me.Label5.Caption = "..."
            ReponseFaite = False
            Winsock1.Connect Trim(strEthernet), Val(strEthernetPort)
            Sleep 500
            Me.Label5.Caption = "Connect..."
            While ReponseFaite = False
                DoEvents
            Wend
            While Winsock1.State <> sckConnected
                DoEvents
            Wend
            Me.Label5.Caption = "Connected on " + strEthernet + ":" + strEthernetPort
        End If
        Winsock1.SendData "I" & vbCrLf
    Else
        MSComm1.Output = "I"
    End If
    DoEvents
End If

If protocole = "Eric" And Not topLecture And GetIniString("POIDS", "ASK", strFile, False) = "RESET" Then
    Unload PontBascule
    Load PontBascule
End If

End Sub

Private Sub Winsock1_DataArrival(ByVal bytesTotal As Long)
'--> ici sont g�r�e les r�ponses du composant winsock
Dim DonneesRecues As String
Dim strMessage As String

On Error GoTo GestError

Winsock1.GetData DonneesRecues
SetIniString "POIDS", "DEBUG", strFile, DonneesRecues
If protocole = "Eric" Then
    If Mid(DonneesRecues, 1, 1) = " " Then
        SetIniString "POIDS", "STATUT", strFile, "INSTABLE"
    Else
        SetIniString "POIDS", "STATUT", strFile, Mid(DonneesRecues, 1, 1)
    End If
    SetIniString "POIDS", "BRUTSIGNE", strFile, Mid(DonneesRecues, 2, 1)
    SetIniString "POIDS", "BRUT", strFile, Mid(DonneesRecues, 3, 5)
    SetIniString "POIDS", "TARESIGNE", strFile, Mid(DonneesRecues, 8, 1)
    SetIniString "POIDS", "TARE", strFile, Mid(DonneesRecues, 9, 5)
    SetIniString "POIDS", "NETSIGNE", strFile, Mid(DonneesRecues, 14, 1)
    SetIniString "POIDS", "NET", strFile, Mid(DonneesRecues, 15, 5)
    
    Me.Label1.Caption = Now
    If IsNumeric(Mid(DonneesRecues, 15, 5)) Then
        Me.Label2.Caption = Mid(DonneesRecues, 15, 5)
    Else
        Me.Label2.Caption = DonneesRecues
    End If
    SetIniString "POIDS", "TICKET", strFile, Mid(DonneesRecues, 20, 6)
    SetIniString "POIDS", "DATE", strFile, Mid(DonneesRecues, 26, 6)
    SetIniString "POIDS", "HEURE", strFile, Mid(DonneesRecues, 32, 6)
    SetIniString "POIDS", "CKS", strFile, Mid(DonneesRecues, 38, 1)
End If

'-> bon on a recu une r�ponse
ReponseFaite = True
Exit Sub
GestError:

End Sub

