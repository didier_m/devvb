Attribute VB_Name = "m_csvEdit"

Private Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
   
Private Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
   
Private Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long

Private Declare Function lstrlen Lib "kernel32" Alias "lstrlenW" (ByVal lpString As Long) As Long

Private Declare Function PathMatchSpec Lib "shlwapi" Alias "PathMatchSpecW" (ByVal pszFileParam As Long, ByVal pszSpec As Long) As Long

Public Declare Function SetFileTime Lib "kernel32" (ByVal hFile As Long, lpcreation As FILETIME, lpLecture As FILETIME, lpLastWriteTime As FILETIME) As Long
Public Declare Function GetFileTime Lib "kernel32" (ByVal hFile As Long, lpCreationTime As FILETIME, lpLastAccessTime As FILETIME, lpLastWriteTime As FILETIME) As Long

Private Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, ByVal NoSecurity As Long, ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long

Private Declare Function SetFileCreatedTime Lib "kernel32" Alias "SetFileTime" (ByVal hFile As Long, lpCreationTime As FILETIME, ByVal NullLastAccessTime As Long, ByVal NullLastWriteTime As Long) As Long
Private Declare Function SetFileAccessTime Lib "kernel32" Alias "SetFileTime" (ByVal hFile As Long, ByVal NullCreationTime As Long, lpLastAccessTime As FILETIME, ByVal NullWriteTime As Long) As Long
Private Declare Function SetFileModifiedTime Lib "kernel32" Alias "SetFileTime" (ByVal hFile As Long, ByVal NullCreationTime As Long, ByVal NullLastAccessTime As Long, lpLastWriteTime As FILETIME) As Long

Public Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle&, ByVal dwMilliseconds&) As Long

Public Declare Function FindFirstChangeNotification Lib "kernel32" Alias "FindFirstChangeNotificationA" _
                                                        (ByVal lpPathName$, ByVal bWatchSubTree&, ByVal dwNotifyFilter&) As Long
                                                        
Public Declare Function FindNextChangeNotification Lib "kernel32" (ByVal hChangeHandle&) As Long
Public Declare Function FindCloseChangeNotification Lib "kernel32" (ByVal hChangeHandle&) As Long

Public Declare Function CompareFileTime Lib "kernel32" (lpFileTime1 As FILETIME, lpFileTime2 As FILETIME) As Long

Public Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

Private Declare Sub ExitProcess Lib "kernel32" (ByVal uExitCode As Long)
Public Declare Function SetConsoleCursorPosition Lib "kernel32" (ByVal hConsoleOutput As Long, dwCursorPosition As COORD) As Long
Public Declare Function FindExecutable Lib "shell32.dll" Alias "FindExecutableA" (ByVal lpFile As String, ByVal lpDirectory As String, ByVal lpResult As String) As Long
Public Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Public Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal Hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

Public Type FILETIME
    dwLowDateTime As Long
    dwHighDateTime As Long
End Type

Private Const GENERIC_READ = &H80000000
Private Const GENERIC_WRITE = &H40000000
Private Const FILE_SHARE_READ = &H1
Private Const FILE_SHARE_WRITE = &H2
Private Const OPEN_EXISTING = 3

Public Enum NOTIFY_CHANGE_TYPE
  cwdName = 1&
  cwdSize = 2&
  cwdLastWrite = 4&
  cwdDirName = 8&
End Enum


Private Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Private Declare Function FileTimeToLocalFileTime Lib "kernel32" (lpFileTime As FILETIME, lpLocalFileTime As FILETIME) As Long
Private Declare Function LocalFileTimeToFileTime Lib "kernel32" (lpLocalFileTime As FILETIME, lpFileTime As FILETIME) As Long
Private Declare Function FileTimeToSystemTime Lib "kernel32" (lpFileTime As FILETIME, lpSystemTime As SYSTEMTIME) As Long
Private Declare Function SystemTimeToFileTime Lib "kernel32" (lpSystemTime As SYSTEMTIME, lpFileTime As FILETIME) As Long

Private fp As FILE_PARAMS
Private stopSearch As Boolean

Public Enum pTypeLigne
    pEntete
    pColumnHeader
    pData
    pSeparateur
End Enum

Public Type WIN32_FIND_DATA
   dwFileAttributes As Long
   ftCreationTime As FILETIME
   ftLastAccessTime As FILETIME
   ftLastWriteTime As FILETIME
   nFileSizeHigh As Long
   nFileSizeLow As Long
   dwReserved0 As Long
   dwReserved1 As Long
   cFileName As String * 260
   cAlternate As String * 14
End Type

Public Type FILE_PARAMS
   bRecurse As Boolean
   bFindOrExclude As Long
   nCount As Long
   nSearched As Long
   sFileNameExt As String
   sFileRoot As String
End Type

Private Declare Function MultiByteToWideChar Lib "kernel32.dll" ( _
    ByVal CodePage As Long, _
    ByVal dwFlags As Long, _
    ByVal lpMultiByteStr As Long, _
    ByVal cbMultiByte As Long, _
    ByVal lpWideCharStr As Long, _
    ByVal cchWideChar As Long _
) As Long

Private Declare Function WideCharToMultiByte Lib "kernel32.dll" ( _
    ByVal CodePage As Long, _
    ByVal dwFlags As Long, _
    ByVal lpWideCharStr As Long, _
    ByVal cchWideChar As Long, _
    ByVal lpMultiByteStr As Long, _
    ByVal cbMultiByte As Long, _
    ByVal lpDefaultChar As Long, _
    ByVal lpUsedDefaultChar As Long _
) As Long

Public Const CP_ACP        As Long = 1252          ' Default ANSI code page.
Public Const CP_UTF8       As Long = 65001      ' UTF8.
Public Const CP_UTF16_LE   As Long = 1200       ' UTF16 - little endian.
Public Const CP_UTF16_BE   As Long = 1201       ' UTF16 - big endian.
Public Const CP_UTF32_LE   As Long = 12000      ' UTF32 - little endian.
Public Const CP_UTF32_BE   As Long = 12001      ' UTF32 - big endian.

Public Declare Function GetStdHandle Lib "kernel32" _
(ByVal nStdHandle As Long) As Long

Private Declare Function WriteFile Lib "kernel32" _
(ByVal hFile As Long, _
lpBuffer As Any, _
ByVal nNumberOfBytesToWrite As Long, _
lpNumberOfBytesWritten As Long, _
lpOverlapped As Any) As Long

Public Const STD_OUTPUT_HANDLE = -11&

Private Type COORD
        X As Integer
        y As Integer
End Type

Private Type SMALL_RECT
        Left As Integer
        Top As Integer
        Right As Integer
        Bottom As Integer
End Type

Private Type CONSOLE_SCREEN_BUFFER_INFO
        dwSize As COORD
        dwCursorPosition As COORD
        wAttributes As Integer
        srWindow As SMALL_RECT
        dwMaximumWindowSize As COORD
End Type

Private m_audtDirContents() As WIN32_FIND_DATA

Private Declare Function GetConsoleScreenBufferInfo Lib "kernel32" _
(ByVal hConsoleOutput As Long, _
lpConsoleScreenBufferInfo As CONSOLE_SCREEN_BUFFER_INFO) As Long

Private Declare Function SetConsoleTextAttribute Lib "kernel32" _
(ByVal hConsoleOutput As Long, ByVal wAttributes As Long) As Long

Public Declare Function CopyFile& Lib "kernel32" Alias "CopyFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Long)

Private Const FOREGROUND_BLUE = &H1     '  text color contains blue.
Private Const FOREGROUND_GREEN = &H2     '  text color contains green.
Private Const FOREGROUND_INTENSITY = &H8     '  text color is intensified.
Private Const FOREGROUND_RED = &H4     '  text color contains red.
Private hOutput             As Long
Private Const vbBackslash = "\"
Private Const vbSlash = "/"

Private Const ALL_FILES = "*.*"

Public isUnix As Boolean
Public envSlash As String
Public fList As String
Public quiet As Boolean
Public keepdate As Boolean
Public recursive As Boolean
Public version As Boolean
Public help As Boolean
Public ldir As String
Public lfile As String
Public Convert As String
Public abom As Boolean
Public watcher As Boolean
Public exitproc As Boolean
Public m_udtFT As FILETIME

Private Const FILE_NOTIFY_CHANGE_ATTRIBUTES = &H4
Private Const FILE_NOTIFY_CHANGE_DIR_NAME = &H2
Private Const FILE_NOTIFY_CHANGE_FILE_NAME = &H1
Private Const FILE_NOTIFY_CHANGE_LAST_WRITE = &H10
Private Const FILE_NOTIFY_CHANGE_SECURITY = &H100
Private Const FILE_NOTIFY_CHANGE_SIZE = &H8
Private Const INVALID_HANDLE_VALUE = -1&

Public cD As FILETIME
Public aD As FILETIME
Public mD As FILETIME



Public Sub Main()
    Dim NomFichier As String
    Dim Extension As String
    Dim strBuffer As String
    Dim lngResult As Long
    Dim TempFile As String
    Dim fileName As String
                      
    NomFichier = Command$
    Dim cD As Date
    Dim aD As Date
    Dim mD As Date
    
    If Mid(NomFichier, 1, 1) = """" Then
        NomFichier = Mid(NomFichier, 2, Len(NomFichier) - 2)
    End If
    
    TempFileName = GetTempFileNameVB("XLS")

    CopyFile TempFileName, TempFileName & ".xlsx", 0
    strBuffer = Space$(260)
    lngResult = FindExecutable(TempFileName & ".xlsx", "", strBuffer)
    Kill TempFileName & ".xlsx"
    Kill TempFileName
            
    If Len(Trim(strBuffer)) > 6 Then
        '-> Dans le cas des fichiers utf8 sans bom, si l'application associ�e est excel on duplique et rajoute bom
        GetFileTimes NomFichier, cD, aD, mD, False
        NomFichier = convertToUtf8(NomFichier)
        SetFileTimes NomFichier, cD, aD, mD, False
        If strBuffer <> "" And InStr(1, strBuffer, "turbograph", vbTextCompare) = 0 Then
            '-> on lance l'application associ�e
            strBuffer = Left(strBuffer, Len(strBuffer) - Len(Dir(strBuffer)))
            strBuffer = Left$(strBuffer, InStr(strBuffer, Chr$(0)) - 1)
            Shell Trim(strBuffer) & " " & Chr(34) & NomFichier & Chr(34)
            End
        Else
            ShellExecute 0, "Open", NomFichier, vbNullString, "", 1
        End If
    Else
        '-> si pas excel on ouvre avec l'application associ�e
        ShellExecute 0, "Open", NomFichier, vbNullString, "", 1
        End
    End If
    
End Sub

Public Function convertToUtf8(sFile As String) As String
    Dim nCodePage                   As Long
    Dim bContainedBOM               As Boolean
    Dim sFileContents               As String

    On Error GoTo GestError
    '--> cette fonction converti le fichier au format utf8 en conservant sa date
    sFileContents = GetContents(sFile, nCodePage, , bContainedBOM)
    If nCodePage = 65001 And bContainedBOM = False Then
        Kill sFile
        PutContents sFile, sFileContents, 65001, True
        convertToUtf8 = sFile
        Exit Function
    End If
        
GestError:
    convertToUtf8 = sFile
End Function

'   Char. number range  |        UTF-8 octet sequence
'      (hexadecimal)    |              (binary)
'   --------------------+---------------------------------------------
'   0000 0000-0000 007F | 0xxxxxxx
'   0000 0080-0000 07FF | 110xxxxx 10xxxxxx
'   0000 0800-0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
'   0001 0000-0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
Public Function Encode_UTF8(astr)
    Dim c
    Dim n
    Dim utftext
     
    utftext = ""
    n = 1
    Do While n <= Len(astr)
        c = AscW(Mid(astr, n, 1))
        If c < 128 Then
            utftext = utftext + Chr(c)
        ElseIf ((c >= 128) And (c < 2048)) Then
            utftext = utftext + Chr(((c \ 64) Or 192))
            utftext = utftext + Chr(((c And 63) Or 128))
        ElseIf ((c >= 2048) And (c < 65536)) Then
            utftext = utftext + Chr(((c \ 4096) Or 224))
            utftext = utftext + Chr((((c \ 64) And 63) Or 128))
            utftext = utftext + Chr(((c And 63) Or 128))
        Else ' c >= 65536
            utftext = utftext + Chr(((c \ 262144) Or 240))
            utftext = utftext + Chr(((((c \ 4096) And 63)) Or 128))
            utftext = utftext + Chr((((c \ 64) And 63) Or 128))
            utftext = utftext + Chr(((c And 63) Or 128))
        End If
        n = n + 1
    Loop
    Encode_UTF8 = utftext
End Function
 
'   Char. number range  |        UTF-8 octet sequence
'      (hexadecimal)    |              (binary)
'   --------------------+---------------------------------------------
'   0000 0000-0000 007F | 0xxxxxxx
'   0000 0080-0000 07FF | 110xxxxx 10xxxxxx
'   0000 0800-0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
'   0001 0000-0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
Public Function Decode_UTF8(astr)
    Dim c0, c1, c2, c3
    Dim n
    Dim unitext
     
    If isUTF8(astr) = False Then
        Decode_UTF8 = astr
        Exit Function
    End If
     
    unitext = ""
    n = 1
    Do While n <= Len(astr)
        c0 = Asc(Mid(astr, n, 1))
        If n <= Len(astr) - 1 Then
            c1 = Asc(Mid(astr, n + 1, 1))
        Else
            c1 = 0
        End If
        If n <= Len(astr) - 2 Then
            c2 = Asc(Mid(astr, n + 2, 1))
        Else
            c2 = 0
        End If
        If n <= Len(astr) - 3 Then
            c3 = Asc(Mid(astr, n + 3, 1))
        Else
            c3 = 0
        End If
         
        If (c0 And 240) = 240 And (c1 And 128) = 128 And (c2 And 128) = 128 And (c3 And 128) = 128 Then
            unitext = unitext + ChrW((c0 - 240) * 65536 + (c1 - 128) * 4096) + (c2 - 128) * 64 + (c3 - 128)
            n = n + 4
        ElseIf (c0 And 224) = 224 And (c1 And 128) = 128 And (c2 And 128) = 128 Then
            unitext = unitext + ChrW((c0 - 224) * 4096 + (c1 - 128) * 64 + (c2 - 128))
            n = n + 3
        ElseIf (c0 And 192) = 192 And (c1 And 128) = 128 Then
            unitext = unitext + ChrW((c0 - 192) * 64 + (c1 - 128))
            n = n + 2
        ElseIf (c0 And 128) = 128 Then
            unitext = unitext + ChrW(c0 And 127)
            n = n + 1
        Else ' c0 < 128
            unitext = unitext + ChrW(c0)
            n = n + 1
        End If
    Loop
 
    Decode_UTF8 = unitext
End Function
 
'   Char. number range  |        UTF-8 octet sequence
'      (hexadecimal)    |              (binary)
'   --------------------+---------------------------------------------
'   0000 0000-0000 007F | 0xxxxxxx
'   0000 0080-0000 07FF | 110xxxxx 10xxxxxx
'   0000 0800-0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
'   0001 0000-0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
Public Function isUTF8(astr)
    Dim c0, c1, c2, c3
    Dim n
     
    isUTF8 = True
    n = 1
    Do While n <= Len(astr)
        c0 = Asc(Mid(astr, n, 1))
        If n <= Len(astr) - 1 Then
            c1 = Asc(Mid(astr, n + 1, 1))
        Else
            c1 = 0
        End If
        If n <= Len(astr) - 2 Then
            c2 = Asc(Mid(astr, n + 2, 1))
        Else
            c2 = 0
        End If
        If n <= Len(astr) - 3 Then
            c3 = Asc(Mid(astr, n + 3, 1))
        Else
            c3 = 0
        End If
         
        If (c0 And 240) = 240 Then
            If (c1 And 128) = 128 And (c2 And 128) = 128 And (c3 And 128) = 128 Then
                n = n + 4
            Else
                isUTF8 = False
                Exit Function
            End If
        ElseIf (c0 And 224) = 224 Then
            If (c1 And 128) = 128 And (c2 And 128) = 128 Then
                n = n + 3
            Else
                isUTF8 = False
                Exit Function
            End If
        ElseIf (c0 And 192) = 192 Then
            If (c1 And 128) = 128 Then
                n = n + 2
            Else
                isUTF8 = False
                Exit Function
            End If
        ElseIf (c0 And 128) = 0 Then
            n = n + 1
        Else
            isUTF8 = False
            Exit Function
        End If
    Loop
End Function

Private Function OpenAppendUTF8(ByVal fileName As String) As Integer
    OpenAppendUTF8 = FreeFile(0)
    Open fileName For Binary Access Write As #OpenAppendUTF8
    Seek #OpenAppendUTF8, LOF(OpenAppendUTF8) + 1
End Function

' Purpose:  Heuristic to determine whether bytes in a file are UTF-8.
Private Function FileBytesAreUTF8(ByVal the_iFileNo As Integer) As Boolean

    Const knSampleByteSize          As Long = 4096
    Dim nLof                        As Long
    Dim nByteCount                  As Long
    Dim nByteIndex                  As Long
    Dim nCharExtraByteCount         As Long
    Dim bytValue                    As Byte
    Dim either                      As Boolean 'For file with only code points < 127
    
    ' We look at the first <knSampleByteSize> bytes of the file. However, if the file is smaller, we will have to
    ' use the smaller size.
    nLof = LOF(the_iFileNo)
    If nLof < knSampleByteSize Then
        nByteCount = nLof
    Else
        nByteCount = knSampleByteSize
    End If

    ' Go to the start of the file.
    Seek #the_iFileNo, 1

    For nByteIndex = 1 To nByteCount

        Get #the_iFileNo, , bytValue

        ' If the character we are processing has bytes beyond 1, then we are onto the next character.
        If nCharExtraByteCount = 0 Then
            '
            ' The UTF-8 specification says that the first byte of a character has masking bits which indicate how many bytes follow.
            '
            ' See: http://en.wikipedia.org/wiki/UTF-8#Description
            '
            ' Bytes in
            ' sequence   Byte 1   Byte 2   Byte 3   Byte 4
            ' 1          0xxxxxxx
            ' 2          110xxxxx 10xxxxxx
            ' 3          1110xxxx 10xxxxxx 10xxxxxx
            ' 4          11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
            '
            If (bytValue And &H80) = &H0 Then
                nCharExtraByteCount = 0
            ElseIf (bytValue And &HE0) = &HC0 Then
                nCharExtraByteCount = 1
            ElseIf (bytValue And &HF0) = &HE0 Then
                nCharExtraByteCount = 2
            ElseIf (bytValue And &HF8) = &HF0 Then
                nCharExtraByteCount = 3
            Else
                ' If none of these masks were matched, then this can't be a UTF-8 character.
                FileBytesAreUTF8 = False
                Exit Function
            End If
        Else
            ' All following bytes must be masked as in the table above.
            If (bytValue And &HC0) = &H80 Then
                nCharExtraByteCount = nCharExtraByteCount - 1
                If nCharExtraByteCount = 0 Then
                    FileBytesAreUTF8 = True
                End If
            Else
                ' Not a UTF8 character.
                FileBytesAreUTF8 = False
                Exit Function
            End If
        End If

    Next nByteIndex

End Function

' Purpose:  Heuristic to determine whether bytes in a file are UTF-8.
Private Function FileBytesAreEither(ByVal the_iFileNo As Integer) As Boolean

    Const knSampleByteSize          As Long = 2048
    Dim nLof                        As Long
    Dim nByteCount                  As Long
    Dim nByteIndex                  As Long
    Dim nCharExtraByteCount         As Long
    Dim bytValue                    As Byte
    Dim either                      As Boolean 'For file with only code points < 127
    
    ' We look at the first <knSampleByteSize> bytes of the file. However, if the file is smaller, we will have to
    ' use the smaller size.
    either = True
    nLof = LOF(the_iFileNo)
    If nLof < knSampleByteSize Then
        nByteCount = nLof
    Else
        nByteCount = knSampleByteSize
    End If

    ' Go to the start of the file.
    Seek #the_iFileNo, 1

    For nByteIndex = 1 To nByteCount

        Get #the_iFileNo, , bytValue

        ' If the character we are processing has bytes beyond 1, then we are onto the next character.
        If nCharExtraByteCount = 0 Then
            '
            ' The UTF-8 specification says that the first byte of a character has masking bits which indicate how many bytes follow.
            '
            ' See: http://en.wikipedia.org/wiki/UTF-8#Description
            '
            ' Bytes in
            ' sequence   Byte 1   Byte 2   Byte 3   Byte 4
            ' 1          0xxxxxxx
            ' 2          110xxxxx 10xxxxxx
            ' 3          1110xxxx 10xxxxxx 10xxxxxx
            ' 4          11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
            '
            If (bytValue And &H80) = &H0 Then
                FileBytesAreEither = True
            Else
                FileBytesAreEither = False
                Exit Function
            End If
        Else
        End If

    Next nByteIndex

End Function

' Purpose:  Take a string whose bytes are in the byte array <the_abytCPString>, with code page <the_nCodePage>, convert to a VB string.
Private Function FromCPString(ByRef the_abytCPString() As Byte, ByVal the_nCodePage As Long) As String

    Dim sOutput                     As String
    Dim nValueLen                   As Long
    Dim nOutputCharLen              As Long

    ' If the code page says this is already compatible with the VB string, then just copy it into the string. No messing.
    If the_nCodePage = CP_UTF16_LE Then
        FromCPString = the_abytCPString()
    Else

        ' Cache the input length.
        nValueLen = UBound(the_abytCPString) - LBound(the_abytCPString) + 1

        ' See how big the output buffer will be.
        nOutputCharLen = MultiByteToWideChar(the_nCodePage, 0&, VarPtr(the_abytCPString(LBound(the_abytCPString))), nValueLen, 0&, 0&)

        ' Resize output byte array to the size of the UTF-8 string.
        sOutput = Space$(nOutputCharLen)

        ' Make this API call again, this time giving a pointer to the output byte array.
        MultiByteToWideChar the_nCodePage, 0&, VarPtr(the_abytCPString(LBound(the_abytCPString))), nValueLen, StrPtr(sOutput), nOutputCharLen

        ' Return the array.
        FromCPString = sOutput

    End If

End Function

Public Function GetContents(ByVal the_sTextFile As String, ByRef out_nCodePage As Long, Optional ByVal the_nDesiredCodePage As Long = -1, Optional ByRef out_bContainedBOM As Boolean) As String

    Dim iFileNo                     As Integer
    Dim abytFileContents()          As Byte
    Dim nDataSize                   As Long


    On Error GoTo GestError
    iFileNo = FreeFile

    OpenForInput the_sTextFile, iFileNo, out_nCodePage, the_nDesiredCodePage, out_bContainedBOM

    ' We want to read the entire contents of the file (not including any BOM value).
    ' After calling OpenForInput(), the file pointer should be positioned after any BOM.
    ' So size file contents buffer to <file size> - <current position> + 1.
    nDataSize = LOF(iFileNo) - Seek(iFileNo) + 1
    ReDim abytFileContents(1 To nDataSize)
    Get #iFileNo, , abytFileContents()

    Close iFileNo

    ' Now we must convert this to UTF-8. But we have to first convert to the Windows NT standard UTF-16 LE.
    GetContents = FromCPString(abytFileContents(), out_nCodePage)
    Exit Function
GestError:
    Close iFileNo
    Err.Raise 999, "getContent", Err.Description
    
End Function

' Purpose:  Reads up to the end of the current line of the file, repositions to the beginning of the next line, if any, and
'           outputs all characters found.
' Inputs:   the_nFileNo     The number of the file.
' Outputs:  out_sLine       The line from the current position in the file.
' Return:   True if there is more data.
Public Function LineInputUTF8(ByVal the_nFileNo As Integer, ByRef out_sLine As String) As Boolean

    Dim bytValue            As Byte
    Dim abytLine()          As Byte
    Dim nStartOfLinePos     As Long
    Dim nEndOfLinePos       As Long
    Dim nStartOfNextLine    As Long
    Dim nLineLen            As Long

    ' Save the current file position as the beginning of the line, and cache this value.
    nStartOfLinePos = Seek(the_nFileNo)

    ' Retrieves the first byte from the current position.
    Get #the_nFileNo, , bytValue

    ' Loop until the end of file is encountered.
    Do Until EOF(the_nFileNo)

        ' Check whether this byte represents a carriage return or line feed character (indicating new line).
        If bytValue = 13 Or bytValue = 10 Then
            ' By this point, the current position is *after* the CR or LF character, so to get the position of the
            ' last byte in the line, we must go back two bytes.
            nEndOfLinePos = Seek(the_nFileNo) - 2

            ' If this is a carriage return, then we must check the next character.
            If bytValue = 13 Then
                Get #the_nFileNo, , bytValue
                ' Is this a line feed?
                If bytValue = 10 Then
                ' Yes. Assume that CR-LF counts as a single NewLine. So the start of the next line should skip over the line feed.
                    nStartOfNextLine = nEndOfLinePos + 3
                Else
                ' No. The start of the next line is the current position.
                    nStartOfNextLine = nEndOfLinePos + 2
                End If
            ElseIf bytValue = 10 Then
            ' If this is a line feed, then the start of the next line is the current position.
                nStartOfNextLine = nEndOfLinePos + 2
            End If

            ' Since we have processed all the bytes in the line, exit the loop.
            Exit Do
        End If

        ' Get the next byte.
        Get #the_nFileNo, , bytValue
    Loop

    ' Check to see if there was an end of line.
    If nEndOfLinePos = 0 Then
    ' No, this is the end of the file - so use all the remaining characters.
        nLineLen = Seek(the_nFileNo) - nStartOfLinePos - 1
    Else
    ' Yes - so use all the characters up to the end of line position.
        nLineLen = nEndOfLinePos - nStartOfLinePos + 1
    End If

    ' Is this line empty?
    If nLineLen = 0 Then
    ' Yes - just return an empty string.
        out_sLine = vbNullString
    Else
    ' No - pull all the bytes from the beginning to the end of the line into a byte array, and then convert that from UTF-8 to a VB string.
        ReDim abytLine(1 To nLineLen)
        Get #the_nFileNo, nStartOfLinePos, abytLine()
        out_sLine = FromCPString(abytLine(), CP_UTF8)
    End If

    ' If there is a line afterwards, then move to the beginning of the line, and return True.
    If nStartOfNextLine > 0 Then
        Seek #the_nFileNo, nStartOfNextLine
        LineInputUTF8 = True
    End If

End Function

' Purpose:  Analogue of 'Open "fileName" For Input As #fileNo' - but also return what type of text this is via a Code Page value.
' Inputs:   the_sFileName
'           the_iFileNo
'           (the_nDesiredCodePage)  The code page that you want to use with this file.
'                                   If this value is set to the default, -1, this indicates that the code page will be ascertained from the file.
' Outputs:  out_nCodePage           There are only six valid values that are returned if <the_nDesiredCodePage> was set to -1.
'               CP_ACP        ANSI code page
'               CP_UTF8       UTF-8
'               CP_UTF16LE    UTF-16 Little Endian (VB and NT default string encoding)
'               CP_UTF16BE    UTF-16 Big Endian
'               CP_UTF32LE    UTF-32 Little Endian
'               CP_UTF32BE    UTF-32 Big Endian
'           (out_bContainedBOM)     If this was set to True, then the file started with a BOM (Byte Order Marker).
Public Sub OpenForInput(ByRef the_sFilename As String, ByVal the_iFileNo As Integer, ByRef out_nCodePage As Long, Optional ByVal the_nDesiredCodePage As Long = -1, Optional ByRef out_bContainedBOM As Boolean)

    ' Note if we want to take account of every case, we should read in the first four bytes, and check for UTF-32 low and high endian BOMs, check
    ' the first three bytes for the UTF-8 BOM, and finally check the first two bytes for UTF-16 low and hight endian BOMs.
    Dim abytBOM(1 To 4)             As Byte
    Dim nCodePage                   As Long

    ' By default, there is no BOM.
    out_bContainedBOM = False

    Open the_sFilename For Binary Access Read As #the_iFileNo

    ' We are interested in -1 (ascertain code page), and then various UTF encodings.
    Select Case the_nDesiredCodePage
    Case -1, CP_UTF8, CP_UTF16_BE, CP_UTF16_LE, CP_UTF32_BE, CP_UTF32_LE

        ' Default code page.
        nCodePage = CP_ACP

        ' Pull in the first four bytes to determine the BOM (byte order marker).
        Get #the_iFileNo, , abytBOM()

        ' The following are the BOMs for text files:
        '
        ' FF FE         UTF-16, little endian
        ' FE FF         UTF-16, big endian
        ' EF BB BF      UTF-8
        ' FF FE 00 00   UTF-32, little endian
        ' 00 00 FE FF   UTF-32, big-endian
        '
        ' Work out the code page from this information.

        Select Case abytBOM(1)
        Case &HFF
            If abytBOM(2) = &HFE Then
                If abytBOM(3) = 0 And abytBOM(4) = 0 Then
                    nCodePage = CP_UTF32_LE
                Else
                    nCodePage = CP_UTF16_LE
                End If
            End If
        Case &HFE
            If abytBOM(2) = &HFF Then
                nCodePage = CP_UTF16_BE
            End If
        Case &HEF
            If abytBOM(2) = &HBB And abytBOM(3) = &HBF Then
                nCodePage = CP_UTF8
            End If
        Case &H0
            If abytBOM(2) = &H0 And abytBOM(3) = &HFE And abytBOM(4) = &HFF Then
                nCodePage = CP_UTF32_BE
            End If
        End Select

        ' Did we match any BOMs?
        If nCodePage = CP_ACP Then
        ' No - we are still defaulting to the ANSI code page.
            ' Special check for UTF-8. The BOM is not specified in the standard for UTF-8, but according to Wikipedia (which is always right :-) ),
            ' only Microsoft includes this marker at the beginning of files.
            If FileBytesAreUTF8(the_iFileNo) Then
                out_nCodePage = CP_UTF8
            Else
                If Convert <> "ANSI" Then
                    If FileBytesAreEither(the_iFileNo) Then
                        out_nCodePage = CP_UTF8
                    Else
                        out_nCodePage = CP_ACP
                    End If
                Else
                    out_nCodePage = CP_ACP
                End If
            End If
        Else
        ' Yes - we have worked out the code page from the BOM.
            ' If no code page was suggested, we now return the code page we found.
            If the_nDesiredCodePage = -1 Then
                out_nCodePage = nCodePage
            End If

            ' Inform the caller that a BOM was found.
            out_bContainedBOM = True
        End If

        ' Reset the file pointer to the beginning of the file data.
        If out_bContainedBOM Then
            ' Note that if the code page found was one of the two UTF-32 values, then we are already in the correct position.
            ' Otherwise, we have to move to just after the end of the BOM.
            Select Case nCodePage
            Case CP_UTF16_BE, CP_UTF16_LE
                Seek #the_iFileNo, 3
            Case CP_UTF8
                Seek #the_iFileNo, 4
            End Select
        Else
            ' There is no BOM, so simply go the beginning of the file.
            Seek #the_iFileNo, 1
        End If

    Case Else
        out_nCodePage = the_nDesiredCodePage
    End Select

End Sub

' Purpose:  Analogue of 'Open "fileName" For Append As #fileNo'
Public Sub OpenForAppend(ByRef the_sFilename As String, ByVal the_iFileNo As Integer, Optional ByVal the_nCodePage As Long = CP_ACP, Optional ByVal the_bPrefixWithBOM As Boolean = True)

    ' Open the file and move to the end of the file.
    Open the_sFilename For Binary Access Write As #the_iFileNo
    Seek the_iFileNo, LOF(the_iFileNo) + 1

    If the_bPrefixWithBOM Then
        WriteBOM the_iFileNo, the_nCodePage
    End If

End Sub

' Purpose:  Analogue of 'Open "fileName" For Output As #fileNo'
Public Sub OpenForOutput(ByRef the_sFilename As String, ByVal the_iFileNo As Integer, Optional ByVal the_nCodePage As Long = CP_ACP, Optional ByVal the_bPrefixWithBOM As Boolean = True)

    ' Ensure we overwrite the file by deleting it ...
    On Error Resume Next
    Kill the_sFilename
    On Error GoTo 0

    ' ... before creating it.
    Open the_sFilename For Binary Access Write As #the_iFileNo

    If the_bPrefixWithBOM Then
        WriteBOM the_iFileNo, the_nCodePage
    End If

End Sub

' Purpose:  Analogue of the 'Print #fileNo, value' statement. But only one value allowed.
'           Setting <the_bAppendNewLine> = False is analagous to 'Print #fileNo, value;'.
Public Sub Print_(ByVal the_iFileNo As Integer, ByRef the_sValue As String, Optional ByVal the_nCodePage As Long = CP_ACP, Optional ByVal the_bAppendNewLine As Boolean = True)

    Const kbytNull                  As Byte = 0
    Const kbytCarriageReturn        As Byte = 13
    Const kbytNewLine               As Byte = 10

    Put #the_iFileNo, , ToCPString(the_sValue, the_nCodePage)

    If the_bAppendNewLine Then
        Select Case the_nCodePage
        Case CP_UTF16_BE
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytCarriageReturn
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytNewLine
        Case CP_UTF16_LE
            Put #the_iFileNo, , kbytCarriageReturn
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytNewLine
            Put #the_iFileNo, , kbytNull
        Case CP_UTF32_BE
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytCarriageReturn
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytNewLine
        Case CP_UTF32_LE
            Put #the_iFileNo, , kbytCarriageReturn
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytNewLine
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytNull
            Put #the_iFileNo, , kbytNull
        Case Else
            Put #the_iFileNo, , kbytCarriageReturn
            Put #the_iFileNo, , kbytNewLine
        End Select
    End If

End Sub

Public Sub PutContents(ByRef the_sFilename As String, ByRef the_sFileContents As String, Optional ByVal the_nCodePage As Long = CP_ACP, Optional the_bPrefixWithBOM As Boolean)

    Dim iFileNo                     As Integer
    
    On Error GoTo GestError
    'WriteToConsole "Convert: " & the_sFilename & vbCrLf
    iFileNo = FreeFile
    OpenForOutput the_sFilename, iFileNo, the_nCodePage, the_bPrefixWithBOM
    Print_ iFileNo, the_sFileContents, the_nCodePage, False
    Close iFileNo
    Exit Sub
GestError:
    WriteToConsole "Error on " & the_sFilename & " " & Err.Description & vbCrLf
    Close iFileNo
    Err.Raise Err.Number, "Error on " & the_sFilename, Err.Description
    
End Sub

' Purpose:  Converts a VB string (UTF-16) to UTF8 - as a binary array.
Private Function ToCPString(ByRef the_sValue As String, ByVal the_nCodePage As Long) As Byte()

    Dim abytOutput()                As Byte
    Dim nValueLen                   As Long
    Dim nOutputByteLen              As Long

    If the_nCodePage = CP_UTF16_LE Then
        ToCPString = the_sValue
    Else

        ' Cache the input length.
        nValueLen = Len(the_sValue)

        ' See how big the output buffer will be.
        nOutputByteLen = WideCharToMultiByte(the_nCodePage, 0&, StrPtr(the_sValue), nValueLen, 0&, 0&, 0&, 0&)

        If nOutputByteLen > 0 Then
            ' Resize output byte array to the size of the UTF-8 string.
            ReDim abytOutput(1 To nOutputByteLen)

            ' Make this API call again, this time giving a pointer to the output byte array.
            WideCharToMultiByte the_nCodePage, 0&, StrPtr(the_sValue), nValueLen, VarPtr(abytOutput(1)), nOutputByteLen, 0&, 0&
        End If

        ' Return the array.
        ToCPString = abytOutput()

    End If

End Function

Private Sub WriteBOM(ByVal the_iFileNo As Integer, ByVal the_nCodePage As Long)

    ' FF FE         UTF-16, little endian
    ' FE FF         UTF-16, big endian
    ' EF BB BF      UTF-8
    ' FF FE 00 00   UTF-32, little endian
    ' 00 00 FE FF   UTF-32, big-endian

    Select Case the_nCodePage
    Case CP_UTF8
        Put #the_iFileNo, , CByte(&HEF)
        Put #the_iFileNo, , CByte(&HBB)
        Put #the_iFileNo, , CByte(&HBF)
    Case CP_UTF16_LE
        Put #the_iFileNo, , CByte(&HFF)
        Put #the_iFileNo, , CByte(&HFE)
    Case CP_UTF16_BE
        Put #the_iFileNo, , CByte(&HFE)
        Put #the_iFileNo, , CByte(&HFF)
    Case CP_UTF32_LE
        Put #the_iFileNo, , CByte(&HFF)
        Put #the_iFileNo, , CByte(&HFE)
        Put #the_iFileNo, , CByte(&H0)
        Put #the_iFileNo, , CByte(&H0)
    Case CP_UTF32_BE
        Put #the_iFileNo, , CByte(&H0)
        Put #the_iFileNo, , CByte(&H0)
        Put #the_iFileNo, , CByte(&HFE)
        Put #the_iFileNo, , CByte(&HFF)
    End Select

End Sub

Private Function QualifyPath(sPath As String) As String
   If Right$(sPath, 1) <> envSlash Then
      QualifyPath = sPath & envSlash
   Else
      QualifyPath = sPath
   End If
End Function

Private Function TrimNull(startstr As String) As String
   TrimNull = Left$(startstr, lstrlen(StrPtr(startstr)))
End Function

Private Function MatchSpec(sFile As String, sSpec As String) As Boolean
   MatchSpec = PathMatchSpec(StrPtr(sFile), StrPtr(sSpec)) = fp.bFindOrExclude
End Function

Private Function GetFileTimes(ByVal file_name As String, _
    ByRef creation_date As Date, ByRef access_date As Date, _
    ByRef modified_date As Date, ByVal local_time As _
    Boolean) As Boolean
Dim file_handle As Long
Dim creation_filetime As FILETIME
Dim access_filetime As FILETIME
Dim modified_filetime As FILETIME
Dim file_time As FILETIME

    ' Assume something will fail.
    GetFileTimes = True

    ' Open the file.
    file_handle = CreateFile(file_name, GENERIC_READ, _
        FILE_SHARE_READ Or FILE_SHARE_WRITE, _
        0&, OPEN_EXISTING, 0&, 0&)
    If file_handle = 0 Then Exit Function

    ' Get the times.
    If GetFileTime(file_handle, creation_filetime, _
        access_filetime, modified_filetime) = 0 Then
        CloseHandle file_handle
        Exit Function
    End If

    ' Close the file.
    If CloseHandle(file_handle) = 0 Then Exit Function

    ' See if we should convert to the local
    ' file system time.
    If local_time Then
        ' Convert to local file system time.
        FileTimeToLocalFileTime creation_filetime, file_time
        creation_filetime = file_time

        FileTimeToLocalFileTime access_filetime, file_time
        access_filetime = file_time

        FileTimeToLocalFileTime modified_filetime, file_time
        modified_filetime = file_time
    End If

    ' Convert into dates.
    creation_date = FileTimeToDate(creation_filetime)
    access_date = FileTimeToDate(access_filetime)
    modified_date = FileTimeToDate(modified_filetime)

    GetFileTimes = False
End Function

Private Function SetFileTimes(ByVal file_name As String, _
    ByVal creation_date As Date, ByVal access_date As Date, _
    ByVal modified_date As Date, ByVal local_times As _
    Boolean) As Boolean
Dim file_handle As Long
Dim creation_filetime As FILETIME
Dim access_filetime As FILETIME
Dim modified_filetime As FILETIME
Dim file_time As FILETIME

    ' Assume something will fail.
    SetFileTimes = True

    ' Convert the dates into FILETIMEs.
    creation_filetime = DateToFileTime(creation_date)
    access_filetime = DateToFileTime(access_date)
    modified_filetime = DateToFileTime(modified_date)

    ' Convert the file times into system file times.
    If local_times Then
        LocalFileTimeToFileTime creation_filetime, file_time
        creation_filetime = file_time

        LocalFileTimeToFileTime access_filetime, file_time
        access_filetime = file_time

        LocalFileTimeToFileTime modified_filetime, file_time
        modified_filetime = file_time
    End If

    ' Open the file.
    file_handle = CreateFile(file_name, GENERIC_WRITE, _
        FILE_SHARE_READ Or FILE_SHARE_WRITE, _
        0&, OPEN_EXISTING, 0&, 0&)
    If file_handle = 0 Then Exit Function

'creation_date = FileTimeToDate(creation_filetime)

    ' Set the times.
    If SetFileTime(file_handle, creation_filetime, _
        access_filetime, modified_filetime) = 0 Then
        CloseHandle file_handle
        Exit Function
    End If

    ' Close the file.
    If CloseHandle(file_handle) = 0 Then Exit Function

    SetFileTimes = False
End Function

Private Function FileTimeToDate(file_time As FILETIME) As Date
Dim system_time As SYSTEMTIME

    ' Convert the FILETIME into a SYSTEMTIME.
    FileTimeToSystemTime file_time, system_time

    ' Convert the SYSTEMTIME into a Date.
    FileTimeToDate = SystemTimeToDate(system_time)
End Function

Private Function DateToFileTime(ByVal the_date As Date) As FILETIME
Dim system_time As SYSTEMTIME
Dim file_time As FILETIME

    ' Convert the Date into a SYSTEMTIME.
    system_time = DateToSystemTime(the_date)

    ' Convert the SYSTEMTIME into a FILETIME.
    SystemTimeToFileTime system_time, file_time
    DateToFileTime = file_time
End Function

Private Function SystemTimeToDate(system_time As SYSTEMTIME) As Date
    With system_time
        SystemTimeToDate = CDate( _
            Format$(.wDay) & "/" & _
            Format$(.wMonth) & "/" & _
            Format$(.wYear) & " " & _
            Format$(.wHour) & ":" & _
            Format$(.wMinute, "00") & ":" & _
            Format$(.wSecond, "00"))
    End With
End Function
' Convert a Date into a SYSTEMTIME.
Private Function DateToSystemTime(ByVal the_date As Date) As SYSTEMTIME
    With DateToSystemTime
        .wYear = Year(the_date)
        .wMonth = Month(the_date)
        .wDay = Day(the_date)
        .wHour = Hour(the_date)
        .wMinute = Minute(the_date)
        .wSecond = Second(the_date)
    End With
End Function

'The following function writes the content of sText variable into the console window:
Private Function WriteToConsole(sText As String) As Boolean
    If quiet Then Exit Function
    Dim lWritten            As Long
    
    If WriteFile(hOutput, ByVal sText, Len(sText), lWritten, ByVal 0) = 0 Then
        WriteToConsole = False
    Else
        WriteToConsole = True
    End If
End Function

Public Function IsBinaryFile(File As String) As Boolean
    
    On Error GoTo IsBinaryFile_Err
    Const aLf = 10, aCR = 13, aSP = 32
    Const MaxRead = 2 ^ 15 - 1
    
    Dim ff  As Integer
    Dim s   As Integer
    Dim i   As Integer
    Dim n   As Integer
    Dim Rtn As Boolean
    
    On Error GoTo IsBinaryFile_Err
    
    ff = FreeFile
    If FileLen(File) = 0 Then Exit Function
    Open File For Binary Access Read As #ff
    n = IIf(LOF(ff) > MaxRead, MaxRead - 1, LOF(ff))
    Do
        i = i + 1
        If i >= n Then
           IsBinaryFile = False
           Rtn = True
        Else
           s = Asc(Input$(1, #ff))
           If s >= aSP Then
              Else
              If s = aCR Or s = aLf Then
              Else
                 IsBinaryFile = True
                 Rtn = True
              End If
           End If
        End If
    Loop Until Rtn
    Close ff
    Exit Function
    
IsBinaryFile_Err:
    IsBinaryFile = True
    If ff Then Close ff
End Function

Public Function FindFilesByName(ByVal sPath$, ByVal eChangeType As NOTIFY_CHANGE_TYPE) As String
  ' finds files with changed names by checking the file names against the cached
  ' values in the module level array

  Dim i&, hFileSearch&, sFileNames$, nUbound&, sChangeType$, bFound As Boolean
  Dim udtFindData As WIN32_FIND_DATA, anFoundIndexes() As Boolean
  
  sChangeType = ReturnChangeTypeString(eChangeType)
  
  ' fix up the path
  If Right$(sPath, 1) <> "\" Then sPath = sPath & "\"

  ' start the search for all files in the folder
  hFileSearch = FindFirstFile(sPath & "*.*", udtFindData)

  If hFileSearch <> -1& Then
    
    If LenB(m_audtDirContents(0).cFileName) Then
      nUbound = UBound(m_audtDirContents)
    Else
      nUbound = (-1)
    End If
    
    ReDim anFoundIndexes(nUbound) As Boolean
    
    Do
      ' reset the variable
      bFound = False
      
      With udtFindData
        .cFileName = StripNulls(.cFileName)
        
        For i = 0 To nUbound
          If LenB(.cFileName) = LenB(m_audtDirContents(i).cFileName) Then
            ' only compare the strings if the length is the same
            If .cFileName = m_audtDirContents(i).cFileName Then
              bFound = True
              anFoundIndexes(i) = True
              Exit For
            End If
          End If
        Next
      
        If bFound = False Then
          sFileNames = sFileNames & "Change Type: " & sChangeType & vbNewLine
          
          If .dwFileAttributes And vbDirectory Then
            sFileNames = sFileNames & "  " & sPath & Trim$(.cFileName) & "  [Dir]" & vbNewLine
          Else
            sFileNames = sFileNames & "  " & sPath & Trim$(.cFileName) & vbNewLine
          End If
        End If
      End With
            
      If FindNextFile(hFileSearch, udtFindData) = API_FALSE Then
        ' if we get ERROR_NO_MORE_FILES close the search and jump out of the loop
        If Err.LastDllError = ERROR_NO_MORE_FILES Then
          Call FindClose(hFileSearch)
          Exit Do
        End If
      End If
    Loop
    
    For i = 0 To nUbound
      If anFoundIndexes(i) = False Then
        With m_audtDirContents(i)
          sFileNames = sFileNames & "Change Type: " & sChangeType & vbNewLine
          
          If .dwFileAttributes And vbDirectory Then
            sFileNames = sFileNames & "  " & sPath & Trim$(.cFileName) & "  [Deleted]" & " [Dir]" & vbNewLine
          Else
            sFileNames = sFileNames & "  " & sPath & Trim$(.cFileName) & "  [Deleted]" & vbNewLine
          End If
        End With
      End If
    Next
    
    FindFilesByName = sFileNames
    
  End If
  
End Function

Public Function StripNulls(ByVal sText As String) As String
  ' strips any nulls from the end of a string
  Dim nPosition&
  
  StripNulls = sText
  
  nPosition = InStr(sText, vbNullChar)
  If nPosition Then StripNulls = Left$(sText, nPosition - 1)
  If Len(sText) Then If Left$(sText, 1) = vbNullChar Then StripNulls = vbZLString
End Function

Private Sub FillFilesArray(ByVal sPath$) 'As FILETIME
  ' fills the module level array that caches all of the info on each file in the dir
  
  Dim i&, nUbound&, hFileSearch&, sFileNames$
  Dim udtFindData As WIN32_FIND_DATA
  
  Erase m_audtDirContents
  
  nUbound = 100
  
  ReDim m_audtDirContents(nUbound) As WIN32_FIND_DATA
  
  ' fix up the path
  If Right$(sPath, 1) <> "\" Then sPath = sPath & "\"

  ' start the search for all files in the folder
  hFileSearch = FindFirstFile(sPath & "*.*", udtFindData)

  If hFileSearch <> INVALID_HANDLE_VALUE Then
    '
    Do
      If i > nUbound Then
        nUbound = nUbound + 100
        
        ReDim Preserve m_audtDirContents(nUbound) As WIN32_FIND_DATA
      End If
      
      With udtFindData
        .cFileName = StripNulls(.cFileName)
      End With
      
      ' save the struct to the array
      m_audtDirContents(i) = udtFindData
            
      If FindNextFile(hFileSearch, udtFindData) = API_FALSE Then
        ' if we get ERROR_NO_MORE_FILES close the search and jump out of the loop
        If Err.LastDllError = ERROR_NO_MORE_FILES Then
          Call FindClose(hFileSearch)
          Exit Do
        End If
      End If
      
      i = i + 1
    Loop
  End If
  
  ReDim Preserve m_audtDirContents(i) As WIN32_FIND_DATA
  
End Sub

Private Function ReturnChangeTypeString(ByVal eChangeType As NOTIFY_CHANGE_TYPE) As String

  If (eChangeType And cwdName) > 0 Then ReturnChangeTypeString = "Name"
  
  If (eChangeType And cwdSize) > 0 Then
    If LenB(ReturnChangeTypeString) Then
      ReturnChangeTypeString = ReturnChangeTypeString & " & Size"
    Else
      ReturnChangeTypeString = "Size"
    End If
  End If
  
  If (eChangeType And cwdLastWrite) > 0 Then
    If LenB(ReturnChangeTypeString) Then
      ReturnChangeTypeString = ReturnChangeTypeString & " & Last Write"
    Else
      ReturnChangeTypeString = "Last Write"
    End If
  End If
  
  If (eChangeType And cwdDirName) > 0 Then
    If LenB(ReturnChangeTypeString) Then
      ReturnChangeTypeString = ReturnChangeTypeString & " & Dir Name"
    Else
      ReturnChangeTypeString = "Dir Name"
    End If
  End If
    
End Function

Public Function StartExcel() As Object
    Dim oExcel        As Object
    Dim sAPPPath        As String
 
    On Error Resume Next
    
    If IsAppRunning("Excel.Application") = True Then    'Excel was already running
        Set oExcel = GetObject(, "Excel.Application")    'Bind to existing instance of Outlook
    Else    'Could not get instance of Outlook, so create a new one
        Set oExcel = CreateObject("Excel.Application")
        If Not oExcel Is Nothing Then GoTo suite
        sAPPPath = GetAppExePath("excel.exe")    'determine outlook's installation path
        Shell (sAPPPath)    'start outlook
        Sleep 1000
        Set oExcel = GetObject(, "Excel.Application")    'Bind to existing instance of Outlook
    End If
suite:
    Set StartExcel = oExcel
 
End Function

Function IsAppRunning(sApp As String) As Boolean
    On Error GoTo Error_Handler
    Dim oApp            As Object
 
    Set oApp = GetObject(, sApp)
    IsAppRunning = True
 
Error_Handler_Exit:
    On Error Resume Next
    Set oApp = Nothing
    Exit Function
 
Error_Handler:
    Resume Error_Handler_Exit
End Function
 
Function GetAppExePath(ByVal sExeName As String) As String
    On Error GoTo Error_Handler
    Dim WSHShell        As Object
 
    Set WSHShell = CreateObject("Wscript.Shell")
    GetAppExePath = WSHShell.RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\" & sExeName & "\")
 
Error_Handler_Exit:
    On Error Resume Next
    Set WSHShell = Nothing
    Exit Function
 
Error_Handler:
    If Err.Number = -2147024894 Then
        'Cannot locate requested exe????
    Else
        MsgBox "The following error has occurred." & vbCrLf & vbCrLf & _
               "Error Number: " & Err.Number & vbCrLf & _
               "Error Source: GetAppExePath" & vbCrLf & _
               "Error Description: " & Err.Description, _
               vbCritical, "An Error has Occurred!"
    End If
    Resume Error_Handler_Exit
End Function

Public Function GetTempFileNameVB(ByVal Id As String, Optional Rep As Boolean) As String

Dim TempPath As String
Dim lpBuffer As String
Dim Result As Long
Dim TempFileName As String

'---> Fonction qui d�termine un nom de fichier tempo sous windows

'-> Recherche du r�pertoire temporaire
lpBuffer = Space$(500)
Result = GetTempPath(Len(lpBuffer), lpBuffer)
TempPath = Mid$(lpBuffer, 1, Result)
If tempPathLocal <> "" Then TempPath = tempPathLocal

'-> Si on ne demande que le r�pertoire de windows
If Rep Then
    GetTempFileNameVB = TempPath
    Exit Function
End If

'-> Cr�ation d'un nom de fichier
TempFileName = Space$(1000)
Result = GetTempFileName(TempPath, Id, 0, TempFileName)
TempFileName = Entry(1, TempFileName, Chr(0))

GetTempFileNameVB = TempFileName

End Function

