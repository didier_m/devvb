Attribute VB_Name = "dm_Cmm"
Option Explicit

'-> Variables de r�cup�ration des infos locales
Public SepDec As String 'S�parateur d�cimal
Public SepMil As String 'S�parateur de millier
Public FormatDateToApply As String 'Format de date � appliquer

'-> Pour gestion des temporisations
Public TailleLue As Long
Public TailleTotale As Long

'-> Pour arr�ter l'attente d'un fichier dans proc�dure WaitFile
Public ExitWait As Boolean

'-> Variable globale d'�change entre les feuilles
Public strRetour As String

'-> Pour gestion des paths dans la fen�tre d'ouverture
Public SavePath As String '-> Path d'ouverture
Public SaveExtension As String '-> Extension d'ouvertue

Public Sub ClearCol(aCol As Collection)

'---> Cette proc�dure supprime le contenu d'une collection

On Error Resume Next



'-> Nettoyer le contenu
Do While aCol.Count <> 0
    aCol.Remove (1)
Loop

End Sub


Public Function IsDir(strDir As String) As Boolean

'---> cette proc�dure indique si un path est valable

On Error GoTo GestError

'-> Ne pas traiter les r�pertoires � blanc
If strDir = "" Then Exit Function

'-> V�rifier
If (GetAttr(strDir) And vbDirectory) = vbDirectory Then IsDir = True

GestError:


End Function


Public Sub SelectTxtBox(ByRef aTxt As TextBox)

'-> S�lectionner le contenu d'une zone de type Text
aTxt.SelStart = 0
aTxt.SelLength = Len(aTxt.Text)

End Sub

Public Function FormatCompareDate(strDateToFormat As String) As String

'-> Tester que la date soit valide
If Not IsDate(strDateToFormat) Then Exit Function

'-> convertir la date au format aaaa/mm/jj
FormatCompareDate = Format(Year(strDateToFormat), "0000") & Format(Month(strDateToFormat), "00") & Format(Day(strDateToFormat), "00")


End Function

Public Function Convert(ByVal StrToAnalyse As String) As String

Dim i As Integer
Dim Tempo As String
Dim FindSep As Boolean

For i = Len(StrToAnalyse) To 1 Step -1
    If Mid$(StrToAnalyse, i, 1) = "." Then
        If Not FindSep Then
            Tempo = SepDec & Tempo
            FindSep = True
        End If
    ElseIf Mid$(StrToAnalyse, i, 1) = "," Then
        If Not FindSep Then
            Tempo = SepDec & Tempo
            FindSep = True
        End If
    Else
        Tempo = Mid$(StrToAnalyse, i, 1) & Tempo
    End If
Next 'Pour tous les caract�res � analyser

Convert = Tempo

End Function

Public Function AddEntryInMatrice(Matrice As String, NewCode As String, Sep As String, Optional IdEntry As Long) As String

'---> Cette proc�dure ajoute une entr�e dans la matrice

Dim i As Long
Dim MatriceTemp As String
Dim ToAdd As Boolean

'-> Si on ne sp�cifie pas
If IdEntry = 0 Then
    If Trim(Matrice) = "" Then
        Matrice = NewCode
    Else
        Matrice = Matrice & Sep & NewCode
    End If
    '-> Renvoyer la matrice
    AddEntryInMatrice = Matrice
    '-> Quitter la fonction
    Exit Function
End If 'Si on sp�cifie un entry ou non

If IdEntry = 1 Then '-> Si on doit ajouter en premier
    If Trim(Matrice) = "" Then
        Matrice = NewCode
    Else
        Matrice = NewCode & Sep & Matrice
    End If
ElseIf IdEntry > NumEntries(Matrice, Sep) Then 'Si on doit rajouter en dernier
    If Trim(Matrice) = "" And Trim(NewCode) <> "" Then
        Matrice = NewCode
    Else
        Matrice = Matrice & Sep & NewCode
    End If
Else
    '-> Il faut ins�rer dans la matrice
    For i = 1 To NumEntries(Matrice, Sep)
        If i = IdEntry Then
            '-> Doit on ins�rer dans la matrice
            If Trim(Matrice) = "" And Trim(NewCode) <> "" Then
                MatriceTemp = NewCode
            Else
                MatriceTemp = MatriceTemp & Sep & NewCode
            End If
        End If
        
        '-> Ajouter le code
        If Trim(Matrice) = "" And Trim(NewCode) <> "" Then
            MatriceTemp = Entry(i, Matrice, Sep)
        Else
            MatriceTemp = MatriceTemp & Sep & Entry(i, Matrice, Sep)
        End If
    Next 'Pour toutes les entr�es de la matrice
    
    '-> Affectation de la avariable temporaire
    Matrice = MatriceTemp
End If
    
'-> Renvoyer la matrice
AddEntryInMatrice = Matrice

End Function


Public Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Integer
Dim PosEnCour As Integer
Dim i As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
End Function

Public Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Integer

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Integer
Dim i As Integer
Dim PosAnalyse As Integer

On Error GoTo GestError

'-< Analyse du vecteur
PosAnalyse = 1
Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1
'-> Quitter la fonction
Exit Function

GestError:
    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
End Function

Public Function DeleteEntry(ByVal Matrice As String, ByVal EntryToDel As Integer, ByVal SepMatrice As String) As String

'---> Cette fonction supprime une entr�e dans la matrice. Elle renvoie la matrice mise � jour
Dim NbEntries As Integer
Dim i As Integer
Dim j As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> R�cup�rer le nombre d'entr�e
NbEntries = NumEntries(Matrice, SepMatrice)

'-> Renvoyer un chaine vide s'il n'y a qu'une seule entr�e
If NbEntries = 1 Then
    'Sil n'y a qu'une seule entr�e, vider la matrice
    DeleteEntry = ""
    Exit Function
ElseIf EntryToDel > NbEntries Then
    'Si on doit supprimer une entr�e qui n'existe pas -> renvoyer la matrice d'origine
    DeleteEntry = Matrice
    Exit Function
ElseIf EntryToDel = NbEntries Then
    'Si on doit supprimer la derni�re entr�e, chercher le s�parateur n-1
    i = InStrRev(Matrice, SepMatrice)
    DeleteEntry = Mid$(Matrice, 1, i - 1)
    Exit Function
ElseIf EntryToDel = 1 Then
    '-> Si on doit supprimer la premi�re entr�e, chercher le premier s�parateur
    i = InStr(1, Matrice, SepMatrice)
    DeleteEntry = Mid$(Matrice, i + 1, Len(Matrice) - i)
    Exit Function
Else
    '-> Suppression d'une entr�e
    j = 1
    i = 0
    NbEntries = 0
    Do
        i = InStr(j, Matrice, SepMatrice)
        NbEntries = NbEntries + 1
        If NbEntries = EntryToDel - 1 Then
            'Lecture de la position de d�but
            CHarDeb = i
            CharEnd = InStr(i + 1, Matrice, SepMatrice)
            DeleteEntry = Mid$(Matrice, 1, CHarDeb - 1) & Mid$(Matrice, CharEnd, Len(Matrice) - CharEnd + 1)
            Exit Function
        End If
        j = i + 1
    Loop
End If
    

End Function


Public Function GetEntryIndex(ByVal Matrice As String, ByVal EntryToSearch As String, ByVal SepMatrice As String) As Integer

'---> Cette fonction retourne l'index d'une valeur dans une matrice. _
Attention : Cette fonction retourne la premi�re it�ration trouv�e

On Error Resume Next

Dim i As Integer, j As Integer
Dim NbEntries As Integer

'-> Recherche de la pchaine de caract�re
i = InStr(1, UCase$(Matrice), UCase$(EntryToSearch))
j = 1
NbEntries = 1
'-> Analyse
Do While j < i
    j = InStr(j, Matrice, SepMatrice)
    NbEntries = NbEntries + 1
    j = j + 1
Loop

GetEntryIndex = NbEntries


End Function

Public Function IsEntryInMatrice(ByVal Matrice As String, EntryToSearch As String, ByVal SepMatrice As String) As Boolean

'---> Cette fonction indique si une entr�e fait partie d'une matrice

On Error Resume Next

Dim i As Integer

If Trim(Matrice) = "" Then Exit Function
If Trim(EntryToSearch) = "" Then Exit Function

For i = 1 To NumEntries(Matrice, SepMatrice)
    If UCase$(Trim(Entry(i, Matrice, SepMatrice))) = UCase$(Trim(EntryToSearch)) Then
        IsEntryInMatrice = True
        Exit Function
    End If
Next

End Function

Public Function IsLegalName(ByVal NewName As String, Optional Tiret As Boolean, Optional CheckXML As Boolean, Optional CheckOnlyXML As Boolean) As Boolean

'---> Fonction qui v�rifie le contenu d'un nom pour y d�tecter tous les caract�res interdits
' Caract�res interdits  \ / : * " < > | et - sur option

Dim FindBad As Boolean

'-> Analyse de tous les caract�res
If Not CheckOnlyXML Then
    If InStr(1, NewName, "\") <> 0 Then FindBad = True
    If InStr(1, NewName, "/") <> 0 Then FindBad = True
    If InStr(1, NewName, ":") <> 0 Then FindBad = True
    If InStr(1, NewName, "*") <> 0 Then FindBad = True
    If InStr(1, NewName, """") <> 0 Then FindBad = True
    If InStr(1, NewName, "<") <> 0 Then FindBad = True
    If InStr(1, NewName, ">") <> 0 Then FindBad = True
    If InStr(1, NewName, "|") <> 0 Then FindBad = True
    If Tiret And InStr(1, NewName, "-") <> 0 Then FindBad = True
End If
If CheckXML Then
    If InStr(1, NewName, "<") <> 0 Then FindBad = True
    If InStr(1, NewName, ">") <> 0 Then FindBad = True
End If

'-> Retourner la valeur
IsLegalName = Not FindBad

End Function


Public Function PrintAsciiByte(hdlFile As Integer, strToPrint As String)

Dim i As Integer
Dim aInt As Integer

For i = 1 To Len(strToPrint)
    aInt = Asc(Mid$(strToPrint, i, 1))
    Put #hdlFile, , aInt + 100
Next

End Function

Public Function GetAsciiByte(hdlFile As Integer, Max As Integer) As String

'---> Convertir la chaine
Dim Reponse As String
Dim i As Integer
Dim aInt As Integer

For i = 1 To Max
    '-> Lecture de la valeur du fichier
    Get #hdlFile, , aInt
    '-> Convertir en Ascii
    Reponse = Reponse & Chr(aInt - 100)
Next 'Pour tous les caract�res

'-> Retourner la valeur
GetAsciiByte = Reponse

End Function

Public Sub SaveBinValue(hdlFile As Integer, CodeEnreg As Integer, ValueEnreg As String)

Put #hdlFile, , CodeEnreg
Put #hdlFile, , CInt(Len(ValueEnreg))
PrintAsciiByte hdlFile, ValueEnreg

End Sub


Public Function KillSelectedFile(strFile As String) As Boolean

'---> Cette proc�dure supprime un fichier et renvoie une valeur indiquant

On Error GoTo GestError

'-> Supprimer un fichier
Kill strFile

'-> Renvoyer une valeur de succ�s
KillSelectedFile = True

'-> Quitter la fonction
Exit Function

GestError:
    

End Function
