VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "dc_field"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'**************************************************************************
'* Description de la structure des donn�es qui sont lues dans le fichier  *
'**************************************************************************

'---> Classe de description d'un enregistrement appartient � dc_Enreg
Public Enum pFieldStatut
    pNormal 'enreg juste ouvet
    pModif  'enreg existant modifi�
    pDelete 'enreg supprim�
    pCrea  'enreg qui a ete cree
    pCours 'enreg juste cree sans aucune verification ni passage
    pNone
End Enum

Public Value As String 'Valeur du champ
Public Struct As String  'Cl� de la struccture en clair la d�finition du champ
Public KeyWord As String 'la cl�
Public IsType As Boolean 'si doit etre afficher dans le treeview
Public Niveau As Single 'niveau de l'enregistrement
Public Statut As pFieldStatut

Public Function IsField(strKey As String) As Boolean
'-> cette fonction permet de savoir si un �l�ment appartient � la structure
Dim afield As dc_field

On Error GoTo GestError
Set afield = Fields.Item(strKey)
IsField = True
Set afield = Nothing
Exit Function

GestError:
IsField = False
End Function

Public Function NextField(strKey As String) As dc_field

Dim afield As dc_field
Dim TopNext As Boolean

For Each afield In Fields
    If afield.KeyWord = strKey Then
        TopNext = True
    End If
    If TopNext = True Then
        Set NextField = afield
        Exit For
    End If
Next

End Function
