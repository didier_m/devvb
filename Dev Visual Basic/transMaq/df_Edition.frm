VERSION 5.00
Object = "{DAFD85F1-343B-47EE-8170-1C26723F7A5D}#2.0#0"; "dogskin.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form df_Edition 
   BackColor       =   &H00CBB49A&
   BorderStyle     =   0  'None
   Caption         =   "Saisie des selections"
   ClientHeight    =   2250
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5925
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   2250
   ScaleWidth      =   5925
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture2 
      Height          =   255
      Left            =   2400
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   5
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin DogSkin.DealCheckBox DealCheckBox1 
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   1800
      Width           =   2775
      _ExtentX        =   4895
      _ExtentY        =   450
      BackColor       =   16579059
      Caption         =   "Format turbo"
      ForeColor       =   -2147483630
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   1
      Checked         =   0   'False
      SendToNextControl=   -1  'True
   End
   Begin DogSkin.DealCmdButton Picture1 
      Height          =   615
      Left            =   4560
      TabIndex        =   1
      Tag             =   "DICTAB�BUTTON-OK"
      Top             =   1500
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   1
      ButtonForm      =   1
      BackColor       =   16579059
   End
   Begin DogSkin.DogSkinObject DogSkinObject1 
      Left            =   0
      Top             =   0
      _ExtentX        =   979
      _ExtentY        =   953
      BorderColor     =   13077835
      BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderForeColor =   10053171
      HeaderBackColor =   16637889
      HeaderLineColor =   15847345
      HeaderUseLine   =   -1  'True
      BeginProperty BodyFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BodyForeColor   =   10053171
      BodyBackColor   =   16579059
      HeaderIco       =   "df_Edition.frx":0000
      HeaderIcoNa     =   "df_Edition.frx":0682
      ShadowColor     =   15847345
   End
   Begin DogSkin.DealCmdButton Picture3 
      Height          =   615
      Left            =   5160
      TabIndex        =   2
      Tag             =   "DICTAB�BUTTON-CANCEL"
      Top             =   1500
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   2
      ButtonForm      =   1
      BackColor       =   16579059
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   5715
      Left            =   120
      TabIndex        =   3
      Top             =   2280
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   10081
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      SmallIcons      =   "ImageList2"
      ForeColor       =   -2147483640
      BackColor       =   16777215
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin MSComctlLib.ImageCombo Combo1 
      Height          =   330
      Left            =   360
      TabIndex        =   0
      Tag             =   "DICT�VALIDE"
      Top             =   840
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   582
      _Version        =   393216
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00C78D4B&
      Height          =   855
      Left            =   120
      Top             =   600
      Width           =   5655
   End
End
Attribute VB_Name = "df_Edition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim LigneEntete(5000) As String
Dim TypeCombo As TypCombo
Enum TypCombo
    pExcel
    pAjout
End Enum

Private Sub Combo1_KeyPress(KeyAscii As Integer)
Dim aItem As ComboItem


If KeyAscii = 13 Then
    '-> on lance l'edition selon la selection effectu�e
Else
    KeyAscii = 0
End If

End Sub

Private Sub Combo1_KeyUp(KeyCode As Integer, Shift As Integer)
ComboSaisieAuto Combo1, False
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
    Case 113 'F2 validation
    
    Case 115, 27 'F4 ou esc annulation bon
        Unload Me
End Select

End Sub

Private Sub Form_Load()

'---> Init du skin
Me.DogSkinObject1.Initialisation False, "S�lection du type d'�dition", pCloseOnly, False
Me.DogSkinObject1.SetMatriceEffect "SHAPE1"
'-> on charge la couleur de fond desbouttons
Me.Picture1.BackColor = Me.DogSkinObject1.BodyBackColor
Me.Picture3.BackColor = Me.DogSkinObject1.BodyBackColor

appTurbo = searchExe("DadsuTurbo.ini")

End Sub

Private Sub Picture1_Click()

'--> selon le type de chargement de l'ecran

Select Case TypeCombo
    Case pExcel
        '-> on lance l'�dition selon les options choisis
        If NumEntries(Me.Combo1.SelectedItem.Tag, "|") = 1 Then
            Call ListViewExport(Me.Combo1.SelectedItem.Tag)
        Else
            '-> c'est un spool turbo
            Call ListViewTurbo(Me.Combo1.SelectedItem.Tag)
        End If
End Select

Unload Me

End Sub

Private Function isCol(strKey As String) As Boolean
'--> cette fonction permet de savoir si la colonne existe
Dim i As Integer
On Error GoTo GestError
i = Me.ListView1.ColumnHeaders(strKey).Index
'-> valeur de succes
isCol = True

Exit Function
GestError:
End Function

Private Sub ListViewExport(strKeyword As String)
'--> lance l'edition sur les salari�s
Dim aItem As ListItem
Dim afield As dc_field
Dim afield2 As dc_field
Dim aCol As ColumnHeader
Dim curNiveau As Integer
Dim curKeyWord As String
Dim Niveau As Variant
Dim strTemp As Variant
Dim signe As Integer
Dim i As Integer

'-> on affiche le timer
df_Drloading.Show
df_Drloading.Label1.Caption = "Export des donn�es en cours..."

'-> on vide le listview
Me.ListView1.ColumnHeaders.Clear
Me.ListView1.ListItems.Clear

On Error Resume Next

'-> on charge les entetes en fonction du param�trage
If Not LoadExport(strKeyword) Then GoTo GestError
strKeyword = strRetour
curNiveau = Structures(strKeyword).FieldNiveau

Set afield2 = New dc_field

'-> on charge le listview en commencant par les informations de plus bas niveau
For Each afield In Fields
    DoEvents
    '-> on regarde si on est sur une nouvelle entr�e de ligne
    If afield.Struct = strKeyword And afield.Statut <> pDelete Then
        Set aItem = Me.ListView1.ListItems.Add(, , "")
        '-> on en profite pour charger les informations transversales de plus haut niveau car
        '   on est d�j� pass� dessus on utilise pour cela l'index
        For Each aCol In Me.ListView1.ColumnHeaders
            '-> on pointe sur les colonnes de niveau inferieur
            If Structures(aCol.Key).FieldNiveau < curNiveau Then
                '-> on se met la cle dans un vecteur pour pouvoir travailler dessus
                Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
                '-> on se charge le vecteur a partir de la cl�
                strTemp = Split(afield.KeyWord, "|")
                For i = 1 To 7
                    If i <= Structures(aCol.Key).FieldNiveau Then
                        Niveau(i) = CSng(strTemp(i))
                    Else
                        Niveau(i) = 0
                    End If
                Next
                curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
                '-> on charge la ligne avec la bonne valeur
                Do While afield.IsField(curKeyWord)
                    If Fields(curKeyWord).Struct = aCol.Key Then
                        '-> on s'occupe du signe de la valeur
                        signe = 1
                        If afield.IsField("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & (Niveau(7) + 1)) Then
                            If Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & (Niveau(7) + 1)).Value = "N" Then signe = -1
                        End If
                        If aCol.Index <> 1 Then
                            If signe <> 1 Then
                                aItem.SubItems(aCol.Index - 1) = "-" & Fields(curKeyWord).Value
                            Else
                                aItem.SubItems(aCol.Index - 1) = Fields(curKeyWord).Value
                            End If
                        Else
                            If signe <> 1 Then
                                aItem.Text = "-" & Fields(curKeyWord).Value
                            Else
                                aItem.Text = Fields(curKeyWord).Value
                            End If
                        End If
                        Exit Do
                    End If
                    '-> on passe a l'enreg suivant
                    Niveau(7) = Niveau(7) + 1
                    curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
                Loop
            End If
        Next
    End If
    '-> on ecrit les infos sur la ligne si la colonne est pr�sente dans le listview
    If isCol(afield.Struct) Then
        If afield.Niveau >= curNiveau Then
            '-> on se met la cle dans un vecteur pour pouvoir travailler dessus
            Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
            '-> on se charge le vecteur a partir de la cl�
            strTemp = Split(afield.KeyWord, "|")
            For i = 1 To 7
                If i <= Structures(aCol.Key).FieldNiveau Then
                    Niveau(i) = CSng(strTemp(i))
                Else
                    Niveau(i) = 0
                End If
            Next
            curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
            '-> on s'occupe du signe de la valeur
            signe = 1
            If afield.IsField("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & (Niveau(7) + 1)) Then
                If Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & (Niveau(7) + 1)).Value = "N" _
                    And Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & (Niveau(7) + 1)).Statut <> pDelete Then signe = -1
            End If
            If ListView1.ColumnHeaders(afield.Struct).Index <> 1 Then
                If signe <> 1 Then
                    aItem.SubItems(ListView1.ColumnHeaders(afield.Struct).Index - 1) = "-" & afield.Value
                Else
                    aItem.SubItems(ListView1.ColumnHeaders(afield.Struct).Index - 1) = afield.Value
                End If
            Else
                If signe <> 1 Then
                    aItem.Text = "-" & afield.Value
                Else
                    aItem.Text = afield.Value
                End If
            End If
        End If
    End If
Next
If Me.DealCheckBox1.Checked Then
    '-> envoyer le contenu du listview dans turbo
    FormatListView Me.ListView1
    PrintListView Me.ListView1, "toto.turbo"
Else
    '-> envoyer sur excel
    ExportToExcel Me.ListView1
End If

GestError:
'-> on masque le timer
Unload df_Drloading

End Sub

Private Sub ListViewTurbo(strKeyword As String)
'--> lance l'edition dans un spool turbo
'    comme pour excel on se charge les donn�es dans un listview
'    strkeyword     "index;titre;nom de la maquette;debut de ligne"
Dim hdlFic As Integer
Dim hdlFic2 As Integer
Dim TempFileName As String
Dim strTag As String
Dim strText As String
Dim Ligne As String
Dim aItem As ListItem
Dim afield As dc_field
Dim aCol As ColumnHeader
Dim colKey As String
Dim curNiveau As Integer
Dim curKeyWord As String
Dim Niveau As Variant
Dim strTemp As Variant
Dim i As Integer
Dim j As Integer
Dim aNode As Node
Dim aNode2 As Node
Dim strFormat As String
Dim strOperation As String
Dim strValeur As String
Dim Res As Long
Dim lpBuffer As String
Dim signe As Integer

On Error Resume Next

'-> on affiche le timer
strTag = strKeyword
df_Drloading.Show
df_Drloading.Label1.Caption = "G�n�ration du spool d'impression en cours..."

'-> on vide le listview
Me.ListView1.ColumnHeaders.Clear
Me.ListView1.ListItems.Clear

'-> on charge les entetes en fonction du param�trage
If Not LoadExport(strKeyword) Then GoTo GestError
strKeyword = strRetour
curNiveau = Structures(strKeyword).FieldNiveau

'-> on charge les infos de la maquette
If Not LoadTagTurbo(strTag) Then GoTo GestError
strTag = strRetour

'-> on charge le listview en commencant par les informations de plus bas niveau
For Each afield In Fields
    DoEvents
    '-> on regarde si on est sur une nouvelle entr�e de ligne
    If afield.Struct = strKeyword And afield.Statut <> pDelete Then
        Set aItem = Me.ListView1.ListItems.Add(, , "")
        '-> on en profite pour charger les informations transversales de plus haut niveau car
        '   on est d�j� pass� dessus on utilise pour cela l'index
        For Each aCol In Me.ListView1.ColumnHeaders
            If InStr(1, aCol.Key, "�") <> 0 Then
                strFormat = "." & String(Val(Entry(1, Entry(2, aCol.Key, "�"), "&")), "0")
                If strFormat = "." Then strFormat = ""
            End If
            For j = 1 To NumEntries(aCol.Key, "&")
                colKey = Entry(j, aCol.Key, "&")
                If InStr(1, colKey, "$") <> 0 Then
                    strOperation = Entry(2, colKey, "$")
                Else
                    strOperation = ""
                End If
                If Structures(1).IsStructure(colKey) Then
                    '-> on pointe sur les colonnes de niveau supeireur
                    If Structures(colKey).FieldNiveau <= curNiveau Then
                        '-> on se met la cle dans un vecteur pour pouvoir travailler dessus
                        Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
                        '-> on se charge le vecteur a partir de la cl�
                        strTemp = Split(afield.KeyWord, "|")
                        For i = 1 To 7
                            If i <= Structures(colKey).FieldNiveau Then
                                Niveau(i) = CSng(strTemp(i))
                            Else
                                Niveau(i) = 0
                            End If
                        Next
                        curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
                        '-> on charge la ligne avec la bonne valeur
                        Do While afield.IsField(curKeyWord)
                            If Fields(curKeyWord).Struct = colKey Then
                                '-> on regarde si on a une valeur de correspondance pour cette valeur
                                strText = GetTrueValue(Fields(curKeyWord).Struct & ":" & aCol.Text, Fields(curKeyWord).Value)
                                If aCol.Index <> 1 Then
                                    If InStr(1, aCol.Key, "�") <> 0 Then
                                        '-> on regarde si la valeur � un signe
                                        signe = 1
                                        If afield.IsField("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & (Niveau(7) + 1)) Then
                                            If Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & (Niveau(7) + 1)).Value = "N" Then signe = -1
                                        End If
                                        aItem.SubItems(aCol.Index - 1) = Val(aItem.SubItems(aCol.Index - 1)) + Val(Fields(curKeyWord).Value * signe)
                                    Else
                                        aItem.SubItems(aCol.Index - 1) = aItem.SubItems(aCol.Index - 1) & strText
                                    End If
                                Else
                                    If InStr(1, aCol.Key, "�") <> 0 Then
                                        aItem.Text = Format(Val(aItem.Text) + Val(Fields(curKeyWord).Value), "#########0" & strFormat)
                                    Else
                                        aItem.Text = aItem.Text & strText
                                    End If
                                End If
                                Exit Do
                            End If
                            '-> on passe a l'enreg suivant
                            Niveau(7) = Niveau(7) + 1
                            curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
                        Loop
                    Else
                        '-> on est ici dans le cas de niveau inferieur en principe c'est pour des cumul
                        Set aNode = df_Dadsu.TreeView1.Nodes(afield.KeyWord)
                        If aNode.Children <> 0 Then
                            Set aNode2 = aNode.Child
                            Do While aNode2 <> aNode.Next
                                '-> on se met la cle dans un vecteur pour pouvoir travailler dessus
                                Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
                                '-> on se charge le vecteur a partir de la cl�
                                strTemp = Split(aNode2.Key, "|")
                                For i = 1 To 7
                                    Niveau(i) = CSng(strTemp(i))
                                Next
                                curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
                                '-> on charge la ligne avec la bonne valeur
                                Do While afield.IsField(curKeyWord)
                                    If Fields(curKeyWord).Struct = colKey Then
                                        '-> on regarde si on a une valeur de correspondance pour cette valeur
                                        strText = GetTrueValue(Fields(curKeyWord).Struct & ":" & aCol.Text, Fields(curKeyWord).Value)
                                        If aCol.Index <> 1 Then
                                            If InStr(1, aCol.Key, "�") <> 0 Then
                                                '-> on regarde si la valeur � un signe
                                                signe = 1
                                                If afield.IsField("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & (Niveau(7) + 1)) Then
                                                    If Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & (Niveau(7) + 1)).Value = "N" Then signe = -1
                                                End If
                                                aItem.SubItems(aCol.Index - 1) = Val(aItem.SubItems(aCol.Index - 1)) + Val(Fields(curKeyWord).Value * signe)
                                            Else
                                                aItem.SubItems(aCol.Index - 1) = aItem.SubItems(aCol.Index - 1) & strText
                                            End If
                                        Else
                                            If InStr(1, aCol.Key, "�") <> 0 Then
                                                aItem.Text = Format(Val(aItem.Text) + Val(Fields(curKeyWord).Value), "#########0" & strFormat)
                                            Else
                                                aItem.Text = aItem.Text & strText
                                            End If
                                        End If
                                        Exit Do
                                    End If
                                    '-> on passe a l'enreg suivant
                                    Niveau(7) = Niveau(7) + 1
                                    curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
                                Loop
                                '-> on se parcour les sous noeud eventuels
                                If aNode2.Children <> 0 Then
                                    Set aNode2 = aNode2.Child
                                Else
                                    '-> on regarde si il y a qque chose sur la meme hauteur sinon on remonte
                                    If aNode2.Next Is Nothing Then
                                        Set aNode2 = aNode2.Parent
                                        Set aNode2 = aNode2.Next
                                        If aNode2 Is Nothing Then Exit Do
                                    Else
                                        Set aNode2 = aNode2.Next
                                    End If
                                End If
                            Loop
                        End If
                    End If
                Else '-> cest juste du texte a ajouter
                    If aCol.Index <> 1 Then
                        If strOperation <> "" And InStr(1, colKey, "^") <> 0 Then
                            Select Case Entry(1, strOperation, "^")
                                Case "0" '+
                                    strValeur = Val(aItem.SubItems(aCol.Index - 1)) + Val(Entry(2, strOperation, "^"))
                                Case "1" '-
                                    strValeur = Val(aItem.SubItems(aCol.Index - 1)) - Val(Entry(2, strOperation, "^"))
                                Case "2" '/
                                    strValeur = Val(aItem.SubItems(aCol.Index - 1)) / Val(Entry(2, strOperation, "^"))
                                Case "3" '*
                                    strValeur = Val(aItem.SubItems(aCol.Index - 1)) * Val(Entry(2, strOperation, "^"))
                            End Select
                        Else
                            strValeur = aItem.SubItems(aCol.Index - 1)
                        End If
                        If InStr(1, aCol.Key, "�") <> 0 Then
                            If Not IsNumeric(strValeur) Then strValeur = "0"
                            aItem.SubItems(aCol.Index - 1) = Format(CDbl(strValeur) + Val(colKey), "#########0" & strFormat)
                        Else
                            aItem.SubItems(aCol.Index - 1) = strValeur & colKey
                        End If
                    Else
                        If strOperation <> "" And InStr(1, colKey, "^") <> 0 Then
                            Select Case Entry(1, strOperation, "^")
                                Case "0" '+
                                    strValeur = Val(aItem.Text) + Val(Entry(2, strOperation, "^"))
                                Case "1" '-
                                    strValeur = Val(aItem.Text) - Val(Entry(2, strOperation, "^"))
                                Case "2" '/
                                    strValeur = Val(aItem.Text) / Val(Entry(2, strOperation, "^"))
                                Case "3" '*
                                    strValeur = Val(aItem.Text) * Val(Entry(2, strOperation, "^"))
                            End Select
                        Else
                            strValeur = aItem.Text
                        End If
                        If InStr(1, aCol.Key, "�") <> 0 Then
                            If Not IsNumeric(strValeur) Then strValeur = "0"
                            aItem.Text = Format(CDbl(strValeur) + Val(colKey), "#########0" & strFormat)
                        Else
                            aItem.Text = strValeur & colKey
                        End If
                    End If
                End If
            Next
        Next
    End If
'    '-> on ecrit les infos sur la ligne si la colonne est pr�sente dans le listview
'    If isCol(afield.Struct) Then
'        If afield.Niveau >= curNiveau Then
'            '-> on regarde si on a une valeur de correspondance pour cette valeur
'            strText = GetTrueValue(afield.Struct, afield.Value)
'            If ListView1.ColumnHeaders(afield.Struct).Index <> 1 Then
'                aItem.SubItems(ListView1.ColumnHeaders(afield.Struct).Index - 1) = strText
'            Else
'                aItem.Text = strText
'            End If
'        End If
'    End If
Next

'-> on genere maintenant le spool turbo
'-> on v�rifie l'existance de la maquette
'-> V�rifier que l'on trouve le fichier de structure
lastMaquette = searchExe(Entry(2, strTag, "|"))
If Dir$(lastMaquette) = "" Then
    DisplayMessage "La maquette " & Chr(13) & Entry(2, strTag, "|") & Chr(13) & "est introuvable.", dsmCritical, dsmOkOnly, "G�n�ration du spool d'impression"
    GoTo GestError
End If

'-> on se cree un fichier temporaire turbo
TempFileName = GetTempFileNameVB("tur") & ".turbo"

'-> Ouvrir le fichier spool
hdlFic = FreeFile
Open TempFileName For Append As #hdlFic

'ouvrir le fichier maquette
hdlFic2 = FreeFile
Open lastMaquette For Input As #hdlFic2

Print #hdlFic, "[SPOOL]"
Print #hdlFic, "[MAQ]"

'-> on ecrit les donn�es de la maquette dans le spool
Do While Not EOF(hdlFic2)
    '-> Lecture de la ligne de la maquette
    Line Input #hdlFic2, Ligne
    '-> Ecriture dans le fichier spool
    Print #hdlFic, Ligne
Loop

'-> on �crit le marqueur de fin de maquette
Print #hdlFic, "[/MAQ]"

'-> on cree les differentes pages du spool
For Each aItem In Me.ListView1.ListItems
    '-> on construit la ligne des donn�es
    Ligne = Entry(3, strTag, "|") & "{"
    For Each aCol In Me.ListView1.ColumnHeaders
        If aCol.Index = 1 Then
            Ligne = Ligne & "^" & aCol.Text & aItem.Text
        Else
            Ligne = Ligne & "^" & aCol.Text & aItem.SubItems(aCol.Index - 1)
        End If
    Next
    '-> on se rajoute la date cela peu servir
    Ligne = Ligne & "^DA  " & Format(Now, "d mmmm yyyy")
    '-> on ecrit la ligne dans le spool
    Ligne = Ligne & "}"
    Print #hdlFic, Ligne
    '-> on ecrit une nouvelle page
    Print #hdlFic, "[PAGE]"
Next

'-> fermer le fichier spool
Close #hdlFic
Close #hdlFic2

'-> si on trouve la variable d'environnement dealtempo on l'enregisstre
lpBuffer = Space$(255)
Res = GetEnvironmentVariable("DEALTEMPO", lpBuffer, Len(lpBuffer))
If Res <> 0 Then
    FileCopy TempFileName, Mid$(lpBuffer, 1, Res) & "/" & "Dadsu" & Format(Now, "yyyyMMddhhmmss") & ".turbo"
End If


'-> ouvrir le spool avec l'editeur associ� au .turbo soit turbograph
ShellExecute Me.hwnd, "Open", TempFileName, vbNullString, App.Path, 1

GestError:
'-> on masque le timer
Unload df_Drloading
Exit Sub

'-> gestion des erreur
GestError2:
Unload df_Drloading
DisplayMessage "Une erreur est survenue sur l'export", dsmCritical, dsmOkOnly, "G�n�ration du spool d'impression"
End Sub

Private Function GetTrueValue(ByRef strKeyword As String, ByRef strValue As String) As String
'--> cette fonction va permettre de ramener une valeur de correspondance
GetTrueValue = GetIniFileValue(strKeyword, strValue, appTurbo)

'-> si on a rien ramen� charg� avec la valeur initiale
If GetTrueValue = "" Then GetTrueValue = strValue

End Function


Private Sub Picture3_Click()

'-> on quitte sans rien faire
Unload Me

End Sub

Public Sub PrintListView(ByRef aList As ListView, ByRef Spool As String, Optional Entete As String, Optional RetourParam As String)
    '-> Fonction qui imprime un Listview centr� sur une feuille A4 en l'envoyant sous turbo
    '-> Spool = chemin & nom de fichier turbo
    Dim MaqTurbo As Integer
    Dim LigneFic As String
    Dim LigneFic2(500) As String
    Dim IndSaut As Integer
    Dim i As Integer
    Dim j As Integer
    Dim Fichier As String
    Dim Portrait As Boolean
    Dim HauteurGrid As Long
    Dim LargeurGrid As Long
    Dim MargeTopEntete As Integer
    Dim MiseEnPage As String
    Dim Hauteur As Variant
    Dim HauteurRef As Long
    Dim k As Integer
    Dim l As Integer
    Dim TopPage As Boolean
    Dim NbPages As String
    Dim NoPage As Integer
    Dim IndEnt As Integer
    Dim aItem As ListItem
    Dim ColRupt As Integer
    Dim StrRupt As String
    Dim CurRupt As String
    Dim z As Integer
    
    NoPage = 1
    IndSaut = 0

    '-> si pas de ligne ne rien faire
    If aList.ListItems.Count = 0 Then Exit Sub
    
    On Error Resume Next
    
    '-> si le spool existe on le supprime
    If Dir(Spool) <> "" Then Kill Spool

    '-> On ouvre un fichier pour creer la maquette
    MaqTurbo = FreeFile
    Fichier = Spool
    Open Fichier For Append As #MaqTurbo
    '-> Calcul de la hauteur du grid
    For i = 0 To aList.ListItems.Count - 1
        HauteurGrid = HauteurGrid + aList.ListItems(1).Height
    Next
    '-> Calcul de la largeur grid
    For i = 1 To aList.ColumnHeaders.Count
        LargeurGrid = LargeurGrid + aList.ColumnHeaders.Item(i).Width
    Next

    '-> Definition du format de mise en page (portrait ou paysage )
    Portrait = False

    '-> ***** On envoie les renseignements de base pour la maquette (entete fichier)
    LigneFic = "[SPOOL]"
    Print #MaqTurbo, LigneFic
    LigneFic = "[MAQ]"
    Print #MaqTurbo, LigneFic
    LigneFic = "\DefEntete�Begin"
    Print #MaqTurbo, LigneFic
    LigneFic = "\Version�2"
    Print #MaqTurbo, LigneFic
    LigneFic = "\FieldListe�"
    Print #MaqTurbo, LigneFic
    LigneFic = "\Nom�" & Spool
    Print #MaqTurbo, LigneFic
    LigneFic = "\Date�" & Now
    Print #MaqTurbo, LigneFic
    Print #MaqTurbo, "\Description�Pas de Description"
    Print #MaqTurbo, "\Largeur�21"
    Print #MaqTurbo, "\Hauteur�29.7"

    '-> On decide si on place le grid en portrait ou paysage selon sa largeur
    If Portrait = True Then
        Print #MaqTurbo, "\Orientation�1" '-> Portrait
        MiseEnPage = 1
    Else
        Print #MaqTurbo, "\Orientation�0" '-> Paysage
        MiseEnPage = 0
    End If
    Entete = "O"  'essai
    RetourParam = ",,,O"
    '-> Imprime une entete (page 0) si un titre a ete passe en parametre
    If Entete <> "" Then IndEnt = Printentete(MaqTurbo, MiseEnPage, Entete, Me.Picture2.ScaleX(LargeurGrid, 3, 7), aList)
    Entete = strRetour
    
    If Entete <> "" Then
        MargeTopEntete = 7
    Else
        MargeTopEntete = 0
    End If

    If Portrait = True Then
        Print #MaqTurbo, "\MargeTop�1" '& Str(MargeTopEntete + 1)
        Print #MaqTurbo, "\MargeLeft�" & Str((21 - Me.Picture2.ScaleX(LargeurGrid, 3, 7)) / 2) '-> On centre horizontalement
    Else
        Print #MaqTurbo, "\MargeTop�1" '& Str(MargeTopEntete + 1)
        If Me.Picture2.ScaleX(LargeurGrid, 3, 7) <= 29.7 - 0.5 Then
            Print #MaqTurbo, "\MargeLeft�" & Str((29.7 - Me.Picture2.ScaleX(LargeurGrid, 3, 7)) / 2 + 0.5)  '-> On centre horizontalement
        Else 'le tableau est plus large on cale sur la droite
            Print #MaqTurbo, "\MargeLeft�" & Str(1.2) '-> On centre horizontalement
        End If
    End If

    Print #MaqTurbo, "\Suite�"
    Print #MaqTurbo, "\Report�"
    Print #MaqTurbo, "\Entete�"
    Print #MaqTurbo, "\Pied�"
    Print #MaqTurbo, "\PageGarde�FAUX"
    Print #MaqTurbo, "\PageSelection�FAUX"
    Print #MaqTurbo, "\RightPage�VRAI"
    Print #MaqTurbo, "\Publipostage�"
    Print #MaqTurbo, "\DefEntete�End"
    Print #MaqTurbo, "[PROGICIEL]"
    Print #MaqTurbo, "\Prog=DEAL"
    Print #MaqTurbo, "\Rub=AUTRES"
    Print #MaqTurbo, "\Cli=DEAL"
    Print #MaqTurbo, "[HTML]"
    Print #MaqTurbo, "\Fichier�"
    Print #MaqTurbo, "\Rupture�"
    Print #MaqTurbo, "\HTML�End"
    '-> ******      Fin de la description entete maquette

    '-> ******      On declare le tableau
    Print #MaqTurbo, "[TB-Tableau]"
    '-> On definit le tableau dans la maquette graphique
    Print #MaqTurbo, "\LargeurTb�0"
    '-> On decide de l'orientation du tableau en fonction de sa largeur
    If Portrait = True Then
        Print #MaqTurbo, "\OrientationTB�1"
    Else
        Print #MaqTurbo, "\OrientationTB�0"
    End If
    '-> ******      Fin de la declaration du tableau
    
    '-> ******      On envoie l'entete (la premiere ligne du tableau) dans le fichier ascii
    If Entete <> "" Then
        For i = 0 To IndEnt
            Print #MaqTurbo, LigneEntete(i)
        Next
    End If
    '-> ******      fin de l'ecriture de l'entete
    
    '-> ******      on cree la ligne correspondant au libell�s de colonnes
    Print #MaqTurbo, "\Begin�Colonnes"
    Print #MaqTurbo, "\Largeur�" & Trim(Str(Me.Picture2.ScaleX(LargeurGrid, 3, 7)))
    Print #MaqTurbo, "\Hauteur�" & Trim(Str(Me.Picture2.ScaleX(aList.ListItems(1).Height, 3, 7) + 0.3))
    Print #MaqTurbo, "\AlignTop�1"
    Print #MaqTurbo, "\Top�0"
    Print #MaqTurbo, "\AlignLeft�0"
    Print #MaqTurbo, "\Left�3"
    Print #MaqTurbo, "\MasterLink�"
    Print #MaqTurbo, "\SlaveLink�"
    Print #MaqTurbo, "\RgChar�"
    Print #MaqTurbo, "\Ligne�1"
    Print #MaqTurbo, "\Colonne�" & aList.ColumnHeaders.Count
    Print #MaqTurbo, "\Varlig�"
    '-> Creation de la ligne de maquette en fonction des colonnes aListgrid
    LigneFic = "\Col�" & Trim(Str(Me.Picture2.ScaleX(aList.ListItems(1).Height, 3, 7))) & "�"
    For j = 0 To aList.ColumnHeaders.Count - 1
        z = GetColumn(aList, j)
        '-> Alignement de la cellule
        If z <> 0 Then
            Select Case aList.ColumnHeaders(z + 1).Alignment
                Case "0"
                    LigneFic = LigneFic & "1" & ";"
                Case "1"
                    LigneFic = LigneFic & "3" & ";"
                Case "2"
                    LigneFic = LigneFic & "2" & ";"
            End Select
        Else
            LigneFic = LigneFic & 1 & ";"
        End If
        '-> texte de la cellule avec gestion des sauts de ligne
        LigneFic = LigneFic & aList.ColumnHeaders(z + 1).Text & ";"
        '-> Largeur de la cellule
        LigneFic = LigneFic & Trim(Str(Me.Picture2.ScaleX(aList.ColumnHeaders(z + 1).Width, 3, 7))) & ";"
        '-> Bordure de la cellule ( x 4 )
        LigneFic = LigneFic & "VRAI,VRAI,VRAI,VRAI" & ";"
        '-> nom de la font
        LigneFic = LigneFic & aList.Font.Name & ";"
        '-> taille de la font
        LigneFic = LigneFic & aList.Font.Size & ";"
        '-> Cellule en gras
        If aList.Font.Bold = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> cellule en italic
        If aList.Font.Italic = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> Cellule en souligne
        If aList.Font.Underline = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> Couleur de police de la cellule
        LigneFic = LigneFic & aList.ForeColor & ";"
        '-> Couleur de fond   de la cellule
        LigneFic = LigneFic & &HFDDFC1 & ";"
        LigneFic = LigneFic & "FAUX;0@0@1|"
    Next j
    '-> on ecrit les infos de la colonne avec les entete des colonnes
    Print #MaqTurbo, LigneFic
    Print #MaqTurbo, "\Champs�"
    Print #MaqTurbo, "\End�Colonnes"
    '->*****        fin des libell�s des colonne
    
    '->*****        on definit la ligne standard du listview avec un champ par colonne
    Print #MaqTurbo, "\Begin�ColonnesStd"
    Print #MaqTurbo, "\Largeur�" & Trim(Str(Me.Picture2.ScaleX(LargeurGrid, 3, 7)))
    Print #MaqTurbo, "\Hauteur�" & Trim(Str(Me.Picture2.ScaleX(aList.ListItems(1).Height, 3, 7)))
    Print #MaqTurbo, "\AlignTop�1"
    Print #MaqTurbo, "\Top�0"
    Print #MaqTurbo, "\AlignLeft�0"
    Print #MaqTurbo, "\Left�2"
    Print #MaqTurbo, "\MasterLink�"
    Print #MaqTurbo, "\SlaveLink�"
    Print #MaqTurbo, "\RgChar�"
    Print #MaqTurbo, "\Ligne�1"
    Print #MaqTurbo, "\Colonne�" & aList.ColumnHeaders.Count
    Print #MaqTurbo, "\Varlig�"
    '-> Creation de la ligne de maquette en fonction des colonnes aListgrid
    '-> on pointe sur la premiere ligne (elles sont toutes identiques!)
    Set aItem = aList.ListItems(1)
    LigneFic = "\Col�" & Trim(Str(Me.Picture2.ScaleX(aItem.Height, 3, 7))) & "�"
    For j = 0 To aList.ColumnHeaders.Count - 1
        z = GetColumn(aList, j)
        '-> Alignement de la cellule
        If z <> 0 Then
            Select Case aList.ColumnHeaders(z + 1).Alignment
                Case "0"
                    LigneFic = LigneFic & "1" & ";"
                Case "1"
                    LigneFic = LigneFic & "3" & ";"
                Case "2"
                    LigneFic = LigneFic & "2" & ";"
            End Select
        Else
            LigneFic = LigneFic & "1" & ";"
        End If
        '-> texte de la cellule en fait le numero de champ correspondant a la colonne
        If z + 1 < 10 Then
            LigneFic = LigneFic & "^000" & (z + 1) & ";"
        Else
            LigneFic = LigneFic & "^00" & (z + 1) & ";"
        End If
        '-> Largeur de la cellule
        LigneFic = LigneFic & Trim(Str(Me.Picture2.ScaleX(aList.ColumnHeaders(z + 1).Width, 3, 7))) & ";"
        '-> Bordure de la cellule ( x 4 )
        LigneFic = LigneFic & "FAUX,VRAI,VRAI,VRAI" & ";"
        '-> nom de la font
        LigneFic = LigneFic & aList.Font.Name & ";"
        '-> taille de la font
        LigneFic = LigneFic & aList.Font.Size & ";"
        '-> Cellule en gras
        If aList.Font.Bold = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> cellule en italic
        If aList.Font.Italic = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> Cellule en souligne
        If aList.Font.Underline = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> Couleur de police de la cellule
        LigneFic = LigneFic & aList.ForeColor & ";"
        '-> Couleur de fond   de la cellule
        LigneFic = LigneFic & &HFFFFFF & ";"
        LigneFic = LigneFic & "FAUX;0@0@1|"
    Next j
    '-> on ecrit les infos de la colonne avec les entete des colonnes
    Print #MaqTurbo, LigneFic
    '-> on cree les champs
    LigneFic = "Champs�"
    For j = 0 To aList.ColumnHeaders.Count - 1
        z = GetColumn(aList, j)
        If z + 1 > 10 Then
            LigneFic = LigneFic & "00" & z + 1 & ";40 |"
        Else
            LigneFic = LigneFic & "000" & z + 1 & ";40 |"
        End If
    Next j
    LigneFic = Mid(LigneFic, 1, Len(LigneFic) - 1)
    Print #MaqTurbo, "\Champs�" & LigneFic
    Print #MaqTurbo, "\End�ColonnesSTD"
    '->*****          Fin de la construction d'une ligne standard ****************
    '-> on termine la maquette
    Print #MaqTurbo, "\Tableau�End"
    Print #MaqTurbo, "[/MAQ]"
    
    '->*****          On charge le tableau avec les donnees des lignes
    '-> hauteur en cours
    If Entete <> "" Then
        Hauteur = 2   '0
    Else
        Hauteur = 2
    End If
    '-> hauteur de reference pour determiner les sauts de pages
    If Portrait = True Then
        HauteurRef = 26.7
    Else
        HauteurRef = 18
    End If
    NbPages = Fix(Me.Picture2.ScaleX(HauteurGrid, 3, 7) / (HauteurRef - 1.9)) + 1
    '-> on charge de toute facon l'entete page et colonne
    'Print #MaqTurbo, "[TB-Tableau(BLK-Entete)][\1]{^0001" & NoPage & "/" & NbPages & Space(5); "}"
    Print #MaqTurbo, "[TB-Tableau(BLK-Colonnes)][\1]"
    '-> on parcour les differentes lignes
    For i = 1 To aList.ListItems.Count
        '-> on pointe sur la ligne
        Set aItem = aList.ListItems(i)
        LigneFic = "{"
        '->on teste si on est sur une ligne de rupture ou pas
        If Trim(UCase(Entry(1, aItem.Key, "|"))) = "RUPTURE" Then
        Else 'cas des lignes normales
            '->creation de la ligne
            For j = 1 To aList.ColumnHeaders.Count
                z = GetColumn(aList, j)
                If z < 10 Then
                    If z = 1 Then
                        LigneFic = LigneFic & "^000" & z & Mid(aItem.Text, 1, 40) & Space(40 - Len(Mid(aItem.Text, 1, 40)))
                    Else
                        LigneFic = LigneFic & "^000" & z & Mid(aItem.ListSubItems(z - 1).Text, 1, 40) & Space(40 - Len(Mid(aItem.ListSubItems(z - 1).Text, 1, 40)))
                    End If
                Else
                    LigneFic = LigneFic & "^00" & z & Mid(aItem.ListSubItems(z - 1).Text, 1, 40) & Space(40 - Len(Mid(aItem.ListSubItems(z - 1).Text, 1, 40)))
                End If
            Next
        End If
        '-> on ecrit la ligne
        Print #MaqTurbo, "[TB-Tableau(BLK-ColonnesStd)][\1]" & LigneFic & "}"
        Hauteur = Hauteur + CVar(Me.Picture2.ScaleX(aItem.Height, 3, 7))
        '-> on regarde si on doit faire un saut de page
        If Hauteur > HauteurRef Then
            Hauteur = 2
            NoPage = NoPage + 1
            Print #MaqTurbo, "[PAGE]"
            'Print #MaqTurbo, "[TB-Tableau(BLK-Entete)][\1]{^0001" & NoPage & "/" & NbPages & Space(5) & "}"
            Print #MaqTurbo, "[TB-Tableau(BLK-Colonnes)][\1]"
        End If
    Next

    '-> On ferme le fichier
    Close MaqTurbo

    '->on lance l'edition par turbograph
    '-> ouvrir le spool avec l'editeur associ� au .turbo soit turbograph
    ShellExecute Me.hwnd, "Open", Fichier, vbNullString, App.Path, 1
End Sub

Public Function Printentete(ByVal MaqTurbo As Integer, ByVal MiseEnPage As String, Optional RetourParam As String, Optional LargeurGrid As Integer, Optional aList As ListView) As Integer
'-> Fonction qui edite une entete d'edition au format TURBO
Dim i As Integer
Dim j As Integer
Dim Largeur As Integer
Dim IndEnt As Integer
Dim NbRupt As String
Dim ColRupt As String

i = 0
IndEnt = 0
If MiseEnPage = 0 Then
    Largeur = 29.7
Else
    Largeur = 21
End If

'-> on charge le texte de l'entete
strRetour = ""
'-> on cherche si il y a des ruptures

For i = 1 To NumEntries(ColRupt, "|") - 1
    NbRupt = NbRupt + "/ " + aList.ColumnHeaders(CInt(Entry(i, ColRupt, "|"))).Text
Next
If NbRupt <> "" Then NbRupt = "   Rupture par :  " & Mid(NbRupt, 2, 300)
strRetour = strRetour + NbRupt
'-> On construit la ligne d'ent�te
LigneEntete(IndEnt) = "\Begin�Entete"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Largeur�" & Trim(Str(LargeurGrid))
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Hauteur�2"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\AlignTop�1"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Top�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\AlignLeft�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Left�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\MasterLink�"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\SlaveLink�"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\RgChar�"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Ligne�1"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Colonne�1"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Varlig�"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\ExportLig�"
IndEnt = IndEnt + 1
'-> hauteur
LigneEntete(IndEnt) = "\Col�2"
'-> alignement
LigneEntete(IndEnt) = LigneEntete(IndEnt) & "�4;"
'-> texte
LigneEntete(IndEnt) = LigneEntete(IndEnt) & strRetour & "        Page : ^0001;"
'-> largeur
If LargeurGrid > 27 Then
    LigneEntete(IndEnt) = LigneEntete(IndEnt) & Trim(Str(27)) & ";"
Else
    LigneEntete(IndEnt) = LigneEntete(IndEnt) & Trim(Str(LargeurGrid)) & ";"
End If
LigneEntete(IndEnt) = LigneEntete(IndEnt) & "FAUX,FAUX,FAUX,FAUX;Comic Sans MS;10;VRAI;FAUX;FAUX;0;16777215;VRAI;0@0@1;0@1"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Champs�0001;5 "
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\End�Entete"
IndEnt = IndEnt + 1
'-> Fin de l'entete

Printentete = IndEnt

End Function

Private Function GetColumn(aList As ListView, Col As Integer) As Integer
'--> cette fonction recherche une colonne par rapport a sa position d'affichage et non pas par son indes
Dim i As Integer

'-> on parcourt les colonnes
For i = 1 To aList.ColumnHeaders.Count
    '-> on regarde la position
    If aList.ColumnHeaders(i).position - 1 = Col Then
        GetColumn = aList.ColumnHeaders(i).SubItemIndex
        Exit For
    End If
    If i = aList.ColumnHeaders.Count Then GetColumn = i
Next

End Function

Private Sub ComboChargeTag(Combo As ImageCombo, Separateur As String, Optional NoShowTag As Boolean)
'--> cette proc�dure permet de charger les tags pours une combo en fonction
'    de la valeur du texte du type "tag - libelle tag" et si demand� on sort le tag du texte de la combo
Dim aItem As ComboItem

'-> on parcourt tous les element de la combo
For Each aItem In Combo.ComboItems
    '-> on affecte le tag
    aItem.Tag = Mid(Trim(Entry(1, aItem.Text, Separateur)), 1, 5)
    '-> si demand� on n'affiche pas le tag dans la combo
    If NoShowTag Then aItem.Text = Trim(Entry(2, aItem.Text, Separateur))
Next

End Sub

Public Sub ComboSaisieAuto(aCombo As ImageCombo, SaisieLibre As Boolean)
'--> cette proc�dure permet la saisie automatique dans une combo
Dim i As Long
Dim sel As Long
Dim aItem As ComboItem

'-> on parcours les elements de la combo
For Each aItem In aCombo.ComboItems
    sel = Len(aCombo.Text)
    '-> si on trouve le texte on l'affiche
    If (StrComp(Left$(aItem.Text, sel), aCombo.Text, vbTextCompare) = 0) And aCombo.Text <> "" Then
        aItem.Selected = True
        aCombo.SelStart = sel
        If Len(aCombo.Text) <> sel Then
            aCombo.SelLength = Len(aCombo.Text) - sel
        Else
            aCombo.SelStart = 0
            aCombo.SelLength = Len(aCombo.Text)
        End If
        GoTo Suite
    End If
Next
'-> on a rien trouv�
If Not SaisieLibre Then
    aCombo.Text = ""
End If
Suite:

End Sub

Public Sub ExportToExcel(aListView As ListView)

Dim Ligne As String
Dim x As ListItem
Dim aFeuille As Object
Dim aClasseur As Object
Dim MyApp As Object
Dim aRange As Object
Dim i As Integer
Dim aRangeToFormat As Object
Dim aCol As ColumnHeader
Dim ValueField As String
Dim NbCol As Integer
Dim ListViewFormat() As Variant

Screen.MousePointer = 11

'On Error GoTo GestError

If Not IsExcel Then
    Screen.MousePointer = 0
    DisplayMessage "Excel non install�", dsmCritical, dsmOkOnly, ""
    Exit Sub
End If

'-> Cr�er une nouvelle instance d'excel
'ShowWait "Export vers Excel en cours"
Set MyApp = CreateObject("Excel.application")

'-> Ajouter une classeur
Set aClasseur = MyApp.Workbooks.Add()

'-> Supprimer les questions
MyApp.displayalerts = False

'-> Supprimer les 2 feuilles en trop
aClasseur.Sheets(3).Delete
aClasseur.Sheets(2).Delete

'-> Get d'un pointeur vers la feuille active
Set aFeuille = aClasseur.activesheet
Set aRange = aFeuille.Range("$A$1")

'-> Raz de la variable
NbCol = 0

'-> Cr�ation de la ligne d'entete
For Each aCol In aListView.ColumnHeaders
    '-> Cr�ation de la ligne
    Ligne = AddEntryInMatrice(Ligne, aCol.Text, "|")
    NbCol = NbCol + 1
Next 'Pour toutes les colonnes

'-> Cr�er la ligne des entetes
If Ligne <> "" Then
    aRange.Value = Ligne
    '-> Eclater sur les colonnes suivantes
    aRange.TextToColumns Destination:=aFeuille.Range("" & aRange.AddressLocal & ""), DataType:=1, _
            TextQualifier:=-4142, ConsecutiveDelimiter:=False, Tab:=False, _
            Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar:="|"
    '-> S�lectionner la ligne en entier
    Set aRangeToFormat = aFeuille.Range(aRange, aRange.offset(, NbCol - 1))
    '-> Appliquer un format
    ApplicExcelFormat aRangeToFormat, 1
    '-> Vider la matrice
    Ligne = ""
End If 'Si on a trouv� les entetes

'-> valeurs par defaut
ListViewFormat = Array(Array(1, 2), Array(2, 2), Array(3, 2), Array(4, 2), Array(5, 2), Array(6, 2), Array(7, 2), Array(8, 2), Array(9, 2), Array(10, 2), Array(11, 2), Array(12, 2), Array(13, 2), Array(14, 2), Array(15, 2), Array(16, 2), Array(17, 2), Array(18, 2), Array(19, 2), Array(20, 2), Array(21, 2), Array(22, 2), Array(23, 2), Array(24, 2), Array(25, 2), Array(26, 2), Array(27, 2), Array(28, 2), Array(29, 2), Array(30, 2), Array(31, 2), Array(32, 2), Array(33, 2), Array(34, 2), Array(35, 2), Array(36, 2), Array(37, 2), Array(38, 2), Array(39, 2), Array(40, 2), Array(41, 2), Array(42, 2), Array(43, 2), Array(44, 2), Array(45, 2), Array(46, 2), Array(47, 2), Array(48, 2), Array(49, 2), Array(50, 2), Array(51, 2), Array(52, 2), Array(53, 2), Array(54, 2), Array(55, 2), Array(56, 2), Array(57, 2), Array(58, 2), Array(59, 2), Array(60, 2), Array(61, 2), Array(62, 2), Array(63, 2), Array(64, 2), Array(65, 2), Array(66, 2), Array(67, 2), Array(68, 2), Array(69, 2), Array(70, 2) _
 , Array(71, 2), Array(72, 2), Array(73, 2), Array(74, 2), Array(75, 2), Array(76, 2), Array(77, 2), Array(78, 2), Array(79, 2), Array(80, 2), Array(81, 2), Array(82, 2), Array(83, 2), Array(84, 2), Array(85, 2), Array(86, 2), Array(87, 2), Array(88, 2), Array(89, 2), Array(90, 2), Array(91, 2), Array(92, 2), Array(93, 2), Array(94, 2), Array(95, 2), Array(96, 2), Array(97, 2), Array(98, 2), Array(99, 2), Array(100, 2), Array(101, 2), Array(102, 2), Array(103, 2), Array(104, 2), Array(105, 2), Array(106, 2), Array(107, 2), Array(108, 2), Array(109, 2), Array(110, 2), Array(111, 2), Array(112, 2), Array(113, 2), Array(114, 2), Array(115, 2), Array(116, 2), Array(117, 2), Array(118, 2), Array(119, 2), Array(120, 2), Array(121, 2), Array(112, 2), Array(123, 2), Array(124, 2), Array(125, 2), Array(126, 2), Array(127, 2), Array(128, 2), Array(129, 2), Array(130, 2), Array(131, 2), Array(132, 2), Array(133, 2), Array(134, 2), Array(135, 2), Array(136, 2), Array(137, 2), Array(138, 2) _
 , Array(139, 2), Array(140, 2), Array(141, 2), Array(142, 2), Array(143, 2), Array(144, 2), Array(145, 2), Array(146, 2), Array(147, 2), Array(148, 2), Array(149, 2), Array(150, 2), Array(151, 2), Array(152, 2), Array(153, 2), Array(154, 2), Array(155, 2), Array(156, 2), Array(157, 2), Array(158, 2), Array(159, 2), Array(160, 2))
'-> on redimensionne le tableau
ReDim Preserve ListViewFormat(aListView.ColumnHeaders.Count)

'-> on reccupere le format du listview
For Each aCol In aListView.ColumnHeaders
    '-> on redimensionne le tableau en conservant l'aquis
    If aCol.Alignment = 1 Then
        '-> alignement � droite c'est un chiffre
        ListViewFormat(aCol.Index - 1)(1) = 1
    Else
        '-> les autres colonnes c'est du string
        ListViewFormat(aCol.Index - 1)(1) = 2
    End If
Next

'-> S�lectionner la ligne suivante
Set aRange = aFeuille.Range("$A$2")

'-> Exporter tous les enregsitrements
For Each x In aListView.ListItems
    'ShowWait "Export vers Excel en cours " & x.Index & "/" & aListView.ListItems.Count
    '-> Cr�er la ligne � exporter
    For i = 1 To aListView.ColumnHeaders.Count
        '-> Tester la valeur a ajouter
        If i = 1 Then
            ValueField = x.Text
        Else
            ValueField = x.SubItems(i - 1)
        End If
        '-> Ajouter dans la matrice des lignes
        Ligne = Ligne & ValueField & "|"
        'Ligne = AddEntryInMatrice(Ligne, ValueField, "|", CLng(i))
    Next 'Pour tous les champs
    
    '-> Exporter cet enregistrement si <> ""
    If Trim(Ligne) <> "" Then
        '-> Transf�rer la ligne dans la cellule active
        aRange.Value = Ligne
        '-> Eclater sur les colonnes suivantes
        aRange.TextToColumns Destination:=aFeuille.Range("" & aRange.AddressLocal & ""), DataType:=1, _
            TextQualifier:=-4142, ConsecutiveDelimiter:=False, Tab:=False, _
            Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar:="|", FieldInfo:=ListViewFormat
    End If
    '-> D�caler de 1 cellule vers le bas
    Set aRange = aRange.offset(1, 0)
    '-> Raz de la variable
    Ligne = ""
Next 'Pour tous les enregistrements dans la page

'-> Appliquer le format d'un coup
Set aRangeToFormat = aFeuille.Range("$A$2", aFeuille.Range("$A$2").offset(aListView.ListItems.Count - 1, NbCol - 1))
ApplicExcelFormat aRangeToFormat, 12

'-> Formatter les colonnes en largeur automatique
For i = 1 To NbCol
    '-> Largeur automatique de la colonne
    aFeuille.Columns(i).AutoFit
Next

'-> Rendre Excel visible
MyApp.Visible = True

'-> Rendre la main sur les questions
MyApp.displayalerts = True

GestError:
           
    '-> Lib�rer les pointeurs
    Set aRange = Nothing
    Set aRangeToFormat = Nothing
    Set aFeuille = Nothing
    Set aClasseur = Nothing
    Set MyApp = Nothing

    '-> D�bloquer la mise � jour
    Screen.MousePointer = 0
    'ShowWait "Export vers Excel en cours", True
End Sub

Public Function IsExcel() As Boolean

'---> Cette procedure indique si Excel est install� sur le poste ou non

Dim ExcelApp As Object

On Error GoTo GestError

'-> Essayer de pointer sur l'objet
Set ExcelApp = CreateObject("Excel.application")

'-> Renvoyer une valeur de succ�s
IsExcel = True

GestError:
    Set ExcelApp = Nothing
    
End Function

Public Sub ApplicExcelFormat(aRange As Object, Ligne As Long)


'---> Cette proc�dure applique un format � une cellule Excel

On Error Resume Next

aRange.Borders(5).LineStyle = -4142
aRange.Borders(6).LineStyle = -4142
With aRange.Borders(7)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(8)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(9)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(10)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(11)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(12)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With


'-> Couleur de fond est gras si c'est le titre
If Ligne = 1 Then
    With aRange.Interior
        .ColorIndex = 15
        .Pattern = 1
    End With
    aRange.Font.Bold = True
    With aRange
        .HorizontalAlignment = -4108
        .VerticalAlignment = -4107
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .ShrinkToFit = False
        .MergeCells = False
    End With
End If

'-> Suppreimer les erreurs de formats
Err.Number = 0

End Sub

Private Function LoadExport(strName As String) As Boolean
'--> cette fonction permet de charger les colonnes du listview en lisant un fichier de
'    param�trage contenant les informations : DadsuExport.ini
Dim hdlFile As Integer
Dim Ligne As String

'On Error GoTo GestError

strName = Entry(1, strName, "|")

'-> V�rifier que l'on trouve le fichier de structure
If DadsuExportini = "" Then Exit Function

'-> Ouvrir le fichier ASCII de r�ponse et charger la matrice des lignes
hdlFile = FreeFile
Open DadsuExportini For Input As #hdlFile

'-> On parcours les lignes du fichier de param�trage
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> On passe les ligne � blanc ou les lignes de commentaire commencant par #
    If Trim(Ligne) <> "" And Mid(Ligne, 1, 1) <> "#" And Mid(Ligne, 1, 1) <> "[" Then
        '-> on verifie que l'on est sur le bon export
        If Entry(1, Ligne, ";") = strName Then
            '-> on charge l'entete du listview
            Me.ListView1.ColumnHeaders.Add , Entry(2, Ligne, ";"), Entry(3, Ligne, ";")
            '-> on affecte un alignement eventuellement � droite pour les chiffres
            If Structures(1).IsStructure(Entry(2, Ligne, ";")) Then
                If Structures(Entry(2, Ligne, ";")).TypeVal = pNumeric Then
                    If Me.ListView1.ColumnHeaders(Entry(2, Ligne, ";")).Index <> 1 Then Me.ListView1.ColumnHeaders(Entry(2, Ligne, ";")).Alignment = lvwColumnRight
                End If
            End If
        End If
    Else
        '-> on reccupere ici la valeur du noeud soit le type
        If Mid(Ligne, 1, 1) = "[" Then
            If Entry(1, Entry(1, Ligne, ";"), "|") = "[" & strName Then
                strRetour = Structures(Entry(3, Ligne, ";")).FieldType
            End If
        End If
    End If 'Ne pas traiter les lignes <> "" et de commentaire
Loop

Close #hdlFile

'-> valeur de succes
LoadExport = True

Exit Function
GestError:
DisplayMessage "Erreur lors de la lecture du fichier de param�trage des exports", dsmCritical, dsmOkOnly, ""

End Function

Private Function LoadTagTurbo(strName As String) As Boolean
'--> cette fonction permet de charger le tag pour l'edition turbo
'    param�trage contenant les informations : DadsuExport.ini
Dim hdlFile As Integer
Dim Ligne As String

'On Error GoTo GestError

strName = Entry(1, strName, "|")

'-> V�rifier que l'on trouve le fichier de structure
If DadsuExportini = "" Then Exit Function

'-> Ouvrir le fichier ASCII de r�ponse et charger la matrice des lignes
hdlFile = FreeFile
Open DadsuExportini For Input As #hdlFile

'-> On parcours les lignes du fichier de param�trage
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> on reccupere ici la valeur du noeud soit le type
    If Mid(Ligne, 1, 1) = "[" Then
        If Entry(1, Entry(1, Ligne, ";"), "|") = "[" & strName Then
            strRetour = Ligne
        End If
    End If
Loop

Close #hdlFile

'-> valeur de succes
LoadTagTurbo = True

Exit Function
GestError:
DisplayMessage "Erreur lors de la lecture du fichier de param�trage des exports turbo", dsmCritical, dsmOkOnly, ""

End Function

Public Function LoadComboExcel() As Boolean
'--> cette fonction permet de charger la combo pour les exports en lisant un fichier de
'    param�trage contenant les informations : DadsuExport.ini
Dim hdlFile As Integer
Dim Ligne As String

If Fields Is Nothing Then Exit Function

On Error GoTo GestError
If Fields.Count = 0 Then Exit Function

'-> V�rifier que l'on trouve le fichier de structure
If DadsuExportini = "" Then GoTo GestError

'-> Ouvrir le fichier ASCII de r�ponse et charger la matrice des lignes
hdlFile = FreeFile
Open DadsuExportini For Input As #hdlFile

Me.Combo1.ComboItems.Clear

'-> On parcours les lignes du fichier de param�trage
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> On passe les ligne � blanc ou les lignes de commentaire commencant par #
    If Mid(Ligne, 1, 1) = "[" Then
        Ligne = Mid(Ligne, 2)
        '-> on charge les combo
        Me.Combo1.ComboItems.Add , "Key|" & Me.Combo1.ComboItems.Count, Mid(Entry(1, Ligne, ";"), 1, 6) & " � " & Entry(2, Ligne, ";")
    End If 'Ne pas traiter les lignes <> "" et de commentaire
Loop

Close #hdlFile

'-> on charge les tags des combos
ComboChargeTag Me.Combo1, "�", True

'-> on charge la valeur par defaut
Me.Combo1.SelectedItem = Me.Combo1.ComboItems(1)
Me.Combo1.Text = Me.Combo1.SelectedItem.Text

'-> valeur de succes
LoadComboExcel = True

Me.Show

Exit Function
GestError:
DisplayMessage "Erreur lors de la lecture du fichier de param�trage", dsmCritical, dsmOkOnly, ""

End Function

Public Sub FormatListView(List As Object)

'---> Cette proc�dure formatte les entetes d'un listView

Dim i As Long
Dim x As Object

'-> Ne rien faire si pas de colonnes
If List.ColumnHeaders.Count = 0 Then Exit Sub

'-> De base toujours cr�er un enregistrement avec les entetes de colonnes
Set x = List.ListItems.Add(, "DEALENREGENTETE")

For i = 0 To List.ColumnHeaders.Count - 1
    '-> Ajouter le libelle de l'entete de la colonne
    If i = 0 Then
        x.Text = List.ColumnHeaders(1).Text
    Else
        x.SubItems(i) = List.ColumnHeaders(i + 1).Text
    End If
    SendMessage List.hwnd, LVM_SETCOLUMNWIDTH, i, 0
Next

'-> Supprimer le premier enregistrement
List.ListItems.Remove ("DEALENREGENTETE")

End Sub
