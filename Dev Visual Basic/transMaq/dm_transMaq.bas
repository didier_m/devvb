Attribute VB_Name = "dm_transMaq"
Option Explicit


Public FileName As String
Public StrError As String
Public Lignes As Collection
Public FileTransMaq As String
Public StrCopyKey As String

Private hdlFile As Integer
Private TempFileName As String
Private Ligne As String

Sub Main()

'-> on initialise le message d'erreur
StrError = "Une erreur est survenue lors du chargement"
'-> on ouvre la feuille de chargement du dadsu
init

End Sub

Public Sub init()
'-> on ouvre la fen�tre de selection de fichier le fichier en retour est stock� dans strretour
Dim LigneCommande As String

'-> R�cup�ration de la ligne de commande
LigneCommande = Command()
LigneCommande = LTrim(LigneCommande)

'-> Supprimer les Quotters s'il y en a
If InStr(1, LigneCommande, """") = 1 Then _
    LigneCommande = Mid$(LigneCommande, 2, Len(LigneCommande) - 1)
If InStr(1, LigneCommande, """") <> 0 Then _
LigneCommande = Mid$(LigneCommande, 1, Len(LigneCommande) - 1)

If Trim(LigneCommande) = "" Then
    df_Drloading.Show
    df_Drloading.Label1.Caption = "Chargement..."
    df_OpenFile.init 0, App.Path, "Choix du fichier spool � transformer", "*.*"
    Unload df_Drloading
    df_OpenFile.Show vbModal
    '-> on v�rifie l'existance d'un fichier � charger
    If Trim(strRetour) = "" Then Exit Sub
End If

'-> on lance la proc�dure de chargement du treeview
LoadtransMaq strRetour

End Sub

Public Function LoadtransMaq(aFilePath As String) As Boolean
Dim hdlFic As Integer

'On Error GoTo GestError:

'-> Ouvrir le fichier spool
'hdlFic = FreeFile
'Open aFilePath For Input As #hdlFic
'Line Input #hdlFic, Ligne

'-> on teste la premiere chaine pour v�rifier que l'on est bien sur un fichier dadsu
'If Mid(Ligne, 1, 1) <> "S" Then GoTo GestError
        
'-> on affiche le timer
df_Drloading.Show

'-> on charge maintenant l'ensemb le des lignes en memoire
If Not MaqLoad(aFilePath) Then GoTo GestError

'-> on lance maintenant le replace des valeurs en fonction des regles donnees
If Not MaqTransformation Then GoTo GestError

'-> on enregistre maintenant le fichier en creant une sauvegarde de l'ancien
If Not MaqSave(aFilePath) Then GoTo GestError

'-> on masque le timer
Unload df_Drloading

Exit Function
GestError:

DisplayMessage StrError, dsmCritical, dsmOkOnly, "Erreur sur l'application"
'-> on ferme le fichier
Close #hdlFic
End
End Function

Private Function MaqTransformation() As Boolean
'--> Cette fonction va faire le replace en fonction des regles donnees dans le spool
'--> les lignes de typeB correspondent � la zone ou mettre la valeur
'--> les lignes de typeA correspondent � la zone ou se trouve la valeur

Dim aLigne As dc_ligne
Dim aLigneN As dc_ligne
Dim b1, b2, b3, b4, b5, b9 As Single
Dim strValeur As String
Dim strLigne As String
Dim Ligne As String
Dim j As Integer

'On Error GoTo GestError

'-> on parcourt les differentes lignes
For Each aLigne In Lignes
    '-> on regarde si on est sur une ligne de typeB
    If InStr(1, aLigne.Value, "bb1", vbTextCompare) Or InStr(1, aLigne.Value, "bb2", vbTextCompare) Or InStr(1, aLigne.Value, "bb3", vbTextCompare) Or InStr(1, aLigne.Value, "bb4", vbTextCompare) Or InStr(1, aLigne.Value, "bb5", vbTextCompare) Then
        '-> on memorise la ligne
        If InStr(1, aLigne.Value, "bb1", vbTextCompare) Then
            b1 = aLigne.numLigne
        End If
        If InStr(1, aLigne.Value, "bb2", vbTextCompare) Then
            b2 = aLigne.numLigne
        End If
        If InStr(1, aLigne.Value, "bb3", vbTextCompare) Then
            b3 = aLigne.numLigne
        End If
        If InStr(1, aLigne.Value, "bb4", vbTextCompare) Then
            b4 = aLigne.numLigne
        End If
        If InStr(1, aLigne.Value, "bb5", vbTextCompare) Then
            b5 = aLigne.numLigne
        End If
    End If
    '-> on regarde si on est sur une ligne de typeA
    If InStr(1, aLigne.Value, "aa1", vbTextCompare) Or InStr(1, aLigne.Value, "aa2", vbTextCompare) Or InStr(1, aLigne.Value, "aa3", vbTextCompare) Or InStr(1, aLigne.Value, "aa4", vbTextCompare) Or InStr(1, aLigne.Value, "aa5", vbTextCompare) Then
        '-> on prend la valeur pour la mettre dans la ligne de typeB correspondante
        If InStr(1, aLigne.Value, "aa1", vbTextCompare) Then
            '-> on reccupere la valeur
            strValeur = Mid(aLigne.Value, InStr(1, aLigne.Value, "aa1", vbTextCompare) + 3, 30)
            '-> on met cette valeur dans la ligne qui avait bb1
            If b1 <> 0 Then
                strLigne = Mid(Lignes("k" & b1).Value, 1, InStr(1, Lignes("k" & b1).Value, "bb1", vbTextCompare) - 1) & strValeur & Mid(Lignes("k" & b1).Value, InStr(1, Lignes("k" & b1).Value, "bb1", vbTextCompare) + 30, 1000)
                Lignes("k" & b1).Value = strLigne
                b1 = 0
            End If
            aLigne.Value = Replace(aLigne.Value, "aa1", "   ", , , vbTextCompare)
        End If
        If InStr(1, aLigne.Value, "aa2", vbTextCompare) Then
            '-> on reccupere la valeur
            strValeur = Mid(aLigne.Value, InStr(1, aLigne.Value, "aa2", vbTextCompare) + 3, 30)
            '-> on met cette valeur dans la ligne qui avait bb2
            If b2 <> 0 Then
                strLigne = Mid(Lignes("k" & b2).Value, 1, InStr(1, Lignes("k" & b2).Value, "bb2", vbTextCompare) - 1) & strValeur & Mid(Lignes("k" & b2).Value, InStr(1, Lignes("k" & b2).Value, "bb2", vbTextCompare) + 30, 1000)
                Lignes("k" & b2).Value = strLigne
                b2 = 0
            End If
            aLigne.Value = Replace(aLigne.Value, "aa2", "   ", , , vbTextCompare)
        End If
        If InStr(1, aLigne.Value, "aa3", vbTextCompare) Then
            '-> on reccupere la valeur
            strValeur = Mid(aLigne.Value, InStr(1, aLigne.Value, "aa3", vbTextCompare) + 3, 30)
            '-> on met cette valeur dans la ligne qui avait bb3
            If b3 <> 0 Then
                strLigne = Mid(Lignes("k" & b3).Value, 1, InStr(1, Lignes("k" & b3).Value, "bb3", vbTextCompare) - 1) & strValeur & Mid(Lignes("k" & b3).Value, InStr(1, Lignes("k" & b3).Value, "bb3", vbTextCompare) + 30, 1000)
                Lignes("k" & b3).Value = strLigne
                b3 = 0
            End If
            aLigne.Value = Replace(aLigne.Value, "aa3", "   ", , , vbTextCompare)
        End If
        If InStr(1, aLigne.Value, "aa4", vbTextCompare) Then
            '-> on reccupere la valeur
            strValeur = Mid(aLigne.Value, InStr(1, aLigne.Value, "aa4", vbTextCompare) + 3, 30)
            '-> on met cette valeur dans la ligne qui avait bb3
            If b4 <> 0 Then
                strLigne = Mid(Lignes("k" & b4).Value, 1, InStr(1, Lignes("k" & b4).Value, "bb4", vbTextCompare) - 1) & strValeur & Mid(Lignes("k" & b4).Value, InStr(1, Lignes("k" & b4).Value, "bb4", vbTextCompare) + 30, 1000)
                Lignes("k" & b4).Value = strLigne
                b4 = 0
            End If
            aLigne.Value = Replace(aLigne.Value, "aa4", "   ", , , vbTextCompare)
        End If
        If InStr(1, aLigne.Value, "aa5", vbTextCompare) Then
            '-> on reccupere la valeur
            strValeur = Mid(aLigne.Value, InStr(1, aLigne.Value, "aa5", vbTextCompare) + 3, 30)
            '-> on met cette valeur dans la ligne qui avait bb5
            If b5 <> 0 Then
                strLigne = Mid(Lignes("k" & b5).Value, 1, InStr(1, Lignes("k" & b5).Value, "bb5", vbTextCompare) - 1) & strValeur & Mid(Lignes("k" & b5).Value, InStr(1, Lignes("k" & b5).Value, "bb4", vbTextCompare) + 30, 1000)
                Lignes("k" & b5).Value = strLigne
                b5 = 0
            End If
            aLigne.Value = Replace(aLigne.Value, "aa5", "   ", , , vbTextCompare)
        End If
    End If
    '-> on regarde si on est sur une ligne de type bb9 si oui on le remplace par six lignes blanches
    If InStr(1, aLigne.Value, "bb9", vbTextCompare) Then
        aLigne.Value = ""
        '-> on ajoute maintenant 5 lignes blanches
        For j = 1 To 5
            Set aLigneN = New dc_ligne
            aLigneN.Value = ""
            aLigneN.numLigne = 9999
            Lignes.Add aLigneN, , , "k" & aLigne.numLigne
        Next
    End If
    '-> on remplace du texte
    If InStr(1, aLigne.Value, "�    No pi�ce   �Date pi�ce�  Montant pay� �   Honoraires   �  Commissions   �   Courtage    �Frais accessoires �Cod Let�", vbTextCompare) Then
        '-> Ouvrir le fichier ASCII de r�ponse et charger la matrice des lignes
        hdlFile = FreeFile
        Open App.Path & "/" & "maq.txt" For Input As #hdlFile
        
        Line Input #hdlFile, Ligne
        aLigne.Value = Ligne
        '-> On parcours les lignes du fichier de param�trage
        Do While Not EOF(hdlFile)
            '-> Lecture de la ligne
            Line Input #hdlFile, Ligne
            Set aLigneN = New dc_ligne
            aLigneN.Value = Ligne
            aLigneN.numLigne = 9999
            Lignes.Add aLigneN, , , "k" & aLigne.numLigne
        Loop
        
        Close #hdlFile
    End If
Next
MaqTransformation = True
Exit Function
'--> gestion des erreurs
GestError:
MaqTransformation = False
End Function

Private Function MaqSave(aFilePath As String) As Boolean
'--> Cette fonction va sauvegarder le spool qui a ete transforme et fait une sauvegarde de l ancien
Dim aLigne As dc_ligne

On Error GoTo GestError

TempFileName = GetTempFileNameVB("dadsu")
hdlFile = FreeFile
Open TempFileName For Output As #hdlFile

'-> on affiche le timer
df_Drloading.Show
df_Drloading.Label1.Caption = "Enregistrement en cours..."

'-> on parcours les lignes pour les ecrire dans le fichier
For Each aLigne In Lignes
    Print #hdlFile, aLigne.Value
Next

'-> fermer le fichier
Close #hdlFile

'-> on masque le timer
Unload df_Drloading

'-> on renomme celui d'origine
FileCopy aFilePath, aFilePath & ".old"

'-> on supprime celui d'origine
Kill aFilePath

'-> copier le fichier
FileCopy TempFileName, aFilePath

'-> Supprimer le fichier temporaire
If Dir$(TempFileName) <> "" Then Kill TempFileName

MaqSave = True
Exit Function

GestError:
MaqSave = False
End Function

Private Function MaqLoad(aFilePath As String) As Boolean
'--> cette fonction permet de charger le spool dans la collection des lignes

Dim curLigne As Single
Dim aLigne As dc_ligne
Dim Ligne As String

On Error GoTo GestError

'-> on initialise les collections
Set Lignes = New Collection
curLigne = 1

'-> Ouvrir le fichier ASCII de r�ponse et charger la matrice des lignes
hdlFile = FreeFile
Open aFilePath For Input As #hdlFile

'-> On parcours les lignes du fichier de param�trage
Do While Not EOF(hdlFile)
    '-> on initialise les objet
    Set aLigne = New dc_ligne
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    aLigne.Value = Ligne
    aLigne.numLigne = curLigne
    Lignes.Add aLigne, "K" & curLigne
    curLigne = curLigne + 1
Loop

Close #hdlFile
'-> valeur de succes
MaqLoad = True
Exit Function
GestError:
StrError = "Erreur lors de la lecture du fichier spool : " & aFilePath
MaqLoad = False

End Function

