VERSION 5.00
Object = "{DAFD85F1-343B-47EE-8170-1C26723F7A5D}#2.0#0"; "dogskin.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form df_Ajout 
   BackColor       =   &H00CBB49A&
   BorderStyle     =   0  'None
   Caption         =   "Saisie des selections"
   ClientHeight    =   5970
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7725
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   5970
   ScaleWidth      =   7725
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin DogSkin.DealCmdButton Picture1 
      Height          =   615
      Left            =   6360
      TabIndex        =   0
      Tag             =   "DICTAB�BUTTON-OK"
      Top             =   5220
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   1
      ButtonForm      =   1
      BackColor       =   16579059
   End
   Begin DogSkin.DogSkinObject DogSkinObject1 
      Left            =   0
      Top             =   0
      _ExtentX        =   979
      _ExtentY        =   953
      BorderColor     =   13077835
      BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderForeColor =   10053171
      HeaderBackColor =   16637889
      HeaderLineColor =   15847345
      HeaderUseLine   =   -1  'True
      BeginProperty BodyFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BodyForeColor   =   10053171
      BodyBackColor   =   16579059
      HeaderIco       =   "df_Ajout.frx":0000
      HeaderIcoNa     =   "df_Ajout.frx":0682
      ShadowColor     =   15847345
   End
   Begin DogSkin.DealCmdButton Picture3 
      Height          =   615
      Left            =   6960
      TabIndex        =   1
      Tag             =   "DICTAB�BUTTON-CANCEL"
      Top             =   5220
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   2
      ButtonForm      =   1
      BackColor       =   16579059
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   4155
      Left            =   360
      TabIndex        =   2
      Top             =   840
      Width           =   6975
      _ExtentX        =   12303
      _ExtentY        =   7329
      _Version        =   393217
      Indentation     =   26
      LabelEdit       =   1
      Style           =   7
      Checkboxes      =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   0
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4080
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   21
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":0D04
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":19DE
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":36B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":4392
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":506C
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":5D46
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":6A20
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":76FA
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":7FD4
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":9CAE
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":A588
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":B262
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":BB3C
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":C416
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":D0F0
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":DDCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":E6A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":1037E
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":12058
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":1784A
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Ajout.frx":19524
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00C78D4B&
      Height          =   4455
      Left            =   120
      Top             =   600
      Width           =   7455
   End
End
Attribute VB_Name = "df_Ajout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
    Case 113 'F2 validation
    
    Case 115, 27 'F4 ou esc annulation bon
        Unload Me
End Select

End Sub

Private Sub Form_Load()

'---> Init du skin
Me.DogSkinObject1.Initialisation False, "Ajouter des enregistrements", pCloseOnly, False
Me.DogSkinObject1.SetMatriceEffect "SHAPE1"
'-> on charge la couleur de fond desbouttons
Me.Picture1.BackColor = Me.DogSkinObject1.BodyBackColor
Me.Picture3.BackColor = Me.DogSkinObject1.BodyBackColor

End Sub

Private Sub Picture1_Click()
'--> sur cette procedure on creer les differents enregistrements � blanc
'    on charge le treeview
'    on affiche le dealgrid pour la saisie
Dim Niveau As Variant
Dim strTemp As Variant
Dim strKeyWord As String
Dim curKeyWord As String
Dim firstKeyWord As String
Dim curNiveau As Integer
Dim curType As String
Dim aNode As Node
Dim strType As String
Dim i As Integer

'On Error GoTo GestError

'-> on pointe sur la cl�
strKeyWord = df_Dadsu.TreeView1.SelectedItem.Key

'-> on se parcours le treeview pour inserer toutes les rubriques selectionn�es
i = 0
For Each aNode In Me.TreeView1.Nodes
    '-> on regarde si on doit traiter le noeud
    If aNode.Checked Then i = i + 1
Next

'-> on vide
If i > 0 Then
    df_Dadsu.DealGrid1.ClearRow (False)
Else
    Exit Sub
End If

'-> on se parcours le treeview pour inserer toutes les rubriques selectionn�es
For Each aNode In Me.TreeView1.Nodes
    '-> on regarde si on doit traiter le noeud
    If aNode.Checked Then
        '-> on pointe sur le niveau du noeud selectionn�
        Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
        curNiveau = Structures(aNode.Key).FieldNiveau
        curType = Structures(aNode.Key).FieldType
        strTemp = Split(strKeyWord, "|")
        For i = 1 To curNiveau - 1
            Niveau(i) = CSng(strTemp(i))
        Next
        Niveau(5) = CSng(strTemp(5))
        '-> on cree le niveau ou inserer le noeud
        curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
        strKeyWord = EnregsCreate(curKeyWord, curType)
        If firstKeyWord = "" Then firstKeyWord = strKeyWord
    End If
Next

'-> on rend visible
df_Dadsu.TreeView1.Nodes(firstKeyWord).EnsureVisible

'-> maintenant que l'on a creer les enregs a blanc on charge le dealgrid
LoadDealGrid firstKeyWord

'-> fermer cette fenetre
Unload Me

Exit Sub
GestError:

End Sub

Private Sub Picture3_Click()

'-> on quitte sans rien faire
Unload Me

End Sub

Public Function LoadTreeview(strKeyWord As String) As Boolean
'--> cette fonction va permettre de charger le treeview en fonction des elements � ajouter
Dim curNiveau As Integer
Dim lastNiveau As Integer
Dim lastKeyWord As String
Dim aType As dc_type
Dim aNode As Node
Dim intImage As Integer

'On Error GoTo GestError

'-> on pointe sur le niveau
curNiveau = Fields(strKeyWord).Niveau
lastNiveau = curNiveau
lastKeyWord = Fields(strKeyWord).Struct

'-> on vide le treeview
Me.TreeView1.Nodes.Clear

'-> on pointe sur la bonne image
intImage = GetImage(lastKeyWord, curNiveau - 1)
'-> on insere le niveau en cours
Me.TreeView1.Nodes.Add , , lastKeyWord, Mid(lastKeyWord, 1, 3) & " - " & Structures(lastKeyWord).FieldDescription, intImage

'-> on parcour les types pour charger le treeview avec les niveau inferieur
For Each aType In Types
    '-> on regarde si ce type doit etre ajout�
    If aType.Niveau > curNiveau Then
        '-> on pointe sur la bonne image
        intImage = GetImage(aType.KeyWord, aType.Niveau - 1)
        '-> on pointe sur le noeud parent de ce noeud
        For Each aNode In Me.TreeView1.Nodes
            If Structures(aNode.Key).FieldNiveau + 1 = aType.Niveau Then
                Exit For
            End If
        Next
        '-> on ajoute le sous niveau au bon endroit
        Me.TreeView1.Nodes.Add aNode.Key, 4, aType.KeyWord, Mid(aType.KeyWord, 1, 3) & " - " & Structures(aType.KeyWord).FieldDescription, intImage
        '-> on rend visible le noeud
        Me.TreeView1.Nodes(aType.KeyWord).EnsureVisible
        '-> on se met a gauche le keyword et le niveau
        lastKeyWord = aType.KeyWord
    End If
Next

Me.Show

LoadTreeview = True

Exit Function
GestError:
DisplayMessage "Erreur lors de l'analyse de l'ajout", dsmCritical, dsmOkOnly, "Ajout"

End Function

Private Sub TreeView1_NodeCheck(ByVal Node As MSComctlLib.Node)
'--> sur un check on check tous les sous niveau et les surniveau de la meme branche
'--> sur un decheck on decheck tous les sous niveaux
Dim aNode As Node
Dim firstNode As Integer
Dim lastNode As Integer

If Node.Children > 0 Then
    Set aNode = Node.Child
    firstNode = aNode.FirstSibling.Index
    lastNode = aNode.LastSibling.Index
    '-> on check les sousniveau
    For Each aNode In Me.TreeView1.Nodes
        If aNode.Index >= firstNode And aNode.Index <= lastNode Then aNode.Checked = Node.Checked
    Next
End If
    
'dans le cas d'un check a true on check les parents
'If Node.Checked Then
'    Set aNode = Node.Parent
'    Do While Not aNode Is Nothing
'        aNode.Checked = True
'        Set aNode = aNode.Parent
'    Loop
'End If

End Sub


