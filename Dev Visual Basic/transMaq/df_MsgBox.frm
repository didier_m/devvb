VERSION 5.00
Object = "{DAFD85F1-343B-47EE-8170-1C26723F7A5D}#2.0#0"; "DogSkin.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form df_MsgBox 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   3975
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   9270
   LinkTopic       =   "Form1"
   ScaleHeight     =   3975
   ScaleWidth      =   9270
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox NewButton1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00FCF9F3&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   7080
      Picture         =   "df_MsgBox.frx":0000
      ScaleHeight     =   480
      ScaleWidth      =   2025
      TabIndex        =   6
      Top             =   3360
      Width           =   2025
      Begin VB.Shape Shape4 
         BorderStyle     =   3  'Dot
         Height          =   350
         Left            =   140
         Top             =   50
         Visible         =   0   'False
         Width           =   1700
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   0
         TabIndex        =   7
         Top             =   100
         Width           =   2055
      End
   End
   Begin VB.PictureBox NewButton2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00FCF9F3&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   5040
      Picture         =   "df_MsgBox.frx":0925
      ScaleHeight     =   480
      ScaleWidth      =   2025
      TabIndex        =   4
      Top             =   3360
      Width           =   2025
      Begin VB.Shape Shape3 
         BorderStyle     =   3  'Dot
         Height          =   350
         Left            =   140
         Top             =   50
         Visible         =   0   'False
         Width           =   1700
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   0
         TabIndex        =   5
         Top             =   100
         Width           =   2055
      End
   End
   Begin VB.PictureBox NewButton3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00FCF9F3&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   3000
      Picture         =   "df_MsgBox.frx":124A
      ScaleHeight     =   480
      ScaleWidth      =   2025
      TabIndex        =   2
      Top             =   3360
      Width           =   2025
      Begin VB.Shape Shape2 
         BorderStyle     =   3  'Dot
         Height          =   350
         Left            =   140
         Top             =   50
         Visible         =   0   'False
         Width           =   1700
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   0
         TabIndex        =   3
         Top             =   100
         Width           =   2055
      End
   End
   Begin VB.TextBox Text1 
      BorderStyle     =   0  'None
      Height          =   1095
      Left            =   1440
      TabIndex        =   0
      Top             =   1680
      Width           =   7455
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   3360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   48
      ImageHeight     =   48
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_MsgBox.frx":1B6F
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_MsgBox.frx":3849
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_MsgBox.frx":5523
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_MsgBox.frx":71FD
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_MsgBox.frx":80D7
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin DogSkin.DogSkinObject DogSkinObject1 
      Left            =   0
      Top             =   0
      _ExtentX        =   979
      _ExtentY        =   953
      BorderColor     =   13077835
      BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderForeColor =   10053171
      HeaderBackColor =   16637889
      HeaderLineColor =   15847345
      HeaderUseLine   =   -1  'True
      BeginProperty BodyFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BodyForeColor   =   10053171
      BodyBackColor   =   16579059
      HeaderIco       =   "df_MsgBox.frx":9DB1
      HeaderIcoNa     =   "df_MsgBox.frx":A433
      ShadowColor     =   16637889
   End
   Begin VB.Image imgIco 
      Height          =   720
      Left            =   240
      Top             =   720
      Width           =   720
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00996633&
      Height          =   2025
      Left            =   1320
      TabIndex        =   1
      Top             =   840
      Width           =   7725
   End
   Begin VB.Shape shapeInput 
      BorderColor     =   &H00996633&
      Height          =   1335
      Left            =   1320
      Top             =   1560
      Width           =   7695
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00996633&
      Height          =   2535
      Left            =   120
      Top             =   600
      Width           =   9015
   End
End
Attribute VB_Name = "df_MsgBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Public DefaultReturnValue As String
Public DefaultCancelValue As String



Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

'-> Afficher un fichier d'aide
If KeyCode = vbKeyF1 Then Call DisplayHelpErrorFile

End Sub
Private Sub DisplayHelpErrorFile()

'---> Proc�dure qui affiche le fichier d'erreur int�gr�

On Error Resume Next

'-> V�rifier si le fichier est rens�ign�
If DogFatalErrorFileHelp = "" Then Exit Sub

'-> V�rifier si on trouve le fichier
If Dir$(DogFatalErrorFileHelp) = "" Then Exit Sub

'-> Afficher dans NotePad
ShellExecute Me.hwnd, "OPEN", "notepad.exe", DogFatalErrorFileHelp, vbNullString, 1


End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    '-> Si on est sur un bouton, donner la valeur de ce bouton
    If TypeOf Me.ActiveControl Is PictureBox Then
        '-> Retourner la valeur du bouton s�lectionn�
        strRetour = Me.ActiveControl.Tag
        Unload Me
    Else
        '-> Si on est en zone de saisie, retourner la valeur saisie car le bouton defaut est OK
        If Me.Text1.Visible Then InputValue = Me.Text1.Text
        '-> Retourner la valeur par d�faut
        strRetour = Me.DefaultReturnValue
        Unload Me
    End If ' Si on est sur un bouton
ElseIf KeyAscii = 27 Then 'ECHAP
    '-> Retourner la valeur par d�faut
    strRetour = Me.DefaultCancelValue
    Unload Me
End If

End Sub

Private Sub Form_Load()

'-> Initialiser le dessin de l'interface
Me.DogSkinObject1.Initialisation False, "", pNoButton, True

'-> Dessin des effets
Me.DogSkinObject1.SetMatriceEffect "Shape1"

End Sub


Private Sub Label2_Click()
Call NewButton3_Click
End Sub

Private Sub Label3_Click()
Call NewButton2_Click
End Sub

Private Sub Label4_Click()
Call NewButton1_Click
End Sub

Private Sub NewButton1_Click()

'-> Setting de la valeursaisie
If Me.Text1.Visible Then InputValue = Me.Text1.Text
strRetour = Me.NewButton1.Tag
Unload Me

End Sub

Private Sub NewButton1_KeyDown(KeyCode As Integer, Shift As Integer)

Select Case KeyCode
    Case 37 'gauche
        If NewButton2.Visible Then NewButton2.SetFocus
    Case 39 'Droite
        If NewButton3.Visible Then
            NewButton3.SetFocus
        ElseIf NewButton2.Visible Then
            NewButton2.SetFocus
        End If
End Select

End Sub

Private Sub NewButton2_KeyDown(KeyCode As Integer, Shift As Integer)

Select Case KeyCode
    Case 37 'gauche
        If NewButton3.Visible Then
            NewButton3.SetFocus
        ElseIf NewButton1.Visible Then
            NewButton1.SetFocus
        End If
    Case 39 'Droite
        If NewButton1.Visible Then NewButton1.SetFocus
End Select

End Sub

Private Sub NewButton3_KeyDown(KeyCode As Integer, Shift As Integer)

Select Case KeyCode
    Case 37 'gauche
        If NewButton1.Visible Then NewButton1.SetFocus
    Case 39 'Droite
        If NewButton2.Visible Then NewButton2.SetFocus
End Select

End Sub


Private Sub NewButton2_Click()

If Me.Text1.Visible Then InputValue = Me.Text1.Text
strRetour = Me.NewButton2.Tag
Unload Me

End Sub

Private Sub NewButton2_GotFocus()

Me.Shape3.Visible = True

End Sub

Private Sub NewButton2_LostFocus()

Me.Shape3.Visible = False

End Sub

Private Sub NewButton1_GotFocus()

Me.Shape4.Visible = True

End Sub

Private Sub NewButton1_LostFocus()

Me.Shape4.Visible = False

End Sub

Private Sub NewButton3_Click()

If Me.Text1.Visible Then InputValue = Me.Text1.Text
strRetour = Me.NewButton3.Tag
Unload Me

End Sub



Private Sub NewButton3_GotFocus()

Me.Shape2.Visible = True

End Sub

Private Sub NewButton3_LostFocus()

Me.Shape2.Visible = False

End Sub

Private Sub Text1_GotFocus()
On Error Resume Next
Me.Text1.SelStart = 0
Me.Text1.SelLength = Len(Me.Text1.Text)
End Sub



