VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form Form1 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFFFFF&
   Caption         =   "DealUpdater DMZ 2016"
   ClientHeight    =   12255
   ClientLeft      =   225
   ClientTop       =   570
   ClientWidth     =   21810
   FillColor       =   &H00FFC0FF&
   FillStyle       =   0  'Solid
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   NegotiateMenus  =   0   'False
   ScaleHeight     =   12255
   ScaleWidth      =   21810
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox List 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Lister les fichiers"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   15480
      TabIndex        =   62
      Top             =   5880
      Value           =   1  'Checked
      Width           =   3975
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Filtrer"
      ForeColor       =   &H00000000&
      Height          =   3375
      Left            =   120
      TabIndex        =   43
      Top             =   3120
      Width           =   10095
      Begin VB.CheckBox Check11 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2280
         TabIndex        =   59
         Top             =   2880
         Width           =   255
      End
      Begin VB.TextBox Text10 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2640
         TabIndex        =   58
         ToolTipText     =   "s�parateur ','"
         Top             =   2880
         Width           =   7335
      End
      Begin VB.CheckBox Check6 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2280
         TabIndex        =   56
         Top             =   2400
         Width           =   255
      End
      Begin VB.TextBox Text9 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2640
         TabIndex        =   55
         ToolTipText     =   "s�parateur ','"
         Top             =   2400
         Width           =   7335
      End
      Begin VB.TextBox Text8 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   6480
         TabIndex        =   53
         Text            =   "dd/mm/yyyy"
         ToolTipText     =   "s�parateur ','"
         Top             =   1920
         Width           =   1455
      End
      Begin VB.CheckBox Check3 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "R�pertoires existants sur 'A' uniquement"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   52
         Top             =   1440
         Value           =   1  'Checked
         Width           =   3975
      End
      Begin VB.TextBox Text6 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2640
         TabIndex        =   48
         ToolTipText     =   "s�parateur ','"
         Top             =   480
         Width           =   7335
      End
      Begin VB.TextBox Text7 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2640
         TabIndex        =   47
         ToolTipText     =   "s�parateur ','"
         Top             =   960
         Width           =   7335
      End
      Begin VB.CheckBox Check9 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2280
         TabIndex        =   46
         Top             =   480
         Width           =   255
      End
      Begin VB.CheckBox Check10 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2280
         TabIndex        =   45
         Top             =   960
         Width           =   255
      End
      Begin VB.TextBox Text5 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2640
         TabIndex        =   44
         Text            =   "dd/mm/yyyy"
         ToolTipText     =   "s�parateur ','"
         Top             =   1920
         Width           =   1455
      End
      Begin VB.Label Label33 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Extensions autoris�es"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   60
         Top             =   2880
         Width           =   2415
      End
      Begin VB.Label Label32 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Ignorer les fichiers"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   57
         Top             =   2400
         Width           =   2415
      End
      Begin VB.Label Label10 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Date maxi"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4440
         TabIndex        =   54
         Top             =   1920
         Width           =   1695
      End
      Begin VB.Label Label22 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Ignorer les r�pertoires"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   51
         Top             =   480
         Width           =   2415
      End
      Begin VB.Label Label31 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "R�pertoires autoris�s"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   50
         Top             =   960
         Width           =   2415
      End
      Begin VB.Label Label9 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Date mini"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   49
         Top             =   1920
         Width           =   1695
      End
   End
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Exporter vers"
      ForeColor       =   &H00000000&
      Height          =   975
      Left            =   120
      TabIndex        =   39
      Top             =   8760
      Width           =   10095
      Begin VB.TextBox Text4 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2640
         TabIndex        =   41
         ToolTipText     =   "R�pertoire d'export qui contiendra les fichiers diff�rents par rapport � la r�f�rence"
         Top             =   480
         Width           =   4095
      End
      Begin VB.CheckBox Check8 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   2280
         TabIndex        =   40
         Top             =   480
         Width           =   255
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "R�pertoire export"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   42
         Top             =   480
         Width           =   3855
      End
   End
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "R�pertoires"
      ForeColor       =   &H00000000&
      Height          =   2895
      Left            =   120
      TabIndex        =   32
      Top             =   120
      Width           =   10095
      Begin VB.CheckBox Check12 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Horodatage des fichiers identiques de 'A' par rapport � 'B'"
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   5400
         TabIndex        =   61
         Top             =   2280
         Value           =   1  'Checked
         Width           =   4455
      End
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   480
         TabIndex        =   35
         ToolTipText     =   "R�pertoire dont les dates de fichier seront �ventuellement modifi�es"
         Top             =   840
         Width           =   4335
      End
      Begin VB.TextBox Text2 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   480
         TabIndex        =   34
         Text            =   "R:\v6\"
         ToolTipText     =   "R�pertoire servant de base de l'analyse"
         Top             =   1680
         Width           =   4335
      End
      Begin VB.TextBox Text3 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2280
         TabIndex        =   33
         Text            =   "d:\log.txt"
         Top             =   2280
         Width           =   2535
      End
      Begin VB.Shape Shape8 
         BorderStyle     =   0  'Transparent
         DrawMode        =   9  'Not Mask Pen
         FillColor       =   &H00FFFFC0&
         FillStyle       =   0  'Solid
         Height          =   735
         Left            =   7440
         Shape           =   2  'Oval
         Top             =   1080
         Width           =   1455
      End
      Begin VB.Shape Shape5 
         BorderStyle     =   0  'Transparent
         FillColor       =   &H00C0FFFF&
         FillStyle       =   0  'Solid
         Height          =   735
         Left            =   6720
         Shape           =   2  'Oval
         Top             =   1080
         Width           =   1455
      End
      Begin VB.Shape Shape2 
         BorderStyle     =   0  'Transparent
         FillColor       =   &H00FFFFC0&
         FillStyle       =   0  'Solid
         Height          =   255
         Left            =   5040
         Top             =   1680
         Width           =   255
      End
      Begin VB.Shape Shape1 
         BorderStyle     =   0  'Transparent
         FillColor       =   &H00C0FFFF&
         FillStyle       =   0  'Solid
         Height          =   255
         Left            =   5040
         Top             =   840
         Width           =   255
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Dossier A"
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   480
         TabIndex        =   38
         Top             =   480
         Width           =   3975
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Dossier B"
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   480
         TabIndex        =   37
         Top             =   1320
         Width           =   4575
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Log du traitement"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   36
         Top             =   2400
         Width           =   1695
      End
      Begin VB.Shape Shape10 
         BorderStyle     =   0  'Transparent
         FillColor       =   &H00C0E0FF&
         FillStyle       =   0  'Solid
         Height          =   495
         Left            =   6240
         Shape           =   3  'Circle
         Top             =   1200
         Width           =   495
      End
      Begin VB.Shape Shape11 
         BorderStyle     =   0  'Transparent
         DrawMode        =   9  'Not Mask Pen
         FillColor       =   &H00FFC0C0&
         FillStyle       =   0  'Solid
         Height          =   495
         Left            =   8880
         Shape           =   3  'Circle
         Top             =   1200
         Width           =   495
      End
      Begin VB.Shape Shape12 
         BorderStyle     =   0  'Transparent
         FillColor       =   &H00FFFFFF&
         FillStyle       =   0  'Solid
         Height          =   1335
         Left            =   6000
         Top             =   720
         Width           =   3735
      End
   End
   Begin VB.CheckBox Check4 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Afficher les num�ros de fiches"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   10680
      TabIndex        =   31
      Top             =   5880
      Value           =   1  'Checked
      Width           =   3975
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Statistiques"
      ForeColor       =   &H00000000&
      Height          =   1455
      Left            =   120
      TabIndex        =   13
      Top             =   9840
      Width           =   10095
      Begin VB.Label Label29 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   6240
         TabIndex        =   30
         Top             =   1080
         Visible         =   0   'False
         Width           =   1200
      End
      Begin VB.Label Label28 
         BackStyle       =   0  'Transparent
         Caption         =   "Modifi�s"
         Height          =   255
         Left            =   4560
         TabIndex        =   29
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label27 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFC0&
         Height          =   195
         Left            =   4560
         TabIndex        =   28
         Top             =   1080
         Width           =   1200
      End
      Begin VB.Label Label26 
         BackStyle       =   0  'Transparent
         Caption         =   "Supprim�s"
         Height          =   255
         Left            =   2880
         TabIndex        =   27
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label25 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0E0FF&
         Height          =   195
         Left            =   2880
         TabIndex        =   26
         Top             =   1080
         Width           =   1200
      End
      Begin VB.Label Label24 
         BackStyle       =   0  'Transparent
         Caption         =   "Nouveaux"
         Height          =   255
         Left            =   2880
         TabIndex        =   25
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label23 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFC0C0&
         Height          =   195
         Left            =   2880
         TabIndex        =   24
         Top             =   480
         Width           =   1200
      End
      Begin VB.Label Label21 
         BackStyle       =   0  'Transparent
         Caption         =   "Identiques"
         Height          =   255
         Left            =   4560
         TabIndex        =   23
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label20 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFC0&
         Height          =   195
         Left            =   4560
         TabIndex        =   22
         Top             =   480
         Width           =   1200
      End
      Begin VB.Label Label19 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   17.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   7920
         TabIndex        =   21
         Top             =   600
         Width           =   1920
      End
      Begin VB.Label Label18 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFC0FF&
         Height          =   195
         Left            =   6240
         TabIndex        =   20
         Top             =   480
         Width           =   1200
      End
      Begin VB.Label Label17 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFC0&
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   1080
         Width           =   2085
      End
      Begin VB.Label Label16 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   480
         Width           =   2085
      End
      Begin VB.Label Label15 
         BackStyle       =   0  'Transparent
         Caption         =   "Export"
         Height          =   255
         Left            =   6240
         TabIndex        =   17
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label14 
         BackStyle       =   0  'Transparent
         Caption         =   "Dossier 'B'"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Dossier 'A'"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "Total Analys�s"
         Height          =   255
         Left            =   8280
         TabIndex        =   14
         Top             =   240
         Width           =   1095
      End
   End
   Begin RichTextLib.RichTextBox RichTextBox1 
      Height          =   5415
      Left            =   10680
      TabIndex        =   10
      Top             =   6240
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   9551
      _Version        =   393217
      Appearance      =   0
      TextRTF         =   $"Form1.frx":19722
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Comparer"
      ForeColor       =   &H00000000&
      Height          =   2055
      Left            =   120
      TabIndex        =   7
      Top             =   6600
      Width           =   10095
      Begin VB.CheckBox Check7 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2280
         TabIndex        =   0
         Top             =   600
         Width           =   255
      End
      Begin VB.CheckBox Check5 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2280
         TabIndex        =   4
         Top             =   1560
         Width           =   375
      End
      Begin VB.CheckBox Check2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Nouveaux sur A = Suppr sur B"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   6480
         TabIndex        =   3
         Top             =   1080
         Width           =   2655
      End
      Begin VB.CheckBox Check1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Nouveaux sur B = Suppr sur A"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2280
         TabIndex        =   2
         Top             =   1080
         Width           =   2655
      End
      Begin VB.ComboBox Combo1 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   2640
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   600
         Width           =   4320
      End
      Begin VB.Shape Shape9 
         BorderStyle     =   0  'Transparent
         FillColor       =   &H00FFFFC0&
         FillStyle       =   0  'Solid
         Height          =   255
         Left            =   7560
         Top             =   600
         Width           =   255
      End
      Begin VB.Shape Shape7 
         BorderStyle     =   0  'Transparent
         FillColor       =   &H00C0FFC0&
         FillStyle       =   0  'Solid
         Height          =   255
         Left            =   2760
         Top             =   1560
         Width           =   255
      End
      Begin VB.Shape Shape6 
         BorderStyle     =   0  'Transparent
         FillColor       =   &H00C0E0FF&
         FillStyle       =   0  'Solid
         Height          =   255
         Left            =   9240
         Top             =   1080
         Width           =   255
      End
      Begin VB.Shape Shape4 
         BorderStyle     =   0  'Transparent
         FillColor       =   &H00FFC0C0&
         FillStyle       =   0  'Solid
         Height          =   255
         Left            =   5160
         Top             =   1080
         Width           =   255
      End
      Begin VB.Shape Shape3 
         BorderStyle     =   0  'Transparent
         FillColor       =   &H00C0FFFF&
         FillStyle       =   0  'Solid
         Height          =   255
         Left            =   7200
         Top             =   600
         Width           =   255
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Identiques"
         Height          =   255
         Left            =   480
         TabIndex        =   12
         Top             =   1560
         Width           =   1335
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Ajout�s/Supprim�s"
         Height          =   255
         Left            =   480
         TabIndex        =   11
         Top             =   1080
         Width           =   1335
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Diff�rents"
         Height          =   255
         Left            =   480
         TabIndex        =   8
         Top             =   600
         Width           =   1935
      End
   End
   Begin VB.CommandButton Command1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FF8080&
      Caption         =   "Lancer le traitement"
      Height          =   495
      Left            =   8040
      MaskColor       =   &H00FFC0C0&
      TabIndex        =   5
      Top             =   11520
      Width           =   2175
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   5535
      Left            =   10680
      TabIndex        =   9
      Top             =   240
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   9763
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ColHdrIcons     =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Fichier"
         Object.Width           =   6950
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Etat"
         Object.Width           =   1253
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Export"
         Object.Width           =   1252
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   3
         Text            =   "Date origine"
         Object.Width           =   2893
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Text            =   "Taille"
         Object.Width           =   1658
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Text            =   "Fiche"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   6
         Text            =   "Date r�f�rence"
         Object.Width           =   2893
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Text            =   "Taille"
         Object.Width           =   1658
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Text            =   "Fiche"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   11640
      Width           =   7680
   End
   Begin VB.Menu LVpopupmenu 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu mnuExportexcel 
         Caption         =   "E&xporter sous Excel"
      End
      Begin VB.Menu empty 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCompare 
         Caption         =   "&Comparer A et B"
      End
      Begin VB.Menu mnuFileOpenAB 
         Caption         =   "Ouvrir A et B"
      End
      Begin VB.Menu mnuExportSuppr 
         Caption         =   "Supprimer de l'export"
      End
      Begin VB.Menu mnuFileA 
         Caption         =   "Fichier(s) A"
         Begin VB.Menu mnuExportA 
            Caption         =   "Exporter"
         End
         Begin VB.Menu mnuHorodatage 
            Caption         =   "Horodatage selon fiche"
         End
         Begin VB.Menu mnuFileOpenA 
            Caption         =   "Ouvrir"
         End
         Begin VB.Menu mnuFilePropertiesA 
            Caption         =   "Propri�t�s"
         End
      End
      Begin VB.Menu mnuFileB 
         Caption         =   "Fichier(s) B"
         Begin VB.Menu mnuExportB 
            Caption         =   "Exporter"
         End
         Begin VB.Menu mnuFileOpenB 
            Caption         =   "Ouvrir"
         End
         Begin VB.Menu mnuFilePropertiesB 
            Caption         =   "Propri�t�s"
         End
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

 Private Const NM_FIRST = &H0& '(0U- 0U)
 Private Const NM_CUSTOMDRAW = (NM_FIRST - 12)
 Private Const WM_NOTIFY = &H4E

 Private Const CDDS_PREPAINT = &H1
 Private Const CDDS_POSTPAINT = &H2
 Private Const CDDS_PREERASE = &H3
 Private Const CDDS_POSTERASE = &H4

 Private Const CDDS_ITEM = &H10000
 Private Const CDDS_ITEMPREPAINT = (CDDS_ITEM Or CDDS_PREPAINT)
 Private Const CDDS_ITEMPOSTPAINT = (CDDS_ITEM Or CDDS_POSTPAINT)
 Private Const CDDS_ITEMPREERASE = (CDDS_ITEM Or CDDS_PREERASE)
 Private Const CDDS_ITEMPOSTERASE = (CDDS_ITEM Or CDDS_POSTERASE)
 Private Const CDDS_SUBITEM = &H20000

 Private Const CDRF_DODEFAULT = &H0
 Private Const CDRF_NEWFONT = &H2
 Private Const CDRF_SKIPDEFAULT = &H4
 Private Const CDRF_NOTIFYPOSTPAINT = &H10
 Private Const CDRF_NOTIFYITEMDRAW = &H20
 Private Const CDRF_NOTIFYSUBITEMDRAW = &H20
 Private Const CDRF_NOTIFYPOSTERASE = &H40

 Private Type RECT
     left As Long
     top As Long
     right As Long
     bottom As Long
 End Type

 Private Type NMHDR
     hwndFrom As Long
     idFrom As Long
     code As Long
 End Type

 Private Type NMCUSTOMDRAW
     hdr As NMHDR
     dwDrawStage As Long
     hdc As Long
     rc As RECT
     dwItemSpec As Long
     uItemState As Long
     lItemlParam As Long
 End Type

 Private Type NMLVCUSTOMDRAW
     nmcd As NMCUSTOMDRAW
     clrText As Long
     clrTextBk As Long
     'Les membres suivants ne sont pas disponibles pour tous les OS
     ' iSubItem As Long
     ' dwItemType As Long
     ' clrFace As Long
     ' iIconEffect As Long
     ' iIconPhase As Long
     ' iPartId As Long
     ' iStateId As Long
     ' rcText As RECT
     ' uAlign As Long
 End Type

 Private Declare Sub CopyMemory _
     Lib "kernel32" _
     Alias "RtlMoveMemory" _
     ( _
     Destination As Any, _
     Source As Any, _
     ByVal Length As Long)

Private Sub Command1_Click()
'-> on fait un minimum des verifs
If Me.Command1.Caption = "STOP" Then
    quit = True
    Me.Command1.Caption = "Lancer le traitement"
    Exit Sub
End If
quit = False

If Trim(Me.Text1.Text) = "" And Trim(Me.Text2.Text) = "" Then
    MsgBox ("R�pertoires non renseign�s!")
    Me.Text1.SetFocus
    Exit Sub
End If

If Trim(Me.Text1.Text) = "" Then Me.Text1.Text = Me.Text2.Text
wdir = Me.Text1.Text

If Trim(Me.Text2.Text) = "" Then Me.Text2.Text = Me.Text1.Text
rdir = Me.Text2.Text

If Mid(wdir, Len(wdir), 1) = "/" Or Mid(wdir, Len(wdir), 1) = "\" Then
Else
    wdir = wdir & "\"
End If

If Mid(rdir, Len(wdir), 1) = "/" Or Mid(rdir, Len(rdir), 1) = "\" Then
Else
    rdir = rdir & "\"
End If

'-> on regarde si on doit ecrire le mouchard
If Me.Text3.Text <> "" Then
    ismouchard = True
    If FileExist(Me.Text3.Text) Then Kill Me.Text3.Text
End If

LockWindowUpdate Me.hWnd
Dim i As Long
For i = Me.ListView1.ListItems.Count To 1 Step -1
    Me.ListView1.ListItems.Remove (i)
Next
LockWindowUpdate 0

'-> on charge les top
Call loadTop

'-> si l'option export est active
If Trim(Me.Text4.Text) <> "" Then
    If Me.Check8.Value = 1 Then isexport = True
    edir = Trim(Me.Text4.Text)
    If Mid(edir, Len(edir), 1) = "/" Or Mid(edir, Len(edir), 1) = "\" Then
    Else
        edir = edir & "\"
    End If
    edir = Replace(edir, "/", "\")
    If isexport And (Dir$(edir & "*", vbNormal) <> "" Or Dir$(edir & "*", vbDirectory) <> "") Then
        If MsgBox("Attention le repertoire d'export n'est pas vide !" & vbCrLf & "Continuer?", vbYesNo, "Warning") = vbNo Then Exit Sub
    End If
End If

rdir = Replace(rdir, "/", "\")
wdir = Replace(wdir, "/", "\")


'-> on lance l'analyse
recursive = True
Me.MousePointer = 11
Me.Command1.Caption = "STOP"
'-
'--> on lance le traitement en se basant sur le r�pertoire de travail (tratement des differences des supprim�s et des identiques)
CreateFileC wdir, rdir, ""
'--> on lance le traitement en se basant sur le repertoire de reference (traitement des fichiers ajout�s)
If rdir <> wdir Then CreateFileD wdir, rdir, ""
'-> on affiche les stats
PutStat
Me.MousePointer = 0
If ismouchard Then Close hdlMouchard
hdlMouchard = 0
Me.Label4.Caption = "End..."
Me.Command1.Caption = "Lancer le traitement"
End Sub

Private Sub loadTop()
'--> cette fonction recharge les top

isexport = False
topAddRef = False
topAddOri = False
topRepSame = False
topSame = False
topIdentSuppr = False
topIdentPrio = False
topDiff = False
topHorodatage = False
topHorodatageFiche = False
cptFileAbsRef = 0
cptFileModify = 0
cptFileCopy = 0
cptFileIdem = 0
cptFileListview = 0
cptFileNewer = 0
cptFileRef = 0
cptFileWork = 0
cptFileTot = 0
cptFileAnachro = 0
dateMini = ""
cptFileRefFiltre = 0
cptFileWorkFiltre = 0
repautori = ""
repignore = ""
fileautori = ""
fileignore = ""

If Me.Check7.Value = 1 Then topDiff = True
If Me.Check1.Value = 1 Then topAddRef = True   '-> pour traiter les nouveaux fichiers
If Me.Check2.Value = 1 Then topAddOri = True '-> pour traiter les fichiers supprim�s
If Me.Check3.Value = 1 Then topRepSame = True   '-> pour traiter les repertoires uniquement connus
If Me.Check5.Value = 1 Then topSame = True  '-> on exporte aussi les fichiers identiques pour pouvoir creer un nouvel env
If Me.Check12.Value = 1 Then topHorodatage = True  '-> on exporte aussi les fichiers identiques pour pouvoir creer un nouvel env
'If Me.Check13.Value = 1 Then topHorodatageFiche = True

If Me.Check9.Value = 1 Then repignore = Trim(Me.Text6.Text)
If Me.Check10.Value = 1 Then repautori = Trim(Me.Text7.Text)
If Me.Check6.Value = 1 Then fileignore = Trim(Me.Text9.Text)
If Me.Check11.Value = 1 Then fileautori = Trim(Me.Text10.Text)

'-> on charge l'ident
ident = Trim(Me.Text5.Text)

'->si les repertoires A & B sont identiques on allume les top pour l'export
If Trim(Me.Text1.Text) <> "" And Trim(Me.Text2.Text) = "" Or Trim(Me.Text1.Text) = Trim(Me.Text2.Text) Then
    '-> comme les sources sont identiques tout est pareil
    topRepSame = True
    topSame = True
End If

If Me.Check8.Value = 1 And Trim(Me.Text4.Text) <> "" Then isexport = True

'-> les dates
If IsDate(Me.Text5.Text) Then dateMini = Format(Trim(Me.Text5.Text), "yyyymmdd")
If IsDate(Me.Text8.Text) Then dateMaxi = Format(Trim(Me.Text8.Text), "yyyymmdd")

'-> si on est juste sur un ndossier on cache des colonnes
If rdir = wdir Then
    Me.ListView1.ColumnHeaders(7).Width = 0
    Me.ListView1.ColumnHeaders(8).Width = 0
    Me.ListView1.ColumnHeaders(9).Width = 0
Else
    Me.ListView1.ColumnHeaders(7).Width = 1640
    Me.ListView1.ColumnHeaders(8).Width = 940
    Me.ListView1.ColumnHeaders(9).Width = 1440
End If

If isexport Then
    Me.ListView1.ColumnHeaders(3).Width = 910
Else
    Me.ListView1.ColumnHeaders(3).Width = 0
End If

End Sub

Private Sub Form_Load()
Dim aControl

On Error Resume Next
'-> on charge la combo des exports
Me.Combo1.AddItem "Fichiers du r�pertoire B (plus r�cents)"
Me.Combo1.AddItem "Fichiers du r�pertoire B (plus anciens)"
Me.Combo1.AddItem "Fichiers du r�pertoire B (tous)"
Me.Combo1.AddItem "Fichiers du r�pertoire A (plus r�cents)"
Me.Combo1.AddItem "Fichiers du r�pertoire A (plus anciens)"
Me.Combo1.AddItem "Fichiers du r�pertoire A (tous)"
Me.Combo1.AddItem "Fichiers dont la fiche est sup�rieure sur A"
Me.Combo1.AddItem "Fichiers dont la fiche est sup�rieure sur B"
Me.Combo1.AddItem "Fichiers dont la fiche est sup�rieure A ou B"
Me.Combo1.ListIndex = 0

For Each aControl In Me.Controls
    If TypeOf aControl Is TextBox Then
        aControl.Text = GetSetting(App.Path & "\" & App.Title, "Properties", aControl.Name)
    End If
    If TypeOf aControl Is CheckBox Then
        aControl.Value = Val(GetSetting(App.Path & "\" & App.Title, "Properties", aControl.Name))
    End If
Next

'Sousclasse la fen�tre
 Subclasser.Subclass Me.hWnd, Me, "Subclasser_Message"

End Sub

Public Sub FileAddInListview(strFichier As String, strFichier2 As String, strEtat As String, strExport As String)
'--> cette proc�dure permet de charger un fichier dans le listview
Dim aItem As ListItem
Dim i As Long

On Error Resume Next
If Me.List.Value <> 1 Then Exit Sub
If Me.ListView1.ListItems.Count > 84000 Then Exit Sub

Me.ListView1.ListItems.add , strFichier & "|" & Me.ListView1.ListItems.Count, Replace(strFichier, "", "")

Set aItem = Me.ListView1.ListItems(strFichier & "|" & (Me.ListView1.ListItems.Count - 1))
aItem.ListSubItems.add , , strEtat
aItem.ListSubItems.add , , strExport

If strFichier2 = "" Then
    strFichier2 = strFichier
    strFichier = ""
    aItem.ListSubItems.add , , ""
    aItem.ListSubItems.add , , ""
    aItem.ListSubItems.add , , ""
    aItem.ListSubItems.add , , Format(FileDateTime(strFichier2), "dd/mm/yyyy   hh:mm")
    aItem.ListSubItems.add , , FormatFileSize(FileLen(strFichier2), 0)
    aItem.ListSubItems.add , , getFiche(strFichier2)
Else
    aItem.ListSubItems.add , , Format(FileDateTime(strFichier), "dd/mm/yyyy   hh:mm")
    aItem.ListSubItems.add , , FormatFileSize(FileLen(strFichier), 0)
    aItem.ListSubItems.add , , getFiche(strFichier)
    If strFichier <> strFichier2 Then
        '-> si les deux fichiers sont diff�rents
        aItem.ListSubItems.add , , Format(FileDateTime(strFichier2), "dd/mm/yyyy   hh:mm")
        aItem.ListSubItems.add , , FormatFileSize(FileLen(strFichier2), 0)
        aItem.ListSubItems.add , , getFiche(strFichier2)
    Else
        '-> on evite le recalcul
        aItem.ListSubItems.add , , aItem.ListSubItems(3).Text
        aItem.ListSubItems.add , , aItem.ListSubItems(4).Text
        aItem.ListSubItems.add , , aItem.ListSubItems(5).Text
    End If
End If

cptFileListview = Me.ListView1.ListItems.Count
End Sub

Public Function listviewRefresh()
'--> cette fonction parcourt le listview et met � jour le statut des fichiers
Dim aItem As ListItem
Dim strFichier As String
Dim strFichier2 As String

For Each aItem In Me.ListView1.ListItems
    strFichier = aItem.Text
    If Replace(strFichier, wdir, "") <> strFichier Then
        strFichier2 = Replace(strFichier, wdir, rdir)
        aItem.ListSubItems.Item(2).Text = Format(FileDateTime(strFichier), "dd/mm/yyyy   hh:mm")
        aItem.ListSubItems.Item(3).Text = FormatFileSize(FileLen(strFichier), 0)
        aItem.ListSubItems.Item(4).Text = getFiche(strFichier)
        If strFichier <> strFichier2 Then
            '-> si les deux fichiers sont diff�rents
            aItem.ListSubItems.Item(5).Text = Format(FileDateTime(strFichier2), "dd/mm/yyyy   hh:mm")
            aItem.ListSubItems.Item(6).Text = FormatFileSize(FileLen(strFichier2), 0)
            aItem.ListSubItems.Item(7).Text = getFiche(strFichier2)
        Else
            '-> on evite le recalcul
            aItem.ListSubItems.Item(5).Text = aItem.ListSubItems(2).Text
            aItem.ListSubItems.Item(6).Text = aItem.ListSubItems(3).Text
            aItem.ListSubItems.Item(7).Text = aItem.ListSubItems(4).Text
        End If
        '-> pour le repertoire des exports
        If isexport Then
            If FileExist(Replace(strFichier, wdir, edir)) Then
                aItem.ListSubItems.Item(2).Text = "A"
            Else
                If FileExist(Replace(strFichier, rdir, edir)) Then
                    aItem.ListSubItems.Item(2).Text = "B"
                End If
            End If
        End If
    Else
        aItem.ListSubItems.Item(4).Text = Format(FileDateTime(strFichier), "dd/mm/yyyy   hh:mm")
        aItem.ListSubItems.Item(5).Text = FormatFileSize(FileLen(strFichier), 0)
        aItem.ListSubItems.Item(6).Text = getFiche(strFichier)
    End If
    DoEvents
Next

End Function

Public Function getFiche(strFile As String) As String
'--> cette fonction va chercher la derniere fiche du fichier
Dim Ligne As String
Dim hdlFile As Integer
Dim i As Integer

If Me.Check4.Value = 0 Then Exit Function

hdlFile = FreeFile
Open strFile For Input As #hdlFile
Do While Not EOF(hdlFile)
    If i > 10 Then Exit Do
    Line Input #hdlFile, Ligne
    If InStr(1, Ligne, "!V61 ") <> 0 Or InStr(1, Ligne, "!V51 ") <> 0 Or InStr(1, Ligne, "!V52 ") <> 0 Then
        getFiche = Mid(Ligne, 5, 6)
        Exit Do
    End If
Loop
Close #hdlFile
If cptFileTot Mod 10 = 0 Then DoEvents

End Function

Public Function getFicheDate(strFile As String) As String
'--> cette fonction va chercher la derniere fiche du fichier
Dim Ligne As String
Dim hdlFile As Integer
Dim i As Integer

If Me.Check4.Value = 0 Then Exit Function

hdlFile = FreeFile
Open strFile For Input As #hdlFile
Do While Not EOF(hdlFile)
    If i > 10 Then Exit Do
    Line Input #hdlFile, Ligne
    If InStr(1, Ligne, "!V61 ") <> 0 Or InStr(1, Ligne, "!V51 ") <> 0 Or InStr(1, Ligne, "!V52 ") <> 0 Then
        Ligne = Replace(Ligne, "  ", " ")
        getFicheDate = Entry(1, Entry(3, Ligne, " "), "!")
        getFicheDate = Entry(1, getFicheDate, "-") + "/" + Entry(2, getFicheDate, "-") + "/" + "20" + Entry(3, getFicheDate, "-")
        Exit Do
    End If
Loop
Close #hdlFile
'-> un ptit refresh!
If cptFileTot Mod 10 = 0 Then DoEvents

End Function

Public Sub PutStat()
    Me.Label16.Caption = cptFileWorkFiltre & "/" & cptFileWork
    Me.Label17.Caption = cptFileRefFiltre & "/" & cptFileRef
    Me.Label20.Caption = cptFileIdem
    Me.Label18.Caption = cptFileCopy
    Me.Label19.Caption = cptFileTot
    Me.Label25.Caption = cptFileAbsRef
    Me.Label23.Caption = cptFileNewer
    Me.Label29.Caption = cptFileAnachro
    Me.Label27.Caption = cptFileModify
End Sub
Private Function FormatFileSize(ByVal Size As Long, FormatType As Integer) As String
'--> cette fonction permet de transformer la taille d'un fichier
Dim sRet As String
Const KB& = 1024
Const MB& = KB * KB

'FormatType = 0 Short String Format
'FormatType = 1 Long String Format
'FormatType = 2 Dual String Format

If Size < KB Then
   sRet = Format(Size, "#,##0") & " Bytes"
Else
   Select Case Size \ KB
      Case Is < 10
         sRet = Format(Size / KB, "0.00") & " KB"
      Case Is < 100
         sRet = Format(Size / KB, "0.0") & " KB"
      Case Is < 1000
         sRet = Format(Size / KB, "0") & " KB"
      Case Is < 10000
         sRet = Format(Size / MB, "0.00") & " MB"
      Case Is < 100000
         sRet = Format(Size / MB, "0.0") & " MB"
      Case Is < 1000000
         sRet = Format(Size / MB, "0") & " MB"
      Case Is < 10000000
         sRet = Format(Size / MB / KB, "0.00") & " GB"
   End Select
   
   Select Case FormatType
     Case 0 'Short
       sRet = sRet
     Case 1 'Long
       sRet = Format(Size, "#,##0") & " Bytes"
     Case 2 'Dual
       sRet = sRet & " (" & Format(Size, "#,##0") & " Bytes)"
   End Select
End If

FormatFileSize = sRet
End Function

Private Sub Form_Resize()
On Error Resume Next
Me.RichTextBox1.Width = Me.Width - Me.RichTextBox1.left - 400
Me.RichTextBox1.Height = Me.Height - Me.RichTextBox1.top - 750
Me.ListView1.Width = Me.Width - Me.ListView1.left - 400
End Sub

Private Sub Form_Unload(Cancel As Integer)
'-> on parcour les controles pour eventuellement les memoriser
Dim aControl

'--> le subclassing du listview
Subclasser.UnSubclassAll

For Each aControl In Me.Controls
    If TypeOf aControl Is TextBox Then
        SaveSetting App.Path & "\" & App.Title, "Properties", aControl.Name, aControl.Text
    End If
    If TypeOf aControl Is CheckBox Then
        SaveSetting App.Path & "\" & App.Title, "Properties", aControl.Name, aControl.Value
    End If
Next

End Sub

Private Sub Label23_Click()

Dim aItem As ListItem

For Each aItem In Me.ListView1.ListItems
   ' Me.ListView2.ListItems.add aItem
Next
End Sub

Private Sub ListView1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
'-> Trier sur les entetes de colonne
ColumnOrder Me.ListView1, ColumnHeader

End Sub

Private Sub ListView1_DblClick()
'--> on lance une comparaison
Dim command As String
Dim strTemp As String
Dim hdlFile As Integer
Dim CurrentPIDProcess As Long

strTemp = Me.ListView1.SelectedItem.Text

hdlFile = FreeFile
Open wdir & "\diff_file.bat" For Output As #hdlFile
command = "fc /L " & strTemp & " " & Replace(strTemp, wdir, rdir) & " > " & wdir & "\diff_file.txt"
Print #hdlFile, command
'->on ferme le fichier
Close #hdlFile

CurrentPIDProcess = Shell(wdir & "\diff_file.bat")

Do While IsPidRunning(CurrentPIDProcess)
    '-> Lib�ration de la pile des messages
    DoEvents
    Sleep 100
Loop

Me.RichTextBox1.TextRTF = OpenFileAPI(wdir & "\diff_file.txt")

Kill wdir & "\diff_file.txt"
Kill wdir & "\diff_file.bat"
End Sub

Private Sub ListView1_KeyDown(KeyCode As Integer, Shift As Integer)
Dim exportFile As String
Dim aRep
Dim i As Long
Dim strRep As String

If Me.ListView1.SelectedItem Is Nothing Then Exit Sub
'-> sur la touche insert on effectue une action
If KeyCode = 45 Then
    exportFile = Replace(Me.ListView1.SelectedItem.Text, wdir, edir)
    strRep = Entry(1, exportFile, "\")
    For i = 2 To NumEntries(exportFile, "\") - 1
        strRep = strRep & "\" & Entry(i, exportFile, "\")
        If Dir(strRep, vbDirectory) = "" Then
            MkDir (strRep)
        End If
    Next
    cptFileCopy = cptFileCopy + 1
    FileCopy Me.ListView1.SelectedItem.Text, exportFile
    Me.ListView1.SelectedItem.ForeColor = vbGreen
End If

'-> suppr
If KeyCode = 46 Then
    Me.ListView1.ListItems.Remove (Me.ListView1.SelectedItem)
End If

cptFileListview = Me.ListView1.ListItems.Count

End Sub

Public Sub ColumnOrder(aList As ListView, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
'--> cette procedure permet de trier les colonnes d'un listview
Dim aItem As ListItem
Dim aCol As ColumnHeader
Dim i As Integer
Dim j As Integer

'-> gestion des erreurs
On Error Resume Next

'-> on ajoute une colonne cachee pour le tri
aList.ColumnHeaders.add , "Cache"
'-> on la masque
aList.ColumnHeaders("Cache").Width = 0

'on vide la colonne cach�e
For Each aItem In aList.ListItems
    aItem.SubItems(aList.ColumnHeaders.Count - 1) = ""
Next
'-> si on a des ruptures et pas sur la colonne en cours on envoi les donnees dans la colonne cachee
'-> on teste l'allignement
Select Case ColumnHeader.Alignment
 Case lvwColumnRight
    '-> on met des blancs devant on est sur des chiffres
     For Each aItem In aList.ListItems
        If Not (Entry(1, aItem.Key, "|") = "Rupture") Then  'And aItem.SubItems(ColumnHeader.Index - 1) = "") Then
            aItem.SubItems(aList.ColumnHeaders.Count - 1) = right(Space(20) & FileLen(aItem.Key), 20) 'aItem.SubItems(aList.ColumnHeaders.Count - 1) + Right(Space(20) & Str((CDbl(Replace(Replace(Replace(aItem.SubItems(ColumnHeader.Index - 1), " Bytes", ""), " MB", "000000"), " KB", "000"))) * 1000), 20)
        Else '-> on est sur une ligne de rupture
            aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + Mid("ZZZZZZZZZZZZZZZZZZ", 1, 15 - CInt(Entry(2, aItem.Key, "|")))
        End If
     Next
 Case lvwColumnLeft, lvwColumnCenter
        '-> on est sur une chaine on regarde si on est sur une colonne de dates
        For Each aItem In aList.ListItems
            '-> on verifie si on a que des dates!!
            If ColumnHeader.Index <> 1 Then
                If Not IsDate(aItem.SubItems(ColumnHeader.Index - 1)) Then
                     '-> on copie la chaine tel quel
                         aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + aItem.SubItems(ColumnHeader.Index - 1)
                Else
                     '-> on est sur des dates on les formate yyyymmdd
                     aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + Format(aItem.SubItems(ColumnHeader.Index - 1), "yyyymmddhhmmss")
                End If
            Else '-> on est sur la premiere colonne
                If Not IsDate(aItem.Text) Then
                     '-> on copie la chaine tel quel
                     aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + aItem.Text
                Else
                     '-> on est sur des dates on les formate yyyymmdd
                     aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + Format(aItem.Text, "yyyymmdd")
                End If
            End If '-> on est sur la premiere colonne ou pas
            If Entry(1, aItem.Key, "|") = "Rupture" Then 'And aItem.SubItems(ColumnHeader.Index - 1) = "") Then
                '-> on est sur une ligne de rupture
                aItem.SubItems(aList.ColumnHeaders.Count - 1) = aItem.SubItems(aList.ColumnHeaders.Count - 1) + Mid("ZZZZZZZZZZZZZZZZZZ", 1, 15 - CInt(Entry(2, aItem.Key, "|")))
            End If
        Next
End Select
'-> Trier sur les entetes de colonne
aList.SortKey = aList.ColumnHeaders("Cache").Index - 1
If aList.SortOrder = lvwAscending Then
    aList.SortOrder = lvwDescending
Else
    aList.SortOrder = lvwAscending
End If
aList.Sorted = True

'-> on supprime la colonne cach�e
aList.ColumnHeaders.Remove ("Cache")

End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

If Button = vbRightButton Then
    PopupMenu LVpopupmenu, vbPopupMenuRightButton
 End If

End Sub

Public Sub ExportToExcel(aListView As ListView)

Dim Ligne As String
Dim x As ListItem
Dim aFeuille As Object
Dim aClasseur As Object
Dim MyApp As Object
Dim aRange As Object
Dim i As Integer
Dim aRangeToFormat As Object
Dim aCol As ColumnHeader
Dim ValueField As String
Dim NbCol As Integer
Dim ListViewFormat() As Variant

Screen.MousePointer = 11

'On Error GoTo GestError

If Not IsExcel Then
    Screen.MousePointer = 0
    MsgBox "Excel non install�", vbCritical, "Erreur"
    Exit Sub
End If

'-> Cr�er une nouvelle instance d'excel
'ShowWait "Export vers Excel en cours"
Set MyApp = CreateObject("Excel.application")

'-> Ajouter une classeur
Set aClasseur = MyApp.Workbooks.add()

'-> Supprimer les questions
MyApp.displayalerts = False

'-> Supprimer les 2 feuilles en trop
'aClasseur.Sheets(3).Delete
'aClasseur.Sheets(2).Delete

'-> Get d'un pointeur vers la feuille active
Set aFeuille = aClasseur.activesheet
Set aRange = aFeuille.Range("$A$1")

'-> Raz de la variable
NbCol = 0

'-> Cr�ation de la ligne d'entete
For Each aCol In aListView.ColumnHeaders
    '-> Cr�ation de la ligne
    Ligne = AddEntryInMatrice(Ligne, aCol.Text, "|")
    NbCol = NbCol + 1
Next 'Pour toutes les colonnes

'-> Cr�er la ligne des entetes
If Ligne <> "" Then
    aRange.Value = Ligne
    '-> Eclater sur les colonnes suivantes
    aRange.TextToColumns Destination:=aFeuille.Range("" & aRange.AddressLocal & ""), DataType:=1, _
            TextQualifier:=-4142, ConsecutiveDelimiter:=False, Tab:=False, _
            Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar:="|"
    '-> S�lectionner la ligne en entier
    Set aRangeToFormat = aFeuille.Range(aRange, aRange.offset(, NbCol - 1))
    '-> Appliquer un format
    ApplicExcelFormat aRangeToFormat, 1
    '-> Vider la matrice
    Ligne = ""
End If 'Si on a trouv� les entetes

'-> valeurs par defaut
ListViewFormat = Array(Array(1, 2), Array(2, 2), Array(3, 2), Array(4, 2), Array(5, 2), Array(6, 2), Array(7, 2), Array(8, 2), Array(9, 2), Array(10, 2), Array(11, 2), Array(12, 2), Array(13, 2), Array(14, 2), Array(15, 2))
 '-> on redimensionne le tableau
ReDim Preserve ListViewFormat(aListView.ColumnHeaders.Count)

'-> on reccupere le format du listview
For Each aCol In aListView.ColumnHeaders
    '-> on redimensionne le tableau en conservant l'aquis
    If aCol.Alignment = 1 Then
        '-> alignement � droite c'est un chiffre
        ListViewFormat(aCol.Index - 1)(1) = 1
    Else
        '-> les autres colonnes c'est du string
        ListViewFormat(aCol.Index - 1)(1) = 2
    End If
Next

'-> S�lectionner la ligne suivante
Set aRange = aFeuille.Range("$A$2")

'-> Exporter tous les enregsitrements
For Each x In aListView.ListItems
    'ShowWait "Export vers Excel en cours " & x.Index & "/" & aListView.ListItems.Count
    '-> Cr�er la ligne � exporter
    For i = 1 To aListView.ColumnHeaders.Count
        '-> Tester la valeur a ajouter
        If i = 1 Then
            ValueField = x.Text
        Else
            ValueField = x.SubItems(i - 1)
        End If
        '-> Ajouter dans la matrice des lignes
        Ligne = Ligne & ValueField & "|"
        'Ligne = AddEntryInMatrice(Ligne, ValueField, "|", CLng(i))
    Next 'Pour tous les champs
    
    '-> Exporter cet enregistrement si <> ""
    If Trim(Ligne) <> "" Then
        '-> Transf�rer la ligne dans la cellule active
        aRange.Value = Ligne
        '-> Eclater sur les colonnes suivantes
        aRange.TextToColumns Destination:=aFeuille.Range("" & aRange.AddressLocal & ""), DataType:=1, _
            TextQualifier:=-4142, ConsecutiveDelimiter:=False, Tab:=False, _
            Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar:="|", FieldInfo:=ListViewFormat, DecimalSeparator:=","
    End If
    '-> D�caler de 1 cellule vers le bas
    Set aRange = aRange.offset(1, 0)
    '-> Raz de la variable
    Ligne = ""
    DoEvents
Next 'Pour tous les enregistrements dans la page

'-> Appliquer le format d'un coup
Set aRangeToFormat = aFeuille.Range("$A$2", aFeuille.Range("$A$2").offset(aListView.ListItems.Count - 1, NbCol - 1))
ApplicExcelFormat aRangeToFormat, 12

'-> Formatter les colonnes en largeur automatique
For i = 1 To NbCol
    '-> Largeur automatique de la colonne
    aFeuille.Columns(i).AutoFit
Next

'-> Rendre Excel visible
MyApp.Visible = True

'-> Rendre la main sur les questions
MyApp.displayalerts = True

GestError:
           
    '-> Lib�rer les pointeurs
    Set aRange = Nothing
    Set aRangeToFormat = Nothing
    Set aFeuille = Nothing
    Set aClasseur = Nothing
    Set MyApp = Nothing

    '-> D�bloquer la mise � jour
    Screen.MousePointer = 0
    'ShowWait "Export vers Excel en cours", True
End Sub

Public Function IsExcel() As Boolean

'---> Cette procedure indique si Excel est install� sur le poste ou non

Dim ExcelApp As Object

On Error GoTo GestError

'-> Essayer de pointer sur l'objet
Set ExcelApp = CreateObject("Excel.application")

'-> Renvoyer une valeur de succ�s
IsExcel = True

GestError:
    Set ExcelApp = Nothing
    
End Function

Public Sub ApplicExcelFormat(aRange As Object, Ligne As Long)


'---> Cette proc�dure applique un format � une cellule Excel

On Error Resume Next

aRange.Borders(5).LineStyle = -4142
aRange.Borders(6).LineStyle = -4142
With aRange.Borders(7)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(8)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(9)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(10)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(11)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(12)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With


'-> Couleur de fond est gras si c'est le titre
If Ligne = 1 Then
    With aRange.Interior
        .ColorIndex = 15
        .Pattern = 1
    End With
    aRange.Font.Bold = True
    With aRange
        .HorizontalAlignment = -4108
        .VerticalAlignment = -4107
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .ShrinkToFit = False
        .MergeCells = False
    End With
End If

'-> Suppreimer les erreurs de formats
Err.Number = 0

End Sub

Private Sub LVpopupmenu_Click()

'->on recharge les top avant une action
Call loadTop

If Me.ListView1.SelectedItem.ListSubItems(2).Text = "" Then
    Me.mnuExportSuppr.Enabled = False
Else
    Me.mnuExportSuppr.Enabled = True
End If

If isexport Then
    mnuExportA.Enabled = True
    mnuExportB.Enabled = True
Else
    mnuExportA.Enabled = False
    mnuExportB.Enabled = False
End If

'-> si on a qque chose dans le listview
If Not Me.ListView1.SelectedItem Is Nothing Then
    Me.mnuFileA.Enabled = True
    Me.mnuFileB.Enabled = True
    Me.mnuCompare.Enabled = True
    Me.mnuFileOpenAB.Enabled = True
    If rdir = wdir Then
        mnuFileB.Enabled = False
        Me.mnuCompare.Enabled = False
        Me.mnuFileOpenAB.Enabled = False
    End If
Else
    Me.mnuFileA.Enabled = False
    Me.mnuFileB.Enabled = False
    Me.mnuCompare.Enabled = False
    Me.mnuFileOpenAB.Enabled = False
End If



End Sub

Private Sub mnuCompare_Click()

Call ListView1_DblClick

End Sub

Private Sub mnuExportB_Click()
'--> on  exporte le fichier issu de B (originie)
Dim strTemp As String
Dim aItem As ListItem

On Error Resume Next

For Each aItem In Me.ListView1.ListItems
    If aItem.Selected Then
        strTemp = aItem.Text
        strTemp = Replace(strTemp, wdir, rdir)
        FileCopy strTemp, Replace(strTemp, wdir, edir)
        aItem.ListSubItems(2).Text = "B"
    End If
Next

End Sub

Private Sub mnuExportA_Click()
'--> on  exporte le fichier issu de A (originie)
Dim strTemp As String
Dim aItem As ListItem

On Error Resume Next

For Each aItem In Me.ListView1.ListItems
    If aItem.Selected Then
        strTemp = aItem.Text
        FileCopy strTemp, Replace(strTemp, wdir, edir)
        aItem.ListSubItems(2).Text = "A"
    End If
Next
End Sub

Public Sub ShowFileProperties(ByVal ps_FileName As String)
   
         Dim lu_ShellExUDT As SHELLEXECUTEINFO
         
         With lu_ShellExUDT
             .hWnd = 0 'hwnd
             .lpVerb = "properties"
             .lpFile = ps_FileName
             .fMask = SEE_MASK_INVOKEIDLIST
             .cbSize = Len(lu_ShellExUDT)
         End With
         
         Call ShellExecuteEx(lu_ShellExUDT)
   
End Sub

Private Sub mnuExportexcel_Click()
    '--> on exporte le listview sous excel
    ExportToExcel Me.ListView1
End Sub

Private Sub mnuExportSuppr_Click()
Dim strTemp As String
Dim aItem As ListItem

On Error Resume Next

For Each aItem In Me.ListView1.ListItems
    If aItem.Selected Then
        strTemp = aItem.Text
        strTemp = Replace(strTemp, wdir, rdir)
        Kill Replace(strTemp, wdir, edir)
        aItem.ListSubItems(2).Text = ""
    End If
Next

End Sub

Private Sub mnuFileOpenA_Click()
Dim strTemp As String
Dim aItem As ListItem

On Error Resume Next

For Each aItem In Me.ListView1.ListItems
    If aItem.Selected Then
        strTemp = aItem.Text
        '-> Lancer si necessaire
        ShellExecute Me.hWnd, "Open", strTemp, vbNullString, App.Path, 1
    End If
Next

End Sub

Private Sub mnuFileOpenAB_Click()

On Error Resume Next
'-> Lancer si necessaire
ShellExecute Me.hWnd, "Open", Replace(Me.ListView1.SelectedItem.Text, rdir, wdir), vbNullString, App.Path, 1
ShellExecute Me.hWnd, "Open", Replace(Me.ListView1.SelectedItem.Text, wdir, rdir), vbNullString, App.Path, 1

End Sub

Private Sub mnuFileOpenB_Click()
Dim strTemp As String
Dim aItem As ListItem

On Error Resume Next

For Each aItem In Me.ListView1.ListItems
    If aItem.Selected Then
        strTemp = aItem.Text
        strTemp = Replace(strTemp, wdir, rdir)
        '-> Lancer si necessaire
        ShellExecute Me.hWnd, "Open", strTemp, vbNullString, App.Path, 1
    End If
Next

End Sub

Private Sub mnuFilePropertiesA_Click()
'--> on  exporte le fichier issu de A (originie)
Dim strTemp As String

strTemp = Me.ListView1.SelectedItem.Text
ShowFileProperties strTemp

End Sub

Private Sub mnuFilePropertiesB_Click()
'--> on  exporte le fichier issu de A (originie)
Dim strTemp As String

strTemp = Me.ListView1.SelectedItem.Text
ShowFileProperties Replace(strTemp, wdir, rdir)

End Sub

 Public Function Subclasser_Message(ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As Long, SetNewValue As Boolean) As Long
     'On traite en fonction du message re�u
     Select Case Msg
         'Quand un contr�le notifie son parent
         Case WM_NOTIFY
             'Ce type de notifications poss�dent toutes une structure commune
             Dim Header As NMHDR

             'On copie la structure � partir de l'adresse offerte
             CopyMemory Header, ByVal lParam, Len(Header)

             'On regarde quel contr�le notifie son parent
             Select Case Header.hwndFrom
                 'S'il s'agit de notre listview
                 Case ListView1.hWnd
                     'On regarde ce qu'il souhaite nous dire
                     Select Case Header.code
                         'Il souhaiterait savoir comment il doit effectuer son tra�age ?
                         Case NM_CUSTOMDRAW
                             'On copie la structure correspondante
                             '(avant on avait uniquement copi� l'en-t�te)
                             Dim LVCust As NMLVCUSTOMDRAW
                             CopyMemory LVCust, ByVal lParam, Len(LVCust)

                             'On regarde le niveau de tra�age (drawstage)
                             Select Case LVCust.nmcd.dwDrawStage
                                 'Avant de peindre la liste
                                 Case CDDS_PREPAINT
                                     'On demande une notification de tra�age
                                     'pour chaque item de la liste
                                     SetNewValue = True
                                     Subclasser_Message = CDRF_NOTIFYITEMDRAW
     
                                 'Avant de peindre un �l�ment
                                 Case CDDS_ITEMPREPAINT
                                     'Un item sur deux sera peint en blanc
'                                     If LVCust.nmcd.dwItemSpec Mod 2 Then
'                                         LVCust.clrTextBk = vbWhite
'                                     Else 'l'autre en rouge
'                                         LVCust.clrTextBk = &H99FFBD '#BDFF99
'                                     End If
                                        Dim i As Integer
                                        Dim aItem As ListItem
                                        i = LVCust.nmcd.dwItemSpec + 1
                                        If i > 0 Then
                                            Set aItem = Me.ListView1.ListItems(i)
                                            If isexport Then
                                                If aItem.ListSubItems.Item(2).Text = "A" Then
                                                    If aItem.ListSubItems.Item(1).Text = "<" Then LVCust.clrTextBk = &HC0FFFF
                                                    If aItem.ListSubItems.Item(1).Text = ">" Then LVCust.clrTextBk = &HC0FFFF
                                                    If aItem.ListSubItems.Item(1).Text = "+>" Then LVCust.clrTextBk = &HC0E0FF
                                                    If aItem.ListSubItems.Item(1).Text = "<+" Then LVCust.clrTextBk = &HFFC0FF    'a la mano
                                                
                                                ElseIf aItem.ListSubItems.Item(2).Text = "B" Then
                                                    If aItem.ListSubItems.Item(1).Text = ">" Then LVCust.clrTextBk = &HFFFFC0
                                                    If aItem.ListSubItems.Item(1).Text = "<" Then LVCust.clrTextBk = &HFFFFC0
                                                    If aItem.ListSubItems.Item(1).Text = "<+" Then LVCust.clrTextBk = &HFFC0C0
                                                    If aItem.ListSubItems.Item(1).Text = "<+" Then LVCust.clrTextBk = &HFFC0FF    '-> � la mano
                                                    
                                                    
                                                ElseIf aItem.ListSubItems.Item(2).Text = "A/B" Then
                                                    LVCust.clrTextBk = &HC0FFC0
                                                End If
                                            Else
                                                    If aItem.ListSubItems.Item(1).Text = "<" Then LVCust.clrTextBk = &HFFFFC0      'plus r�cent sur B
                                                    If aItem.ListSubItems.Item(1).Text = ">" Then LVCust.clrTextBk = &HC0FFFF   'plus r�cent sur A
                                                    If aItem.ListSubItems.Item(1).Text = "+>" Then LVCust.clrTextBk = &HC0E0FF  'supprim�
                                                    If aItem.ListSubItems.Item(1).Text = "<+" Then LVCust.clrTextBk = &HFFC0C0  'nouveau
                                                    If aItem.ListSubItems.Item(1).Text = "=" Then LVCust.clrTextBk = &HC0FFC0  'identique
                                            End If
                                        End If
                             End Select
                             'On recopie la structure modifi�e
                             CopyMemory ByVal lParam, LVCust, Len(LVCust)
                     End Select
             End Select
     End Select
 End Function

Private Sub mnuHorodatage_Click()
'--> on  exporte le fichier issu de A (originie)
Dim strTemp As String
Dim strDate As String
Dim aItem As ListItem

On Error Resume Next

For Each aItem In Me.ListView1.ListItems
    If aItem.Selected Then
        strTemp = aItem.Text
        strDate = getFicheDate(strTemp)
        SetFileTimes strTemp, CDate(strDate), CDate(strDate), CDate(strDate), False
        aItem.ListSubItems.Item(3).Text = Format(FileDateTime(strTemp), "dd/mm/yyyy   hh:mm")
    End If
Next

End Sub
