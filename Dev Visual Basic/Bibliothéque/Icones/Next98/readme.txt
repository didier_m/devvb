=================================================
 NeXT 98 Icons
 (c)1999 by Masatoshi Ueji. All rights reserved.
=================================================

This is a set of hi-color NeXTSTEP icons,
which redesigned to the style of Windows98 icons.

Copyright Notice
----------------

The enclosed icons are FREEWARE for personal use.
These icons are Copyrighted By Masatoshi Ueji.
Do NOT distribute, change, rename without my expressed permission.
If you want to re-distribute them on your web page,
and to use them as graphics of your web page,
please drop me an e-mail and let me know the URL of your web page. 

---------------------------------------------
Masatoshi Ueji <mueji@venus.dti.ne.jp>
M's Factory (http://www.venus.dti.ne.jp/~mueji/)
