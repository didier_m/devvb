Attribute VB_Name = "ModMain"
Option Explicit

Const SW_HIDE = 0
Const SW_SHOWNORMAL = 1
Const SHFS_SHOWTASKBAR = &H1
Const SHFS_HIDETASKBAR = &H2
Const SHFS_SHOWSIPBUTTON = &H4
Const SHFS_HIDESIPBUTTON = &H8
Const SHFS_SHOWSTARTICON = &H10
Const SHFS_HIDESTARTICON = &H20
Const HWND_TOPMOST = -1
Const HWND_NOTOPMOST = -2
Const SWP_SHOWWINDOW = &H40
Const SM_CXSCREEN = &H0
Const SM_CYSCREEN = &H1
Const HHTASKBARHEIGHT = 26

Declare Function GetSystemMetrics Lib "Coredll" ( _
    ByVal nIndex As Long) As Long

Declare Function SHFullScreen Lib "aygshell" ( _
    ByVal hwndRequester As Long, _
    ByVal dwState As Long) As Boolean

Declare Function MoveWindow Lib "Coredll" ( _
    ByVal hwnd As Long, _
    ByVal x As Long, _
    ByVal y As Long, _
    ByVal nWidth As Long, _
    ByVal nHeight As Long, _
    ByVal bRepaint As Long) As Long

Declare Function SetForegroundWindow Lib "Coredll" ( _
    ByVal hwnd As Long) As Boolean

Declare Function GetLastError Lib "Coredll" () As Long

Declare Function ShowWindow Lib "Coredll" ( _
    ByVal hwnd As Long, _
    ByVal nCmdShow As Long) As Long

Declare Function FindWindow Lib "Coredll" Alias "FindWindowW" ( _
    ByVal lpClassName As String, _
    ByVal lpWindowName As String) As Long

Public Lig_Selected As ListItem
Public Pal_Selected As ListItem
Public RepSynchro As String '-> repertoire de synchro
Public strDepot As String


'Public Palettes As Collection
'Public Couple_Produits As Collection
Public Palettes(100) As String
Public Couple_Produits(10000) As String
Public NbDefault As Integer

'-> Pour pouvoir affecter au listview la selection d'une ligne enti�re
Declare Function SendMessage Lib "Coredll" Alias "SendMessageW" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Public Declare Function GetFocus Lib "Coredll" () As Long
Const LVM_GETEXTENDEDLISTVIEWSTYLE = &H1037
Const LVM_SETEXTENDEDLISTVIEWSTYLE = &H1036
Const LVS_EX_FULLROWSELECT = &H20
Const LVS_EX_GRIDLINES = &H1


'-> Pour pouvoir ne saisir que des chiffres dans une Textbox
Public Declare Function GetWindowLong Lib "Coredll" Alias "GetWindowLongW" (ByVal hwnd As Long, ByVal nIndex As Long) As Long
Public Declare Function SetWindowLong Lib "Coredll" Alias "SetWindowLongW" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Const GWL_STYLE = -16
Const ES_NUMBER = &H2000

Sub Main()

Dim wFileSystem As FileSystem
Dim wFileSystem_1 As FileSystem
Dim wFileSystem_2 As FileSystem
Dim wFile As File
Dim Fich As String
'-> procedure dans le cas de l'emulation
StartForEmulation

'-> on reccupere le repertoire de synchronisation sur le pocket
RepSynchro = GetIniValue("REPSYNCHRO")

'-> a l'ouverture on bute tout les fichier commande dont la taille est a zero
' Definition d'un objet "FileSystem" pour afficher les fichers d'un repertoire
Set wFileSystem = CreateObject("FileCtl.FileSystem")
' Definiton d'1 objet "File" pour la manipulation des fichiers (ecriture, lecture)
Set wFile = CreateObject("FileCtl.File")
' on parcourt les commandes et on recherche celles dont la taille est a zero
Fich = wFileSystem.Dir(App.Path & "\Navettes\*.*", fsAttrNormal)
Do While Fich <> ""
    If wFileSystem.FileLen(App.Path & "\Navettes\" & Fich) < 5 Then
        '-> on bute la commande trait�e (taille nulle)
        wFileSystem.Kill App.Path & "\Navettes\" & Fich
    End If
    '-> on pren le fichier suivant
    Fich = wFileSystem.Dir
Loop

'-> gestion des logs
'-> on regarde le fichier log du pocket et on supprime le fichier si il existe
If wFileSystem.Dir(App.Path & "\Log.txt", fsAttrNormal) <> "" Then
    wFileSystem.Kill App.Path & "\Log.txt"
End If

PutLog "D�but" & Now

' Afficher la feuille
'-> on regarde le code depot du pocket
If GetIniValue("DEPOT") = "" Then
    '-> on lance la saisie du code depot
    ParamDepot.Initialise
Else
    '-> on lance la visu des commandes
    ScanCodBar.Show
End If

    
End Sub

'-> Procedure permettant de ne saisir que des nombres dans une Textbox
Public Sub SetNumberBox(ByVal wTextBox As TextBox)

Dim s As Long
s = GetWindowLong(wTextBox.hwnd, GWL_STYLE)
s = s Or ES_NUMBER
Call SetWindowLong(wTextBox.hwnd, GWL_STYLE, s)

End Sub

'-> Procedure permettant de s�lectionner une ligne entiere
Public Sub LVFullRowSelect(ByVal wLV As ListViewCtrl)

Dim h As Long
Dim s As Long

' On r�cup�re le hWnd du ListView en lui donnant le focus et en
' Utilisant l'API GetFocus()
Call wLV.SetFocus
h = GetFocus()

' On r�cup�re les param�tres de style courant du ListView dans s
s = SendMessage(h, LVM_GETEXTENDEDLISTVIEWSTYLE, 0, 0)

'On ajoute (si cel� n'est pas d�j� le cas) le FullRowSelect(selection d'1 ligne entiere)
s = s Or LVS_EX_FULLROWSELECT
Rem On enregistre les nouveaux param�tres
Call SendMessage(h, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, s)

' On ajoute (si cel� n'est pas d�j� le cas) la grille
s = s Or LVS_EX_GRIDLINES
' On enregistre les nouveaux param�tres
Call SendMessage(h, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, s)

End Sub

Public Function GetIniValue(StrKey As String) As String
'--> cette fonction ramene une valeur du fichier ini
Dim wFileSystem As FileSystem
Dim wFile As File
Dim wLigne As String

'-> Par defaut rien
GetIniValue = ""
' Definition d'un objet "FileSystem" pour afficher les fichers d'un repertoire
Set wFileSystem = CreateObject("FileCtl.FileSystem")
' Definiton d'1 objet "File" pour la manipulation des fichiers (ecriture, lecture)
Set wFile = CreateObject("FileCtl.File")
'-> on ouvre le fichier ini pour avoir le repertoire de synchronisation

If wFileSystem.Dir(App.Path & "\ScanCod.ini", fsAttrNormal) <> "" Then
    ' Ouverture du fichier en lecture
    Call wFile.Open(App.Path & "\ScanCod.ini", fsModeInput, fsAccessRead)
    Do While Not wFile.EOF
        ' Recuperation de la 1ere ligne : ligne entete cdes
        wLigne = wFile.LineInputString
        '-> on recherche le strkey
        If Trim(Entry(1, wLigne, "=")) = Trim(StrKey) Then
            GetIniValue = Trim(Entry(2, wLigne, "="))
            Exit Do
        End If
    Loop
    '-> on ferme le fichier
    wFile.Close
End If

End Function


Public Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Integer
Dim PosEnCour As Integer
Dim i As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
End Function

Public Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Integer

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Integer
Dim i As Integer
Dim PosAnalyse As Integer

'-> Par defaut, renvoyer 1
NumEntries = 1

'-< Analyse du vecteur
PosAnalyse = 1
Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1

End Function

Public Function StrCDbl(StrVal As String) As Double
'--> cette fonction permet d'eviter les erreur
If IsNumeric(StrVal) Then
    StrCDbl = CDbl(StrVal)
Else
    StrCDbl = CDbl("0")
End If
End Function

Public Function StartForEmulation()
'--> cette fonction permet de charger les fichiers de test au demarrage pour l'emulateur
Dim wFileSystem As FileSystem
Dim wFile As File
Dim Rep As String
Dim sous_rep As String
Dim fichier As String

'-> si on ne trouve pas le fichier test c'est qu'on est sur l'emulateur
If GetIniValue("REPSYNCHRO") = "" Then
    MsgBox "Mode Emulation"
    ' Definition d'un objet "FileSystem" pour afficher les fichers d'un repertoire
    Set wFileSystem = CreateObject("FileCtl.FileSystem")
    ' Definiton d'1 objet "File" pour la manipulation des fichiers (ecriture, lecture)
    Set wFile = CreateObject("FileCtl.File")
    
    ' Creation des sous repertoires
    If wFileSystem.Dir(App.Path & "\Navettes", fsAttrDirectory) = "" Then
        Call wFileSystem.MkDir(App.Path & "\Navettes")
    End If
            
    '-> Creation du fichier ini
    Call wFile.Open(App.Path & "\ScanCod.ini", fsModeOutput, fsAccessWrite, fsLockReadWrite)
    'Ecriture dans le fichier
    Call wFile.LinePrint("REPSYNCHRO=\\dwi3351\dealgate\v6\dog\partageweb\xcspool\pha")
    Call wFile.LinePrint("DEPOT=001")
    ' Fermeteure du fichier
    Call wFile.Close
End If

End Function

Public Sub PutLog(StrLigne As String)
'-> cette fonction permet d'ecrire dans le fichier log
Dim wFileSystem As FileSystem
Dim wFile As File

' Definition d'1 objet "FileSystem" pour affichage fichier + dossier
Set wFileSystem = CreateObject("FileCtl.FileSystem")
' Definiton d'1 objet "File" pour la manipulation des fichiers (ecriture, lecture)
Set wFile = CreateObject("FileCtl.File")

Call wFile.Open(App.Path & "\Log.txt", fsModeAppend, fsAccessWrite, fsLockReadWrite)
'-> on ecrit la ligne
wFile.LinePrint StrLigne
'-> on ferme le fichier
Call wFile.Close

End Sub

Public Sub FullScreen(ByVal frmHwnd As Long, ByVal makeFull As Boolean)
    Dim lret


    If Not makeFull Then
        'ShowSIP frmHwnd, True
        'ShowStart frmHwnd, True
        'ShowTaskbar frmHwnd, True
        lret = FindWindow("menu_worker", "")


        If lret <> 0 Then 'window found
            ShowWindow lret, SW_SHOWNORMAL
        End If
        lret = SetForegroundWindow(frmHwnd)
        lret = MoveWindow(frmHwnd, 0, HHTASKBARHEIGHT, _
        GetSystemMetrics(SM_CXSCREEN), _
        GetSystemMetrics(SM_CYSCREEN), True)
    Else
        ShowSIP frmHwnd, False
        ShowStart frmHwnd, False
        ShowTaskbar frmHwnd, False
        'show form full screen
        lret = FindWindow("menu_worker", "")


        If lret <> 0 Then 'window found
            ShowWindow lret, SW_HIDE
        End If
        lret = SetForegroundWindow(frmHwnd)
        lret = MoveWindow(frmHwnd, 0, 0, _
        GetSystemMetrics(SM_CXSCREEN), _
        GetSystemMetrics(SM_CYSCREEN) + HHTASKBARHEIGHT, 0)
    End If
End Sub

Public Sub ShowSIP(ByVal frmHwnd As Long, ByVal ShowIt As Boolean)
    Dim lret
    lret = SetForegroundWindow(frmHwnd)


    If Not ShowIt Then
        lret = SHFullScreen(frmHwnd, SHFS_HIDESIPBUTTON)
    Else
        lret = SHFullScreen(frmHwnd, SHFS_SHOWSIPBUTTON)
    End If
End Sub

Public Sub ShowStart(ByVal frmHwnd As Long, ByVal ShowIt As Boolean)
    Dim lret
    lret = SetForegroundWindow(frmHwnd)


    If Not ShowIt Then
        lret = SHFullScreen(frmHwnd, SHFS_HIDESTARTICON)
    Else
        lret = SHFullScreen(frmHwnd, SHFS_SHOWSTARTICON)
    End If
End Sub

Public Sub ShowTaskbar(ByVal frmHwnd As Long, ByVal ShowIt As Boolean)
    Dim lret
    lret = SetForegroundWindow(frmHwnd)


    If Not ShowIt Then
        lret = SHFullScreen(frmHwnd, SHFS_HIDETASKBAR)
    Else
        lret = SHFullScreen(frmHwnd, SHFS_SHOWTASKBAR)
    End If
End Sub

Public Function formatDateToString() As String
    
    Dim strMth As String, strDay As String, strHr As String
    Dim strMin As String, strSec As String
    Dim Datetemp As Date
    
    Datetemp = Now
    
    If Len(Month(Datetemp)) = 1 Then
    strMth = "0" & Month(Datetemp)
    Else
    strMth = Month(Datetemp)
    End If
    
    If Len(Day(Datetemp)) = 1 Then
    strDay = "0" & Day(Datetemp)
    Else
    strDay = Day(Datetemp)
    End If
    
    If Len(Hour(FormatDateTime(Datetemp, vbLongTime))) = 1 Then
    strHr = "0" & Hour(FormatDateTime(Datetemp, vbLongTime))
    Else
    strHr = Hour(FormatDateTime(Datetemp, vbLongTime))
    End If
    
    If Len(Minute(FormatDateTime(Datetemp, vbLongTime))) = 1 Then
    strMin = "0" & Minute(FormatDateTime(Datetemp, vbLongTime))
    Else
    strMin = Minute(FormatDateTime(Datetemp, vbLongTime))
    End If
    
    If Len(Second(FormatDateTime(Datetemp, vbLongTime))) = 1 Then
    strSec = "0" & Second(FormatDateTime(Datetemp, vbLongTime))
    Else
    strSec = Second(FormatDateTime(Datetemp, vbLongTime))
    End If
    
    Dim strTemp As String
    
    strTemp = Year(Datetemp) & strMth & strDay & strHr & strMin & strSec
    formatDateToString = strTemp
End Function
