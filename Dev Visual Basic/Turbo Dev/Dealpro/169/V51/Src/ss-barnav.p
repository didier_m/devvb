/************************************************************************************************************
&DOC-DEFINE Definition des variables necessaires
&END
************************************************************************************************************/

/*---> Outil */
{d-gfic.f}

DEF VAR    z-bareme      AS  CHAR .
DEF BUFFER temp-d-tables FOR d-tables .

/*---> File d-tables              */
&Scoped-define File d-tables

/*---> VueEcr Vue associee */
Gfic-VueEcr="ss-barnav" .

/*---> Naviga-File */
&Scoped-define Naviga-File WHERE d-tables.ident     = glo-ident                     ~
                             AND d-tables.progiciel = "SOPHIE"                      ~
                             AND d-tables.motcle    = "BARNAV"                      ~
                             AND d-tables.typtab    = ""                            ~
                             AND d-tables.prefix    = deal-get-screen ( "bareme-brit" )  ~
                             
/*---> Find-File Acces a la table */
&Scoped-Define Find-File d-tables.ident     = glo-ident                    ~
                     AND d-tables.progiciel = "SOPHIE"                     ~
                     AND d-tables.motcle    = "BARNAV"                     ~
                     AND d-tables.typtab    = ""                           ~
                     AND d-tables.prefix    = deal-get-screen ( "bareme-brit" ) ~
                     AND d-tables.codtab    = deal-get-screen ( "navire" ) ~

/*---> Assign-File Assignation de zones sur le create */
&Scoped-Define Assign-File ASSIGN d-tables.ident     = glo-ident  ~
                                  d-tables.progiciel = "SOPHIE"   ~
                                  d-tables.motcle    = "BARNAV"   ~
                                  d-tables.typtab    = ""         ~
                                  d-tables.prefix    = z-bareme .

/************************************************************************************************************
&DOC-GFIC Moteur du traitement du fichier
&END
************************************************************************************************************/
{d-gfic.i}

/************************************************************************************************************
&DOC-WAIT-FOR Affichage et attente de saisie 
&END
************************************************************************************************************/
If not this-procedure:persistent
then do: 
  DEAL-INPUT ( "" , "update" , "" ) .
  WAIT-FOR CLOSE OF THIS-PROCEDURE.
End.

/************************************************************************************************************
&PROC-INITKEY Initialisation de la clef ligne
&END
************************************************************************************************************/
PROCEDURE INITKEY :
Define input parameter i-Motcle as char no-undo.

END PROCEDURE. /* INITKEY */

/************************************************************************************************************
&PROC-LOCAL-TRIGGER Triggers specifiques au programme 
  - P1 : Evenement � tester 
  - P2 : Le mot clef du champ qui d�clanche le trigger 
  - P3 : Param�tre associe au champ
&END
************************************************************************************************************/
PROCEDURE LOCAL-TRIGGER :
  Define input parameter i-Event  as char no-undo.
  Define input parameter i-Motcle as char no-undo.
  Define input parameter i-param  as char no-undo.

  Case i-motcle :

    When "categorie"
    Then do:
    
      IF int(deal-get-screen ( "categorie" )) < 1 
      OR int(deal-get-screen ( "categorie" )) > 20 
      THEN RETURN "error" .

    End. /* UPDATE */

  End CASE. /* Case i-Motcle */

  Return "" .

END PROCEDURE. /* LOCAL-TRIGGER */

/************************************************************************************************************
&PROC-Retour-Parent Apres Execution d'un GFIC avec un attribut Exchange
                    i-motcle  = Nom du bouton 
&END
************************************************************************************************************/
PROCEDURE Retour-parent :
Define Input parameter i-motcle as char no-undo.
  
  /* rafraichissement du dataview apres la multi-selection */
  deal-event ( "dataview" , "_refresh" ) .

  Return "" .

END PROCEDURE. /* Retour-parent */
