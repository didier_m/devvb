/* 169-FIC 15 Integration des budgets analytiques par saisie : Pb sur mouchard <BUG AIR 27/10/00> */
/* 169-FIC 10 Integration budget par ASCII : gerer les messages avec MESSPROG <BUG AIR 07/09/00> */
/* 169-FIC 7 Creation d'un prog d'import de BUDANA par la saisie des budgets <NEW air 25/08/00> */
/*=======================================================*/
/* x169-bua.p Integration budgets analytiques BRITTANY   */
/*=======================================================*/


{connect.i  }
{budget-a.i }
{budget-a.f }
{varlect.i  }

{chx-opt.f }

{acc-mess.i "x169-bua" }
if not available messprog then do :
    MESSAGE "Pas de Messprog x169-bua !" 
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
    return.
end.


define stream lect-asc .   /* canal lecture ascii */
define stream erreur .     /* canal mouchard */

define var lig-lue      as char .
define var lig-don      as char extent 50.
define var int-ascii    as char format "x(75)" .
define var a            as int.
define var b            as int.
define var bua-c-segana as char .
define var bua-nblig    as int .
define var bua-lig      as int .
Define Var mouch        As Char.
Define Var err-don      As Char format "x(100)".
define var lop-chx      as char .
define var err-fic      as logical init no.

form
     int-ascii skip
     with frame fr-asc row 10 centered no-label Overlay {v6Frame.i} 
     with title messprog.libel1[1] .


repeat:
    message color normal messpro.libel1[1] .
    message color normal messpro.libel1[8] .
    update int-ascii go-on ( {f4.i} ) with frame fr-asc .

lop-chx = messprog.libel1[2] .

assign  chx-def    = 1 .

        {chx-opt.i "3" "lop-chx" "21" "12" "40" "no-box" "fr-tnt-chx1"}

    if keyfunction ( lastkey) = "end-error" or chx-trait = 2
    then do :
        hide frame fr-asc no-pause.
        return.
    end.

    if search( int-ascii) = ? then DO :
    leave.
    message messprog.libel1[3] . bell. bell.
    readkey pause 10.  return .
    END.

    
    /* RAZ des budgets */
    if chx-trait = 3 
    then do :
    for each budana where budana.execpt = bua-execpt   and
                               budana.codaxe = bua-axe      and
                               budana.type   = bua-scene   
                         
                               exclusive-lock :
                    
                display budana.c-segana format "x(60)"  
                with frame fr-raz row 10 10 down  centered
                overlay no-label title messprog.libel1[4] .
                
                pause 0 .
                              
                Budana.mt-cpt = 0 .
         End .


         hide frame fr-raz no-pause .
         hide frame fr-asc no-pause .
         RETURN.
    end .


LEAVE.
end.
hide frame fr-asc no-pause.


/* ouverture du canal du fichier a integrer */
input   stream lect-asc from value( int-ascii ).
        
/* ouverture du canal du fichier mouchard */
mouch = "err-bud." + operat.
Output Stream Erreur To Value ( mouch ).


general:
repeat :

       assign lig-lue = "" 
              err-fic = no
              bua-nblig = bua-nblig + 1
              .


       import  stream lect-asc  lig-lue.
       
       Do a = 1 To Num-entries ( lig-lue , ";" ) :
            lig-don[a] = Entry ( a , lig-lue , ";" ).
       End.
       if lig-don[1] = "" then next.

       /* en fonction de la premiere zone, on integre sur une vue differente */
       If lig-don[1] = "VUE-1" 
       Then Do :
           /* lecture societe */
           lect-tab = lig-don [3] .
           
           {l-soc.i } 
           if lect-tab = "*" 
           then do :
              err-don = messprog.mes[1] + lig-don[2] + lig-don[3] + messprog.libel1[5] + string (bua-nblig).
              err-fic = yes.
              Put Stream erreur err-don skip.
           end .
    
           /* lecture etablissement */
           lect-tab = lig-don [4] .
           {l-eta.i }
           
           if lect-tab = "*" 
           then do :
              err-don = messprog.mes[2] + lig-don[2] + lig-don[4] + messprog.libel1[5] + string(bua-nblig).
              err-fic = yes.
              Put Stream erreur err-don skip.
           end .
           
           /* lecture segment section */
           lect-segana = "01" .
           lect-ana = string( lig-don [5] , "x(3)" ).
    
           {l-ana.i}
           if not available tables
           then do :
              err-don = messprog.mes[3] + lig-don[2] + lig-don[5] + messprog.libel1[5] + string (bua-nblig).
              err-fic = yes.
              Put Stream erreur err-don skip.
           end.
       
           /* lecture segment nature */
           lect-segana = "02" .
           lect-ana = string( lig-don [6] , "x(4)" ).
    
           {l-ana.i}
           if not available tables
           then do :
              err-don = messprog.mes[4] + lig-don[2] + lig-don[6] + messprog.libel1[5] + string (bua-nblig).
              err-fic = yes.
              Put Stream erreur err-don skip.
           end.

           if not err-fic  then do :
               bua-mt-cpt = 0 .
    
               repeat a = 1  to 12 :
                    bua-mt-cpt [a] = decimal ( lig-don[a + 8] )  .
               end.
        
               bua-c-segana = "".
        
               bua-c-segana = string ( lig-don [3] , "X(4)" )  + /* societe */
                              string ( lig-don [4] , "X(4)" ) + /* ETB      */
                              string ( lig-don [5] , "X(3)" ) + /* Section  */
                              string ( lig-don [6] , "X(4)" ) . /* Nature   */
               
               If bua-axe = ""   Then bua-axe   = Substring ( lig-don[2] , 1 , 3 ).
               If bua-scene = "" Then bua-scene = Substring ( lig-don[2] , 5 , 3 ).

                /* budget mensuel */
                find budana where budana.execpt   = bua-execpt   and
                                  budana.codaxe   = bua-axe      and
                                  budana.type     = bua-scene    and
                                  budana.c-segana = bua-c-segana
                                  exclusive-lock no-wait no-error.
        
                if not available budana
                then do:
                        if locked budana then next.
        
                     assign
                        bua-percpt-mini = "001"
                        bua-percpt-maxi = "012" .
        
                        create budana .
                     assign
                        budana.execpt   = bua-execpt
                        budana.codaxe   = bua-axe
                        budana.type     = bua-scene
                        budana.c-segana = bua-c-segana.
                end.
           
           
                /* maj budana */
                repeat b = 1 to 12 :
                     budana.mt-cpt[ b ] = bua-mt-cpt[ b ] .
                end.
        
                budana.sens = "1" .
                
                bua-lig = bua-lig + 1. /* compteur des lignes integrees */
                
           end. /* if not err-fic  */
       
       End.
       
       Else If lig-don[1] = "VUE-2" 
       Then Do :
           /* lecture societe */
           lect-tab = lig-don [3] .
           
           {l-soc.i } 
           if lect-tab = "*" 
           then do :
              err-don = messprog.mes[1] + lig-don[2] + lig-don[3] + messprog.libel1[5] + string (bua-nblig).
              err-fic = yes.
              Put Stream erreur err-don skip.
           end .
    
           /* lecture etablissement */
           lect-tab = lig-don [4] .
           {l-eta.i }
           
           if lect-tab = "*" 
           then do :
              err-don = messprog.mes[2] + lig-don[2] + lig-don[4] + messprog.libel1[5] + string (bua-nblig).
              err-fic = yes.
              Put Stream erreur err-don skip.
           end .
           
           /* lecture segment signataire */
           lect-segana = "03" .
           lect-ana = string( lig-don [5] , "x(3)" ).
    
           {l-ana.i}
           if not available tables
           then do :
              err-don = messprog.mes[5] + lig-don[2] + lig-don[5] + messprog.libel1[5] + string (bua-nblig).
              err-fic = yes.
              Put Stream erreur err-don skip.
           end.
       
           /* lecture segment projet */
           lect-segana = "05" .
           lect-ana = string( lig-don [6] , "x(3)" ).
    
           {l-ana.i}
           if not available tables
           then do :
              err-don = messprog.mes[6] + lig-don[2] + lig-don[6] + messprog.libel1[5] + string (bua-nblig).
              err-fic = yes.
              Put Stream erreur err-don skip.
           end.

           /* lecture segment Section */
           lect-segana = "01" .
           lect-ana = string( lig-don [7] , "x(3)" ).
    
           {l-ana.i}
           if not available tables
           then do :
              err-don = messprog.mes[3] + lig-don[2] + lig-don[7] + messprog.libel1[5] + string (bua-nblig).
              err-fic = yes.
              Put Stream erreur err-don skip.
           end.
       
           /* lecture segment Secteur */
           lect-segana = "04" .
           lect-ana = string( lig-don [8] , "x(6)" ).
    
           {l-ana.i}
           if not available tables
           then do :
              err-don = messprog.mes[7] + lig-don[2] + lig-don[8] + messprog.libel1[5] + string (bua-nblig).
              err-fic = yes.
              Put Stream erreur err-don skip.
           end.
           
           if not err-fic then do :
               bua-mt-cpt = 0 .
    
               repeat a = 1  to 12 :
                    bua-mt-cpt [a] = decimal ( lig-don[a + 8] )  .
               end.
        
               bua-c-segana = "".
        
               bua-c-segana = string ( lig-don [3] , "X(4)" ) + /* societe    */
                              string ( lig-don [4] , "X(4)" ) + /* ETB        */
                              string ( lig-don [5] , "X(3)" ) + /* Signataire */
                              string ( lig-don [6] , "X(3)" ) + /* Projet     */
                              string ( lig-don [7] , "X(3)" ) + /* Section    */
                              string ( lig-don [8] , "X(6)" )   /* Secteur    */
                              .
               
               If bua-axe = ""   Then bua-axe   = Substring ( lig-don[2] , 1 , 3 ).
               If bua-scene = "" Then bua-scene = Substring ( lig-don[2] , 5 , 3 ).
                
                
                /* budget mensuel */
                find budana where budana.execpt   = bua-execpt   and
                                  budana.codaxe   = bua-axe      and
                                  budana.type     = bua-scene    and
                                  budana.c-segana = bua-c-segana
                                  exclusive-lock no-wait no-error.
        
                if not available budana
                then do:
                        if locked budana then next.
        
                     assign
                        bua-percpt-mini = "001"
                        bua-percpt-maxi = "012" .
        
                        create budana .
                     assign
                        budana.execpt   = bua-execpt
                        budana.codaxe   = bua-axe
                        budana.type     = bua-scene
                        budana.c-segana = bua-c-segana.
                end.
           
           
                /* maj budana */
                repeat b = 1 to 12 :
                     budana.mt-cpt[ b ] = bua-mt-cpt[ b ] .
                end.
        
                budana.sens = "1" .
                
                bua-lig = bua-lig + 1. /* compteur des lignes integrees */
            
            end. /* if not err-fic */
       
       End.
       
       
end.

err-don = messprog.libel1[6] + String ( bua-nblig ).
Put Stream erreur err-don skip.

err-don = messprog.libel1[7] + String ( bua-lig ).
Put Stream erreur err-don skip.

output Stream erreur close .
input stream lect-asc close.

hide frame fr-asc no-pause.

run bat-imp.p ( mouch ) .
