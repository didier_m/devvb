/* 169-FIC 20 Ajouter une zone specif dans les auxiliaires et test en revalorisation <EVO isa 14/06/01> */
/* 169-FIC 1 Gestion de la fiche specifique auxiliaire <NEW mu 18/08/00> */
/* LUCIE-FIC 520 Ajout de la gestion des auxiliaires depuis LUCIE/ELODIE pour cli.169 <EVO bp 02/08/00> */
/* x169-tis.p */ 

{connect.i}
{s-parsai.i}
{x169-tie.i}
{gesaux.i}
{varlect.i}
{acc-mess.i "x169-tie"}

def var aaaa like auxili.cee no-undo .
define var sov-regs as char no-undo. 
define var sov-typaux as char no-undo. 
define var sov-auxili as char no-undo. 
define var var-libel as char format "x(60)". 

assign sov-typaux  =  typaux 
       sov-auxili  =  auxili
       . 
 
sov-regs = regs-soc. 

/* chargement des zones */ 
case parsai-cle : 
  when "telephone"     then  parsai-affich  = string(auxspe.libel1[1], "x(30)" ). 
  
  when "fax"           then  parsai-affich  = string(auxspe.libel1[2], "x(30)" ). 
  
    when "email"       then  parsai-affich  = string(auxspe.libel1[3], "x(30)" ). 
    when "revalo"      then  parsai-affich  = string(auxspe.nombre[1], "9"     ). 
 
  when "intcee"     
  then do : 
       find auxili where auxili.codsoc = auxspe.codsoc
                     and auxili.typaux = spe-typaux
                     and auxili.codaux = spe-codaux
            no-lock no-error .
       if available auxili then aaaa = auxili.cee.
       
       if auxspe.libel1[5] = "" then auxspe.libel1[5] = "N".
       parsai-affich  = string(auxspe.libel1[5], "x(1)") + " " + aaaa.  
  end.

  WHEN "codifbdf"   then  parsai-affich  = string(auxspe.libel1[7], "x(30)"). 
  
  when "cdebud"     
  then do : 
       assign lect-lib = "" 
              parsai-affich   = string( auxspe.libel1[8] , "x(10)" ) 
              . 

       find tables where tables.codsoc = "" 
                     and tables.etabli = "" 
                     and tables.typtab = "169" 
                     and tables.prefix = "BUDGET" 
                     and tables.codtab = auxspe.libel1[8] 
            no-lock no-error . 
       if available tables then lect-lib = tables.libel1[mem-langue] . 
       else message color normal messprog.mes[5] .
       parsai-affich = parsai-affich + " " + lect-lib . 
  end.

  when "cdepost"     
  then do : 
       assign lect-lib = "" 
              parsai-affich   = string( auxspe.libel1[9] , "x(8)" ) 
              .

      /* find tables where tables.codsoc = "" 
                     and tables.etabli = "" 
                     and tables.typtab = "169" 
                     and tables.prefix = "CODEPOSTAL" 
                     and tables.codtab = auxspe.libel1[9] 
            no-lock no-error . 
       if available tables then lect-lib = tables.libel1[mem-langue] . 
       else message color normal messprog.mes[6] . */

       parsai-affich = parsai-affich + " " + lect-lib . 
  end.
       
end case .
 
app-nomzone = parsai-cle .
 
if parsai-type = "c"  and zone-charg[7] <> "VISU" 
then case parsai-cle : 
    
    when "telephone"
    then do :
         form   auxspe.libel1[1] format "x(30)"
                with frame fr-a01
                row ( frame-line(fr-parsai) + frame-row(fr-parsai) )
                col ( frame-col(fr-paraff ) + 1 ) no-box  overlay {v6frame.i} no-label.
         display auxspe.libel1[1] with frame fr-a01 .
         repeat with frame fr-a01 :
             message color normal messprog.mes[4] .
             update auxspe.libel1[1]   go-on( {f4.i} {f1.i} {fb.i} {fh.i} ) .
             if keyfunction( lastkey) = "end-error" then undo , leave .
             leave .
         end.
         parsai-affich =  string( auxspe.libel1[1], "x(30)" ).
    end.

    when "fax"
    then do :
         form   auxspe.libel1[2] format "x(30)"
                with frame fr-a02
                row ( frame-line(fr-parsai) + frame-row(fr-parsai) )
                col ( frame-col(fr-paraff ) + 1 ) no-box  overlay {v6frame.i} no-label.
         display auxspe.libel1[2]     with frame fr-a02 .
         repeat with frame fr-a02 :
             message color normal messprog.mes[4] .
             update auxspe.libel1[2]   go-on( {f4.i} {f1.i} {fb.i} {fh.i} ) .
             if keyfunction( lastkey) = "end-error" then undo , leave .
             leave .
         end.
         parsai-affich =  string( auxspe.libel1[2], "x(30)" ).
    end.
    
    when "email"
    then do :
         form   auxspe.libel1[3] 
                with frame fr-a03
                row ( frame-line(fr-parsai) + frame-row(fr-parsai) )
                col ( frame-col(fr-paraff ) + 1 ) no-box  overlay {v6frame.i} no-label.
         display auxspe.libel1[3]     with frame fr-a03 .
         repeat with frame fr-a03 :
             message color normal messprog.mes[4] .
             update auxspe.libel1[3]   go-on( {f4.i} {f1.i} {fb.i} {fh.i} ) .
             if keyfunction( lastkey) = "end-error" then undo , leave .
             leave .
         end.
         parsai-affich =  string(auxspe.libel1[3], "x(30)" ).
    end.
    
    when "intcee"
    then do :
         form   auxspe.libel1[5] format "x(1)"      
                with frame fr-a04
                row ( frame-line(fr-parsai) + frame-row(fr-parsai) )
                col ( frame-col(fr-paraff ) + 1 ) no-box  overlay {v6frame.i} no-label.
         display auxspe.libel1[5]     with frame fr-a04 .
         repeat with frame fr-a04 :
             message color normal messprog.mes[3] .
             update auxspe.libel1[5]   go-on( {f4.i} {f1.i} {fb.i} {fh.i} ) .
             if keyfunction( lastkey) = "end-error" then undo , leave .
             leave .
         end.
         if auxspe.libel1[5] = "O" or auxspe.libel1[5] = "N"
         then parsai-affich =  string(auxspe.libel1[5], "x(1)") + " " + aaaa.
         else do:
              auxspe.libel1[5] = "O".
              parsai-affich =  string(auxspe.libel1[5], "x(1)") + " " + aaaa.
              retry.
         end.
    end.

    when "codifbdf"
    then do :
         form   auxspe.libel1[7] format "x(30)"
                with frame fr-a05
                row ( frame-line(fr-parsai) + frame-row(fr-parsai) )
                col ( frame-col(fr-paraff ) + 1 ) no-box  overlay {v6frame.i} no-label.
         display auxspe.libel1[7]     with frame fr-a05 .
         repeat with frame fr-a05 :
             message color normal messprog.mes[4] .
             update auxspe.libel1[7]   go-on( {f4.i} {f1.i} {fb.i} {fh.i} ) .
             if keyfunction( lastkey) = "end-error" then undo , leave .
             leave .
         end.
         parsai-affich =  string(auxspe.libel1[7], "x(30)").
    end.

    when "cdebud" 
    then do :
         form   auxspe.libel1[8] format "x(10)" 
                lect-lib 
                with frame fr-a06
                row ( frame-line(fr-parsai) + frame-row(fr-parsai) )
                col ( frame-col(fr-paraff ) + 1 ) no-box  overlay {v6frame.i} no-label.
         display auxspe.libel1[8]  lect-lib    with frame fr-a06 .
         repeat with frame fr-a06 :
             message color normal messprog.mes[4] .
             update auxspe.libel1[8]   go-on( {f4.i} {f1.i} {fb.i} {fh.i} {fr.i}) .
             if keyfunction( lastkey) = "end-error" then undo , leave .
             if keyfunction( lastkey) = "FIND" 
             then do: 
                  assign codsoc = ""  
                         etabli = "" 
                         typtab = "169" 
                         prefix = "BUDGET" 
                         libel  = auxspe.libel1[8] 
                         . 
                         {run.i tabrech.p } 
                         if codtab = "$rien$" then undo , retry . 
                         auxspe.libel1[8] = codtab . 
             end. 
             if auxspe.libel1[8] <> "" 
             then do: 
                  find tables where tables.codsoc = "" 
                                AND tables.etabli = "" 
                                and tables.typtab = "169" 
                                and tables.prefix = "BUDGET" 
                                AND tables.codtab = auxspe.libel1[8]
                       no-lock no-error . 
                  if available tables then lect-lib = tables.libel1[mem-langue] . 
                  else do:
                       message color normal messprog.mes[5] .
                       undo , retry .
                  end. 
             end.
             ELSE lect-lib = "".
             leave .
         end.
         parsai-affich = string( auxspe.libel1[8] , "x(10)" ) + " " + lect-lib.
    end.
    
    when "cdepost" 
    then do :
         form   auxspe.libel1[9] format "x(8)" 
                lect-lib 
                with frame fr-a07
                row ( frame-line(fr-parsai) + frame-row(fr-parsai) )
                col ( frame-col(fr-paraff ) + 1 ) no-box  overlay {v6frame.i} no-label.
         display auxspe.libel1[9]  lect-lib    with frame fr-a07 .
         repeat with frame fr-a07 :
             message color normal messprog.mes[4] .
             update auxspe.libel1[9]   go-on( {f4.i} {f1.i} {fb.i} {fh.i} {fr.i}) .
             if keyfunction( lastkey) = "end-error" then undo , leave .
             if keyfunction( lastkey) = "FIND" 
             then do: 
                  assign codsoc = ""  
                         etabli = "" 
                         typtab = "169" 
                         prefix = "CODEPOSTAL" 
                         libel  = auxspe.libel1[9] 
                         . 
                         {run.i tabrech.p } 
                         if codtab = "$rien$" then undo , retry . 
                         auxspe.libel1[9] = codtab . 
             end. 
            /* if auxspe.libel1[9] <> "" 
             then do: 
                  find tables where tables.codsoc = "" 
                                AND tables.etabli = "" 
                                and tables.typtab = "169" 
                                and tables.prefix = "CODEPOSTAL" 
                                AND tables.codtab = auxspe.libel1[9]
                       no-lock no-error . 
                  if available tables then lect-lib = tables.libel1[mem-langue] . 
                  else next.
			do:
                       message color normal messprog.mes[6] .
                       undo , retry .
                  end. 
             end. */
             ELSE lect-lib = "".
             leave .
         end.
         parsai-affich = string( auxspe.libel1[9] , "x(8)" ) + " " + lect-lib.
    end.
    when "revalo"
    then do :
       if spe-typaux <> "FOU" 
       then do :
         form   auxspe.nombre[1] format "9"
                with frame fr-a08
                row ( frame-line(fr-parsai) + frame-row(fr-parsai) )
                col ( frame-col(fr-paraff ) + 1 ) no-box  overlay {v6frame.i} no-label.
         display auxspe.nombre[1] with frame fr-a08 .
         repeat with frame fr-a08 :
             message color normal messprog.mes[4] .
             message color normal messprog.mes[10] . 
             update auxspe.nombre[1]   go-on( {f4.i} {f1.i} {fb.i} {fh.i} ) .
             if keyfunction( lastkey) = "end-error" then undo , leave .
             if auxspe.nombre[1] > 1 then undo , retry . 
             leave .
         end.
         parsai-affich =  string( auxspe.nombre[1], "9" ).
       end.
    end.
    
         
end case . 
 
assign typaux = sov-typaux 
       auxili = sov-auxili 
       regs-soc = sov-regs 
       . 
 
hide frame fr-a01  no-pause.
hide frame fr-a02  no-pause.
hide frame fr-a03  no-pause.
hide frame fr-a04  no-pause.
hide frame fr-a05  no-pause.
hide frame fr-a06  no-pause.
hide frame fr-a07  no-pause.
hide frame fr-a08  no-pause.
       


