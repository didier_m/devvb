/* LUCBRI-FIC 10 diverses modifs effectu�es chez le  client 169 a la mise en place <EVO bp 18/09/00> */
/* LUCBRI-FIC 7 en creation sous elodie retourner le libelle du pays dans la zone <EVO BP 25/08/00> */
/* 169-FIC 1 Gestion de la fiche specifique auxiliaire <NEW mu 18/08/00> */
/* LUCIE-FIC 520 Ajout de la gestion des auxiliaires depuis LUCIE/ELODIE pour cli.169 <EVO bp 02/08/00> */
/* x169-ti1.p */

{ connect.i }
{ gesaux.i  }
{ varlect.i }

define buffer auxspe for tabdom.
def var i as int .
define var sav-zone-charg as char extent 9 no-undo .
define var aa as int no-undo .

/*--------------------- acces a messprog --------------------------*/
{acc-mess.i "x169-tie" }
if not available messprog 
then do :
     message "MESSAGE x169-tie INEXISTANT dans MESSPROG 2 "  view-as alert-box .
     return .
end.

regs-fileacc = "AUXILI" + gau-typaux.
{ regs-rec.i }

/* cas de la suppression */
if aux-delete
then do:
     find auxspe where auxspe.codsoc = regs-soc   and
                       auxspe.etabli = "AUX"      and
                       auxspe.typtab = "169"      and
                       auxspe.prefix = gau-typaux and
                       auxspe.codtab = gau-codaux
                       use-index primaire 
          exclusive-lock no-wait no-error .
     if available auxspe 
     then do : 
          find auxapr where auxapr.codsoc = auxspe.codsoc         
                        and auxapr.typaux = auxspe.prefix         
                        and auxapr.codaux = auxspe.codtab
                        use-index auxapr-1 
              exclusive-lock no-wait no-error .
          if available auxapr then delete auxapr .
          delete auxspe .
     end.
     return .
end.

/* creation et modif de auxspe */
find auxspe where auxspe.codsoc = regs-soc   and
                  auxspe.etabli = "AUX"      and
                  auxspe.typtab = "169"      and
                  auxspe.prefix = gau-typaux and
                  auxspe.codtab = gau-codaux
                  use-index primaire 
     exclusive-lock no-wait no-error .
if not available auxspe 
then do :
     create auxspe .
     assign auxspe.codsoc = regs-soc
            auxspe.etabli = "AUX"
            auxspe.typtab = "169"
            auxspe.prefix = gau-typaux
            auxspe.codtab = gau-codaux
            parsai-creation = yes .
     { majmoucb.i auxspe }
end.
find auxapr where auxapr.codsoc = regs-soc         
              and auxapr.typaux = gau-typaux         
              and auxapr.codaux = gau-codaux
              use-index auxapr-1
     exclusive-lock no-error .
if not available auxapr 
then do : 
     find auxili where auxili.codsoc = regs-soc 
                   and auxili.typaux = gau-typaux
                   and auxili.codaux = gau-codaux
                   use-index tiers
          no-lock no-error .
     if available auxili
     then do :
          
        IF gau-typaux = "fou" 
        THEN DO:
            create auxapr . 
            assign auxapr.codsoc = regs-soc        
                   auxapr.typaux = auxili.typaux         
                   auxapr.codaux = auxili.codaux
                   auxapr.libabr = auxili.libabr 
                   auxapr.typaux-fac = auxili.typaux         
                   auxapr.codaux-fac = auxili.codaux
                   .
            do i = 1 to 5 :
                auxapr.adres[i] = auxili.adres[i] .
            end.
            FIND tables WHERE tables.codsoc = ""
                        AND   tables.typtab = "pay"
                        AND   tables.prefix = "code-pays"
                        AND   tables.codtab = auxili.pays
                        NO-LOCK NO-ERROR.
            IF AVAILABLE tables THEN auxapr.activi[ 10 ] = tables.libel1[ mem-langue] .
               
            FIND FIRST TABDOM where tabdom.codsoc = regs-soc
                              AND    tabdom.etabli = ""
                              and   tabdom.typtab = auxapr.typaux-fac    
                              and   tabdom.prefix = auxapr.codaux-fac
                              no-lock  no-error .

            if available tabdom
            then do :
                auxapr.modreg = tabdom.libel [7].

                FOR FIRST TABGES where tabges.codsoc = ""
                                 and   tabges.etabli = ""
                                 and   tabges.typtab = "ECH"
                                 and   tabges.prefix = "CODECH"
                                 and   tabges.libel2 [1] = tabdom.libel [8]
                                 no-lock :

                    auxapr.codech = tabges.codtab .
                end.
            end. 

      /* Donnees complementaire */
            FIND TABDOM where tabdom.codsoc = regs-soc   and
                              tabdom.etabli = "AUX"      and
                              tabdom.typtab = "TAT"      and
                              tabdom.prefix = auxapr.typaux and
                              tabdom.codtab = auxapr.codaux
                              no-lock no-error .
            if available tabdom 
            then assign auxapr.teleph-respon = tabdom.libel1 [1]
                        auxapr.fax-respon    = tabdom.libel1 [2]
                        auxapr.alpha-5       = tabdom.libel1 [3]
                        auxapr.nom-respon    = tabdom.libel1 [4]
                        .
           { majmoucb.i auxapr }
        END.
     end.
end.

{ majmoucb.i auxapr }
{ majmoucb.i auxspe }

find auxili where auxili.codsoc = regs-soc 
                   and auxili.typaux = gau-typaux
                   and auxili.codaux = gau-codaux
                   use-index tiers
          no-lock no-error.
if available auxili and auxili.typaux = "FOU"
then do :

    find auxapr where auxapr.codsoc = auxspe.codsoc         
/*                  and auxapr.typaux = auxspe.prefix */        
                  and auxapr.typaux = "FOU"         
                  and auxapr.codaux = auxspe.codtab
                  use-index auxapr-1
                  exclusive-lock no-error .
           
    if not available auxapr
    then do :      
        message messprog.libel2 [1] view-as alert-box 
                QUESTION BUTTONS YES-NO
                TITLE "" 
                UPDATE choice2 AS LOGICAL.
        
        CASE choice2:
            WHEN TRUE
            THEN DO:
                do aa = 1 to 9 :
                    sav-zone-charg [aa]  = zone-charg [aa].
                end.
                ASSIGN zone-charg       = ""
                       zone-charg [1]   = auxili.codaux
                .
                run auxi-000.p .

                do aa = 1 to 9 :
                    zone-charg [aa]  = sav-zone-charg [aa].
                end.
            END.
            OTHERWISE .
        END CASE.
    end .
    else do :
        message messprog.libel2 [2] view-as alert-box 
                QUESTION BUTTONS YES-NO
                TITLE "" 
                UPDATE choice AS LOGICAL.
        
        CASE choice:
            WHEN TRUE
            THEN DO:
                auxapr.libabr = auxili.libabr .
                do i = 1 to 5 :
                    auxapr.adres[i] = auxili.adres[i] .
                end.
                FIND tables WHERE tables.codsoc = ""
                            AND   tables.typtab = "pay"
                            AND   tables.prefix = "code-pays"
                            AND   tables.codtab = auxili.pays
                            NO-LOCK NO-ERROR.
                IF AVAILABLE tables THEN auxapr.activi[ 10 ] = tables.libel1[ mem-langue] .

                assign auxapr.siret      = auxili.siret
                       auxapr.libabr     = auxili.libabr.
 
                FIND FIRST TABDOM where tabdom.codsoc = regs-soc
                                    and tabdom.etabli = ""
                                    and tabdom.typtab = auxapr.typaux-fac    
                                    and tabdom.prefix = auxapr.codaux-fac
                                    and tabdom.codtab begins "f"
                                    use-index primaire
                                    no-lock  no-error .
                if not available tabdom then
                    FIND FIRST TABDOM where tabdom.codsoc = regs-soc
                                      and tabdom.etabli = ""
                                      and tabdom.typtab = auxapr.typaux-fac    
                                      and tabdom.prefix = auxapr.codaux-fac
                                      use-index primaire
                                      no-lock  no-error .
                         
                if available tabdom
                then do :
                    auxapr.modreg = tabdom.libel [7].
                    FOR FIRST TABGES where tabges.codsoc = ""
                                     and   tabges.etabli = ""
                                     and   tabges.typtab = "ECH"
                                     and   tabges.prefix = "CODECH"
                                     and   tabges.libel2 [1] = tabdom.libel [8]
                                     use-index primaire
                                     no-lock :
                           
                                     auxapr.codech = tabges.codtab .
                    end.
               end. 

            /* Donnees complementaire */

               FIND TABDOM where tabdom.codsoc = regs-soc   and
                                 tabdom.etabli = "AUX"      and
                                 tabdom.typtab = "169"      and
                                 tabdom.prefix = auxapr.typaux-fac and
                                 tabdom.codtab = auxapr.codaux-fac
                                 use-index primaire
                                 no-lock no-error .

               if available tabdom 
               then assign auxapr.teleph-respon = tabdom.libel1 [4]
                           auxapr.fax-respon    = tabdom.libel1 [5]
                           auxapr.alpha-5       = tabdom.libel1 [3]
                           .

               { majmoucb.i auxapr }
 
            END.
            OTHERWISE .
        END CASE.
    end.    

end.        

release auxspe .
release auxapr .

