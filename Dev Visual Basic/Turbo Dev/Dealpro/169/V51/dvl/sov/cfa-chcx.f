/* LUCBRI-FIC 9 pour les recherches graphique, mise en place d'include gerant la large <EVO bp 07/09/00> */
/*************************************************************/
/* Gestion du scroll de selection des commandes non-livrees */
/* Nom du module   cfa-chcx.f                              */
/* Version a mettre dans le specif des clients graphiques */
/*********************************************************/

&global-define browse-file entete
&global-define browse-multiple
&global-define browse-label
&global-define browse-row     01.00
&global-define browse-col     01.00
&global-define browse-height  10.00
&global-define browse-width   130
&global-define browse-display   entete.typcom ~
                                entete.numbon FORMAT "x(8)" ~
                                top-partiel   ~
                                entete.datbon ~
                                entete.ref-tiers ~
                                entete.zon-dec-3 format ">>>,>>>,>>>.99-" ~
                                entete.totht     format ">>>,>>>,>>>.99-" ~
                                entete.devise ~
                                no-commande ~
                                entete.etabli

 
