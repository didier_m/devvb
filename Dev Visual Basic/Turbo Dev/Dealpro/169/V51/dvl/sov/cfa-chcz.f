/*************************************************************/
/* Gestion du scroll de selection des commandes non-livrees */
/* Nom du module   cfa-chgx.f                              */
/* Version a mettre dans le specif des clients graphiques */
/*********************************************************/
&global-define browse-file fictri
&global-define browse-multiple
&global-define browse-label 
&global-define browse-row     01.00 
&global-define browse-col     01.00 
&global-define browse-height  10.00 
&global-define browse-width   130.00 
&global-define browse-display   fictri.typcom ~
                                fictri.numbon ~
                                fictri.partiel   ~
                                fictri.datbon ~
                                fictri.ref-tiers   ~
                                fictri.zon-dec-3 format ">>>,>>>,>>>.99-" ~
                                fictri.totht     format ">>>,>>>,>>>.99-" ~
                                fictri.devise ~
                                fictri.commande ~
                                fictri.etabli                              
