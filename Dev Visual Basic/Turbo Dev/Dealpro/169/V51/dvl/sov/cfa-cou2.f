/* LUCBRI-FIC 9 pour les recherches graphique, mise en place d'include gerant la large <EVO bp 07/09/00> */
/*************************************************************/
/* Gestion du scroll de selection des factures              */
/* Nom du module   cfa-cou2.f                              */
/* Version a mettre dans le specif des clients graphiques */
/*********************************************************/

&global-define browse-file facach
&global-define browse-label 
&global-define browse-row     01.00 
&global-define browse-col     01.00 
&global-define browse-height  15.00 
&global-define browse-width   130.00 
&global-define browse-display   facach.codaux ~
                                facach.docume ~
                                facach.piece ~
                                facach.date-recept ~
                                facach.cntfac ~
                                facach.datpie ~
                                facach.mt-ttc ~
                                facach.devpie ~
                                facach.echeance ~
                                facach.ope-dest ~
                                facach.journal ~
                                facach.libel 
