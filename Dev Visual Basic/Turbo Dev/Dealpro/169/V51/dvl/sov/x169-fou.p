/**********************************************************************************************/
/*                                                                                            */
/*   Generation d'un fichier ASCII des fournisseurs modifies ou crees pour MAJ ELODIE VMS     */
/*                                                                   BP le 09.08.2000         */
/**********************************************************************************************/
DEFINE VARIABLE nombanque AS CHARACTER  NO-UNDO.
DEFINE VARIABLE nomville  AS CHARACTER  NO-UNDO.
DEFINE VARIABLE banque    AS CHARACTER  NO-UNDO.
DEFINE VARIABLE guichet   AS CHARACTER  NO-UNDO.
DEFINE VARIABLE compte    AS CHARACTER  NO-UNDO.
DEFINE VARIABLE rib       AS CHARACTER  NO-UNDO.
DEFINE VARIABLE norme     AS CHARACTER  NO-UNDO.
DEFINE VARIABLE modreg    AS CHARACTER  NO-UNDO.
DEFINE VARIABLE codech    AS CHARACTER  NO-UNDO.
DEFINE VARIABLE pays      AS CHARACTER  NO-UNDO.
DEFINE VARIABLE appcee    AS CHARACTER  NO-UNDO.
output to "m:\transfert_vms\fourni01.txt" .
for each auxili WHERE auxili.codsoc = "ref1"
                AND   auxili.typaux = "fou"
                AND ( auxili.datcre = TODAY 
                OR    auxili.datmaj > DATE( "05/10/2000" ) )  
                no-lock :
    ASSIGN nombanque = ""
           nomville  = ""
           banque    = ""
           guichet   = ""
           compte    = ""
           rib       = ""
           norme     = ""
           modreg    = ""
           codech    = ""
           appcee    = ""
           .
    FIND  TABDOM where tabdom.codsoc = "ref1"
                  AND    tabdom.etabli = "aux"
                  and    tabdom.typtab = "169"    
                  and    tabdom.prefix = auxili.typaux
                  AND    tabdom.codtab = auxili.codaux
                  no-lock  no-error .
    if available tabdom THEN appcee = tabdom.libel1[5] .

    FIND FIRST TABDOM where tabdom.codsoc = "ref1"
                              AND    tabdom.etabli = ""
                              and    tabdom.typtab = auxili.typaux    
                              and    tabdom.prefix = auxili.codaux
                              no-lock  no-error .
    if available tabdom
    then do :
        ASSIGN nombanque = tabdom.libel1[ 1 ]
               nomville  = tabdom.libel1[ 2 ]
               banque    = tabdom.libel1[ 3 ]
               guichet   = tabdom.libel1[ 4 ]
               compte    = tabdom.libel1[ 5 ]
               rib       = tabdom.libel1[ 6 ]
               norme     = STRING( tabdom.nombre[ 1 ] , "99" )
               modreg    = tabdom.libel1[ 7 ]
               codech    = tabdom.libel1[ 8 ]
               .
        IF codech <> "" THEN
        do: 
            FIND FIRST intcor WHERE intcor.evenement-int = "tiers"
                        AND   intcor.s-evenement-int = ""
                        AND   intcor.codsoc        = ""
                        AND   intcor.etabli        = ""                       
                        AND   intcor.prefix        = "calech"
                        AND   intcor.codtab        = string( codech )
                        NO-LOCK NO-ERROR.
           
            IF AVAILABLE intcor  
            THEN codech = intcor.codint .
            ELSE codech = "" .
        END.
        IF modreg <> "" THEN
        do: 
            FIND FIRST intcor WHERE intcor.evenement-int = "tiers"
                       AND   intcor.s-evenement-int = ""
                        AND   intcor.codsoc        = ""
                        AND   intcor.etabli        = ""
                        AND   intcor.prefix        = "code-reglm"
                        AND   intcor.codtab        = modreg
                        NO-LOCK NO-ERROR.
            IF AVAILABLE intcor  
            THEN modreg = intcor.codint .
            ELSE modreg = "" .
        END.
        IF norme <> "" THEN
        do: 
           
            FIND FIRST intcor WHERE intcor.evenement-int = "tiers"
                        AND   intcor.s-evenement-int = ""
                        AND   intcor.codsoc        = ""
                        AND   intcor.etabli        = ""
                        AND   intcor.prefix        = "dom-type"
                        AND   intcor.codtab        = norme
                        NO-LOCK NO-ERROR.
            IF AVAILABLE intcor  
            THEN norme = intcor.codint .
            ELSE norme = "" .
        END.
    END.
    FIND FIRST intcor WHERE intcor.evenement-int = "tiers"
                        AND   intcor.s-evenement-int = ""
                        AND   intcor.codsoc        = ""
                        AND   intcor.etabli        = ""
                        AND   intcor.prefix        = "code-pays"
                        AND   intcor.codtab        = auxili.pays
                        NO-LOCK NO-ERROR.
    IF AVAILABLE intcor  
    THEN pays = intcor.codint .
    ELSE pays = "" .

    MESSAGE auxili.codsoc "|" auxili.typaux "|" auxili.codaux "|" auxili.libabr "|"
            auxili.cpt "|" auxili.adres[1] "|" auxili.adres[2]  "|" auxili.adres[3] "|" 
            auxili.adres[4]  "|" auxili.adres[5] "|" pays "|" auxili.devise "|"
            auxili.ctax "|" auxili.typaux-pai "|" auxili.codaux-pai "|"
            auxili.cee "|" auxili.pays "|" auxili.ttva "|"
            nombanque "|" nomville "|" norme "|" banque "|" guichet "|" compte "|" rib "|"
            modreg "|" codech "|" appcee "|" auxili.siret "|"
            auxili.datcre "|" auxili.datmaj "|" .
END.
