/************************************************************************************************************
&PROC-INIT-ZONES Les zones n'ayant pas d'enregistrement dans leur table respective sont rendues inactives
&END
************************************************************************************************************/
PROCEDURE INIT-ZONES :

  FIND FIRST t-qualif NO-LOCK NO-ERROR.
  IF NOT AVAILABLE t-qualif THEN DEAL-ATTRIBUT ( "qualif" , "_sensitive=no" ).

  FIND FIRST t-divisi NO-LOCK NO-ERROR.
  IF NOT AVAILABLE t-divisi THEN DEAL-ATTRIBUT ( "divisi" , "_sensitive=no" ).

  FIND FIRST t-sitgeo NO-LOCK NO-ERROR.
  IF NOT AVAILABLE t-sitgeo THEN DEAL-ATTRIBUT ( "sitgeo" , "_sensitive=no" ).

END PROCEDURE. /* INIT-ZONES */


/************************************************************************************************************
&PROC-EXCEPTION-STD Liste des zones standard a exclure
&END
************************************************************************************************************/
PROCEDURE EXCEPTION-STD :
  DEF INPUT PARAMETER appelant        AS CHAR NO-UNDO. 
  DEF INPUT PARAMETER rendre-sensitif AS LOG  NO-UNDO.

  CASE appelant :
    
    /* Zones uniquement modifiables si Presence d'un bulletin sur la periode en cours */
    WHEN "AFTER-DISPLAY" OR WHEN "BEFORE-TRANSAC"
    THEN DO :
      IF NOT rendre-sensitif
      THEN DO :
        broker-trav = REPLACE ( broker-trav , "folder,"       , "" ).
        broker-trav = REPLACE ( broker-trav , ",sortie"       , "" ).
        broker-trav = REPLACE ( broker-trav , ",date-fin"     , "" ).
        broker-trav = REPLACE ( broker-trav , ",neutra"       , "" ).
        broker-trav = REPLACE ( broker-trav , ",date-neutral" , "" ).
        broker-trav = REPLACE ( broker-trav , ",date-anc,"    , "," ).
        broker-trav = REPLACE ( broker-trav , ",mutuel"       , "" ).
        broker-trav = REPLACE ( broker-trav , ",optmut"       , "" ).
        broker-trav = REPLACE ( broker-trav , ",codtro"       , "" ).
      END.
      ELSE DO :
        broker-trav = REPLACE ( broker-trav , ",coefficient" , "" ).
        broker-trav = REPLACE ( broker-trav , ",bareme"      , "" ).
        broker-trav = REPLACE ( broker-trav , ",categorie"   , "" ).
      END.
    END.

  END CASE.

  /* Recuperation du matricule VMS */
  IF DEAL-GET-SCREEN ( "ancien-mat" ) = "" 
  THEN DO :
    FIND d-tables NO-LOCK WHERE
         d-tables.ident     = glo-ident                       AND
         d-tables.progiciel = "SOPHIE"                        AND
         d-tables.motcle    = "CORRESP"                       AND
         d-tables.typtab    = ""                              AND
         d-tables.prefix    = ""                              AND
         d-tables.alpha [1] = DEAL-GET-SCREEN ( "matricule" ) NO-ERROR.

    IF AVAILABLE d-tables 
    THEN DEAL-PUT-SCREEN ( "ancien-mat" , d-tables.codtab ).
  END.

END PROCEDURE. /* EXCEPTION-STD */


/************************************************************************************************************
&PROC-LOCAL-MUTUEL Gestion des zones liees a la mutuelle
&END
************************************************************************************************************/
PROCEDURE LOCAL-MUTUEL :
  DEFINE INPUT PARAMETER i-Event  AS CHAR NO-UNDO.
  DEFINE INPUT PARAMETER i-Motcle AS CHAR NO-UNDO.
  DEFINE INPUT PARAMETER i-Param  AS CHAR NO-UNDO.

  CASE i-event :

    WHEN "GET" 
    THEN DO :
      IF DEAL-GET-SCREEN ( "mutuel" ) = "0" 
      THEN DO :
        DEAL-ATTRIBUT ( "optmut" , "_sensitive=no" ).
        
        RETURN "GO".
      END.
      ELSE DEAL-ATTRIBUT ( "optmut" , "_sensitive=yes" ).
    END.

  END CASE.

  RETURN "".
END PROCEDURE. /* LOCAL-MUTUEL */


/************************************************************************************************************
&PROC-LOCAL-SITGEO Gestion des zones liees a la situation geographique
&END
************************************************************************************************************/
PROCEDURE LOCAL-SITGEO :
  DEFINE INPUT PARAMETER i-Event  AS CHAR NO-UNDO.
  DEFINE INPUT PARAMETER i-Motcle AS CHAR NO-UNDO.
  DEFINE INPUT PARAMETER i-Param  AS CHAR NO-UNDO.

  CASE i-event :

    WHEN "GET" 
    THEN DO :
      IF DEAL-GET-SCREEN ( "sitgeo" ) = "SED" 
      THEN DO :
        DEAL-ATTRIBUT ( "folder#Page7" , "_sensitive=no" ).
        DEAL-ATTRIBUT ( "folder#Page8" , "_sensitive=yes" ).

        DEAL-PUT-SCREEN ( "regroupement"                  , "" ).
        DEAL-PUT-SCREEN ( "barbase"                       , "" ).
        DEAL-PUT-SCREEN ( "barstab"                       , "" ).
        DEAL-PUT-SCREEN ( "datstab"                       , "" ).
        DEAL-PUT-SCREEN ( "quartier"                      , "" ).
        DEAL-PUT-SCREEN ( "aninsc"                        , "" ).
        DEAL-PUT-SCREEN ( "lettre"                        , "" ).
        DEAL-PUT-SCREEN ( "chrono"                        , "" ).
        DEAL-PUT-SCREEN ( "codtro"                        , "" ).
        DEAL-PUT-SCREEN ( "01,02,03,04,05,06,07,08,09,10" , "" ).
        DEAL-PUT-SCREEN ( "11,12,13,14,15,16,17,18,19,20" , "" ).
      END.
      ELSE DO :
        DEAL-ATTRIBUT ( "folder#Page7" , "_sensitive=yes" ).
        DEAL-ATTRIBUT ( "folder#Page8" , "_sensitive=no" ).
                                            
        DEAL-PUT-SCREEN ( "fonction"    , "" ).
        DEAL-PUT-SCREEN ( "emplbrit"    , "" ).
        DEAL-PUT-SCREEN ( "coefficient" , "" ).
        DEAL-PUT-SCREEN ( "bareme"      , "" ).
        DEAL-PUT-SCREEN ( "categorie"   , "" ).
      END.
    END.
  
  END CASE.

  RETURN "".
END PROCEDURE. /* LOCAL-SITGEO */


/************************************************************************************************************
&PROC-LOCAL-EMPLBRIT Gestion des zones liees a l'emploi DRH
&END
************************************************************************************************************/
PROCEDURE LOCAL-EMPLBRIT :
  DEFINE INPUT PARAMETER i-Event  AS CHAR NO-UNDO.
  DEFINE INPUT PARAMETER i-Motcle AS CHAR NO-UNDO.
  DEFINE INPUT PARAMETER i-Param  AS CHAR NO-UNDO.

  CASE i-event :

    WHEN "GET" 
    THEN DO :
      FIND d-tables NO-LOCK WHERE 
           d-tables.ident     = glo-ident                      AND
           d-tables.progiciel = "SOPHIE"                       AND
           d-tables.motcle    = "EMPLOI"                       AND
           d-tables.typtab    = ""                             AND
           d-tables.prefix    = ""                             AND
           d-tables.codtab    = DEAL-GET-SCREEN ( "emplbrit" ) NO-ERROR.

      IF AVAILABLE d-tables 
      THEN DO :
        DEAL-PUT-SCREEN ( "coefficient" , d-tables.alpha [1] ).
        DEAL-PUT-SCREEN ( "bareme"      , d-tables.alpha [2] ).
        DEAL-PUT-SCREEN ( "categorie"   , d-tables.alpha [3] ).
      END.
    END.

  END CASE.

  RETURN "".
END PROCEDURE. /* LOCAL-EMPLBRIT */


/************************************************************************************************************
&PROC-CTRL-DATSTAB Controle de saisie de la date de stabilisation
&END
************************************************************************************************************/
PROCEDURE CTRL-DATSTAB :
  DEFINE INPUT PARAMETER i-Event  AS CHAR NO-UNDO.
  DEFINE INPUT PARAMETER i-Motcle AS CHAR NO-UNDO.
  DEFINE INPUT PARAMETER i-Param  AS CHAR NO-UNDO.

  CASE i-event :

    WHEN "GET" 
    THEN DO :
      IF DEAL-GET-SCREEN ( "barstab" ) <> "" AND DEAL-GET-SCREEN ( "datstab") = "" THEN RETURN "ERROR".
    END.
  
  END CASE.

  RETURN "".
END PROCEDURE. /* CTRL-DATSTAB */


/************************************************************************************************************
&PROC-CATPRO-SPE Proposition des zones en saisie
&END
************************************************************************************************************/
PROCEDURE CATPRO-SPE :
  
  IF periode.catpro <> DEAL-GET-SCREEN ( "catpro" )
  THEN DO :
    CASE DEAL-GET-SCREEN ( "socpai" ) :
      WHEN "BAI"
      THEN DO :

        CASE DEAL-GET-SCREEN ( "catpro" ) :
          WHEN "AGM" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie" , "01"  ).
            DEAL-PUT-SCREEN ( "sitgeo"    , "SED" ).
            DEAL-PUT-SCREEN ( "regime"    , "RG"  ).
          END.

          WHEN "EMP" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie" , "02"  ).
            DEAL-PUT-SCREEN ( "sitgeo"    , "SED" ).
            DEAL-PUT-SCREEN ( "regime"    , "RG"  ).
          END.

          WHEN "CAF" OR WHEN "CAU" OR WHEN "CSU" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie" , "03"  ).
            DEAL-PUT-SCREEN ( "sitgeo"    , "SED" ).
            DEAL-PUT-SCREEN ( "regime"    , "RG"  ).
          END.
               
          WHEN "OHO" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie"    , "153" ).
            DEAL-PUT-SCREEN ( "regroupement" , "15"  ).
            DEAL-PUT-SCREEN ( "sitgeo"       , "NAV" ).
            DEAL-PUT-SCREEN ( "regime"       , "RG"  ).
          END.

          WHEN "SHO" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie"    , "152" ).
            DEAL-PUT-SCREEN ( "regroupement" , "15"  ).
            DEAL-PUT-SCREEN ( "sitgeo"       , "NAV" ).
            DEAL-PUT-SCREEN ( "regime"       , "RG"  ).
          END.

          WHEN "MHO" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie"    , "151" ).
            DEAL-PUT-SCREEN ( "regroupement" , "15"  ).
            DEAL-PUT-SCREEN ( "sitgeo"       , "NAV" ).
            DEAL-PUT-SCREEN ( "regime"       , "RG"  ).
          END.

          WHEN "OME" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie"    , "163" ).
            DEAL-PUT-SCREEN ( "regroupement" , "16"  ).
            DEAL-PUT-SCREEN ( "sitgeo"       , "NAV" ).
            DEAL-PUT-SCREEN ( "regime"       , "RG"  ).
          END.

          WHEN "SME" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie"    , "162" ).
            DEAL-PUT-SCREEN ( "regroupement" , "16"  ).
            DEAL-PUT-SCREEN ( "sitgeo"       , "NAV" ).
            DEAL-PUT-SCREEN ( "regime"       , "RG"  ).
          END.

          WHEN "MME"
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie"    , "161" ).
            DEAL-PUT-SCREEN ( "regroupement" , "16"  ).
            DEAL-PUT-SCREEN ( "sitgeo"       , "NAV" ).
            DEAL-PUT-SCREEN ( "regime"       , "RG"  ).
          END.

          WHEN "OIM" OR WHEN "SIM" OR WHEN "EIM" OR WHEN "MIM"
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie"    , "17"  ).
            DEAL-PUT-SCREEN ( "regroupement" , "17"  ).
            DEAL-PUT-SCREEN ( "sitgeo"       , "NAV" ).
            DEAL-PUT-SCREEN ( "regime"       , "IM"  ).
          END.
        END CASE. /* CASE DEAL-GET-SCREEN ( "catpro" ) */
      END. /* BAI */

      WHEN "SER"
      THEN DO :
        DEAL-PUT-SCREEN ( "regime" , "RG" ).
 
        CASE DEAL-GET-SCREEN ( "catpro" ) :
          WHEN "AGM" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie" , "01"  ).
            DEAL-PUT-SCREEN ( "sitgeo"    , "SED" ).
          END.
                
          WHEN "EMP" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie" , "02"  ).
            DEAL-PUT-SCREEN ( "sitgeo"    , "SED" ).
          END.

          WHEN "CAU" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie" , "03"  ).
            DEAL-PUT-SCREEN ( "sitgeo"    , "SED" ).
          END.

          WHEN "OHO" OR WHEN "OME"
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie"    , "353" ).
            DEAL-PUT-SCREEN ( "regroupement" , "35"  ).
            DEAL-PUT-SCREEN ( "sitgeo"       , "NAV" ).
          END.

          WHEN "SHO" OR WHEN "SME" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie"    , "352" ).
            DEAL-PUT-SCREEN ( "regroupement" , "35"  ).
            DEAL-PUT-SCREEN ( "sitgeo"       , "NAV" ).
          END.

          WHEN "MHO" OR WHEN "MME" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie"    , "351" ).
            DEAL-PUT-SCREEN ( "regroupement" , "35"  ).
            DEAL-PUT-SCREEN ( "sitgeo"       , "NAV" ).
          END.
          
          WHEN "0RG" OR WHEN "ERG"
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie"    , "373" ).
            DEAL-PUT-SCREEN ( "regroupement" , "37"  ).
            DEAL-PUT-SCREEN ( "sitgeo"       , "NAV" ).
          END.

          WHEN "SRG" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie"    , "372" ).
            DEAL-PUT-SCREEN ( "regroupement" , "37"  ).
            DEAL-PUT-SCREEN ( "sitgeo"       , "NAV" ).
          END.
 
          WHEN "MRG" 
          THEN DO :
            DEAL-PUT-SCREEN ( "type-paie"    , "371" ).
            DEAL-PUT-SCREEN ( "regroupement" , "37"  ).
            DEAL-PUT-SCREEN ( "sitgeo"       , "NAV" ).
          END.
        END CASE. /* CASE DEAL-GET-SCREEN ( "catpro" ) */
      END. /* SERESTEL */

      WHEN "SDMC" 
      THEN DO :
        DEAL-PUT-SCREEN ( "type-paie" , "04"  ).
        DEAL-PUT-SCREEN ( "sitgeo"    , "SED" ).
        DEAL-PUT-SCREEN ( "regime"    , "RG"  ).
      END. /* SDMC */
    END CASE. /* CASE DEAL-GET-SCREEN ( "socpai" ) */
  
  END.
END PROCEDURE. /* CATPRO-SPE */
