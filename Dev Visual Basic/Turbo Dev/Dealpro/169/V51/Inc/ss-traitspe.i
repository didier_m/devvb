DEF VAR v-traitement  AS CHAR FORMAT "x"          NO-UNDO.
DEF VAR v-date-mini   AS DATE FORMAT "99/99/9999" NO-UNDO.
DEF VAR v-date-maxi   AS DATE FORMAT "99/99/9999" NO-UNDO.
DEF VAR v-mois-gen    AS CHAR FORMAT "x(6)"       NO-UNDO.
DEF VAR v-mois-ref    AS CHAR FORMAT "x(6)"       NO-UNDO.
DEF VAR v-ponderation AS CHAR                     NO-UNDO.
DEF VAR v-sequence    AS CHAR                     NO-UNDO.
DEF VAR v-rapport     AS CHAR FORMAT "x(20)"      NO-UNDO.
DEF VAR v-export      AS CHAR FORMAT "x(20)"      NO-UNDO.
