/* 169-FIC 4 Creation de tris specifs pour balance agee depuis AUXSPE <NEW air 23/08/00> */

procedure bornes-bdf :
    define var a as int no-undo .
    define buffer auxspe for tabdom.
    rg-ordre = int( age-classe[spe-a]).

    if rg-ordre = 0 then return .
    if not available c-auxili then return .

    assign    c-segment[rg-ordre] = "xxx"
              c-segment-lib[rg-ordre] = "SANS " .

    for first auxspe where auxspe.codsoc = c-auxili.codsoc and
                           auxspe.etabli = "AUX"           and
                           auxspe.typtab = "169"           and
                           auxspe.prefix = c-auxili.typaux and
                           auxspe.codtab = c-auxili.codaux
                           no-lock :

        if auxspe.libel1[7] < age-mini[spe-a] or
           auxspe.libel1[7] > age-maxi[spe-a] then do : age-ok = no . return . end.
        if age-tri[ spe-a] = "3" then
         if lookup ( auxspe.libel1[7] , spe-mat[spe-a] ) = 0 then do : age-ok = no . return . end .



        spe-cod[ rg-ordre ]    = auxspe.libel1[7] .
/*
        for first tables field(libel)
                where tables.codsoc = "" and
                      tables.etabli = "" and
                      tables.typtab = "SPE" and
                      tables.prefix = "XMAU-AGEN"  and
                      tables.codtab = spe-cod[rg-ordre]
                     no-lock :
              spe-lib[ rg-ordre] = tables.libel1[mem-langue] .
        end.  
*/
        if spe-cod[rg-ordre] = "" then spe-cod[rg-ordre] = "xxx" .

 end. /* else do */


end procedure .

procedure bornes-budget :
    define var a as int no-undo .
    define buffer auxspe for tabdom.
    
    
    rg-ordre = int( age-classe[spe-a]).

    if rg-ordre = 0 or rg-ordre > 15 then return .
    if not available c-auxili then return .

    assign    c-segment[rg-ordre] = "xxx"
              c-segment-lib[rg-ordre] = "SANS " .

    for first auxspe where auxspe.codsoc = c-auxili.codsoc and
                           auxspe.etabli = "AUX"           and
                           auxspe.typtab = "169"           and
                           auxspe.prefix = c-auxili.typaux and
                           auxspe.codtab = c-auxili.codaux
                           no-lock :

        if auxspe.libel1[8] < age-mini[spe-a] or
           auxspe.libel1[8] > age-maxi[spe-a] then do : age-ok = no . return . end.
        if age-tri[ spe-a] = "3" then
         if lookup ( auxspe.libel1[8] , spe-mat[spe-a] ) = 0 then do : age-ok = no . return . end .



        spe-cod[ rg-ordre ]    = auxspe.libel1[8] .

        for first tables field(libel)
                where tables.codsoc = "" and
                      tables.etabli = "" and
                      tables.typtab = "169" and
                      tables.prefix = "BUDGET"  and
                      tables.codtab = spe-cod[rg-ordre]
                     no-lock :
              spe-lib[ rg-ordre] = tables.libel1[mem-langue] .
        end.
        if spe-cod[rg-ordre] = "" then spe-cod[rg-ordre] = "xxx" .

 end. /* else do */


end procedure .

procedure bornes-CPostal :
    define var a as int no-undo .
    define buffer auxspe for tabdom.
    
    
    rg-ordre = int( age-classe[spe-a]).

    if rg-ordre = 0 or rg-ordre > 15 then return .
    if not available c-auxili then return .

    assign    c-segment[rg-ordre] = "xxx"
              c-segment-lib[rg-ordre] = "SANS " .

    for first auxspe where auxspe.codsoc = c-auxili.codsoc and
                           auxspe.etabli = "AUX"           and
                           auxspe.typtab = "169"           and
                           auxspe.prefix = c-auxili.typaux and
                           auxspe.codtab = c-auxili.codaux
                           no-lock :

        if auxspe.libel1[9] < age-mini[spe-a] or
           auxspe.libel1[9] > age-maxi[spe-a] then do : age-ok = no . return . end.
        if age-tri[ spe-a] = "3" then
         if lookup ( auxspe.libel1[9] , spe-mat[spe-a] ) = 0 then do : age-ok = no . return . end .



        spe-cod[ rg-ordre ]    = auxspe.libel1[9] .

        for first tables field(libel)
                where tables.codsoc = "" and
                      tables.etabli = "" and
                      tables.typtab = "169" and
                      tables.prefix = "CODEPOSTAL"  and
                      tables.codtab = spe-cod[rg-ordre]
                     no-lock :
              spe-lib[ rg-ordre] = tables.libel1[mem-langue] .
        end.
        if spe-cod[rg-ordre] = "" then spe-cod[rg-ordre] = "xxx" .

 end. /* else do */


end procedure .

/*
procedure x100-detail:
    define var a as int no-undo .
    define buffer auxspe for tabdom.


    rg-ordre = int( age-classe[spe-a]).


    if rg-ordre = 0 or rg-ordre > 15 then return .
    if not available c-auxili then return .

    assign    c-segment[rg-ordre] = "xxx"
              c-segment-lib[rg-ordre] = "SANS " .

    for first auxspe where auxspe.codsoc = c-auxili.codsoc and
                           auxspe.etabli = "AUX"           and
                           auxspe.typtab = "MAU"           and
                           auxspe.prefix = c-auxili.typaux and
                           auxspe.codtab = c-auxili.codaux
                           no-lock :

        if auxspe.libel1[6] < age-mini[spe-a] or
           auxspe.libel1[6] > age-maxi[spe-a] then do : age-ok = no . return . end.
        if age-tri[ spe-a] = "3" then
         if lookup ( auxspe.libel1[6] , spe-mat[spe-a] ) = 0 then do : age-ok = no . return . end .



        age-zone-spe = auxspe.libel1[6] + "|" + age-zone-spe.

        for first tables field(libel)
                where tables.codsoc = "" and
                      tables.etabli = "" and
                      tables.typtab = "SPE" and
                      tables.prefix = "XMAU-SECT"  and
                      tables.codtab = spe-cod[rg-ordre]
                     no-lock :
              spe-lib1 = tables.libel1[mem-langue] + "|" + spe-lib1.
        end.
        if entry( 1 , age-zone-spe , "|") = "" then entry( 1 , age-zone-spe , "|") = "xxx" .

 end. /* else do */


end procedure .
*/
