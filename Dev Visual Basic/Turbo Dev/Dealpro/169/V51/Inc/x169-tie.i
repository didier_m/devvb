/* 169-FIC 1 Gestion de la fiche specifique auxiliaire <NEW mu 18/08/00> */
/* LUCIE-FIC 520 Ajout de la gestion des auxiliaires depuis LUCIE/ELODIE pour cli.169 <EVO bp 02/08/00> */
/* x169-tie.i */ 

def {1} shared var spe-codsoc like codsoc-soc no-undo .
def {1} shared var spe-typaux like auxili.typaux no-undo .
def {1} shared var spe-codaux like auxili.codaux no-undo .
def {1} shared var spe-recid  as recid no-undo .
def {1} shared buffer auxspe for tabdom .


