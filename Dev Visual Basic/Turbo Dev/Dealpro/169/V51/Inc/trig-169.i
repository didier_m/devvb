/* 169-FIC 6 Creation d'un trigger pour MAJ solana et solan1 <NEW air 25/08/00> */
/*--------------------------------------------*/
/* trig-169.p   le 23/08/2000                 */
/*--------------------------------------------*/
/* {connect.i}
{boot-pro.i } */


/*---------------------------------------------------------------------*/
/* a l'alimentation de solana et solan1,                               */
/* on ne mouvemente pas les CG dans l'indice a nouveau                 */
/*---------------------------------------------------------------------*/

/*--- --------------------< SOLAN1 >-----------------------------------*/
on write of solan1 do : 
   Assign
       solan1.CODSOC             = right-trim( substring(solan1.CODSOC,1,4) ) 
       solan1.CODETB             = right-trim( substring(solan1.CODETB,1,4) ) 
       solan1.EXECPT             = right-trim( substring(solan1.EXECPT,1,4) ) 
       solan1.HEUMAJ             = right-trim( substring(solan1.HEUMAJ,1,5) ) 
       solan1.OPEMAJ             = right-trim( substring(solan1.OPEMAJ,1,3) ) 
   .
   
   /* Raz du report a nouveau */
   Assign solan1.Dt-cpt[28] = 0
          solan1.Ct-cpt[28] = 0
          solan1.Dt-dev[28] = 0
          solan1.Ct-dev[28] = 0
          Solan1.qte[28]    = 0.
 
end . /* on write of solan1 */

/*--- --------------------< SOLANA >-----------------------------------*/
on write of solana do : 
   Assign
       solana.CODSOC             = right-trim( substring(solana.CODSOC,1,4) ) 
       solana.CODETB             = right-trim( substring(solana.CODETB,1,4) ) 
       solana.EXECPT             = right-trim( substring(solana.EXECPT,1,4) ) 
       solana.CODGEN             = right-trim( substring(solana.CODGEN,1,10) ) 
       solana.DEVISE             = right-trim( substring(solana.DEVISE,1,3) ) 
       solana.CODE-QUANT         = right-trim( substring(solana.CODE-QUANT,1,3) ) 
       solana.HEUMAJ             = right-trim( substring(solana.HEUMAJ,1,5) ) 
       solana.OPEMAJ             = right-trim( substring(solana.OPEMAJ,1,3) ) 
   .
   
   /* Raz du report a nouveau */
   Assign solana.Dt-cpt[28] = 0
          solana.Ct-cpt[28] = 0
          solana.Dt-dev[28] = 0
          solana.Ct-dev[28] = 0
          solana.qte[28]    = 0.

 
end . /* on write of solana */
 
/*--- --------------------< TABLES >-----------------------------------*/
on write of tables 
DO :
    IF tables.codsoc = ""
                     AND tables.etabli = ""
                     AND tables.typtab = "ANA"
                     AND tables.prefix = "segment-01" 
    THEN DO :
        FIND eloaff WHERE 
       eloaff.codsoc             = tables.codsoc AND
       eloaff.codetb             = tables.etabli AND
       eloaff.codaff             = tables.codtab 
       EXCLUSIVE-LOCK NO-ERROR.
     
    IF AVAILABLE eloaff THEN ASSIGN
       eloaff.libel[1]           = tables.libel1[1]
       eloaff.libel[2]           = tables.libel1[2]
       eloaff.libel[3]           = tables.libel1[3]
       eloaff.libel[4]           = tables.libel1[4]
       eloaff.libel[5]           = tables.libel1[5] .
   
    END.
end . /* on write of tables */
 


