/* V42ELO-FIC 625 Consultation par le F12. ( desactiver par defaut les autorisations ) <BUG/EVO lbo 19/01/00> */
/* V42ELO-FIC 508 Gestion des autorisations en visu (F12) suite fiche 481. <EVO lbo 29/11/99> */
/* V42ELO-FIC 481 Gestion des autorisations operateurs en consultation par F12. <EVO lbo 22/11/99> */
/* cvextest.i : Tests autorisation par operateur sur F12 ( a mettre dans /applic/client ) */



/* REM : Retirer le REM pour activer le test des autorisation ( dans /applic/client ) 

{out-jnl.i}  /* autorisation journal/operat   */
{out-opty.i} /* autorisation typaux/operat    */
{out-opan.i} /* autorisation analytique/operat*/

*/

{out-opty.i} /* autorisation typaux/operat   ACTIVE */
{out-opcg.i} /* autorisation codgen/operat   ACTIVE */
