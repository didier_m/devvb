/* LUCIE-FIC 664 Passer la fenetre d'affichage du rechbro en .f + libelle du rechbro <EVO bp 27/11/00> */
/*************************************************************/
/*************************************************************/
/* Gestion du scroll de selection des factures              */
/* Nom du module   rec-fac1.f                              */
/* Version a mettre dans le specif client 169 - Brittany  */
/*********************************************************/
 
&global-define browse-file entete
&global-define browse-label 
&global-define browse-row     01.00 
&global-define browse-col     01.00 
&global-define browse-height  15.00 
&global-define browse-width   130.00 
&global-define browse-display   entete.numfac ~
                                entete.codaux ~
                                entete.datbon format "99/99/99" ~
                                entete.datdep format "99/99/99" ~
                                entete.opecre ~
                                entete.ttc ~
                                entete.devise ~
                                aux-affair  ~
                                entete.nivcom


{ rech-bro.f }

assign  browse-title        = x-libelle[81] 
        entete.numfac:label = x-libelle[82] 
        entete.codaux:label = x-libelle[83] 
        entete.datbon:label = x-libelle[84] 
        entete.datdep:label = x-libelle[85] 
        entete.opecre:label = x-libelle[86] 
        entete.ttc:label    = x-libelle[87] 
        entete.devise:label = x-libelle[88] 
        aux-affair:label    = x-libelle[89] 
        entete.nivcom:LABEL = x-libelle[90]
        . 