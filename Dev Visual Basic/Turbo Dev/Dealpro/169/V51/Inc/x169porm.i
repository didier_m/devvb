/* 169-FIC 5 Rajout tri specifs pour la page F du portefeuille (issus du AUXSPE) <EVO air 23/08/00> */
/* EXEMPLE DE SPECIF test sur la periode de cloture */ 
    
    def var ind as int.
    
    case rg-ecr : 
        when ecr-601t
        then do: 
            ind = 0.   
            
            regs-fileacc = "AUXILI" + mvtcpt.typaux .
            {regs-rec.i}
            
            Find Tabdom where Tabdom.codsoc = regs-soc      and 
                              Tabdom.etabli = "AUX"         and
                              Tabdom.typtab = "169"         and
                              Tabdom.prefix = mvtcpt.typaux and
                              Tabdom.codtab = mvtcpt.codaux
                              no-lock No-error.
                if not available tabdom then do : top-ok = "N" . next lect-mvtcpt . end. 
                              
                Else if Tabdom.libel1[7] < por-minit[rg-ecr] or 
                        Tabdom.libel1[7] > por-maxit[rg-ecr] then do : top-ok = "N" . next lect-mvtcpt . end.
                if por-trit[rg-ecr] = " 3" and 
                lookup( Tabdom.libel1[7]  , por-multt[rg-ecr]  ,"-" ) = 0
                then do : top-ok = "N" . next lect-mvtcpt . end. 
        
        
                
            c-segment[ rg-ordre ]    = Tabdom.libel1[7]  .
            if c-segment[rg-ordre] = "" then c-segment[rg-ordre] = "NO" .  
               
                          
       end . 

       when ecr-602t
       then do:    
            regs-fileacc = "AUXILI" + mvtcpt.typaux .
            {regs-rec.i}
            
            Find Tabdom where Tabdom.codsoc = regs-soc      and 
                              Tabdom.etabli = "AUX"         and
                              Tabdom.typtab = "169"         and
                              Tabdom.prefix = mvtcpt.typaux and
                              Tabdom.codtab = mvtcpt.codaux
                              no-lock No-error.
                if not available tabdom then do : top-ok = "N" . next lect-mvtcpt . end. 
        
                else if Tabdom.libel1[8] < por-minit[rg-ecr] or 
                        Tabdom.libel1[8] > por-maxit[rg-ecr] then do : top-ok = "N" . next lect-mvtcpt . end.        
                if por-trit[rg-ecr] = "3" and 
                lookup( Tabdom.libel1[8] , por-multt[rg-ecr]  ,"-" ) = 0
                then do : top-ok = "N" . next lect-mvtcpt . end. 
                
            c-segment[ rg-ordre ]    = Tabdom.libel1[8].
            if c-segment[rg-ordre] = "" then c-segment[rg-ordre] = "NO" .  
            
            find Tables where Tables.codsoc = ""
                          and Tables.etabli = ""
                          and Tables.typtab = "169"
                          And Tables.prefix = "BUDGET"
                          And Tables.codtab = Tabdom.libel1[8]
                          no-lock no-error.
    
            if available Tables
            then c-segment-lib[rg-ordre] = Tables.libel1[mem-langue].
            else c-segment-lib[rg-ordre] = "Inex." .
        
       end . 

       when ecr-603t
       then do:    
            regs-fileacc = "AUXILI" + mvtcpt.typaux .
            {regs-rec.i}
            
            Find Tabdom where Tabdom.codsoc = regs-soc      and 
                              Tabdom.etabli = "AUX"         and
                              Tabdom.typtab = "169"         and
                              Tabdom.prefix = mvtcpt.typaux and
                              Tabdom.codtab = mvtcpt.codaux
                              no-lock No-error.
                if not available tabdom then do : top-ok = "N" . next lect-mvtcpt . end. 
        
                else if Tabdom.libel1[9] < por-minit[rg-ecr] or 
                        Tabdom.libel1[9] > por-maxit[rg-ecr] then do : top-ok = "N" . next lect-mvtcpt . end.        
                if por-trit[rg-ecr] = "3" and 
                lookup( Tabdom.libel1[9] , por-multt[rg-ecr]  ,"-" ) = 0
                then do : top-ok = "N" . next lect-mvtcpt . end. 
                
            c-segment[ rg-ordre ]    = Tabdom.libel1[9].
            if c-segment[rg-ordre] = "" then c-segment[rg-ordre] = "NO" .  
            
            find Tables where Tables.codsoc = ""
                          and Tables.etabli = ""
                          and Tables.typtab = "169"
                          And Tables.prefix = "CODEPOSTAL"
                          And Tables.codtab = Tabdom.libel1[9]
                          no-lock no-error.
    
            if available Tables
            then c-segment-lib[rg-ordre] = Tables.libel1[mem-langue].
            else c-segment-lib[rg-ordre] = "Inex." .
       end . 

            
end case.     
