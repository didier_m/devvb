/* 169-FIC 19 Creation include specif pour acces code bdf dans AUXSPE <NEW air 28/03/01> */
/* 169-FIC 18 Extraction banque de france : creation include specif pour AUXPSE <EVO AIR 06/03/01> */
 /* x169x102.i : include specif pour rechercher le code statistique 
                 dans la fiche tiers specifique */
 
 regs-fileacc = "AUXILI" + mvtcpt-tie.typaux.
               { regs-rec.i }
 FIND  tabdom WHERE tabdom.codsoc = regs-soc
                AND tabdom.etabli = "aux"
                AND tabdom.typtab = "169"
                AND tabdom.prefix = mvtcpt-tie.typaux
                AND TABDOM.codtab = mvtcpt-tie.codaux
                EXCLUSIVE-LOCK NO-ERROR .
     
     IF AVAILABLE tabdom AND tabdom.libel1[7] <> ""
         THEN ASSIGN tmp-stat = tabdom.libel1[7].
         ELSE assign tmp-stat = code-stat-def.
