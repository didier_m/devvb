/* 169-FIC 21 Brancher les engagements dans la balance analytique <EVO isa 23/07/01> */
/* V423ELO-FIC 325 Brancher les engagements ( SOLBUD/ENG) dans la balance Analytique <EVO isa 23/07/01> */
/* V423ELO-FIC 304 Balance analytique. Active l'include noyau au lieu du specifique. <BUG LBO 11/07/01> */
/* V423ELO-FIC 291 Balance analytique : ajouter type de budget en cumule ou periode <EVO isa 06/07/01> */
/* V423ELO-FIC 57 Gand evolution dans les balances analytiques <NEW/EVO isa 19/03/01> */
/* V42ELO-FIC 518 Acces au nouveau budget par les balances analytiques + Specif export <NEW isa 02/12/99> */
/* CROSS-DEB Cumule de solbud dans le matrice BAN-xxx CROSS-FIN */
/*--------------------------------*/
/*                BAN-sma4.i       */
/*--------------------------------*/

/*--------------------------------*/
/* DEAL Informatique ISA - FRE -  */
/*                                */
/* ecrite  le 18 03 1994          */
/* revisee le 18 03 1994          */
/*--------------------------------*/

/* include noyau  {banxsma4.i}  */
/* pour 196      {x196sma4.i} */
/* pout 169     */ {x169sma4.i}  
