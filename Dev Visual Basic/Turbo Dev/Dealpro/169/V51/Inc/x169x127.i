/* 169-FIC 17 include specif pour edition de la decimalisation dans les releves <EVO AIR 21/02/01> */
/* lolo le 20/02/01 : recuperation de la decimale du montant de releve */
    
    Solde = detrve.mt-dev-rel + ht-escrelev-ini + tv-escrelev-ini.
        
    if solde < 0 then solde = - solde.
    
    if dec(solde) - int(solde) < 0  then ma-valeur = string((dec(solde) - ( int(solde) - 1 )) * 100 , "99" ).
    else ma-valeur = string((dec(solde) - int(solde)) * 100 , "99" ).
    
    { maq-majm.i "05" "110" "ma-valeur" }
        

