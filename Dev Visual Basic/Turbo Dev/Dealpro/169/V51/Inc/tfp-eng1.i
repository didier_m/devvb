/************************************************************/
/*             T F P - E N G 1 . I                          */
/*   Specif brittany pour MAJ engagement Lucie dans Elodie  */
/************************************************************/

FIND FIRST LIGSPE where ligspe.codsoc =  lignes.codsoc 
	              and   ligspe.motcle =  lignes.motcle 
	              and   ligspe.typcom =  lignes.typcom 
	              and   ligspe.numbon =  lignes.numbon 
	              and   ligspe.chrono =  lignes.chrono 
                  NO-LOCK NO-ERROR.
IF AVAILABLE ligspe THEN 
DO:
    find FIRST tables where tables.codsoc = codsoc-soc      
                      AND   tables.etabli = ""              
                      AND   tables.typtab = "PAR"          
                      AND   tables.prefix = "PERIODE"       
                      AND   tables.dattab[2] >= ligspe.date-1
                      NO-LOCK NO-ERROR .
    IF AVAILABLE tables THEN 
        ASSIGN  tra-execpt  = substring ( tables.codtab , 1 , 4 )
                tra-percpt  = substring ( tables.codtab , 5 , 3 ) .

    if tra-execpt + tra-percpt > baf-execpt + baf-permax then next .
END.
ELSE DO:
    find FIRST tables where tables.codsoc = codsoc-soc      
                      AND   tables.etabli = ""              
                      AND   tables.typtab = "PAR"          
                      AND   tables.prefix = "PERIODE"       
                      AND   tables.dattab[2] >= lignes.datdep
                      NO-LOCK NO-ERROR .
    IF AVAILABLE tables THEN 
        ASSIGN  tra-execpt  = substring ( tables.codtab , 1 , 4 )
                tra-percpt  = substring ( tables.codtab , 5 , 3 ) .

    if tra-execpt + tra-percpt > baf-execpt + baf-permax then next .
END.
