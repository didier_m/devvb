/* 169-FIC 5 Rajout tri specifs pour la page F du portefeuille (issus du AUXSPE) <EVO air 23/08/00> */
/* xyyypor6.i page specif du portefeuille */ 
/* execution d'un specif en borne mini et maxi */ 
/* procedure modif des longeurs et du libelle */ 

procedure por-ini6 : 
    assign 
    por-long-seg6[1] = 10  
    por-long-seg6[2] = 10
    por-long-seg6[3] = 5
    por-long-seg6[4] = 0
    por-long-seg6[5] = 0
    por-long-seg6[6] = 0
    por-long-seg6[7] = 0
    por-long-seg6[8] = 0
    por-long-seg6[9] = 0
    por-long-seg6[10] = 0 
    por-long-seg6[11] = 0  
    por-long-seg6[12] = 0
    por-long-seg6[13] = 0
    por-long-seg6[14] = 0
    por-long-seg6[15] = 0.
end . 
      


procedure por-sai6: /* selection mini et maxi */ 
def var t-aux as char.
t-aux = "cli".

regs-soc = codsoc-soc.
regs-fileacc = "AUXILICLI"  .
{ regs-rec.i }

if por-mini6[ ecr-choix ] = "MULTI"
then do:
        por-mini6[ ecr-choix ] = "".
        por-maxi6[ ecr-choix ] = "".
        por-mul6[ecr-choix ]   = "" .
        display por-mini6[ ecr-choix] no-label
                        with frame fr-por-mini6.
        display por-maxi6[ ecr-choix] no-label
                        with frame fr-por-maxi6.
end.
ETI-mini:
repeat : 

        message color normal por-mess2[1] + " " + por-mess1[6].
        update por-mini6[ecr-choix ] no-labels
                go-on( {fh.i}  {f4.i} {fr.i}) with frame fr-por-mini6.

        if keyfunction( lastkey) = "end-error" or 
           keyfunction(lastkey) = "cursor-up" then leave eti-mini.
        display por-mini6[ ecr-choix] no-label with frame fr-por-mini6.
               
        if keyfunction(lastkey) = "find"
        then do:

            case ecr-choix : 
                when 2
                    then do:  
                        assign codsoc = ""
                               etabli = ""
                               typtab = "169"
                               prefix = "BUDGET"
                               libel  = por-mini6[ ecr-choix ].
                         {run.i tabrech.p}
                         if codtab = "$RIEN$" then undo , retry.
                         else por-mini6[ ecr-choix ] = codtab .
                    end.

                when 3
                    then do:  
                        assign codsoc = ""
                               etabli = ""
                               typtab = "169"
                               prefix = "CODEPOSTAL"
                               libel  = por-mini6[ ecr-choix ].
                         {run.i tabrech.p}
                         if codtab = "$RIEN$" then undo , retry.
                         else por-mini6[ ecr-choix ] = codtab .
                    end.
                    
            end. /* case ecr-choix */
        end.
        
        if por-mini6[ ecr-choix] <> ""
        then do:
            case ecr-choix : 
                when 2
                    then do:  
                        find tables where tables.codsoc = ""
                                      and tables.etabli = ""
                                      and tables.typtab = "169"
                                      and tables.prefix = "BUDGET"
                                      and tables.codtab = por-mini6[ ecr-choix ]
                                      no-lock no-error.
                        if not available tables then undo , retry.
                    end.

                when 3
                    then do:  
                        
                        find tables where tables.codsoc = ""
                                      and tables.etabli = ""
                                      and tables.typtab = "169"
                                      and tables.prefix = "CODEPOSTAL"
                                      and tables.codtab = por-mini6[ ecr-choix ]
                                      no-lock no-error.
                        if not available tables then undo , retry.
                         
                    end.
                    
            end. /* case ecr-choix */
        end.
        
        
        display por-mini6[ ecr-choix] no-label with frame fr-por-mini6.
        /* test si egalite */
        if por-tri6[ ecr-choix] = " 1" or
           por-tri6[ ecr-choix] = " 6"
        then do:
                por-maxi6[ ecr-choix ] = por-mini6[ ecr-choix ] .
                display por-maxi6[ ecr-choix] no-label
                with frame fr-por-maxi6.
                leave.
        end.
        if por-maxi6[ ecr-choix] = ""
        then do:
                por-maxi6[ ecr-choix ] = por-mini6[ ecr-choix ] .
                display por-maxi6[ ecr-choix] no-label
                with frame fr-por-maxi6.
        end.
ETI-maxi:
repeat:
        message color normal por-mess2[1] + " " + por-mess2[2].
        message color normal por-mess1[6] .
        update  por-maxi6[ ecr-choix ] no-labels
                go-on( Return {f4.i} {fh.i} {fr.i} ) with frame fr-por-maxi6.

        if keyfunction( lastkey) = "cursor-up"
        then next eti-maxi .
        
        if keyfunction( lastkey) = "end-error" then leave eti-maxi.

        if keyfunction(lastkey) = "find"
        then do:
            case ecr-choix : 
                when 2
                    then do:  
                        assign codsoc = ""
                               etabli = ""
                               typtab = "169"
                               prefix = "BUDGET"
                               libel  = por-maxi6[ ecr-choix ].
                         {run.i tabrech.p}
                         if codtab = "$RIEN$" then undo , retry.
                         else por-maxi6[ ecr-choix ] = codtab .
                    end.

                when 3
                    then do:  
                        assign codsoc = ""
                               etabli = ""
                               typtab = "169"
                               prefix = "CODEPOSTAL"
                               libel  = por-maxi6[ ecr-choix ].
                         {run.i tabrech.p}
                         if codtab = "$RIEN$" then undo , retry.
                         else por-maxi6[ ecr-choix ] = codtab .
                    end.
                  
                    
            end. /* case ecr-choix */
        end.
        
        if por-maxi6[ ecr-choix] <> ""
        then do:
            case ecr-choix : 
                when 2
                    then do:  
                        find tables where tables.codsoc = ""
                                      and tables.etabli = ""
                                      and tables.typtab = "169"
                                      and tables.prefix = "BUDGET"
                                      and tables.codtab = por-maxi6[ ecr-choix ]
                                      no-lock no-error.
                        if not available tables then undo , retry.
                    end.

                when 3
                    then do:  
                        
                        find tables where tables.codsoc = ""
                                      and tables.etabli = ""
                                      and tables.typtab = "169"
                                      and tables.prefix = "CODEPOSTAL"
                                      and tables.codtab = por-maxi6[ ecr-choix ]
                                      no-lock no-error.
                        if not available tables then undo , retry.
                         
                    end.
                    
            end. /* case ecr-choix */
        end.
        
        if por-maxi6[ecr-choix] < por-mini6[ecr-choix] then undo , retry.
        display por-maxi6[ ecr-choix] no-label with frame fr-por-maxi6.
        leave . 
end. /* end  eti-mini */
leave . 
end. /* end  eti-maxi */

end. 

procedure por-mul6: 
          
           if keyfunction(lastkey) = "end-error" then undo , return.
           case ecr-choix : 
                when 2
                    then do:  
                        prefix = "TABLES,,,169,BUDGET" .
                    end.

                when 3
                    then do:  
                        prefix = "TABLES,,,169,CODEPOSTAL" .
                    end.
                    
            end. /* case ecr-choix */
   

        
        
	  codtab = ""   . 
	  libel = "$SEP-$" + por-mul6 [ecr-choix] .      
	  run r-elomul.p( input prefix , input codtab,input-output libel ) .
	  if libel = "$rien$" then undo , retry.
	  assign por-mini6[ecr-choix] = "MULTI" 
	         por-maxi6[ecr-choix] = "MULTI" 
		     por-mul6[ecr-choix] = libel .  

end. 
