/* LUCBRI-FIC 11 Mise en place dans cad-arti.i du cadrage article ( 6 avec pre-zeros ) <EVO bp 18/09/00> */
/*---------------------------------------------------------*/
/*           Include cadrage code MAGASIN                  */
/*                                                         */
/*                      cad-arti.i                         */
/*---------------------------------------------------------*/

/*   BRITTANY :Pre-zero sur 6 de long - + TRIM   */

{1}  =  trim( {1} ) .

if {1} = ""  then  {1} = "000000" .

{1} = SUBSTRING ( "000000" , 1 , 6 - LENGTH( {1} )  ) + {1} .
