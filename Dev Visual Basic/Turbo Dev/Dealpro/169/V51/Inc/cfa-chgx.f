/* LUCBRI-FIC 10 diverses modifs effectu�es chez le  client 169 a la mise en place <EVO bp 18/09/00> */
/* LUCBRI-FIC 9 pour les recherches graphique, mise en place d'include gerant la large <EVO bp 07/09/00> */
/*************************************************************/
/* Gestion du scroll de selection des commandes non-livrees */
/* Nom du module   cfa-chgx.f                              */
/* Version a mettre dans le specif des clients graphiques */
/*********************************************************/
&global-define browse-file facach
&global-define browse-label 
&global-define browse-row     01.00 
&global-define browse-col     01.00 
&global-define browse-height  10.00 
&global-define browse-width   130.00 
&global-define browse-display   facach.docume ~
                                facach.piece  ~
                                facach.cntfac ~
                                facach.datpie  format "99.99.9999" ~
                                facach.mt-dev  format ">>>,>>>,>>>.99-" ~
                                facach.devpie ~
                                facach.echeance format "99.99.9999" ~
                                facach.ope-dest ~
                                facach.journal  ~
                                facach.libel
