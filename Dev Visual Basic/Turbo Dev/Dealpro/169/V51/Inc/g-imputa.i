/* LUCBRI-FIC 3 Modif pour saisie code imputation sur 7 caracteres en articles/tables <EVO lci 20/07/00> */
/*---------------------------------------------------------*/
/* Forme Partagable de l'entete de la gestion des comptes  */
/* par type de produit et taux de TVA                      */
/* g-imputa.i                                              */
/*---------------------------------------------------------*/
 
def {1} shared var soc-impapr      like impapr.codsoc .
def {1} shared var imp-motcle      like impapr.motcle .
 
def {1} shared var libelle         as char                extent 49.
def {1} shared var imp-tva         as char format "x(3)"  extent 10.
def {1} shared var imp-libtva      as char format "x(13)" extent 10.
def {1} shared var imp-cpt         as char format "x(10)" extent 10.
def {1} shared var imp-cptana      as char format "x(10)" extent 100.
def {1} shared var imp-lib1        as char format "x(17)" extent 10.
def {1} shared var imp-cpttva      as char format "x(10)" extent 10.
def {1} shared var imp-lib2        as char format "x(17)" extent 10.
def {1} shared var sai-tva         as char format "x(3)"  .
def {1} shared var sai-libtva      as char format "x(13)" .
def {1} shared var sai-cpt         as char format "x(10)" .
def {1} shared var sai-lib1        as char format "x(17)" .
def {1} shared var sai-cpttva      as char format "x(10)" .
def {1} shared var sai-lib2        as char format "x(17)" .
def {1} shared var imput           as char format "x(7)" .
def {1} shared var typimp          as char format "x(3)" .
def {1} shared var lib-typimp      as char format "x(30)" .
def {1} shared var lib-typart      as char format "x(35)".
 
def {1} shared var touche          as char format "x(20)" no-undo .
def {1} shared var ok-lecture      as logical no-undo.
def {1} shared var indic           as integer format ">9".
 
def {1} shared var imp-i as int no-undo .
def {1} shared var imp-j as int no-undo .
def {1} shared var new-ligne as char no-undo .
 
 
 
def {1} shared frame imp-entete .
def {1} shared frame imp-entete1 .
def {1} shared frame imp-ligne .
def {1} shared frame imp-colonne .
 
form libelle[2]  format "x(18)"   typimp   lib-typimp skip
     libelle[3]  format "x(18)"   imput               skip
     libelle[4]  format "x(18)"   lib-typart
     with frame imp-entete  no-label  with row 2  centered   overlay {v6frame.i} .
 
form libelle[5] format "x(3)"
     libelle[6] format "x(13)"
     libelle[7] format "x(10)"
     libelle[8] format "x(17)"
     libelle[9] format "x(10)"
     libelle[10] format "x(17)"
     with frame imp-entete1  row 8  centered  no-label  no-box   overlay {v6frame.i} .
 
form sai-tva
     sai-libtva
     sai-cpt
     sai-lib1
     sai-cpttva
     sai-lib2
     with frame imp-ligne  row 9  centered  10 down  no-label   overlay {v6frame.i} .
 
form "" with frame imp-colonne  row 9  col 6  10 down  no-label   overlay {v6frame.i} .
 
 
