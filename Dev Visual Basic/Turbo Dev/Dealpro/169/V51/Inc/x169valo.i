/* 169-FIC 20 Ajouter une zone specif dans les auxiliaires et test en revalorisation <EVO isa 14/06/01> */
procedure devxval1 : 
    case soldes.typaux :
    when  "FOU"  then devaux-exclu = 0.
    otherwise  do: 
        if devaux-sov = string( soldes.typaux , "xxx" ) + trim( soldes.codaux ) then return . 
        
        assign devaux-exclu = 0 
               devaux-sov = string( soldes.typaux , "xxx" ) + trim( soldes.codaux ) . 
    
        regs-fileacc = "AUXILI" + soldes.typaux . 
        { regs-rec.i }
    
        for first tabdom field( ) 
                         where     tabdom.codsoc = regs-soc 
                               aND tabdom.etabli = "AUX"      
                               AND tabdom.typtab = "169"      
                               AND tabdom.prefix = soldes.typaux 
                               AND tabdom.codtab = soldes.codaux 
                               and tabdom.nombre[1] = 1  
         no-lock : 
            devaux-exclu = 1 .        
         end.

    end. 
    end case . 
end procedure . 
