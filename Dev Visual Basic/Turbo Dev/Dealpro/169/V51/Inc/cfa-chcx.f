/* LUCIE-FIC 737 OP 7778 - recherche des cde en cfa - acces a 1 cde et ses avenants <EVO bp 17/01/01> */
/* LUCBRI-FIC 9 pour les recherches graphique, mise en place d'include gerant la large <EVO bp 07/09/00> */
/*************************************************************/
/* Gestion du scroll de selection des commandes non-livrees */
/* Nom du module   cfa-chcx.f                              */
/* Version a mettre dans le specif des clients graphiques */
/*********************************************************/

&global-define browse-file entete
&global-define browse-multiple
&global-define browse-label
&global-define browse-row     01.00
&global-define browse-col     01.00
&global-define browse-height  10.00
&global-define browse-width   130
&global-define browse-display   entete.typcom ~
                                numbon FORMAT "x(8)" ~
                                top-partiel   ~
                                entete.datbon ~
                                entete.ref-tiers ~
                                entete.zon-dec-3 format ">>>,>>>,>>>.99-" ~
                                entete.totht     format ">>>,>>>,>>>.99-" ~
                                entete.devise ~
                                no-commande ~
                                entete.etabli

 { rech-bro.f }

assign  browse-title            = messprog.libel1[4]
        entete.typcom:label     = messprog.libel1[5]
        numbon:label            = messprog.libel1[6]
        top-partiel:label       = messprog.libel1[7]
        entete.datbon:label     = messprog.libel1[8]
        entete.ref-tiers:label  = messprog.libel1[9]
        entete.zon-dec-3:label  = messprog.libel1[10]
        entete.totht:label      = messprog.libel1[11]
        entete.devise:label     = messprog.libel1[12]
        no-commande:LABEL       = messprog.libel1[14]
        entete.etabli:label     = messprog.libel1[13]
        .
