/* LUCIE-FIC 414 Gestion de la devise en gestion du tarif fournisseur + mep soc regr. <EVO bp 25/05/00> */
regs-app = "ELODIE" .
{ regs-cha.i }
regs-app = "NATHALIE" .
{ regs-cha.i }

def var tabges-soc    as char format "x(2)". /* societe tables bacchus */
def var remcli-soc    as char format "x(2)". /* societe remises article*/
def var articl-soc    as char format "x(2)". /* societe fic article    */
def var magasi-soc    as char format "x(2)". /* societe fic magasi     */
def var stocks-soc    as char format "x(2)". /* societe fic stocks     */
def var tabges-etabli as char format "x(3)". /* etablis tables         */
def var tarcli-soc    as char format "x(4)". /* societe tarif fournisseur */
def var client-soc   as char format "x(2)". /* societe client          */
def var auxapr-soc   as char format "x(2)". /* societe client          */
def var fourni-soc   as char format "x(2)". /* societe fournisseur     */
def var cptgen-soc   as char format "x(2)". /* societe plan comptable  */

{ regs-inc.i "cptgen"}
cptgen-soc = regs-soc.

{ regs-inc.i "tabges" }
tabges-soc = regs-soc.

{ regs-inc.i "articl" }
articl-soc = regs-soc.

{regs-inc.i "remcli" }
remcli-soc = regs-soc.

{regs-inc.i "magasi" }
magasi-soc = regs-soc.

{regs-inc.i "stocks" }
stocks-soc = regs-soc.

{regs-inc.i "tarcli" }
tarcli-soc = regs-soc.

{regs-inc.i "auxspe" }
auxapr-soc = regs-soc.

tabges-etabli = "   " .
