/* 169-FIC 14 Include specif pour edition des releves <EVO AIR 27/10/00> */
/*- x169x120.i : Brittany FERRIES
                 Procedure RELRX120 : edition du # 97 dans rang 01
                 procedure EDIT-DOCLIB : Edition de doclib dans rang 08 -*/

/* Appele par relrx120.i qui doit activer ce programme dans le repertoire applic/client */

   
PROCEDURE relrx120 :
  Def buffer x169-tables  for tables.
  
  regs-fileacc = "AUXILI" + detrve.typaux /*aux-mvtcpt.typaux */.
  {regs-rec.i}
  
  Find First AUXILI
       Where AUXILI.codsoc = regs-soc       AND
             AUXILI.typaux = detrve.typaux /*aux-mvtcpt.typaux*/  AND
             AUXILI.codaux = detrve.codaux /*aux-mvtcpt.codaux*/
             no-lock no-error.
  If available auxili and auxili.pays <> ""
  then do:
   
          /*****************************************************/
         /* recherche de la situation associee si elle existe */
        /*****************************************************/            
            Find x169-tables where x169-tables.Codsoc = ""
                               and x169-tables.Etabli = ""
                               and x169-tables.Typtab = "PAY"
                               and x169-tables.Prefix = "CODE-PAYS"
                               and x169-tables.Codtab = auxili.pays
                               no-lock no-error.
            If available x169-tables
            Then Do:
                  { maq-majr.i "01" "97" "x169-tables.libel1[mem-langue]" } /* libelle du code pays du tiers */
            End.

   end. /* available doclib */
    
END PROCEDURE.


PROCEDURE EDIT-DOCLIB :

def input parameter rowid-mvtcpt as rowid . 

define buffer doc-mvtcpt for mvtcpt.

define var doc-a as integer.
/* define var edi-doc as char extent 20. */

find doc-mvtcpt where rowid(doc-mvtcpt) = rowid-mvtcpt no-lock no-error.
if not available doc-mvtcpt then return.

    Find First DOCLIB Where doclib.codsoc = doc-mvtcpt.codsoc          AND
                            doclib.codetb = doc-mvtcpt.codetb          AND
                            doclib.docume = doc-mvtcpt.docume
                            no-lock no-error.
        If available doclib 
        then do:
            
           
            If ma-nblig-edirel > ma-liguti
            then do :

                    { maq-edix.i "07" "edirel" }  /* report      */
                    { maq-edix.i "06" "edirel" }  /* top of form */
                     ma-nblig-edirel = 0.
                    run relr-110.p.               /* chargement entete */


                    /*- Edition de # specifique client -*/
                    If lookup ( "relrx120" , this-procedure:internal-entries ) <> 0
                    Then run relrx120.


                    { maq-edix.i "01" "edirel" }  /* edition entete*/
                    { maq-edix.i "02" "edirel" }  /* edition entete des lignes */
            End.
            
            do doc-a = 1 to extent ( doclib.libel ) :
                { maq-majm.i "08" "doc-a" "doclib.libel[doc-a]" }
            end.
           
            
        
        end.

        { maq-edix.i "08" "edirel" }

END PROCEDURE.
