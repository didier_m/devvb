/*-------------------------------------------------------*/
/* Initialisations specifiques - st-ctrl-spe.i           */
/*    <1> Fiche Employe         : Procedure init-employe */
/*    <2> Fiche Famille         : Procedure init-famille */   
/*    <3> Fiche Periode         : Procedure init-periode */
/*    <4> Memoires Interimaires : Procedure init-periemp */
/*    <5> Saisies Variables     : Procedure init-varpai  */
/*                                                       */
/* Controles specifiques - st-ctrl-spe.i                 */
/*    <1> Fiche Employe         : Procedure spe-employe  */
/*    <2> Fiche Famille         : Procedure spe-famille  */   
/*    <3> Fiche Periode         : Procedure spe-periode  */
/*    <4> Memoires Interimaires : Procedure spe-periemp  */
/*    <5> Saisies Variables     : Procedure spe-varpai   */
/*-------------------------------------------------------*/

PROCEDURE SPE-PERIODE :

  DEFINE OUTPUT PARAMETER o-rejets AS INT  NO-UNDO.
  DEFINE OUTPUT PARAMETER o-explic AS CHAR NO-UNDO.
  
  /*---------------------------------------------------------------*/
  /* Coherence Date de debut de mutuelle - Date de fin de mutuelle */
  /*---------------------------------------------------------------*/
  IF tmp-periode.spe-date3 <> ? AND tmp-periode.spe-date2 > tmp-periode.spe-date3
  THEN DO :
    ASSIGN
    o-rejets = o-rejets + 1
    o-explic = STRING (tmp-periode.fic-lig , ">>>>>9")
             + "|" 
             + "DATES DE MUTUELLE INCOHERENTES " 
             + STRING (tmp-periode.spe-date2 , "99/99/9999") 
             + " "
             + STRING (tmp-periode.spe-date3 , "99/99/9999"). 

    RETURN.
  END.

  /*--------------------*/
  /* Controle du regime */
  /*--------------------*/
  IF tmp-periode.spe-alpha7 <> "IM" AND tmp-periode.spe-alpha7 <> "RG"
  THEN DO :
    ASSIGN
    o-rejets = o-rejets + 1
    o-explic = STRING (tmp-periode.fic-lig , ">>>>>9")
             + "|" 
             + "REGIME INEXISTANT " + tmp-periode.spe-alpha7.

    RETURN.
  END.

  /*--------------------------------------*/
  /* Controle du regroupement (Navigants) */
  /*--------------------------------------*/
  IF tmp-periode.sitgeo = "NAV" OR tmp-periode.spe-alpha6 <> ""
  THEN DO :
    FIND d-tables NO-LOCK WHERE
         d-tables.ident     = sop-client             AND
         d-tables.progiciel = "SOPHIE"               AND
         d-tables.motcle    = "REGROUPEMENT"         AND 
         d-tables.typtab    = ""                     AND
         d-tables.prefix    = ""                     AND
         d-tables.codtab    = tmp-periode.spe-alpha6 NO-ERROR.

    IF NOT AVAILABLE d-tables 
    THEN DO :
      ASSIGN
      o-rejets = o-rejets + 1
      o-explic = STRING (tmp-periode.fic-lig , ">>>>>9")
               + "|" 
               + "REGROUPEMENT INEXISTANT " + tmp-periode.spe-alpha6.

      RETURN.
    END.
  END.

  /*----------------------------------------*/
  /* Controle du bareme de base (Navigants) */
  /*----------------------------------------*/
  IF tmp-periode.sitgeo = "NAV" OR tmp-periode.spe-alpha1 <> ""
  THEN DO :
    FIND d-tables NO-LOCK WHERE
         d-tables.ident     = sop-client             AND
         d-tables.progiciel = "SOPHIE"               AND
         d-tables.motcle    = "BAREME"               AND 
         d-tables.typtab    = ""                     AND
         d-tables.prefix    = ""                     AND
         d-tables.codtab    = tmp-periode.spe-alpha1 NO-ERROR.

    IF NOT AVAILABLE d-tables 
    THEN DO :
      ASSIGN
      o-rejets = o-rejets + 1
      o-explic = STRING (tmp-periode.fic-lig , ">>>>>9")
               + "|" 
               + "BAREME DE BASE INEXISTANT " + tmp-periode.spe-alpha1.

      RETURN.
    END.
  END.
  
  /*-------------------------------------------------*/
  /* Controle du bareme de stabilisation (Navigants) */
  /*-------------------------------------------------*/
  IF tmp-periode.sitgeo = "NAV" AND tmp-periode.spe-alpha2 <> ""
  THEN DO :
    FIND d-tables NO-LOCK WHERE
         d-tables.ident     = sop-client             AND
         d-tables.progiciel = "SOPHIE"               AND
         d-tables.motcle    = "BAREME"               AND 
         d-tables.typtab    = ""                     AND
         d-tables.prefix    = ""                     AND
         d-tables.codtab    = tmp-periode.spe-alpha2 NO-ERROR.

    IF NOT AVAILABLE d-tables 
    THEN DO :
      ASSIGN
      o-rejets = o-rejets + 1
      o-explic = STRING (tmp-periode.fic-lig , ">>>>>9")
               + "|" 
               + "BAREME DE STABILISATION INEXISTANT " + tmp-periode.spe-alpha2.

      RETURN.
    END.
  END.
  
  /*-------------------------------------------*/
  /* Controle du quartier maritime (Navigants) */
  /*-------------------------------------------*/
  IF tmp-periode.spe-alpha3 <> ""
  THEN DO :
    FIND d-tables NO-LOCK WHERE
         d-tables.ident     = sop-client             AND
         d-tables.progiciel = "SOPHIE"               AND
         d-tables.motcle    = "QUARTIER"             AND 
         d-tables.typtab    = ""                     AND
         d-tables.prefix    = ""                     AND
         d-tables.codtab    = tmp-periode.spe-alpha3 NO-ERROR.

    IF NOT AVAILABLE d-tables 
    THEN DO :
      ASSIGN
      o-rejets = o-rejets + 1
      o-explic = STRING (tmp-periode.fic-lig , ">>>>>9")
               + "|" 
               + "QUARTIER MARITIME INEXISTANT " + tmp-periode.spe-alpha3.

      RETURN.
    END.
  END.
    
  /*----------------------------------------*/
  /* Controle de l'emploi DRH (Sedentaires) */
  /*----------------------------------------*/
  IF tmp-periode.spe-alpha10 <> ""
  THEN DO :
    FIND d-tables NO-LOCK WHERE
         d-tables.ident     = sop-client              AND
         d-tables.progiciel = "SOPHIE"                AND
         d-tables.motcle    = "EMPLOI"                AND 
         d-tables.typtab    = ""                      AND
         d-tables.prefix    = ""                      AND
         d-tables.codtab    = tmp-periode.spe-alpha10 NO-ERROR.

    IF NOT AVAILABLE d-tables 
    THEN DO :
      ASSIGN
      o-rejets = o-rejets + 1
      o-explic = STRING (tmp-periode.fic-lig , ">>>>>9")
               + "|" 
               + "EMPLOI DRH INEXISTANT " + tmp-periode.spe-alpha10.

      RETURN.
    END.
    ELSE DO :
      ASSIGN
      tmp-periode.coefficient = d-tables.alpha [1]
      tmp-periode.bareme      = d-tables.alpha [2]
      tmp-periode.spe-alpha11 = d-tables.alpha [3].
    END.
  END.

END PROCEDURE. /* SPE-PERIODE */
