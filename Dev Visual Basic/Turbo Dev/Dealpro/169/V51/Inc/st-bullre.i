/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 169/Sophie/inc/st-bullre.i                                                                  !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V51     7 16-10-00!Evo!Pascal Caubet  !Fin du module de ventilation des charges                              !
!V51   308 04-02-00!EVO!Louis ARTIGUEBE!Refonte du calcul bulletin                                            !
!                  !   !               !L'option Visualisation n'existe plus.                                 !
!                  !   !               !Les cas de traitement possible sont maintenant Pr�paration et Validati!
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/*--------------------------------*/
/* Regles programmees specifiques */
/*--------------------------------*/

PROCEDURE REGLES-SPECIFIQUES :

  CASE bu-za4 :

    /*----------------------------------*/
    /* OMUT Code Option de la mutuelle  */
    /*----------------------------------*/
    WHEN "OMUT" THEN bu-valele = INT (periode.optmut).

    /*---------------------------------------*/
    /* SITF Situation familiale de l'employe */
    /*---------------------------------------*/
    WHEN "SITF" 
    THEN DO :
      IF employe.fam-situation = "M" OR employe.fam-situation = "U"
      THEN bu-valele = 1.
      ELSE bu-valele = 0.
    END.

    /*-------------------------------------------------*/
    /* AN1A Anciennete de plus d'un an dans la societe */
    /*-------------------------------------------------*/
    WHEN "AN1A" 
    THEN DO :
      IF t-perbul.fin-adm - periode.date-entsoc >= 365
      THEN bu-valele = 1.
      ELSE bu-valele = 0.
    END.

    /*---------------------------------------------*/
    /* DPSF Renvoie 1 si le salarie a droit au PSF */
    /*---------------------------------------------*/
    WHEN "DPSF" 
    THEN DO :
      IF employe.fam-situation = "M" OR employe.fam-charge > 0 
      THEN bu-valele = 1.
      ELSE bu-valele = 0.
    END.

    /*-----------------------------------------------------------------------------------------------*/
    /* ANCI Renvoie le nombre d'annees d'anciennete en tenant compte des periodes de conges parental */
    /*-----------------------------------------------------------------------------------------------*/
    WHEN "ANCI"
    THEN DO :
      anciennete = t-perbul.fin-adm - periode.date-anc - bu-memcum [562].

      FIND LAST t-tabanc NO-LOCK WHERE
                t-tabanc.socanc        = t-parame.socanc                            AND
                t-tabanc.catanc        = t-catpro.catanc                            AND
                t-tabanc.conanc        = t-conven.conanc                            AND
                t-tabanc.perio-applic <= STRING (YEAR  (periode.date-anc) , "9999")
                                       + STRING (MONTH (periode.date-anc) , "99")   NO-ERROR.

      IF AVAILABLE t-tabanc 
      THEN DO a-i = 1 TO 30 :
        IF TRUNCATE (anciennete / 360 , 0) <= t-tabanc.annee [a-i] 
        THEN DO :
          bu-valele = t-tabanc.pc-anc [a-i].
          LEAVE.
        END.
      END.
    END.

    /*----------------------------------------*/
    /* AGEB Renvoie l'age au jour du paiement */
    /*----------------------------------------*/
    WHEN "AGEB" 
    THEN DO :
      bu-valele = (t-perbul.date-paiement - employe.nais-date) / 365.

      IF MONTH (employe.nais-date) = MONTH (t-perbul.date-paiement) 
      THEN bu-valele = ROUND (bu-valele , 0).
      ELSE bu-valele = ROUND (bu-valele , 2).
    END.

  END CASE.

END PROCEDURE.



/************************************************************************************************************
&PROC-SPE-INITIALISATION-1 Initialisation de la table des ventilations
&END
************************************************************************************************************/
PROCEDURE SPE-INITIALISATION-1 :

  BUFFER m-ventil:EMPTY-TEMP-TABLE.

END PROCEDURE. /* SPE-INITIALISATION-1 */


/************************************************************************************************************
&PROC-SPE-ALIMENTATION-REPARTITION Alimentation de la table des ventilations
&END
************************************************************************************************************/
PROCEDURE SPE-ALIMENTATION-REPARTITION :

  /* NB : Ce module est forcement specifique. 
          Il est plac� dans un .i pour eviter les temps de transfert du fichier de repartition a chaque ligne.

          En plus de l'alimentation du fichier de repartition, ce module peut etre utilise pour completer ou
          changer des zones de M-RESULT.

          Le type de ventilation est prevu pour les cas particuliers. 
          Les zones de ventilation fonctionnent dans le meme esprit que l'analytique. */

  IF m-result.typrub = "A" 
  THEN DO :
    FIND FIRST m-ventil EXCLUSIVE-LOCK WHERE                           
               m-ventil.typven     = "00"                 AND
               m-ventil.analyt-01  = m-result.analyt [ 1] AND
               m-ventil.analyt-02  = m-result.analyt [ 2] AND 
               m-ventil.analyt-03  = m-result.analyt [ 3] AND
               m-ventil.analyt-04  = m-result.analyt [ 4] AND
               m-ventil.analyt-05  = m-result.analyt [ 5] AND
               m-ventil.analyt-06  = m-result.analyt [ 6] AND
               m-ventil.analyt-07  = m-result.analyt [ 7] AND
               m-ventil.analyt-08  = m-result.analyt [ 8] AND
               m-ventil.analyt-09  = m-result.analyt [ 9] AND
               m-ventil.analyt-10  = m-result.analyt [10] AND
               m-ventil.ventil-01  = m-result.ventil-01   AND
               m-ventil.ventil-02  = m-result.ventil-02   AND
               m-ventil.ventil-03  = m-result.ventil-03   AND
               m-ventil.ventil-04  = m-result.ventil-04   AND
               m-ventil.ventil-05  = m-result.ventil-05   AND
               m-ventil.ventil-06  = m-result.ventil-06   AND
               m-ventil.ventil-07  = m-result.ventil-07   AND
               m-ventil.ventil-08  = m-result.ventil-08   AND
               m-ventil.ventil-09  = m-result.ventil-09   AND
               m-ventil.ventil-10  = m-result.ventil-10   NO-ERROR.

    IF NOT AVAILABLE m-ventil 
    THEN DO :
      CREATE m-ventil.

      ASSIGN
      m-ventil.typven     = "00"
      m-ventil.analyt-01  = m-result.analyt [ 1] 
      m-ventil.analyt-02  = m-result.analyt [ 2]  
      m-ventil.analyt-03  = m-result.analyt [ 3] 
      m-ventil.analyt-04  = m-result.analyt [ 4] 
      m-ventil.analyt-05  = m-result.analyt [ 5] 
      m-ventil.analyt-06  = m-result.analyt [ 6] 
      m-ventil.analyt-07  = m-result.analyt [ 7] 
      m-ventil.analyt-08  = m-result.analyt [ 8] 
      m-ventil.analyt-09  = m-result.analyt [ 9] 
      m-ventil.analyt-10  = m-result.analyt [10] 
      m-ventil.ventil-01  = m-result.ventil-01  
      m-ventil.ventil-02  = m-result.ventil-02  
      m-ventil.ventil-03  = m-result.ventil-03  
      m-ventil.ventil-04  = m-result.ventil-04  
      m-ventil.ventil-05  = m-result.ventil-05  
      m-ventil.ventil-06  = m-result.ventil-06  
      m-ventil.ventil-07  = m-result.ventil-07  
      m-ventil.ventil-08  = m-result.ventil-08  
      m-ventil.ventil-09  = m-result.ventil-09  
      m-ventil.ventil-10  = m-result.ventil-10.
    END. 

    m-ventil.brut = m-ventil.brut + m-result.b-montant [7].
  END. /* IF m-result.typrub = "A" */

END PROCEDURE. /* SPE-ALIMENTATION-REPARTITION */


/************************************************************************************************************
&PROC-SPE-VENTILATION Ventilations
&END
************************************************************************************************************/
PROCEDURE SPE-VENTILATION :

  DEF VAR a          AS INT          NO-UNDO.
  DEF VAR total-pour AS DEC          NO-UNDO.
  DEF VAR aventiler  AS DEC EXTENT 7 NO-UNDO.
  DEF VAR ventile    AS DEC EXTENT 7 NO-UNDO.

  /* Suite normale */
  retour = "". 

  /* Pas de ventilation pour les types A et S */
  IF m-result.typrub = "A" OR m-result.typrub = "S" THEN RETURN.

  /* Pas de ventilation si celle-ci existe deja */
  IF m-result.analyt [1] <> "" OR m-result.analyt [2] <> "" OR m-result.analyt [3] <> "" OR
     m-result.ventil-01  <> "" OR m-result.ventil-02  <> "" OR m-result.ventil-03  <> "" 
  THEN RETURN.

  /* Calcul des pourcentages si besoin */
  IF pourcentage-a-calculer THEN RUN spe-calcul-pourcentage.

  /* Sauvegarde des montants */
  ASSIGN
  total-pour    = 0
  aventiler [1] = m-result.b-montant [1]
  aventiler [2] = m-result.b-montant [2]
  aventiler [3] = m-result.b-montant [3]
  aventiler [4] = m-result.b-montant [4]
  aventiler [5] = m-result.b-montant [5]
  aventiler [6] = m-result.b-montant [6]
  aventiler [7] = m-result.b-montant [7].

  /* Boucle de lecture des ventilations */
  FOR EACH m-ventil : 

    ASSIGN
    ventile    = 0
    total-pour = total-pour + m-ventil.pourcentage.

    /* Derniere ligne */
    IF total-pour = 100 
    THEN DO : 
      ASSIGN
      ventile [1] = aventiler [1]
      ventile [2] = m-result.b-montant[2]
      ventile [3] = aventiler [3]
      ventile [4] = aventiler [4]
      ventile [5] = m-result.b-montant[5]
      ventile [6] = aventiler [6]
      ventile [7] = aventiler [7].
    END.
    ELSE DO :
      ASSIGN
      ventile   [1] = ROUND (m-result.b-montant [1] * m-ventil.pourcentage / 100 , 2)
      ventile   [2] =        m-result.b-montant [2]
      ventile   [3] = ROUND (m-result.b-montant [3] * m-ventil.pourcentage / 100 , 2)
      ventile   [4] = ROUND (m-result.b-montant [4] * m-ventil.pourcentage / 100 , 2)
      ventile   [5] =        m-result.b-montant [5]
      ventile   [6] = ROUND (m-result.b-montant [6] * m-ventil.pourcentage / 100 , 2)
      ventile   [7] = ROUND (m-result.b-montant [7] * m-ventil.pourcentage / 100 , 2)
      
      aventiler [1] = aventiler [1] - ventile[1]
      aventiler [2] =                 ventile[2]
      aventiler [3] = aventiler [3] - ventile[3]
      aventiler [4] = aventiler [4] - ventile[4]
      aventiler [5] =                 ventile[5]
      aventiler [6] = aventiler [6] - ventile[6]
      aventiler [7] = aventiler [7] - ventile[7].
    END.

    /* Creation de result */
    RUN create-result.

    /* Complement de result avec les zones issues de m-ventil */
    ASSIGN
    RESULT.analyt [ 1]   = m-ventil.analyt-01
    RESULT.analyt [ 2]   = m-ventil.analyt-02
    RESULT.analyt [ 3]   = m-ventil.analyt-03
    RESULT.analyt [ 4]   = m-ventil.analyt-04
    RESULT.analyt [ 5]   = m-ventil.analyt-05
    RESULT.analyt [ 6]   = m-ventil.analyt-06
    RESULT.analyt [ 7]   = m-ventil.analyt-07
    RESULT.analyt [ 8]   = m-ventil.analyt-08
    RESULT.analyt [ 9]   = m-ventil.analyt-09
    RESULT.analyt [10]   = m-ventil.analyt-10

    RESULT.ventil-01     = m-ventil.ventil-01
    RESULT.ventil-02     = m-ventil.ventil-02
    RESULT.ventil-03     = m-ventil.ventil-03
    RESULT.ventil-04     = m-ventil.ventil-04
    RESULT.ventil-05     = m-ventil.ventil-05
    RESULT.ventil-06     = m-ventil.ventil-06
    RESULT.ventil-07     = m-ventil.ventil-07
    RESULT.ventil-08     = m-ventil.ventil-08
    RESULT.ventil-09     = m-ventil.ventil-09
    RESULT.ventil-10     = m-ventil.ventil-10
 
    RESULT.b-montant [1] = ventile [1]
    RESULT.b-montant [2] = ventile [2]
    RESULT.b-montant [3] = ventile [3]
    RESULT.b-montant [4] = ventile [4]
    RESULT.b-montant [5] = ventile [5]
    RESULT.b-montant [6] = ventile [6]
    RESULT.b-montant [7] = ventile [7].
  END. /* FOR EACH m-ventil */

  retour = "NEXT".

END PROCEDURE. /* SPE-VENTILATION */


/************************************************************************************************************
&PROC-SPE-CALCUL-POURCENTAGE Calcul des pourcentages
&END
************************************************************************************************************/
PROCEDURE SPE-CALCUL-POURCENTAGE :

  DEF VAR ligne      AS INT NO-UNDO.
  DEF VAR nb-ligne   AS INT NO-UNDO.
  DEF VAR total-brut AS DEC NO-UNDO.
  DEF VAR total-pour AS DEC NO-UNDO.

  ASSIGN
  ligne      = 0
  nb-ligne   = 0
  total-brut = 0
  total-pour = 0.

  FOR EACH m-ventil : 
    IF m-ventil.brut <> 0 
    THEN DO :
      ASSIGN
      nb-ligne   = nb-ligne   + 1
      total-brut = total-brut + brut.
    END.
    ELSE DELETE m-ventil.
  END.

  IF nb-ligne <> 0 
  THEN DO :
    FOR EACH m-ventil :
      ligne = ligne + 1.
      
      /* Derniere ligne */
      IF ligne = nb-ligne 
      THEN m-ventil.pourcentage = 100 - total-pour.
      ELSE DO :
        ASSIGN
        m-ventil.pourcentage = m-ventil.brut / total-brut * 100
        total-pour           = total-pour + m-ventil.pourcentage.
      END.
    END.
  END.

  pourcentage-a-calculer = NO.

END PROCEDURE. /* SPE-CALCUL-POURCENTAGE */
