/************************************************************************************************************
&PROC-INIT-VENTILS
&END
************************************************************************************************************/
PROCEDURE INIT-VENTILS :
  
  IF periode.sitgeo = "SED" 
  THEN DEAL-ATTRIBUT ("folder#ventil" , "_sensitive=no").
  ELSE DO : 
    DEAL-ATTRIBUT ("folder#ventil" , "_sensitive=yes").

    IF gfic-create THEN DEAL-PUT-SCREEN ("ventil-02" , periode.spe-alpha1).
  END.

END PROCEDURE. /* INIT-VENTILS */


/************************************************************************************************************
&PROC-LOCAL-NAVIRE
&END
************************************************************************************************************/
PROCEDURE LOCAL-NAVIRE :
  DEFINE INPUT PARAMETER i-Event  AS CHAR NO-UNDO. 
  DEFINE INPUT PARAMETER i-Motcle AS CHAR NO-UNDO.
  DEFINE INPUT PARAMETER i-Param  AS CHAR NO-UNDO.

  CASE i-Event :
    WHEN "GET"
    THEN DO :
      
      CASE i-motcle :
        WHEN "ventil-01" 
        THEN DO :
          DEAL-PUT-SCREEN ( "ventil-09" , DEAL-GET-SCREEN ( "ventil-01" )).

          IF DEAL-GET-SCREEN ( "ventil-01" ) <> "" AND DEAL-GET-SCREEN ( "ventil-02" ) <> ""
          THEN RUN AFFICHAGE ("CATEGORIE").
        END.
      END CASE.

    END.
  END CASE. 

  RETURN "".

END PROCEDURE. /* LOCAL-NAVIRE */


/************************************************************************************************************
&PROC-LOCAL-BAREME-BRIT
&END
************************************************************************************************************/
PROCEDURE LOCAL-BAREME-BRIT :
  DEFINE INPUT PARAMETER i-Event  AS CHAR NO-UNDO. 
  DEFINE INPUT PARAMETER i-Motcle AS CHAR NO-UNDO.
  DEFINE INPUT PARAMETER i-Param  AS CHAR NO-UNDO.

  CASE i-Event :
    WHEN "GET"
    THEN DO :
      RUN AFFICHAGE ("SECTEUR").

      IF DEAL-GET-SCREEN ( "ventil-01" ) <> "" AND DEAL-GET-SCREEN ( "ventil-02" ) <> ""
      THEN RUN AFFICHAGE ("CATEGORIE").
    END.
  END CASE. 

  RETURN "".

END PROCEDURE. /* LOCAL-BAREME-BRIT */


/************************************************************************************************************
&PROC-LOCAL-SITEMB
&END
************************************************************************************************************/
PROCEDURE LOCAL-SITEMB :
  DEFINE INPUT PARAMETER i-Event  AS CHAR NO-UNDO. 
  DEFINE INPUT PARAMETER i-Motcle AS CHAR NO-UNDO.
  DEFINE INPUT PARAMETER i-Param  AS CHAR NO-UNDO.

  CASE i-Event :

    WHEN "GET" THEN RUN AFFICHAGE ("DECOMPTE").

  END CASE.

  IF DEAL-GET-SCREEN ( "ventil-06" ) = "" THEN RETURN "GO".

  RETURN "".

END PROCEDURE. /* LOCAL-SITEMB */


/************************************************************************************************************
&PROC-AFFICHAGE
&END
************************************************************************************************************/
PROCEDURE AFFICHAGE :
  DEFINE INPUT PARAMETER motcle AS CHAR NO-UNDO.

  CASE motcle :
    WHEN "SECTEUR" 
    THEN DO :
      FIND d-tables NO-LOCK WHERE
           d-tables.ident     = glo-ident                       AND
           d-tables.progiciel = "SOPHIE"                        AND
           d-tables.motcle    = "BAREME"                        AND
           d-tables.typtab    = ""                              AND
           d-tables.prefix    = ""                              AND
           d-tables.codtab    = DEAL-GET-SCREEN ( "ventil-02" ) NO-ERROR.

      IF AVAILABLE d-tables 
      THEN DO :
        DEAL-PUT-SCREEN ("ventil-03" , d-tables.alpha [3]).
        DEAL-PUT-SCREEN ("ventil-04" , d-tables.alpha [4]).
      END.
    END.

    WHEN "DECOMPTE"
    THEN DO :
      FIND d-tables NO-LOCK WHERE
           d-tables.ident     = glo-ident                       AND
           d-tables.progiciel = "SOPHIE"                        AND
           d-tables.motcle    = "POSITION"                      AND
           d-tables.typtab    = ""                              AND
           d-tables.prefix    = DEAL-GET-SCREEN ( "ventil-05" ) AND
           d-tables.codtab    = DEAL-GET-SCREEN ( "ventil-06" ) NO-ERROR.

      IF AVAILABLE d-tables THEN DEAL-PUT-SCREEN ( "ventil-07" , d-tables.alpha [6]).
    END.

    WHEN "CATEGORIE" 
    THEN DO :
      FIND d-tables NO-LOCK WHERE
           d-tables.ident     = glo-ident                       AND
           d-tables.progiciel = "SOPHIE"                        AND
           d-tables.motcle    = "BARNAV"                        AND
           d-tables.typtab    = ""                              AND
           d-tables.prefix    = DEAL-GET-SCREEN ( "ventil-02" ) AND
           d-tables.codtab    = DEAL-GET-SCREEN ( "ventil-01" ) NO-ERROR.

      IF AVAILABLE d-tables THEN DEAL-PUT-SCREEN ( "ventil-08" , d-tables.alpha [1]).
    END.
  END CASE.

END PROCEDURE. /* AFFICHAGE */
