/* 169-FIC 4 Creation de tris specifs pour balance agee depuis AUXSPE <NEW air 23/08/00> */
/* YMAU-FIC 3 Ajouter tri sur codes specifiques tiers (code agent + secteur) <EVO YO 16/08/99> */
/* TAT-FIC 19 Balance age ajout des selections <EVO isa 06/07/99> */
/* TAT-FIC 9 Balance AGEE - Creation des clespe suivantes <EVO isa 12/03/99> */
 /* balance agee detail - libelle du specif */       
case age-axe : 
    otherwise assign edit[ 4  ] = entry(1 , temp-detail.zone-spe, "|" )
                     edit[ 21 ] = entry(1,  temp-detail.zone-lib, "|" ) no-error .
end case .    
