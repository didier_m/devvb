/* 169-FIC 3 Interface pour la fiche spe auxiliaire <NEW mu 18/08/00> */
/* X169-FIC */
find x-spe where x-spe.evenement = parint.evenement and
                 x-spe.s-evenement = parint.s-evene and
                 x-spe.prefix = "SPECIF"            and
                 x-spe.codsoc = "P-L"               and
                 x-spe.etabli = ""                  and
                 x-spe.codint = parsai-cle
            exclusive-lock no-error .
            if available x-spe then
            assign  tnt-pos = x-spe.ascii-pos
                    tnt-lgr = x-spe.ascii-lgr
                    tnt-nomint = x-spe.codtab
                    tnt-def    = x-spe.libel.
