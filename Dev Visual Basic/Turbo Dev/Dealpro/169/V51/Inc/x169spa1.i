/* 169-FIC 4 Creation de tris specifs pour balance agee depuis AUXSPE <NEW air 23/08/00> */
/* procedure selection mini et maxi sur la fiche specif BRITANY */ 

procedure saisie: 

def var a-mini as int.
def var a-maxi as int.
def var a-typtab as char . 
def var a-prefix as char . 

def var a      as int.

def var mem-lig as int.
def var trav    as char.

if age-mini[ ecr-choix ] = "MULTI"
then do:
        age-mini[ ecr-choix ] = "".
        age-maxi[ ecr-choix ] = "".
        case ecr-choix : 
            when 07 then age-multi-qte = "" .
            when 08 then age-multi-spe = "" . 
            when 09 then age-multi-09  = "" . 
            when 10 then age-multi-10  = "" . 
            when 11 then age-multi-11  = "" . 
            when 12 then age-multi-12  = "" . 
            when 13 then age-multi-13  = "" . 
            when 14 then age-multi-14  = "" .
            when 15 then age-multi-15  = "" .  
        end case .     
        display age-mini[ ecr-choix] no-label
                        with frame fr-age-mini.
        display age-maxi[ ecr-choix] no-label
                        with frame fr-age-maxi.
end.

        case ecr-choix : 
            when 07 then .
            when 08 then . 
            when 09 then assign a-typtab = "169" a-prefix = "BUDGET" . 
            when 10 then assign a-typtab = "169" a-prefix = "CODEPOSTAL" . 
            when 11 then . 
            when 12 then . 
            when 13 then . 
            when 14 then .
            when 15 then .  
        end case .     

ETI-mini:
repeat on endkey undo , return :
        app-nomzone = a-prefix .
    	   message messprog.libel1[9] + messprog.libel1[11].

        message color normal messprog.libel1[9] + "  -  " messprog.libel1[11].
        
        update age-mini[ ecr-choix ] no-labels
                go-on( Return {fr.i} ) with frame fr-age-mini.

        if keyfunction( lastkey ) = "find"
        then do :
                
                assign 
                codsoc = "" 
                etabli = "" 
                typtab = a-typtab
                prefix = a-prefix 
                libel = age-mini [ecr-choix].
                {run.i tabrech.p}

                if codtab = "$rien$" then undo , retry.
                age-mini[ecr-choix] = codtab.
        end.

        display age-mini[ ecr-choix] no-label with frame fr-age-mini.
        /* test si egalite */
        if age-tri[ ecr-choix] = "1" or
           age-tri[ ecr-choix] = "6"
        then do:
                age-maxi[ ecr-choix ] = age-mini[ ecr-choix ] .
                display age-maxi[ ecr-choix] no-label
                with frame fr-age-maxi.
                leave.
        end.

        if age-maxi[ ecr-choix] = ""
        then do:
                age-maxi[ ecr-choix ] = age-mini[ ecr-choix ] .
                display age-maxi[ ecr-choix] no-label
                with frame fr-age-maxi.
        end.

ETI-maxi:
repeat on endkey undo , return . 
        if age-tri[ ecr-choix ] = "1" or age-tri[ecr-choix] = "5" then leave.

        message messprog.libel1[9] + messprog.libel1[11].
        update age-maxi[ ecr-choix ] no-labels
                go-on( Return {fr.i} ) with frame fr-age-maxi.


        if keyfunction( lastkey ) = "find"
        then do :
                
                assign 
                codsoc = "" 
                etabli = "" 
                typtab = a-typtab  
                prefix = a-prefix 
                libel = age-maxi [ecr-choix].
                {run.i tabrech.p}

                if codtab = "$rien$" then undo , retry.
                age-maxi [ecr-choix] = codtab.
        end.


        if age-maxi[ecr-choix]  < age-mini[ecr-choix] then undo , retry.
        display age-maxi[ ecr-choix] no-label with frame fr-age-maxi.
        leave eti-mini.
end. /* end  eti-mini */
end. /* end  eti-maxi */
 
 
end procedure .  
