/* 169-FIC 3 Interface pour la fiche spe auxiliaire <NEW mu 18/08/00> */
/* X169-FIC */
if not available x-spe
        then do:
            create x-spe .
            assign
                x-spe.evenement = parint.evenement
                x-spe.s-evenement = parint.s-evene
                x-spe.prefix = "SPECIF"
                x-spe.codsoc = "P-L"
                x-spe.etabli = ""
                x-spe.codint = parsai-cle.
         end.
         {majmoucb.i "x-spe" }
         assign  x-spe.ascii-pos = tnt-pos
                 x-spe.ascii-lgr = tnt-lgr
                 x-spe.codtab  = tnt-nomint
                 x-spe.libel   = tnt-def .
