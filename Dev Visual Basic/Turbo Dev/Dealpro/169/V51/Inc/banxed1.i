/* 169-FIC 21 Brancher les engagements dans la balance analytique <EVO isa 23/07/01> */
/* V423ELO-FIC 325 Brancher les engagements ( SOLBUD/ENG) dans la balance Analytique <EVO isa 23/07/01> */
/* V423ELO-FIC 179 ajout d'un specif pour 498 <EVO yo 18/05/01> */
/* V423ELO-FIC 71 Chargement dans une variable du scenario pour traitement SPECIF <EVO isa 22/03/01> */
/* V422ELO-FIC 639 Balance analytique - NOUVELLE OPTION : edition derniere rupture en COL <EVO isa 18/12/00> */
/* V422ELO-FIC 624 Ajout le branchement d'un specif dans l'edition des balances <EVO isa 08/12/00> */
/* V42ELO-FIC 519 Include specif permettant de faire des calculs avant export <CLI isa 02/12/99> */
/* V42ELO-FIC 518 Acces au nouveau budget par les balances analytiques + Specif export <NEW isa 02/12/99> */
/* SPE-CLIENT */ 
/* permet de perturber ban-export [1 a 65] */ 
/* exemple recalcul des ecarts et % dans les memes devises {xyyyban1.i} */
/* exemple client 489   {x489ban1.i}  */ 
/* exemple client 504  {x504ban1.i} */ 
/* exemple client 196  {x196ban1.i}  */
/* exemple client 498  {x498ban1.i}  */
/* exemple client x169 */ {x169ban1.i} 
