/*------------------------------------------------*/
/* nt-webexp-bat.p - Lancementen batch extraction */
/*                   Lucie pour le Web            */
/*------------------------------------------------*/
/* groupe sert a definir le type de ls avec groupe ou pas */
{ connect.i new }

output to value ( os-getenv("APPLIC") + "/log/nt-webexp.log" ) .

/* ouverture des bases sur l'application 1 */
/*run boot-cnx.p ( 1 ).*/

/* run du traitement */                       
run nt-webexp.p.

output close .
quit.

