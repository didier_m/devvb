/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/nathalie/src/nt-webdeb.p                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  4887 22-04-03!Evo!bp             !les pieces envoy�s sur le web doivent etre visible par tous les signat!
!V52  4798 11-04-03!Bug!bp             !la zone contenenat le fichier scan n'est pas remis a blanc            !
!V52  4513 14-03-03!New!bp             !Dans le cas de g�n�ration d'une r�ception, suppression du fichier de v!
!V52  4460 11-03-03!New!bp             !mise en place du module d'echange avec le web pour Eurogem            !
!V52  4114 05-02-03!New!bp             !Mise en place retour critere qualite commande et r�ception            !
!V52  3910 15-01-03!New!bp             !Pouvoir acceder au enregistrement de d-dictab sans utiliser la fonctio!
!V52  3271 21-11-02!New!bp             !Retour modifications �ffectu�es chez le client 560                    !
!V52  3212 18-11-02!New!bp             !Mise a jour des modif eff�ctu�es chez le client 560                   !
!V52  3147 12-11-02!New!bp             !Mise en place d'un debbuger quand gestion en batch                    !
!V52  2940 24-10-02!New!Bernard PUISSAN!pb compile                                                            !
!V52  2788 11-10-02!New!Bernard PUISSAN!Modif sur traitements web                                             !
!-------------------------------------------------------------------------------------------------------------!
!                             I N C L U D E S                !                    T A B L E S                 !
!------------------------------------------------------------+------------------------------------------------!
!connect.i             !                                     !tabges                                          !
!compana.i             !NEW                                  !BONLIG                                          !
!d-functv42.i          !                                     !BONENT                                          !
!n-xxxdfbi.i           !                                     !BONENS                                          !
!nt-webrcf.i           !                                     !BONLGS                                          !
!nt-webdeb.i           !                                     !proglib                                         !
!nt-webrqt.i           !                                     !                                                !
!nt-websig.i           !                                     !                                                !
!nt-webgen.pp          !                                     !                                                !
!n-genmail.pp          !                                     !                                                !
!n-get-dictab.pp       !                                     !                                                !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
!-------------------------------------------------------------------------------------------------------------!
!                                                  P R O C E D U R E S                                        !
!-------------------------------------------------------------------------------------------------------------!
!DEL-BON               !                                                                                      !
&End
**************************************************************************************************************/
/*----------------------------------------------------------------------------*/
/*                           N T - W E B D E B . P                            */
/*  Generation des Demandes d'Achat a partir de fichiers crees la saisie WEB  */
/*----------------------------------------------------------------------------*/

DEF STREAM fo. 


IF session:BATCH = YES THEN OUTPUT TO "nt-webdeb.log".

/* OUTPUT TO "nt-webdeb.log". */

MESSAGE "Debut nt-webdeb.p le " STRING(TODAY,"99/99/9999") STRING(TIME,"hh:mm:ss").

/* { d-brokdeal.i }  */


{ connect.i }                                                    
{ compana.i NEW }   /* pour gestion des segements analytiques */ 
{  d-functv42.i }    

DEF VAR broker-trav AS CHAR.                                     
DEF new SHARED var glo-codsoc as char no-undo .                  
DEF new SHARED var glo-etabli as char no-undo .                  

def            buffer B-TYPBON  for  TYPBON .
def new shared buffer B-BONENT  for  BONENT .

def new shared buffer B-BONLIG  for  BONLIG .
def new shared buffer C-BONLIG  for  BONLIG .
def new shared buffer B-ARTICL  for  ARTICL .
def new shared buffer B-bonens  for  bonens .
def new shared buffer B-BONLGS  for  BONLGS .
DEF            BUFFER hab-tabges FOR tabges .
DEF            BUFFER z-tabges FOR tabges .

def new shared var    cde-rang    as int            no-undo .

def stream kan-rep .
def stream kan-fic .
def stream kan-out .
def stream kan-sup .


{ n-xxxdfbi.i }

DEF VAR temps AS DEC NO-UNDO .

def var sav-zch        as char   extent 10          no-undo .
def var x-codsoc       as char                      no-undo .
def var x-codetb       as char                      no-undo .
DEF VAR x-ligne        AS CHAR                      NO-UNDO .
def var i-err          as int                       no-undo .

def var ligne          as char  format "x(60)"      no-undo .

def var fichier-lec    as char  format "x(90)"      no-undo .
def var fichier-ren    as char  format "x(90)"      no-undo .
def var repert-ecr     as char  format "x(90)"      no-undo .
def var repert-lec     as char  format "x(90)"      no-undo .
def var repert-mail    as char  format "x(90)"      no-undo .
def var repert-ren     as char  format "x(90)"      no-undo .

def var zone-io-e      as char                      no-undo .
def var zone-io-l      as char                      no-undo .
def var car-sep        as char                      no-undo .
DEF VAR car-sep2       AS CHAR                      NO-UNDO .
DEF VAR dir-sep        AS CHAR                      NO-UNDO .

def var x-typtrt       as char                      no-undo .
def var x-auteur       as char                      no-undo .
def var x-motcle       like bonent.motcle           no-undo .
def var y-motcle       like bonent.motcle           no-undo .
DEF VAR y-typimp       AS CHAR                      NO-UNDO .
def var x-typcom       like bonent.typcom           no-undo .
def var x-numbon       like bonent.numbon           no-undo .
def var x-bonref       as char                      no-undo .
def var x-devise       as char                      no-undo .
def var x-action       as char                      no-undo .
def var x-typaux       like auxges.typaux           no-undo .
def var x-codaux       like auxges.codaux           no-undo .
def var x-typaux-liv   like auxges.typaux           no-undo .
def var x-codaux-liv   like auxges.codaux           no-undo .
def var x-magasin      like bonent.magasin          no-undo .
def var x-articl       like bonlig.articl           no-undo .
DEF VAR x-demandeur    AS CHAR                      NO-UNDO .
DEF VAR y-demandeur    AS CHAR                      NO-UNDO .
def var typtva-exo     like bonent.ttva             no-undo .
def var codtva-exo     like bonlig.ctax             no-undo .
def var nom-client     as char                      no-undo .
def var radic-appui    as char                      no-undo .

def var i-param        as char                      no-undo .
def var i-param2       as char                      no-undo .
def var i-erreur       as char                      no-undo .
def var o-param        as char                      no-undo .
def var mat-work       as char   extent 10          no-undo .

def var tarfou-soc     as char                      no-undo .
def var tarcli-soc     as char                      no-undo .
def var auxges-soc     as char                      no-undo .
def var articl-soc     as char                      no-undo .
def var magasi-soc     as char                      no-undo .
def var impapr-soc     as char                      no-undo .
def var habili-soc     as char                      no-undo .

def var ent-rowid      as rowid                     no-undo .
def var lig-rowid      as rowid                     no-undo .
def var art-rowid      as rowid                     no-undo .
def var x-tietar       as char                      no-undo .
def var clifac-tar     as log                       no-undo .
def var tar-specif     as char                      no-undo .
def var rem-specif     as char                      no-undo .

def var tx-dev         as dec                       no-undo .
def var dev-gescom     as char                      no-undo .
DEF VAR compte-cg      AS CHAR                      NO-UNDO .
DEF VAR compte-tv      AS CHAR                      NO-UNDO .
def var nb-lus         as int                       no-undo .
def var x-x            as char                      no-undo .
def var x-y            as char                      no-undo .
def var x-z            as char                      no-undo .
def var ind-qte        as int                       no-undo .
def var x-pas          like bonlig.chrono           no-undo .
def var x-chrono       like bonlig.chrono           no-undo .

def var i-i            as int                       no-undo .
def var j-j            as int                       no-undo .
def var k-k            as int                       no-undo .
def var l-l            as int                       no-undo .
def var m-m            as int                       no-undo .

def var y-y            as int                       no-undo .
def var z-z            as int                       no-undo .
def var r-r            as dec                       no-undo .
def var sav-datech     as date                      no-undo .
def var code-remise    as char                      no-undo .
def var tx-escagi      as dec                       no-undo .
def var taux-remise    as dec                       no-undo .
def var clitar         as char                      no-undo .
def var arguments      as char                      no-undo .

DEF VAR filiere        AS CHAR                      NO-UNDO .
DEF VAR aff-codsoc     AS CHAR                      NO-UNDO .
DEF VAR aff-etabli     AS CHAR                      NO-UNDO .
DEF VAR ctrl-filiere   AS LOG                       NO-UNDO .
DEF VAR ctrl-message   AS CHAR                      NO-UNDO .
def var param-ana      as char                      no-undo .
DEF VAR typcom-filiere AS CHAR                      NO-UNDO .
DEF VAR affaire        AS CHAR                      NO-UNDO .
DEF VAR inc-auto       AS CHAR                      NO-UNDO .
DEF VAR increment      AS INT                       NO-UNDO .
DEF VAR nombre         AS INT                       NO-UNDO .

def var i-par          as char               no-undo .
def var o-par          as char               no-undo .
def var cle-habili     as char               no-undo .
def var cle-mottyp     as char               no-undo .
def var nb-habili      as int                no-undo .

def var ges-operat     as char               no-undo .
DEF VAR fou-ok         AS LOG                NO-UNDO .
DEF VAR commande-gen   AS CHAR               NO-UNDO .
DEF VAR commande-vct   AS CHAR               NO-UNDO .

DEF VAR operat-mini    AS CHARACTER        NO-UNDO.
DEF VAR operat-maxi    AS CHARACTER        NO-UNDO.
DEF VAR typcom-mini    AS CHARACTER        NO-UNDO. 
DEF VAR typcom-maxi    AS CHARACTER        NO-UNDO. 
DEF VAR fourni-mini    AS CHARACTER        NO-UNDO. 
def var fourni-maxi    AS CHARACTER        NO-UNDO. 
DEF VAR articl-mini    AS CHARACTER        NO-UNDO. 
DEF VAR articl-maxi    AS CHARACTER        NO-UNDO. 
DEF VAR numbon-mini    AS CHARACTER        NO-UNDO. 
DEF VAR numbon-maxi    AS CHARACTER        NO-UNDO. 
DEF VAR reffou-mini    AS CHARACTER        NO-UNDO. 
DEF VAR reffou-maxi    AS CHARACTER        NO-UNDO. 
DEF VAR datcre-mini    AS DATE FORMAT "99/99/9999" NO-UNDO .
DEF VAR datcre-maxi    AS DATE FORMAT "99/99/9999" NO-UNDO .

DEF VAR z-mini         AS CHAR             NO-UNDO .
DEF VAR z-maxi         AS CHAR             NO-UNDO .

def var i-from         as char             no-undo .  
def var i-to           as char             no-undo .  
def var i-copy         as char             no-undo .  
def var i-objet        as char             no-undo .  
def var i-file         as char             no-undo .  
def var i-message      as char             no-undo .  
DEF VAR opesig         AS CHAR             NO-UNDO .
DEF VAR x-opesig       AS CHAR             NO-UNDO .
def var mt-plafond     as dec   extent 9   no-undo .

DEF VAR remp-operat    AS CHAR             NO-UNDO .
DEF VAR suffixe        AS CHAR             NO-UNDO .
DEF VAR x-lgcomm       AS CHAR             NO-UNDO .
DEF VAR critere        AS CHAR             NO-UNDO .
DEF VAR nb-qualit      AS INT              NO-UNDO .
def VAR base-workflow  as char              no-undo . 

DEF VAR dbg            AS LOG  INIT NO    NO-UNDO .

def BUFFER b-proglib for proglib .

def temp-table fictmp  FIELD tmp-filiere AS char
                       FIELD tmp-typaux  AS CHAR
                       FIELD tmp-codaux  AS CHAR
                       FIELD tmp-opesig  AS CHAR
                       FIELD tmp-chrono  AS INT
                       field tmp-fichier as CHAR
                       FIELD tmp-erreur  AS CHAR
                       index cle is PRIMARY tmp-filiere tmp-typaux tmp-chrono tmp-codaux tmp-opesig .


/* zones pour generation workflow du BAP */
def var mspx           as char                      no-undo .
def var msqt           as char                      no-undo .                  
def var x-fichier      as char                      no-undo .
def var x-repert       as char                      no-undo .
DEF VAR fichier-scan   AS CHAR                      NO-UNDO .
DEF VAR repert-ini     AS CHAR                      NO-UNDO .
DEF VAR repert-cop     AS CHAR                      NO-UNDO .
DEF VAR fichier-cop    AS CHAR                      NO-UNDO .

DEF VAR numcde         AS INT       NO-UNDO .
DEF VAR chrono         AS INT       NO-UNDO .
DEF VAR vect-cde       AS CHAR      NO-UNDO .
DEF VAR vect-fac       AS CHAR      NO-UNDO .
DEF VAR qtecde         AS DEC       NO-UNDO .
DEF VAR mttcde         AS DEC       NO-UNDO .
DEF VAR qtercf         AS DEC       NO-UNDO .
DEF VAR mttrcf         AS DEC       NO-UNDO .
DEF VAR qtefac         AS DEC       NO-UNDO .
DEF VAR mttfac         AS DEC       NO-UNDO .
DEF VAR qtedfac        AS DEC       NO-UNDO .
DEF VAR mttdfac        AS DEC       NO-UNDO .
DEF VAR opegen         AS CHAR      NO-UNDO .
DEF VAR tot-mttcde     AS DEC       NO-UNDO .
DEF VAR tot-mttrcf     AS DEC       NO-UNDO .
DEF VAR tot-mttfac     AS DEC       NO-UNDO .
DEF VAR tot-mttdfac    AS DEC       NO-UNDO .

DEF VAR tot-ht         AS DEC       NO-UNDO .

DEFINE temp-table ligbon no-undo       
            FIELD clef             AS CHAR FORMAT "x(4)"
            field numcde           as char format "x(17)" 
            FIELD articl           AS CHAR FORMAT "x(10)"
            FIELD libart           AS CHAR FORMAT "x(78)"
            field devise           as char format "x(3)"
            field qte-cde          as DEC
            field mtt-cde          as DEC       
            field numrcf           as char format "x(17)"
            field qte-rcf          as DEC
            field mtt-rcf          as DEC
            field numdfac          as char format "x(17)"
            field qte-dfac         as DEC 
            field mtt-dfac         as DEC 
            field numfac           as char format "x(17)"
            field qte-fac          as DEC
            field mtt-fac          as DEC
            FIELD affaire          AS CHAR FORMAT "x(10)"
            FIELD ssaff            AS CHAR FORMAT "x(10)"
            FIELD nature           AS CHAR FORMAT "x(10)" 
            index primaire is primary clef
            .

DEF BUFFER liv-bonlig       FOR bonlig .
DEF BUFFER cde-bonlig       FOR bonlig .
DEF BUFFER fac-stadet       FOR stadet .

IF search (  "nt-webdeb.debug" ) <> ? THEN dbg = YES .

assign  x-motcle = "ACA"
        x-typtrt = "def" 
        mem-langue = 1 .

assign  car-sep  = chr(124)  /*  "|"   */
        car-sep2 = chr(164)  /*  "�"   */
        x-y        = ""
        mspx     = ">>>>>>>>>9.99-"
        msqt     = ">>>>>>>>9.999-"
        .

/*  lancement super-procedure */
/* hdl-widget = deal-run ( "nt-sp-webdeb.p" ) .         */
/* THIS-PROCEDURE:ADD-SUPER-PROCEDURE ( HDL-Widget ) .  */

DEF VAR hdl-widget AS HANDLE.
RUN nt-sp-webdeb.p PERSISTENT SET hdl-widget .
THIS-PROCEDURE:ADD-SUPER-PROCEDURE ( HDL-Widget ) .

FIND tabges WHERE tabges.codsoc = ""
            AND   tabges.etabli = ""
            AND   tabges.typtab = "PRM"  
            AND   tabges.prefix = "IMPORT-WEB"  
            AND   tabges.codtab = "DEB" .
IF NOT AVAILABLE tabges THEN
DO:
    MESSAGE "Attention, traitement impossible : Param�trage non trouv� : PRM IMPORT-WEB DEB ..." .
    RETURN .
END.

IF SUBSTR(  tabges.libel1[1] , 1 , 1 ) = "$" 
THEN  repert-lec = OS-GETENV( TRIM( substr(tabges.libel1[1] , 2 , 30 )) ) .
ELSE  repert-lec = TRIM( tabges.libel1[1] + tabges.libel1[2] ) .


IF SUBSTR(  tabges.libel1[3] , 1 , 1 ) = "$" 
THEN  repert-ren = OS-GETENV( TRIM( SUBSTR(tabges.libel1[3], 2 , 30 )) ) .
ELSE  repert-ren = TRIM( tabges.libel1[3] + tabges.libel1[4] ) .

FIND tabges WHERE tabges.codsoc = ""
            AND   tabges.etabli = ""
            AND   tabges.typtab = "PRM"  
            AND   tabges.prefix = "EXPORT-WEB"  
            AND   tabges.codtab = "DEB" .
IF NOT AVAILABLE tabges THEN
DO:
    MESSAGE "Attention, traitement impossible : Param�trage non trouv� : PRM EXPORT-WEB DEB ..." .
    RETURN .
END.   
IF SUBSTR(  tabges.libel1[1] , 1 , 1 ) = "$" 
THEN  repert-ecr  = OS-GETENV( TRIM(  substr(tabges.libel1[1] , 2 , 30 )) ) .
ELSE  repert-ecr = TRIM( tabges.libel1[1] + tabges.libel1[2] ) .

dir-sep = chr(92)   /*   "\"    */  . 
IF tabges.libel1[3] <> "" THEN dir-sep = SUBSTR( tabges.libel1[3] , 1 , 1 ) .

IF OPSYS = "unix"  THEN dir-sep = CHR(47) .

repert-lec = repert-lec + dir-sep .
repert-ren = repert-ren + dir-sep .
repert-ecr = repert-ecr + dir-sep .

IF dbg = YES THEN MESSAGE "nt-webdeb.p : Repert-lec" repert-lec .
IF dbg = YES THEN MESSAGE "nt-webdeb.p : Repert-ren" repert-ren .
IF dbg = YES THEN MESSAGE "nt-webdeb.p : Repert-ecr" repert-ecr.

FIND tabges WHERE tabges.codsoc = ""
            AND   tabges.etabli = ""
            AND   tabges.typtab = "PRM"  
            AND   tabges.prefix = "ENVOI-MAIL"  
            AND   tabges.codtab = "" .
IF NOT AVAILABLE tabges THEN
    MESSAGE "Attention, l'envoi de Mail est impossible : Param�trage non trouv� : PRM ENVOI-MAIL ..." .
if AVAILABLE tabges THEN repert-mail = TRIM( tabges.libel1[1] + tabges.libel1[2] ) .

IF SUBSTR( repert-mail, 1 ,1) = "$" THEN repert-mail = OS-GETENV( TRIM(substr(repert-mail,2,30) ) )  .

IF dbg = YES THEN MESSAGE "nt-webdeb.p : Repert-mail" repert-mail .

assign  typtva-exo = "EXO"
        codtva-exo = "EXO" .
assign  typtab = "PRM"  prefix = "INFO-TVA"  codtab = "" .

run n-chparax( output x-x ) .

if x-x <> ""  then
   assign  typtva-exo = entry( 1 , x-x , "/" )
           codtva-exo = entry( 2 , x-x , "/" ) NO-ERROR .

assign  typtab = "PRM"  prefix = "SAI-OOO"  codtab = "CLIFAC-TAR" .

run n-chparax( output x-x ) .

clifac-tar = no .

if entry( 1 , x-x , "/" ) = "O"  then  clifac-tar = yes .

FOR EACH TABGES where tabges.codsoc = ""
                and   tabges.etabli = ""
                and   tabges.typtab = "APR"
                and   tabges.prefix = "MTACH-AUTO"
                no-lock :
    i-i = int( substring( tabges.codtab , 1 , 1 ) )  no-error .
    if i-i > 0  and  i-i <= 9  then  mt-plafond[ i-i ] = tabges.nombre[ 1 ] .
END .
/*  Chargement du parametrage workflow pour calcul sur HT ou TTC  */
base-workflow = "TTC" .
FIND tabges WHERE tabges.codsoc = ""
            AND   tabges.etabli = ""
            AND   tabges.typtab = "PRM"  
            AND   tabges.prefix = "WORKFLOW"  
            AND   tabges.codtab = "BASE-MTT" 
            NO-LOCK NO-ERROR.
IF AVAILABLE tabges  AND TRIM( tabges.libel1[1] ) = "HT" then base-workflow = "HT" .

traitement:
REPEAT:
    input stream kan-rep from os-dir( repert-lec ) .

    REPEAT :

        if search (  "nt-webdeb.stop" ) <> ?  then  
        DO:
            MESSAGE "Programme termin� par d�tection du fichier nt-webdeb.stop " STRING(TODAY,"99/99/9999") STRING(TIME,"hh:mm:ss").
            input stream kan-rep close .

            os-delete value( search (  "nt-webdeb.stop" ) ) .

            LEAVE traitement.
        END.

        import stream kan-rep ligne .

        /* controle si le fichier commence bien par l-*/
        assign  x-y = "l-" 
                x-x = substring( ligne , 1 , 2 ) .
        if x-x <> x-y  then  next .

        x-x = substring( ligne , 1 , 6 ) .
        i-i = index( ligne , "." ) .
        x-auteur = entry( 1 , substring( ligne , i-i + 1 ) , "_" ) NO-ERROR .

        assign  fichier-lec = repert-lec + ligne
                fichier-ren = repert-ren + ligne .

        IF dbg = YES THEN MESSAGE "nt-webdeb.p : Lecture du fichier" fichier-lec .

        IF NUM-ENTRIES(ligne, "-" ) > 5 AND ENTRY( 6 , ligne , "-" ) = "bap" THEN x-x = "l-rcf-" NO-ERROR .

        EMPTY TEMP-TABLE fictmp .

        CASE x-x:
            WHEN "l-rcf-" THEN DO:
                IF dbg = YES THEN MESSAGE "nt-webdeb.p : Avant entree nt-webrcf.i" .
                { nt-webrcf.i }
            END.

            WHEN "l-deb-" THEN  DO: 
                 IF dbg = YES THEN MESSAGE  "nt-webdeb.p : Avant entree nt-webdeb.i" .
                { nt-webdeb.i }    
            END.  

            WHEN  "l-rqt-" THEN DO:
                IF dbg = YES THEN MESSAGE  "nt-webdeb.p : Avant entree nt-webrqt.i" .
                { nt-webrqt.i }
            END.

            OTHERWISE DO:
                IF dbg = YES THEN MESSAGE  "nt-webdeb.p : Avant entree nt-websig.i" .
                { nt-websig.i }
            END.
        END CASE.

        input stream kan-fic close .

        IF dbg = YES THEN MESSAGE  "nt-webdeb.p : Fin traitement du fichier " fichier-lec.

        IF repert-ren <> "" THEN OS-COPY value( fichier-lec ) value( fichier-ren ) .
        os-delete value( fichier-lec ) .

    END .  /*  REPEAT  */

    input stream kan-rep close .

    IF session:BATCH = NO THEN MESSAGE "nt-webdeb.p : Pause du programme avant prochain traitement"  STRING(TODAY,"99/99/9999") STRING(TIME,"hh:mm:ss").

    pause 5 .

END. /* repeat traitement */

/*  PROCEDURES  */


{ nt-webgen.pp }

{ n-genmail.pp }   /*  generation de fichier pour envoi de mail  */

{ n-get-dictab.pp }  /* acces a d-dictab */

PROCEDURE DEL-BON :

         /*  Suppression Pr�alable des lignes avant MaJ  */                                     
         FOR EACH BONLIG where bonlig.codsoc = bonent.codsoc                                  
                         and   bonlig.motcle = bonent.motcle                                  
                         and   bonlig.typcom = bonent.typcom                                  
                         and   bonlig.numbon = bonent.numbon                                  
                         exclusive-lock :                                                       
             DELETE BONLIG .                                                                    
         END .                                                                                  

         FOR EACH BONENS where bonens.codsoc = bonent.codsoc                                  
                         and   bonens.motcle = bonent.motcle                                  
                         and   bonens.typcom = bonent.typcom                                  
                         and   bonens.numbon = bonent.numbon                                  
                         exclusive-lock :                                                       
             DELETE BONENS .                                                                    
         END .                                                                                  

         FOR EACH BONLGS where bonlgs.codsoc = bonent.codsoc                                  
                         and   bonlgs.motcle = bonent.motcle                                  
                         and   bonlgs.typcom = bonent.typcom                                  
                         and   bonlgs.numbon = bonent.numbon                                  
                         exclusive-lock :                                                       
             DELETE BONLGS .                                                                    
         END .                                                                                  

         FOR EACH PROGLIB where proglib.codsoc = bonent.codsoc                                
                          and   proglib.motcle = bonent.motcle                                
                          and   proglib.etabli = ""                                             
                          and   proglib.clef   = "COMMANDE"                                     
                          and   proglib.edition = "ENT" + string( bonent.typcom ,   "xxx" ) + 
                                                          string( bonent.numbon , "x(10)" )   
                          and   proglib.chrono = 0                                              
                          exclusive-lock :                                                      
             DELETE PROGLIB .                                                                   
         END .                                                                                  

         FOR EACH PROGLIB where proglib.codsoc = bonent.codsoc                                
                          and   proglib.motcle = bonent.motcle                                
                          and   proglib.etabli = ""                                             
                          and   proglib.clef   = "COMMANDE"                                     
                          and   proglib.edition = "PIE" + string( bonent.typcom ,   "xxx" ) + 
                                                          string( bonent.numbon , "x(10)" )   
                          and   proglib.chrono = 0                                              
                          exclusive-lock :                                                      
             DELETE PROGLIB .                                                                   
         END .                                                                                  

         FOR EACH PROGLIB where proglib.codsoc = bonent.codsoc                                
                          and   proglib.motcle = bonent.motcle                                
                          and   proglib.etabli = ""                                             
                          and   proglib.edition = "LIG-TX"                                      
                          and   proglib.clef begins string( bonent.typcom ,   "xxx" ) +       
                                                    string( bonent.numbon , "x(10)" )         
                          and   proglib.chrono = 0                                              
                          exclusive-lock :                                                      
             DELETE PROGLIB .                                                                   

         END .                                                                                  

END .  /*  PROCEDURE DEL-BON  */

