/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/nathalie/inc/nt-webdeb.i                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  5709 09-07-03!Evo!bp             !Mise en place Fournisseur provisoire dans la gestion du WEB           !
!V52  5571 30-06-03!New!bp             !Export de la table des codes qauntit�s                                !
!V52  5335 12-06-03!New!bp             !specif eurogem - Il faut obligatoirement 2 signataires diff�rents     !
!V52  4891 23-04-03!Evo!bp             !dans les fichiers re�us, de temps en temps des lignes � blanc         !
!V52  4798 11-04-03!Bug!bp             !la zone contenenat le fichier scan n'est pas remis a blanc            !
!V52  4772 10-04-03!New!bp             !Mise en place workflow sur les pieces FAC                             !
!V52  4697 02-04-03!New!bp             !Pb sur le cicuit de signature : manque l'adaptation dans nt-websig.i  !
!V52  4687 01-04-03!New!bp             !M�morisation du N� de bon manuel dans bonent.ref-tiers                !
!V52  4680 31-03-03!New!bp             !pb sur remont� de l'unit� de cde                                      !
!V52  4593 21-03-03!New!bp             !Pb sur remont�e de donn�es vers le client serveur                     !
!V52  4531 17-03-03!New!bp             !pb sur signature, si operateur � le droit de tout signer              !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/*-----------------------------------------------------------------------------*/
/*           N T - W E B D E B . I  : Integration des demandes de besoins     */
/*                       E  U  R  O  G  E  M                                 */
/*--------------------------------------------------------------------------*/

REPEAT TRANSACTION :
    MESSAGE "Integration du fichier" ligne  STRING(TODAY,"99/99/9999") STRING(TIME,"hh:mm:ss").

    ASSIGN commande-gen = "" 
           commande-vct = ""
           .

    x-action = "" .
    x-action = ENTRY( 5 , ligne , "-" ) NO-ERROR.
    IF x-action <> "maj" THEN LEAVE .

    assign  nb-lus   = 0
            x-chrono = 0
            radic-appui = "ZZZZZ" .

    INPUT stream kan-fic from value( fichier-lec ) .

    /* 1ere ligne du fichier =  Ligne d'Ent�te  */
    zone-io-e = "" .
    IMPORT stream kan-fic unformatted zone-io-e .

    if entry( 1 , zone-io-e , car-sep ) <> "L-ENTETE"  then  leave .

    codsoc-soc = entry( 1 , entry( 2 , zone-io-e , car-sep ) , "-" ) NO-ERROR .
    codetb-etb = entry( 2 , entry( 2 , zone-io-e , car-sep ) , "-" ) NO-ERROR .
    x-typcom   = entry( 4 , zone-io-e , car-sep ) NO-ERROR .

    IF x-typcom <> "cdf" AND x-typcom <> "cdp" THEN LEAVE .

    affaire     =  entry( 3 , zone-io-e , car-sep ) NO-ERROR .

    x-numbon   = trim( entry( 5 , zone-io-e , car-sep ) ) NO-ERROR .
    x-numbon   = fill( " " , 8 - length( x-numbon ) ) + x-numbon .
    if trim( x-numbon ) <> ""  then  x-numbon = x-numbon + "00" .

    operat      = x-auteur .   
    x-demandeur = operat . 

    x-demandeur = ENTRY( 2 , entry( 2 , ligne , "." ) , "_" ) NO-ERROR .

    glo-codsoc = codsoc-soc .
    glo-etabli = codetb-etb .

    regs-app = "NATHALIE" .
    { regs-cha.i }
    regs-app = "ELODIE" .
    { regs-cha.i }

    { regs-inc.i  AUXSPE }
    auxges-soc = regs-soc .

    { regs-inc.i  ARTICL }
    articl-soc = regs-soc .

    { regs-inc.i  MAGASI }
    magasi-soc = regs-soc .

    { regs-inc.i  IMPAPR }
    impapr-soc = regs-soc .

    { regs-inc.i  HABILI }
    habili-soc = regs-soc .

    { regs-inc.i  TARFOU }
    tarfou-soc = regs-soc .

    { regs-inc.i  TARCLI }
    tarcli-soc = regs-soc .

    assign  typtab = "PRM"  prefix = "IMPORT-" + x-typtrt   codtab = "PARAM" .
    run n-chparax( output x-x ) .
    if x-x <> "" then
    do :
        x-magasin =      entry( 1 , x-x , "/" )    no-error .
        x-pas     = dec( entry( 2 , x-x , "/" ) )  no-error .
        ind-qte   = dec( entry( 3 , x-x , "/" ) )  no-error .
    end .
    if x-magasin = ""  then  x-magasin = "001" .
    if x-pas < 1  or  x-pas > 100  then  x-pas = 5 .
    if ind-qte <> 1  and  ind-qte <> 2  then  ind-qte = 1 .
    cde-rang = ind-qte .

    /*  Chargement de la Devise de Gestion  */
    assign  typtab = "PRM"  prefix = "TX-EUR-FRF"  codtab = "" .
    run n-chparax( output x-x ) .
    if x-x <> ""  then  dev-gescom  = entry( 1 , x-x , "/" ) NO-ERROR .
    if dev-gescom = ""  then  dev-gescom = "EUR" .

    /*  Chargement du parametrage analytique ( Pour la Gestion )  */
    assign  typtab = "PRM"  prefix = "PARAM-ANA"  codtab = x-motcle + x-typcom .
    run n-chparax( output x-x ) .
    if x-x = ""  then
    do :
        assign  typtab = "PRM"  prefix = "PARAM-ANA"  codtab = x-motcle .
        run n-chparax( output x-x ) .
        if x-x = ""  then
        do :
            assign  typtab = "PRM"  prefix = "PARAM-ANA"  codtab = "" .
            run n-chparax( output x-x ) .
        end .
    end .
    if x-x <> ""  then  param-ana = entry( 1 , x-x , "/" ) NO-ERROR . 

    run charana .

    case soet-seg[ 1 ] : 
        when 0 then assign aff-codsoc = ""
                           aff-etabli = "" .
        when 1 then assign aff-codsoc = glo-codsoc
                           aff-etabli = "" .
        when 2 then assign aff-codsoc = glo-codsoc
                           aff-etabli = glo-etabli .
    end case .

    x-typaux = ENTRY( 28 , zone-io-e , car-sep )  NO-ERROR .
    x-codaux = ENTRY( 29 , zone-io-e , car-sep )  NO-ERROR .
    if x-codaux <> ""  then
    do :
        { regs-inc.i  "AUXILI" + x-typaux }
        { cadr-aux.i  regs-soc  x-typaux  x-codaux }

    end .

    /*  Retour d'une nouvelle saisie ( x-numbon = "" )  ou  d'une Modification ( x-numbon <> "" )  */
    /*  Si Nouveau Bon :  */
    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Debut d'analyse du contenu des lignes" .
    REPEAT :
        /* phase 1 de traitement d'une nouvelle DB - analyse des lignes pour pouvoir les tri�r */

        zone-io-l = "" .
        IMPORT stream kan-fic unformatted zone-io-l .

        if entry( 1 , zone-io-l , car-sep ) <> "L-LIGNES"  then  NEXT .

        FIND articl WHERE articl.codsoc = articl-soc
                    AND   articl.articl = ENTRY( 3 , zone-io-l , car-sep )
                    NO-LOCK NO-ERROR .
        IF dbg = YES THEN MESSAGE "nt-webdeb.i : Code article " articl-soc ENTRY( 3 , zone-io-l , car-sep ) AVAILABLE articl.

        IF NOT AVAILABLE articl THEN 
        DO:
            CREATE fictmp .
            ASSIGN x-chrono = x-chrono + 1 
                   tmp-filiere = "9"    /* erreur --> code article sans fili�re */
                   tmp-chrono = x-chrono
                   tmp-fichier = zone-io-l
                   tmp-erreur  = "erreur --> code article inconnu ..."
                   .
            NEXT .
        END.

        /* recherche du code fili�re li� � l'article */
        filiere = "" .

        IF x-typcom = "cdf" THEN filiere = "1" .
        IF x-typcom = "cdp" THEN filiere = "2" .        

        ctrl-filiere = YES .

         /*  contr�le existence du  fournisseur */
        /* + chargement de x-typaux et x-codaux */

        RUN verif-fou .

        ctrl-message = "" .

        /* contr�le specifique � chaque filiere */
        IF filiere = "1" THEN 
        DO:
            IF fou-ok = NO  THEN
                ASSIGN ctrl-filiere = NO
                       ctrl-message = "Achat encadr� impossible, fournisseur inconnu ..." + 
                                      x-typaux + " " +  x-codaux 
                       .
        END.

        IF filiere = "2" THEN 
        DO:
            IF fou-ok = NO  THEN
                ASSIGN ctrl-filiere = NO
                       ctrl-message = "Achat encadr� impossible, fournisseur inconnu ..." + 
                                      x-typaux + " " + x-codaux 
                       .
        END.

        IF dbg = YES THEN MESSAGE "nt-webdeb.i : Apres controle filiere" filiere ctrl-message .

        CREATE fictmp .
        x-chrono = x-chrono + 1 .
        ASSIGN fictmp.tmp-filiere = filiere
               fictmp.tmp-typaux  = x-typaux
               fictmp.tmp-codaux  = x-codaux
               fictmp.tmp-opesig  = ""
               fictmp.tmp-chrono  = x-chrono
               fictmp.tmp-fichier = zone-io-l
               fictmp.tmp-erreur  = ctrl-message 
               .
    END.

    /*********************************************************/
    /*     G E N E R A T I O N   D E S    B O N S            */    
    /*********************************************************/
    /* peut etre envisager de charger ce parametre en debut de prog pour toutes les societ�s */
    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Phase de generation des bons par filiere - lecture fictmp" .

    x-chrono = 0 .
    typcom-filiere = "CDF,CDP" .

    assign  typtab = "PRM"  prefix = "TYPCOM"  codtab = "FILIERE" .
    run n-chparax( output x-x ) .

    IF x-x <> "" THEN
    DO i-i = 1 TO 2:
       IF ENTRY( i-i , x-x , "/") <> "" THEN ENTRY( i-i , typcom-filiere ) = ENTRY( i-i , x-x , "/") .
    END.

    x-numbon = "" . 
    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Generation bon filiere " ENTRY( 1 , typcom-filiere ) .
    FOR EACH fictmp WHERE tmp-filiere = "1" 
                    NO-LOCK :

        /* un bon pour toutes les lignes de type 1 */
        IF x-numbon = "" THEN
        do:
            assign  x-motcle = "ACC"
                    x-typcom = ENTRY( 1 , typcom-filiere )
                    .
            RUN cre-entete.

        END.

        IF trim(fictmp.tmp-codaux) <> trim(x-codaux) THEN
        DO:
            RUN cre-entete .
        END.

        RUN cre-ligne.

    END.

    x-numbon = "" .
    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Generation bon filiere " ENTRY( 2 , typcom-filiere ) .
    FOR EACH fictmp WHERE tmp-filiere = "2" 
                    NO-LOCK :

       /* un bon pour toutes les lignes de type 2 */
        IF x-numbon = "" THEN
        do:
            assign  x-motcle = "ACC"
                    x-typcom = ENTRY( 2 , typcom-filiere )
                    .
            RUN cre-entete.

        END.

        IF trim(fictmp.tmp-codaux) <> trim(x-codaux) THEN
        DO:
            RUN cre-entete .
        END.

        RUN cre-ligne.

    END.

    /* retour a l'envoyeur des lignes en erreurs */
    fou-ok = NO .
    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Generation des DEMANDES erron�es" .
    FOR EACH fictmp WHERE fictmp.tmp-filiere = "9" 
                    NO-LOCK :
        IF fou-ok = NO THEN
        DO:
            /* recherche operateur de remplacement pour retour si pb */
            assign remp-operat = x-demandeur
                   suffixe     = x-demandeur
                   .
           find first tabges where tabges.codsoc = ""            
                             AND   tabges.etabli = ""           
                             and   tabges.typtab = "APR"        
                             and   tabges.prefix = "OPEACH" 
                             and   tabges.libel1[ 2 ] = x-demandeur
                             no-lock no-error.
            if available tabges and tabges.libel2[13] <> "" then
            do:
                FIND z-tabges WHERE z-tabges.codsoc = ""            
                              AND   z-tabges.etabli = ""           
                              and   z-tabges.typtab = "APR"        
                              and   z-tabges.prefix = "OPEACH"
                              AND   z-tabges.codtab = tabges.libel2[ 13 ]
                              NO-LOCK NO-ERROR .
                IF AVAILABLE z-tabges then
                    assign remp-operat = z-tabges.codtab
                           suffixe     = z-tabges.codtab + "_" + x-demandeur
                           .    
            end.

            x-x = repert-ecr + remp-operat + dir-sep + entry( 1 , ligne , "." ) + "." + suffixe .

            x-x = lc( x-x ) .  
            message "generation de " x-x STRING(TODAY,"99/99/9999") STRING(TIME,"hh:mm:ss") .

            output stream kan-out to value ( x-x ) .

            x-ligne = zone-io-e .

            PUT stream kan-out unformatted  x-ligne  skip .
            fou-ok = YES .
        END.

        x-ligne = fictmp.tmp-fichier .
        PUT stream kan-out unformatted  x-ligne  skip .

        x-ligne = "l-erreur" + car-sep + fictmp.tmp-erreur .
        PUT stream kan-out unformatted  x-ligne  skip .

    END.
    IF fou-ok = YES  THEN input stream kan-out close .

    z-z = NUM-ENTRIES( commande-gen , car-sep ) - 1.
    IF z-z < 0  THEN z-z = 0 .

    /*******************************************************************************/
    /*  Envoi d'un message de generation des pieces � l'operateur de saisie        */
    /*******************************************************************************/
    IF z-z <> 0  THEN
    DO:
        IF dbg = YES THEN MESSAGE "nt-webdeb.i : Envoi mail de generation des bons au cr�ateur" .
        i-to = operat .

        ASSIGN i-from    = i-to
               i-copy    = ""
               i-objet   = ""
               i-file    = ""
               i-message = ""
               .
        assign  typtab = "PRM"  prefix = "OPERATEUR"    codtab = "MAIL" .
        run n-chparax( output x-x ) .
        if x-x <> "" THEN i-from = entry( 1 , x-x , "/" )    no-error .

        /*      i-objet = deal-get-dictab ( "genPIE-nat[LABEL]" , "nom" ) . */
        RUN n-get-dictab( INPUT "599,DEAL" , INPUT "LABEL" , INPUT "GENPIE-NAT" , INPUT "1" , OUTPUT i-objet ) .

       IF i-objet = "" THEN i-objet = "Traitement de votre demande " .

       /*       i-message = deal-get-dictab ( "genpie-nat[message]" , "designation" ) .   */
       RUN n-get-dictab( INPUT "599,DEAL" , INPUT "MESSAGE" , INPUT "GENPIE-NAT" , INPUT "1" , OUTPUT i-message ) .


       IF i-message = "" AND z-z = 1 THEN i-message = "A partir de votre demande, la piece suivante a �t� g�n�r�e : ".
       IF i-message = "" AND z-z <> 1 THEN i-message = "A partir de votre demande, les pieces suivantes ont �t� g�n�r�es : ".

       DO y-y = 1 TO z-z:
           i-message = i-message + CHR(10) + ENTRY( y-y , commande-vct ) .         
       END.
       IF dbg = YES THEN MESSAGE "nt-webdeb.i : Avant n-genmail : " i-from i-to i-message .

       RUN n-genmail( INPUT i-from , INPUT i-to , input i-copy ,  
                                    INPUT i-objet , INPUT i-file , INPUT i-message ) .

    END.

    /****************************************************************************************************/
    /* pour chaque piece g�n�r�e, envoie des pieces a l'op�rateur demandeaur et au prochain signataire */
    /*  + envoie d'un mail au prochain signataire                                                     */
    /*************************************************************************************************/
    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Debut phase de recalcul des bons et recherche signataires. " z-z " bons" .
    DO y-y = 1 TO z-z:
        /* calcul des differents totaux pour chaque bon g�n�r� */

        x-x = "HT,RHT,TTC,RTT,PDS,VOL,QTE" .

        i-param = ENTRY( y-y , commande-gen , car-sep ) + "|" + x-x + "|" + articl-soc .

        run n-totbon( input i-param , output o-param ) NO-ERROR . 

        ent-rowid = to-rowid( entry( y-y , commande-gen , car-sep ) ) .

        /* recherche signataire operateur / motcle */

        FIND b-bonent WHERE rowid( b-bonent ) = ent-rowid  
                    EXCLUSIVE-LOCK NO-ERROR.
        RUN hab-signat .

        IF dbg = YES  THEN 
        DO k-k = 1 TO 9 :
            MESSAGE "nt-webdeb.i : Signataire trouve " k-k b-bonent.signataire[ k-k ] .
        END.
        /* controle si tous les signataires existe encore */
        /* si existe plus remplacer par lucie + mail a operature administrateur */
        i-message = "" .
        DO k-k = 1 TO 9 :
             IF b-bonent.signataire[ k-k ] <> "" then
             do:
                 FIND TABGES where tabges.codsoc = ""                                                                        
                             and   tabges.etabli = ""                                                                        
                             and   tabges.typtab = "APR"                                                                     
                             and   tabges.prefix = "OPEACH"                                                                  
                             and   tabges.codtab = b-bonent.signataire[ k-k ]                                                
                             no-lock  no-error .  
                 IF NOT AVAILABLE tabges THEN 
                     ASSIGN b-bonent.signataire[ k-k ] = "LUC"
                            i-message = "erreur" .

             END.
        END.

        IF i-message <> "" THEN
        DO:
            ASSIGN i-from = b-bonent.opecre
                   i-copy = ""
                   i-objet = ""
                   i-file = ""
                   i-message = ""
                   .
            assign  typtab = "PRM"  prefix = "OPERATEUR"    codtab = "MAIL" .
            run n-chparax( output x-x ) .
            if x-x <> "" THEN i-from = entry( 1 , x-x , "/" )    no-error .

            assign  typtab = "PRM"  prefix = "ERREUR-MAIL"    codtab = "" .
            run n-chparax( output x-x ) .
            if x-x <> "" THEN i-to = "@" + entry( 1 , x-x , "/" )  no-error .

            /*      i-objet = deal-get-dictab ( "cirsig-nat[LABEL]" , "nom" ) . */
            RUN n-get-dictab( INPUT "599,DEAL" , INPUT "LABEL" , INPUT "CIRSIG-NAT" , INPUT "1" , OUTPUT i-objet ) .

            IF i-objet = "" THEN i-objet = "Probl�me circuit de Signature d'op�rateur : #01        " .

            i-objet = REPLACE( i-objet , "#01" , b-bonent.signataire[ 1 ] ).

             /*       i-message = deal-get-dictab ( "cirsig-nat[message]" , "designation" ) .   */
            RUN n-get-dictab( INPUT "599,DEAL" , INPUT "MESSAGE" , INPUT "CIRSIG-NAT" , INPUT "1" , OUTPUT i-message ) .

            i-message = "ATTENTION ... Vous devez v�rifier le circuit de signature de l'operateur #01 "
                                         .
            i-message = REPLACE( i-message , "#01" , b-bonent.opecre ).  

            IF dbg = YES THEN MESSAGE "nt-webdeb.i : avant n-genmail ==>" i-from i-to i-message .
            RUN n-genmail( INPUT i-from , INPUT i-to , input i-copy ,  
                           INPUT i-objet , INPUT i-file , INPUT i-message ) .

        END.

        k-k = 0 .                                                                                                       
        FOR EACH TABGES where tabges.codsoc = habili-soc                                                                
                        and   tabges.etabli = ""                                                                        
                        and   tabges.typtab = "HAB"                                                                     
                        and   tabges.prefix = "PRF-" + cle-habili                                                  
                        no-lock :                                                                                       

             l-l = int( substring( tabges.codtab , 1 , 1 ) ) .                                                           

             IF l-l < 1 OR l-l > 9 THEN NEXT .
             /*  Si l'op�rateur n'est pas renseign�  */                                                                  
             if b-bonent.signataire[ l-l ] = ""  then  next .                                                            

             /*  Si la signature est d�j� pos�e  */                                                                      
             if b-bonent.ope-signat[ l-l ] <> ""  then  next .                                                           

             /*  S'il n'y a pas de signature � poser */                                                                  
/*              if tabges.libel2[ 2 ] <> "O"  then  next . */

             /*  S'il n'y a pas de profil � signer auparavant  */                                                        
             k-k = l-l .                                                                                                 
             m-m = tabges.nombre[ 2 ] . 

             if m-m <> 0  and  b-bonent.ope-signat[ m-m ] = ""  then  k-k = m-m .                                        

             /* si c'est l'opearateur de creation qui doit signer , pose signature */
             IF b-bonent.signataire[ k-k ] = ges-operat THEN
             DO:
                 ASSIGN b-bonent.ope-signat[ k-k ]= ges-operat
                        b-bonent.dat-signat[ k-k ] = TODAY
                        k-k = 0
                        .
                 NEXT.
             END.

             leave .                                                                                                                                                                                                                             
        END .  /*  EACH TABGES  */    

        RELEASE b-bonent .

        /*  Apr�s MAJ : envoi au 1er signataire   */  
        FIND BONENT where rowid( bonent ) = ent-rowid  
                    NO-LOCK NO-ERROR.

        IF dbg = YES THEN MESSAGE "nt-webdeb.i : Apres recherche habilitation" habili-soc bonent.motcle l-l k-k .
        if k-k <> 0  then                                                                                               
        do :                                                                                                            
            FIND TABGES where tabges.codsoc = ""                                                                        
                        and   tabges.etabli = ""                                                                        
                        and   tabges.typtab = "APR"                                                                     
                        and   tabges.prefix = "OPEACH"                                                                  
                        and   tabges.codtab = bonent.signataire[ k-k ]                                                
                        no-lock  no-error .  
            IF dbg = YES  THEN MESSAGE "nt-webdeb.i : L'operateur qui signe existe-t'il ?" k-k bonent.signataire[ k-k ] AVAILABLE tabges .
            if available tabges  then                                                                                   
            do :            
                i-to = "" .
                x-x = trim( tabges.libel1[ 2 ] ) .    /*  Operateur de connection du Signataire  */  
                FIND d-operat where d-operat.ident  = "deal"
                              and   d-operat.typtab = "operat" 
                              AND   d-operat.codtab = x-x
                              NO-LOCK NO-ERROR.

                IF AVAILABLE d-operat AND TRIM( tabges.libel2[14]) = "1" THEN
                DO:
                    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Avant nt-webgen: envoi fichier signature " x-x .
                    i-to = x-x .
                    i-param = string( rowid( bonent ) ) + "|SIG" + string( k-k ) + "|" + x-x + "|" + ligne.   

                    run nt-webgen( input i-param , input i-erreur , output o-param ) .  
                END.

                /* envoi mail au prochain signataire */
                FIND TABGES where tabges.codsoc = codsoc-soc               
                            and   tabges.etabli = ""
                            and   tabges.typtab = "CHR"
                            and   tabges.prefix = "TYPCOM"
                            and   tabges.codtab = TRIM( bonent.motcle + bonent.typcom )
                            NO-LOCK NO-ERROR.

                IF AVAILABLE tabges AND tabges.nombre[3] = 1 THEN 
                DO:
                    ASSIGN i-from = bonent.opecre
                           i-copy = ""
                           i-objet = ""
                           i-file = ""
                           i-message = ""
                           .
                    assign  typtab = "PRM"  prefix = "OPERATEUR"    codtab = "MAIL" .
                    run n-chparax( output x-x ) .
                    if x-x <> "" THEN i-from = entry( 1 , x-x , "/" )    no-error .

              /*      i-objet = deal-get-dictab ( "sigsig-nat[LABEL]" , "nom" ) . */
                    RUN n-get-dictab( INPUT "599,DEAL" , INPUT "LABEL" , INPUT "SIGSIG-NAT" , INPUT "1" , OUTPUT i-objet ) .

                    IF i-objet = "" THEN i-objet = "Signature de la piece : #01 #02 #03 " .

                    i-objet = REPLACE( i-objet , "#01" , bonent.codsoc ).
                    i-objet = REPLACE( i-objet , "#02" , bonent.etabli ).
                    i-objet = REPLACE( i-objet , "#03" , bonent.typcom + " " + SUBSTR( bonent.numbon , 1 , 8 ) ) .
                    i-message = REPLACE( i-message , "#04" , bonent.signataire[1] ).  

                    FIND z-TABGES where z-tabges.codsoc = ""                                                                        
                                  and   z-tabges.etabli = ""                                                                        
                                  and   z-tabges.typtab = "APR"                                                                     
                                  and   z-tabges.prefix = "OPEACH"                                                                  
                                  and   z-tabges.codtab = bonent.signataire[ 1 ]                                                
                                  no-lock  no-error .              
                    IF AVAILABLE z-tabges THEN i-message = REPLACE( i-message , "#05" , z-tabges.libel1[1] ).  
             /*       i-message = deal-get-dictab ( "sigsig-nat[message]" , "designation" ) .   */
                    RUN n-get-dictab( INPUT "599,DEAL" , INPUT "MESSAGE" , INPUT "SIGSIG-NAT" , INPUT "1" , OUTPUT i-message ) .

                    IF i-message = "" THEN i-message = "Vous devez signer la commande #01 "
                                         .
                    i-message = REPLACE( i-message , "#01" , bonent.codsoc ).  
                    i-message = REPLACE( i-message , "#02" , bonent.etabli ).  
                    i-message = REPLACE( i-message , "#03" , bonent.typcom + " " + SUBSTR( bonent.numbon , 1 , 8 ) ) .
                    i-message = REPLACE( i-message , "#04" , bonent.signataire[1] ).  

                    FIND z-TABGES where z-tabges.codsoc = ""                                                                        
                                  and   z-tabges.etabli = ""                                                                        
                                  and   z-tabges.typtab = "APR"                                                                     
                                  and   z-tabges.prefix = "OPEACH"                                                                  
                                  and   z-tabges.codtab = bonent.signataire[ 1 ]                                                
                                  no-lock  no-error .              
                    IF AVAILABLE z-tabges THEN i-message = REPLACE( i-message , "#05" , z-tabges.libel1[1] ).  

                    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Avant n-genmail ==> " i-from i-to i-message .
                    RUN n-genmail( INPUT i-from , INPUT i-to , input i-copy ,  
                                    INPUT i-objet , INPUT i-file , INPUT i-message ) .

                END.

                IF dbg = YES THEN MESSAGE "nt-webdeb.i : Avant nt-webgen : envoi fichier genere VISU " bonent.opecre .
                x-x = bonent.opecre .    /*  Matricule   */                                    
                i-param = string( rowid( bonent ) ) + "|VIS0" + "|" + x-x + "|" + ligne .     

                run nt-webgen( input i-param , input i-erreur , output o-param ) .
            end .                                                                                                    
        end .                                                                                                           
        else  do :     

            /* commande valid�e envoi fichier operateur creation */
            FIND BONENT where rowid( bonent ) = ent-rowid  
                        EXCLUSIVE-LOCK NO-ERROR.
            bonent.stat-valide = 1 .  
            IF dbg = YES THEN MESSAGE "nt-webdeb.i : Avant nt-webgen: envoi fichier genere VALIDE " bonent.opecre .
            x-x = bonent.opecre .    /*  Matricule   */                                   
            i-param = string( rowid( bonent ) ) + "|VIS1"  + "|" + x-x + "|" + ligne .     
            run nt-webgen( input i-param , input i-erreur , output o-param ) . 

            /* envoi fichier de prereception si commande est livrable */
            FIND B-TYPBON where b-typbon.codsoc = codsoc-soc
                          and   b-typbon.motcle = bonent.motcle
                          and   b-typbon.typcom = bonent.typcom
                          no-lock  no-error .
            IF AVAILABLE b-typbon AND b-typbon.livrable = 0 AND b-typbon.typaux <> "*M" THEN
            DO:
                /* si piece livr�s en automatique, g�n�ration de la r�ception */
                FIND BONENS  where bonens.codsoc = bonent.codsoc
                             and   bonens.motcle = bonent.motcle
                             and   bonens.typcom = bonent.typcom
                             and   bonens.numbon = bonent.numbon
                             and   bonens.theme  = "INT-DBWEB"
                             and   bonens.chrono = 0
                             no-lock no-error .
                if available bonens AND bonens.alpha[2] = "1" then
                DO:
                    IF dbg = YES THEN MESSAGE "nt-webdeb.i : G�n�ration automatique de la r�ception "  .

                    RUN nt-webgen-rcf( input i-param , input i-erreur , output o-param ) .

                     IF dbg = YES THEN MESSAGE "nt-webdeb.i : Avant nt-webgen: suppression fichier sur la commande r�ceptionn�e chez " bonent.opecre .
                     x-x = bonent.opecre .    /*  Matricule   */            

                     i-param = string( rowid( bonent ) ) + "|VIS3"  + "|" + x-x + "|" + ligne .     
                     run nt-webgen( input i-param , input i-erreur , output o-param ) . 

                END.
                ELSE DO:
                    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Avant nt-webgen-liv : envoi fichier pre-reception " bonent.opecre .
                    x-x = bonent.opecre .    /*  Matricule   */                                   
                    i-param = string( rowid( bonent ) ) + "|RCF"  + "|" + x-x + "|" + ligne .     
                    run nt-webgen-liv( input i-param , input i-erreur , output o-param ) . 
                END.
            END.

            /* run n-genmail ==> envoi mail a l'opecre + demandeur  */
            FIND TABGES where tabges.codsoc = codsoc-soc               
                        and   tabges.etabli = ""
                        and   tabges.typtab = "CHR"
                        and   tabges.prefix = "TYPCOM"
                        and   tabges.codtab = TRIM( bonent.motcle + bonent.typcom )
                        NO-LOCK NO-ERROR.

            IF AVAILABLE tabges AND tabges.nombre[3] = 1 THEN 
            DO:
                ASSIGN i-from = bonent.opecre
                       i-to   = x-demandeur
                       i-copy = bonent.opecre
                       i-objet = ""
                       i-file = ""
                       i-message = ""
                       .
                assign  typtab = "PRM"  prefix = "OPERATEUR"    codtab = "MAIL" .
                run n-chparax( output x-x ) .
                if x-x <> "" THEN i-from = entry( 1 , x-x , "/" )    no-error .

           /*     i-objet = deal-get-dictab ( "valsig-nat[LABEL]" , "nom" ) . */
                RUN n-get-dictab( INPUT "599,DEAL" , INPUT "LABEL" , INPUT "VALSIG-NAT" , INPUT "1" , OUTPUT i-objet ) .

                IF i-objet = "" THEN i-objet = "Validation de la piece : #01 #02 #03 " .

                i-objet = REPLACE( i-objet , "#01" , bonent.codsoc ).
                i-objet = REPLACE( i-objet , "#02" , bonent.etabli ).
                i-objet = REPLACE( i-objet , "#03" , bonent.typcom + " " + SUBSTR( bonent.numbon , 1 , 8 ) ) .

           /*     i-message = deal-get-dictab ( "valsig-nat[message]" , "designation" ) . */
                RUN n-get-dictab( INPUT "599,DEAL" , INPUT "MESSAGE" , INPUT "VALSIG-NAT" , INPUT "1" , OUTPUT i-message ) .

                IF i-message = "" THEN i-message = "La commande suivante est valid�e #01 "
                                     .
                i-message = REPLACE( i-message , "#01" , bonent.codsoc ).  
                i-message = REPLACE( i-message , "#02" , bonent.etabli ).  
                i-message = REPLACE( i-message , "#03" , bonent.typcom + " " + SUBSTR( bonent.numbon , 1 , 8 ) ) .

                IF dbg = YES THEN MESSAGE "nt-webdeb.i : avant n-genmail ==> " i-to i-from i-message .
                RUN n-genmail( INPUT i-from , INPUT i-to , input i-copy ,  
                                    INPUT i-objet , INPUT i-file , INPUT i-message ) .

            END.

            RELEASE bonent.
        end.                                                                                                                                         
    END .    
    MESSAGE "fin traitement" .
    LEAVE .    
END.

PROCEDURE cre-entete :

    fou-ok = YES .
    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Procedure de creation des entetes " .
    REPEAT WHILE fou-ok = YES:
        i-param = x-motcle + "|" + x-typcom + "|O" .
        o-param = "".

        run n-nocom( input i-param , input-output o-param ) .

        x-numbon = o-param + "00" .
        FIND B-BONENT where b-bonent.codsoc = codsoc-soc
                      and   b-bonent.motcle = x-motcle
                      and   b-bonent.typcom = x-typcom
                      and   b-bonent.numbon = x-numbon
                      USE-INDEX primaire no-lock  no-error .

        if NOT available b-bonent THEN fou-ok = NO .
    END.

    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Procedure de creation de l' entete " codsoc-soc x-motcle x-typcom x-numbon .
    CREATE B-BONENT .
    assign  b-bonent.codsoc = codsoc-soc
            b-bonent.etabli = codetb-etb
            b-bonent.motcle = x-motcle
            b-bonent.typcom = x-typcom
            b-bonent.numbon = x-numbon .

    FIND BONENT where bonent.codsoc = codsoc-soc
                and   bonent.motcle = x-motcle
                and   bonent.typcom = x-typcom
                and   bonent.numbon = radic-appui + x-demandeur + "00"
                no-lock  no-error .
    if not available bonent  then
    do :
        FIND BONENT where bonent.codsoc = codsoc-soc
                    and   bonent.motcle = x-motcle
                    and   bonent.typcom = x-typcom
                    and   bonent.numbon = radic-appui + "000" + "00"
                    no-lock  no-error .
    end .
    if available bonent  then
    do :
        { n-duplent.i  b-bonent  bonent }
        assign  b-bonent.codaux = ""
                b-bonent.typaux = ""
                b-bonent.typaux-fac = ""
                b-bonent.codaux-fac = ""
                b-bonent.typaux-liv = ""
                b-bonent.codaux-liv = ""
                .

        do i-i = 1 to 5 :
            assign  b-bonent.adres[ i-i ]     = ""
                    b-bonent.adres-liv[ i-i ] = "" .
        end .
    end .
    FIND B-TYPBON where b-typbon.codsoc = codsoc-soc
                  and   b-typbon.motcle = x-motcle
                  and   b-typbon.typcom = x-typcom
                  no-lock  no-error .

    ASSIGN x-typaux-liv = ""
           x-codaux-liv = ""
           .

    IF b-typbon.typaux begins "*M" 
    THEN  assign  x-typaux = ""
                  x-codaux = ""
                  .              
    ELSE assign x-typaux = fictmp.tmp-typaux
                x-codaux = fictmp.tmp-codaux
                .

    x-devise = trim( entry( 27 , zone-io-e , car-sep ) ) NO-ERROR .
    IF x-devise = ""  THEN x-devise = dev-gescom .
    x-opesig = fictmp.tmp-opesig .

    if x-codaux <> ""  then
    do :
        { regs-inc.i  "AUXILI" + x-typaux }
        { cadr-aux.i  regs-soc  x-typaux  x-codaux }

        FIND AUXGES where auxges.codsoc = auxges-soc
                    and   auxges.typaux = x-typaux
                    and   auxges.codaux = x-codaux
                    NO-LOCK NO-ERROR .
        if not available auxges  then  leave .
    end .

    assign  b-bonent.nivcom     = ""
            b-bonent.typaux     = x-typaux
            b-bonent.codaux     = x-codaux
            b-bonent.etabli     = codetb-etb
            b-bonent.magasin    = x-magasin
            b-bonent.devise     = x-devise 
            b-bonent.ref-tiers  =  trim( entry( 42 , zone-io-e , car-sep ) )
            b-bonent.typbon     = "E"
            .

    /*  Taux Devise  */
    tx-dev = 1 .
    if x-devise <> dev-gescom  then
    do :
        assign  bi-codsoc = codsoc-soc  bi-codetb = codetb-etb     bi-execpt = ""    bi-percpt = ""
                bi-datpie = today       bi-devini = x-devise       bi-mtini  = 1000
                bi-motcle = "$GESCOM_" + dev-gescom .
        run out-mona( input  bi-codsoc , input  bi-codetb , input  bi-execpt ,  input  bi-percpt ,
                      input  bi-datpie , input  bi-devini , input  bi-mtini  , input  bi-motcle ,
                      output bi-mtpie  ,  output bi-mtcpt  , output bi-txpie , output tx-dev  ,
                      output bi-devcpt , output bi-devpie ,  output bi-monnaie, output bi-retour ) .
        if bi-monnaie = 0  then  tx-dev = bi-txpie .
        if dev-gescom <> bi-devcpt
        then  do :
            r-r = dec( entry( 15 , bi-retour , "_" ) ) no-error .
            if r-r <> 0  then  tx-dev = r-r / 1000 .
        end .
    end .

    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Le taux de la devise est : " tx-dev x-devise .

    b-bonent.txdev = tx-dev .

    /*  Date de Commande  */
    b-bonent.datbon = today .
    b-bonent.dattar = b-bonent.datbon .

    /*  Date de Livraison Prevue  */
    x-x = entry( 39 , zone-io-e , car-sep ) NO-ERROR .
    x-y = substring( x-x , 1 , 2 ) + substring( x-x , 4 , 2 ) +
          substring( x-x , 7 , 4 ) .
    if x-y <> ""  then
    do :
        b-bonent.datdep = date( int( substring( x-y , 3 , 2 ) ) ,
                                int( substring( x-y , 1 , 2 ) ) ,
                                int( substring( x-y , 5 , 4 ) ) )  no-error .
        if error-status:error  then  b-bonent.datdep = today .
    end .
    ELSE b-bonent.datdep = today .

    b-bonent.datliv = b-bonent.datdep .

    b-bonent.ref-magasin = x-typcom + right-trim( x-bonref ) .

    b-bonent.segana[ 1 ] = affaire .
    b-bonent.segana[ 2 ] = entry( 15 , zone-io-e , car-sep ) . /* code sous-affaire */
    b-bonent.segana[ 3 ] = entry( 16 , zone-io-e , car-sep ) . /* code nature */



    IF X-CODAUX <> ""  THEN
    DO :
        assign  b-bonent.typaux-fac     = auxges.typaux-fac
                b-bonent.codaux-fac     = auxges.codaux-fac
                b-bonent.modreg         = auxges.modreg
                b-bonent.codech         = auxges.codech
                b-bonent.domapr         = auxges.domapr
                b-bonent.ttva           = auxges.ttva
                b-bonent.codpri         = auxges.code-tarif
                b-bonent.nom-contact    = auxges.nom-respon
                b-bonent.tel-contact    = auxges.teleph-respon
                b-bonent.fax-contact    = auxges.fax-respon
                b-bonent.repres[ 1 ]    = auxges.repres[ 1 ]
                b-bonent.adres[ 1 ]     = auxges.adres[ 1 ]
                b-bonent.adres[ 2 ]     = auxges.adres[ 2 ]
                b-bonent.adres[ 3 ]     = auxges.adres[ 3 ]
                b-bonent.adres[ 4 ]     = auxges.adres[ 4 ]
                b-bonent.adres[ 5 ]     = auxges.adres[ 5 ]
                b-bonent.adres-liv[ 1 ] = auxges.adres[ 1 ]
                b-bonent.adres-liv[ 2 ] = auxges.adres[ 2 ]
                b-bonent.adres-liv[ 3 ] = auxges.adres[ 3 ]
                b-bonent.adres-liv[ 4 ] = auxges.adres[ 4 ]
                b-bonent.adres-liv[ 5 ] = auxges.adres[ 5 ] 

                .

        i-i = int( auxges.code-langue ) .
        if i-i < 1  then  i-i = 1 .
        b-bonent.code-langue = i-i .

        /*  Transport : Lecture de AUXGES ( TRANSPORT )  */
        i-param = "R|AUXGES|" + string( rowid( AUXGES ) ) + "|TRANSPORT|0" .
        run n-ficsuit( input i-param , input-output o-param ) .
        if int( entry( 1 , o-param , "|" ) ) = 0  then
        do :
            do i-i = 3 to num-entries( o-param , "|" ) :
                assign  x-z = entry ( i-i , o-param   , "|"  )
                        x-x = entry ( 1 , x-z , "="  )
                        x-y = entry ( 2 , x-z , "="  ) NO-ERROR .
                if x-x = "A01"  then  b-bonent.transp      = x-y .
                if x-x = "A02"  then  b-bonent.lieu-depart = x-y .
                if x-x = "A03"  then  b-bonent.modliv      = x-y .
                if x-x = "A04"  then  b-bonent.modtrp      = x-y .
            end .
        end .
        i-param = b-bonent.magasin + "|" + b-bonent.lieu-depart + "|" + "|" + x-motcle + "|" +
                  b-bonent.codaux + "|" + string( b-bonent.datdep , "99/99/9999" )  .
        run n-ccoutrp( input i-param , output o-param ) .
        if entry( 1 , o-param , "|" ) <> ""  then
        do :
            b-bonent.typ-mttrp = entry( 1 , o-param , "|" ) NO-ERROR .
            b-bonent.cout-trp  = dec( entry( 2 , o-param , "|" ) ) NO-ERROR .
        end .

        /*  Si suivi CEE : MaJ de AUXGES ( CEE )  */
        mat-work = "" .
        i-param = "R|AUXGES|" + string( rowid( AUXGES ) ) + "|CEE|0" .
        run n-ficsuit( input i-param , input-output o-param ) .
        if int( entry( 1 , o-param , "|" ) ) = 0  then
        do :
            do i-i = 3 to num-entries( o-param , "|" ) :
                assign  x-z = entry ( i-i , o-param   , "|"  )
                        x-x = entry ( 1 , x-z , "="  )
                        x-y = entry ( 2 , x-z , "="  ) NO-ERROR .
                if x-x = "A01"  then  mat-work[ 1 ] = x-y .
                if x-x = "A02"  then  mat-work[ 2 ] = x-y .
                if x-x = "A03"  then  mat-work[ 3 ] = x-y .
                if x-x = "A04"  then  mat-work[ 4 ] = x-y .
                if x-x = "A05"  then  mat-work[ 5 ] = x-y .
                if x-x = "A06"  then  mat-work[ 6 ] = x-y .
            end .
        end .
        if mat-work[ 2 ] = "O"  then
        do :
            assign  i-param = "U|BONENT|" + string( rowid( B-BONENT ) ) + "|CEE|0"
                    o-param = "AK="   + string( x-typaux ,   "xxx" ) +
                                        string( x-codaux , "x(10)" ) +
                              "|A01=" + mat-work[ 1 ] + "|A02=" + mat-work[ 2 ] +
                              "|A03=" + mat-work[ 3 ] + "|A04=" + mat-work[ 4 ] +
                              "|A05=" + mat-work[ 5 ] + "|A06=" + mat-work[ 6 ] .
            run n-ficsuit( input i-param , input-output o-param ) .
        end .

    END .  /*  X-CODAUX <> ""  */

    IF entry( 35 , zone-io-e , car-sep ) <> "" THEN 
            b-bonent.modliv = entry( 35 , zone-io-e , car-sep ).

    IF entry( 36 , zone-io-e , car-sep ) <> "" THEN 
            b-bonent.modreg = entry( 36 , zone-io-e , car-sep ).

    IF entry( 37 , zone-io-e , car-sep ) <> "" THEN 
            b-bonent.codech = entry( 37 , zone-io-e , car-sep ).

    IF entry( 38 , zone-io-e , car-sep ) <> "" THEN 
            b-bonent.nom-contact = entry( 38 , zone-io-e , car-sep ).

    IF entry( 39 , zone-io-e , car-sep ) <> "" THEN 
            b-bonent.modreg = entry( 36 , zone-io-e , car-sep ).

    IF entry( 40 , zone-io-e , car-sep ) <> "" THEN 
            b-bonent.lib-piefac = entry( 40 , zone-io-e , car-sep ).

    ASSIGN b-bonent.adres[ 1 ]     = entry( 30 , zone-io-e , car-sep )  
           b-bonent.adres[ 2 ]     = entry( 31 , zone-io-e , car-sep )  
           b-bonent.adres[ 3 ]     = entry( 32 , zone-io-e , car-sep )  
           b-bonent.adres[ 4 ]     = entry( 33 , zone-io-e , car-sep )  
           b-bonent.adres[ 5 ]     = entry( 34 , zone-io-e , car-sep ) .

/*     FIND FIRST AFFADL where affadl.codsoc  = aff-codsoc            */
/*                        and   affadl.etabli  = aff-etabli           */
/*                        and   affadl.affaire = b-bonent.segana[ 1 ] */
/*                        no-lock  no-error .                         */
/*     if available affadl  then                                      */
/*     do :                                                           */
/*         assign  b-bonent.num-adl        = affadl.chrono            */
/*                 b-bonent.adres-liv[ 1 ] = affadl.adres[ 1 ]        */
/*                 b-bonent.adres-liv[ 2 ] = affadl.adres[ 2 ]        */
/*                 b-bonent.adres-liv[ 3 ] = affadl.adres[ 3 ]        */
/*                 b-bonent.adres-liv[ 4 ] = affadl.adres[ 4 ]        */
/*                 b-bonent.adres-liv[ 5 ] = affadl.adres[ 5 ] .      */
/*     end .                                                          */

    assign  b-bonent.adres-liv[ 1 ] = entry(  9 , zone-io-e , car-sep )
            b-bonent.adres-liv[ 2 ] = entry( 10 , zone-io-e , car-sep )
            b-bonent.adres-liv[ 3 ] = entry( 11 , zone-io-e , car-sep ) 
            b-bonent.adres-liv[ 4 ] = entry( 12 , zone-io-e , car-sep )
            b-bonent.adres-liv[ 5 ] = entry( 13 , zone-io-e , car-sep ) .

    /* chargement adresse liv si code client saisie en filiere 1 */
    message "client livre " x-codaux-liv .
    if x-codaux-liv <> ""  then
    do :
        { regs-inc.i  "AUXILI" + x-typaux-liv }
        { cadr-aux.i  regs-soc  x-typaux-liv  x-codaux-liv }

        FIND AUXGES where auxges.codsoc = auxges-soc
                    and   auxges.typaux = x-typaux-liv
                    and   auxges.codaux = x-codaux-liv
                    NO-LOCK NO-ERROR .
        if available auxges  THEN
        DO:
            ASSIGN b-bonent.typaux-liv     = x-typaux-liv
                   b-bonent.codaux-liv     = x-codaux-liv
                   .
            IF auxges.adres[ 1 ] + auxges.adres[ 2 ] + auxges.adres[ 3 ] +
               auxges.adres[ 4 ] + auxges.adres[ 5 ] <> "" THEN
                ASSIGN b-bonent.adres-liv[ 1 ] = auxges.adres[ 1 ]   
                       b-bonent.adres-liv[ 2 ] = auxges.adres[ 2 ]   
                       b-bonent.adres-liv[ 3 ] = auxges.adres[ 3 ]   
                       b-bonent.adres-liv[ 4 ] = auxges.adres[ 4 ]   
                       b-bonent.adres-liv[ 5 ] = auxges.adres[ 5 ]   
                       .    
         END.
         IF NUM-ENTRIES(fictmp.tmp-fichier) > 33 THEN
         DO:
            IF  trim( entry( 30 , fictmp.tmp-fichier , car-sep ) ) <> "" THEN
                b-bonent.adres-liv[ 1 ] = trim( entry( 30 , fictmp.tmp-fichier , car-sep ) ).
            IF  trim( entry( 31 , fictmp.tmp-fichier , car-sep ) ) <> "" THEN
                b-bonent.adres-liv[ 2 ] = trim( entry( 31 , fictmp.tmp-fichier , car-sep ) ).
            IF  trim( entry( 32 , fictmp.tmp-fichier , car-sep ) ) <> "" THEN
                b-bonent.adres-liv[ 3 ] = trim( entry( 32 , fictmp.tmp-fichier , car-sep ) ).
            IF  trim( entry( 33 , fictmp.tmp-fichier , car-sep ) ) <> "" THEN
                b-bonent.adres-liv[ 4 ] = trim( entry( 33 , fictmp.tmp-fichier , car-sep ) ).
            IF  trim( entry( 34 , fictmp.tmp-fichier , car-sep ) ) <> "" THEN
                b-bonent.adres-liv[ 5 ] = trim( entry( 34 , fictmp.tmp-fichier , car-sep ) ).
         END.
    end .

    /*  Lignes de Texte d'Ent�te  */
    if entry( 41 , zone-io-e , car-sep ) <> ""  then
    DO :
        i-par = x-motcle + "|" + codsoc-soc + "|" + "" + "|" + "COMMANDE" + "|" + 
                "ENT" + x-typcom  + x-numbon . 
        o-par = entry( 41 , zone-io-e , car-sep ) .
        o-par = REPLACE( o-par , "<BR>" , CHR(10) ).
        run n-majprgl( input i-par , input o-par ) .

    END . 

    /*  Lignes de Texte de pied  */
    if entry( 14 , zone-io-e , car-sep ) <> ""  then
    DO :
        i-par = x-motcle + "|" + codsoc-soc + "|" + "" + "|" + "COMMANDE" + "|" + 
                "PIE" + x-typcom  + x-numbon . 
        o-par = entry( 14 , zone-io-e , car-sep ) .
        o-par = REPLACE( o-par , "<BR>" , CHR(10) ).
        run n-majprgl( input i-par , input o-par ) .

    END . 

    i-par = x-demandeur .
    run n-opeges( input i-par , output o-par ) .
    ges-operat = o-par .
    b-bonent.signataire[ 1 ] = ges-operat .

    { majmoucb.i  b-bonent }

    VALIDATE b-bonent .

    ent-rowid = rowid( b-bonent ) .

    x-tietar = string( b-bonent.typaux , "xxx" ) + b-bonent.codaux .
    if string( b-bonent.typaux-fac , "xxx" ) + b-bonent.codaux-fac <> x-tietar  then
    do :
        if clifac-tar = yes
           then  x-tietar = string( b-bonent.typaux-fac , "xxx" ) + b-bonent.codaux-fac + "," + x-tietar .
           else  x-tietar = x-tietar + "," + string( b-bonent.typaux-fac , "xxx" ) + b-bonent.codaux-fac .
    end .

    assign  rem-specif = b-bonent.codpri
            tar-specif = b-bonent.codpri .
    if b-bonent.motcle begins "A"  then  tar-specif = "" .


    /* memorisation du demandeur et du code client dans bonens */
    CREATE bonens .
    ASSIGN bonens.codsoc = b-bonent.codsoc    
           bonens.motcle = b-bonent.motcle
           bonens.typcom = b-bonent.typcom
           bonens.numbon = b-bonent.numbon
           bonens.theme  = "INT-DBWEB"       
           bonens.chrono = 0   
           bonens.alpha[ 1 ] = TRIM( entry( 8 , zone-io-e , car-sep ) )
           bonens.alpha[ 2 ] = TRIM( entry( 43 , zone-io-e , car-sep ) )
           bonens.alpha[ 3 ] = TRIM( entry( 42 , zone-io-e , car-sep ) )
           . 
           bonens.alpha[ 4 ] = TRIM( entry( 44 , zone-io-e , car-sep ) ) NO-ERROR .

    { majmoucb.i  bonens }

    VALIDATE bonens .

    x-chrono = 0 .

    commande-gen = commande-gen + string( rowid( B-BONENT ) ) + car-sep .
    commande-vct = commande-vct + b-bonent.typcom + " " + substr( b-bonent.numbon , 1 , 8 ) + "," .
    IF dbg = YES  THEN MESSAGE "nt-webdeb.i : Fin creation entete" . 
END .


PROCEDURE cre-ligne:

        /*          L I G N E S                   */

        FIND B-BONENT where rowid( b-bonent ) = ent-rowid  exclusive-lock  no-error .

            x-chrono = x-chrono + x-pas .

        x-articl = right-trim( entry( 3 , fictmp.tmp-fichier , car-sep ) ) NO-ERROR .

        { n-cadarti.i  x-articl }

        FIND articl WHERE articl.codsoc = articl-soc
                    AND   articl.articl = x-articl
                    NO-LOCK NO-ERROR.
        IF dbg = YES  THEN MESSAGE "nt-webdeb.i : Creation ligne " b-bonent.codsoc b-bonent.motcle 
                                    b-bonent.typcom b-bonent.numbon x-chrono "article " x-articl .
        CREATE B-BONLIG .
        assign  b-bonlig.codsoc    = b-bonent.codsoc
                b-bonlig.motcle    = b-bonent.motcle
                b-bonlig.typcom    = b-bonent.typcom
                b-bonlig.numbon    = b-bonent.numbon
                b-bonlig.chrono    = x-chrono .

        VALIDATE b-bonlig .

        lig-rowid = rowid( b-bonlig ) .

        assign  b-bonlig.articl      = x-articl
                b-bonlig.libart      = articl.libart1[ 1 ]
                b-bonlig.typaux      = b-bonent.typaux
                b-bonlig.codaux      = b-bonent.codaux
                b-bonlig.typaux-fac  = b-bonent.typaux-fac
                b-bonlig.codaux-fac  = b-bonent.codaux-fac
                b-bonlig.magasin     = b-bonent.magasin
                b-bonlig.codpri      = b-bonent.codpri
                b-bonlig.codlig      = "N"
                b-bonlig.nivcom      = b-bonent.nivcom
                b-bonlig.datbon      = b-bonent.datbon
                b-bonlig.datdep      = b-bonent.datdep
                b-bonlig.txdev       = b-bonent.txdev
                b-bonlig.devise      = b-bonent.devise
                b-bonlig.transp      = b-bonent.transp
                b-bonlig.codech      = b-bonent.codech
                b-bonlig.modliv      = b-bonent.modliv
                b-bonlig.modreg      = b-bonent.modreg
                b-bonlig.datech      = b-bonent.datech
                b-bonlig.codadr-liv  = b-bonent.num-adl 
                b-bonlig.top-livre   = ""
                NO-ERROR .


        IF articl.divers >= "1" THEN
        DO:
            /*  Parametrage des largeurs des zones description  */
            assign  typtab = "PRM"  prefix = "SAI-LGCOMM"  codtab = b-bonent.motcle + b-bonent.typcom .
            run n-chparax( output x-x ) .
            if x-x = ""  then
            do :
                assign  typtab = "PRM"  prefix = "SAI-LGCOMM"  codtab = b-bonent.motcle .
                run n-chparax( output x-x ) .
            end .

            i-i = 104 .
            if x-x <> ""  then
            do :
                i-i = int( entry( 11 , x-x , "/" ) ) .
                if i-i > 20  and  i-i < 150  THEN  x-x = string( i-i ) .
                                             else  i-i = 104 .
            end .
            i-i = i-i / 1.43 .
            x-lgcomm = string( i-i ) .

            b-bonlig.libart  = entry( 25 , fictmp.tmp-fichier , car-sep ).

            o-par = entry( 26 , fictmp.tmp-fichier , car-sep ) + CHR(10) +
                    entry( 27 , fictmp.tmp-fichier , car-sep ) + CHR(10) +
                    entry( 28, fictmp.tmp-fichier , car-sep )
                    .

            o-par = REPLACE( o-par , "<BR>" , CHR(10) ).

           /*  Mise a Jour Proglib ( Texte Libell� )  */
            x-x = b-bonent.typcom + b-bonent.numbon + string( b-bonlig.chrono  , "9999" ) .

            i-par = x-lgcomm .
            run dt-chr10( input i-par , input-output o-par ) .

            i-par = b-bonent.motcle + "|" + b-bonent.codsoc +
                    "|" + "" + "|" + x-x + "|" + "LIG-TX" .

            run n-majprgl( input i-par , input o-par ) . 

/*             /*  M�mo. de la premi�re ligne de PROGLIB dans LIBART ( Si Article Texte )  */                                 */
/*             FIND PROGLIB where proglib.motcle  = b-bonent.motcle                                                           */
/*                          and   proglib.codsoc  = b-bonent.codsoc                                                           */
/*                          and   proglib.etabli  = ""                                                                        */
/*                          and   proglib.clef    = x-x                                                                       */
/*                          and   proglib.edition = "LIG-TX"                                                                  */
/*                          and   proglib.chrono  = 0                                                                         */
/*                          NO-LOCK  no-error .                                                                               */
/*             if available proglib AND articl.divers = "2" then  b-bonlig.libart = substr( proglib.libelle[ 1 ] , 1 , 78 ) . */

        end .

        b-bonlig.qte[ ind-qte ] = dec( entry( 23 , fictmp.tmp-fichier , car-sep ) ) no-error .

        /*  Date de Livraison Pr�vue  */
        x-x = entry( 2 , fictmp.tmp-fichier , car-sep ) NO-ERROR .
        x-y = substring( x-x , 1 , 2 ) + substring( x-x , 4 , 2 ) + 
              substring( x-x , 7 , 4 ) .
        if x-y <> ""  then
        do :
            b-bonlig.datdep = date( int( substring( x-y , 3 , 2 ) ) ,
                                    int( substring( x-y , 1 , 2 ) ) ,
                                    int( substring( x-y , 5 , 4 ) ) )  no-error .
        end .

        x-x = entry( 4 , fictmp.tmp-fichier , car-sep ) NO-ERROR .
        if x-x <> ""  then  b-bonlig.typaux = x-x .

        x-x = entry( 5 , fictmp.tmp-fichier , car-sep ) NO-ERROR .
        if x-x <> ""  then
        DO:  
            { regs-inc.i  "AUXILI" + b-bonlig.typaux }
            { cadr-aux.i  regs-soc  b-bonlig.typaux  x-x }
            b-bonlig.codaux = x-x .
        END.

        if available articl  then
        do :
            assign  b-bonlig.ctax    = articl.tva
                    b-bonlig.tenusto = articl.tenusto
                    b-bonlig.imput   = articl.impach
                    b-bonlig.usto    = articl.usto
                    .

            IF b-bonlig.motcle BEGINS "V" THEN b-bonlig.imput = articl.impvte .

            art-rowid = rowid( articl ) .

            if b-bonlig.tenusto   =  ""   then  b-bonlig.tenusto = "1" .
            if available b-typbon  then
            do :
                if b-typbon.tenusto   <> "1"  then  b-bonlig.tenusto = "0" .
                if b-typbon.tenusto   =  "2"  then  b-bonlig.tenusto = "2" .
            end .

            if b-bonent.ttva = typtva-exo  then b-bonlig.ctax = codtva-exo .
        end .

        FIND ARTBIS where artbis.codsoc = articl-soc
                    and   artbis.motcle = "ACH"
                    and   artbis.articl = x-articl
                    and   artbis.typaux = b-bonlig.typaux
                    and   artbis.codaux = b-bonlig.codaux
                    no-lock  no-error .
        if not available artbis  then
           FIND ARTBIS where artbis.codsoc = articl-soc
                       and   artbis.motcle = "VTE"
                       and   artbis.articl = x-articl
                       and   artbis.typaux = ""
                       and   artbis.codaux = ""
                       no-lock  no-error .
        if available artbis  then
        do :
            assign  b-bonlig.ufac     = artbis.ufac
                    b-bonlig.ucde     = artbis.ucde
                    b-bonlig.coef-cde = artbis.coef-cde 
                    b-bonlig.coef-fac = artbis.coef-fac . 
        end .

         b-bonlig.ucde    = entry( 24 , fictmp.tmp-fichier , car-sep ) NO-ERROR .

        /*  Analytique  */

        b-bonlig.segana[ 1 ] = entry( 31 , fictmp.tmp-fichier , car-sep ) NO-ERROR .
        b-bonlig.segana[ 2 ] = entry( 32 , fictmp.tmp-fichier , car-sep ) NO-ERROR .
        b-bonlig.segana[ 3 ] = entry( 33 , fictmp.tmp-fichier , car-sep ) NO-ERROR .

        do i-i = 4 to 9 :
            b-bonlig.segana[ i-i ] = b-bonent.segana[ i-i ] .
        end .


        y-motcle = "ACH" .
        y-typimp = "" .
        if b-bonlig.motcle begins "V"  then  y-motcle = "VTE" .
        if b-bonlig.tenusto = "2"      then  y-typimp = b-bonlig.typcom .

        FIND IMPAPR where impapr.codsoc = impapr-soc
                    and   impapr.motcle = y-motcle
                    and   impapr.typimp = y-typimp
                    and   impapr.imputa = b-bonlig.imput
                    no-lock  no-error .
        if not available impapr  then
            FIND IMPAPR where impapr.codsoc = impapr-soc
                        and   impapr.motcle = y-motcle
                        and   impapr.typimp = y-typimp
                        and   impapr.imputa = "999"
                        no-lock  no-error .
        IF AVAILABLE impapr THEN
        do i-i = 1 to 10 :
             if trim(impapr.typtva[ i-i ]) <> trim(b-bonlig.ctax)  then  next .

             compte-cg  = impapr.compte-cg[ i-i ] .
             compte-tv  = impapr.compte-tva[ i-i ] .
        end .

        b-bonlig.codgen = compte-cg .
        b-bonlig.cpttva = compte-tv .

        IF ENTRY( 24, fictmp.tmp-fichier , car-sep ) <> "" THEN 
            b-bonlig.ucde   = ENTRY( 24, fictmp.tmp-fichier , car-sep ) NO-ERROR .

        /*  Prix  */
        b-bonlig.pu-brut = dec( entry( 21 , fictmp.tmp-fichier , car-sep ) ) no-error .
        b-bonlig.pu-net  = b-bonlig.pu-brut .

        if b-bonlig.pu-net = 0  and  available articl  and  b-bonlig.ref-marche = ""  then
        DO :

            /*  Calcul Prix Tarif  */
            i-param = x-tietar + "|" + tar-specif + "|" + string( ent-rowid ) + "|" +
                      string( lig-rowid )  + "|" + string( art-rowid ) + "|" + "" + "|" +
                      string( ind-qte ) + "|" + b-bonent.devise .
            run n-caltar( input i-param , output o-param ) .

            /*  Calcul Remises  */
            i-param = x-tietar + "|" + rem-specif + "|" + string( ent-rowid ) + "|" +
                      string( lig-rowid )  + "|" + string( art-rowid ) + "|" + "" + "|" + 
                      string( ind-qte ) + "|" + b-bonent.devise .  
            run n-calrem( input i-param , output o-param ) .

        END .  /*  DO  TARIFICATION  */


        if b-bonlig.pu-net = 0  then  b-bonlig.codpri = "A" .

        /*  Calcul Prix Net  */
        i-param = string( ind-qte ) + "|" + string( lig-rowid ) .
        run n-calpxn( input i-param , output o-param ) .

        b-bonlig.htnet = b-bonlig.qte[ ind-qte ] * b-bonlig.pu-net .
        if b-bonlig.coef-fac < 0  then  b-bonlig.htnet = - ( b-bonlig.htnet / b-bonlig.coef-fac ) .
        if b-bonlig.coef-fac > 0  then  b-bonlig.htnet =     b-bonlig.htnet * b-bonlig.coef-fac   .
        b-bonlig.htnet = round( b-bonlig.htnet , 2 ) .

        { majmoucb.i  b-bonlig }

        CREATE bonlgs.                             
        ASSIGN bonlgs.codsoc = b-bonlig.codsoc       
               bonlgs.motcle = b-bonlig.motcle       
               bonlgs.typcom = b-bonlig.typcom       
               bonlgs.numbon = b-bonlig.numbon       
               bonlgs.chrono = b-bonlig.chrono       
               bonlgs.theme  = "INT-DBWEB"          
               bonlgs.ss-chrono = 0   
               .
               bonlgs.alpha[1] = ENTRY( 18 , fictmp.tmp-fichier , car-sep )  NO-ERROR . 
               bonlgs.alpha[2] = ENTRY( 19 , fictmp.tmp-fichier , car-sep )  NO-ERROR . 


        { majmoucb.i bonlgs }                      

        nb-lus = nb-lus + 1 .

        RELEASE b-bonlig .
        RELEASE bonlgs .

        IF dbg = YES  THEN MESSAGE "nt-webdeb.i : Fin de cr�ation de ligne " .

END .  /* REPEAT TRANSACTION  */

/*************************************************/
/*     Procedure de contr�le des fournisseurs   */
/***********************************************/
PROCEDURE verif-fou :

  /*  MESSAGE "procedure verif-fou" . */

    fou-ok = YES .

    IF ENTRY( 28 , zone-io-e , car-sep )= "" AND ENTRY( 29 , zone-io-e , car-sep ) = "" THEN
    DO:
        /* cas des demandes d'achat , fournisseur non obligatoire */
        x-typaux = "" .
        x-codaux = "" .
        fou-ok = NO .
        RETURN .
    END.

    ASSIGN x-typaux = ENTRY( 28 , zone-io-e , car-sep )
           x-codaux = ENTRY( 29 , zone-io-e , car-sep )
           .
    x-y = x-typaux .
    regs-fileacc = "AUXILI" + x-y .
    { regs-rec.i }
    { cadr-aux.i  regs-soc  x-y  x-codaux }

    IF ENTRY( 29 , zone-io-e , car-sep ) <> "" THEN 
    DO:
        FIND auxges WHERE auxges.codsoc = auxges-soc
                    AND   auxges.typaux = x-typaux 
                    AND   auxges.codaux = x-codaux
                    NO-LOCK NO-ERROR .
        IF NOT AVAILABLE auxges THEN fou-ok = NO .
        RETURN.
    END.

    /* gestion des fournisseur provisoire - Inactif chez eurogem pour le moment - car siret non saisi */
    x-typaux = "prv" .    
    FIND FIRST auxges WHERE auxges.codsoc = auxges-soc
                      AND   auxges.siret  = substr( trim(ENTRY( 18 , zone-io-e , car-sep )) , 1 , 15 )
                      AND   auxges.typaux = x-typaux
                      USE-INDEX siret NO-LOCK NO-ERROR .
    IF AVAILABLE auxges THEN RETURN.

    x-codaux = "" .
    REPEAT WHILE x-codaux = "" :
        assign  typtab = "NAT"  prefix = "AUXG-000"  codtab = "INC-AUTO" .
        run n-chparax( output inc-auto ) .
        inc-auto = entry( 1 , inc-auto , "/" ) .

        ASSIGN codsoc    = auxges-soc   
               etabli    = ""   
               typtab    = "CHR"
               prefix    = "AUXGES"           
               codtab    = x-typaux
               increment = int( entry( 1 , inc-auto  ) ) .

        if increment < 1  or  increment > 19  then  increment = 1 .
        run n-incauto.p( increment , 1 , 0 , "" , input-output nombre ) .
        x-x = string( nombre , ">>>>>>>>>9" ) .

        FIND auxges WHERE auxges.codsoc = auxges-soc
                    AND   auxges.typaux = "prv"
                    AND   auxges.codaux = x-x
                    NO-LOCK NO-ERROR.

        IF NOT AVAILABLE auxges THEN x-codaux = x-x .

    END.

    CREATE auxges.

    ASSIGN auxges.codsoc     = auxges-soc
           auxges.typaux     = x-typaux
           auxges.codaux     = x-codaux
           auxges.typaux-fac = x-typaux
           auxges.codaux-fac = x-codaux
           auxges.adres[ 1 ] = substr(ENTRY( 30 , zone-io-e , car-sep ) , 1 , 32 )
           auxges.adres[ 2 ] = substr(ENTRY( 31 , zone-io-e , car-sep ) , 1 , 32 )
           auxges.adres[ 3 ] = substr(ENTRY( 32 , zone-io-e , car-sep ) , 1 , 32 )
           auxges.adres[ 4 ] = substr(ENTRY( 33 , zone-io-e , car-sep ) , 1 , 32 )
           auxges.adres[ 5 ] = substr(ENTRY( 34 , zone-io-e , car-sep ) , 1 , 32 )
        /*     auxges.ttva       = auxili.ttva  */
           auxges.devise     = ENTRY(  28 , zone-io-e , car-sep )
        /*     auxges.modreg     = auxili.mreg    */
           auxges.code-pays  = ENTRY( 22 , zone-io-e , car-sep )
           auxges.siret      = substr(trim(ENTRY( 18 , zone-io-e , car-sep ) ) , 1 , 15 )
           auxges.libabr     = substr(ENTRY( 17 , zone-io-e , car-sep ) , 1 , 20 )
           auxges.teleph     = substr(ENTRY( 19 , zone-io-e , car-sep ) , 1 , 20 )
           auxges.nom-respon = substr(ENTRY( 20 , zone-io-e , car-sep ) , 1 , 32 )
           .

    { majmoucb.i auxges }


    regs-fileacc = "AUXILI" + auxges.typaux .
    { regs-rec.i }
    find AUXILI  where auxili.codsoc = regs-soc
                 and   auxili.typaux = auxges.typaux
                 and   auxili.codaux = auxges.codaux
                 exclusive-lock no-error .

    IF NOT AVAILABLE auxili THEN CREATE auxili.

    ASSIGN auxili.codsoc     = regs-soc
           auxili.typaux     = auxges.typaux
           auxili.codaux     = auxges.codaux
           auxili.typaux-pai = auxges.typaux-fac
           auxili.codaux-pai = auxges.codaux-fac
           auxili.adres[1]   = auxges.adres[1]
           auxili.adres[2]   = auxges.adres[2]
           auxili.adres[3]   = auxges.adres[3]
           auxili.adres[4]   = auxges.adres[4]
           auxili.adres[5]   = auxges.adres[5]
           auxili.ttva       = auxges.ttva
           auxili.devise     = auxges.devise
    /*     auxili.mreg    =   b-auxges.modreg    Erik faut-il le faire ? */
           auxili.pays       = auxges.code-pays
           auxili.siret      = auxges.siret
           auxili.libabr     = substr( auxges.libabr , 1 , 15 )
           .

    { majmoucb.i auxili }

    regs-fileacc = "AUXILI" + auxges.typaux .
    { regs-rec.i }

    find tabdom where tabdom.codsoc = regs-soc
                and   tabdom.etabli = ""
                and   tabdom.typtab = "SPE"
                and   tabdom.prefix = auxili.typaux
                and   tabdom.codtab = auxili.codaux
                exclusive-LOCK no-error.

    if not available tabdom then create TABDOM .

    ASSIGN tabdom.codsoc = regs-soc
           tabdom.etabli = ""
           tabdom.typtab = "SPE"
           tabdom.prefix = auxili.typaux
           tabdom.codtab = auxili.codaux
           substr( tabdom.libel1[1] , 1 , 20) = auxges.teleph
           substr( tabdom.libel1[2] , 1 , 20) = auxges.fax
           substr( tabdom.libel1[3] , 1 , 32) = auxges.e-mail
           substr( tabdom.libel1[4] , 1 , 32) = auxges.nom-respon
           .

    { majmoucb.i tabdom }

    RELEASE auxges . 

    RELEASE auxili.

    release tabdom .


    RETURN "" .

END.

/*******************************************/
/* procedure de recherche des signataires */
/*****************************************/

PROCEDURE HAB-SIGNAT :

    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Debut recherche signataire HAB-SIGNAT" b-bonent.codsoc b-bonent.typcom b-bonent.numbon .

    def var i-i         as int              no-undo .
    def var i-j         as int              no-undo .
    def var x-x         as char             no-undo .
    def var x-y         as char             no-undo .

    DEF VAR ind-achete  AS INT        NO-UNDO .
    DEF VAR mod-profil  AS LOG        NO-UNDO .

    DEF VAR vect-signat  AS CHAR     NO-UNDO .

    nb-habili = 0 .

    x-motcle = b-bonent.motcle .
    x-typcom = b-bonent.typcom .

    IF x-motcle = "ACH" AND x-typcom = "FAC"  THEN
        ASSIGN x-motcle = "ACC"
               x-typcom = "CDF"
               .

    cle-mottyp = x-motcle + " " .

    FIND B-TYPBON where b-typbon.codsoc = b-bonent.codsoc
                  and   b-typbon.motcle = x-motcle
                  and   b-typbon.typcom = X-typcom
                  no-lock  no-error .

    if available b-typbon  then
    do :
        if b-typbon.motcle = "ACC"  and  ( int( b-typbon.typiec ) >= 1  and
           int( b-typbon.typiec ) <= 3  )
        then  overlay( cle-mottyp , 4 , 1 ) = string( int( b-typbon.typiec ) , "9" ) .
        if substring( b-typbon.typaux , 1 , 1 ) = "*"  then
           overlay( cle-mottyp , 4 , 1 ) = substring( b-typbon.typaux , 2 , 1 ) .
    end .

    cle-habili = cle-mottyp .
    cle-mottyp = right-trim( cle-mottyp ) .
    if b-typbon.cle-habilit <> ""  then
       overlay( cle-habili , 4 , 1 ) = substring( b-typbon.cle-habilit , 1 , 1 ) .
    cle-habili = right-trim( cle-habili ) .

    FIND TABGES where tabges.codsoc = habili-soc
                and   tabges.etabli = ""
                and   tabges.typtab = "HAB"
                and   tabges.prefix = "ACTION"
                and   tabges.codtab = cle-habili
                no-lock  no-error .
    if not available tabges then  cle-habili = "" . 
    else  do : 
        if tabges.libel2[ 2 ] begins "O"  then  mod-profil = yes . 
    end .

    i-par = x-demandeur .

    run n-opeges( input i-par , output o-par ) .
    ges-operat = o-par .
    if cle-habili = ""  then return .

    FIND LAST TABGES where tabges.codsoc = habili-soc
                     and   tabges.etabli = ""
                     and   tabges.typtab = "HAB"
                     and   tabges.prefix = "PRF-" + cle-habili
                     no-lock  no-error .
    IF AVAILABLE tabges THEN  nb-habili = int( substring( tabges.codtab , 1 , 1 ) ) .

    IF nb-habili = 0 THEN RETURN.

    /*  lecture Fiche Habilitations  */
    FIND HAB-TABGES where hab-tabges.codsoc = habili-soc
                   and   hab-tabges.etabli = ""
                   and   hab-tabges.typtab = "HAB"
                   and   hab-tabges.prefix = "HAB-" + cle-habili
                   and   hab-tabges.codtab = ges-operat
                   no-lock  no-error .

    IF dbg = YES THEN MESSAGE "nt-webdeb.i : Apres lecture habilitation " AVAILABLE tabges cle-habili ges-operat b-bonent.typcom  .
    IF AVAILABLE hab-tabges THEN
    do i-i = 1 to nb-habili :

        IF i-i = 1  THEN 
        do:
            b-bonent.signataire[ i-i ] = ges-operat .
            NEXT.
        END.

        x-x = hab-tabges.libel2[ i-i ] .

        IF hab-tabges.libel1[ i-i ] = "O" OR hab-tabges.libel1[ i-i ] = "*" THEN x-x = ges-operat .

        FIND TABGES where tabges.codsoc = habili-soc                                                                
                    and   tabges.etabli = ""                                                                        
                    and   tabges.typtab = "HAB"                                                                     
                    AND   tabges.prefix = "PRF-" + cle-habili   
                    AND   tabges.codtab = STRING( i-i , "9" )
                    NO-LOCK NO-ERROR .

/*                                                                                                                            */
/*         if i-j = 0  and  mod-profil = yes  then                                                                            */
/*         do :                                                                                                               */
/*              assign  matprf-obli[ i-i ] = "O"                                                                              */
/*                      matprf-codm[ i-i ] = 0 .                                                                              */
/*              if hab-tabges.libel2[ i-i ] = ges-operat  or  hab-tabges.libel2[ i-i ] = ""  then  matprf-sign[ i-i ] = "N" . */
/*              if hab-tabges.libel2[ i-i ] <> ges-operat  and  hab-tabges.libel2[ i-i ] <> ""  then  i-j = i-i .             */
/*         end .                                                                                                              */

/*         IF i-j = 0 AND mod-profil = YES THEN                                                      */
/*         DO:                                                                                       */
/*             IF entry( 1 , x-x ) = ges-operat THEN b-bonent.signataire[ i-i ] = entry( 1 , x-x ) . */
/*             IF entry( 1 , x-x ) <> ges-operat THEN                                                */
/*                 ASSIGN b-bonent.signataire[ i-i ] = entry( 1 , x-x )                              */
/*                        i-j = i-i                                                                  */
/*                        .                                                                          */
/*         END.                                                                                      */
        IF i-j < 2 AND mod-profil = YES THEN
        DO:
            IF entry( 1 , x-x ) = ges-operat THEN b-bonent.signataire[ i-i ] = entry( 1 , x-x ) .
            IF entry( 1 , x-x ) <> ges-operat THEN 
                ASSIGN b-bonent.signataire[ i-i ] = entry( 1 , x-x ) 
/*                        i-j = i-i */
                       .
            IF LOOKUP(  entry( 1 , x-x ) , vect-signat ) = 0 AND tabges.libel2[2] = "O" THEN 
                ASSIGN vect-signat = vect-signat + "," + entry( 1 , x-x )
                       i-j = i-j + 1 .
        END.

        ELSE DO:
            IF AVAILABLE tabges  THEN
            DO: 
                IF tabges.libel2[1] = "O" THEN b-bonent.signataire[ i-i ] = entry( 1 , x-x ) .
                ELSE DO:
                    IF tabges.libel2[2] = "O" AND tabges.nombre[ 1 ] <> 0 THEN                                                      
                    DO:                                                                                     
                         /*  Si code plafond est <> 0 et montant Commande  > Plafond : On grise la validation */
                        j-j =  tabges.nombre[ 1 ] .                                                          
                        if j-j <= 0  or  j-j > 9   then  j-j = 0 .                                           
                        if j-j <> 0  and  mt-plafond[ j-j ] > 0   then                                       
                        do : 
                            if ( b-bonent.ttc-bon * b-bonent.txdev >= mt-plafond[ j-j ] and base-workflow = "TTC" ) or
                               ( b-bonent.ht-bon  * b-bonent.txdev >= mt-plafond[ j-j ] and base-workflow = "HT" )     
                            THEN b-bonent.signataire[ i-i ] = entry( 1 , x-x ) .
                            ELSE b-bonent.signataire[ i-i ] = "" .
                        end .  

                    END.
                    ELSE b-bonent.signataire[ i-i ] = entry( 1 , x-x ) .
                END.

            END.
            ELSE b-bonent.signataire[ i-i ] = entry( 1 , x-x ) .
        END.
    end .

END .  /*  PROCEDURE HAB-SIGNAT  */ /*&FIN*/


