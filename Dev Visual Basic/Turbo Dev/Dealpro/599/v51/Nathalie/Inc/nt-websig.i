/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/nathalie/inc/nt-websig.i                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  5450 20-06-03!New!bp             !Pb de lock quand une piece est sign�e sur le WEB                      !
!V52  4927 28-04-03!Bug!bp             !Pb sur le refus de BAP par le web                                     !
!V52  4892 23-04-03!Evo!bp             !Les cdp doivent g�n�rer des rcf et pas des rcp en r�ception           !
!V52  4799 11-04-03!Bug!bp             !Pb sur signature des BAP                                              !
!V52  4772 10-04-03!New!bp             !Mise en place workflow sur les pieces FAC                             !
!V52  4697 02-04-03!New!bp             !Pb sur le cicuit de signature : manque l'adaptation dans nt-websig.i  !
!V52  4593 21-03-03!New!bp             !Pb sur remont�e de donn�es vers le client serveur                     !
!V52  4513 14-03-03!New!bp             !Dans le cas de g�n�ration d'une r�ception, suppression du fichier de v!
!V52  4460 11-03-03!New!bp             !mise en place du module d'echange avec le web pour Eurogem            !
!V52  4185 12-02-03!New!bp             !Ajustement des libelles des mails                                     !
!V52  4114 05-02-03!New!bp             !Mise en place retour critere qualite commande et r�ception            !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/*-----------------------------------------------------------------------------------------------*/
/*           N T - W E B S I G . I  : Signature d'une piece et envoi au prochain signataire     */
/*---------------------------------------------------------------------------------------------*/

REPEAT TRANSACTION :
    MESSAGE "nt-websig.i : Traitement du fichier" ligne  STRING(TODAY,"99/99/9999") STRING(TIME,"hh:mm:ss") .

    def var x-operat     as char                no-undo .

    DEF VAR x-signat     AS CHAR                NO-UNDO .

/*     MESSAGE "Traitement du fichier" ligne  STRING(TODAY,"99/99/9999") STRING(TIME,"hh:mm:ss") */
/*         VIEW-AS ALERT-BOX.                                                                    */

    /*******************************************/
    /* phase 1 - analyse du fichier requete   */
    /*****************************************/

    codsoc-soc = ENTRY( 3 , ligne , "-" ) .

    glo-codsoc = codsoc-soc .
    glo-etabli = "" .

    regs-app = "NATHALIE" .
    { regs-cha.i }
    regs-app = "ELODIE" .
    { regs-cha.i }

    { regs-inc.i  AUXSPE }
    auxges-soc = regs-soc .

    { regs-inc.i  ARTICL }
    articl-soc = regs-soc .

    { regs-inc.i  MAGASI }
    magasi-soc = regs-soc .

    { regs-inc.i  IMPAPR }
    impapr-soc = regs-soc .

    { regs-inc.i  HABILI }
    habili-soc = regs-soc .

    { regs-inc.i  TARFOU }
    tarfou-soc = regs-soc .

    { regs-inc.i  TARCLI }
    tarcli-soc = regs-soc .


    INPUT stream kan-fic from value( fichier-lec ) .

    x-operat = x-auteur .

    traitement:
    REPEAT:
        /* 1ere ligne du fichier =  Ligne d'Ent�te  */
        zone-io-e = "" .
        IMPORT stream kan-fic unformatted zone-io-e .

        IF NUM-ENTRIES( zone-io-e , "|" ) < 2 THEN LEAVE traitement. 

        x-x = ENTRY( 1 , zone-io-e , "|" ) .

        CASE x-x :
            WHEN "l-signat" THEN RUN trt-signat  .

            WHEN "critere" THEN RUN trt-qualite .

            OTHERWISE do:
                IF dbg = YES THEN MESSAGE "nt-websig.i : Attention fichier de signature erron�. verifiez les droits sur les directories ..." .
                LEAVE traitement.
            END.
        END CASE.

    END.
    IF dbg = YES THEN MESSAGE "nt-websig.i : Sortie du module nt-websig.i" .
    LEAVE .
END.

/**********************************/
/* Traitement des signatures     */
/********************************/

PROCEDURE TRT-SIGNAT:

    DEF VAR mod-profil  AS LOG        NO-UNDO .
    DEF VAR y-motcle AS CHAR NO-UNDO .

    IF NUM-ENTRIES( zone-io-e , "|" ) < 4 THEN RETURN.
    IF dbg = YES THEN MESSAGE "nt-websig.i : Procedure trt-signat " zone-io-e .

    i-par = x-auteur .
    run n-opeges( input i-par , output o-par ) .
    ges-operat = o-par .

    ASSIGN codetb-etb = ENTRY( 2 , ENTRY( 2 , zone-io-e , car-sep ) , "-" )
           x-typcom   = ENTRY( 3 , zone-io-e , car-sep )
           x-numbon   = string( DEC( ENTRY( 4 , zone-io-e , car-sep )) , ">>>>>>>9" )  + "00"
           .

    FIND bonent WHERE bonent.codsoc = codsoc-soc
                AND   bonent.typcom = x-typcom
                AND   bonent.numbon = x-numbon
                EXCLUSIVE-LOCK NO-ERROR.

    IF NOT AVAILABLE bonent THEN RETURN .

    y-motcle = bonent.motcle .

    IF bonent.motcle = "ACH" AND bonent.typcom = "FAC"  THEN 
        ASSIGN y-motcle = "ACC"
               X-typcom = "CDF"
               .
    cle-mottyp = y-motcle + " " .

    FIND B-TYPBON where b-typbon.codsoc = codsoc-soc
                  and   b-typbon.motcle = y-motcle
                  and   b-typbon.typcom = x-typcom
                  no-lock  no-error .

    if available b-typbon  then
    do :
        if b-typbon.motcle = "ACC"  and  ( int( b-typbon.typiec ) >= 1  and
           int( b-typbon.typiec ) <= 3  )
        then  overlay( cle-mottyp , 4 , 1 ) = string( int( b-typbon.typiec ) , "9" ) .
        if substring( b-typbon.typaux , 1 , 1 ) = "*"  then
           overlay( cle-mottyp , 4 , 1 ) = substring( b-typbon.typaux , 2 , 1 ) .
    end .

    cle-habili = cle-mottyp .
    cle-mottyp = right-trim( cle-mottyp ) .
    if b-typbon.cle-habilit <> ""  then
       overlay( cle-habili , 4 , 1 ) = substring( b-typbon.cle-habilit , 1 , 1 ) .
    cle-habili = right-trim( cle-habili ) .

    i-i = 0 .
    DO k-k = 2 TO 9 :
        IF trim(bonent.ope-signat[ k-k ]) = "" AND i-i = 0 THEN 
        DO:
            i-i = k-k .
        END.
    END.

    FIND TABGES where tabges.codsoc = ""                                                                        
                and   tabges.etabli = ""                                                                        
                and   tabges.typtab = "APR"                                                                     
                and   tabges.prefix = "OPEACH"                                                                  
                and   tabges.codtab = bonent.signataire[ 1 ]                                                
                no-lock  no-error .                                                                             

    if available tabges  then x-demandeur = trim( tabges.libel1[ 2 ] ) .    /*  Matricule du demandeur  */ 

    x-signat = "9" .
    IF trim( entry( 5 , zone-io-e , car-sep ) ) = "1" THEN x-signat = "0" .

    /* memorisation commentaire signature dans bonens dans Alpha[1] 9 si refus 0 si signe */
    assign  i-param = "U|BONENT|" + string( rowid( BONENT ) ) + "|SIGNAT-" + string( i-i ) + "|0"
                o-param = "A01=" + x-signat                                  +
                          "|A02=" + trim( entry( 6 , zone-io-e , car-sep ) ) +                     
                          "|A03=" + trim( entry( 7 , zone-io-e , car-sep ) ) +
                          "|A04=" + trim( entry( 8 , zone-io-e , car-sep ) ) +
                          "|A05=" + ges-operat .
    run n-ficsuit( input i-param , input-output o-param ) . 

    IF trim( entry( 5 , zone-io-e , car-sep ) ) = "1" THEN /*  Acceptation ( 1 ) - Refus ( 0 )  */
    DO:
        IF dbg = YES THEN MESSAGE "nt-websig.i : Signature accept�e " bonent.codsoc bonent.typcom bonent.numbon " signataire " ges-operat .

        ASSIGN bonent.ope-signat[ i-i ] = ges-operat
               bonent.dat-signat[ i-i ] = TODAY
               .

        DO k-k = i-i + 1 TO 9 :
            IF bonent.signataire[ k-k ] = bonent.signataire[ i-i ] THEN
            DO:
                ASSIGN bonent.ope-signat[ k-k ] = ges-operat
                       bonent.dat-signat[ k-k ] = TODAY     
                       .
                IF dbg = YES THEN MESSAGE "nt-websig.i - Signature suivante automatique car m�me signataire " k-k . 
            END.
            ELSE k-k = 9 .
       END.

       /*     /*  Apr�s MAJ : Appel du signataire suivant ( ou du premier )  */                                               */
        k-k = 0 .                                                                                                       
        FOR EACH TABGES where tabges.codsoc = habili-soc                                                                
                        and   tabges.etabli = ""                                                                        
                        and   tabges.typtab = "HAB"                                                                     
                        and   tabges.prefix = "PRF-" + cle-habili                                                  
                        no-lock :                                                                                       

             i-i = int( substring( tabges.codtab , 1 , 1 ) ) .                                                           

             IF i-i = 1 THEN NEXT .

             /*  Si l'op�rateur n'est pas renseign�  */                                                                  
             if bonent.signataire[ i-i ] = ""  then  next .                                                            

             /*  Si la signature est d�j� pos�e  */                                                                      
             if bonent.ope-signat[ i-i ] <> ""  then  next .                                                           

             /*  S'il n'y a pas de signature � poser */                                                                  
     /*        if tabges.libel2[ 2 ] <> "O"  then  next .  */                                                                

             /*  S'il n'y a pas de profil � signer auparavant  */                                                        
             k-k = i-i .                                                                                                 
             j-j = tabges.nombre[ 2 ] .                                                                                  
             if j-j <> 0  and  bonent.ope-signat[ j-j ] = ""  then  k-k = j-j .                                        

             leave .                                                                                                     

        END .  /*  EACH TABGES  */                                                                                      

        if k-k <> 0  then                                                                                               
        do :                                                                                                            
            FIND TABGES where tabges.codsoc = ""                                                                        
                        and   tabges.etabli = ""                                                                        
                        and   tabges.typtab = "APR"                                                                     
                        and   tabges.prefix = "OPEACH"                                                                  
                        and   tabges.codtab = bonent.signataire[ k-k ]                                                
                        no-lock  no-error .                                                                             
            if available tabges  then                                                                                   
            do :                                                                                                        
                x-x = trim( tabges.libel1[ 2 ] ) .    /*  Matricule du Signataire  */  
                FIND d-operat where d-operat.ident  = "deal"
                              and   d-operat.typtab = "operat" 
                              AND   d-operat.codtab = x-x
                              NO-LOCK NO-ERROR.
                IF AVAILABLE d-operat AND TRIM( tabges.libel2[14]) = "1" THEN
                DO:
                    i-to = x-x .
                    IF bonent.typcom = "fac" THEN
                    DO:
                        RUN trt-signat-bap  .
                        IF NOT available bonent THEN RETURN.
                    END.
                    ELSE DO:
                        i-param = string( rowid( bonent ) ) + "|SIG" + string( k-k ) + "|" + x-x + "|" + ligne.  
                        i-erreur = "" .
                        IF dbg = YES THEN MESSAGE " avant nt-webgen: envoi fichier signature" i-to .
                        run nt-webgen( input i-param , input i-erreur , output o-param ) . 
                    END.
                END.

               /* run n-genmail ==> envoi mail au prochain signataire */
                FIND TABGES where tabges.codsoc = codsoc-soc               
                            and   tabges.etabli = ""
                            and   tabges.typtab = "CHR"
                            and   tabges.prefix = "TYPCOM"
                            and   tabges.codtab = TRIM( bonent.motcle + bonent.typcom )
                            NO-LOCK NO-ERROR.

                IF AVAILABLE tabges AND tabges.nombre[3] = 1 THEN 
                DO:
                    ASSIGN i-from = bonent.opecre
                           i-copy = ""
                           i-file = ""
                           i-objet = "" .
                           i-message = ""
                           .
                    assign  typtab = "PRM"  prefix = "OPERATEUR"    codtab = "MAIL" .
                    run n-chparax( output x-x ) .
                    if x-x <> "" THEN i-from = entry( 1 , x-x , "/" )    no-error .

             /*       i-objet = deal-get-dictab ( "sigsig-nat[LABEL]" , "nom" ) .  */

                    RUN n-get-dictab( INPUT "599,DEAL" , INPUT "LABEL" , INPUT "SIGSIG-NAT" , INPUT "1" , OUTPUT i-objet ) .
                    IF ENTRY( 2 , ligne , "-" ) = "fac" THEN
                        RUN n-get-dictab( INPUT "599,DEAL" , INPUT "LABEL" , INPUT "SIGFAC-NAT" , INPUT "1" , OUTPUT i-objet ) .

                    IF i-objet = "" THEN i-objet = "Signature de la piece : #01 #02 #03 " .

                    i-objet = REPLACE( i-objet , "#01" , bonent.codsoc ).
                    i-objet = REPLACE( i-objet , "#02" , bonent.etabli ).
                    i-objet = REPLACE( i-objet , "#03" , bonent.typcom + " " + SUBSTR( bonent.numbon , 1 , 8 ) ) .

              /*      i-message = deal-get-dictab ( "sigsig-nat[message]" , "designation" ) .  */
                    RUN n-get-dictab( INPUT "599,DEAL" , INPUT "MESSAGE" , INPUT "SIGSIG-NAT" , INPUT "1" , OUTPUT i-message ) .

                    IF ENTRY( 2 , ligne , "-" ) = "fac" THEN
                        RUN n-get-dictab( INPUT "599,DEAL" , INPUT "MESSAGE" , INPUT "SIGFAC-NAT" , INPUT "1" , OUTPUT i-objet ) .

                    IF i-message = "" THEN i-message = "Vous devez signer la pi�ce #03 " .
                                        .
                    i-message = REPLACE( i-message , "#01" , bonent.codsoc ).  
                    i-message = REPLACE( i-message , "#02" , bonent.etabli ).  
                    i-message = REPLACE( i-message , "#03" , bonent.typcom + " " + SUBSTR( bonent.numbon , 1 , 8 ) ) .
                    i-message = REPLACE( i-message , "#04" , bonent.signataire[1] ).  

                     FIND z-TABGES where z-tabges.codsoc = ""                                                                        
                                   and   z-tabges.etabli = ""                                                                        
                                   and   z-tabges.typtab = "APR"                                                                     
                                   and   z-tabges.prefix = "OPEACH"                                                                  
                                   and   z-tabges.codtab = bonent.signataire[ 1 ]                                                
                                   no-lock  no-error .              
                     IF AVAILABLE z-tabges THEN i-message = REPLACE( i-message , "#05" , z-tabges.libel1[1] ).  

                    RUN n-genmail( INPUT i-from , INPUT i-to , input i-copy ,  
                                    INPUT i-objet , INPUT i-file , INPUT i-message ) .
                    IF dbg = YES THEN MESSAGE "nt-websig.i : Apres n-genmail ==> " i-from i-to i-message .
                END.

                /* renvoie de la commande � l'operateur qui a fait la saisie - sauf pour les pieces FAC*/
                IF bonent.typcom <> "fac"  THEN
                DO:
                    x-x = bonent.opecre .    /*  Matricule   */                                   
                    i-param = string( rowid( bonent ) ) + "|VIS0"  + "|" + x-x + "|" + ligne . 
                    i-erreur = "" .
                    IF dbg = YES THEN MESSAGE "nt-websig.i : Avant renvoi fichier operateur initial" x-x .
                    run nt-webgen( input i-param , input i-erreur , output o-param ) .
                END.
            end .                                                                                                    
        end .                                                                                                           
        else  do :       
            /* commande valid�e envoi fichier operateur creation */
            bonent.stat-valide = 1 .  
            x-x = bonent.opecre .    /*  Matricule   */         

            IF bonent.typcom = "fac" THEN
                RUN trt-valid-bap  .
            ELSE DO:

                i-param = string( rowid( bonent ) ) + "|VIS1"  + "|" + x-x + "|" + ligne . 
                i-erreur = "" .
                IF dbg = YES THEN MESSAGE "nt-websig.i : Avant envoi fichier piece valid�e " x-x .
                run nt-webgen( input i-param , input i-erreur , output o-param ) . 

                /* envoie fichier de prereception si commande est livrable */
                FIND B-TYPBON where b-typbon.codsoc = codsoc-soc
                              and   b-typbon.motcle = bonent.motcle
                              and   b-typbon.typcom = bonent.typcom
                              no-lock  no-error .
                IF AVAILABLE b-typbon AND b-typbon.livrable = 0 THEN
                DO:
                                   /* si piece livr�s en automatique, g�n�ration de la r�ception */
                    FIND BONENS  where bonens.codsoc = bonent.codsoc
                                 and   bonens.motcle = bonent.motcle
                                 and   bonens.typcom = bonent.typcom
                                 and   bonens.numbon = bonent.numbon
                                 and   bonens.theme  = "INT-DBWEB"
                                 and   bonens.chrono = 0
                                 no-lock no-error .
                    if available bonens AND bonens.alpha[2] = "1" then
                    DO:
                        IF dbg = YES THEN MESSAGE "nt-websig.i : G�n�ration automatique de la r�ception "  .

                        RUN nt-webgen-rcf( input i-param , input i-erreur , output o-param ) .

                        IF dbg = YES THEN MESSAGE "nt-websig.i : generation fichier vis3 sur la commande r�ceptionn�e chez " bonent.opecre .
                        x-x = bonent.opecre .    /*  Matricule   */                                   
                        i-param = string( rowid( bonent ) ) + "|VIS3"  + "|" + x-x + "|" + ligne .     
                        run nt-webgen( input i-param , input i-erreur , output o-param ) . 

                    END.
                    ELSE DO:
                        x-x = bonent.opecre .    /*  Matricule   */                                   
                        i-param = string( rowid( bonent ) ) + "|RCF"  + "|" + x-x + "|" + ligne .  
                        IF dbg = YES THEN MESSAGE "nt-websig.i : Envoi fichier pour livraison" x-x.
                        run nt-webgen-liv( input i-param , input i-erreur , output o-param ) . 
                    END.
                END.
            END.
            /* run n-genmail ==> envoi mail a l'opecre + demandeur  */
            FIND TABGES where tabges.codsoc = codsoc-soc               
                        and   tabges.etabli = ""
                        and   tabges.typtab = "CHR"
                        and   tabges.prefix = "TYPCOM"
                        and   tabges.codtab = TRIM( bonent.motcle + bonent.typcom )
                        NO-LOCK NO-ERROR.

            IF AVAILABLE tabges AND tabges.nombre[3] = 1 THEN 
            DO:
                ASSIGN i-from = bonent.opecre
                       i-to   = x-demandeur
                       i-copy = bonent.opecre
                       i-objet = ""
                       i-file = ""
                       i-message = "" 
                       .
                assign  typtab = "PRM"  prefix = "OPERATEUR"    codtab = "MAIL" .
                run n-chparax( output x-x ) .
                if x-x <> "" THEN i-from = entry( 1 , x-x , "/" )    no-error .

           /*     i-objet = deal-get-dictab ( "valsig-nat[LABEL]" , "nom" ) .  */
                RUN n-get-dictab( INPUT "599,DEAL" , INPUT "LABEL" , INPUT "VALSIG-NAT" , INPUT "1" , OUTPUT i-objet ) .
                IF ENTRY( 2 , ligne , "-" ) = "fac" THEN
                    RUN n-get-dictab( INPUT "599,DEAL" , INPUT "LABEL" , INPUT "VALFAC-NAT" , INPUT "1" , OUTPUT i-objet ) .

                IF i-objet = "" THEN i-objet = "Validation de la piece : #01 #02 #03 " .

                i-objet = REPLACE( i-objet , "#01" , bonent.codsoc ).
                i-objet = REPLACE( i-objet , "#02" , bonent.etabli ).
                i-objet = REPLACE( i-objet , "#03" , bonent.typcom + " " + SUBSTR( bonent.numbon , 1 , 8 ) ) .

           /*     i-message = deal-get-dictab ( "valsig-nat[message]" , "designation" ) .  */
                RUN n-get-dictab( INPUT "599,DEAL" , INPUT "message" , INPUT "VALSIG-NAT" , INPUT "1" , OUTPUT i-message ) .
                IF ENTRY( 2 , ligne , "-" ) = "fac" THEN
                    RUN n-get-dictab( INPUT "599,DEAL" , INPUT "message" , INPUT "VALFAC-NAT" , INPUT "1" , OUTPUT i-objet ) .

                IF i-message = "" THEN i-message = "La commande suivante est valid�e #01 " .

                i-message = REPLACE( i-message , "#01" , bonent.codsoc ).  
                i-message = REPLACE( i-message , "#02" , bonent.etabli ).  
                i-message = REPLACE( i-message , "#03" , bonent.typcom + " " + SUBSTR( bonent.numbon , 1 , 8 ) ) .

                RUN n-genmail( INPUT i-from , INPUT i-to , input i-copy ,  
                                INPUT i-objet , INPUT i-file , INPUT i-message ) . 
                IF dbg = YES THEN MESSAGE "nt-websig.i : apres n-genmail ==> " i-from i-to i-message .
           END.

           IF bonent.typcom = "fac" THEN DELETE bonent .
        end.                                         

/*         leave . */

    END .  /*  Acceptation ( 1 ) - Refus ( 0 ) */   
    ELSE DO:  /* gestion du refus de signature */                                                              

        IF dbg = YES THEN MESSAGE "nt-websig.i : Gestion du refus de signature " .                             

        IF bonent.typcom = "FAC" THEN  RUN trt-refus-bap .
        ELSE DO:
            x-x = bonent.opecre .    /*  Matricule   */                                                            
            i-param  = string( rowid( bonent ) ) + "|VIS2"  + "|" + x-x + "|" + ligne .                            
            i-erreur = STRING( i-i )                    + car-sep +                                                
                       ges-operat                       + car-sep +                                                
                       ENTRY( 6 , zone-io-e , car-sep ) + car-sep +                                                
                       ENTRY( 7 , zone-io-e , car-sep ) + car-sep +                                                
                       ENTRY( 8 , zone-io-e , car-sep )                                                            
                       .                                                                                           

            IF dbg = YES THEN MESSAGE "nt-websig.i : Envoi fichier signature refus�e " x-x .                   

            run nt-webgen( input i-param , input i-erreur , output o-param ) .                                     
        END.
        /* run n-genmail ==> envoi mail a l'opecre + demandeur  */                                             
        FIND TABGES where tabges.codsoc = codsoc-soc                                                           
                    and   tabges.etabli = ""                                                                   
                    and   tabges.typtab = "CHR"                                                                
                    and   tabges.prefix = "TYPCOM"                                                             
                    and   tabges.codtab = TRIM( bonent.motcle + bonent.typcom )                                
                    NO-LOCK NO-ERROR.                                                                          

        IF AVAILABLE tabges AND tabges.nombre[3] = 1 THEN                                                      
        DO:                                                                                                    
           FIND d-operat WHERE d-operat.ident     = "DEAL"                                                     
                         AND   d-operat.typtab    = "operat"                                                   
                         AND   d-operat.famoperat = ""                                                         
                         AND   d-operat.codtab    = x-operat                                                   
                         NO-LOCK NO-ERROR .                                                                    

            FIND tabges WHERE TABGES.codsoc = ""                                                               
                        AND   tabges.typtab = "CST"                                                            
                        AND   tabges.prefix = "REFSIGACH"                                                      
                        AND   tabges.codtab = TRIM( ENTRY( 6 , zone-io-e , car-sep ) )                         
                        NO-LOCK NO-ERROR.                                                                      

            ASSIGN i-from = bonent.opecre                                                                      
                   i-to   = bonent.opecre                                                                      
                   i-copy = x-demandeur                                                                        
                   i-file = ""                                                                                 
                   .                                                                                           
            assign  typtab = "PRM"  prefix = "OPERATEUR"    codtab = "MAIL" .                                  
            run n-chparax( output x-x ) .                                                                      
            if x-x <> "" THEN i-from = entry( 1 , x-x , "/" )    no-error .                                    

            i-objet = "" .                                                                                     
        /*    i-objet = deal-get-dictab ( "refsig-nat[LABEL]" , "nom" ) .  */                                  
            RUN n-get-dictab( INPUT "599,DEAL" , INPUT "LABEL" , INPUT "REFSIG-NAT" , INPUT "1" , OUTPUT i-objet ) .
            IF ENTRY( 2 , ligne , "-" ) = "fac" THEN
                RUN n-get-dictab( INPUT "599,DEAL" , INPUT "LABEL" , INPUT "REFFAC-NAT" , INPUT "1" , OUTPUT i-objet ) .

            IF i-objet = "" THEN   i-objet = "Refus de la piece : #01 #02 #03 "  .                                                                                           

            i-objet = REPLACE( i-objet , "#01" , bonent.codsoc ).                                              
            i-objet = REPLACE( i-objet , "#02" , bonent.etabli ).                                              
            i-objet = REPLACE( i-objet , "#03" , bonent.typcom + " " + SUBSTR( bonent.numbon , 1 , 8 ) ) .     
            i-objet = REPLACE( i-objet , "#10" , x-operat ).               /* operateur qui refuse */          
            i-objet = REPLACE( i-objet , "#11" , d-operat.libel[1] ) .    /* nom operateur */                  

            i-message  = "" .                                                                                  
        /*    i-message = deal-get-dictab ( "refsig-nat[message]" , "designation" ) .  */                      
            RUN n-get-dictab( INPUT "599,DEAL" , INPUT "message" , INPUT "refSIG-NAT" , INPUT "1" , OUTPUT i-message ) .
            IF ENTRY( 2 , ligne , "-" ) = "fac" THEN
                RUN n-get-dictab( INPUT "599,DEAL" , INPUT "message" , INPUT "REFFAC-NAT" , INPUT "1" , OUTPUT i-objet ) .

            IF i-message = "" THEN                                                                             
                   i-message = "La commande suivante est refus�e #01 par #10 #11"  + CHR(10)                   
                                + "#12" + CHR(10)  + "#13" + CHR(10) + "#14"                                   
                                .                                                                              
            i-message = REPLACE( i-message , "#01" , bonent.codsoc ).                                          
            i-message = REPLACE( i-message , "#02" , bonent.etabli ).                                          
            i-message = REPLACE( i-message , "#03" , bonent.typcom + " " + SUBSTR( bonent.numbon , 1 , 8 ) ) . 
            i-message = REPLACE( i-message , "#10" , x-operat ).               /* operateur qui refuse */      
            i-message = REPLACE( i-message , "#11" , d-operat.libel[1] ) .    /* nom operateur */              
            IF AVAILABLE tabges THEN  i-message = REPLACE( i-message , "#12" , TRIM( tabges.libel1[1]) ).      
            i-message = REPLACE( i-message , "#13" , ENTRY( 7 , zone-io-e , car-sep ) ).                       
            i-message = REPLACE( i-message , "#14" , ENTRY( 8 , zone-io-e , car-sep ) ).                       


            RUN n-genmail( INPUT i-from , INPUT i-to , input i-copy ,                                          
                            INPUT i-objet , INPUT i-file , INPUT i-message ) .                                 
            IF dbg = YES THEN MESSAGE "nt-websig.i : Apres n-genmail ==>" i-from i-to i-message .                             
       END.                                                                                                    

       RUN del-bon .

       DELETE bonent .

    END.

    RELEASE bonent.
END.

PROCEDURE trt-qualite :
    IF dbg = YES THEN MESSAGE "nt-websig.i : Procedure trt-qualite " zone-io-e .

    ASSIGN x-typcom   = ENTRY( 2 , ligne , "-" )
           x-numbon   = ENTRY( 4 , ligne , "-" ) + "00"
           .

    FIND b-bonent WHERE b-bonent.codsoc = codsoc-soc
                  AND   b-bonent.typcom = x-typcom
                  AND   b-bonent.numbon = x-numbon
                  EXCLUSIVE-LOCK NO-ERROR.

    IF NOT AVAILABLE b-bonent THEN RETURN .

    critere = entry( 2 , zone-io-e , car-sep ) .
    nb-qualit = NUM-ENTRIES( critere , car-sep2 ) .

    /*  Si suivi qualit� : MaJ de BONENS ( Qualit� )  */
    if nb-qualit <> 0  then
    do :
        assign  i-par = "U|BONENT|" + string( rowid( b-BONENT ) ) + "|QUALITE|0"
               o-par = "AK=" + b-bonent.typaux + b-bonent.codaux .
        j-j = 0 .
        do i-i = 1 to nb-qualit :
            x-x = ENTRY( 1 , ENTRY( i-i , critere , car-sep2 ) ) .
            x-y = string( int( ENTRY( 2 , ENTRY( i-i , critere , car-sep2 ) )) , "9" ) .
            if x-x <> ""  then  j-j = j-j + 1 .
            o-par = o-par + "|" + "A" + x-x + "=" + x-y .
        end .
        if j-j <> 0  then  run n-ficsuit( input i-par , input-output o-par ) .
    end .

    RELEASE b-bonent .
END.


PROCEDURE trt-signat-bap :
      /* le circuit de wokflow sur le BAP est totalement sign� : Mise � jour de MVTCPT */

   IF dbg = YES THEN MESSAGE "nt-websig.i : Procedure trt-signat-bap " zone-io-e .

   x-repert = repert-ecr .

   find FIRST stadet where stadet.codsoc = bonent.codsoc
                     and   stadet.motcle = bonent.motcle
                     and   stadet.typcom = bonent.typcom
                     and   stadet.numbon = bonent.numbon
                     no-lock no-error.
    if not available stadet then  
    DO: 
        DELETE bonent.
        RETURN "" .
    END.

    /* si le BAP du docume est d�ja pass� � oui pas de traitement */
    FIND mvtcpt WHERE mvtcpt.codsoc  = stadet.codsoc
                and   mvtcpt.codetb  = stadet.etabli
                and   mvtcpt.docume  = stadet.docume
                and   mvtcpt.bap-top = "O" 
                NO-LOCK NO-ERROR.
    if available mvtcpt then  
    DO: 
        DELETE bonent.
        RETURN "" .
    END.

    { nt-webbap.i }

    IF dbg = YES THEN MESSAGE "nt-websig.i : fin Procedure trt-signat-bap " .
END.

PROCEDURE trt-valid-bap :

      /* le circuit de wokflow sur le BAP est totalement sign� : Mise � jour de MVTCPT */

   IF dbg = YES THEN MESSAGE "nt-websig.i : Procedure trt-valid-bap " zone-io-e .
   x-x = bonent.opecre .

   FIND FIRST stadet WHERE stadet.codsoc = bonent.codsoc
                     AND   stadet.motcle = bonent.motcle
                     AND   stadet.typcom = bonent.typcom
                     AND   stadet.numbon = bonent.numbon
                     NO-LOCK NO-ERROR.
   IF AVAILABLE stadet THEN
   DO:
       x-x = stadet.opecre .    /*  Matricule   */         
        /* si le BAP du docume est pass� � oui pas de traitement */
       FIND FIRST mvtcpt WHERE mvtcpt.codsoc  = stadet.codsoc
                         and   mvtcpt.codetb  = stadet.etabli
                         and   mvtcpt.docume  = stadet.docume
	                     and   mvtcpt.bap-top <> "O" 
                         EXCLUSIVE-LOCK NO-ERROR.
       IF AVAILABLE mvtcpt THEN  
            ASSIGN x-x             = mvtcpt.bap-ope 
                   mvtcpt.bap-top  = "O"
                   mvtcpt.bap-cod  = ""
                   mvtcpt.bap-ope  = x-auteur 
                   mvtcpt.bap-lib  = trim( entry( 7 , zone-io-e , car-sep ) ) + trim( entry( 8 , zone-io-e , car-sep ) )
                   mvtcpt.bap-date = TODAY
                   .

   END.

   IF dbg = YES THEN MESSAGE "nt-websig.i : suppression des fichiers suite validation sur BAP " bonent.opecre .                           
   i-param = string( rowid( bonent ) ) + "|VIS9"  + "|" + x-x + "|" + ligne .     
   run nt-webgen( input i-param , input i-erreur , output o-param ) . 

END.

PROCEDURE trt-refus-bap :
    /*  refus sur le circuit du BAP */
    IF dbg = YES THEN MESSAGE "nt-websig.i : Procedure trt-refus-bap " zone-io-e .

    FIND FIRST stadet WHERE stadet.codsoc = bonent.codsoc
                      AND   stadet.motcle = bonent.motcle
                      AND   stadet.typcom = bonent.typcom
                      AND   stadet.numbon = bonent.numbon
                      NO-LOCK NO-ERROR.
    IF AVAILABLE stadet THEN
    DO:     
    /* g�n�ration dans BONENS de la clef pour edition liste des BAP refuse */
        assign  i-param = "U|STADET|" + string( rowid(  STADET ) ) + "|REFUS-BAP|0".
                o-param = "AK=REFUS-BAP" +
                          "|A02=" + trim( entry( 6 , zone-io-e , car-sep ) ) +                     
                          "|A03=" + trim( entry( 7 , zone-io-e , car-sep ) ) +
                          "|A04=" + trim( entry( 8 , zone-io-e , car-sep ) ) +
                          "|A05=" + ges-operat .
        run n-ficsuit( input i-param , input-output o-param ) .

        FIND FIRST mvtcpt WHERE mvtcpt.codsoc  = stadet.codsoc
                          and   mvtcpt.codetb  = stadet.etabli
                          and   mvtcpt.docume  = stadet.docume
        	              and   mvtcpt.bap-top <> "O"
                          EXCLUSIVE-LOCK NO-ERROR.
        IF AVAILABLE mvtcpt THEN  
            ASSIGN x-x             = mvtcpt.bap-ope 
                   mvtcpt.bap-top  = "N"
                   mvtcpt.bap-cod  = trim( entry( 6 , zone-io-e , car-sep ) )
                   mvtcpt.bap-ope  = x-auteur 
                   mvtcpt.bap-lib  = trim( entry( 7 , zone-io-e , car-sep ) ) + trim( entry( 8 , zone-io-e , car-sep ) )
                   mvtcpt.bap-date = TODAY
                   .
    END.

    IF dbg = YES THEN MESSAGE "nt-websig.i : suppression des fichiers suite refus sur BAP " bonent.opecre .
    x-x = bonent.opecre .    /*  Matricule   */                                   
    i-param = string( rowid( bonent ) ) + "|VIS9"  + "|" + x-x + "|" + ligne .     
    run nt-webgen( input i-param , input i-erreur , output o-param ) . 
END.