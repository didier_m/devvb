/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/nathalie/inc/nt-webrcf.i                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  4980 09-05-03!Bug!bp             !l'analytique n'est pas charg�e en ent�te d'une r�ception              !
!V52  4927 28-04-03!Bug!bp             !Pb sur le refus de BAP par le web                                     !
!V52  4901 23-04-03!Bug!bp             !les commandes ne sont pas sold�es en automatique lorsque toues les lig!
!V52  4798 11-04-03!Bug!bp             !la zone contenenat le fichier scan n'est pas remis a blanc            !
!V52  4766 10-04-03!New!bp             !Pb sur r�cup�ration du codaux du nom de fichier                       !
!V52  4513 14-03-03!New!bp             !Dans le cas de g�n�ration d'une r�ception, suppression du fichier de v!
!V52  4460 11-03-03!New!bp             !mise en place du module d'echange avec le web pour Eurogem            !
!V52  4198 12-02-03!New!bp             !Bug lorsqu'en receptions, la ligne de commande n'existe plus          !
!V52  4197 12-02-03!New!bp             !ajout message our le debbugage des criteres                           !
!V52  4114 05-02-03!New!bp             !Mise en place retour critere qualite commande et r�ception            !
!V52  4060 29-01-03!New!bp             !Les RCF cr�es doivent �tre valid�es automatiquements                  !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/*-----------------------------------------------------------------------------*/
/*           N T - W E B R C F . I  : Integration des receptions       */
/*-----------------------------------------------------------------------------*/

REPEAT TRANSACTION :

    DEF VAR err-log AS LOG NO-UNDO .
    DEF VAR recept-gen AS CHAR NO-UNDO .

    MESSAGE "nt-webrcf.i : Int�gration du fichier" ligne  STRING(TODAY,"99/99/9999") STRING(TIME,"hh:mm:ss").

/* MESSAGE "nt-wefrcf.i" ligne            */
/*     VIEW-AS ALERT-BOX INFO BUTTONS OK. */
/*                                        */
    x-action = ENTRY( 6 , ligne , "-" ) .

    assign  nb-lus   = 0
            x-chrono = 0
            radic-appui = "ZZZZZ" 
            commande-gen = "" .

    INPUT stream kan-fic from value( fichier-lec ) .


    codsoc-soc = entry( 3 , ligne , "-" ) NO-ERROR .
    codetb-etb = entry( 4 , ligne , "-" ) NO-ERROR .

    operat = ENTRY( 2 , ligne , "." ) .

    glo-codsoc = codsoc-soc .
    glo-etabli = codetb-etb .

    regs-app = "NATHALIE" .
    { regs-cha.i }
    regs-app = "ELODIE" .
    { regs-cha.i }

    { regs-inc.i  AUXSPE }
    auxges-soc = regs-soc .

    { regs-inc.i  ARTICL }
    articl-soc = regs-soc .

    { regs-inc.i  MAGASI }
    magasi-soc = regs-soc .

    { regs-inc.i  IMPAPR }
    impapr-soc = regs-soc .

    { regs-inc.i  HABILI }
    habili-soc = regs-soc .

    { regs-inc.i  TARFOU }
    tarfou-soc = regs-soc .

    { regs-inc.i  TARCLI }
    tarcli-soc = regs-soc .

    assign  typtab = "PRM"  prefix = "IMPORT-" + x-typtrt   codtab = "PARAM" .
    run n-chparax( output x-x ) .
    if x-x <> "" then
    do :
        x-magasin =      entry( 1 , x-x , "/" )    no-error .
        x-pas     = dec( entry( 2 , x-x , "/" ) )  no-error .
        ind-qte   = dec( entry( 3 , x-x , "/" ) )  no-error .
    end .
    if x-magasin = ""  then  x-magasin = "1" .
    if x-pas < 1  or  x-pas > 100  then  x-pas = 5 .
    if ind-qte <> 1  and  ind-qte <> 2  then  ind-qte = 1 .
    cde-rang = ind-qte .

    /*  Chargement de la Devise de Gestion  */
    assign  typtab = "PRM"  prefix = "TX-EUR-FRF"  codtab = "" .
    run n-chparax( output x-x ) .
    if x-x <> ""  then  dev-gescom  = entry( 1 , x-x , "/" ) NO-ERROR .
    if dev-gescom = ""  then  dev-gescom = "EUR" .

    /*  Chargement du parametrage analytique ( Pour la Gestion )  */
    assign  typtab = "PRM"  prefix = "PARAM-ANA"  codtab = x-motcle + x-typcom .
    run n-chparax( output x-x ) .
    if x-x = ""  then
    do :
        assign  typtab = "PRM"  prefix = "PARAM-ANA"  codtab = x-motcle .
        run n-chparax( output x-x ) .
        if x-x = ""  then
        do :
            assign  typtab = "PRM"  prefix = "PARAM-ANA"  codtab = "" .
            run n-chparax( output x-x ) .
        end .
    end .
    if x-x <> ""  then  param-ana = entry( 1 , x-x , "/" ) NO-ERROR . 

    /*  Parametrage des largeurs des zones description  */
    assign  typtab = "PRM"  prefix = "SAI-LGCOMM"  codtab = x-motcle + x-typcom .
    run n-chparax( output x-x ) .
    if x-x = ""  then
    do :
        assign  typtab = "PRM"  prefix = "SAI-LGCOMM"  codtab =x-motcle .
        run n-chparax( output x-x ) .
    end .

    i-i = 104 .
    if x-x <> ""  then
    do :
        i-i = int( entry( 11 , x-x , "/" ) ) .
        if i-i > 20  and  i-i < 150  THEN  x-x = string( i-i ) .
                                     else  i-i = 104 .
    end .
    i-i = i-i / 1.43 .
    x-lgcomm = string( i-i ) .

    run charana .

    case soetaff :
        when 0 then assign aff-codsoc = ""
                           aff-etabli = "" .
        when 1 then assign aff-codsoc = glo-codsoc
                           aff-etabli = "" .
        when 2 then assign aff-codsoc = glo-codsoc
                           aff-etabli = glo-etabli .
    end case .

    assign  x-motcle = "ACH"          
            x-typaux = "F"
            x-codaux = ENTRY( 7 , ligne , "-" )
            x-typcom = "RCF"
            .
    x-codaux = REPLACE ( x-codaux , "�" , "-" ) .

    assign  typtab = "PRM"  prefix = "TYPCOM"  codtab = "FILIERE" .
    run n-chparax( output x-x ) .

    IF x-x <> "" THEN x-typcom = ENTRY( 10 , x-x , "/" ) .

    IF x-typcom = "" THEN
    DO:
        FIND FIRST TYPBON where typbon.codsoc = codsoc-soc
                          and   typbon.motcle = x-motcle
                          no-lock  no-error .
        IF NOT AVAILABLE typbon THEN LEAVE .

        x-typcom = typbon.typcom .
    END.

    IF ENTRY( 5 , ligne , "-" ) = "" THEN   
         IF dbg = YES THEN MESSAGE "nt-webrcf.i : d�but g�n�ration entete piece r�ception " .
         RUN cre-entete-rcf.
    ELSE DO:
        IF dbg = YES THEN MESSAGE "nt-webrcf.i : d�but retour MAJ BAP par r�ceptionnaire" .
        /*  cas modification du BAP apres controle facture */

        FIND B-BONENT where b-bonent.codsoc = glo-codsoc
                      and   b-bonent.motcle = x-motcle
                      and   b-bonent.typcom = entry( 2 , ligne , "-" )
                      and   b-bonent.numbon = string( dec( entry( 5 , ligne , "-" )) , ">>>>>>>9" )  + "00"
                      no-lock  no-error . 

        IF AVAILABLE b-bonent THEN
        DO:
            /* g�n�ration dans BONENS de la clef pour edition liste des BAP ok */
            assign  i-param = "U|BONENT|" + string( rowid( B-BONENT ) ) + "|VALID-BAP|0".
                    o-param = "AK=VALID-BAP" .               

            run n-ficsuit( input i-param , input-output o-param ) .

            /* suppression de tous les blocage-bap dan la pice */
            find bonens where bonens.codsoc = b-bonent.codsoc
                        and   bonens.motcle = b-bonent.motcle
                        and   bonens.typcom = b-bonent.typcom
                        and   bonens.numbon = b-bonent.numbon
                        AND   bonens.theme  = "BLOCAGE-BAP"
                        EXCLUSIVE-LOCK NO-ERROR .
            IF AVAILABLE bonens THEN DELETE bonens.

        END.

        LEAVE .

    END.

    IF dbg = YES THEN MESSAGE "nt-webrcf.i : d�but g�n�ration ligne piece r�ception " .
    REPEAT :
    /* G�n�ration des lignes de r�ception */

        zone-io-l = "" .
        ctrl-message = "" .
        IMPORT stream kan-fic unformatted zone-io-l .

        if entry( 1 , zone-io-l , car-sep ) = "CRITERE"  then  
        DO:
            IF dbg = YES THEN MESSAGE "nt-webrcf.i : int�gration des crit�res de qualit�" .
            critere = entry( 2 , zone-io-l , car-sep ) .
            nb-qualit = NUM-ENTRIES( critere , car-sep2 ) - 1 .

            /*  Si suivi qualit� : MaJ de BONENS ( Qualit� )  */
            if nb-qualit <> 0  then
            do :
                assign  i-par = "U|BONENT|" + string( rowid( b-BONENT ) ) + "|QUALITE|0"
                       o-par = "AK=" + b-bonent.typaux + b-bonent.codaux .
                j-j = 0 .
                do i-i = 1 to nb-qualit :
                    x-x = ENTRY( 1 , ENTRY( i-i , critere , car-sep2 ) ) .
                    x-y = string( int( ENTRY( 2 , ENTRY( i-i , critere , car-sep2 ) )) , "9" ) NO-ERROR.
                    if x-x <> ""  then  j-j = j-j + 1 .
                    o-par = o-par + "|" + "A" + x-x + "=" + x-y .
                end .
                if j-j <> 0  then  run n-ficsuit( input i-par , input-output o-par ) .
            end .
        END.

        if entry( 1 , zone-io-l , car-sep ) <> "L-LIGNES"  then  NEXT .

        err-log = NO .

        IF b-bonent.segana[ 1] = ""  THEN
        DO:
            FIND bonent WHERE bonent.codsoc = codsoc-soc
                        AND   bonent.motcle = "ACC"
                        AND   bonent.typcom = ENTRY( 2 , zone-io-l , car-sep )
                        AND   bonent.numbon = string( dec( ENTRY( 3 , zone-io-l , car-sep ) ) , ">>>>>>>9" ) + "00"
                        NO-LOCK NO-ERROR.
            IF AVAILABLE bonent THEN
                ASSIGN  b-bonent.segana[ 1 ] = bonent.segana[ 1 ]
                        b-bonent.segana[ 2 ] = bonent.segana[ 2 ]
                        b-bonent.segana[ 3 ] = bonent.segana[ 3 ]
                        .
        END.

        RUN cre-ligne-rcf .

        IF err-log = YES THEN
        DO:
            MESSAGE "nt-webrcf.i - ERREUR " zone-io-l ctrl-message .
            /* ligne en erreur , la renvoy�e � l'operateur */

        END.

    END.

    z-z = NUM-ENTRIES( recept-gen , car-sep ) - 1.

    DO y-y = 1 TO z-z:

        x-x = "HT,RHT,TTC,RTT,PDS,VOL,QTE" .

        i-param = ENTRY( y-y , recept-gen , car-sep ) + "|" + x-x + "|" + articl-soc .

        run n-totbon( input i-param , output o-param ) NO-ERROR . 
    END.

    /* renvoie des commandes restant � receptionn�es avec nlles qte */
    IF dbg = YES THEN MESSAGE "nt-webrcf.i : phase de renvoi des fichiers avec MAJ du restant � livrer " .

    z-z = NUM-ENTRIES( commande-gen , car-sep ) - 1.

    DO y-y = 1 TO z-z:

        x-x = "HT,RHT,TTC,RTT,PDS,VOL,QTE" .

        i-param = ENTRY( y-y , commande-gen , car-sep ) + "|" + x-x + "|" + articl-soc .

        run n-totbon( input i-param , output o-param ) NO-ERROR . 

        ent-rowid = to-rowid( entry( y-y , commande-gen , car-sep ) ) .

        /*  Apr�s MAJ : envoi au 1er signataire   */  
        FIND BONENT where rowid( bonent ) = ent-rowid  
                    NO-LOCK NO-ERROR.
        IF NOT AVAILABLE bonent THEN
        DO:
             IF dbg = YES THEN MESSAGE "nt-webrcf.i : piece non trouvee !!!!!!!!!!! " .
            NEXT .
        END.
        IF bonent.solde-liv = 1 THEN  
        DO:
            IF dbg = YES THEN MESSAGE "nt-webdeb.i : Avant nt-webgen: suppression fichier sur la commande r�ceptionn�e chez " bonent.opecre .
            x-x = bonent.opecre .    /*  Matricule   */   
            i-param = string( rowid( bonent ) ) + "|VIS3"  + "|" + x-x + "|" + ligne .     
            run nt-webgen( input i-param , input i-erreur , output o-param ) . 
            NEXT .
        END.

        /* renvoie du fichier de prereception si commande est livrable */
        x-x = bonent.opecre .    /*  Matricule   */                                   
        i-param = string( rowid( bonent ) ) + "|RCF"  + "|" + x-x + "|" + ligne .  

        IF dbg = YES THEN MESSAGE "nt-webrcf.i : avant nt-webgen-liv " i-param .
        run nt-webgen-liv( input i-param , input i-erreur , output o-param ) . 

    END.

    LEAVE.
END.

/************************************************************************/
/*                       P  R  O  C  E  D  U  R  E  S                  */
/**********************************************************************/

PROCEDURE cre-entete-rcf:

    fou-ok = YES .

    REPEAT WHILE fou-ok = YES:
        i-param = x-motcle + "|" + x-typcom + "|O" .
        o-param = "".

        run n-nocom( input i-param , input-output o-param ) .

        x-numbon = o-param + "00" .
        FIND B-BONENT where b-bonent.codsoc = codsoc-soc
                      and   b-bonent.motcle = x-motcle
                      and   b-bonent.typcom = x-typcom
                      and   b-bonent.numbon = x-numbon
                      USE-INDEX primaire no-lock  no-error .

        if NOT available b-bonent THEN fou-ok = NO .
    END.

    x-chrono = 0 .

    CREATE  B-BONENT .
    assign  b-bonent.codsoc = codsoc-soc
            b-bonent.etabli = codetb-etb
            b-bonent.motcle = x-motcle
            b-bonent.typcom = x-typcom
            b-bonent.numbon = x-numbon .

    FIND BONENT where bonent.codsoc = codsoc-soc
                and   bonent.motcle = x-motcle
                and   bonent.typcom = x-typcom
                and   bonent.numbon = radic-appui + operat + "00"
                USE-INDEX primaire no-lock  no-error .
    if not available bonent  then
    do :
        FIND BONENT where bonent.codsoc = codsoc-soc
                    and   bonent.motcle = x-motcle
                    and   bonent.typcom = x-typcom
                    and   bonent.numbon = radic-appui + "000" + "00"
                    USE-INDEX primaire no-lock  no-error .
    end .
    if available bonent  then
    do :
        { n-duplent.i  b-bonent  bonent }

    end .

    FIND B-TYPBON where b-typbon.codsoc = codsoc-soc
                  and   b-typbon.motcle = x-motcle
                  and   b-typbon.typcom = x-typcom
                  no-lock  no-error .


    { regs-inc.i  "AUXILI" + x-typaux }
    { cadr-aux.i  regs-soc  x-typaux  x-codaux }

    FIND AUXGES where auxges.codsoc = auxges-soc
                and   auxges.typaux = x-typaux
                and   auxges.codaux = x-codaux
                no-lock  no-error .
    if not available auxges THEN leave .

    assign  b-bonent.typaux         = x-typaux
            b-bonent.codaux         = x-codaux
            b-bonent.magasin        = x-magasin
            b-bonent.devise         = auxges.devise 
            b-bonent.typbon         = "E"
            b-bonent.typaux-fac     = auxges.typaux-fac
            b-bonent.codaux-fac     = auxges.codaux-fac
            b-bonent.modreg         = auxges.modreg
            b-bonent.codech         = auxges.codech
            b-bonent.domapr         = auxges.domapr
            b-bonent.ttva           = auxges.ttva
            b-bonent.codpri         = auxges.code-tarif
            b-bonent.nom-contact    = auxges.nom-respon
            b-bonent.tel-contact    = auxges.teleph-respon
            b-bonent.fax-contact    = auxges.fax-respon
            b-bonent.repres[ 1 ]    = auxges.repres[ 1 ]
            b-bonent.adres[ 1 ]     = auxges.adres[ 1 ]
            b-bonent.adres[ 2 ]     = auxges.adres[ 2 ]
            b-bonent.adres[ 3 ]     = auxges.adres[ 3 ]
            b-bonent.adres[ 4 ]     = auxges.adres[ 4 ]
            b-bonent.adres[ 5 ]     = auxges.adres[ 5 ]
            b-bonent.adres-liv[ 1 ] = auxges.adres[ 1 ]
            b-bonent.adres-liv[ 2 ] = auxges.adres[ 2 ]
            b-bonent.adres-liv[ 3 ] = auxges.adres[ 3 ]
            b-bonent.adres-liv[ 4 ] = auxges.adres[ 4 ]
            b-bonent.adres-liv[ 5 ] = auxges.adres[ 5 ] 
            .


  /*  Date de Commande  */
    b-bonent.datbon = today .
    b-bonent.dattar = b-bonent.datbon .

    /*  Date de Livraison Prevue  */
    b-bonent.datdep = today .

    b-bonent.datliv = b-bonent.datdep .

    b-bonent.ref-magasin = x-typcom + right-trim( x-bonref ) .

/*     b-bonent.segana[ 1 ] = affaire .                                                   */
/*     b-bonent.segana[ 2 ] = entry( 15 , zone-io-e , car-sep ) . /* code sous-affaire */ */
/*     b-bonent.segana[ 3 ] = entry( 16 , zone-io-e , car-sep ) . /* code nature */       */

     i-i = int( auxges.code-langue ) .
     if i-i < 1  then  i-i = 1 .
     b-bonent.code-langue = i-i .

  /*  Taux Devise  */
    tx-dev = 1 .
    if auxges.devise <> dev-gescom  then
    do :
        assign  bi-codsoc = codsoc-soc  bi-codetb = codetb-etb     bi-execpt = ""    bi-percpt = ""
                bi-datpie = today       bi-devini = x-devise       bi-mtini  = 1000
                bi-motcle = "$GESCOM_" + dev-gescom .
        run out-mona( input  bi-codsoc , input  bi-codetb , input  bi-execpt ,  input  bi-percpt ,
                      input  bi-datpie , input  bi-devini , input  bi-mtini  , input  bi-motcle ,
                      output bi-mtpie  ,  output bi-mtcpt  , output bi-txpie , output tx-dev  ,
                      output bi-devcpt , output bi-devpie ,  output bi-monnaie, output bi-retour ) .
        if bi-monnaie = 0  then  tx-dev = bi-txpie .
        if dev-gescom <> bi-devcpt
        then  do :
            r-r = dec( entry( 15 , bi-retour , "_" ) ) no-error .
            if r-r <> 0  then  tx-dev = r-r / 1000 .
        end .
    end .

    b-bonent.txdev = tx-dev .

     /*  Transport : Lecture de AUXGES ( TRANSPORT )  */
     i-param = "R|AUXGES|" + string( rowid( AUXGES ) ) + "|TRANSPORT|0" .
     run n-ficsuit( input i-param , input-output o-param ) .
     if int( entry( 1 , o-param , "|" ) ) = 0  then
     do :
        do i-i = 3 to num-entries( o-param , "|" ) :
            assign  x-z = entry ( i-i , o-param   , "|"  )
                    x-x = entry ( 1 , x-z , "="  )
                    x-y = entry ( 2 , x-z , "="  ) NO-ERROR .
            if x-x = "A01"  then  b-bonent.transp      = x-y .
            if x-x = "A02"  then  b-bonent.lieu-depart = x-y .
            if x-x = "A03"  then  b-bonent.modliv      = x-y .
            if x-x = "A04"  then  b-bonent.modtrp      = x-y .
        end .
    end .

    i-param = b-bonent.magasin + "|" + b-bonent.lieu-depart + "|" + "|" + x-motcle + "|" +
              b-bonent.codaux + "|" + string( b-bonent.datdep , "99/99/9999" )  .
    run n-ccoutrp( input i-param , output o-param ) .
    if entry( 1 , o-param , "|" ) <> ""  then
    do :
        b-bonent.typ-mttrp = entry( 1 , o-param , "|" ) NO-ERROR .
        b-bonent.cout-trp  = dec( entry( 2 , o-param , "|" ) ) NO-ERROR .
    end .

    /*  Si suivi CEE : MaJ de AUXGES ( CEE )  */
    mat-work = "" .
    i-param = "R|AUXGES|" + string( rowid( AUXGES ) ) + "|CEE|0" .
    run n-ficsuit( input i-param , input-output o-param ) .
    if int( entry( 1 , o-param , "|" ) ) = 0  then
    do :
        do i-i = 3 to num-entries( o-param , "|" ) :
            assign  x-z = entry ( i-i , o-param   , "|"  )
                    x-x = entry ( 1 , x-z , "="  )
                    x-y = entry ( 2 , x-z , "="  ) NO-ERROR .
            if x-x = "A01"  then  mat-work[ 1 ] = x-y .
            if x-x = "A02"  then  mat-work[ 2 ] = x-y .
            if x-x = "A03"  then  mat-work[ 3 ] = x-y .
            if x-x = "A04"  then  mat-work[ 4 ] = x-y .
            if x-x = "A05"  then  mat-work[ 5 ] = x-y .
            if x-x = "A06"  then  mat-work[ 6 ] = x-y .
        end .
    end .
    if mat-work[ 2 ] = "O"  then
    do :
        assign  i-param = "U|BONENT|" + string( rowid( B-BONENT ) ) + "|CEE|0"
                o-param = "AK="   + string( x-typaux ,   "xxx" ) +
                                    string( x-codaux , "x(10)" ) +
                          "|A01=" + mat-work[ 1 ] + "|A02=" + mat-work[ 2 ] +
                          "|A03=" + mat-work[ 3 ] + "|A04=" + mat-work[ 4 ] +
                          "|A05=" + mat-work[ 5 ] + "|A06=" + mat-work[ 6 ] .
        run n-ficsuit( input i-param , input-output o-param ) .
    end .

     b-bonent.nivcom = "3" .
     b-bonent.stat-valide = 1 .

    { majmoucb.i b-bonent }

    IF LOOKUP( string( rowid( b-BONENT ) ) , recept-gen , car-sep ) = 0 THEN
        recept-gen = recept-gen + string( rowid( b-BONENT ) ) + car-sep .

END.

PROCEDURE cre-ligne-rcf :

    IF dbg = YES THEN MESSAGE "nt-webrcf.i :  procedure cre-ligne-rcf - ligne"  zone-io-l .
    /* recherche de la ligne de commande a livr�e */

    FIND bonlig WHERE bonlig.codsoc = codsoc-soc
                AND   bonlig.motcle = "ACC"
                AND   bonlig.typcom = ENTRY( 2 , zone-io-l , car-sep )
                AND   bonlig.numbon = string( dec( ENTRY( 3 , zone-io-l , car-sep ) ) , ">>>>>>>9" ) + "00"
                AND   bonlig.chrono = int( ENTRY( 4 , zone-io-l , car-sep ) )
                EXCLUSIVE-LOCK NO-ERROR .
    IF NOT AVAILABLE bonlig THEN 
    DO:
        err-log = YES.
        ctrl-message = "Ligne � r�ceptionn�e supprim�e ..." .
        RETURN .
    END.
    IF bonlig.top-livre = "1" THEN
    DO:
        err-log = YES.
        ctrl-message = "Ligne d�ja sold�e en livraison ..." .
        RETURN .
    END.
    IF DEC( ENTRY( 12 , zone-io-l , car-sep ) ) > ( bonlig.qte[1] - bonlig.qte[2] ) THEN
    DO:
        err-log = YES.
        ctrl-message = "La quantit� r�ception�e est sup�rieure � la quantit� attendue ..." .
        RETURN .
    END.

    x-chrono = x-chrono + x-pas .

    CREATE b-bonlig .
    BUFFER-COPY BONLIG  except bonlig.motcle  bonlig.typcom  bonlig.numbon
                               bonlig.chrono
                        TO b-BONLIG
                        assign  b-bonlig.motcle = b-bonent.motcle
                                b-bonlig.typcom = b-bonent.typcom
                                b-bonlig.numbon = b-bonent.numbon
                                b-bonlig.chrono = x-chrono .

    ASSIGN b-bonlig.qte[ 1 ]   = 0 
           b-bonlig.qte[ 2 ]   = DEC( ENTRY( 12 , zone-io-l , car-sep ) )
           b-bonlig.motcle-cde = bonlig.motcle
           b-bonlig.typcom-cde = bonlig.typcom
           b-bonlig.numbon-cde = bonlig.numbon
           b-bonlig.chrono-cde = bonlig.chrono
           b-bonlig.nivcom     = "3"
           .
    { majmoucb.i  b-bonlig }


    IF ENTRY( 14 , zone-io-l , car-sep ) = "0" or
       DEC( ENTRY( 11 , zone-io-l , car-sep ) ) <> DEC( ENTRY( 12 , zone-io-l , car-sep ) ) THEN
    DO:      
        assign  i-param = "U|BONLIG|" + string( rowid( B-BONLIG ) ) + "|RECEPT-BAP|0".
                o-param = "A01=" + ENTRY( 13 , zone-io-l , car-sep ) + 
                          "|A02=" + substr( ENTRY( 15 , zone-io-l , car-sep ) ,1 , 32 ) +
                          "|A03=" + ENTRY( 14 , zone-io-l , car-sep )   +
                          "|N01=" + string( DEC( ENTRY( 11 , zone-io-l , car-sep ) ) )                 
                          .
        IF ENTRY( 15 , zone-io-l , car-sep ) <> "" THEN
        DO:
            x-x = b-bonlig.typcom + b-bonlig.numbon + string( b-bonlig.chrono  , "9999" ) .

            i-par = x-lgcomm .
            o-par =  ENTRY( 15 , zone-io-l , car-sep ) .
            run dt-chr10( input i-par , input-output o-par ) .

            i-par = b-bonlig.motcle + "|" + b-bonlig.codsoc +
                        "|" + "" + "|" + x-x + "|" + "BAP-TX" .

            run n-majprgl( input i-par , input o-par ) . 
            /* dans nombre[1] qte recue */

            run n-ficsuit( input i-param , input-output o-param ) .
        END.
    END.

    /*  Mise a Jour Proglib ( Texte Libell� )  */
    IF ENTRY( 17 , zone-io-l , car-sep ) <> "" THEN
    DO:
        x-x = b-bonlig.typcom + b-bonlig.numbon + string( b-bonlig.chrono  , "9999" ) .

        i-par = x-lgcomm .
        o-par =  ENTRY( 17 , zone-io-l , car-sep ) .
        run dt-chr10( input i-par , input-output o-par ) .

        i-par = b-bonlig.motcle + "|" + b-bonlig.codsoc +
                    "|" + "" + "|" + x-x + "|" + "LIG-TX" .

        run n-majprgl( input i-par , input o-par ) . 
    END.

    b-bonent.ref-tiers = entry( 16 , zone-io-l , car-sep ) .

    IF dbg = YES THEN MESSAGE "nt-webrcf.i : fin creation ligne " b-bonlig.codsoc b-bonlig.typcom  b-bonlig.numbon b-bonlig.chrono.


    /*  mise a jour de la ligne de commande initiale */
/*     FIND bonlig WHERE bonlig.codsoc = codsoc-soc    */
/*                 AND   bonlig.motcle = bonlig.motcle */
/*                 AND   bonlig.typcom = bonlig.typcom */
/*                 AND   bonlig.numbon = bonlig.numbon */
/*                 AND   bonlig.chrono = bonlig.chrono */
/*                 EXCLUSIVE-LOCK NO-ERROR .           */

    bonlig.qte[ 2 ] = bonlig.qte[ 2 ] + DEC( ENTRY( 12 , zone-io-l , car-sep ) ) .

    IF ENTRY( 18 , zone-io-l , car-sep ) = "1" THEN bonlig.top-livre = "1" .

    if bonlig.qte[ 2 ] >= bonlig.qte[ 1 ] THEN  bonlig.top-livre = "1" .

    IF bonlig.top-livre = "1" THEN  run maj-solliv . 

    { majmoucb.i  bonlig }

    /*  memorisation dans le vect commande-gen des pieces qu'il faudra renvoyer */
    FIND BONENT where bonent.codsoc = bonlig.codsoc
                and   bonent.motcle = bonlig.motcle
                and   bonent.typcom = bonlig.typcom
                and   bonent.numbon = bonlig.numbon
                NO-LOCK NO-ERROR .

    IF LOOKUP( string( rowid( BONENT ) ) , commande-gen , car-sep ) = 0 THEN
        commande-gen = commande-gen + string( rowid( BONENT ) ) + car-sep .

    IF dbg = YES THEN MESSAGE "nt-webrcf.i : fin MAJ ligne " bonlig.codsoc bonlig.typcom  bonlig.numbon bonlig.chrono.

    RELEASE bonlig .
    RELEASE b-bonlig .

END.

PROCEDURE MAJ-SOLLIV :
    IF dbg = YES THEN MESSAGE "nt-webrcf.i : mise a jour solde livraison de " bonlig.codsoc bonlig.typcom  bonlig.numbon .

    def var i-par       as char         no-undo .
    def var o-par       as char         no-undo .

    FIND FIRST C-BONLIG where c-bonlig.codsoc    =  bonlig.codsoc
                        and   c-bonlig.motcle    =  bonlig.motcle
                        and   c-bonlig.typcom    =  bonlig.typcom
                        and   c-bonlig.numbon    =  bonlig.numbon
                        and   c-bonlig.chrono   <>  bonlig.chrono
                        and   c-bonlig.articl   <>  ""
                        and   c-bonlig.top-livre =  ""
                        no-lock  no-error .
    if available c-bonlig  then  return .

    FIND BONENT where bonent.codsoc = bonlig.codsoc
                and   bonent.motcle = bonlig.motcle
                and   bonent.typcom = bonlig.typcom
                and   bonent.numbon = bonlig.numbon
                exclusive-lock  no-error .
    if available bonent  then  bonent.solde-liv = 1 .

    i-par = "Update|BONENT|SLL" .
    o-par = string( rowid( bonent ) ) .
    run n-majtrte( i-par , input-output o-par ) .

    RELEASE bonent .

END .  /*  MAJ-SOLLIV  */


