/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/nathalie/inc/nt-webrqt.i                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  4926 28-04-03!Bug!bp             !Pb sur le cadrage des num�ro de bon dans le module de gestion des requ!
!V52  4889 22-04-03!Evo!bp             !en demande de piece d'archivage, remontee toutes les pieces dont l'ope!
!V52  3271 21-11-02!New!bp             !Retour modifications �ffectu�es chez le client 560                    !
!V52  3147 12-11-02!New!bp             !Mise en place d'un debbuger quand gestion en batch                    !
!V52  2156 06-08-02!New!Bernard PUISSAN!developpement du modules d'integration de la deb                      !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/*-----------------------------------------------------------------------------------------------*/
/*           N T - W E B R Q T . I  : Generation des fichiers suite a une requete du web        */
/*---------------------------------------------------------------------------------------------*/

REPEAT TRANSACTION :
    MESSAGE "nt-webrqt.i : gestion de la requete du fichier" ligne  STRING(TODAY,"99/99/9999") STRING(TIME,"hh:mm:ss") .

    /*******************************************/
    /* phase 1 - analyse du fichier requete   */
    /*****************************************/

    codsoc-soc = ENTRY( 3 , ligne , "-" ) .

    glo-codsoc = codsoc-soc .
    glo-etabli = codetb-etb .

    regs-app = "NATHALIE" .
    { regs-cha.i }
    regs-app = "ELODIE" .
    { regs-cha.i }

    { regs-inc.i  AUXSPE }
    auxges-soc = regs-soc .

    { regs-inc.i  ARTICL }
    articl-soc = regs-soc .

    { regs-inc.i  MAGASI }
    magasi-soc = regs-soc .

    { regs-inc.i  IMPAPR }
    impapr-soc = regs-soc .

    { regs-inc.i  HABILI }
    habili-soc = regs-soc .

    { regs-inc.i  TARFOU }
    tarfou-soc = regs-soc .

    { regs-inc.i  TARCLI }
    tarcli-soc = regs-soc .


    INPUT stream kan-fic from value( fichier-lec ) .

    find first tabges where tabges.codsoc = ""            
                   AND   tabges.etabli = ""           
                   and   tabges.typtab = "APR"        
                   and   tabges.prefix = "OPEACH" 
                   and   tabges.libel1[ 2 ] = x-auteur
                   no-lock no-error.
    IF NOT AVAILABLE tabges THEN RETURN .
    ASSIGN operat-mini = tabges.codtab
           operat-maxi = tabges.codtab
           typcom-mini = ""
           typcom-maxi = "zzz"
           fourni-mini = ""
           fourni-maxi = "zzzzzzzzzz"
           articl-mini = ""
           articl-maxi = "zzzzzzzzzzzzz"
           numbon-mini = ""
           numbon-maxi = "99999999"
           reffou-mini = ""
           reffou-maxi = "zzzzzzzzzzzzzz"
           .

    ASSIGN datcre-mini = DATE("01/01/2000") 
           datcre-maxi = DATE("01/01/2099")
           .

    IF dbg = YES THEN MESSAGE "nt-webrqt.i - debut analyse fichier demande " .
    analyse:
    REPEAT:
          /* 1ere ligne du fichier =  Ligne d'Ent�te  */
        zone-io-e = "" .
        IMPORT stream kan-fic unformatted zone-io-e .

        IF NUM-ENTRIES( zone-io-e , "|" ) < 3 THEN LEAVE analyse.

        x-x = ENTRY( 1 , zone-io-e , car-sep ) . 
        IF ENTRY( 2 , zone-io-e , car-sep) = "EQ" THEN 
                    ASSIGN z-mini = ENTRY( 3 , zone-io-e , car-sep)
                           z-maxi = ENTRY( 3 , zone-io-e , car-sep)
                           .
        IF ENTRY( 2 , zone-io-e , car-sep) = "INC" THEN 
                    ASSIGN z-mini = entry( 1 , ENTRY( 3 , zone-io-e , car-sep) , car-sep2 )
                           z-maxi = ENTRY( 2 , ENTRY( 3 , zone-io-e , car-sep) , car-sep2 )
                           .
        CASE x-x :
            WHEN "typpie" THEN 
                ASSIGN typcom-mini = z-mini
                       typcom-maxi = z-maxi
                       .

            WHEN "fourni" THEN 
            DO:
                x-y = "f" .
                regs-fileacc = "AUXILI" + x-y .
                { regs-rec.i }
                { cadr-aux.i  regs-soc  x-y  z-mini }
                { cadr-aux.i  regs-soc  x-y  z-maxi }
                ASSIGN fourni-mini = z-mini
                       fourni-maxi = z-maxi
                       .
            END.
            WHEN "articl" THEN 
                ASSIGN articl-mini = z-mini
                       articl-maxi = z-maxi
                       .

             WHEN "numbon" THEN 
                ASSIGN numbon-mini = string( int(z-mini) , ">>>>>>>9" ) + "00"
                       numbon-maxi = string( int(z-maxi) , ">>>>>>>9" )  + "00"
                       .

            WHEN "datcre" THEN do: 
                datcre-mini = date( int( substring( z-mini , 3 , 2 ) ) ,
                                    int( substring( z-mini , 1 , 2 ) ) ,
                                    int( substring( z-mini , 5 , 4 ) ) )  no-error .

                datcre-maxi = date( int( substring( z-maxi , 3 , 2 ) ) ,
                                    int( substring( z-maxi , 1 , 2 ) ) ,
                                    int( substring( z-maxi , 5 , 4 ) ) )  no-error .
            END.

            WHEN "reffou" THEN 
                ASSIGN reffou-mini = z-mini
                       reffou-maxi = z-maxi
                       .
/* enlever car il faut s�lectionner toutes les pieces dont l'operateur fait partie du circuit de signature */
/*             WHEN "operat" THEN              */
/*                 ASSIGN operat-mini = z-mini */
/*                        operat-maxi = z-maxi */
/*                        .                    */
        END CASE .
    END. /* repeat */

    IF dbg = YES THEN MESSAGE "nt-webrqt.i - fin analyse fichier demande " .

    /**********************************************/
    /* phase 2 - selection des commandes         */
    /********************************************/

    commande-gen = "" .

    IF typcom-mini <> typcom-maxi THEN
        FOR EACH typbon WHERE typbon.codsoc  = codsoc-soc
                        AND   typbon.typcom >= typcom-mini
                        AND   typbon.typcom <= typcom-maxi
                        NO-LOCK:
            RUN SELECTION .
        END.
    ELSE DO: 
        FIND FIRST typbon WHERE typbon.codsoc  = codsoc-soc
                          AND   typbon.typcom = typcom-mini
                          NO-LOCK NO-ERROR.
        IF AVAILABLE typbon THEN RUN SELECTION .
    END.

    /****************************************************************************************************/
    /* pour chaque piece s�lectionn�e, envoie des pieces a l'op�rateur demandeaur                      */
    /*                                                                                                */
    /*************************************************************************************************/
    i-i = NUM-ENTRIES( commande-gen , car-sep ) - 1 .

    IF dbg = YES THEN MESSAGE "nt-webrqt.i - Nombre de pieces trouv�es : " i-i . 

    DO j-j = 1 TO i-i:
        i-param  = ENTRY( j-j , commande-gen , car-sep ) + "|ARCH|" + x-auteur + "|" + ligne .
        i-erreur = "OK" .        

        IF dbg = YES THEN MESSAGE "nt-webrqt.i - avant generation fichier archivage " i-param .
        run nt-webgen( input i-param , input i-erreur , output o-param ) .  

    END.

    MESSAGE "fin de la requete du fichier" ligne  STRING(TODAY,"99/99/9999") STRING(TIME,"hh:mm:ss") .

    LEAVE .    
END.

/*********************************************/
/* procedure de selection des commandes     */
/*******************************************/

PROCEDURE SELECTION:
    IF dbg = YES THEN MESSAGE "nt-webrqt.i - debut procedure selection " .

    FOR EACH bonent WHERE bonent.codsoc     =  codsoc-soc
                    AND   bonent.motcle     =  typbon.motcle
                    AND   bonent.typcom     =  typbon.typcom 
                    AND   bonent.numbon     >= numbon-mini
                    AND   bonent.numbon     <= numbon-maxi
                    AND   bonent.codaux     >= fourni-mini
                    AND   bonent.codaux     <= fourni-maxi
/*                     AND   bonent.opecre     >= operat-mini */
/*                     AND   bonent.opecre     <= operat-maxi */
                    AND   bonent.datcre     >= datcre-mini
                    AND   bonent.datcre     <= datcre-maxi
                    AND   bonent.ref-tiers  >= reffou-mini
                    AND   bonent.ref-tiers  <= reffou-maxi   
                    AND   bonent.stat-valide = 1 
                    USE-INDEX primaire
                    NO-LOCK :
        IF operat-mini <> "" THEN 
        DO:
            fou-OK = NO .
            DO i-i = 1 TO 9 :
                IF bonent.signataire[ i-i ] = operat-mini THEN fou-OK = YES .
            END.
            IF fou-OK = NO THEN NEXT .  
        END.

        IF articl-mini <> "" THEN 
        DO:
            FIND FIRST bonlig WHERE bonlig.codsoc =  bonent.codsoc
                              AND   bonlig.motcle =  bonent.motcle
                              AND   bonlig.typcom =  bonent.typcom
                              AND   bonlig.numbon =  bonent.numbon
                              AND   bonlig.articl >= articl-mini
                              AND   bonlig.articl <= articl-maxi 
                              NO-LOCK NO-ERROR.
            IF NOT AVAILABLE bonlig THEN  NEXT .
        END.

        commande-gen = commande-gen + string( rowid( BONENT ) ) + car-sep .      

    END.
    IF dbg = YES THEN MESSAGE "nt-webrqt.i - fin procedure selection " .
END.