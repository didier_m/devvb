/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/nathalie/inc/nt-webgen.pp                                                               !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  4900 23-04-03!Bug!bp             !Pb lors de la suppression des fichiers, la zone suffixe est modifi�e  !
!V52  4892 23-04-03!Evo!bp             !Les cdp doivent g�n�rer des rcf et pas des rcp en r�ception           !
!V52  4887 22-04-03!Evo!bp             !les pieces envoy�s sur le web doivent etre visible par tous les signat!
!V52  4798 11-04-03!Bug!bp             !la zone contenenat le fichier scan n'est pas remis a blanc            !
!V52  4772 10-04-03!New!bp             !Mise en place workflow sur les pieces FAC                             !
!V52  4681 31-03-03!New!bp             !Pb sur generation du fichier de retour si refus de signature          !
!V52  4608 24-03-03!New!bp             !Pb sur entete des commandes � r�ceptionn�es                           !
!V52  4593 21-03-03!New!bp             !Pb sur remont�e de donn�es vers le client serveur                     !
!V52  4513 14-03-03!New!bp             !Dans le cas de g�n�ration d'une r�ception, suppression du fichier de v!
!V52  4460 11-03-03!New!bp             !mise en place du module d'echange avec le web pour Eurogem            !
!V52  4383 03-03-03!New!bp             !Pb sur le retour de commentaire de lignes                             !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/

/*  A FAIRE : Analyser i-err pour passer les messages d'erreur .............  */

PROCEDURE NT-WEBGEN :


    def input  parameter i-par     as char     no-undo .
    def input  parameter i-err     as char     no-undo .
    def output parameter o-par     as char     no-undo .

    /*  i-par = rowid de b-bonent   */
    /*  i-err = ""               ==>   Cas Normal                                      */
    /*        = "RFS|........."  ==> Refus de Signature                                */
    /*        = "ERR|........."  ==> Erreur(s) g�n�r�e(s) par la v�rification du Bon   */
    /*        = "OK"             ==> Pas d'Erreur lors de la v�rification du Bon       */

    def var i-i          as int                 no-undo .
    def var x-x          as char                no-undo .
    def var x-y          as char                no-undo .
    def var x-rowid      as rowid               no-undo .
    def var x-ligne      as char                no-undo .
    def var x-nomfic     as char                no-undo .
    def var z-nomfic     as char                no-undo .
    def var x-operat     as char                no-undo .
    def var x-action     as char                no-undo .
    def var x-datheu     as char                no-undo .
    def var x-typcom     as char                no-undo .
    def var x-codaux     as char                no-undo .

    def var remp-operat     as char                no-undo .
    def var sav-remp-operat     as char            no-undo .
    def var sav-suffixe         as char            no-undo .
    def var suffixe as char .       

    def var x-fichier    as char                no-undo .

    def var x-liblib     as char    extent 9    no-undo .
    def var ope-refsig   as int                 no-undo .
    o-par = "" .

    x-rowid = to-rowid( entry( 1 , i-par , "|" ) ).

    x-action = entry( 2 , i-par , "|" ) no-error .
    x-operat = entry( 3 , i-par , "|" ) no-error .
    x-typtrt = trim( entry( 2 , i-par , "|" ) ) no-error .
    x-auteur = x-operat .

    FIND b-bonent where rowid( b-bonent ) = x-rowid  no-lock  no-error .

    if dbg = yes then message "nt-webgen.pp : relecture piece " available b-bonent .
    if not available b-bonent  then return .

    assign remp-operat = x-operat
           suffixe     = x-operat
           .

    find first tabges where tabges.codsoc = ""            
                     AND   tabges.etabli = ""           
                     and   tabges.typtab = "APR"        
                     and   tabges.prefix = "OPEACH" 
                     and   tabges.libel1[ 2 ] = x-operat
                     no-lock no-error.
    if available tabges and tabges.libel2[13] <> "" then
    do:
        FIND z-tabges WHERE z-tabges.codsoc = ""            
                      AND   z-tabges.etabli = ""           
                      and   z-tabges.typtab = "APR"        
                      and   z-tabges.prefix = "OPEACH"
                      AND   z-tabges.codtab = tabges.libel2[ 13 ]
                      NO-LOCK NO-ERROR .
        IF AVAILABLE z-tabges then
            assign remp-operat = z-tabges.libel1[ 2 ]
                   suffixe     = z-tabges.libel1[ 2 ] + "_" + x-auteur
                   .    
    end.

    if dbg = yes then message "nt-webgen.pp : Apres recherche operateur de remplacement" remp-operat "-" suffixe .

    x-typcom = b-bonent.typcom .
    /* si i-erreur = DEL ==> supession des fichiers de signature dans la directorie de l'operateur */
    if i-err = "DEL" then
    do:
        x-fichier = repert-ecr + remp-operat + dir-sep .
        if dbg = yes then message "nt-webgen.pp : Phase de suppression des fichiers de signature dans " x-fichier .
        input stream kan-sup from os-dir( x-fichier ) .
        REPEAT :
            import stream kan-sup x-ligne .

            if entry( 1 , x-ligne , "-" ) = "l" and
               entry( 2 , x-ligne , "-" ) =  b-bonent.typcom and
               entry( 3 , x-ligne , "-" ) =  trim( codsoc-soc ) and
               entry( 4 , x-ligne , "-" ) =  trim(substr(b-bonent.numbon , 1 , 8 ) ) and 
               substr( entry( 5 , x-ligne , "-" ) , 1 , 3 )  = substr( x-action , 1 , 3 ) then
            do:
                 x-x = repert-ecr + remp-operat + dir-sep + x-ligne .
                 os-delete value( x-x ) . 
            end .
        end.
        input stream kan-sup close .
        return .
    end.
    /* si typtrt = 'vis' alors commencer par supprimer tous les fichiers de la meme commande en 'vis' */
    if substr( x-typtrt , 1 , 3 ) = "vis" then
    do:
        assign sav-remp-operat = remp-operat 
               sav-suffixe     = suffixe
               .
        do i-i = 1 to 9 :
            if b-bonent.signataire[ i-i ] = "" then next .
            find tabges  where tabges.codsoc = ""
     	                 and   tabges.etabli = ""
                         and   tabges.typtab = "APR" 
                         and   tabges.prefix = "OPEACH" 
                         and   tabges.codtab = trim( b-bonent.signataire[ i-i ] )
                         no-lock no-error . 

            if not available tabges then next.

            if TRIM( tabges.libel2[14]) <> "1" then next .  /* operateur non web */

            assign remp-operat = tabges.libel1[ 2 ]
                   suffixe     = tabges.libel1[ 2 ] .
                   .  

            if available tabges and tabges.libel2[13] <> "" then  /* operateur de remplacement */
            do:
                FIND z-tabges WHERE z-tabges.codsoc = ""            
                              AND   z-tabges.etabli = ""           
                              and   z-tabges.typtab = "APR"        
                              and   z-tabges.prefix = "OPEACH"
                              AND   z-tabges.codtab = tabges.libel2[ 13 ]
                              NO-LOCK NO-ERROR .
                IF AVAILABLE z-tabges then
                    assign remp-operat = z-tabges.libel1[ 2 ]
                           suffixe     = z-tabges.libel1[ 2 ] + "_" + tabges.libel1[ 2 ]
                           .    
            end.

            x-fichier = repert-ecr + remp-operat + dir-sep .
            if dbg = yes then message "nt-webgen.pp : Phase de suppression des fichiers de visualisation dans " x-fichier .
            input stream kan-sup from os-dir( x-fichier ) .
            REPEAT :
                import stream kan-sup x-ligne .

                if entry( 1 , x-ligne , "-" ) = "l" and
                   entry( 2 , x-ligne , "-" ) =  b-bonent.typcom and
                   entry( 3 , x-ligne , "-" ) =  trim( codsoc-soc ) and
                   entry( 4 , x-ligne , "-" ) =  trim(substr(b-bonent.numbon , 1 , 8 ) ) and 
                   substr( entry( 5 , x-ligne , "-" ) , 1 , 3 )  = "vis" then
                do:
                     x-x = repert-ecr + remp-operat + dir-sep + x-ligne .
                     os-delete value( x-x ) . 
                end .
            end.
        end.
        input stream kan-sup close .
        if x-typtrt = "VIS9" then return . /* cas suppression des pieces en visu apres reception automatique */
        assign remp-operat = sav-remp-operat 
               suffixe     = sav-suffixe 
               .
    end.

    if x-typtrt = "arch" or x-typtrt = "vis3"
        then x-datheu = string( year( b-bonent.datcre) , "9999") + 
                        string( month( b-bonent.datcre) , "99" ) + 
                        string( day( b-bonent.datcre) , "99" )   +
                        substr( b-bonent.heucre , 1 , 2 )        + substr( b-bonent.heucre , 4 , 2 ) + "00" .
        else x-datheu = entry( 6 , entry( 4 , i-par , "|" ) , "-" ) .

    x-codaux = replace( trim(b-bonent.codaux) , "-" , "�" ) .
    x-nomfic = "l-" + b-bonent.typcom + "-" + trim( codsoc-soc ) + "-" +
               trim(substr(b-bonent.numbon , 1 , 8 ) ) + "-" + x-typtrt + "-" + x-datheu + "-" +
               x-codaux + "-" + trim(b-bonent.ref-tiers) +  
               "." + suffixe no-error .

    ope-refsig = 0 .
    if x-typtrt = "vis2" then
        assign x-nomfic = "l-deb-" + trim( codsoc-soc ) + "-" +
                           trim(substr(b-bonent.numbon , 1 , 8 ) ) + "-" + x-typtrt + "-" + x-datheu + "-" +
                           x-codaux + "-" + trim(b-bonent.ref-tiers) +  
                           "." + suffixe 
               x-typcom = b-bonent.typcom
               ope-refsig = int( entry( 1 , i-err , "|" ) )
               z-nomfic = "l-" + b-bonent.typcom + "-" + trim( codsoc-soc ) + "-" +
                           trim(substr(b-bonent.numbon , 1 , 8 ) ) + "-" + x-typtrt + "-" + x-datheu + "-" +
                           x-codaux + "-" + trim(b-bonent.ref-tiers) +  
                           "." + suffixe
               .    

    x-x = repert-ecr + remp-operat + dir-sep + x-nomfic .

    x-x = lc( x-x ) .  
    if dbg = yes then message "nt-webgen.pp : Nom du fichier a g�n�rer" x-x .
    IF session:BATCH = YES THEN message "generation de " x-x STRING(TODAY,"99/99/9999") STRING(TIME,"hh:mm:ss") . 

    x-fichier = "�" + x-x .
    output stream kan-out to value ( x-x ) .

    i-par = b-bonent.opecre .
    run n-opeges( input i-par , output o-par ) .
    ges-operat = o-par .

    y-demandeur = ges-operat .

    x-ligne = "L-ENTETE"                                + car-sep +
               b-bonent.codsoc + "-" + b-bonent.etabli      + car-sep + 
               right-trim(b-bonent.segana[1] )            + car-sep +
               x-typcom                                   + car-sep +
               substr( b-bonent.numbon , 1 , 8 )          + car-sep +
               string( b-bonent.datcre ,"99/99/9999")     + car-sep +
               ges-operat                                 + car-sep +                                          
               y-demandeur                                        + car-sep +
               right-trim( b-bonent.adres-liv[1] )        + car-sep +  
               right-trim( b-bonent.adres-liv[2] )        + car-sep +  
               right-trim( b-bonent.adres-liv[3] )        + car-sep +  
               right-trim( b-bonent.adres-liv[4] )        + car-sep +  
               right-trim( b-bonent.adres-liv[5] )        + car-sep .

    FIND PROGLIB where proglib.codsoc = b-bonent.codsoc
                 and   proglib.motcle = b-bonent.motcle
                 and   proglib.etabli = ""
                 and   proglib.clef   = "COMMANDE"
                 and   proglib.edition = "PIE" + string( b-bonent.typcom ,   "xxx" ) + 
                                                 string( b-bonent.numbon , "x(10)" )
                 and   proglib.chrono = 0
                 no-lock  no-error .
    if available proglib  then
    do i-i = 1 to 9 :
        x-ligne = x-ligne + right-trim(proglib.libelle[ i-i ]) + "<BR>" .
    end . 

    x-ligne = x-ligne + car-sep +
              right-trim( b-bonent.segana[ 2 ] ) + car-sep +
              right-trim( b-bonent.segana[ 3 ] ) + car-sep 
              .

    do i-i = 1 to 9 :   
        find tabges  where tabges.codsoc = ""
  	             and   tabges.etabli = ""
	             and   tabges.typtab = "APR" 
	             and   tabges.prefix = "OPEACH" 
	             and   tabges.codtab = trim( b-bonent.signataire[ i-i ] )
                     no-lock no-error .
        if available tabges then x-ligne = x-ligne + right-trim( tabges.libel1[ 1 ] ) + car-sep2 .
        else x-ligne = x-ligne + car-sep2 .

        find tabges  where tabges.codsoc = ""
  	             and   tabges.etabli = ""
	             and   tabges.typtab = "APR" 
	             and   tabges.prefix = "OPEACH" 
	             and   tabges.codtab = trim( b-bonent.ope-signat[ i-i ] )
                     no-lock no-error .
        if available tabges then 
             x-ligne = x-ligne + right-trim( tabges.libel1[ 1 ] ) + car-sep2  +
                       string( b-bonent.dat-signat[ i-i ] , "99/99/9999" ) + car-sep .
        else 
              if ope-refsig = i-i then  x-ligne = x-ligne + car-sep2 + "R" + car-sep .
                                  else  x-ligne = x-ligne + car-sep2 + car-sep .

    end . 

    x-ligne = x-ligne + string( b-bonent.ht-bon , ">>>>>>>>9.99-" ) + car-sep +
                        b-bonent.devise                             + car-sep +
                        b-bonent.typaux                            + car-sep +
                        b-bonent.codaux                             + car-sep +
                        b-bonent.adres[1]                           + car-sep +
                        b-bonent.adres[2]                           + car-sep +
                        b-bonent.adres[3]                           + car-sep +
                        b-bonent.adres[4]                           + car-sep +
                        b-bonent.adres[5]                           + car-sep +
                        b-bonent.modliv                             + car-sep +
                        b-bonent.modreg                             + car-sep +
                        b-bonent.codech                             + car-sep +
                        b-bonent.nom-contact                        + car-sep +
                        string( b-bonent.datliv , "99/99/9999" )    + car-sep +
                        b-bonent.lib-piefac                         + car-sep  /* ref-demande */
                        .
    FIND PROGLIB where proglib.codsoc = b-bonent.codsoc
                 and   proglib.motcle = b-bonent.motcle
                 and   proglib.etabli = ""
                 and   proglib.clef   = "COMMANDE"
                 and   proglib.edition = "ENT" + string( b-bonent.typcom ,   "xxx" ) + 
                                                 string( b-bonent.numbon , "x(10)" )
                 and   proglib.chrono = 0
                 no-lock  no-error .

    if available proglib  then
    do i-i = 1 to 9 :
        x-ligne = x-ligne + right-trim(proglib.libelle[ i-i ]) + "<BR>" .
    end . 

    FIND BONENS  where bonens.codsoc = b-bonent.codsoc
                 and   bonens.motcle = b-bonent.motcle
                 and   bonens.typcom = b-bonent.typcom
                 and   bonens.numbon = b-bonent.numbon
                 and   bonens.theme  = "INT-DBWEB"
                 and   bonens.chrono = 0
                 no-lock no-error .
    if available bonens then 
        x-ligne = x-ligne + car-sep + trim( bonens.alpha[ 3 ] ) + car-sep + 
                            trim( bonens.alpha[ 2 ] )           + car-sep +
                            trim( bonens.alpha[ 4 ] ).

    else x-ligne = x-ligne  + car-sep + car-sep + car-sep.

    put stream kan-out unformatted  x-ligne  skip .    

    FOR EACH BONLIG where bonlig.codsoc = b-bonent.codsoc
                    and   bonlig.motcle = b-bonent.motcle
                    and   bonlig.typcom = b-bonent.typcom
                    and   bonlig.numbon = b-bonent.numbon
                    no-lock  :

        FIND BONLGS where bonlgs.codsoc = bonlig.codsoc
                    and   bonlgs.motcle = bonlig.motcle 
                    and   bonlgs.typcom = bonlig.typcom
                    and   bonlgs.numbon = bonlig.numbon
                    and   bonlgs.chrono = bonlig.chrono
                    and   bonlgs.theme  = "INT-DBWEB"
                    no-lock no-error .

         x-ligne = "L-LIGNES"                             + car-sep +  /* 1 */
                   string( bonlig.datdep , "99/99/9999" ) + car-sep +  /* 2 */
                   trim( bonlig.articl )                  + car-sep +  /* 3 */
                   b-bonent.typaux                          + car-sep +  /* 4 */ 
                   b-bonent.codaux                          + car-sep    /* 5 */           
                   . 

        validate auxges . RELEASE auxges . 

        if b-bonent.typaux = "prv" then
        do:
            find auxges where auxges.codsoc = auxges-soc
                        and   auxges.typaux = b-bonent.typaux
                        and   auxges.codaux = b-bonent.codaux
                        no-lock no-error.
            if available auxges then
                x-ligne = x-ligne + auxges.adres[ 1 ]  + car-sep +
                                    auxges.libabr      + car-sep +
                                    auxges.code-pays   + car-sep +     
                                    auxges.CODE-LANGUE + car-sep +
                                    auxges.devise      + car-sep +
                                    auxges.siret       + car-sep +                                    
                                    auxges.adres[ 2 ]  + car-sep +                                     
                                    auxges.adres[ 3 ]  + car-sep +
                                    auxges.adres[ 4 ]  + car-sep +
                                    auxges.adres[ 5 ]  + car-sep +
                                    auxges.teleph      + car-sep + 
                                    auxges.nom-respon  + car-sep
                                    .
        end.

        if not available auxges then 
            x-ligne = x-ligne +  car-sep + car-sep + car-sep + car-sep +  
                                 car-sep + car-sep + car-sep + car-sep +                                         
                                 car-sep + car-sep + car-sep + car-sep .   
        if available bonlgs 
        then x-ligne = x-ligne + bonlgs.alpha[ 1 ] + car-sep +
                                 bonlgs.alpha[ 2 ] + car-sep +
                                 bonlgs.alpha[ 3 ] + car-sep 
                                 .

        else x-ligne = x-ligne + car-sep + car-sep + car-sep .
        x-ligne = x-ligne +  trim( string( bonlig.pu-net , ">>>>>>>>9.99" ) ) + car-sep +
                             bonlig.devise                                    + car-sep +
                             string( bonlig.qte[ 1 ] )                        + car-sep +
                             bonlig.ucde                                      + car-sep +
                             right-trim( bonlig.libart )                      + car-sep   
                             .


        /*    Chargement Messages de Proglib : LIG-TX  = Lignes specifiques de Texte.    */

        x-x = bonlig.typcom  + bonlig.numbon + string( bonlig.chrono , "9999" ) .

        find PROGLIB where proglib.motcle  = bonlig.motcle
                     and   proglib.codsoc  = bonlig.codsoc
                     and   proglib.etabli  = ""
                     and   proglib.clef    = x-x
                     and   proglib.edition = "LIG-TX"
                     and   proglib.chrono  = 0
                     no-lock no-error.

        if available proglib then
        do i-i = 1 to 3 :
            x-ligne = x-ligne  + right-trim( proglib.libelle[ i-i ] ) + car-sep .

        end .

        else  x-ligne = x-ligne  +  car-sep   .

        x-ligne  = x-ligne + car-sep + car-sep +
                   bonlig.segana[ 1 ] + car-sep +
                   bonlig.segana[ 2 ] + car-sep +
                   bonlig.segana[ 3 ] .


        put stream kan-out unformatted  x-ligne  skip .

    end.

    if x-typtrt = "VIS2" then   /* refus de signature */
    do:
        x-ligne = "L-ERRSIG" + car-sep + i-err .
        put stream kan-out unformatted  x-ligne  skip .
    end.

    output stream kan-out close .

    if x-typtrt = "VIS2" then   /* refus de signature */
    do:
        x-x = lc(  repert-ecr + remp-operat + dir-sep + x-nomfic ) .
        x-y = lc(  repert-ecr + remp-operat + dir-sep + z-nomfic ) .
        pause 1 .
        OS-COPY value( x-x ) value( x-y ) .
        if dbg = yes then message "nt-webgen.pp : copie de " x-x " sur " x-y .
    end.
    if x-typtrt = "VIS1" then   /* Commande sign�e, envoi fichier pour saisie critere */
    do:
        find auxges where auxges.codsoc = auxges-soc
                    and   auxges.typaux = b-bonent.typaux
                    and   auxges.codaux = b-bonent.codaux
                    no-lock no-error.
        if available auxges and lookup( "CRI" , auxges.type-tiers ) <> 0 then 
        do:
            z-nomfic = replace( x-nomfic , "-vis1-" , "-cri-" ) .
            x-x = lc(  repert-ecr + remp-operat + dir-sep + x-nomfic ) .
            x-y = lc(  repert-ecr + remp-operat + dir-sep + z-nomfic ) .
            pause 1 .
            OS-COPY value( x-x ) value( x-y ) .
            if dbg = yes then message "nt-webgen.pp : copie de " x-x " sur " x-y .
        end.
    end.

    o-par = "1" .

    output close .        
    if dbg = yes then message "nt-webgen.pp : Apres close du fichier" x-nomfic .

    if substr( x-typtrt , 1 , 3 ) = "VIS" then
    do:
        temps = ETIME .
        REPEAT WHILE ETIME < Temps + 1000 :
        END.        

        x-x = lc(  repert-ecr + remp-operat + dir-sep + x-nomfic ) .
        if x-typtrt = "vis2" then x-x = lc(  repert-ecr + remp-operat + dir-sep + z-nomfic ) .
        /* envoi fichier de visu aux autres signataires */
        do i-i = 2 to 9 :        
            if  b-bonent.signataire[ i-i ] = "" then next .            
            find tabges  where tabges.codsoc = ""
     	                 and   tabges.etabli = ""
                         and   tabges.typtab = "APR" 
                         and   tabges.prefix = "OPEACH" 
                         and   tabges.codtab = trim( b-bonent.signataire[ i-i ] )
                         no-lock no-error . 

            if not available tabges then next.

            if TRIM( tabges.libel2[14]) <> "1" then next .  /* operateur non web */

            assign remp-operat = tabges.libel1[ 2 ]
                   suffixe     = tabges.libel1[ 2 ] .
                   .            
            if available tabges and tabges.libel2[13] <> "" then  /* operateur de remplacement */
            do:
                FIND z-tabges WHERE z-tabges.codsoc = ""            
                              AND   z-tabges.etabli = ""           
                              and   z-tabges.typtab = "APR"        
                              and   z-tabges.prefix = "OPEACH"
                              AND   z-tabges.codtab = tabges.libel2[ 13 ]
                              NO-LOCK NO-ERROR .
                IF AVAILABLE z-tabges then
                    assign remp-operat = z-tabges.libel1[ 2 ]
                           suffixe     = z-tabges.libel1[ 2 ] + "_" + tabges.libel1[ 2 ]
                           .    
            end.
            if x-typtrt = "vis2" 
                then z-nomfic = entry( 1 , z-nomfic , "." ) + "." + suffixe . 
                else z-nomfic = entry( 1 , x-nomfic , "." ) + "." + suffixe .

            x-y = lc(  repert-ecr + remp-operat + dir-sep + z-nomfic ) .
            if x-x <> x-y then
            do:
                OS-COPY value( x-x ) value( x-y ) .
                if dbg = yes then message "nt-webgen.pp : copie de " x-x " sur " x-y .        
            end.
        end.
    end.
end.

/***************************************************************/
/*              generation piece pour reception               */
/*************************************************************/

PROCEDURE NT-WEBGEN-LIV :

    def input  parameter i-par     as char     no-undo .
    def input  parameter i-err     as char     no-undo .
    def output parameter o-par     as char     no-undo .

    def var i-i          as int                 no-undo .
    def var x-x          as char                no-undo .
    def var x-y          as char                no-undo .
    def var x-rowid      as rowid               no-undo .
    def var x-ligne      as char                no-undo .
    def var x-nomfic     as char                no-undo .

    def var x-operat     as char                no-undo .
    def var x-action     as char                no-undo .

    def var x-fichier    as char                no-undo .
    def var x-codaux     as char                no-undo .

    def var remp-operat   as char               no-undo .
    def var suffixe as char .       

    def var x-liblib     as char    extent 9    no-undo .
    o-par = "" .

    x-rowid = to-rowid( entry( 1 , i-par , "|" ) ).

    x-action = entry( 2 , i-par , "|" ) no-error .
    x-operat = entry( 3 , i-par , "|" ) no-error .
    x-typtrt = trim( entry( 2 , i-par , "|" ) ) no-error .

    if x-typtrt <> "rcf" then return .

    FIND b-bonent where rowid( b-bonent ) = x-rowid  no-lock  no-error .
    if dbg = yes then message "nt-webgen.pp : procedure NT-WEBGEN-LIV ==> relecture piece " available b-bonent .
    if not available b-bonent  then return .

    assign remp-operat = x-operat
           suffixe     = x-operat
           .

    find first tabges where tabges.codsoc = ""            
                      AND   tabges.etabli = ""           
                      and   tabges.typtab = "APR"        
                      and   tabges.prefix = "OPEACH" 
                      and   tabges.libel1[ 2 ] = x-operat
                      no-lock no-error.
    if available tabges and tabges.libel2[13] <> "" then
    do:
        FIND z-tabges WHERE z-tabges.codsoc = ""            
                      AND   z-tabges.etabli = ""           
                      and   z-tabges.typtab = "APR"        
                      and   z-tabges.prefix = "OPEACH"
                      AND   z-tabges.codtab = tabges.libel2[ 13 ]
                      NO-LOCK NO-ERROR .
        IF AVAILABLE z-tabges then
            assign remp-operat = z-tabges.libel1[ 2 ]
                   suffixe     = z-tabges.libel1[ 2 ] + "_" + x-auteur
                   .    
    end.

    find auxges where auxges.codsoc = auxges-soc
                and   auxges.typaux = b-bonent.typaux
                and   auxges.codaux = b-bonent.codaux
                no-lock no-error.
    if available auxges and lookup( "cri" , auxges.type-tiers ) <> 0 then x-typtrt = "rcfcri" .

    x-codaux = replace( trim(b-bonent.codaux) , "-" , "�" ) .
    x-nomfic = "l-" + b-bonent.typcom + "-" + trim( codsoc-soc ) + "-" + b-bonent.etabli + "-" +
               trim(substr(b-bonent.numbon , 1 , 8 ) ) + "-" + x-typtrt + "-" +
               x-codaux + "-" + trim(b-bonent.ref-tiers) +
               "." + suffixe no-error .

    x-x = repert-ecr + remp-operat + dir-sep + x-nomfic .

    x-x = lc( x-x ) .  
    IF session:BATCH = YES THEN message "generation de " x-x STRING(TODAY,"99/99/9999") STRING(TIME,"hh:mm:ss") . 
    output stream kan-out to value ( x-x ) .

    i-par = operat .
    run n-opeges( input i-par , output o-par ) .
    ges-operat = o-par .

    x-ligne = "L-ENTETE"                                + car-sep +
               b-bonent.codsoc + "-" + b-bonent.etabli      + car-sep + 
               right-trim(b-bonent.segana[1] )            + car-sep +
               b-bonent.typcom                            + car-sep +
               substr( b-bonent.numbon , 1 , 8 )          + car-sep +
               string( b-bonent.datcre ,"99/99/9999")     + car-sep +
               ges-operat                                 + car-sep +                                          
               ges-operat                                 + car-sep +
               right-trim( b-bonent.adres-liv[1] )        + car-sep +  
               right-trim( b-bonent.adres-liv[2] )        + car-sep +  
               right-trim( b-bonent.adres-liv[3] )        + car-sep +  
               right-trim( b-bonent.adres-liv[4] )        + car-sep +  
               right-trim( b-bonent.adres-liv[5] )        + car-sep .

    FIND PROGLIB where proglib.codsoc = b-bonent.codsoc
                 and   proglib.motcle = b-bonent.motcle
                 and   proglib.etabli = ""
                 and   proglib.clef   = "COMMANDE"
                 and   proglib.edition = "PIE" + string( b-bonent.typcom ,   "xxx" ) + 
                                                 string( b-bonent.numbon , "x(10)" )
                 and   proglib.chrono = 0
                 no-lock  no-error .

    if available proglib  then
    do i-i = 1 to 9 :
        x-ligne = x-ligne + right-trim(proglib.libelle[ i-i ]) .
    end . 

    x-ligne = x-ligne + car-sep +
              right-trim( b-bonent.segana[ 2 ] ) + car-sep +
              right-trim( b-bonent.segana[ 3 ] ) + car-sep 
              .

    do i-i = 1 to 9 :   
        x-ligne = x-ligne + car-sep2  + car-sep2 + car-sep .
    end . 

    x-ligne = x-ligne + string( b-bonent.ht-bon , ">>>>>>>>9.99-" ) + car-sep +
                        b-bonent.devise                             + car-sep +
                        b-bonent.typaux                             + car-sep +
                        b-bonent.codaux                             + car-sep +
                        b-bonent.adres[1]                           + car-sep +
                        b-bonent.adres[2]                           + car-sep +
                        b-bonent.adres[3]                           + car-sep +
                        b-bonent.adres[4]                           + car-sep +
                        b-bonent.adres[5]                           + car-sep +
                        b-bonent.modliv                             + car-sep +
                        b-bonent.modreg                             + car-sep +
                        b-bonent.codech                             + car-sep +
                        b-bonent.nom-contact                        + car-sep +
                        string( b-bonent.datliv , "99/99/9999" )    + car-sep +
                        b-bonent.lib-piefac                         + car-sep  /* ref-demande */
                        .
    FIND PROGLIB where proglib.codsoc = b-bonent.codsoc
                 and   proglib.motcle = b-bonent.motcle
                 and   proglib.etabli = ""
                 and   proglib.clef   = "COMMANDE"
                 and   proglib.edition = "ENT" + string( b-bonent.typcom ,   "xxx" ) + 
                                                 string( b-bonent.numbon , "x(10)" )
                 and   proglib.chrono = 0
                 no-lock  no-error .

    if available proglib  then
    do i-i = 1 to 9 :
        x-ligne = x-ligne + right-trim(proglib.libelle[ i-i ]) .
    end . 

    FIND BONENS  where bonens.codsoc = b-bonent.codsoc
                 and   bonens.motcle = b-bonent.motcle
                 and   bonens.typcom = b-bonent.typcom
                 and   bonens.numbon = b-bonent.numbon
                 and   bonens.theme  = "INT-DBWEB"
                 and   bonens.chrono = 0
                 no-lock no-error .
    if available bonens then 
        x-ligne = x-ligne + car-sep + trim( bonens.alpha[ 3 ] ) + car-sep + 
                            trim( bonens.alpha[ 2 ] )           + car-sep + 
                            trim( bonens.alpha[ 4 ] ).
    else x-ligne = x-ligne  + car-sep + car-sep + car-sep.


    put stream kan-out unformatted  x-ligne  skip .

    FOR EACH BONLIG where bonlig.codsoc = b-bonent.codsoc
                    and   bonlig.motcle = b-bonent.motcle
                    and   bonlig.typcom = b-bonent.typcom
                    and   bonlig.numbon = b-bonent.numbon
                    and   bonlig.codlig = "N"
                    and   bonlig.top-livre <> "1" 
                    no-lock  :

         x-ligne = "L-LIGNES"                              + car-sep +
                   b-bonent.typcom                           + car-sep +
                   substr(b-bonent.numbon , 1 , 8 )          + car-sep +
                   string( bonlig.chrono , "99999" )       + car-sep + 
                   trim( bonlig.articl )                   + car-sep +
                   trim( bonlig.libart )                   + car-sep +
                   string( bonlig.qte[1] , ">>>>>>>9.999-" ) + car-sep +
                   string( ( bonlig.qte[1] - bonlig.qte[2] ) , ">>>>>>>9.999-" ) + car-sep +                       
                   string( bonlig.pu-net , ">>>>>>>9.99" ) + car-sep +
                   string( bonlig.datdep , "99/99/9999" )      
                   .                                       

        put stream kan-out unformatted  x-ligne  skip .
    end.

    output stream kan-out close .

    o-par = "1" .

    output close .        

end .

PROCEDURE NT-WEBGEN-RCF :

    def input  parameter i-par     as char     no-undo .
    def input  parameter i-err     as char     no-undo .
    def output parameter o-par     as char     no-undo .

    def var x-rowid      as rowid               no-undo .

    def var x-motcle     as char                no-undo .
    def var x-typcom     as char                no-undo .
    def var x-numbon     as char                no-undo .
    def var x-codaux     as char                no-undo .

    def buffer new-bonent for bonent .
    def buffer new-bonlig for bonlig .

    x-rowid = to-rowid( entry( 1 , i-par , "|" ) ).
    FIND b-bonent where rowid( b-bonent ) = x-rowid  exclusive-lock  no-error .

    if dbg = yes then message "nt-webgen.pp : procedure NT-WEBGEN-RCF ==> relecture piece " available b-bonent .
    if not available b-bonent  then return .

    assign glo-codsoc = b-bonent.codsoc 
           x-motcle = "ACH" 
           x-typcom = "RCF" .

    assign  i-par = x-motcle + "|" + x-typcom + "|O" 
            o-par = "" .

    run n-nocom( input i-par , input-output o-par ) .
    x-numbon = o-par + "00" .


    CREATE new-bonent .

    assign new-bonent.codsoc = glo-codsoc
           new-bonent.motcle = x-motcle
           new-bonent.typcom = x-typcom
           new-bonent.numbon = x-numbon
           .

    { n-duplent.i  new-bonent b-bonent } 

    assign  new-bonent.datech = ?
            new-bonent.datbon = today
            new-bonent.datdep = today
            new-bonent.dattar = bonent.datbon
            new-bonent.datliv = bonent.datdep
            .

    if new-bonent.nivcom < "3"  then  new-bonent.nivcom = "" .
    if new-bonent.nivcom > "3"  then  new-bonent.nivcom = "3" .

    new-bonent.stat-valide = 1 .  /*  Validation Implicite  */

    new-bonent.solde-liv   = 1 .  /*  Commande sold�e sert uniquement � l'envopi d'un bon de commande */

    { majmoucb.i  new-bonent }

    DO I-I = 1  TO  3 :
        x-y = "ENT" .
        if i-i = 2  then  x-y = "PIE" .
        if i-i = 3  then  x-y = "ZZZ" .
        assign  x-x = x-y + string( b-bonent.typcom , "xxx" ) + b-bonent.numbon
                x-z = x-y + string(  new-bonent.typcom , "xxx" )   +  new-bonent.numbon .
        FOR EACH PROGLIB where proglib.motcle  = b-bonent.motcle
                         and   proglib.codsoc  = glo-codsoc
                         and   proglib.etabli  = ""
                         and   proglib.clef    = "COMMANDE"
                         and   proglib.edition = x-x
                         no-lock :
            CREATE B-PROGLIB .
            BUFFER-COPY PROGLIB  except proglib.edition  TO B-PROGLIB
                                 assign b-proglib.edition = x-z .
            { majmoucb.i  b-proglib }
        END .
    END .  /*  DO I-I = 1 TO 2  */

    FOR EACH BONENS where bonens.codsoc = glo-codsoc
                    and   bonens.motcle = b-bonent.motcle
                    and   bonens.typcom = b-bonent.typcom
                    and   bonens.numbon = b-bonent.numbon
                    and   bonens.theme <> "QUALITE"
                    no-lock :
        if bonens.theme begins "NS-"  then  next .
        CREATE B-BONENS .
        BUFFER-COPY BONENS  except bonens.typcom  bonens.numbon  TO B-BONENS
                            assign b-bonens.typcom = new-bonent.typcom
                                   b-bonens.numbon = new-bonent.numbon .
        { majmoucb.i  b-bonens }
    END .

    for each b-bonlig where b-bonlig.codsoc = b-bonent.codsoc
                      and   b-bonlig.motcle = b-bonent.motcle
                      and   b-bonlig.typcom = b-bonent.typcom
                      and   b-bonlig.numbon = b-bonent.numbon
                      exclusive-lock :

        CREATE new-bonlig .
        assign  new-bonlig.codsoc = new-bonent.codsoc
                new-bonlig.motcle = new-bonent.motcle
                new-bonlig.typcom = new-bonent.typcom
                new-bonlig.numbon = new-bonent.numbon
                new-bonlig.chrono = b-bonlig.chrono .

        { n-dupllig.i  new-bonlig  b-bonlig } 

        assign  new-bonlig.datbon     = new-bonent.datbon
                new-bonlig.datdep     = new-bonent.datdep
                new-bonlig.datech     = new-bonent.datech
                new-bonlig.nivcom     = new-bonent.nivcom .

        assign  new-bonlig.qte[ 1 ]       = 0
                new-bonlig.qte[ 2 ]       = 0
                new-bonlig.qte[ 2 ] = b-bonlig.qte[1]
                .

        /* demande CGE pour m�moriser Contrat de r�f�rence */
        assign new-bonlig.motcle-cde = b-bonlig.motcle 
               new-bonlig.typcom-cde = b-bonlig.typcom 
               new-bonlig.numbon-cde = b-bonlig.numbon 
               new-bonlig.chrono-cde = b-bonlig.chrono
               .

         new-bonlig.top-livre = "1" . /* top solde livraison */

        { majmoucb.i   new-bonlig }

        assign  x-x = string( b-bonlig.typcom , "xxx" ) + string( b-bonlig.numbon , "x(10)" ) +
                      string( b-bonlig.chrono , "9999" )
                x-z = string( new-bonlig.typcom , "xxx" ) + string(  new-bonlig.numbon , "x(10)" ) +
                      string( new-bonlig.chrono , "9999" ) .
        FOR EACH PROGLIB where proglib.motcle  = new-bonlig.motcle
                         and   proglib.codsoc  = glo-codsoc
                         and   proglib.etabli  = ""
                         and   proglib.clef    = x-x
                         no-lock :
            CREATE B-PROGLIB .
            BUFFER-COPY PROGLIB  except proglib.clef  TO B-PROGLIB
                                 assign b-proglib.clef = x-z .
            { majmoucb.i  b-proglib }
        END .

        FOR EACH BONLGS where bonlgs.codsoc = b-bonlig.motcle
                        and   bonlgs.motcle = b-bonlig.motcle
                        and   bonlgs.typcom = b-bonlig.typcom
                        and   bonlgs.numbon = b-bonlig.numbon
                        and   bonlgs.chrono = b-bonlig.chrono
                        no-lock :
            CREATE B-BONLGS .
            BUFFER-COPY BONLGS  except bonlgs.typcom  bonlgs.numbon  TO B-BONLGS
                                assign  b-bonlgs.typcom = new-bonlig.typcom
                                        b-bonlgs.numbon = new-bonlig.numbon .
            { majmoucb.i  b-bonlgs }
        END .

        b-bonlig.qte[2]    = b-bonlig.qte[1] .
        b-bonlig.top-livre = "1" .

        { majmoucb.i b-bonlig }

        release b-bonlig.
        release new-bonlig.
    end.


    x-x = "HT,RHT,TTC,RTT,PDS,VOL,QTE" .
    i-par = string( rowid( new-bonent ) ) + "|HT,RHT,TTC,RTT,PDS,VOL,QTE|" + articl-soc .
    run n-totbon( input i-par , output o-par ) .

    b-bonent.solde-liv = 1 .
    { majmoucb.i b-bonent }

    validate b-bonent .

    i-par = "Update|BONENT|SLL" .
    o-par = string( rowid( b-bonent ) ) .
    run n-majtrte( i-par , input-output o-par ) .


    x-x = "Societe " + glo-codsoc + " - Cr�ation reception No : " 
          +  new-bonent.motcle + "-" + new-bonent.typcom + "-" + 
          substring(  new-bonent.numbon , 1 , 8 ) + "  � partir de :"
          + b-bonent.motcle + "-" + b-bonent.typcom + "-" + 
          substring( b-bonent.numbon , 1 , 8 ) .

    message x-x .

    FIND TABGES where tabges.codsoc = codsoc-soc               
                and   tabges.etabli = ""
                and   tabges.typtab = "CHR"
                and   tabges.prefix = "TYPCOM"
                and   tabges.codtab = TRIM( b-bonent.motcle + b-bonent.typcom )
                NO-LOCK NO-ERROR.

    IF AVAILABLE tabges AND tabges.nombre[3] = 1 THEN 
    DO:
        ASSIGN i-from = b-bonent.opecre
               i-to   = x-demandeur
               i-copy = b-bonent.opecre
               i-objet = ""
               i-file = ""
               i-message = "" 
               .
        assign  typtab = "PRM"  prefix = "OPERATEUR"    codtab = "MAIL" .
        run n-chparax( output x-x ) .
        if x-x <> "" THEN i-from = entry( 1 , x-x , "/" )    no-error .

        RUN n-get-dictab( INPUT "599,DEAL" , INPUT "LABEL" , INPUT "GENRCF-NAT" , INPUT "1" , OUTPUT i-objet ) .

        IF i-objet = "" THEN i-objet = "G�n�ration de r�ception : #01 #02 #04 " .

        i-objet = REPLACE( i-objet , "#01" , b-bonent.codsoc ).
        i-objet = REPLACE( i-objet , "#02" , b-bonent.etabli ).
        i-objet = REPLACE( i-objet , "#04" , new-bonent.typcom + " " + SUBSTR( new-bonent.numbon , 1 , 8 ) ) .


        RUN n-get-dictab( INPUT "599,DEAL" , INPUT "message" , INPUT "GENRCF-NAT" , INPUT "1" , OUTPUT i-message ) .

        IF i-message = "" THEN i-message = "La commande suivante #01 #02 #03 � g�n�r� " + chr(10) +
                                           "la r�ception suivante #04" .

        i-message = REPLACE( i-message , "#01" , b-bonent.codsoc ).  
        i-message = REPLACE( i-message , "#02" , b-bonent.etabli ).  
        i-message = REPLACE( i-message , "#03" , b-bonent.typcom + " " + SUBSTR( b-bonent.numbon , 1 , 8 ) ) .
        i-message = REPLACE( i-message , "#04" , new-bonent.typcom + " " + SUBSTR( new-bonent.numbon , 1 , 8 ) ) .

        RUN n-genmail( INPUT i-from , INPUT i-to , input i-copy ,  
                         INPUT i-objet , INPUT i-file , INPUT i-message ) . 
        IF dbg = YES THEN MESSAGE "nt-webgen.pp : apres n-genmail ==> " i-from i-to i-message .
    END.

    release new-bonent.
    release b-bonent .
    if dbg = yes then message "nt-webgen.pp : fin procedure NT-WEBGEN-RCF " .
end.