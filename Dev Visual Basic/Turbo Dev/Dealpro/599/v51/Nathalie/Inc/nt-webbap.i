/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/nathalie/inc/nt-webbap.i                                                                !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  5604 02-07-03!New!bp             !Ne pas faire apparaitre les messages si le lancement n'est pas en batc!
!V52  4927 28-04-03!Bug!bp             !Pb sur le refus de BAP par le web                                     !
!V52  4895 23-04-03!Evo!bp             !pour client 599, seuls les BAP � blanc sont envoy�s sur le web        !
!V52  4798 11-04-03!Bug!bp             !la zone contenenat le fichier scan n'est pas remis a blanc            !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/*----------------------------------------------------------------------------*/
/*                           N T - W E B D E B . I                            */
/*  Generation des Demandes d'Achat a partir de fichiers crees la saisie WEB  */
/*----------------------------------------------------------------------------*/       

        ASSIGN tot-mttcde = 0
               tot-mttrcf = 0
               tot-mttdfac = 0
               tot-mttfac = 0
               vect-cde = ""
               .
        /* recalcul du HT de la facture pour recherche du circuit */

        opegen = "" .
        tot-ht = 0 .
       /* boucle sur les pieces factur�es pour memorise�s toutes les commandes touch�es par cette facture */
        FOR EACH stadet WHERE stadet.codsoc = bonent.codsoc
                        AND   stadet.motcle = "ACH"
                        AND   stadet.typcom = "FAC"
                        AND   stadet.numbon = bonent.numbon
                        NO-LOCK :
            tot-ht = tot-ht + stadet.fac-ht .
            /* boucle sur les lignes de r�ception */
            IF stadet.numbon-cde = ""  THEN NEXT .
            FOR EACH liv-bonlig WHERE liv-bonlig.codsoc = stadet.codsoc
                                AND   liv-bonlig.motcle = stadet.motcle-cde
                                AND   liv-bonlig.typcom = stadet.typcom-cde
                                AND   liv-bonlig.numbon = stadet.numbon-cde
                                AND   liv-bonlig.chrono = stadet.chrono-cde
                                NO-LOCK :
                /* boucle sur les commandes */
                IF liv-bonlig.numbon-cde = ""  THEN NEXT .
                FOR EACH cde-bonlig WHERE cde-bonlig.codsoc = liv-bonlig.codsoc
                                    AND   cde-bonlig.motcle = liv-bonlig.motcle-cde
                                    AND   cde-bonlig.typcom = liv-bonlig.typcom-cde
                                    AND   cde-bonlig.numbon = liv-bonlig.numbon-cde
                                    AND   cde-bonlig.chrono = liv-bonlig.chrono-cde
                                    NO-LOCK :

                    IF LOOKUP(cde-bonlig.motcle + car-sep2 + cde-bonlig.typcom + car-sep2 + cde-bonlig.numbon , vect-cde ) = 0 THEN
                    DO: 
                        IF opegen = ""  THEN opegen = cde-bonlig.opecre.
                        IF vect-cde <> ""  THEN vect-cde = vect-cde  + car-sep  .
                        vect-cde = vect-cde + cde-bonlig.motcle + car-sep2 + 
                                              cde-bonlig.typcom + car-sep2 +
                                              cde-bonlig.numbon  .
                     END.              
                END. /* FOR EACH cde-bonlig */
                IF opegen = ""  THEN opegen = liv-bonlig.opecre.
            END. /* FOR EACH liv-bonlig */
        END. /* FOR EACH stadet */

        bonent.ttc-bon = tot-ht .
        bonent.txdev   = 1 .

        if num-entries( vect-cde , car-sep ) = 0 
            then numcde = 0 .
            else numcde = num-entries( vect-cde , car-sep) .

        chrono = 0 .

        IF opegen = "" THEN opegen = bonent.opecre .

        x-demandeur = opegen .

        IF bonent.signataire[1] = "" THEN RUN hab-signat . 

        IF  numcde <> 0 THEN
        do i-i = 1 to numcde :

            FOR EACH cde-bonlig WHERE cde-bonlig.codsoc = codsoc-soc     
                                AND   cde-bonlig.motcle = ENTRY( 1 , ENTRY( i-i , vect-cde , car-sep ) , car-sep2 )  
                                AND   cde-bonlig.typcom = ENTRY( 2 , ENTRY( i-i , vect-cde , car-sep ) , car-sep2 )  
                                AND   cde-bonlig.numbon = ENTRY( 3 , ENTRY( i-i , vect-cde , car-sep ) , car-sep2 )
                                NO-LOCK : 
                CREATE ligbon .
                chrono = chrono + 1 .
                ASSIGN ligbon.clef   = STRING( chrono , "9999" )
                       ligbon.articl = cde-bonlig.articl
                       ligbon.numcde = cde-bonlig.typcom + cde-bonlig.numbon + STRING( cde-bonlig.chrono , "9999" )
                       ligbon.qte-cde = cde-bonlig.qte[ 1 ]
                       ligbon.libart  = cde-bonlig.libart 
                       .

                r-r = cde-bonlig.qte[ 1 ] * cde-bonlig.pu-net .

                if cde-bonlig.coef-fac > 0  then  r-r =     r-r * cde-bonlig.coef-fac .
                if cde-bonlig.coef-fac < 0  then  r-r = - ( r-r / cde-bonlig.coef-fac ) .

                ligbon.mtt-cde = r-r .
                tot-mttcde     = tot-mttcde + r-r .

                FOR EACH liv-bonlig WHERE liv-bonlig.codsoc     = cde-bonlig.codsoc    
                                    AND   liv-bonlig.motcle-cde = cde-bonlig.motcle
                                    AND   liv-bonlig.typcom-cde = cde-bonlig.typcom
                                    AND   liv-bonlig.numbon-cde = cde-bonlig.numbon
                                    AND   liv-bonlig.chrono-cde = cde-bonlig.chrono
                                    USE-INDEX commande
                                    NO-LOCK :  
                    IF ligbon.numrcf <> "" THEN
                    DO:
                        CREATE ligbon.
                        chrono = chrono + 1 .
                        ASSIGN ligbon.clef    = STRING( chrono , "9999" )
                               ligbon.articl  = cde-bonlig.articl
                               ligbon.numcde  = cde-bonlig.typcom + cde-bonlig.numbon + STRING( cde-bonlig.chrono , "9999" )
                               .
                    end.
                    ASSIGN ligbon.numrcf  = liv-bonlig.typcom + liv-bonlig.numbon + STRING( liv-bonlig.chrono , "9999" )    
                           ligbon.qte-rcf = liv-bonlig.qte[ 2 ]
                           .

                    r-r = liv-bonlig.qte[ 2 ] * liv-bonlig.pu-net .

                    if liv-bonlig.coef-fac > 0  then  r-r =     r-r * liv-bonlig.coef-fac .
                    if liv-bonlig.coef-fac < 0  then  r-r = - ( r-r / liv-bonlig.coef-fac ) .

                    ligbon.mtt-rcf = r-r .
                    tot-mttrcf     = tot-mttrcf + r-r .

                    FOR EACH fac-stadet WHERE fac-stadet.codsoc     = liv-bonlig.codsoc 
                                        AND   fac-stadet.motcle-cde = liv-bonlig.motcle 
                                        AND   fac-stadet.typcom-cde = liv-bonlig.typcom 
                                        AND   fac-stadet.numbon-cde = liv-bonlig.numbon 
                                        AND   fac-stadet.chrono-cde = liv-bonlig.chrono
                                        USE-INDEX commande
                                        NO-LOCK :
                        IF fac-stadet.numbon = bonent.numbon THEN
                        DO:
                            IF ligbon.numfac <> "" THEN 
                            DO:
                                CREATE ligbon.
                                chrono = chrono + 1 .
                                ASSIGN ligbon.clef    = STRING( chrono , "9999" )
                                       ligbon.articl  = cde-bonlig.articl
                                       ligbon.numcde  = cde-bonlig.typcom + cde-bonlig.numbon + STRING( cde-bonlig.chrono , "9999" )
                                       ligbon.numrcf  = liv-bonlig.typcom + liv-bonlig.numbon + STRING( liv-bonlig.chrono , "9999" )
                                       .
                            END.
                            ASSIGN ligbon.numfac  = fac-stadet.typcom +  fac-stadet.numbon + STRING(  fac-stadet.chrono , "9999" ) 
                                   ligbon.qte-fac = fac-stadet.qte 
                                   ligbon.mtt-fac = fac-stadet.fac-ht
                                   ligbon.affaire = fac-stadet.segana[1] 
                                   ligbon.ssaff   = fac-stadet.segana[2] 
                                   ligbon.nature  = fac-stadet.segana[3] 
                                   tot-mttfac     = tot-mttfac + fac-stadet.fac-ht
                                   .
                        END.
                        ELSE DO:
                            IF ligbon.numdfac <> "" THEN 
                            DO:
                                CREATE ligbon.
                                chrono = chrono + 1 .
                                ASSIGN ligbon.clef    = STRING( chrono , "9999" )
                                       ligbon.articl  = cde-bonlig.articl
                                       ligbon.numcde  = cde-bonlig.typcom + cde-bonlig.numbon + STRING( cde-bonlig.chrono , "9999" )
                                       ligbon.numrcf  = liv-bonlig.typcom + liv-bonlig.numbon + STRING( liv-bonlig.chrono , "9999" )
                                       .
                            END.     
                            ASSIGN ligbon.numdfac  = fac-stadet.numfac /*fac-stadet.typcom + fac-stadet.numbon + STRING( fac-stadet.chrono , "9999" ) */
                                   ligbon.qte-dfac = fac-stadet.qte 
                                   ligbon.mtt-dfac = fac-stadet.fac-ht
                                   tot-mttdfac     = tot-mttdfac + fac-stadet.fac-ht
                                   .
                        END.
                    END.
                END.
            END.
        END.

        /* cas des lignes de factures sans r�ception */
        FOR EACH fac-stadet WHERE fac-stadet.codsoc = bonent.codsoc
                            AND   fac-stadet.motcle = bonent.motcle
                            AND   fac-stadet.typcom = bonent.typcom
                            AND   fac-stadet.numbon = bonent.numbon
                            AND   fac-stadet.numbon-cde = ""
                            NO-LOCK :
            CREATE ligbon.
            chrono = chrono + 1 .
            ASSIGN ligbon.clef    = STRING( chrono , "9999" )
                   ligbon.articl  = fac-stadet.articl
                   ligbon.numfac  = fac-stadet.typcom + fac-stadet.numbon + STRING(  fac-stadet.chrono , "9999" ) 
                   ligbon.qte-fac = fac-stadet.qte 
                   ligbon.mtt-fac = fac-stadet.fac-ht
                   tot-mttfac     = tot-mttfac + fac-stadet.fac-ht
                   ligbon.libart  = ""
                   ligbon.affaire = fac-stadet.segana[1] 
                   ligbon.ssaff   = fac-stadet.segana[2] 
                   ligbon.nature  = fac-stadet.segana[3] 
                   .
        END.

        find FIRST stadet where stadet.codsoc = bonent.codsoc
                          and   stadet.motcle = bonent.motcle
                          and   stadet.typcom = bonent.typcom
                          and   stadet.numbon = bonent.numbon
                          no-lock no-error.
        IF AVAILABLE stadet THEN
        DO:
            assign remp-operat = opegen
                   suffixe     = opegen
                   .

            find first tabges where tabges.codsoc = ""            
                              AND   tabges.etabli = ""           
                              and   tabges.typtab = "APR"        
                              and   tabges.prefix = "OPEACH" 
                              and   tabges.libel1[ 2 ] = opegen
                              no-lock no-error.
            if available tabges and tabges.libel2[13] <> "" then
            do:
                FIND z-tabges WHERE z-tabges.codsoc = ""            
                              AND   z-tabges.etabli = ""           
                              and   z-tabges.typtab = "APR"        
                              and   z-tabges.prefix = "OPEACH"
                              AND   z-tabges.codtab = tabges.libel2[ 13 ]
                              NO-LOCK NO-ERROR .
                IF AVAILABLE z-tabges then
                    assign remp-operat = z-tabges.libel1[ 2 ]
                           suffixe     = z-tabges.libel1[ 2 ] + "_" + opegen
                           .    
            end.

            IF SESSION:BATCH = YES THEN message "apres recherche operateur de remplacement" remp-operat "-" suffixe .

            repert-ini = lc( x-repert + remp-operat  ) .

            x-codaux = REPLACE ( stadet.codaux-fac , "-" , "�" ) .
            x-fichier = lc( repert-ini + dir-sep + "l-fac-" + stadet.codsoc + "-" + TRIM( SUBSTR( stadet.numbon , 1 , 8 ) ) + 
                        "-vis0-" + string( year( stadet.datcre) , "9999") +  string( month( stadet.datcre) , "99" ) + 
                        string( day( stadet.datcre) , "99" ) + "000000-" + x-codaux + "-." + suffixe ) . 

            IF SESSION:BATCH = YES THEN message "nt-webbap.i : Nom du fichier a g�n�rer" x-fichier .

            output stream kan-sup to value (x-fichier ) .

            x-ligne = "L-ENTETE"                                  + car-sep +
                       stadet.codsoc + "-" + stadet.etabli        + car-sep + 
                       right-trim(stadet.segana[1] )              + car-sep +
                       stadet.typcom                              + car-sep +
                       substr( stadet.numbon , 1 , 8 )            + car-sep +
                       string( stadet.datcre ,"99/99/9999")       + car-sep +
                       stadet.opecre                              + car-sep +                                          
                       opegen                                     + car-sep +
                       ""       + car-sep +  ""       + car-sep + ""       + car-sep +
                       ""       + car-sep +  ""       + car-sep + ""       + car-sep +
                       ""       + car-sep +  ""       + car-sep .

            do i-i = 1 to 9 :   
                find tabges  where tabges.codsoc = ""
          	             and   tabges.etabli = ""
        	             and   tabges.typtab = "APR" 
        	             and   tabges.prefix = "OPEACH" 
        	             and   tabges.codtab = trim( bonent.signataire[ i-i ] )
                             no-lock no-error .
                if available tabges then x-ligne = x-ligne + right-trim( tabges.libel1[ 1 ] ) + car-sep2 .
                else x-ligne = x-ligne + car-sep2 .

                find tabges  where tabges.codsoc = ""
          	             and   tabges.etabli = ""
        	             and   tabges.typtab = "APR" 
        	             and   tabges.prefix = "OPEACH" 
        	             and   tabges.codtab = trim( bonent.ope-signat[ i-i ] )
                             no-lock no-error .
                if available tabges 
                    then  x-ligne = x-ligne + right-trim( tabges.libel1[ 1 ] ) + car-sep2  +
                                    string( bonent.dat-signat[ i-i ] , "99/99/9999" ) + car-sep .
                    ELSE x-ligne = x-ligne + car-sep2 + car-sep .      
            end . 

            /* recherche du fichier scan� de la facture */
            fichier-scan = "" .
            FIND d-tables WHERE d-tables.ident     =  "599" /* glo-ident */
                          AND   d-tables.progiciel = "Nathalie"
                          AND   d-tables.motcle    = "ACH"
                          AND   d-tables.typtab    = trim(stadet.docume)
                          AND   d-tables.prefix    = glo-codsoc
                          AND   d-tables.codtab    = "FACTURE"
                          NO-LOCK NO-ERROR .
            IF AVAILABLE d-tables THEN fichier-scan = d-tables.libel[2] .

            x-ligne = x-ligne + 
                      STRING( tot-mttfac , mspx )                + car-sep +
                      stadet.devise                              + car-sep +
                      stadet.typaux-fac                          + car-sep +
                      stadet.codaux-fac                          + car-sep +
                      stadet.numfac                              + car-sep +
                      string( stadet.datfac ,"99/99/9999")       + car-sep +
                      stadet.docume                              + car-sep +
                      fichier-scan                               + car-sep +  
                      STRING( tot-mttcde , mspx )                + car-sep +
                      STRING( tot-mttrcf , mspx )                + car-sep +
                      STRING( tot-mttdfac , mspx )               + car-sep .


            IF AVAILABLE mvtcpt  
                 THEN x-ligne = x-ligne + 
                                mvtcpt.bap-cod                        + car-sep +
                                mvtcpt.bap-lib                        + car-sep +
                                /*string( mvtcpt.bap-date , "99/99/9999" ) */ "" + car-sep +
                                mvtcpt.bap-ope .
                ELSE x-ligne = x-ligne + car-sep + car-sep + car-sep .                                    


            put stream kan-sup unformatted x-ligne skip .

        END.        

        FOR EACH ligbon WHERE ligbon.clef <> ""
                        NO-LOCK            
                        BREAK BY ligbon.numcde
                        :

            IF FIRST-OF (ligbon.numcde) THEN
            DO:       
                ASSIGN qtecde  = 0
                       mttcde  = 0
                       qtercf  = 0
                       mttrcf  = 0
                       qtefac  = 0
                       mttfac  = 0
                       qtedfac = 0
                       mttdfac = 0 
                       x-x     = "" .
            END.

            ASSIGN qtecde  = qtecde  + ligbon.qte-cde
                   mttcde  = mttcde  + ligbon.mtt-cde
                   qtercf  = qtercf  + ligbon.qte-rcf
                   mttrcf  = mttrcf  + ligbon.mtt-rcf
                   qtedfac = qtedfac + ligbon.qte-dfac
                   mttdfac = mttdfac + ligbon.mtt-dfac
                   qtefac  = qtefac  + ligbon.qte-fac
                   mttfac  = mttfac  + ligbon.mtt-fac
                   .
            x-x = x-x + car-sep + substr(ligbon.numrcf , 1 , 11 ) + "-" + STRING( int(SUBSTR(ligbon.numrcf , 14 , 4 ) )) 
                                + car-sep2 + STRING( ligbon.qte-rcf )  + car-sep2 + STRING( ligbon.mtt-rcf )  + car-sep2 +
                                   ligbon.numdfac 
                                + car-sep2 + STRING( ligbon.qte-dfac ) + car-sep2 + STRING( ligbon.mtt-dfac ) + car-sep2 +
                                  substr(ligbon.numfac , 1 , 11 ) + "-" + STRING( int(SUBSTR(ligbon.numfac , 14 , 4 ) )) 
                                + car-sep2 + STRING( ligbon.qte-fac )  + car-sep2 + STRING( ligbon.mtt-fac ) + car-sep2 +
                                  ligbon.affaire + car-sep2 + ligbon.ssaff + car-sep2 + ligbon.nature .

            IF LAST-OF (ligbon.numcde) THEN
            DO:
                x-x = "L-LIGNES" + car-sep + substr(ligbon.numcde , 1 , 11 )                  + car-sep + 
                                             STRING( int(SUBSTR(ligbon.numcde , 14 , 4 ) ))   + car-sep + 
                                             ligbon.articl            + car-sep + 
                                             STRING( qtecde , msqt )  + car-sep + STRING( mttcde , mspx )  + car-sep +
                                             STRING( qtercf , msqt )  + car-sep + STRING( mttrcf , mspx )  + car-sep +
                                             STRING( qtedfac , msqt ) + car-sep + STRING( mttdfac , mspx ) + car-sep +
                                             STRING( qtefac , msqt )  + car-sep + STRING( mttfac , mspx )  + car-sep +
                                             ligbon.libart +
                                             x-x           
                                             .
                put stream kan-sup unformatted x-x skip .
            END.
        END.

        output stream kan-sup close .

       /*   envoi du fichier au  signataire suivant ( ou au premier )  */           
        k-k = 0 .                                                                                                       
        FOR EACH TABGES where tabges.codsoc = habili-soc                                                                
                        and   tabges.etabli = ""                                                                        
                        and   tabges.typtab = "HAB"                                                                     
                        and   tabges.prefix = "PRF-" + cle-habili                                                  
                        no-lock :                                                                                       

             i-i = int( substring( tabges.codtab , 1 , 1 ) ) .                                                           

             IF i-i = 1 THEN NEXT . 

             /*  Si l'op�rateur n'est pas renseign�  */                                                                  
             if bonent.signataire[ i-i ] = ""  then  next .                                                            

             /*  Si la signature est d�j� pos�e  */                                                                      
             if bonent.ope-signat[ i-i ] <> ""   then  next .                                                           

             /*  S'il n'y a pas de signature � poser */                                                                  
/*              if tabges.libel2[ 2 ] <> "O"  then  next . */

             /*  S'il n'y a pas de profil � signer auparavant  */                                                        
             k-k = i-i .                                                                                                 
             j-j = tabges.nombre[ 2 ] .                                                                                  
             if j-j <> 0  and  bonent.ope-signat[ j-j ] = ""  then  k-k = j-j .                                        

             leave .                                                                                                     

        END .  /*  EACH TABGES  */                                                                                      

        if k-k <> 0 then                                                                                               
        do :                                                                                                            
            FIND TABGES where tabges.codsoc = ""                                                                        
                        and   tabges.etabli = ""                                                                        
                        and   tabges.typtab = "APR"                                                                     
                        and   tabges.prefix = "OPEACH"                                                                  
                        and   tabges.codtab = bonent.signataire[ k-k ]                                                
                        no-lock  no-error .                                                                             
            if available tabges  then                                                                                   
            do :                          

                opegen = trim( tabges.libel1[ 2 ] ) .    /*  Matricule du Signataire  */  
                FIND d-operat where d-operat.ident  = "deal"
                              and   d-operat.typtab = "operat" 
                              AND   d-operat.codtab = opegen
                              NO-LOCK NO-ERROR.
                IF AVAILABLE d-operat AND TRIM( tabges.libel2[14]) = "1" THEN
                DO:
                    assign remp-operat = opegen
                           suffixe     = opegen
                           .

                    find first tabges where tabges.codsoc = ""            
                                      AND   tabges.etabli = ""           
                                      and   tabges.typtab = "APR"        
                                      and   tabges.prefix = "OPEACH" 
                                      and   tabges.libel1[ 2 ] = opegen
                                      no-lock no-error.
                    if available tabges and tabges.libel2[13] <> "" then
                    do:
                        FIND z-tabges WHERE z-tabges.codsoc = ""            
                                      AND   z-tabges.etabli = ""           
                                      and   z-tabges.typtab = "APR"        
                                      and   z-tabges.prefix = "OPEACH"
                                      AND   z-tabges.codtab = tabges.libel2[ 13 ]
                                      NO-LOCK NO-ERROR .
                        IF AVAILABLE z-tabges then
                            assign remp-operat = z-tabges.libel1[ 2 ]
                                   suffixe     = z-tabges.libel1[ 2 ] + "_" + opegen
                                   .    
                    end.

                    IF SESSION:BATCH = YES THEN message "apres recherche operateur de remplacement" remp-operat "-" suffixe .

                    repert-cop = lc( x-repert + remp-operat  ) .

                    x-codaux    = REPLACE ( stadet.codaux-fac , "-" , "�" ) .
                    fichier-cop = lc( repert-cop + dir-sep + "l-fac-" + stadet.codsoc + "-" + TRIM( substr( stadet.numbon ,1 , 8 ) ) + 
                                  "-sig" + string( k-k , "9") + "-" + string( year( stadet.datcre) , "9999") +  string( month( stadet.datcre) , "99" ) + 
                                  string( day( stadet.datcre) , "99" ) + "000000-" + x-codaux + "-." + suffixe ) . 

                    IF SESSION:BATCH = YES THEN message "nt-webbap.i : Nom du fichier a g�n�rer"   fichier-cop.

                    OS-COPY value( x-fichier  ) value( fichier-cop ) .

                END.

            end .                                                                                                    
        end .                                                                  

        empty TEMP-TABLE ligbon.