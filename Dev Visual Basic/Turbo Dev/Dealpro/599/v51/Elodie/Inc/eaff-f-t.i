/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/elodie/inc/eaff-f-t.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  5584 01-07-03!Evo!lau            !Cr�ation lors de l'interface des segments 1 (avec top) non existant   !
!-------------------------------------------------------------------------------------------------------------!
!                             I N C L U D E S                !                    T A B L E S                 !
!------------------------------------------------------------+------------------------------------------------!
!select-z.i            !"codaff" "eloaff.codaff"             !                                                !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/* V423ELO-FIC 819 Ajout d'un controle sur la s�lection. <EVO mat 16/04/02> */
/* V42ELO-FIC 542 Evolution de l'edition de la fiche affaire <EVO YO 09/12/99> */
/* V42ELO-FIC 427 Edition de fiche affaire: <EVO YO 22/10/99> */
/* V42ELO-FIC 330 Dans l'EPAC Selection mini-maxi sur le code postal et sur la ville <EVO lau 08/09/99> */
/* ELO4-FIC 843 Edition Fiche Affaire - branchement .i specif dans selection <EVO fre 26/02/99> */
/* ELO4-FIC 739 Edition parametrable des Fiches & Listes Affaires <NEW fre 01/02/99> */
/*---------------------------------------------*/
/* selections sur fichier eloaff -             */
/*---------------------------------------------*/
{select-z.i "codaff"  "{1}eloaff.codaff"       }
{select-z.i "exedeb"  "({1}eloaff.execpt-deb + {1}eloaff.percpt-deb)" ",'9999999'" }
{select-z.i "execlo"  "({1}eloaff.execpt-clo + {1}eloaff.percpt-clo)" ",'9999999'" }
{select-z.i "codpos"  "{1}eloaff.adres[4]" ",'xxxxx'" }
{select-z.i "ville"   "{1}eloaff.adres[5]"     }
{select-z.i "societe" "{1}eloaff.codsoc"       }
{select-z.i "etablis" "{1}eloaff.codetb"       }
{select-z.i "exearr"  "({1}eloaff.execpt-arr + {1}eloaff.percpt-arr)" ",'9999999'" }
{select-z.i "top-cre" "{1}eloaff.libel[10]"    }

run select-t.p .