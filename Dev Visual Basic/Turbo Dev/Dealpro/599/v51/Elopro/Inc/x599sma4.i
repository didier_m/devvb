/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/elopro/inc/x599sma4.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  5428 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5426 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5425 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5424 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5422 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/* chargement des differents budgets et des engagements LUCIE */

if ban-eng <> "" 
then do :
    if lookup( "banaeng" ,this-procedure:internal-entries) <> 0  then run banaeng. 
end.
else do : 
    /* les budgets lies aux scenariis demandes */
    {banxsma4.i} /* budget dans solbud */ 

    if ban-refaxe = "ZZZ" 
    then do :
            ban-mtspe[1] = 0.
            /* Recherche du budget INITIAL */
            for first ban-solbud WHERE ban-solbud.vue        = solbud.vue
                                   AND ban-solbud.scenario   = "Z00"
                                   AND ban-solbud.codsoc     = solbud.codsoc
                                   AND ban-solbud.codetb     = solbud.codetb
                                   AND ban-solbud.execpt     = solbud.execpt
                                   AND ban-solbud.segment    = solbud.segments
                                   and ban-solbud.devise     = solbud.devise
                                   and ban-solbud.code-quant = solbud.code-quant
                                   and ban-solbud.codgen     = solbud.codgen
                                   no-lock : 

                 do a = integer( ban-percpt-mini ) to integer( ban-percpt-maxi ) :
                    ban-mtspe[1] = ban-mtspe[1] + ban-exclu *  ban-solbud.cpt[a]  .
                 end.

            end. 

    /*         /* Recherche des engagements : montant de commande et d'engagement */                            */
    /*         for first ban-solbud WHERE ban-solbud.vue        = solbud.vue                                    */
    /*                                AND ban-solbud.scenario   = "Z90"                                         */
    /*                                AND ban-solbud.codsoc     = solbud.codsoc                                 */
    /*                                AND ban-solbud.codetb     = solbud.codetb                                 */
    /*                                AND ban-solbud.execpt     = solbud.execpt                                 */
    /*                                AND ban-solbud.segment    = solbud.segments                               */
    /*                                and ban-solbud.devise     = solbud.devise                                 */
    /*                                and ban-solbud.code-quant = solbud.code-quant                             */
    /*                                and ban-solbud.codgen     = solbud.codgen                                 */
    /*                                no-lock :                                                                 */
    /*                                                                                                          */
    /*              do a = integer( ban-percpt-mini ) to integer( ban-percpt-maxi ) :                           */
    /*                 assign ban-mtspe[2] = ban-mtspe[2] + ban-exclu *  ban-solbud.cpt   [a]  /* commandes  */ */
    /*                        ban-mtspe[3] = ban-mtspe[3] + ban-exclu *  ban-solbud.specpt[a]  /* receptions */ */
    /*                        .                                                                                 */
    /*              end.                                                                                        */
    /*                                                                                                          */
    /*         end.                                                                                             */
    /*                                                                                                          */
    end.


end.

