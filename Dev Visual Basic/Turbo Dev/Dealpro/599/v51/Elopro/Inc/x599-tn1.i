/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/elopro/inc/x599-tn1.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  4469 12-03-03!New!lbo            !Interface Oracle Fournisseur                                          !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/*- x599-tn1.i : -*/

find x-spe where x-spe.evenement   = parint.evenement 
             and x-spe.s-evenement = parint.s-evene 
             and x-spe.prefix      = "SPECIF"            
             and x-spe.codsoc      = "P-L"               
             and x-spe.etabli      = ""                  
             and x-spe.codint      = parsai-cle
no-lock no-error .
if available x-spe then assign tnt-pos    = x-spe.ascii-pos
                               tnt-lgr    = x-spe.ascii-lgr
                               tnt-nomint = x-spe.codtab
                               tnt-def    = x-spe.libel.