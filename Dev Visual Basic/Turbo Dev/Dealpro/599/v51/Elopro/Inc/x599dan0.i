/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/elopro/inc/x599dan0.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  5430 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/

/* Specifique entete Extrait Balance analytique */

if ban-axe begins "Z" and anapreex.m-segana[3] > ""
THEN DO:


   /* code et libelle affaire */
   ent-gen[21] = anapreex.m-segana[3].
   find eloaff where eloaff.codsoc = ( if anapreex.m-segana[1] = "!" then codsoc-soc else anapreex.m-segana[1] ) 
                 and eloaff.codetb = ""
                 and eloaff.codaff = anapreex.m-segana[3]
                 no-lock no-error.
      if available eloaff then ent-gen[22] = eloaff.libel[mem-langue].

   /* codes et libelles SITE : axe Z03 */
   find first analaxe where analaxe.codsoc   = ( if anapreex.m-segana[1] = "!" then codsoc-soc else anapreex.m-segana[1] )
                        and analaxe.codetb   = ""
                        and analaxe.code-don = anapreex.m-segana[3]
                        and analaxe.axe      = "Z03"
                        no-lock no-error.
      if available analaxe 
      then do :
        ent-gen[23] = entry ( 5 , analaxe.hierarchie ).
        find b-tables where b-tables.codsoc = ""
                        and b-tables.etabli = ""
                        and b-tables.typtab = "ANA"
                        and b-tables.prefix = "AXE-Z0305" 
                        and b-tables.codtab = entry ( 5 , analaxe.hierarchie )
                        no-lock no-error.
             if available b-tables then ent-gen[24] = b-tables.libel1[mem-langue].
                                   else ent-gen[24] = "NON TROUVE".
      end.


   /* codes et libelles du charge d'execution : axe Z02 */
   find first analaxe where analaxe.codsoc   = ( if anapreex.m-segana[1] = "!" then codsoc-soc else anapreex.m-segana[1] )
                        and analaxe.codetb   = ""
                        and analaxe.code-don = anapreex.m-segana[3]
                        and analaxe.axe      = "Z02"
                        no-lock no-error.
      if available analaxe 
      then do :
        ent-gen[25] = entry ( 4 , analaxe.hierarchie ).
        find b-tables where b-tables.codsoc = ""
                        and b-tables.etabli = ""
                        and b-tables.typtab = "ANA"
                        and b-tables.prefix = "AXE-Z0204" 
                        and b-tables.codtab = entry ( 4 , analaxe.hierarchie )
                        no-lock no-error.
             if available b-tables then ent-gen[26] = b-tables.libel1[mem-langue].
                                   else ent-gen[26] = "NON TROUVE".
      end.


   /* codes et libelles du client : axe Z06 */
   find first analaxe where analaxe.codsoc   = ( if anapreex.m-segana[1] = "!" then codsoc-soc else anapreex.m-segana[1] )
                        and analaxe.codetb   = ""
                        and analaxe.code-don = anapreex.m-segana[3]
                        and analaxe.axe      = "Z06"
                        no-lock no-error.
      if available analaxe 
      then do :
        ent-gen[27] = analaxe.codaxe.
        find b-tables where b-tables.codsoc = ""
                        and b-tables.etabli = ""
                        and b-tables.typtab = "ANA"
                        and b-tables.prefix = "AXE-Z0601" 
                        and b-tables.codtab = analaxe.codaxe
                        no-lock no-error.
             if available b-tables then ent-gen[28] = b-tables.libel1[mem-langue].
                                   else ent-gen[28] = "NON TROUVE".
      end.



END.

