/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/elopro/inc/x599base.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  5428 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5426 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5425 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5424 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5422 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/* Specifique entete Balance analytique */

DEFINE VARIABLE codsoc    AS CHARACTER  NO-UNDO.
DEFINE VARIABLE affaire   AS CHARACTER  NO-UNDO.
define buffer b-tables for tables.

if basi-vue begins "Z" 
THEN DO:

    affaire = c-seg[3] . 
    codsoc = cod-timin[1].

   /* code et libelle affaire */
   edit-ent[91] = affaire.
   find eloaff where eloaff.codsoc = codsoc
                 and eloaff.codetb = ""
                 and eloaff.codaff = affaire
                 no-lock no-error.
      if available eloaff then edit-ent[92] = eloaff.libel[mem-langue].

   /* codes et libelles SITE : axe Z03 */
   find first analaxe where analaxe.codsoc   = codsoc
                        and analaxe.codetb   = ""
                        and analaxe.code-don = affaire
                        and analaxe.axe      = "Z03"
                        no-lock no-error.
      if available analaxe 
      then do :
        edit-ent[93] = entry ( 5 , analaxe.hierarchie ).
        find b-tables where b-tables.codsoc = ""
                        and b-tables.etabli = ""
                        and b-tables.typtab = "ANA"
                        and b-tables.prefix = "AXE-Z0305" 
                        and b-tables.codtab = entry ( 5 , analaxe.hierarchie )
                        no-lock no-error.
             if available b-tables then edit-ent[94] = b-tables.libel1[mem-langue].
                                   else edit-ent[94] = "NON TROUVE".
      end.


   /* codes et libelles du charge d'execution : axe Z02 */
   find first analaxe where analaxe.codsoc   = codsoc
                        and analaxe.codetb   = ""
                        and analaxe.code-don = affaire
                        and analaxe.axe      = "Z02"
                        no-lock no-error.
      if available analaxe 
      then do :
        edit-ent[95] = entry ( 4 , analaxe.hierarchie ).
        find b-tables where b-tables.codsoc = ""
                        and b-tables.etabli = ""
                        and b-tables.typtab = "ANA"
                        and b-tables.prefix = "AXE-Z0204" 
                        and b-tables.codtab = entry ( 4 , analaxe.hierarchie )
                        no-lock no-error.
             if available b-tables then edit-ent[96] = b-tables.libel1[mem-langue].
                                   else edit-ent[96] = "NON TROUVE".
      end.


   /* codes et libelles du client : axe Z06 */
   find first analaxe where analaxe.codsoc   = codsoc
                        and analaxe.codetb   = ""
                        and analaxe.code-don = affaire
                        and analaxe.axe      = "Z06"
                        no-lock no-error.
      if available analaxe 
      then do :
        edit-ent[97] = analaxe.codaxe.
        find b-tables where b-tables.codsoc = ""
                        and b-tables.etabli = ""
                        and b-tables.typtab = "ANA"
                        and b-tables.prefix = "AXE-Z0601" 
                        and b-tables.codtab = analaxe.codaxe
                        no-lock no-error.
             if available b-tables then edit-ent[98] = b-tables.libel1[mem-langue].
                                   else edit-ent[98] = "NON TROUVE".
      end.



END. /*&FIN*/ /*&FIN*/ /*&FIN*/ /*&FIN*/