/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/elopro/inc/ban-dluc.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  5430 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5430 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5429 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!-------------------------------------------------------------------------------------------------------------!
!                             I N C L U D E S                !                    T A B L E S                 !
!------------------------------------------------------------+------------------------------------------------!
!                      !                                     !BONLIG                                          !
!                      !                                     !solbud                                          !
!                      !                                     !BONENT                                          !
!                      !                                     !STADET                                          !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
procedure commandes-lucie :

DEFINE VARIABLE qte-pre    AS DECIMAL    NO-UNDO.
DEFINE VARIABLE qte-exe    AS DECIMAL    NO-UNDO.
DEFINE VARIABLE qte-liv    AS DECIMAL    NO-UNDO.
DEFINE VARIABLE qte-liv2   AS DECIMAL    NO-UNDO.
DEFINE VARIABLE qte-eng    AS DECIMAL    NO-UNDO.
DEFINE VARIABLE qte-fac    AS DECIMAL    NO-UNDO.
DEFINE VARIABLE tot-facini AS DECIMAL    NO-UNDO.
DEFINE VARIABLE mt-cap     AS DECIMAL    NO-UNDO.
def buffer rec-bonlig for bonlig.
def buffer rec-bonent for bonent.

    /***************************/
    /* Boucle sur les Lignes   */
    /***************************/
    FOR EACH bonlig where bonlig.codsoc = solbud.codsoc
                    and   bonlig.motcle = "ACC"
/*                     and   bonlig.typcom = bonent.typcom  */
/*                     AND   bonlig.numbon = bonent.numbon  */
                    and   bonlig.segana[1] = solbud.seg-a01
                    and   bonlig.segana[2] = solbud.seg-a02
                    and   bonlig.segana[3] = solbud.seg-a03
                    no-lock :

        IF bonlig.codlig <> "N" THEN NEXT . 
        IF SUBSTR( bonlig.codgen , 1 , 1 ) < "6"  THEN NEXT .

          /************************************************/
         /* Boucle sur les commandes non livr�es         */
        /************************************************/
        Find bonent WHERE bonent.codsoc = solbud.codsoc
                      AND bonent.etabli = solbud.codetb
                      AND bonent.motcle = "ACC"
                      and bonent.typcom = bonlig.typcom
                      AND bonent.numbon = bonlig.numbon
                      NO-LOCK no-error.
            /* exclure les pieces non valid�es et sold�es en LIV */
            if not available bonent or bonent.stat-valide = 0 OR bonent.solde-liv <> 0 THEN NEXT.

        /*     IF bonent.datbon < date-mini THEN NEXT . */
        /*     IF bonent.datbon > date-trt THEN NEXT .  */



        ASSIGN qte-pre  = 0 
               qte-exe  = 0
               qte-liv  = 0  
               qte-liv2 = 0.

        /************************************************/
        /* Recherche des Lignes Livrees non facturees  */
        /**********************************************/
        FOR EACH rec-bonlig where rec-bonlig.codsoc     = bonlig.codsoc   
                            AND   rec-bonlig.motcle     = "ACH"
                            AND   rec-bonlig.motcle-cde = "ACC"
                            and   rec-bonlig.typcom-cde = bonlig.typcom
                            and   rec-bonlig.numbon-cde = bonlig.numbon
                            and   rec-bonlig.chrono-cde = bonlig.chrono
                            and   rec-bonlig.nivcom     = "3"
                            use-index commande  no-lock :

            FIND rec-bonent where rec-bonent.codsoc = rec-bonlig.codsoc 
                            and   rec-bonent.motcle = rec-bonlig.motcle 
                            and   rec-bonent.typcom = rec-bonlig.typcom 
                            and   rec-bonent.numbon = rec-bonlig.numbon
                            no-lock no-error.
                if not available  rec-bonent OR rec-bonent.solde-fac = 1 then next .

                /*  Si date de livraison post�rieur  a la date du     */
                /*  Traitement ====>  On ne prend pas                 */

                qte-liv2 = qte-liv2 + rec-bonlig.qte[ 2] .

/*                     IF rec-bonlig.datdep > date-trt THEN NEXT .  */

                qte-liv = qte-liv + rec-bonlig.qte[ 2] .

                /* recherche des facturations effectuees sur cette commande */
                ASSIGN tot-facini = 0 qte-fac = 0.
                FOR EACH stadet use-index commande
                                where stadet.codsoc       = rec-bonlig.codsoc  and
                                      stadet.motcle       = rec-bonlig.motcle  and
                                      stadet.motcle-cde   = rec-bonlig.motcle  and
                                      stadet.typcom-cde   = rec-bonlig.typcom  and
                                      stadet.numbon-cde   = rec-bonlig.numbon  and
                                      stadet.chrono-cde   = rec-bonlig.chrono  AND
                                      substring ( stadet.datcpt , 1 , 7  )  <= ban-execpt + ban-percpt-maxi
                                      no-lock :

                    ASSIGN qte-fac    = qte-fac    + stadet.qte
                           tot-facini = tot-facini + stadet.fac-ht
                           .

                END.



        END .

        /* pas d'engagement sur la ligne si qte cde = qte livree */
        IF qte-liv >= bonlig.qte [ 1 ] THEN NEXT.

        IF  bonlig.top-livre = "1" THEN qte-eng = qte-liv2        - qte-liv .
                                   ELSE qte-eng = bonlig.qte[ 1 ] - qte-liv .

        IF qte-eng <= 0  THEN  NEXT .   

        mt-cap = ROUND( qte-eng * bonlig.pu-net , 2) .
        if mt-cap = 0  then  next .


        create detail . 
        assign detail.tri-1 = anapreex.c-seg 
               detail.tri-2 = trim(bonlig.typcom) + trim(bonlig.numbon)  + "," +  /* pour le tri par commande */
                              string(bonlig.qte[1])          + "," +  /* quantite commandee */
                              string(qte-liv)                + "," +  /* quantitee livree   */
                              string(qte-eng)                + "," +  /* quantitee engagee  */
                              string(bonlig.pu-net)          + "," +  /* Prix unitaire      */
                              string(mt-cap)                 + "," + 
                              string(qte-fac)                + "," +  /* quantite facturees */
                              string(tot-facini)             + "," +  /* montant facturees */
                              "BONLIG"    
               detail.enr   = rowid( bonlig ) .  


    END .  /*  FOR EACH bonlig  */

end procedure.



procedure detail-bonlig :

DEFINE VARIABLE qte-cde AS DECIMAL    NO-UNDO. /* commandes non livrees */
DEFINE VARIABLE qte-liv AS DECIMAL    NO-UNDO. /* livraisons non facturees */

       find bonlig where rowid(bonlig ) = detail.enr no-lock no-error .
       if not available bonlig then next .  

       assign edit [ 4 ]  = bonlig.typcom
              edit [ 5 ]  = bonlig.numfac
              edit [ 6 ]  = bonlig.libart
              edit [ 18 ] = string( bonlig.chrono )
              edit [ 19 ] = bonlig.devise
              edit [ 24 ] = bonlig.opecre
/*               edit [ 26 ] = bonlig.execpt + " " + bonlig.percpt */
/*               edit [ 42 ] = bonlig.libel                        */
/*               edit [ 43 ] = string (bonlig.niv-relance, "9")    */
              edit [ 45 ] = bonlig.typaux 
              edit [ 46 ] = bonlig.codaux
              .     

       /* le tiers */              
       codsoc-soc = bonlig.codsoc.
       regs-fileacc = "AUXILI" + bonlig.typaux.
       {regs-rec.i}
       for first auxili where auxili.codsoc = regs-soc
                          and auxili.typaux = bonlig.typaux
                          and auxili.codaux = bonlig.codaux
                          no-lock :
            assign edit [ 27 ] = auxili.adres[1] 
                   edit [ 47 ] = auxili.adres[2]
                   edit [ 48 ] = auxili.adres[3]
                   edit [ 49 ] = auxili.adres[4]
                   .
       end.

       /* montant commande et livre */
       assign qte-cde  = ( dec( entry ( 2 , detail.tri-2 ) ) - dec( entry ( 3 , detail.tri-2 ) ) ) * dec( entry ( 5 , detail.tri-2 ) ) /* le command� non livre */
              qte-liv  = ( dec( entry ( 3 , detail.tri-2 ) ) * dec( entry ( 5 , detail.tri-2 ) ) ) - dec( entry ( 8 , detail.tri-2 ) ) /* le command� non livre */
              edit[80] = string ( qte-cde ) /* le command� */
              edit[81] = string ( qte-liv ) /* le livr�    */
              .                   
        {ban-danr.i } 
       {ban-dan1.i "31"  "edit" }
        edit = "".


end procedure.



