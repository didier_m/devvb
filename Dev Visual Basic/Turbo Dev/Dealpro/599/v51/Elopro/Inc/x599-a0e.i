/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/elopro/inc/x599-a0e.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  4749 08-04-03!New!lio            !EXPORT -                                                              !
!                  !   !               !pb sur le #62 car bap-top sur 1 seul caracter  dans .maq mettre { a la!
!V52  4683 01-04-03!New!104            !EXPORT -                                                              !
!                  !   !               !1-ZONES CHARGES A PASSER AU MENU                                      !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
if var-type-client = "facture"  or var-type-client = "cap"
then do:



    nb-mvtcpt = nb-mvtcpt + 1 .

    FIND  tables no-lock WHERE  tables.codsoc = codsoc-soc AND
                        tables.etabli = "" AND
                        tables.typtab = "par" AND
                        tables.prefix = "periode" AND 
                        tables.codtab = {3}.execpt + {3}.percpt + {3}.percpt no-error .

    IF AVAILABLE tables  then det-lig[110] = string( tables.dattab[2] , "99/99/9999" ) .  

    assign  det-lig[99]  = string({3}.opemaj )  
            det-lig[104] = string({3}.dt-devini )  
            det-lig[105] = string({3}.ct-devini )  
            det-lig[106] = string( nb-mvtcpt )  
            det-lig[107] = string({3}.bap-top , "x" ) + det-lig[64] 
            det-lig[98]  = string({3}.bap-code )  
            det-lig[108] = string({3}.dt-devcpt )  
            det-lig[109] = string({3}.ct-devcpt )      
            det-lig[02]  = "  " .

    /*remplacement de { par # 62  bap top  */
    find first maqtecrit 
      where maqtecrit.unicite  = ma-maquet   
        and maqtecrit.rang     = 1           
        and maqtecrit.ligne    = 1           
        and maqtecrit.etiq     = 0           
        and maqtecrit.position = 0 
    no-error . 
    if available maqtecrit 
    then do :
        Overlay ( maqtecrit.Ma-Edi , index ( maqtecrit.ma-edi , "~{" ) , 1 ) = String({3}.bap-top , "X" ).
    end.


    do a = 1 to 10 :
        det-lig[200 + a ] = string( {3}.mt-stat[a] ) .
    end.


    /*Nature et prestation*/
    if {3}.ana-segment[2] <> "00000000" AND
        {3}.ana-segment[2] <> " " 
    then det-lig[100] = "9" + {3}.ana-segment[2] .  
    else det-lig[100] = {3}.ana-segment[3] .  



    if  {3}.ana-segment[1] <> "" 
    THEN do :

        /*axe metier*/
        find analaxe  
          Where   analaxe.axe      = "z05"                        
            and   analaxe.typlien  = "04"                         
            and   analaxe.codsoc   = {3}.codsoc                    
            and   analaxe.codetb   = ""                           
            and   analaxe.typaux   = ""  
            and   analaxe.code-don = {3}.ana-segment[1]
        no-lock no-error .

       if available analaxe then det-lig[101] = analaxe.codaxe
                .
       release analaxe .  

       /*axe centre profile CDP*/
       find analaxe  
         Where   analaxe.axe      = "z04"                        
           and   analaxe.typlien  = "04"                         
           and   analaxe.codsoc   = {3}.codsoc                    
           and   analaxe.codetb   = ""                           
           and   analaxe.typaux   = ""  
           and   analaxe.code-don = {3}.ana-segment[1]
       no-lock no-error .

       if available analaxe then det-lig[02] = analaxe.codaxe
           .
       ELSE det-lig[02] = "  " .

    end.

  /*modereg la table de corresp. intcor avec le code evenement "FOU"*/
  find intcor no-lock
    where intcor.evenement      = "FOU"
      and intcor.s-evenement    = ""
      and intcor.prefix         = "code-reglm"
      and intcor.codsoc         = ""
      and intcor.etabli         = ""
      and intcor.codtab         = {3}.modreg
  no-error .

  if available intcor  then det-lig[41] = intcor.codint .

  /*code tva la table de corresp. intcor avec le code evenement "FOU"*/
  find intcor no-lock
    where intcor.evenement      = "FOU"
      and intcor.s-evenement    = ""
      and intcor.prefix         = "type-taxe"
      and intcor.codsoc         = ""
      and intcor.etabli         = ""
      and intcor.codtab         = {3}.typtaxe  
  no-error .

  if available intcor  then det-lig[30] = intcor.codint .



end. /*var-type-client */

