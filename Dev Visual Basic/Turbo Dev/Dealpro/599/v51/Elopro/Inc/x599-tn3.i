/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/elopro/inc/x599-tn3.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  4469 12-03-03!New!lbo            !Interface Oracle Fournisseur                                          !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/*- x599-tn3.i -*/

if not available x-spe
then do:
     create x-spe .
     assign x-spe.evenement   = parint.evenement
            x-spe.s-evenement = parint.s-evene
            x-spe.prefix      = "SPECIF"
            x-spe.codsoc      = "P-L"
            x-spe.etabli      = ""
            x-spe.codint      = parsai-cle.
end.

{majmoucb.i "x-spe" }
assign x-spe.ascii-pos = tnt-pos
       x-spe.ascii-lgr = tnt-lgr
       x-spe.codtab    = tnt-nomint
       x-spe.libel     = tnt-def .