/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/elopro/inc/x599banx.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  5428 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5426 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5425 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5424 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!V52  5422 19-06-03!Evo!air            !tableaux de bords EUROGEM                                             !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
/* permet de modifier des variables avant traitement : nom du scenario 2 d'actualisation des budgets */

if ban-refaxe = "ZZZ" 
and ban-scene2 > "" 
then for last tables where tables.codsoc = codsoc-soc
                       and tables.etabli = codetb-etb
                       and tables.typtab = "BUD"
                       and tables.prefix = "CLOT-BUD"
                       and tables.codtab begins string( ban-execpt , "x(4)" )
                       no-lock :

        ban-scene2 = "Z" + string ( tables.nombre[31] , "99" ) no-error  . 

end. /*&FIN*/ /*&FIN*/ /*&FIN*/ /*&FIN*/