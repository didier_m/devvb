/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 599/elopro/inc/eaff-zo1.i                                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52  4528 17-03-03!New!mat            !EXPORT - Fiche Affaire et ITEM par EPAC                               !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/
IF AVAILABLE B-ELOAFF
THEN REPEAT :
        case motcle : 
          when "datfin-valid" 
          then do:
              find tables where tables.codsoc = b-eloaff.codsoc
                            and tables.etabli = ""
                            and tables.typtab = "par"
                            and tables.prefix = "periode"
                            and tables.codtab = b-eloaff.execpt-clo + b-eloaff.percpt-clo + b-eloaff.percpt-clo
                            no-lock no-error.
              if available tables then zone[a] =  string(tables.dattab[2]). 
                                  else zone[a] = "".
          end.
        end case . /* motcle */ 
LEAVE .
END. /* IF AVAILABLE B-eloaff */

IF AVAILABLE B-ELOAFX
THEN REPEAT :
    case motcle:
        when "statut"
        then do:
            if b-eloafx.libel[13] = "0" then zone[a] = "Y".
                                        else if b-eloafx.libel[13] <> "0" then zone[a] = "N".
                                                                      /*   else zone[a] = "". */
        end.
    end case.
LEAVE .
END. /* IF AVAILABLE B-eloaff */

IF AVAILABLE B-ELOAFT
THEN REPEAT :
    case motcle:
        when "item"
        then do:
            zone[a] = string(b-eloaft.codspe , "x(8)") .
        end.
    end case.

    If rupture-a = 0 then leave .

LEAVE .
END. /* IF AVAILABLE B-eloaft */




