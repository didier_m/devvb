VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmFormatCell 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Format cellule"
   ClientHeight    =   4275
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5805
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4275
   ScaleWidth      =   5805
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   2640
      Top             =   3840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Choix d'une couleur"
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Height          =   375
      Left            =   4680
      Picture         =   "frmFormatCell.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   3840
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   5280
      Picture         =   "frmFormatCell.frx":038A
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   3840
      Width           =   495
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3735
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   6588
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "Police"
      TabPicture(0)   =   "frmFormatCell.frx":07CC
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label3"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label4"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label5"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "List1"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "List2"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Check1"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Check2"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Check3"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Picture1"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Picture2"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Frame1"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Command3"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Command4"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).ControlCount=   15
      TabCaption(1)   =   "Format"
      TabPicture(1)   =   "frmFormatCell.frx":07E8
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame2"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Option3"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Text1"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Option2"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "Option1"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).ControlCount=   5
      TabCaption(2)   =   "Alignement"
      TabPicture(2)   =   "frmFormatCell.frx":0804
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Label8"
      Tab(2).Control(1)=   "Image1"
      Tab(2).Control(2)=   "Label9"
      Tab(2).Control(3)=   "Toolbar1"
      Tab(2).Control(4)=   "ImageList1"
      Tab(2).ControlCount=   5
      Begin VB.Frame Frame2 
         Height          =   1095
         Left            =   -74400
         TabIndex        =   25
         Top             =   2520
         Width           =   4575
         Begin VB.CheckBox Check7 
            Caption         =   "Valeur absolue"
            Height          =   255
            Left            =   2280
            TabIndex        =   32
            Top             =   240
            Width           =   1455
         End
         Begin VB.CheckBox Check4 
            Caption         =   "%"
            Height          =   255
            Left            =   3840
            TabIndex        =   31
            Top             =   240
            Width           =   495
         End
         Begin VB.CheckBox Check8 
            Caption         =   "Couleur si n�gatif"
            Height          =   375
            Left            =   120
            TabIndex        =   30
            Top             =   600
            Width           =   1215
         End
         Begin VB.PictureBox Picture3 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   1440
            ScaleHeight     =   225
            ScaleWidth      =   345
            TabIndex        =   29
            Top             =   720
            Width           =   375
         End
         Begin VB.CommandButton Command5 
            Height          =   255
            Left            =   1815
            Picture         =   "frmFormatCell.frx":0820
            Style           =   1  'Graphical
            TabIndex        =   28
            Top             =   720
            Width           =   255
         End
         Begin VB.CheckBox Check6 
            Caption         =   "Separateur de millier"
            Height          =   375
            Left            =   2280
            TabIndex        =   27
            Top             =   600
            Width           =   1815
         End
         Begin VB.ListBox List3 
            Height          =   255
            Left            =   1440
            Sorted          =   -1  'True
            TabIndex        =   26
            Top             =   240
            Width           =   615
         End
         Begin VB.Label Label7 
            Caption         =   "D�cimales : "
            Height          =   255
            Left            =   120
            TabIndex        =   33
            Top             =   240
            Width           =   975
         End
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   -69960
         Top             =   120
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   60
         ImageHeight     =   30
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   9
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFormatCell.frx":0916
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFormatCell.frx":0E60
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFormatCell.frx":13AA
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFormatCell.frx":18F4
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFormatCell.frx":1E3E
               Key             =   ""
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFormatCell.frx":2388
               Key             =   ""
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFormatCell.frx":28D2
               Key             =   ""
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFormatCell.frx":2E1C
               Key             =   ""
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmFormatCell.frx":3366
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   540
         Left            =   -74760
         TabIndex        =   23
         Top             =   1680
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   953
         ButtonWidth     =   1773
         ButtonHeight    =   953
         Style           =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   9
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   2
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   4
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   5
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   6
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   7
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   8
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   9
            EndProperty
         EndProperty
      End
      Begin VB.CommandButton Command4 
         Height          =   255
         Left            =   2175
         Picture         =   "frmFormatCell.frx":38B0
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   3120
         Width           =   255
      End
      Begin VB.CommandButton Command3 
         Height          =   255
         Left            =   975
         Picture         =   "frmFormatCell.frx":39A6
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   3120
         Width           =   255
      End
      Begin VB.OptionButton Option3 
         Caption         =   "Appliquer un format num�rique � la cellule : "
         Height          =   255
         Left            =   -74640
         TabIndex        =   19
         Top             =   2280
         Width           =   3495
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   -74400
         TabIndex        =   18
         Top             =   1800
         Width           =   4575
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Ne pas appliquer un format. Remplacer la valeur par un texte de substitution : "
         Height          =   375
         Left            =   -74640
         TabIndex        =   17
         Top             =   1320
         Width           =   5175
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Ne pas appliquer un format. Traiter la valeur telle qu'elle est d�finie dans la source de donn�es."
         Height          =   495
         Left            =   -74640
         TabIndex        =   16
         Top             =   600
         Value           =   -1  'True
         Width           =   5055
      End
      Begin VB.Frame Frame1 
         Caption         =   "Exemple : "
         Height          =   1575
         Left            =   3120
         TabIndex        =   13
         Top             =   1800
         Width           =   2415
         Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
            Height          =   1215
            Left            =   120
            TabIndex        =   34
            Top             =   240
            Width           =   2055
            _ExtentX        =   3625
            _ExtentY        =   2143
            _Version        =   393216
            Rows            =   1
            Cols            =   1
            FixedRows       =   0
            FixedCols       =   0
            AllowBigSelection=   0   'False
            FocusRect       =   0
            HighLight       =   0
            ScrollBars      =   0
            BorderStyle     =   0
            Appearance      =   0
         End
      End
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1560
         ScaleHeight     =   225
         ScaleWidth      =   585
         TabIndex        =   12
         Top             =   3120
         Width           =   615
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   360
         ScaleHeight     =   225
         ScaleWidth      =   585
         TabIndex        =   10
         Top             =   3120
         Width           =   620
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Soulign�"
         Height          =   255
         Left            =   4440
         TabIndex        =   8
         Top             =   1450
         Width           =   975
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Italic"
         Height          =   255
         Left            =   4440
         TabIndex        =   7
         Top             =   1150
         Width           =   735
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Gras"
         Height          =   255
         Left            =   4440
         TabIndex        =   6
         Top             =   840
         Width           =   1095
      End
      Begin VB.ListBox List2 
         Height          =   840
         Left            =   3120
         Sorted          =   -1  'True
         TabIndex        =   2
         Top             =   840
         Width           =   1215
      End
      Begin VB.ListBox List1 
         Height          =   1620
         Left            =   360
         Sorted          =   -1  'True
         TabIndex        =   1
         Top             =   840
         Width           =   2535
      End
      Begin VB.Label Label9 
         Caption         =   "S�lection : "
         Height          =   255
         Left            =   -74760
         TabIndex        =   24
         Top             =   1320
         Width           =   2415
      End
      Begin VB.Image Image1 
         Height          =   450
         Left            =   -71520
         Picture         =   "frmFormatCell.frx":3A9C
         Top             =   840
         Width           =   900
      End
      Begin VB.Label Label8 
         Caption         =   "Alignement interne de la cellule : "
         Height          =   255
         Left            =   -74760
         TabIndex        =   22
         Top             =   600
         Width           =   2535
      End
      Begin VB.Label Label5 
         Caption         =   "Couleur de la cellule :"
         Height          =   375
         Left            =   1560
         TabIndex        =   11
         Top             =   2640
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "Couleur de la police : "
         Height          =   375
         Left            =   360
         TabIndex        =   9
         Top             =   2640
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Attributs : "
         Height          =   255
         Left            =   4440
         TabIndex        =   5
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Taille : "
         Height          =   255
         Left            =   3120
         TabIndex        =   4
         Top             =   480
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Police : "
         Height          =   255
         Left            =   360
         TabIndex        =   3
         Top             =   480
         Width           =   615
      End
   End
End
Attribute VB_Name = "frmFormatCell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Objet Cellule en cours de modif
Public aCell As clsCell

'-> Variable temporaire de formattage de la cellule
Private TempCell As clsCell

'-> Choix du format numeric
Private IdFormat As Integer

'-> Indique que l'on charge
Dim IsLoading As Boolean


Public Sub InitTialisation()

    '---> Cette proc�dure Initialise les param�tres d'une cellule
    Dim aLb As Libelle
    
    '-> Indiquer que l'on charge
    IsLoading = True
    
    '-> Charge les libelles
    Set aLb = Libelles("FRMFORMATCELL")
    
    '-> Attributs de police
    Me.List1.Text = aCell.FormatCell.FontName
    Me.List2.Text = aCell.FormatCell.FontSize
    Me.Check1.Value = Abs(aCell.FormatCell.FontBold)
    Me.Check2.Value = Abs(aCell.FormatCell.FontItalic)
    Me.Check3.Value = Abs(aCell.FormatCell.FontUnderline)
    Me.Picture1.BackColor = aCell.FormatCell.FontColor
    Me.Picture2.BackColor = aCell.FormatCell.BackColor
    
    '-> Attributs de format
    Select Case aCell.FormatCell.Format
        Case 0 'Pas de format
            Me.Option1.Value = True
        Case 1 'Libelle de substitution
            Me.Option2.Value = True
            Me.Text1.Text = aCell.FormatCell.Libelle
        Case 2 'Format num�ric
            Me.Option3.Value = True
            Me.List3.Text = aCell.FormatCell.NbDec
            Me.Check4.Value = Abs(aCell.FormatCell.Pourcentage)
            Me.Check7.Value = Abs(aCell.FormatCell.Absolute)
            Me.Check6.Value = Abs(aCell.FormatCell.UseSepMil)
            Me.Check8.Value = Abs(aCell.FormatCell.UseColorNeg)
            Me.Picture3.BackColor = aCell.FormatCell.ColorNeg
    End Select
    
    '-> Gestion de l'affichage
    SelectOption
    
    '-> Setting de la variable interne
    IdFormat = aCell.FormatCell.Format
    
    '-> Alignement interne de la cellule
    Me.Image1.Picture = Me.ImageList1.ListImages(aCell.FormatCell.InterneAlign).Picture
    
    '-> Dessiner l'exemple de la cellule
    ExecFormatCell aCell.FormatCell, 0, 0, Me.MSFlexGrid1
    Me.MSFlexGrid1.Text = aLb.GetCaption(1)
    
    '-> Faire une copie de la cellule en cellule temporaire
    Set TempCell = New clsCell
    CopyFormat TempCell.FormatCell, aCell.FormatCell
    
    '-> Fin du chargement
    IsLoading = False

End Sub


Private Sub Check1_Click()

    If IsLoading Then Exit Sub
    
    '-> Inverser la propri�t�
    TempCell.FormatCell.FontBold = Not (TempCell.FormatCell.FontBold)
    
    '-> Dessiner l'exemple
    ExecFormatCell TempCell.FormatCell, 0, 0, Me.MSFlexGrid1


End Sub

Private Sub Check2_Click()

    If IsLoading Then Exit Sub
    
    '-> Inverser la propri�t�
    TempCell.FormatCell.FontItalic = Not (TempCell.FormatCell.FontItalic)
    
    '-> Dessiner l'exemple
    ExecFormatCell TempCell.FormatCell, 0, 0, Me.MSFlexGrid1
End Sub

Private Sub Check3_Click()

    If IsLoading Then Exit Sub
    
    '-> Inverser la propri�t�
    TempCell.FormatCell.FontUnderline = Not (TempCell.FormatCell.FontUnderline)
    
    '-> Dessiner l'exemple
    ExecFormatCell TempCell.FormatCell, 0, 0, Me.MSFlexGrid1
End Sub

Private Sub Check4_Click()

    If IsLoading Then Exit Sub
    TempCell.FormatCell.Pourcentage = Not TempCell.FormatCell.Pourcentage

End Sub

Private Sub Check6_Click()

    If IsLoading Then Exit Sub
    TempCell.FormatCell.UseSepMil = Not TempCell.FormatCell.UseSepMil

End Sub

Private Sub Check7_Click()

    If IsLoading Then Exit Sub
    
    TempCell.FormatCell.Absolute = Not TempCell.FormatCell.Absolute

End Sub

Private Sub Check8_Click()

    If IsLoading Then Exit Sub
    TempCell.FormatCell.UseColorNeg = Not TempCell.FormatCell.UseColorNeg
    TempCell.FormatCell.ColorNeg = Me.Picture3.BackColor

End Sub

Private Sub Command1_Click()

    '-> Faire une copie de la cellule temporaire dans la cellule d�finitive
    CopyFormat aCell.FormatCell, TempCell.FormatCell
    
    '-> Indiquer qu'une mise � jour est Ok
    strRetour = "OK"
    
    '-> D�charger la feuille
    Unload Me

End Sub

Private Sub Command2_Click()

    '-> D�charger la feuille
    Unload Me

End Sub

Private Sub Command3_Click()

    '---> Modifier la couleur
    Me.Picture1.BackColor = SelectColor(Me.Picture1.BackColor)
    TempCell.FormatCell.FontColor = Me.Picture1.BackColor
    '-> Dessiner l'exemple
    ExecFormatCell TempCell.FormatCell, 0, 0, Me.MSFlexGrid1

End Sub

Private Sub Command4_Click()

    '---> Modifier la couleur
    Me.Picture2.BackColor = SelectColor(Me.Picture2.BackColor)
    TempCell.FormatCell.BackColor = Me.Picture2.BackColor
    '-> Dessiner l'exemple
    ExecFormatCell TempCell.FormatCell, 0, 0, Me.MSFlexGrid1

End Sub

Private Sub Command5_Click()

    '---> Modifier la couleur
    Me.Picture3.BackColor = SelectColor(Me.Picture3.BackColor)
    TempCell.FormatCell.ColorNeg = Me.Picture3.BackColor

End Sub

Private Sub Form_Load()

    '---> Charger la liste de toutes les polices
    Dim i As Integer
    Dim aRect As RECT
    
    For i = 0 To Screen.FontCount - 1
        Me.List1.AddItem Screen.Fonts(i)
    Next
    
    '-> Charger la taille des polices
    For i = 8 To 72 Step 2
        Me.List2.AddItem i
    Next
    
    '-> Charger la liste des d�cimales
    For i = 0 To 5
        Me.List3.AddItem i
    Next
    
    '-> Donner sa dimension � la cellule exemple
    GetClientRect Me.MSFlexGrid1.hwnd, aRect
    Me.MSFlexGrid1.RowHeight(0) = Me.ScaleY(aRect.Bottom, 3, 1)
    Me.MSFlexGrid1.ColWidth(0) = Me.ScaleX(aRect.Right, 3, 1)

End Sub

Private Function SelectColor(aColor As Long) As Long

    '---> Cette fonction a pour but d'afficher le choix d'une couleur
    
    On Error GoTo GestError
    
    Me.CommonDialog1.Color = aColor
    Me.CommonDialog1.ShowColor
    SelectColor = Me.CommonDialog1.Color
    Exit Function
    
GestError:
    SelectColor = aColor

End Function

Private Sub List1_Click()
    If IsLoading Then Exit Sub
    '-> Modifier la police de la cellule
    TempCell.FormatCell.FontName = Me.List1.Text
    '-> Dessiner l'exemple
    ExecFormatCell TempCell.FormatCell, 0, 0, Me.MSFlexGrid1
End Sub

Private Sub List2_Click()

    If IsLoading Then Exit Sub
    
    '-> Taille de la police
    TempCell.FormatCell.FontSize = CInt(Me.List2.Text)
    '-> Dessiner l'exemple
    ExecFormatCell TempCell.FormatCell, 0, 0, Me.MSFlexGrid1


End Sub

Private Sub List3_Click()

    If IsLoading Then Exit Sub
    
    '-> NbDec
    TempCell.FormatCell.NbDec = CInt(Me.List3.Text)


End Sub

Private Sub Option1_Click()

    If IsLoading Then Exit Sub
    TempCell.FormatCell.Format = 0
    SelectOption

End Sub


Private Sub SelectOption()

    '---> Setting des objets en affichage selon le format � appliquer
    
    If IsLoading Then Exit Sub
    
    Select Case TempCell.FormatCell.Format
        Case 0
            Me.Text1.Enabled = False
            Me.Frame2.Enabled = False
        Case 1
            Me.Text1.Enabled = True
            Me.Frame2.Enabled = False
        Case 2
            Me.Text1.Enabled = False
            Me.Frame2.Enabled = True
    End Select

End Sub

Private Sub Option2_Click()

    If IsLoading Then Exit Sub
    TempCell.FormatCell.Format = 1
    TempCell.FormatCell.Libelle = Me.Text1.Text
    SelectOption

End Sub

Private Sub Option3_Click()

    If IsLoading Then Exit Sub
    TempCell.FormatCell.Format = 2
    
    '-> NbDec
    If Me.List3.Text = "" Then Me.List3.ListIndex = 0
    TempCell.FormatCell.NbDec = CInt(Me.List3.Text)
    
    '-> Valeur absolue
    If Me.Check7.Value = 1 Then
        TempCell.FormatCell.Absolute = True
    Else
        TempCell.FormatCell.Absolute = False
    End If
    '-> Pourcentage
    If Me.Check4.Value = 1 Then
        TempCell.FormatCell.Pourcentage = True
    Else
        TempCell.FormatCell.Pourcentage = False
    End If
    '-> S�parateur de milier
    If Me.Check6.Value = 1 Then
        TempCell.FormatCell.UseSepMil = True
    Else
        TempCell.FormatCell.UseSepMil = False
    End If
    '-> UseColorNeg
    If Me.Check8.Value = 1 Then
        TempCell.FormatCell.UseColorNeg = True
        TempCell.FormatCell.ColorNeg = Me.Picture3.BackColor
    Else
        TempCell.FormatCell.UseColorNeg = False
    End If
    SelectOption


End Sub

Private Sub Text1_Change()

    If IsLoading Then Exit Sub
    
    If TempCell.FormatCell.Format = 1 Then TempCell.FormatCell.Libelle = Me.Text1.Text

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    If IsLoading Then Exit Sub
    '-> Mettre � jour la variable
    TempCell.FormatCell.InterneAlign = Button.Index
    '-> Charger la bonne image
    Me.Image1.Picture = Me.ImageList1.ListImages(TempCell.FormatCell.InterneAlign).Picture
    '-> Dessiner l'exemple
    ExecFormatCell TempCell.FormatCell, 0, 0, Me.MSFlexGrid1
End Sub
