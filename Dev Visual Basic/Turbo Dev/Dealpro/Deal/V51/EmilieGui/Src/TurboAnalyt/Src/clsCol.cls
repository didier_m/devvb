VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'******************
'* Classe Colonne *
'******************

Public Libelle As String '-> Libelle de la colonne
Public Code As String
Public Visible As Boolean
Public TypeCol As Integer '-> Type de colonne : 0 = Entry, 1 = Calcul, 2 = S�paration
Public Largeur As Single '-> Largeur de la colonne
Public LargeurTwips As Long '-> Largeur en twips de la colonne
Public Calcul As String
Public CalculRtf As String

Public Position As Integer '-> Indique la position de la colonne dans la matrice
Public Field As String '-> Pour les champs de base, indiquer le champ affecter
Public Progiciel As String '-> Code du progiciel de rattachement

Private Sub Class_Initialize()

    '-> Initialiser la largeur par d�faut de la colonne
    Largeur = 30

End Sub
