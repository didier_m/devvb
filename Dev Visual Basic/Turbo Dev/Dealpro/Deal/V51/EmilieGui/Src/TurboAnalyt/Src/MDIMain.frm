VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.MDIForm MDIMain 
   BackColor       =   &H8000000C&
   Caption         =   "MDIForm1"
   ClientHeight    =   5745
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   8715
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   5520
      Top             =   1200
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
   Begin VB.PictureBox picSplit 
      Align           =   3  'Align Left
      BorderStyle     =   0  'None
      Height          =   5745
      Left            =   3045
      MousePointer    =   9  'Size W E
      ScaleHeight     =   5745
      ScaleWidth      =   105
      TabIndex        =   1
      Top             =   0
      Width           =   105
   End
   Begin VB.PictureBox picNaviga 
      Align           =   3  'Align Left
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   5745
      Left            =   0
      ScaleHeight     =   383
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   203
      TabIndex        =   0
      Top             =   0
      Width           =   3045
      Begin MSComctlLib.ImageList ImageList2 
         Left            =   1320
         Top             =   3960
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   3
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MDIMain.frx":0000
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MDIMain.frx":059A
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MDIMain.frx":0B34
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   1320
         Top             =   4560
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   1
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MDIMain.frx":0ECE
               Key             =   "TAB"
            EndProperty
         EndProperty
      End
      Begin VB.PictureBox picTitre 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         Height          =   90
         Left            =   120
         ScaleHeight     =   6
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   128
         TabIndex        =   3
         Top             =   3000
         Width           =   1920
      End
      Begin MSComctlLib.TreeView TreeNaviga 
         Height          =   2175
         Left            =   0
         TabIndex        =   2
         Top             =   240
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   3836
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   265
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin RichTextLib.RichTextBox RichTextBox1 
         Height          =   855
         Left            =   0
         TabIndex        =   4
         Top             =   0
         Visible         =   0   'False
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   1508
         _Version        =   393217
         TextRTF         =   $"MDIMain.frx":1357
      End
      Begin VB.Image imgClose 
         Height          =   165
         Left            =   2160
         Picture         =   "MDIMain.frx":13D9
         Top             =   0
         Width           =   165
      End
   End
   Begin VB.Menu mnuFichier 
      Caption         =   ""
      Begin VB.Menu mnuOpen 
         Caption         =   ""
      End
      Begin VB.Menu mnuNew 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   ""
      End
   End
   Begin VB.Menu mnuFenetre 
      Caption         =   ""
      Begin VB.Menu mnuStructure 
         Caption         =   ""
      End
      Begin VB.Menu mnuMosH 
         Caption         =   ""
      End
      Begin VB.Menu mnuMosV 
         Caption         =   ""
      End
      Begin VB.Menu mnuCascade 
         Caption         =   ""
      End
      Begin VB.Menu mnuReorIco 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "MDIMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim aTb As clsTableau

Dim aNodePrev As Node

Private Sub MDIForm_Load()
    
    Dim aLb As Libelle
    
    '-> Chargement des messages
    Set aLb = Libelles("MDIMAIN")
    
    '---> Init de l'�cran MESPPROG
    Me.Caption = aLb.GetCaption(1)
    
    '-> Menu Fichier
    Me.mnuFichier.Caption = aLb.GetCaption(2)
    Me.mnuOpen.Caption = aLb.GetCaption(3)
    Me.mnuNew.Caption = aLb.GetCaption(4)
    Me.mnuExit.Caption = aLb.GetCaption(5)
    
    '-> Menu Fen�tre
    Me.mnuFenetre.Caption = aLb.GetCaption(6)
    Me.mnuStructure.Caption = aLb.GetCaption(7)
    Me.mnuMosH.Caption = aLb.GetCaption(8)
    Me.mnuMosV.Caption = aLb.GetCaption(9)
    Me.mnuCascade.Caption = aLb.GetCaption(10)
    Me.mnuReorIco.Caption = aLb.GetCaption(11)
        
    Set aLb = Nothing
        
    '-> Par d�faut afficher la fen�tre de structure
    mnuStructure_Click
    
    Me.TreeNaviga.ImageList = Me.ImageList1
    
    '-> Charger la feuille des menus
    Load frmMenu

End Sub


Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    Dim Rep As VbMsgBoxResult
    Dim aLb As Libelle
    
    '-> Chargement des messages
    Set aLb = Libelles("MDIMAIN")
    
    '-> Demander confirmation MESSPROG
    Rep = MsgBox(aLb.GetCaption(12), vbQuestion + vbYesNo, "Quitter")
    If Rep = vbNo Then
        Cancel = 1
        Exit Sub
    End If
    
    '-> Libere le pointeur
    Set aLb = Nothing
    
    '-> Decharger la feuille de menu
    Unload frmMenu

End Sub

Private Sub mnuExit_Click()

    '-> Fin du prog
    Unload Me

End Sub

Private Sub mnuCascade_Click()
    Me.Arrange 0
End Sub

Private Sub mnuMosH_Click()
    Me.Arrange 1
End Sub

Private Sub mnuMosV_Click()
    Me.Arrange 2
End Sub

Private Sub mnuNew_Click()
    '---> Ouvre un nouveau tableau en creation
    Dim aTb As clsTableau
    Dim aFrm As frmTableau
    Dim StrTempo As String
    Dim aNode As Node
    
    '-> Initialise une nouvelle classe tableau
    Set aTb = New clsTableau
    
    '-> saisie du nom et du code du nouveau tableau
    frmAdd.TypeAdd = 2
    frmAdd.Show vbModal

    '-> On affecte le nom du tableau
    aTb.Name = UCase$(Trim(Entry(1, strRetour, Chr(0))))
    aTb.Libel = UCase$(Trim(Entry(2, strRetour, Chr(0))))
    
    '-> Ajouter dans la collection
    Tableaux.Add aTb, "TB_" & UCase$(aTb.Name)
    
    '-> Charger une feuille
    Set aFrm = New frmTableau
    
    '-> Ajouter dans le treeview
    StrTempo = "TB_" & UCase$(aTb.Name)
    Set aNode = Me.TreeNaviga.Nodes.Add(, , StrTempo, aTb.Name & " - " & aTb.Libel, 1)
    aNode.Tag = aFrm.hwnd
    aNode.Selected = True
    aFrm.aNodeKey = StrTempo
    
    '-> Dessiner le tableau
    aFrm.Init aTb
    
End Sub

Private Sub mnuOpen_Click()

    '---> Ouverture d'un tableau
    Dim aFrm As frmTableau
    Dim StrTempo As String
    Dim aNode As Node
    Dim aProgiciel As clsProgiciel
    Dim aDes As clsDescriptif
    Dim aField As clsDescriptif
    Dim i As Integer
    Dim Ligne As String
    Dim hdlFile As Integer
    Dim IndNode As Integer
    Dim NbLignes As Integer
    
    Dim Fichier As String
    
    On Error GoTo ErrorOpen
    
    frmOuvrir.Show vbModal
    '-> Si on a annul� l'action
    If strRetour = "" Then Exit Sub
    '-> On recupere le nom du fichier a analyser
    Fichier = strRetour
    
    '-> Lancer la cr�ation d'un nouveau tableau
    Set aTb = LoadTb(Fichier)
    
    '-> Ajouter dans la collection
    Tableaux.Add aTb, "TB_" & UCase$(aTb.Name)
    
    '-> Charger une feuille
    Set aFrm = New frmTableau
    
    '-> Ajouter dans le treeview
    StrTempo = "TB_" & UCase$(aTb.Name)
    Set aNode = Me.TreeNaviga.Nodes.Add(, , StrTempo, aTb.Name & " - " & aTb.Libel, 1)
    aNode.Tag = aFrm.hwnd
    aNode.Selected = True
    aFrm.aNodeKey = StrTempo
    
    '-> Dessiner le tableau
    aFrm.Init aTb
    
    '-> Quitter la proc�dure
    Exit Sub
    
ErrorOpen:
    
    'MsgBox "Erreur dans l'ouverture du tableau", vbCritical + vbOKOnly, "Erreur dans l'ouveture du tableau"
    Set aTb = Nothing
    If Not aFrm Is Nothing Then
        Me.TreeNaviga.Nodes.Remove (aFrm.aNodeKey)
        Unload aFrm
    End If

End Sub

Private Sub mnuReorIco_Click()
    Me.Arrange 3
End Sub

Private Sub mnuStructure_Click()

    '-> Afficher ou ne pas afficher le volet exploration de spool
    IsNaviga = Not IsNaviga
    Me.mnuStructure.Checked = IsNaviga
    Me.picSplit.Visible = IsNaviga
    Me.picNaviga.Visible = IsNaviga

End Sub

Private Sub picNaviga_Resize()

    Dim aRect As RECT
    Dim Res As Long
    
    On Error Resume Next
    
    '-> R�cup�rer la taille de la zone cliente
    Res = GetClientRect(Me.picNaviga.hwnd, aRect)
    
    '-> Positionner l'entete de la fen�tre
    Me.picTitre.Left = 3
    Me.picTitre.Top = 7
    Me.picTitre.Width = aRect.Right - 6 - imgClose.Width
    Me.imgClose.Left = aRect.Right - imgClose.Width
    Me.imgClose.Top = Me.picTitre.Top - 2
    
    '-> Positionner le treeview
    Me.TreeNaviga.Left = Me.picTitre.Left
    Me.TreeNaviga.Top = Me.picTitre.Top * 2 + Me.picTitre.Height
    Me.TreeNaviga.Width = Me.picTitre.Width + Me.imgClose.Width + 6
    Me.TreeNaviga.Height = Me.picNaviga.ScaleY(Me.picNaviga.Height, 1, 3) - 21


End Sub

Private Sub picSplit_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    Dim aPt As POINTAPI
    Dim Res As Long
    
    If Button = vbLeftButton Then
        '-> R�cup�rer la position du curseur
        Res = GetCursorPos(aPt)
        '-> Convertir en coordonn�es clientes
        Res = ScreenToClient(Me.hwnd, aPt)
        '-> Tester les positions mini et maxi
        If aPt.X > Me.picNaviga.ScaleX(2, 7, 3) And aPt.X < Me.picNaviga.ScaleX(Me.Width, 1, 3) - Me.picNaviga.ScaleX(2, 7, 3) Then Me.picNaviga.Width = Me.picNaviga.ScaleX(aPt.X, 3, 1)
    End If

End Sub

Private Sub picTitre_Resize()

    '---> Dessiner la barre de titre
    Me.picTitre.Line (0, 0)-(Me.picTitre.Width - 1, 0), RGB(255, 255, 255)
    Me.picTitre.Line (0, 1)-(Me.picTitre.Width - 1, 1), RGB(255, 255, 255)
    Me.picTitre.Line (1, 1)-(Me.picTitre.Width - 1, 1), &HE0E0E0
    Me.picTitre.Line (0, 3)-(Me.picTitre.Width - 1, 3), RGB(255, 255, 255)
    Me.picTitre.Line (0, 4)-(Me.picTitre.Width - 1, 4), RGB(255, 255, 255)
    Me.picTitre.Line (1, 4)-(Me.picTitre.Width - 1, 4), &HE0E0E0

End Sub

Private Sub TreeNaviga_DblClick()

    '-> Ne rien faire s'il n'y a pas de nodes
    If Me.TreeNaviga.Nodes.Count = 0 Then Exit Sub
    
    '-> V�rifier qu'une icone soit s�lectionn�e
    If Me.TreeNaviga.SelectedItem Is Nothing Then Exit Sub
    
    '-> Activer la feuille sp�cifi�e
'    SendMessage CLng(Me.TreeNaviga.SelectedItem.Tag), WM_CHILDACTIVATE, 0, 0

End Sub

