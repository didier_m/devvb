VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmParamEntete 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Param�trage du tableau"
   ClientHeight    =   9255
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   7350
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9255
   ScaleWidth      =   7350
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   6720
      Top             =   6840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   6600
      Picture         =   "frmParamEntete.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   32
      Top             =   8880
      Width           =   615
   End
   Begin VB.Frame Frame9 
      Caption         =   "Exemple : "
      Height          =   2655
      Left            =   0
      TabIndex        =   62
      Top             =   6600
      Width           =   6495
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
         Height          =   2295
         Left            =   120
         TabIndex        =   63
         Top             =   240
         Width           =   6255
         _ExtentX        =   11033
         _ExtentY        =   4048
         _Version        =   393216
         Rows            =   3
         Cols            =   3
         ScrollBars      =   0
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Entete de colonne : "
      Height          =   2295
      Left            =   3720
      TabIndex        =   25
      Top             =   4320
      Width           =   3615
      Begin VB.ListBox List4 
         Height          =   255
         Left            =   2760
         TabIndex        =   28
         Top             =   720
         Width           =   735
      End
      Begin VB.ListBox List1 
         Height          =   1035
         ItemData        =   "frmParamEntete.frx":0442
         Left            =   1800
         List            =   "frmParamEntete.frx":0444
         TabIndex        =   31
         Top             =   1200
         Width           =   1695
      End
      Begin VB.ComboBox Combo2 
         Height          =   315
         Left            =   960
         Sorted          =   -1  'True
         TabIndex        =   26
         Top             =   360
         Width           =   2535
      End
      Begin VB.CheckBox Check4 
         Caption         =   "Gras"
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   1440
         Width           =   855
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Italic"
         Height          =   255
         Left            =   240
         TabIndex        =   33
         Top             =   1680
         Width           =   735
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Souslign�"
         Height          =   255
         Left            =   240
         TabIndex        =   30
         Top             =   1920
         Width           =   1335
      End
      Begin VB.PictureBox picColorCol 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1800
         ScaleHeight     =   225
         ScaleWidth      =   345
         TabIndex        =   27
         Top             =   720
         Width           =   375
      End
      Begin VB.Label Label17 
         Caption         =   "Alignement interne : "
         Height          =   255
         Left            =   240
         TabIndex        =   57
         Top             =   1080
         Width           =   1455
      End
      Begin VB.Label Label16 
         Caption         =   "Couleur de la police : "
         Height          =   255
         Left            =   240
         TabIndex        =   56
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label8 
         Caption         =   "Taille : "
         Height          =   255
         Left            =   2280
         TabIndex        =   55
         Top             =   720
         Width           =   615
      End
      Begin VB.Label Label6 
         Caption         =   "Police : "
         Height          =   255
         Left            =   240
         TabIndex        =   54
         Top             =   360
         Width           =   615
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Entete de ligne : "
      Height          =   2295
      Left            =   0
      TabIndex        =   17
      Top             =   4320
      Width           =   3615
      Begin VB.ListBox List3 
         Height          =   255
         Left            =   2760
         TabIndex        =   20
         Top             =   720
         Width           =   735
      End
      Begin VB.ListBox List2 
         Height          =   1035
         Left            =   1800
         TabIndex        =   24
         Top             =   1080
         Width           =   1695
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   960
         Sorted          =   -1  'True
         TabIndex        =   18
         Top             =   360
         Width           =   2535
      End
      Begin VB.PictureBox picColorLig 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1800
         ScaleHeight     =   225
         ScaleWidth      =   345
         TabIndex        =   19
         Top             =   720
         Width           =   375
      End
      Begin VB.CheckBox lblFontLigUnder 
         Caption         =   "Souslign�"
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   1920
         Width           =   1335
      End
      Begin VB.CheckBox lblFontLigItalic 
         Caption         =   "Italic"
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   1680
         Width           =   735
      End
      Begin VB.CheckBox lblFontLigBold 
         Caption         =   "Gras"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label15 
         Caption         =   "Police : "
         Height          =   255
         Left            =   240
         TabIndex        =   39
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label13 
         Caption         =   "Taille : "
         Height          =   255
         Left            =   2280
         TabIndex        =   38
         Top             =   720
         Width           =   615
      End
      Begin VB.Label Label12 
         Caption         =   "Couleur de la police : "
         Height          =   255
         Left            =   240
         TabIndex        =   37
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Label11 
         Caption         =   "Alignement interne : "
         Height          =   255
         Left            =   240
         TabIndex        =   36
         Top             =   1080
         Width           =   1455
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Param�trage g�n�ral : "
      Height          =   4215
      Left            =   0
      TabIndex        =   34
      Top             =   0
      Width           =   7335
      Begin VB.TextBox Text4 
         Height          =   285
         Left            =   240
         TabIndex        =   2
         Top             =   3720
         Width           =   2175
      End
      Begin VB.TextBox Text3 
         Height          =   285
         Left            =   4440
         TabIndex        =   16
         Top             =   3720
         Width           =   2655
      End
      Begin VB.Frame Frame10 
         Caption         =   "Dimensions : "
         Height          =   1095
         Left            =   2640
         TabIndex        =   11
         Top             =   3000
         Width           =   1695
         Begin VB.VScrollBar VScroll2 
            Height          =   255
            Left            =   1320
            TabIndex        =   15
            Top             =   720
            Width           =   255
         End
         Begin VB.VScrollBar VScroll1 
            Height          =   255
            Left            =   1320
            TabIndex        =   13
            Top             =   360
            Width           =   255
         End
         Begin VB.TextBox Text2 
            Enabled         =   0   'False
            Height          =   285
            Left            =   960
            TabIndex        =   14
            Top             =   720
            Width           =   375
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   285
            Left            =   960
            TabIndex        =   12
            Top             =   360
            Width           =   375
         End
         Begin VB.Label Label21 
            Caption         =   "Largeur : "
            Height          =   255
            Left            =   120
            TabIndex        =   65
            Top             =   720
            Width           =   735
         End
         Begin VB.Label Label20 
            Caption         =   "Hauteur : "
            Height          =   255
            Left            =   120
            TabIndex        =   64
            Top             =   360
            Width           =   735
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Redimensionner : "
         Height          =   1095
         Left            =   4920
         TabIndex        =   10
         Top             =   2160
         Width           =   2175
         Begin VB.OptionButton optRedim 
            Caption         =   "Les deux"
            Height          =   255
            Index           =   3
            Left            =   1080
            TabIndex        =   61
            Top             =   720
            Width           =   975
         End
         Begin VB.OptionButton optRedim 
            Caption         =   "Lignes"
            Height          =   255
            Index           =   2
            Left            =   1080
            TabIndex        =   60
            Top             =   360
            Width           =   855
         End
         Begin VB.OptionButton optRedim 
            Caption         =   "Colonnes"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   59
            Top             =   720
            Width           =   1095
         End
         Begin VB.OptionButton optRedim 
            Caption         =   "Aucun"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   58
            Top             =   360
            Width           =   855
         End
      End
      Begin VB.PictureBox Picture6 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4320
         ScaleHeight     =   225
         ScaleWidth      =   345
         TabIndex        =   9
         Top             =   2400
         Width           =   375
      End
      Begin VB.PictureBox Picture5 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   6720
         ScaleHeight     =   225
         ScaleWidth      =   345
         TabIndex        =   8
         Top             =   1800
         Width           =   375
      End
      Begin VB.Frame Frame5 
         Caption         =   "Type de ligne des cellules : "
         Height          =   1335
         Left            =   240
         TabIndex        =   0
         Top             =   360
         Width           =   2295
         Begin VB.OptionButton optLigCell 
            Caption         =   "Raised"
            Height          =   255
            Index           =   3
            Left            =   1080
            TabIndex        =   51
            Top             =   840
            Width           =   975
         End
         Begin VB.OptionButton optLigCell 
            Caption         =   "Inset"
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   50
            Top             =   840
            Value           =   -1  'True
            Width           =   855
         End
         Begin VB.OptionButton optLigCell 
            Caption         =   "Flat"
            Height          =   255
            Index           =   1
            Left            =   1080
            TabIndex        =   49
            Top             =   360
            Width           =   735
         End
         Begin VB.OptionButton optLigCell 
            Caption         =   "None"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   48
            Top             =   360
            Width           =   1215
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Type de ligne des entetes : "
         Height          =   1335
         Left            =   240
         TabIndex        =   1
         Top             =   1800
         Width           =   2295
         Begin VB.OptionButton optLigEntete 
            Caption         =   "Raised"
            Height          =   255
            Index           =   3
            Left            =   1080
            TabIndex        =   47
            Top             =   840
            Width           =   975
         End
         Begin VB.OptionButton optLigEntete 
            Caption         =   "Inset"
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   46
            Top             =   840
            Value           =   -1  'True
            Width           =   735
         End
         Begin VB.OptionButton optLigEntete 
            Caption         =   "Flat"
            Height          =   255
            Index           =   1
            Left            =   1080
            TabIndex        =   45
            Top             =   360
            Width           =   615
         End
         Begin VB.OptionButton optLigEntete 
            Caption         =   "None"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   44
            Top             =   360
            Width           =   855
         End
      End
      Begin VB.PictureBox Picture4 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000D&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4320
         ScaleHeight     =   225
         ScaleWidth      =   345
         TabIndex        =   7
         Top             =   1800
         Width           =   375
      End
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   6720
         ScaleHeight     =   225
         ScaleWidth      =   345
         TabIndex        =   6
         Top             =   1200
         Width           =   375
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Appliquer une bordure au tableau"
         Height          =   495
         Left            =   2640
         TabIndex        =   3
         Top             =   360
         Value           =   1  'Checked
         Width           =   2055
      End
      Begin VB.Frame Frame2 
         Height          =   615
         Left            =   4800
         TabIndex        =   4
         Top             =   360
         Width           =   2295
         Begin VB.ListBox List5 
            Height          =   255
            Left            =   1560
            TabIndex        =   66
            Top             =   240
            Width           =   615
         End
         Begin VB.OptionButton optBorder 
            Caption         =   "3D"
            Height          =   255
            Index           =   1
            Left            =   960
            TabIndex        =   41
            Top             =   240
            Value           =   -1  'True
            Width           =   615
         End
         Begin VB.OptionButton optBorder 
            Caption         =   "Flat"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   40
            Top             =   240
            Width           =   615
         End
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4320
         ScaleHeight     =   225
         ScaleWidth      =   345
         TabIndex        =   5
         Top             =   1200
         Width           =   375
      End
      Begin VB.Label Label9 
         Caption         =   "Libell� du tableau : "
         Height          =   255
         Left            =   240
         TabIndex        =   68
         Top             =   3360
         Width           =   1455
      End
      Begin VB.Label Label7 
         Caption         =   "Libell� de l'angle : "
         Height          =   255
         Left            =   4440
         TabIndex        =   67
         Top             =   3360
         Width           =   1335
      End
      Begin VB.Label Label5 
         Caption         =   "Couleur des lignes des cellules : "
         Height          =   495
         Left            =   2640
         TabIndex        =   53
         Top             =   2280
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "Couleur des lignes des ent�tes  : "
         Height          =   495
         Left            =   4920
         TabIndex        =   52
         Top             =   1680
         Width           =   1575
      End
      Begin VB.Label Label3 
         Caption         =   "Couleur des cellules s�lectionn�es : "
         Height          =   495
         Left            =   2640
         TabIndex        =   43
         Top             =   1680
         Width           =   1455
      End
      Begin VB.Label Label2 
         Caption         =   "Couleur du tableau : "
         Height          =   255
         Left            =   4920
         TabIndex        =   42
         Top             =   1200
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Couleur des ent�tes : "
         Height          =   255
         Left            =   2640
         TabIndex        =   35
         Top             =   1200
         Width           =   1575
      End
   End
End
Attribute VB_Name = "frmParamEntete"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public aTb As clsTableau
Dim IsLoading As Boolean '-< Indique que l'on est en cours de chargement

Public Sub Init()

    '---> Cette proc�dure initialise l'interface du param�trage tableau
    
    Dim i As Integer
    
    '-> Indiquer que l'on est en cours de chargement
    IsLoading = True
    
    '-> Valeurs du tableau
    Me.optLigCell(aTb.pGridLines).Value = True
    Me.optLigEntete(aTb.pGridLinesFixed).Value = True
    Me.Check1.Value = aTb.pBorderStyle
    Me.optBorder(aTb.pAppearance).Value = True
    Me.List5.Text = aTb.pGridLineWidth
    Me.Picture1.BackColor = aTb.pBackColorFixed
    Me.Picture2.BackColor = aTb.pBackColorBkg
    Me.Picture4.BackColor = aTb.pBackColorSel
    Me.Picture5.BackColor = aTb.pGridColorFixed
    Me.Picture6.BackColor = aTb.pGridColor
    Me.optRedim(aTb.pAllowUserResizing).Value = True
    
    '-> Format des lignes d'entete
    Me.Combo1.Text = aTb.FormatEnteteLig.FontName
    Me.picColorLig.BackColor = aTb.FormatEnteteLig.FontColor
    Me.List3.Text = aTb.FormatEnteteLig.FontSize
    Me.lblFontLigBold = aTb.FormatEnteteLig.FontBold
    Me.lblFontLigItalic = aTb.FormatEnteteLig.FontItalic
    Me.lblFontLigUnder = aTb.FormatEnteteLig.FontUnderline
    Me.List2.ListIndex = aTb.FormatEnteteLig.InterneAlign - 1
    
    '-> Format des colonnes d'entete
    Me.Combo2.Text = aTb.FormatEnteteCol.FontName
    Me.picColorCol.BackColor = aTb.FormatEnteteCol.FontColor
    Me.List4.Text = aTb.FormatEnteteCol.FontSize
    Me.Check4 = aTb.FormatEnteteCol.FontBold
    Me.Check3 = aTb.FormatEnteteCol.FontItalic
    Me.Check2 = aTb.FormatEnteteCol.FontUnderline
    Me.List1.ListIndex = aTb.FormatEnteteCol.InterneAlign - 1
    
    '-> Hauteur et largeur de l'angle
    Me.Text1.Text = aTb.HauteurEnt
    Me.Text2.Text = aTb.LargeurEnt
    
    '-> Libell� de l'angle
    Me.Text3.Text = aTb.LibEnt
    
    '-> Init de l'exemple
    For i = 1 To Me.MSFlexGrid1.Cols - 1
        Me.MSFlexGrid1.TextArray(i) = "Col " & i
    Next
    
    For i = 1 To Me.MSFlexGrid1.Rows - 1
        Me.MSFlexGrid1.TextArray(i * Me.MSFlexGrid1.Cols) = "Ligne " & i
    Next
    
    Me.MSFlexGrid1.RowHeight(0) = Me.ScaleY(10, 6, 1)
    Me.MSFlexGrid1.ColWidth(0) = Me.ScaleX(20, 6, 1)
    
    '-> Appliquer l'exemple
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1
    
    '-> Initialiser les barres de d�filement
    Me.VScroll1.Min = 1
    Me.VScroll1.Max = 500
    Me.VScroll1.Value = aTb.HauteurEnt
    
    Me.VScroll2.Min = 1
    Me.VScroll2.Max = 500
    Me.VScroll2.Value = aTb.LargeurEnt
    
    '-> Indiquer que le chargment est fini
    IsLoading = False
    
    '-> Libell� du tableau
    Me.Text4.Text = aTb.Libel

End Sub

Private Sub Check1_Click()

    '-> Appliquer la modification
    aTb.pBorderStyle = Me.Check1.Value
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub Check2_Click()

    '-> Aplliquer la modif
    aTb.FormatEnteteCol.FontUnderline = Me.Check2.Value
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub Check3_Click()

    '-> Aplliquer la modif
    aTb.FormatEnteteCol.FontItalic = Me.Check3.Value
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub Check4_Click()

    '-> Aplliquer la modif
    aTb.FormatEnteteCol.FontBold = Me.Check4.Value
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub Combo1_Click()

    '-> Aplliquer la modif
    aTb.FormatEnteteLig.FontName = Me.Combo1.Text
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub Combo2_Click()

    '-> Aplliquer la modif
    aTb.FormatEnteteCol.FontName = Me.Combo2.Text
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub


Private Sub Command1_Click()
    
    strRetour = "OK"
        
    '-> Modifier le libell� du tableau
    aTb.Libel = Trim(Me.Text4.Text)
    
    '-> Mettre � jour l'icone dans le browse des tableaux
    MDIMain.TreeNaviga.Nodes("TB_" & UCase$(Trim(aTb.Name))).Text = Me.Text4.Text
            
    '-> D�charger la feuille
    Unload Me

End Sub

Private Sub Command2_Click()
strRetour = ""
Unload Me
End Sub

Private Sub Form_Load()

    Dim i As Integer
    Dim aRect As RECT
    
    
    '-> Hauteur et largeur des cellules
    GetClientRect Me.MSFlexGrid1.hwnd, aRect
    For i = 0 To 2
        Me.MSFlexGrid1.ColWidth(i) = Me.ScaleX(aRect.Right / 3, 3, 1)
    Next
    For i = 0 To 2
        Me.MSFlexGrid1.RowHeight(i) = Me.ScaleY(aRect.Bottom / 3, 3, 1)
    Next
    
    '-> Taille des lignes
    For i = 1 To 10
        Me.List5.AddItem i
    Next
    
    '-> Charger les combo avex la liste des polices
    For i = 0 To Screen.FontCount - 1
        Me.Combo1.AddItem Screen.Fonts(i)
        Me.Combo2.AddItem Screen.Fonts(i)
    Next
    
    '-> Charger les tailles de police
    For i = 8 To 72 Step 2
        Me.List3.AddItem i
        Me.List4.AddItem i
    Next
    
    '-> Charger avec les types d'alignements
    For i = 0 To 8
        Me.List1.AddItem CellAlign(i)
        Me.List2.AddItem CellAlign(i)
    Next

End Sub


Private Sub lblFontLigBold_Click()

    '-> Appliquer les modifs
    aTb.FormatEnteteLig.FontBold = Me.lblFontLigBold.Value
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub lblFontLigItalic_Click()

    '-> Appliquer les modifs
    aTb.FormatEnteteLig.FontItalic = Me.lblFontLigItalic.Value
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub lblFontLigUnder_Click()

    '-> Appliquer les modifs
    aTb.FormatEnteteLig.FontUnderline = Me.lblFontLigUnder.Value
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub List1_Click()

    '-> Appliquer les modifs
    aTb.FormatEnteteCol.InterneAlign = Me.List1.ListIndex + 1
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub List2_Click()

    '-> Appliquer les modifs
    aTb.FormatEnteteLig.InterneAlign = Me.List2.ListIndex + 1
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub List3_Click()

    '-> Appliquer les modifs
    aTb.FormatEnteteLig.FontSize = CInt(Me.List3.Text)
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub List4_Click()

    '-> Appliquer les modifs
    aTb.FormatEnteteCol.FontSize = CInt(Me.List4.Text)
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub List5_Click()

    '---> Appliquer la modification
    aTb.pGridLineWidth = Me.List5.Text
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub optBorder_Click(Index As Integer)

    '-> Appliquer la modification
    aTb.pAppearance = Index
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1


End Sub

Private Sub optLigCell_Click(Index As Integer)

    aTb.pGridLines = Index
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub optLigEntete_Click(Index As Integer)

    aTb.pGridLinesFixed = Index
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub optRedim_Click(Index As Integer)

    aTb.pAllowUserResizing = Index
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1

End Sub

Private Sub picColorCol_Click()

    '-> Afficher le choix des couleurs
    Dim aColor As Long
    
    aColor = GetColor(Me.CommonDialog1)
    If aColor = -12345 Then Exit Sub
    
    '-> Appliquer la modification
    aTb.FormatEnteteCol.FontColor = aColor
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1
    Me.picColorCol.BackColor = aColor

End Sub

Private Sub picColorLig_Click()

    '-> Afficher le choix des couleurs
    Dim aColor As Long
    
    aColor = GetColor(Me.CommonDialog1)
    If aColor = -12345 Then Exit Sub
    
    '-> Appliquer la modification
    aTb.FormatEnteteLig.FontColor = aColor
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1
    Me.picColorLig.BackColor = aColor

End Sub

Private Sub Picture1_DblClick()

    Dim aColor As Long
    
    aColor = GetColor(Me.CommonDialog1)
    If aColor = -12345 Then Exit Sub
    
    '-> Appliquer la modification
    aTb.pBackColorFixed = aColor
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1
    Me.Picture1.BackColor = aColor

End Sub

Private Sub Picture2_Click()

    Dim aColor As Long
    
    aColor = GetColor(Me.CommonDialog1)
    If aColor = -12345 Then Exit Sub
    
    '-> Appliquer la modification
    aTb.pBackColorBkg = aColor
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1
    Me.Picture2.BackColor = aColor

End Sub


Private Sub Picture4_Click()

    Dim aColor As Long
    
    aColor = GetColor(Me.CommonDialog1)
    If aColor = -12345 Then Exit Sub
    
    '-> Appliquer la modification
    aTb.pBackColorSel = aColor
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1
    Me.Picture4.BackColor = aColor

End Sub

Private Sub Picture5_Click()

    '-> Afficher le choix des couleurs
    Dim aColor As Long
    
    aColor = GetColor(Me.CommonDialog1)
    If aColor = -12345 Then Exit Sub
    
    '-> Appliquer la modification
    aTb.pGridColorFixed = aColor
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1
    Me.Picture5.BackColor = aColor

End Sub

Private Sub Picture6_Click()

    '-> Afficher le choix des couleurs
    Dim aColor As Long
    
    aColor = GetColor(Me.CommonDialog1)
    If aColor = -12345 Then Exit Sub
    
    '-> Appliquer la modification
    aTb.pGridColor = aColor
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1
    Me.Picture6.BackColor = aColor

End Sub


Private Sub Text3_Change()
    aTb.LibEnt = Me.Text3.Text
End Sub

Private Sub Text3_GotFocus()
    SelectTxtBox Me.Text3
End Sub

Private Sub Text3_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 45, 61, 124, 167
            KeyAscii = 0
    End Select
End Sub

Private Sub Text4_Change()
    SelectTxtBox Me.Text4
End Sub

Private Sub Text4_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 45, 61, 124, 167
            KeyAscii = 0
    End Select
End Sub

Private Sub VScroll1_Change()

    If IsLoading Then Exit Sub
    Me.Text1.Text = Me.VScroll1.Value
    aTb.HauteurEnt = Me.VScroll1.Value

End Sub

Private Sub VScroll2_Change()

    If IsLoading Then Exit Sub
    Me.Text2.Text = Me.VScroll2.Value
    aTb.LargeurEnt = Me.VScroll2.Value

End Sub
