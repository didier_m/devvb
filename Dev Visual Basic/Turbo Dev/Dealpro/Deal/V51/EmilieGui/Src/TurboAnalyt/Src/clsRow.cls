VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsRow"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'-> D�finition d'une classe ligne dans un tableau de bord
Public Code As String '-> Code de la ligne
Public Libelle As String '-> Libelle de la ligne
Public TypeLig As Integer 'Type de ligne ( 0 -> D�tail , 1 -> Total, 2 -> Blanc )
Public Hauteur As Single 'Hauteur de la ligne
Public HauteurTwips As Long  'Hauteur de la ligne en twips
Public Calcul As String '-> Calcul a ex�cuter
Public CalculRtf As String '-> Calcul au format RTF
Public Visible As Boolean '-> Indique si on affiche la ligne dans le tableau
Public Cellules As Collection '-> Collection des cellules de la ligne
Public Position As Integer '-> Indique l'emplacement de la ligne dans le browse
Public Conditions As Collection  '-> Collection des conditions

Private Sub Class_Initialize()

    '-> Initialiser la collections des cellules de cette ligne
    Set Cellules = New Collection
    
    '-> Initialiser la collection des conditions
    Set Conditions = New Collection
    
    '-> Hauteur de 5 mm au d�but
    Hauteur = 5
    

End Sub


Public Function IsCondition(ByVal CodeColonne As String) As Boolean

    '---> Cette fonction indique si une colonne existe d�ja en tant que condition dans cette ligne
    
    
    Dim CdtCode As String
    Dim CdtProgi As String
    Dim aCdt As clsCondition
    
    '-> Eclater la variable en code/progiciel
    CdtCode = Entry(3, CodeColonne, Chr(0))
    CdtProgi = Entry(2, CodeColonne, Chr(0))
    
    '-> Analyse des conditions pour le progiciel sp�cifi�
    For Each aCdt In Conditions
        '-> V�rifier si le code est ok
        If aCdt.Colonne = CdtCode Then
            If aCdt.Progiciel = CdtProgi Then
                '-> Renvoyer une valeur de succ�s
                IsCondition = True
                Exit Function
            End If 'Si on est sur le m�me progiciel
        End If 'Si on est sur le m�me code
    Next

End Function

Public Sub AddCdt(ColonneCode As String, Progiciel As String)

    '---> Cette proc�dure ajoute une condition dans la ligne
    
    Dim aCdt As New clsCondition
    
    '-> Init du code de la colonne
    aCdt.Colonne = UCase$(Trim(ColonneCode))
    aCdt.Progiciel = Progiciel
    
    '-> AJout dans la collection
    Conditions.Add aCdt, "CDT_" & UCase$(Trim(ColonneCode)) & "_" & Progiciel

End Sub


