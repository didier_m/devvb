VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSelectionCell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Cette classe est associ�e � un FlexGrid et indique _
les objets s�lectionn�s dans le flexGrid

Public ptStartX As Integer
Public ptStartY As Integer
Public ptEndX As Integer
Public ptEndY As Integer
Public Hwnd As Long

Public Sub DrawSelection()

'---> Cette proc�dure dessine l'objet s�lectionn� dans le browse

Dim hdlBordure As Long
Dim aPoint As POINTAPI
Dim Res As Long
Dim Old As Long
Dim hdc As Long


Exit Sub
'-> Recup�rer le contexte de p�riph�rique associ� au flex grid
hdc = GetDC(Hwnd)
If hdc = 0 Then Exit Sub

'-> Cr�er le stylo de dessin
hdlBordure = CreatePen(PS_SOLID, 1, RGB(255, 0, 0))
'-> S�lectionner cet objet dans le contexte de p�riph�rique
Old = SelectObject(hdc, hdlBordure)
'-> Dessin de la bordure haut
Res = MoveToEx(hdc, Me.ptStartX, Me.ptStartY, aPoint)
LineTo hdc, Me.ptEndX, Me.ptStartY

'-> Dessin de la bordure gauche
Res = MoveToEx(hdc, Me.ptStartX, Me.ptStartY, aPoint)
LineTo hdc, Me.ptStartX, Me.ptEndY

'-> Dessin de la bordure bas
Res = MoveToEx(hdc, Me.ptStartX, Me.ptEndY, aPoint)
LineTo hdc, Me.ptEndX, Me.ptEndY

'-> Dessin de la bordure droite
Res = MoveToEx(hdc, Me.ptEndX, Me.ptStartY, aPoint)
LineTo hdc, Me.ptEndX, Me.ptEndY



'-> S�lectionner l'ancien pinceau
SelectObject hdc, Old
'-> Supprimer l'objet GDI bordure
DeleteObject hdlBordure
'-> Relacher le contexte de p�riph�rique
ReleaseDC Hwnd, hdc



End Sub
