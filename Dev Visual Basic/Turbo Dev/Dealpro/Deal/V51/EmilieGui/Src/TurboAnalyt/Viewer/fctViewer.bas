Attribute VB_Name = "fctVieWer"
'**********************************
'* Module de gestion des tableaux *
'**********************************

Option Explicit

'---> Variable d'�change
Public strRetour As String

'-> API pour gestion des SPLITS
Public Type POINTAPI
    X As Long
    y As Long
End Type

'-> Indique si la fen�tre de structure est affich�e
Public IsNaviga As Boolean

'-> Collection des tableaux ouverts dans l'�diteur
Public Tableaux As Collection
'-> Collection des cumuls sur axes d'analyse
Public AXES As Collection
'-> Collection des cumuls sur axes detail
Public DETAILS As Collection

'-> definit le tableau en cours
Public TabEnCours As String
'-> Fichier tableau en cours
Public FicEnCours As String

'-> Permet de retrouver la colonne sur laquelle on clique pour le detail
Public aColRefDet As clsCol
Public aLigRefDet As clsRow

'-> Pour d�tection des Click sur tableau
Public ClickX As Integer
Public ClickY As Integer

'-> Node qui sert de reference pour inserer dans le treeview au bon niveau
Public aNodePrev As Node

'-> Permet de determiner si un tableau donn� est deploy� ou pas
Public Zoom As Boolean

'-> Pour redim auto des listview
Public Const LVM_SETCOLUMNWIDTH = 4126 '( LVM_FIRST + 30 )

Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function ScreenToClient Lib "user32" (ByVal hwnd As Long, lpPoint As POINTAPI) As Long
Public Declare Function ClientToScreen Lib "user32" (ByVal hwnd As Long, lpPoint As POINTAPI) As Long

Const ConstCell = "ALIGN;CONTENU;LARGEUR;VRAI,BORDUREBAS,VRAI,VRAI;FONT;FONTSIZE;BOLD;ITALIC;UNDERLINE;FORECOLOR;BACKCOLOR;FAUX;0@0"


Sub Main()
    
    '---> Point d'entree du programme
    '---> verifie si les fichiers sont passes en parametres ou si on doit les saisir
    '---> Lance la creation du tableau et des cumuls
    '---> Initialise les collections ( tableaux et cumuls )
    
    Dim aTb As clsTableau
    Dim NbLignes As Integer
    Dim Ligne As String
    Dim aProgiciel As clsProgiciel
    Dim FicTab As String
    Dim ListeFic As String
    Dim i As Integer
    Dim aLb As Libelle
    Dim Index As Integer
    
    '-> Initialisation des param�tres de r�pertoire
    Call Initialisation
    
    '-> Initialiser la collection des tableaux
    Set Tableaux = New Collection
    '-> Initialisation des collections de cumuls
    Set AXES = New Collection
    Set DETAILS = New Collection

    '-> Initialisation des parametres regionnaux ( separateurs decimales et milliers etc.... )
    InitConvertion
        
    RessourcePath = GetPath(App.Path)
    
    '-> Chargement des messages multilangue de turboanalyt
    If Not CreateMessProg("TurboAnalytViewer-0" & Index + 1 & ".lng") Then GoTo GestError
    
    '-> Chargement des messages
    Set aLb = Libelles("FCTVIEWER")
        
    '-> teste si ligne de commande contenant le fichiers  ou si mode ouverture manuelle de fichiers
    If Command$ <> "" Then
    
        '-> On recupere le fichier tableau de la ligne de commande
        FicTab = Trim(Entry(1, Command$, "|"))
        
        '-> On recupere la liste de fichiers de donn�es
        ListeFic = Mid$(Command$, InStr(Command$, "|") + 1, Len(Command$))
        
        '-> On teste que le fichier tableau entr� dans la ligne de commande existe
        If Dir$(Entry(1, Command$, "|")) = "" Then 'MESSPROG
            MsgBox aLb.GetCaption(1) & " " & Entry(1, Command$, "|") & " " & aLb.GetCaption(2)
            End
        End If
        
        '-> On teste que les sources donn�es existent
        For i = 1 To NumEntries(ListeFic, "|")
            If Dir$(Entry(1, Entry(i, ListeFic, "|"), "�")) = "" Then  'MESSPROG
                MsgBox aLb.GetCaption(3) & Entry(2, Entry(i, ListeFic, "|"), "�") & " " & aLb.GetCaption(2)
                End
            End If
        Next
        
        '-> On affecte le tableau en cours
        FicEnCours = FicTab
        
        '-> On lance l'analyse des fichiers de donn�es selectionn�s par l'utilisateur
        AnalyseSourcesDonnees ListeFic, FicTab
        mdiMain.Show
    Else
        '-> On ouvre la fenetre principale
       mdiMain.Show
    End If
    
    Set aLb = Nothing
    
    Exit Sub

GestError:
    MsgBox "Erreur dans le chargement des libelles multi-langues...", vbExclamation
    End

End Sub

Public Function GetCumul(CleRef As String, ByRef aCol As Collection) As clsCumul
    '---> Permet de recuperer un cumul en fonction de sa clef et quelque soit son sous-niveau
    Dim i As Integer
    Dim aColCumul  As Collection
    
    On Error GoTo NotCumul
    
    Set aColCumul = aCol
    
    '-> On parcours la collection pour voir si le cumul existe
    For i = 1 To NumEntries(CleRef, "|")
        Set GetCumul = aColCumul(Trim(Entry(i, CleRef, "|")))
        Set aColCumul = GetCumul.Cumuls
    Next

    Exit Function

NotCumul:
    
End Function

Public Function IsCumul(ByRef aCol As Collection, aClef As String) As Boolean
    '---> Permet de determiner si un element est membre d'une collection de cumuls
    Dim aCumul As clsCumul
    
    On Error GoTo NotCumul
    
    '-> On cherche a pointer sur le cumul de la collection, de cumul en cours de trt
    Set aCumul = aCol(aClef)
    '-> Si on a pu pointer alors OK
    IsCumul = True
    Exit Function
    
NotCumul:
    '-> Si on a pas pu pointer alors on retourne false
    IsCumul = False
    
End Function

Public Function AnalyseFichierAXE(AxeAnalyse As Collection, ByRef aTb As clsTableau, Niveau As Integer, Ligne As String, ByRef ColEnCours As Collection, ByRef aProgiciel As clsProgiciel, NbLignes As Integer, NodeOk As Boolean, FicAnalyse As String, Optional ClefAxe As String) As clsCumul
    '---> Permet d'analyser de maniere recursive un niveau d'axe donn� en fonction d'un fichier ascii de donn�es
    '---> Et cree les nodes representant l'axe d'analyse dans le treeview
    '---> AxeAnalyse : collection des champs de l'axe a analyser ( detail ou axe analytique )
    '---> aTb : la classe tableau
    '---> Niveau : le niveau de l'axe en cours d'analyse
    '---> Ligne : la ligne de fichier ascii a analyser
    '---> ColEnCours : la collection dans laquelle on va inserer le cumul ( recursif donc taper dans la bonne sous-collection )
    '---> aProgiciel : progiciel du champ de l'axe que l'on analyse
    '---> NbLigne : No de la ligne dans le fichier
    '---> Un booleen qui precise si on doit creer les nodes ou pas ( on les cree que sur l'axe d'analyse )
    '---> Une valeur optionnelle qui permet de mettre dans un cumul Detail la clef d'acces au cumul axe
    
    Dim i As Integer
    Dim j As Integer
    Dim aDes As clsDescriptif
    Dim aField As clsDescriptif
    Dim aCumul As clsCumul
    Dim aValeur As clsValeur
    Dim aLigne As clsRow
    Dim aCol As clsCol
    Dim aNode As Node
    Dim strTemp As String
    
    '-> Analyse du niveau en cours
    
    '-> Recup du champ pour le niveau d'analyse en cours soit dans l'axe d'analyse soit dans l'axe detail selon le niveau
    Set aDes = AxeAnalyse("NV_" & Niveau)
    
    '-> Recup du descro du champ de l'axe d'analyse en cours de trt
    Set aField = aProgiciel.Descriptifs("FIELD_" & aDes.Field)
            
    '-> teste si le cumul existe deja pour le mettre a jour
    If IsCumul(ColEnCours, "CUM_" & Trim(Entry(aField.Entree, Ligne, "|"))) Then
        '-> On pointe sur le cumul deja existant
        Set aCumul = ColEnCours("CUM_" & Trim(Entry(aField.Entree, Ligne, "|")))
        '-> On met les valeurs du fichier dans le cumul  correspondant
        For Each aLigne In aTb.Lignes
            For Each aCol In aTb.Colonnes
                '-> Teste si colonne appartenant au progiciel en cours
                If aCol.Progiciel <> aProgiciel.Code Then GoTo NextCol1
                '-> teste si colonne de detail
                If aCol.TypeCol = 0 Then
                    '-> On recupere l'entree du champ de la colonne dans le fichier
                    Set aField = aProgiciel.Descriptifs("FIELD_" & aCol.Field)
                    '-> tester si la zone est num�rique
                    strTemp = Convert(Entry(aField.Entree, Ligne, "|"))
                    '-> On recupere la valeur a mettre a jour
                    If IsNumeric(strTemp) Then
                        aCumul.Valeurs(aLigne.Code & "|" & aCol.Code).Value = aCumul.Valeurs(aLigne.Code & "|" & aCol.Code).Value + CDbl(strTemp)
                        '-> On precise la ligne de fichier ascii correspondante pour cette valeur
                        aCumul.Valeurs(aLigne.Code & "|" & aCol.Code).colLigDetails.Add NbLignes & "�" & FicAnalyse
                    End If
                End If
NextCol1:
            Next '-> On passe a la colonne suivante
        Next '-> On passe a la ligne suivante
    
        '-> On se repositionne sur le bon etage de la hierarchie
        If NodeOk Then
            If aNodePrev Is Nothing Then
                Set aNodePrev = mdiMain.TreeNaviga.Nodes(aCumul.Key)
            Else
                Set aNodePrev = mdiMain.TreeNaviga.Nodes(aNodePrev.Key & "|" & aCumul.Key)
            End If
        End If
    Else
        '-> On initialise le cumul et sa clef
        Set aCumul = New clsCumul
        aCumul.Key = "CUM_" & Trim(Entry(aField.Entree, Ligne, "|"))
        '-> Si cumulde detail alors on renseigne la clef d'acces au cumul axe
        If ClefAxe <> "" Then aCumul.ClefAxe = ClefAxe
        '-> On cree le nombre de valeurs correspondant au nombre de cases du tableau
        For Each aLigne In aTb.Lignes
            For Each aCol In aTb.Colonnes
                '-> On definit une nouvelle valeur
                Set aValeur = New clsValeur
                '-> On met a jour la collection de details de la classe valeur
                aCumul.Valeurs.Add aValeur, aLigne.Code & "|" & aCol.Code
                '-> On affecte le progiciel correspondant a la valeur
                aValeur.Progiciel = aCol.Progiciel
                '-> On affecte sa clef en fontion de sa position
                aValeur.Key = aLigne.Code & "|" & aCol.Code
                '-> teste si colonne de detail
                If aCol.TypeCol = 0 Then
                    '-> Teste si colonne appartenant au progiciel en cours de maniere a ne pas rensigner la valeur
                    If aCol.Progiciel <> aProgiciel.Code Then
                        aValeur.Value = 0
                        GoTo NextCol2
                    End If
                    '-> On recupere le champ de la colonne dans le fichier
                    Set aField = aProgiciel.Descriptifs("FIELD_" & aCol.Field)
                    '-> tester si la zone est num�rique
                    strTemp = Convert(Entry(aField.Entree, Ligne, "|"))
                    '-> Mise a jour des valeurs si type de donn�es correct
                    If IsNumeric(Trim(strTemp)) Then
                        aValeur.Value = CDbl(strTemp)
                        '-> On precise la ligne de fichier ascii correspondante pour cette valeur
                        aValeur.colLigDetails.Add NbLignes & "�" & FicAnalyse
                    End If
                Else
                    If aCol.TypeCol = 1 Then
                        '-> cas de la colonne de calcul : on stocke dans la valeur le calcul voulu pour cette colonne
                        aValeur.Calcul = True
                    Else
                        '-> cas de la colonne de separation
                        
                    End If
                    
                End If
NextCol2:
            Next '-> On passe a la colonne suivante
        Next '-> On passe a la ligne suivante
        
        '-> On ajoute le cumul que l'on vient de creer dans la collection de niveau superieur
        ColEnCours.Add aCumul, aCumul.Key
        
        '-> On ajoute un node pour le cumul que l'on vient de creer
        If NodeOk Then
            If aNodePrev Is Nothing Then
                Set aNode = mdiMain.TreeNaviga.Nodes.Add(, , aCumul.Key, aCumul.Key, 1)
            Else
                Set aNode = mdiMain.TreeNaviga.Nodes.Add(aNodePrev.Key, 4, aNodePrev.Key & "|" & aCumul.Key, Entry(Niveau, aCumul.Key, "|"), 1)
            End If
            '-> On garde en memoire la reference au node precedent pour inserer dans la hierarchie
            Set aNodePrev = aNode
            '-> On precise l'image a afficher si on double clique sur le node
            aNode.ExpandedImage = 6
            '-> On reference le node comme representation d'un cumul AXE
            aNode.Tag = "AXE"
        End If
        
    End If
    
    '-> teste si il reste un niveau d'axe a analyser
    If Niveau + 1 > AxeAnalyse.Count Then
        Exit Function
    Else
        '***** RECURSIVITE ***********
        '-> On relance la fonction sur le niveau inferieur avec la collection de cumuls que l'on vient de mettre a jour
        AnalyseFichierAXE AxeAnalyse, aTb, Niveau + 1, Ligne, aCumul.Cumuls, aProgiciel, NbLignes, NodeOk, FicAnalyse, ClefAxe
    End If
    
    '-> On retourne le cumul de dernier niveau trait�
    Set AnalyseFichierAXE = aCumul
    
End Function
    
Public Function EvalueCondition(aTb As clsTableau, Ligne As String, aProgRef As clsProgiciel) As Boolean
    '---> Permet d'evaluer une condition et ses differentes valeurs en fonction d'une ligne de fichier ASCII donn�e
    Dim i As Integer
    Dim j As Integer
    Dim hdlFile As Integer
    Dim aCol As clsCol
    Dim aCdt As clsCondition
    Dim aValCdt As clsValueCdt
    Dim aLigne As clsRow
    Dim aField As clsDescriptif
    Dim Value As Variant
    Dim ValueRef As Variant
    Dim ValueRefMin As Variant
    Dim ValueRefMax As Variant
    Dim aValueCdt As clsValueCdt
    Dim ConditionOk As Boolean
    Dim ValueConditionOk As Boolean
    
    '-> On considere au debut que toutes les conditions sont bonnes puis on teste une par une
    EvalueCondition = True
    
    '-> On parcours chaque ligne du tableau
    For Each aLigne In aTb.Lignes
        
        '-> teste si ligne de detail car pas de condition sur les autres types de lignes
        If aLigne.TypeLig <> 0 Then GoTo NextLig
        
        '-> On recupere les conditions pour chaque ligne du tableau
        For i = 1 To aLigne.Conditions.Count
            
            '-> recup de la condition
            Set aCdt = aLigne.Conditions(i)
            
            '-> Teste si la condition porte sur le progiciel en cours de trt
            If aCdt.Progiciel <> aProgRef.Code Then GoTo NextCondition
            
            '-> Recup de la valeur du champ a tester ( correspondant a la condition ) dans le fichier ascii
            Value = GetConditionEntry(aTb, aLigne, aCdt, Ligne)
            
            '-> On part du pricipe que la condition est validee et puis on teste
            ConditionOk = True
            
            '-> On parcours les differents valeurs de conditions et on les teste
            For j = 1 To aCdt.ValueCdts.Count
            
                '-> Recup de la valeur de condition
                Set aValueCdt = aCdt.ValueCdts(j)
                '-> On fait les conversions necessaires aux bonnes comparaisons
                Value = GetConvertedValue(Value)
                ValueRefMin = GetConvertedValue(aValueCdt.ValMini)
                ValueRefMax = GetConvertedValue(aValueCdt.ValMaxi)
                
                '-> fait la comparaison entre la valeur du fichier ascii et les valeurs de conditions une fois bien formattees
                ValueConditionOk = CompareConditionValues(Value, ValueRefMin, ValueRefMax, aValueCdt.Operation)
                
                '-> Evalue les valeurs de conditions entre elles selon qu'elles sont marqu�es par un 'ET' ou par un 'OU'
                Select Case aValueCdt.Operande
                    Case 0 ' ET
                        '-> Si les valeurs de la condition sont rassembl�es avec un ET et que l'une d'entre elles est fausse alors on sort dessuite
                        If ValueConditionOk = False Then
                            ConditionOk = False
                            '-> On teste si on a evalue au moins 2 cdt pour sortir ( en effet : faux 'ET' XXXX fera toujours faux )
                            If j > 1 Then Exit For
                        End If
                    Case 1 ' OU
                        '-> Si les valeurs de la condition sont rassembl�es avec un OU alors on concatene les resultats des tests
                        ConditionOk = ConditionOk + ValueConditionOk
                End Select '-> Selon si les valeurs de condition sont imbriqu�es avec ET ou OU
                
            Next 'Pour chaque valeur de condition
            
            '-> Meme evaluation des conditions entre elles selon qu'elles sont imbriqu�es par 'ET' ou par 'OU'
            Select Case aCdt.TypeCdt
                Case 0 ' ET
                    '-> Si les conditions sont concatenees avec des ET et qu'une d'entre elles est fausse alors on sort � faux
                    If ConditionOk = False Then
                        EvalueCondition = False
                        '-> On teste si on a evalue au moins 2 cdt pour sortir ( en effet : faux 'ET' XXXX fera toujours faux )
                        If i > 1 Then Exit Function
                    End If
                Case 1 ' OU
                    '-> Si les conditions sont concaten�es avec un OU alors on concatene les resultats des tests
                    EvalueCondition = EvalueCondition + ConditionOk
            End Select '-> Selon si les conditions sont imbriqu�es avec ET ou OU
            
NextCondition:
            
        Next '-> Pour chaque condition
            

NextLig: ' Permet de passer a la ligne suivante si test invalid�

    Next 'Pour chaque ligne
    
End Function

Private Function GetConditionEntry(ByRef aTb As clsTableau, ByRef aLigne As clsRow, ByRef aCdt As clsCondition, Ligne As String) As Variant
    '---> Permet de retourner la valeur d'une condition depuis le fichier ascii de donn�es
    Dim aProgiciel As clsProgiciel
    Dim aField As clsDescriptif
    
    '-> Recup du progiciel associe a la condition
    Set aProgiciel = aTb.Progiciels("PRO_" & aCdt.Progiciel)
    '-> Recup du champ sur lequel porte la condition
    Set aField = aProgiciel.Descriptifs("FIELD_" & aCdt.Colonne)
    '-> On recupere la valeur du champ a tester
    GetConditionEntry = Trim(Entry(aField.Entree, Ligne, "|"))
    
End Function

Private Function GetConvertedValue(Value As Variant) As Variant
    '---> Permet d'evaluer le type de donn�es a tester et le convertit au bon format
    
    '-> remet le bon format de chiffres si valeur numerique et transforme en
    '-> type de donn�es Double pour comparaisons numeriques
    If IsNumeric(Value) Then
        '-> On commence par convertir la valeur en type numerique
        If Value <> "" Then
            Value = CDbl(Convert(Value))
        Else
            Value = 0
        End If
    End If
    
    '-> On retourne la valeur
    GetConvertedValue = Value
    
End Function

Private Function CompareConditionValues(ByRef Value As Variant, ByRef ValueRefMin As Variant, ByRef ValueRefMax As Variant, ByRef Operation As Integer) As Boolean
    '---> permet de faire les comparaison entre la valeur du fichier ascii et
    '---> les differentes valeurs de la condition en cour d'evaluation
    '---> En fonction des operandes specifie dans la condition
    Dim ValueConditionOk As Boolean
    
    '-> On teste la valeur selon son operande
    ValueConditionOk = True
    Select Case Operation
        Case 1 '-> egal
            If Value <> ValueRefMin Then ValueConditionOk = False
        Case 2 '-> different
            If Value = ValueRefMin Then ValueConditionOk = False
        Case 3 '-> inferieur
            If Value >= ValueRefMin Then ValueConditionOk = False
        Case 4 '-> superieur
            If Value <= ValueRefMin Then ValueConditionOk = False
        Case 5 '-> compris entre
            If Value <= ValueRefMin Then ValueConditionOk = False
            If Value >= ValueRefMax Then ValueConditionOk = False
        Case 6 '-> contenant
            If InStr(Value, ValueRefMin) = 0 Then ValueConditionOk = False
        Case 7 '-> ne contenant pas
            If InStr(Value, ValueRefMin) <> 0 Then ValueConditionOk = False
        Case 8 '-> commencant par
            If InStr(1, Value, ValueRefMin) = 0 Then ValueConditionOk = False
    End Select 'Selon la nature de la condition
    
    '-> On retourne la valeur
    CompareConditionValues = ValueConditionOk
    
End Function

Public Sub ExporteExcel(ByRef TabEnCours As String, ByRef ClefTreeView As String)
    '---> permet d'exporter un tableau dans excel
    Dim wrkExcel As Excel.Application
    Dim wrkClasseur As Excel.Workbook
    Dim wrkFeuille As Excel.Worksheet
    Dim aRange As Range
    Dim aTb As clsTableau
    Dim aRangeL As Range
    Dim aRangeC As Range
    Dim aCol As clsCol
    Dim aRow As clsRow
    Dim IndLig As Integer
    Dim IndCol As Integer
    Dim hdlFile As Integer
    Dim Res As Long
    Dim lpSection As String
    Dim aCell As clsCell
    Dim aCumul As clsCumul
    Dim RefLig As Integer
    Dim RefCol As Integer
    Dim ClasseurExistant As Boolean
    
    On Error GoTo GestError
    
    RefLig = 0
    RefCol = 0
    
    '-> recup du tableau en cours
    Set aTb = Tableaux("TB_" & TabEnCours)
    
    '-> Initialiser l'application
    Set wrkExcel = New Excel.Application
    '-> On desactive les messages de XL
    wrkExcel.DisplayAlerts = False
    
    '-> On affecte le nombre de feuille initial a la creation du classeur
    wrkExcel.SheetsInNewWorkbook = 1
    
    '-> On selectionne le cumul qui nous servira a remplir les valeurs des cellules
    Set aCumul = GetCumul(ClefTreeView, AXES)
    
    '-> On saisit le parametrage de l'export
    frmParamXL.Show vbModal
    
    '-> Si on a interrompu la saisie des params alors on sort
    If strRetour = "N" Then Exit Sub
    
    '-> On prepare un classeur selon les parametres choisis
    ClasseurExistant = False
    If Entry(1, strRetour, "|") = "" Then
        '-> On declare un nouveau classeur dans excel
        Set wrkClasseur = wrkExcel.Workbooks.Add
        '-> On cree une nouvelle feuille
        Set wrkFeuille = wrkClasseur.Sheets(wrkClasseur.Sheets.Count)
        '-> On precise le nom de la feuille si le choix en a ete fait dans le parametrage
        If Entry(4, strRetour, "|") <> "" Then wrkFeuille.Name = Trim(Entry(4, strRetour, "|"))
    Else
        '-> On ouvre le classeur choisi
        Set wrkClasseur = wrkExcel.Workbooks.Open(Entry(1, strRetour, "|"))
        
        '-> On se positionne sur la cellule de reference
        If Entry(4, strRetour, "|") <> "" Then
            '-> On cree une nouvelle feuille dans le classeur
            Set wrkFeuille = wrkClasseur.Sheets.Add
            wrkFeuille.Move , wrkClasseur.Sheets(wrkClasseur.Sheets.Count)
            wrkFeuille.Name = Trim(Entry(4, strRetour, "|"))
        Else
            '-> On ouvre une existante
            Set wrkFeuille = wrkClasseur.Sheets(Entry(2, strRetour, "|"))
        End If
        
        '-----> se placer sur une cellule en particulier
        If Entry(3, strRetour, "|") <> "" Then
            Set aRange = wrkFeuille.Range(Entry(3, strRetour, "|"))
            '-> On affecte les valeurs de depart pour inserer dans les ligs et cols
            RefLig = aRange.Row
            RefCol = aRange.Column - 1
        End If
        
    End If
    
    '-> On recupere les proprietes de l'entete de colonne dans le fichier tableau '.DATA'
    hdlFile = FreeFile
    Open aTb.FileName For Input As #hdlFile
    '-> On recupere la rubrique voulue dans le fichier
    lpSection = Space$(30000)
    Res = GetPrivateProfileString("PARAM_ENTETE", "FORMATENTETECOL", "", lpSection, Len(lpSection), aTb.FileName)
    If Res <> 0 Then
        '-> On cree les entetes de colonnes
        For IndCol = 1 To aTb.Colonnes.Count
            '-> Recup de la colonne
            Set aCol = aTb.Colonnes(IndCol)
            '- On cree l'entete de colonne
            Set aRangeC = wrkFeuille.Cells(1 + RefLig, IndCol + 1 + RefCol)
            '-> Renseigne les proprietes de police de la cellule
            With aRangeC.Font
                .Bold = CBool(Entry(2, Entry(3, lpSection, "|"), "�"))
                .Italic = CBool(Entry(2, Entry(4, lpSection, "|"), "�"))
                .Size = Entry(2, Entry(2, lpSection, "|"), "�")
                .Underline = CBool(Entry(2, Entry(5, lpSection, "|"), "�"))
                .Name = Entry(2, Entry(1, lpSection, "|"), "�")
                .Color = Entry(2, Entry(6, lpSection, "|"), "�")
            End With
            '-> Permet la conversion des types d'alignements internes
            ConvertAlignement Entry(2, Entry(16, lpSection, "|"), "�"), aRangeC
            aRangeC.RowHeight = mdiMain.picNaviga.ScaleY(aTb.HauteurEnt, 6, 2)
            aRangeC.ColumnWidth = mdiMain.picNaviga.ScaleX(aCol.Largeur, 6, 4)
            aRangeC.Interior.Color = Entry(2, Entry(7, lpSection, "|"), "�")
            '-> On place le libelle de la colonne
            aRangeC.Value = aCol.Libelle
            
            '-> Cree les bordures de la cellule
            CreateBorders aRangeC
        Next
    End If
    
    '-> On recupere les proprietes de l'entete de'une ligne dans le fichier tableau '.DATA'
    lpSection = Space$(30000)
    Res = GetPrivateProfileString("PARAM_ENTETE", "FORMATENTETELIG", "", lpSection, Len(lpSection), aTb.FileName)
    If Res <> 0 Then
        '-> On cree les entetes de lignes
        For IndLig = 1 To aTb.Lignes.Count
            '-> On recupere la ligne de tableau
            Set aRow = aTb.Lignes(IndLig)
            '-> On cree l'entete de colonne
            Set aRangeL = wrkFeuille.Cells(IndLig + 1 + RefLig, 1 + RefCol)
            '-> Renseigne les proprietes de police de la cellule
            With aRangeL.Font
                .Bold = CBool(Entry(2, Entry(3, lpSection, "|"), "�"))
                .Italic = CBool(Entry(2, Entry(4, lpSection, "|"), "�"))
                .Size = Entry(2, Entry(2, lpSection, "|"), "�")
                .Underline = CBool(Entry(2, Entry(5, lpSection, "|"), "�"))
                .Name = Entry(2, Entry(1, lpSection, "|"), "�")
                .Color = Entry(2, Entry(6, lpSection, "|"), "�")
            End With
            '-> Affectation de l'alignement et des mensurations de la cellule d'entete ligne
            ConvertAlignement Entry(2, Entry(16, lpSection, "|"), "�"), aRangeL
            aRangeL.RowHeight = mdiMain.picNaviga.ScaleY(aRow.Hauteur, 6, 2)
            aRangeL.ColumnWidth = mdiMain.picNaviga.ScaleX(aTb.LargeurEnt, 6, 4)
            aRangeL.Interior.Color = Entry(2, Entry(7, lpSection, "|"), "�")
            '-> On place le libelle de la ligne du tableau
            aRangeL.Value = aRow.Libelle
            
            '-> Cree les bordures de la cellule
            CreateBorders aRangeL
        Next
    End If
    
    '-> Creation des cellules du tableau avec valeurs des cumuls
    For IndLig = 1 To aTb.Lignes.Count

        '-> Recup de la ligne
        Set aRow = aTb.Lignes(IndLig)
        '-> teste si ligne visible sinon passe a la suivante
        If aRow.Visible = False Then GoTo NextLig

        For IndCol = 1 To aTb.Colonnes.Count
            
            '-> Recup de la colonne
            Set aCol = aTb.Colonnes(IndCol)
            
            '-> n'envoie que les colonnes visibles au moment du lancement
            If aCol.Visible = False Then
                If Zoom = False Then GoTo NextCol
            End If

            '-> Teste si ligne visible
            If aCol.Visible = False Then GoTo NextCol

            '-> On renseigne la cellule en cours
            Set aRange = wrkFeuille.Cells(IndLig + 1 + RefLig, IndCol + 1 + RefCol)
            aRange.Value = aCumul.Valeurs(aRow.Code & "|" & aCol.Code).Value
            aRange.RowHeight = mdiMain.picNaviga.ScaleY(aRow.Hauteur, 6, 2)
            aRange.ColumnWidth = mdiMain.picNaviga.ScaleX(aCol.Largeur, 6, 4)
            aRange.HorizontalAlignment = aRangeC.HorizontalAlignment
            aRange.VerticalAlignment = aRangeC.VerticalAlignment
            '-> Cree les bordures de la cellule
            CreateBorders aRange

NextCol:

        Next

NextLig:

    Next
    
    '-> dechargement memoire de la fenetre de parametrage
    Unload frmParamXL
    '-> On reactive les messages de XL
    wrkExcel.DisplayAlerts = True
    '-> On affiche la feuille excel
    wrkExcel.Visible = True
    
    Exit Sub
    
    Set wrkExcel = Nothing
    Set wrkClasseur = Nothing
    Set wrkFeuille = Nothing
    
GestError:

    '-> On libere l'espace memoire
    If Not wrkExcel Is Nothing Then wrkExcel.Quit
    Set wrkExcel = Nothing
    Set wrkClasseur = Nothing
    Set wrkFeuille = Nothing
    
End Sub

Public Sub ConvertAlignement(ByRef strAlign As Integer, aRange As Range)
    '---> Fonction qui passe d'un format d'alignement turbo a un format d'alignement XL
    
    Select Case strAlign
        Case 1
            aRange.VerticalAlignment = -4160
            aRange.HorizontalAlignment = -4131
        Case 2
            aRange.VerticalAlignment = -4160
            aRange.HorizontalAlignment = -4108
        Case 3
            aRange.VerticalAlignment = -4160
            aRange.HorizontalAlignment = -4152
        Case 4
            aRange.VerticalAlignment = -4108
            aRange.HorizontalAlignment = -4131
        Case 5
            aRange.VerticalAlignment = -4108
            aRange.HorizontalAlignment = -4108
        Case 6
            aRange.VerticalAlignment = -4108
            aRange.HorizontalAlignment = -4152
        Case 7
            aRange.VerticalAlignment = -4107
            aRange.HorizontalAlignment = -4131
        Case 8
            aRange.VerticalAlignment = -4107
            aRange.HorizontalAlignment = -4108
        Case 9
            aRange.VerticalAlignment = -4107
            aRange.HorizontalAlignment = -4152
    End Select
    
End Sub

Public Sub CreateBorders(ByRef aRange As Range)
    '---> Permet de creer les bordures d'une cellule
    
    '-> Bordure du bas
    With aRange.Borders(xlEdgeBottom)
        .LineStyle = xlContinuous
        .Weight = xlThick
    End With
    '-> Bordure du haut
    With aRange.Borders(xlEdgeTop)
        .LineStyle = xlContinuous
        .Weight = xlThick
    End With
    '-> Bordure gauche
    With aRange.Borders(xlEdgeLeft)
        .LineStyle = xlContinuous
        .Weight = xlThick
    End With
    '-> Bordure droite
    With aRange.Borders(xlEdgeRight)
        .LineStyle = xlContinuous
        .Weight = xlThick
    End With
    
End Sub

Public Sub DrawTbByObject(aFlex As MSFlexGrid, aTb As clsTableau)

    '---> Cette proc�dure met � jour un flexgrid en fonction de l'objet tableau associ�
    
    Dim aLig As clsRow
    Dim aCol As clsCol
    Dim aCell As clsCell
    Dim i As Integer, j As Integer
    
    
    
    '-> Mettre � jour le nombre de lignes et de colonnes
    aFlex.Rows = aTb.Lignes.Count + 1
    aFlex.Cols = aTb.Colonnes.Count + 1
    
    '-> Cr�er toutes les lignes
    For i = 1 To aTb.Lignes.Count
        '-> Pointer sur la ligne
        Set aLig = aTb.Lignes("LIG_" & Entry(i, aTb.MatriceLig, Chr(0)))
        '-> Mettre � jour le libell�
        aFlex.TextArray((i - 1) * aFlex.Cols) = aLig.Libelle
    Next 'Pour toutes les lignes
    
    '-> Param�trage de l'angle
    aFlex.RowHeight(0) = aTb.HauteurEnt
    aFlex.ColWidth(0) = aTb.LargeurEnt
    aFlex.TextArray(0) = aTb.LibEnt
    
    '-> Param�trer les lignes
    For i = 1 To aTb.Lignes.Count
        '-> Pointer sur la ligne
        Set aLig = aTb.Lignes("LIG_" & Entry(i, aTb.MatriceLig, Chr(0)))
        '-> Indiquer la position de la ligne dans le browse
        aLig.Position = i
        '-> Donner la hauteur de ligne
        aFlex.RowHeight(i) = aLig.HauteurTwips
        '-> Libell� des lignes
        aFlex.TextArray(i * aFlex.Cols) = aLig.Libelle
    Next 'Pour toutes les lignes
    
    '-> Param�trage des colonnes
    For i = 1 To aTb.Colonnes.Count
        '-> Pointer sur la colonne
        Set aCol = aTb.Colonnes("COL_" & Entry(i, aTb.MatriceCol, "|"))
        '-> Indiquer la position de la colonne dans le browse
        aCol.Position = i
        '-> Donner la larguer � la colonne
        If aCol.Visible = True Or Zoom Then
            aFlex.ColWidth(i) = aCol.LargeurTwips
        Else
            aFlex.ColWidth(i) = 0
        End If
        '-> Libell� de la colonne
        aFlex.TextArray(i) = aCol.Libelle
    Next
    
    '-> Formatter les cellules dans le flex grid
    For i = 1 To aFlex.Rows - 1
        '-> Pointer sur la ligne
        Set aLig = aTb.Lignes("LIG_" & Entry(i, aTb.MatriceLig, Chr(0)))
        '-> Donner la larguer � la colonne
        If aLig.Visible = True Or Zoom Then
            aFlex.RowHeight(i) = aLig.HauteurTwips
        Else
            aFlex.RowHeight(i) = 0
        End If
        For j = 1 To aFlex.Cols - 1
            '-> Pointer sur la cellule
            Set aCell = aLig.Cellules("CELL_" & Entry(j, aTb.MatriceCol, "|"))
            '-> Formatter la cellule
            ExecFormatCell aCell.FormatCell, i, j, aFlex
        Next
    Next 'Pour toutes les lignes
    
    '-> Formatter les entetes lignes et colonnes
    ExecTbParamOnFlex aTb, aFlex
    
    '-> D�bloquer le flexgrid
    LockWindowUpdate 0

End Sub

Public Function IsFieldInAxeDetail(Field As String, aTb As clsTableau) As Boolean
    '---> permet de determiner si un champ appartient au descro de l'axe detail dans un progiciel pour un tableau donn�
    Dim aProg As clsProgiciel
    Dim aDes As clsDescriptif
    
    IsFieldInAxeDetail = False
    '-> On parcours les progiciels du tableau pour analyser leurs axes de detail
    For Each aProg In aTb.Progiciels
        If Not aProg.IsAxeAnalyse Then
            For Each aDes In aProg.AxeDetails
                If Trim(aDes.Field) = Trim(Field) Then
                    IsFieldInAxeDetail = True
                    Exit Function
                End If
            Next
        End If
    Next
        
End Function

Public Function PrintaFlex(aFlex As MSFlexGrid, IsTbAnalyt As Boolean, Optional aPic As PictureBox) As String

Dim HauteurBrowse As Double
Dim LargeurBrowse As Double
Dim ParentObj As Object
Dim HauteurPage As Double
Dim HauteurPrint As Double
Dim MargeTop As Integer
Dim IsPaysage As Boolean
Dim SpoolName As String
Dim MaqRef As String
Dim NbCol As Long, i As Long
Dim NbLig As Long, j As Long
Dim hdlFile As Integer
Dim DefCol As String
Dim TempoStr As String
Dim IsEnteteLig As Boolean
Dim IsEntete As Boolean
Dim NbPage As String
Dim DataPage As String
Dim IsSautPage As Boolean
Dim SpoolData As String

'-> Pour ne pas se d�placer sur toutes les lignes d'entete
Dim EnteteFont As String
Dim EnteteFontSize As Integer
Dim EnteteBold As Boolean
Dim EnteteItalic As Boolean
Dim EnteteUnderligne As Boolean

'-> Pour dessin de la temporisation
Dim IsTempo As Boolean

'-> Pour rename du fichier
Dim CorpFile As String

On Error GoTo EndError

'-> Tester si on doit imprimer la temporisation
If Not aPic Is Nothing Then
    IsTempo = True
End If

'-> Bloquer la mise � jour du flex
LockWindowUpdate aFlex.hwnd

'-> R�cup�rer un pointeur sur le parent du flex pour effectuer les convertions
Set ParentObj = aFlex.Parent

'-> R�cup�rer la largeur du browse � imprimer et la convertir en CM
LargeurBrowse = ParentObj.ScaleX(aFlex.Width, ParentObj.ScaleMode, 7)

'->Get ici  de la marge haute pour d�termination de la hauteur d'impression THIERRY
HauteurPrint = 1
NbPage = 1

'-> D�terminer si la page est en mode paysage ou portrait
If LargeurBrowse > 20 Then
    '-> On  est en mode paysage
    IsPaysage = True
    MaqRef = ImpPath & "AnalytTB_IT.Maqgui"
    HauteurPage = 21
Else
    '-> On est en mode portrait
    IsPaysage = False
    MaqRef = ImpPath & "AnalytTB_FR.Maqgui"
    HauteurPage = 29.7
End If

'-> Cr�er un fichier temporaire
SpoolName = GetTempFileNameVB("TB")
FileCopy MaqRef, SpoolName

'-> Ouvrir le fichier en mode append
hdlFile = FreeFile
Open SpoolName For Append As #hdlFile

'-> Impression du tableau
Print #hdlFile, "[TB-TBANALYT]"
Print #hdlFile, "\LARGEURTB�0"
Print #hdlFile, "\ORIENTATIONTB�0"

'-> R�cup�ration du param�trage d'une cellule d'entete
aFlex.Col = 0
aFlex.Row = 0
EnteteFont = aFlex.CellFontName
EnteteFontSize = aFlex.CellFontSize
EnteteBold = aFlex.CellFontBold
EnteteItalic = aFlex.CellFontItalic
EnteteUnderligne = aFlex.CellFontUnderline

'-> Impression d'une ligne de tableau pour chaque ligne du browse
For i = 0 To aFlex.Rows - 1
    '-> Dessin de la temporisation
    If IsTempo Then
        DrawWait aFlex.Rows - 1, i, aPic
    End If
    
    '-> Imprimer l'entete de la ligne
    Print #hdlFile, "\Begin�Block" & i
    '-> Imprimer la largeur du block
    Print #hdlFile, "\Largeur�" & ParentObj.ScaleX(aFlex.Width, ParentObj.ScaleMode, 7)
    '-> Imprimer la hauteur du block
    Print #hdlFile, "\Hauteur�" & ParentObj.ScaleY(aFlex.RowHeight(i), 1, 7)
    '-> Imprimer les diff�rents param de l'entete
    Print #hdlFile, "\AlignTop�1"
    Print #hdlFile, "\Top�0"
    Print #hdlFile, "\AlignLeft�3"
    Print #hdlFile, "\Left�0"
    Print #hdlFile, "\MasterLink�"
    Print #hdlFile, "\SlaveLink�"
    Print #hdlFile, "\RgChar�"
    Print #hdlFile, "\Ligne�1"
    Print #hdlFile, "\Colonne�" & aFlex.Cols
    '-> Imprimer la ligne de chaque colonne
    DefCol = "\Col�" & ParentObj.ScaleY(aFlex.RowHeight(i), 1, 7) & "�"
    '-> Ne se d�placer en ligne que si on n'est pas sur une ligne d'entete
    If i < 1 Then
        IsEnteteLig = True
    Else
        '-> Positionner la ligne
        If IsTbAnalyt Then aFlex.Row = i
        IsEnteteLig = False
    End If
    
    '-> Tester si on doit mettre un block de saut de page ou non
    If HauteurPrint + ParentObj.ScaleY(aFlex.RowHeight(i), 1, 7) > HauteurPage - 1 Then
        HauteurPrint = 1
        IsSautPage = True
    Else
        HauteurPrint = HauteurPrint + ParentObj.ScaleY(aFlex.RowHeight(i), 1, 7)
        IsSautPage = False
    End If
    
    For j = 0 To aFlex.Cols - 1
        '-> Ne se positionner en colonne que si on n'est pas sur un entete
        If j < aFlex.FixedCols Then
            IsEntete = True
        Else
            If i < 1 Then
                IsEntete = True
            Else
                If IsTbAnalyt Then aFlex.Col = j
                IsEntete = False
            End If
        End If
        '-> get de la d�finition d'une colonne
        TempoStr = ConstCell
        '-> Alignement interne
        If Not IsEntete Then
            TempoStr = Replace(TempoStr, "ALIGN", "6")
        Else
            TempoStr = Replace(TempoStr, "ALIGN", "5")
        End If
        '-> Largeur de la cellule
        TempoStr = Replace(TempoStr, "LARGEUR", ParentObj.ScaleX(aFlex.ColWidth(j), 1, 7))
        '-> Contenu de la cellule
        TempoStr = Replace(TempoStr, "CONTENU", aFlex.TextArray(i * aFlex.Cols + j))
        '-> Font
        If IsEntete Then
            TempoStr = Replace(TempoStr, "FONT;", EnteteFont & ";")
        Else
            TempoStr = Replace(TempoStr, "FONT;", aFlex.CellFontName & ";")
        End If
        '-> Taille
        If IsEntete Then
            TempoStr = Replace(TempoStr, "FONTSIZE", EnteteFontSize)
        Else
            TempoStr = Replace(TempoStr, "FONTSIZE", aFlex.CellFontSize)
        End If
        '-> BOLD
        If IsEntete Then
            If EnteteBold Then
                TempoStr = Replace(TempoStr, "BOLD", "VRAI")
            Else
                TempoStr = Replace(TempoStr, "BOLD", "FAUX")
            End If
        Else
            If aFlex.CellFontBold Then
                TempoStr = Replace(TempoStr, "BOLD", "VRAI")
            Else
                TempoStr = Replace(TempoStr, "BOLD", "FAUX")
            End If
        End If
        '-> ITALIC
        If IsEntete Then
            If EnteteItalic Then
                TempoStr = Replace(TempoStr, "ITALIC", "VRAI")
            Else
                TempoStr = Replace(TempoStr, "ITALIC", "FAUX")
            End If
        Else
            If aFlex.CellFontItalic Then
                TempoStr = Replace(TempoStr, "ITALIC", "VRAI")
            Else
                TempoStr = Replace(TempoStr, "ITALIC", "FAUX")
            End If
        End If
        '-> UNDERLINE
        If IsEntete Then
            If EnteteUnderligne Then
                TempoStr = Replace(TempoStr, "UNDERLINE", "VRAI")
            Else
                TempoStr = Replace(TempoStr, "UNDERLINE", "FAUX")
            End If
        Else
            If aFlex.CellFontUnderline Then
                TempoStr = Replace(TempoStr, "UNDERLINE", "VRAI")
            Else
                TempoStr = Replace(TempoStr, "UNDERLINE", "FAUX")
            End If
        End If
        
        '-> FORECOLOR
        If IsEntete Then
            TempoStr = Replace(TempoStr, "FORECOLOR", aFlex.ForeColorFixed)
        Else
            If IsTbAnalyt Then
                TempoStr = Replace(TempoStr, "FORECOLOR", aFlex.ForeColor)
            Else
                TempoStr = Replace(TempoStr, "FORECOLOR", aFlex.CellForeColor)
            End If
        End If
        
        '-> BACKCOLOR
        If IsEntete Then
            TempoStr = Replace(TempoStr, "BACKCOLOR", aFlex.BackColorFixed)
        Else
            If IsTbAnalyt Then
                TempoStr = Replace(TempoStr, "BACKCOLOR", aFlex.CellBackColor)
            Else
                TempoStr = Replace(TempoStr, "BACKCOLOR", aFlex.BackColor)
            End If
        End If
        
        '-> Bordure du bas
        If IsSautPage Or i = aFlex.Rows - 1 Then
            TempoStr = Replace(TempoStr, "BORDUREBAS", "VRAI")
        Else
            TempoStr = Replace(TempoStr, "BORDUREBAS", "FAUX")
        End If
        
        '-> Concatainer
        If j = 0 Then
            DefCol = DefCol & TempoStr
        Else
            DefCol = DefCol & "|" & TempoStr
        End If
                
    Next 'Pour chaque colonne
        
    '-> Imprimer la d�finition de la ligne
    Print #hdlFile, DefCol
    '-> Pas de champs
    Print #hdlFile, "\CHAMPS�"
    '-> Fin de la ligne
    Print #hdlFile, "\END�BLOCK" & i
    
    '-> Imprimer l'appel de la ligne
    SpoolData = SpoolData & Chr(13) & Chr(10) & "[TB-TBANALYT(BLK-BLOCK" & i & ")][\1]"
    
    '-> Imprimer le saut de page
    If IsSautPage Then SpoolData = SpoolData & Chr(13) & Chr(10) & "[PAGE]" & Chr(13) & Chr(10) & "[TB-TBANALYT(BLK-BLOCK0" & ")][\1]"
       
Next 'Pour toutes les lignes du browse

'-> fermer le tableau
Print #hdlFile, "TABLEAU�END"

'-> fermer la maquette
Print #hdlFile, "[/MAQ]"

'-> Imprimer l'appel du spool
Print #hdlFile, SpoolData

'-> Fermer le spool
Close #hdlFile

'-> Lib�rer la mise � jour de l'�cran
LockWindowUpdate 0

'-> Faire un rename du ficier
CorpFile = Entry(1, SpoolName, ".") & ".turbo"
Name SpoolName As CorpFile

'-> Retourner le nom du fichier
PrintaFlex = CorpFile

Exit Function

EndError:

    'Afficher un message d'erreur MESSPROG
    MsgBox "Erreur lors de la g�n�ration du spool", vbCritical + vbOKOnly, "Erreur d'impression"
    '-> Ne pas retourner un nom de fichier
    PrintaFlex = ""
    '-> Lib�rer la mise � jour de l'�cran
    LockWindowUpdate 0

End Function


Public Function TriListeFic(Liste As String, aTb As clsTableau) As String
    '---> permet de traiter la liste de fichiers ascii dans l'ordre de ceux qui ont un axe d'analyse
    Dim aProgiciel As clsProgiciel
    Dim i As Integer
    Dim strRep As String
    
    For i = 1 To NumEntries(Liste, "|")
        Set aProgiciel = aTb.Progiciels("PRO_" & Entry(2, Entry(i, Liste, "|"), "�"))
        If aProgiciel.IsAxeAnalyse Then
            strRep = Entry(i, Liste, "|") & "|" & strRep
        Else
            strRep = strRep & Entry(i, Liste, "|") & "|"
        End If
    Next
    '-> On retourne la liste triee
    strRep = Mid$(strRep, 1, Len(strRep) - 1)
    TriListeFic = strRep
    
End Function

Public Sub UpdateCumulsWithDetail(ByRef aTb As clsTableau)
    '---> permet de mettre a jour les cumuls axes avec les valeurs des cumuls de detail
    Dim aCumulDet As clsCumul
    Dim aCumulAxe As clsCumul
    Dim aCol As clsCol
    Dim aLigne As clsRow
    Dim aValeur As clsValeur
    Dim strClef As String
    Dim aProg As clsProgiciel
    Dim i As Integer
    Dim j As Integer
    
    '-> On parcours les cumuls de plus haut niveau dans la collection de details
    For Each aCumulDet In DETAILS
        '-> On recupere le cumul axe correspondant a la clef d'acces axe du cumul detail
        If aCumulDet.ClefAxe <> "" Then
            strClef = aCumulDet.ClefAxe
            For i = 1 To NumEntries(aCumulDet.ClefAxe, "|")
                Set aCumulAxe = GetCumul(strClef, AXES)
                '-> On met a jour les valeurs des bonnes colonnes
                For Each aLigne In aTb.Lignes
                    '-> On cherche a renseigner les colonnes associees a l'axe de detail
                    For Each aCol In aTb.Colonnes
                        '-> On teste si colonne de detail sinon on ejecte
                        If aCol.TypeCol <> 0 Then GoTo NextCol
                        '-> On recuper le progiciel de la colonne
                        Set aProg = aTb.Progiciels("PRO_" & aCol.Progiciel)
                        '-> On verifie que le progiciel n'ait pas d'axe d'analyse pour mettre a jour
                        ' en effet si axe analyse alors forcement axe detail donc pas de mise a jour des valeurs
                        If aProg.IsAxeAnalyse Then GoTo NextCol
                        Set aValeur = aCumulAxe.Valeurs(aLigne.Code & "|" & aCol.Code)
                        '-> On met les valeurs a jour
                        aValeur.Value = aValeur.Value + aCumulDet.Valeurs(aLigne.Code & "|" & aCol.Code).Value
                        '-> On met les references details a jour ( les ligne et le nom du fichier ascii qui constituent cette valeur )
                        For j = 1 To aCumulDet.Valeurs(aLigne.Code & "|" & aCol.Code).colLigDetails.Count
                            aValeur.colLigDetails.Add aCumulDet.Valeurs(aLigne.Code & "|" & aCol.Code).colLigDetails(j)
                        Next

NextCol:
                    Next '-> On passe a la colonne suivante
                Next '-> On passe au niveau de cumul superieur
                If InStr(strClef, "|") <> 0 Then strClef = Mid$(strClef, 1, InStrRev(strClef, "|") - 1)
            Next '-> On passe a la ligne suivante
        End If '-> Si il existe un clef d'acces a un axe d'analyse
    Next '-> On passe au cumul detail suivant
    
End Sub

Public Sub AnalyseSourcesDonnees(ListeFichiers As String, FicEnCours As String)
    '---> ListeFichiers de type MONFICHIER-PROGICIEL|MONFICHIER-PROGICIEL.........
    '---> FicEnCours Fichier tableau( .DATA) en cours d'analyse
    '---> permet d'analyser le fichier ascii qui definit le tableau
    '---> permet d'analyser les fichiers ascii de donn�ees
    '---> permet de lancer la creation des cumuls
    Dim hdlFile As Integer
    Dim Fichier As String
    Dim aTb As clsTableau
    Dim Ligne As String
    Dim aProgiciel As clsProgiciel
    Dim NbLignes As Integer
    Dim TailleLue As Long
    Dim TailleTotale As Long
    Dim aNode As Node
    Dim ConditionsOk As Boolean
    Dim i As Integer
    Dim j As Integer
    Dim ClefDetail As String
    Dim ClefAxe As String
    Dim aDes As clsDescriptif
    Dim aField As clsDescriptif
    Dim NodeOk As Boolean
    
    '-> Lancer la cr�ation d'un nouveau tableau en fonction du fichier .DATA precis�
    Set aTb = LoadTb(FicEnCours, frmWait.Picture1)
    
    '-> Ajouter dans la collection de tableaux
    Tableaux.Add aTb, "TB_" & UCase$(aTb.Name)
    
    '-> On recupere la reference du tableau en cours ( car peut etre plusierus tableaux trait�s )
    TabEnCours = UCase$(aTb.Name)
    
    '-> Afficher la feuille de temporisation
    frmWait.Show
    frmWait.Enabled = False
    
    '-> Ouverture fichier ascii en lecture pour analyse
    hdlFile = FreeFile
    NbLignes = 1
    
    '-> On remet la liste des fichiers dans l'ordre de ceux qui ont l'axe d'analyse
    ListeFichiers = TriListeFic(ListeFichiers, aTb)
    
    '-> On parcours tous les fichiers de donn�es pour creer les cumuls
    For i = 1 To NumEntries(ListeFichiers, "|")
        '-> On reinitialise le compteur de lignes pour chaque fichier
        NbLignes = 1
        '-> On ouvre le fichier de donn�es
        Open Entry(1, Entry(i, ListeFichiers, "|"), "�") For Input As #hdlFile
        '-> R�cuperer la taille du fichier
        TailleTotale = GetFileSizeVB(Entry(1, Entry(i, ListeFichiers, "|"), "�"))
        '-> On recupere le progiciel associe au fichier
        Set aProgiciel = aTb.Progiciels("PRO_" & Entry(2, Entry(i, ListeFichiers, "|"), "�"))
        
        '-> Si il n'y a qu'un seul fichier on precise de creer les nodes meme sur le fichier de detail
        If NumEntries(ListeFichiers, "|") = 1 Then
            If Not aProgiciel.IsAxeAnalyse Then
                NodeOk = True
            Else
                NodeOk = False
            End If
        End If
        
        '-> On traite le fichier ascii ligne a ligne
        Do While Not EOF(hdlFile)
            '-> Recup de la ligne du fichier
            Line Input #hdlFile, Ligne
            
            '-> Dessin de la temporisation
            TailleLue = TailleLue + Len(Ligne) + 2
            DrawWait TailleTotale, TailleLue, frmWait.Picture2
            
            '-> teste les conditions de la ligne par rapport a la ligne de fichier ascii
            ConditionsOk = EvalueCondition(aTb, Ligne, aProgiciel)
            '-> Analyse sur axe d'analyse si existe si conditions respect�es
            If ConditionsOk Then
                ClefAxe = ""
                '-> On analyse la ligne pour chaque progiciel dans le tableau
                If aProgiciel.IsAxeAnalyse Then
                    '-> On lance la recursivite sur l'axe d'analyse
                    AnalyseFichierAXE aProgiciel.AxeAnalyses, aTb, 1, Ligne, AXES, aProgiciel, NbLignes, True, Entry(1, Entry(i, ListeFichiers, "|"), "�")
                    '-> On stocke la clef d'acces au cumul depuis le detail
                    For j = 1 To aProgiciel.AxeAnalyses.Count
                        ClefAxe = ClefAxe & Entry(j, Ligne, "|") & "|"
                    Next
                    ClefAxe = Mid$(ClefAxe, 1, Len(ClefAxe) - 1)
                End If
            
                '-> Analyse sur axe de detail si existe en precisant qu'on ne demarre l'analyse qu'au niveau
                If aProgiciel.IsAxeDet Then
                    '-> On lance la recursivite sur l'axe detail
                    AnalyseFichierAXE aProgiciel.AxeDetails, aTb, 1, Ligne, DETAILS, aProgiciel, NbLignes, NodeOk, Entry(1, Entry(i, ListeFichiers, "|"), "�"), ClefAxe
                End If
                
            End If
            
            '-> Pour chaque ligne de fichier on reinitialise le node de reference
            Set aNodePrev = Nothing
            NbLignes = NbLignes + 1
            
        Loop '-> On boucle sur les lignes du fichier de donn�es
        
        '-> Fermeture fichier ascii de donn�es
        Close #hdlFile
        
    Next '-> On analyse le fichier de donn�es suivant
    
    '-> On met les cumuls axes a jour avec les valeurs des cumuls de detail
    UpdateCumulsWithDetail aTb
    
    '-> On ferme la fenetre de temporisation
    Unload frmWait
    
End Sub

Public Sub DispDetail(DetCol As Boolean)
    '---> Afficher le justificatif des cumuls du tableau en cours
    strRetour = ""
    Load frmTabCal
    frmTabCal.CodeCumul = mdiMain.TreeNaviga.SelectedItem.Key
    frmTabCal.CodeTB = TabEnCours
    '-> On lance l'initialisation
    frmTabCal.Initialisation
    '-> Affichage de la fenetre de detail
    If strRetour <> "N" Then
        frmTabCal.Show vbModal
    Else
        strRetour = ""
    End If
    
End Sub

Public Sub ChargeValeurs(ByRef aCumul As clsCumul, ListeDetail As ListView, aValeur As clsValeur)
    '---> Charge le listview de detail d'un cumul en fonction des valeurs des fichiers ascii correspondants
    '-> Pour chaque onglet du listview on renseigne les listview
    Dim j As Integer
    Dim k As Integer
    Dim Lig As Integer
    Dim hdlFile As Integer
    Dim RefItem As Integer
    Dim aColHead As ColumnHeader
    Dim Ligne As String
                      
    RefItem = 1
    For j = 1 To aValeur.colLigDetails.Count
        '-> On rajoute une ligne dans le listview
        ListeDetail.ListItems.Add
        '-> On parcours la liste de lignes de fichiers correspondant a cette valeur
        For k = 1 To ListeDetail.ColumnHeaders.Count
            '-> On recupere une colonne
            Set aColHead = ListeDetail.ColumnHeaders(k)
            
            '-> On ouvre le fichier correspondant au detail de cette valeur
            hdlFile = FreeFile
            Open Entry(2, aValeur.colLigDetails(j), "�") For Input As #hdlFile

            '-> On pointe sur la ligne
            For Lig = 1 To Entry(1, aValeur.colLigDetails(j), "�")
                Line Input #hdlFile, Ligne
            Next
            '-> On met les valeurs a jour
            If k = 1 Then
                ListeDetail.ListItems(RefItem).Text = Entry(Entry(2, ListeDetail.ColumnHeaders(k).Tag, "|"), Ligne, "|")
            Else
                ListeDetail.ListItems(RefItem).SubItems(k - 1) = Entry(Entry(2, ListeDetail.ColumnHeaders(k).Tag, "|"), Ligne, "|")
            End If

            '-> On referme le fichier
            Close #hdlFile

        Next
        RefItem = RefItem + 1
    Next

End Sub

Public Sub FormatEnteteCol(List As ListView)
    '---> permet de redimensionner automatiquement les colonnes d'un listview
    Dim i As Long
    Dim X As ListItem
     
    '-> Ne rien faire si pas de colonnes
    If List.ColumnHeaders.Count = 0 Then Exit Sub
     
    '-> De base toujours cr�er un enregistrement avec les entetes de colonnes
    Set X = List.ListItems.Add(, "DEALENREGENTETE")
     
    For i = 0 To List.ColumnHeaders.Count - 1
        '-> Ajouter le libelle de l'entete de la colonne
        If i = 0 Then
            X.Text = List.ColumnHeaders(1).Text
        Else
            X.SubItems(i) = List.ColumnHeaders(i + 1).Text
        End If
        SendMessage List.hwnd, LVM_SETCOLUMNWIDTH, i, 0
    Next
     
    '-> Supprimer le premier enregistrement
    List.ListItems.Remove ("DEALENREGENTETE")
 
End Sub

