VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCumul"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'***************************************
' Classe des cumuls
'***************************************

'-> Cle d'acces dans la collection
Public Key As String
'-> Definition d'une collection des sous-cumuls
Public Cumuls As Collection
'-> Definition d'une collection de valeurs
Public Valeurs As Collection
'-> Definition de la clef d'acces au cumul AXE depuis le cumul detail
Public ClefAxe As String


Private Sub Class_Initialize()
    Set Valeurs = New Collection
    Set Cumuls = New Collection
End Sub

Private Sub Class_Terminate()
    Set Valeurs = Nothing
    Set Cumuls = Nothing
End Sub
