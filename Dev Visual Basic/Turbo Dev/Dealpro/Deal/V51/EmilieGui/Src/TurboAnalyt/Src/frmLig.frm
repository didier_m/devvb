VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmLig 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   5235
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   8070
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5235
   ScaleWidth      =   8070
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin TabDlg.SSTab SSTab1 
      Height          =   4695
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   8055
      _ExtentX        =   14208
      _ExtentY        =   8281
      _Version        =   393216
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "G�n�ral "
      TabPicture(0)   =   "frmLig.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame1"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Conditions"
      TabPicture(1)   =   "frmLig.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Frame3"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Calculs"
      TabPicture(2)   =   "frmLig.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame4"
      Tab(2).ControlCount=   1
      Begin VB.Frame Frame4 
         Caption         =   "El�ments du calcul : "
         Height          =   4215
         Left            =   -74880
         TabIndex        =   15
         Top             =   360
         Width           =   7695
         Begin RichTextLib.RichTextBox Rtf 
            Height          =   3135
            Left            =   120
            TabIndex        =   18
            Top             =   840
            Width           =   7455
            _ExtentX        =   13150
            _ExtentY        =   5530
            _Version        =   393217
            BackColor       =   16777215
            Enabled         =   -1  'True
            TextRTF         =   $"frmLig.frx":0054
         End
         Begin MSComctlLib.Toolbar Toolbar3 
            Height          =   330
            Left            =   240
            TabIndex        =   19
            Top             =   360
            Width           =   735
            _ExtentX        =   1296
            _ExtentY        =   582
            ButtonWidth     =   609
            ButtonHeight    =   582
            Wrappable       =   0   'False
            Style           =   1
            ImageList       =   "ImageList1"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ADD"
                  Object.ToolTipText     =   "Inserer une ligne"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "CHECK"
                  Object.ToolTipText     =   "V�rifier la formule de calcul"
                  ImageIndex      =   2
               EndProperty
            EndProperty
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Conditions : "
         Height          =   4215
         Left            =   120
         TabIndex        =   14
         Top             =   360
         Width           =   7815
         Begin VB.ComboBox Combo1 
            Height          =   315
            Left            =   240
            TabIndex        =   21
            Top             =   720
            Width           =   3255
         End
         Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
            Height          =   2295
            Left            =   240
            TabIndex        =   16
            Top             =   1800
            Width           =   3615
            _ExtentX        =   6376
            _ExtentY        =   4048
            _Version        =   393216
            Cols            =   3
            FixedCols       =   0
            AllowBigSelection=   0   'False
            ScrollBars      =   2
         End
         Begin MSFlexGridLib.MSFlexGrid MSFlexGrid2 
            Height          =   2295
            Left            =   3960
            TabIndex        =   17
            Top             =   1800
            Width           =   3735
            _ExtentX        =   6588
            _ExtentY        =   4048
            _Version        =   393216
            Cols            =   4
            FixedCols       =   0
            ScrollBars      =   2
         End
         Begin VB.Image Image5 
            Height          =   480
            Left            =   4560
            Picture         =   "frmLig.frx":00D6
            Top             =   1200
            Width           =   480
         End
         Begin VB.Image Image4 
            Height          =   480
            Left            =   3960
            Picture         =   "frmLig.frx":03E0
            Top             =   1200
            Width           =   480
         End
         Begin VB.Image Image3 
            Height          =   480
            Left            =   840
            Picture         =   "frmLig.frx":06EA
            Top             =   1200
            Width           =   480
         End
         Begin VB.Image Image2 
            Height          =   480
            Left            =   240
            Picture         =   "frmLig.frx":09F4
            Top             =   1200
            Width           =   480
         End
         Begin VB.Image Image1 
            Height          =   240
            Left            =   3600
            Picture         =   "frmLig.frx":0CFE
            ToolTipText     =   "Acc�s au descriptif"
            Top             =   750
            Width           =   240
         End
         Begin VB.Label Label4 
            Caption         =   "S�lection d'un progiciel : "
            Height          =   255
            Left            =   240
            TabIndex        =   20
            Top             =   360
            Width           =   2175
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Propri�t�s : "
         Height          =   4215
         Left            =   -74880
         TabIndex        =   10
         Top             =   360
         Width           =   7815
         Begin VB.CheckBox Check1 
            Alignment       =   1  'Right Justify
            Caption         =   "Ligne visible"
            Height          =   195
            Left            =   600
            TabIndex        =   3
            Top             =   1800
            Width           =   2175
         End
         Begin VB.Frame Frame2 
            Caption         =   "Type de ligne : "
            Height          =   1455
            Left            =   600
            TabIndex        =   4
            Top             =   2280
            Width           =   3975
            Begin VB.OptionButton TypeLig 
               Caption         =   "Ligne de s�paration"
               Height          =   195
               Index           =   2
               Left            =   240
               TabIndex        =   7
               Top             =   1080
               Width           =   2535
            End
            Begin VB.OptionButton TypeLig 
               Caption         =   "Ligne de cumul"
               Height          =   195
               Index           =   1
               Left            =   240
               TabIndex        =   6
               Top             =   720
               Width           =   2535
            End
            Begin VB.OptionButton TypeLig 
               Caption         =   "Ligne de d�tail li�e � un fichier"
               Height          =   195
               Index           =   0
               Left            =   240
               TabIndex        =   5
               Top             =   360
               Value           =   -1  'True
               Width           =   2535
            End
         End
         Begin VB.TextBox Text3 
            Height          =   285
            Left            =   1560
            TabIndex        =   2
            Top             =   1320
            Width           =   1215
         End
         Begin VB.TextBox Text2 
            Height          =   285
            Left            =   1560
            TabIndex        =   1
            Top             =   960
            Width           =   3015
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1560
            TabIndex        =   0
            Top             =   600
            Width           =   1215
         End
         Begin VB.Label Label3 
            Caption         =   "Hauteur : "
            Height          =   255
            Left            =   600
            TabIndex        =   13
            Top             =   1320
            Width           =   855
         End
         Begin VB.Label Label2 
            Caption         =   "Libell� : "
            Height          =   255
            Left            =   600
            TabIndex        =   12
            Top             =   960
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Code : "
            Height          =   255
            Left            =   600
            TabIndex        =   11
            Top             =   600
            Width           =   1215
         End
      End
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   7560
      Picture         =   "frmLig.frx":1088
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   4800
      Width           =   495
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   360
      Top             =   4680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLig.frx":14CA
            Key             =   "Macro"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLig.frx":15DC
            Key             =   "Spell Check"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmLig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public aRow As clsRow 'Pointeur vers la ligne en cours
Public TbCode As String 'code du tableau en cours

'-> Indique le type de la ligne
Private IdTypeLig As Integer

'-> Indique que l'on est en train de charger le RTF de calcul
Private IsInserting As Boolean


Private Sub Combo1_Click()

    '-> Ne rien faire si on n'a pas s�lectionn� un objet
    If Me.Combo1.ListIndex = -1 Then Exit Sub
    
    '-> Afficher la liste des conditions pour ce progiciel
    DisplayCdtByProgiciel

End Sub

Private Sub Form_Load()

    '-> Redimensionner le flexGrid
    
    Dim aRect As RECT
    Dim aLb As Libelle
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMLIG")
    
    '-> Liste des conditions
    GetClientRect Me.MSFlexGrid1.hwnd, aRect
    Me.MSFlexGrid1.ColWidth(0) = Me.ScaleX(aRect.Right / 6, 3, 1)
    Me.MSFlexGrid1.ColWidth(1) = Me.ScaleX(aRect.Right / 3, 3, 1)
    Me.MSFlexGrid1.ColWidth(2) = Me.ScaleX(aRect.Right, 3, 1) - Me.MSFlexGrid1.ColWidth(0) - Me.MSFlexGrid1.ColWidth(1)
    
    '-> Liste des valeurs des conditions
    GetClientRect Me.MSFlexGrid2.hwnd, aRect
    Me.MSFlexGrid2.ColWidth(0) = Me.ScaleX(1, 7, 1)
    Me.MSFlexGrid2.ColWidth(1) = Me.ScaleX(aRect.Right / 3, 3, 1)
    Me.MSFlexGrid2.ColWidth(2) = (Me.ScaleX(aRect.Right, 3, 1) - (Me.MSFlexGrid2.ColWidth(0) + Me.MSFlexGrid2.ColWidth(1))) / 2
    Me.MSFlexGrid2.ColWidth(3) = (Me.ScaleX(aRect.Right, 3, 1)) - (Me.MSFlexGrid2.ColWidth(0) + Me.MSFlexGrid2.ColWidth(1) + Me.MSFlexGrid2.ColWidth(2))
    
    '-> Supprimer la premi�re ligne
    Me.MSFlexGrid1.Rows = 1
    Me.MSFlexGrid2.Rows = 1
    
    '-> Gestion des libell�s pour le browse des conditions MESSPROG
    Me.MSFlexGrid1.TextArray(1) = aLb.GetCaption(1)
    Me.MSFlexGrid1.TextArray(2) = aLb.GetCaption(2)
    
    '-> Gestion des libell�s pour le browse des valeurs des conditions MESSPROG
    Me.MSFlexGrid2.TextArray(1) = aLb.GetCaption(3)
    Me.MSFlexGrid2.TextArray(2) = aLb.GetCaption(4)
    Me.MSFlexGrid2.TextArray(3) = aLb.GetCaption(5)

    Set aLb = Nothing
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    '---> Mettre � jour les param�tres de la ligne
    
    Dim SetError As Boolean
    Dim aCdt As clsCondition
    Dim DelCdt As Boolean
    Dim aLb As Libelle
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMLIG")
    
    '-> V�rifier la hauteur sp�cifi�e
    If Trim(Me.Text3.Text) = "" Then SetError = True
    If Not IsNumeric(Me.Text3.Text) Then
        SetError = True
    Else
        If CLng(Me.Text3.Text) = 0 Then SetError = True
    End If
    
    If SetError Then 'MESSPROG
        MsgBox aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetCaption(7)
        Me.Text3.SetFocus
        Cancel = 1
        Exit Sub
    End If
    
    '-> Mettre � jour les propri�t�s
    aRow.Visible = CBool(Me.Check1.Value)
    aRow.Hauteur = Trim(Me.Text3.Text)
    aRow.HauteurTwips = Me.ScaleY(aRow.Hauteur, 6, 1)
    aRow.Libelle = Me.Text2.Text
    aRow.TypeLig = IdTypeLig
    
    '-> Si on n'est pas en ligne de type d�tail, supprimer toutes les conditions existantes
    Select Case IdTypeLig
        
        Case 0 '-> On est dans une ligne de d�tail
            '-> Virer les formules de calcul
            aRow.Calcul = ""
            aRow.CalculRtf = ""
            DelCdt = False
        Case 1 '-> On est dans une ligne de type calcul
            '-> R�cup�rer les formules de calcul
            aRow.CalculRtf = Me.Rtf.TextRTF
            aRow.Calcul = Me.Rtf.Text
            '-> Supprimer toutes les conditions
            DelCdt = True
        Case 3 '-> On est dans une ligne de s�paration
            '-> Vider la zone de calcul
            aRow.Calcul = ""
            aRow.CalculRtf = ""
            '-> Supprimer toutes les conditions
            DelCdt = True
    End Select
        
    If DelCdt Then
        Do While aRow.Conditions.Count <> 0
            aRow.Conditions.Remove (1)
        Loop
    End If
    
    Set aLb = Nothing

End Sub

Private Sub Image1_Click()

    Dim aTb As clsTableau
    Dim OldProgi As String
    
    '-> Get d'un pointeur vers le tableau
    Set aTb = Tableaux("TB_" & Me.TbCode)
    
    '-> R�cup�rer le progiciel s�l�ectionn� s'il y en a un
    If Me.Combo1.ListIndex <> -1 Then
        OldProgi = UCase$(Trim(Entry(1, Me.Combo1.List(Me.Combo1.ListIndex), "-")))
    End If
    
    '-> Initialiser le gestionnaire de descriptif
    Set frmDescriptif.aTb = aTb
    frmDescriptif.Show vbModal
    
    '-> Recharger la liste des progiciels
    Loadprogi OldProgi
    
    '-> s�lectionner par d�faut le progiciel sp�cifi� si necessaire
    If OldProgi <> "" Then Combo1_Click

End Sub

Private Sub Image2_Click()

    '---> Cette proc�dure sert � jouter une condition pour un progiciel donn�
    
    Dim aTb As clsTableau
    Dim aLb As Libelle
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMLIG")
    
    '-> V�rifier dans un premier temps que le progiciel soit s�lectionn�
    If Me.Combo1.ListIndex = -1 Then 'MESSPROG
        MsgBox aLb.GetCaption(8), vbExclamation + vbOKOnly, aLb.GetCaption(7)
        Me.Combo1.SetFocus
        Exit Sub
    End If
    
    '-> Get d'un pointeur vers le tableaus en coues
    Set aTb = Tableaux("TB_" & UCase$(Trim(Me.TbCode)))
    
    '-> Afficher la liste des choix possibles
    Set frmListeCol.aTb = aTb
    frmListeCol.TypeDisplay = 2
    frmListeCol.TypeProgi = GetProgicielString
    frmListeCol.Init
    frmListeCol.Show vbModal
    
    '-> Tester le retour
    If strRetour = "" Then Exit Sub
    
    '-> V�rifier que la colonne s�lectionn�e ne soit pas d�ja cod�e
    If aRow.IsCondition(strRetour) Then   'MESSPROG
        MsgBox aLb.GetCaption(9), vbCritical + vbOKOnly, aLb.GetCaption(7)
        Exit Sub
    End If
    
    '-> Ajouter la condition dans la ligne
    aRow.AddCdt UCase$(Trim(Entry(3, strRetour, Chr(0)))), UCase$(Trim(Entry(2, strRetour, Chr(0))))
    
    '-> Ajouter dans le browse
    AddCdtInBrowse UCase$(Trim(Entry(3, strRetour, Chr(0)))), UCase$(Trim(Entry(2, strRetour, Chr(0))))
    
    Set aLb = Nothing
    
End Sub

Private Sub Image3_Click()

    '---> Cette proc�dure supprime une condition d'une ligne pour un logiciel donn�
    
    Dim Rep As VbMsgBoxResult
    Dim Condition As String
    Dim Progiciel As String
    Dim aLb As Libelle
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMLIG")
    
    '-> V�rifier qu'il y a des conditions
    If Me.MSFlexGrid1.Rows = 1 Then Exit Sub
    
    '-> R�cup�rer le param�trage
    Progiciel = GetProgicielString
    Condition = UCase$(Trim(Me.MSFlexGrid1.TextArray(Me.MSFlexGrid1.Row * Me.MSFlexGrid1.Cols + 1)))
    
    '-> Demander confirmation MESSPROG
    Rep = MsgBox(aLb.GetCaption(10) & Condition & aLb.GetCaption(11) & Progiciel, vbQuestion + vbYesNo, aLb.GetCaption(12))
    If Rep = vbNo Then Exit Sub
    
    '-> Suppression de la condition
    DelCondition Condition, Progiciel
    
    '-> S'il reste des conditions, repositionner
    If Me.MSFlexGrid1.Rows <> 1 Then
        '-> Positionner sur la premi�re des conditions
        Me.MSFlexGrid1.Row = 1
        '-> Afficher les valeurs de cette condition
        MSFlexGrid1_Click
        '-> Vider l'op�rande sur la premi�re ligne
        Me.MSFlexGrid1.TextArray(1 * Me.MSFlexGrid1.Cols) = ""
    End If
    
    '-> On libere le pointeur
    Set aLb = Nothing
    
End Sub

Private Sub Image4_Click()
    
    Dim aLb As Libelle
    
    '-> V�rifier qu'un progiciel soit s�lectionn�
    If Me.Combo1.ListIndex = -1 Then Exit Sub
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMLIG")
    
    '-> V�rifier que l'on a affect� une condition en premier
    If Me.MSFlexGrid1.Rows = 1 Then 'MESSPROG
        MsgBox aLb.GetCaption(13), vbExclamation + vbOKOnly, aLb.GetCaption(7)
        Exit Sub
    End If
            
    '-> V�rifier que l'on pointe bien sur une condition en particulier
    If Me.MSFlexGrid1.Row = 0 Then 'MESSPROG
        MsgBox aLb.GetCaption(14), vbExclamation + vbOKOnly, aLb.GetCaption(7)
        Exit Sub
    End If
    
    '-> Ajouter une valeur pour cette condition
    AddValueCdt
    
    '-> On libere le pointeur
    Set aLb = Nothing
    
End Sub

Private Sub Image5_Click()

    Dim Rep As VbMsgBoxResult
    Dim aLb As Libelle
    
    '-> V�rifier qu'il y a une valeur de condition � supprimer
    If Me.MSFlexGrid2.Rows = 1 Then Exit Sub
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMLIG")
    
    '-> Toujours supprimer la derni�re valeur de la condition
    Me.MSFlexGrid2.Row = Me.MSFlexGrid2.Rows - 1
            
    '-> Demander condirmation MESSPROG
    Rep = MsgBox(aLb.GetCaption(15) & Chr(13) & Me.MSFlexGrid2.TextArray(Me.MSFlexGrid2.Row * Me.MSFlexGrid2.Cols) _
                                                     & Chr(13) & Me.MSFlexGrid2.TextArray(Me.MSFlexGrid2.Row * Me.MSFlexGrid2.Cols + 1) _
                                                     & Chr(13) & Me.MSFlexGrid2.TextArray(Me.MSFlexGrid2.Row * Me.MSFlexGrid2.Cols + 2) _
                                                     & Chr(13) & Me.MSFlexGrid2.TextArray(Me.MSFlexGrid2.Row * Me.MSFlexGrid2.Cols + 2) _
                                                     , vbQuestion + vbYesNo, aLb.GetCaption(12))
    If Rep = vbNo Then Exit Sub
    
    '-> Supprimer la condition dans la classe
    DelValueCdt
    
    '-> On libere le pointeur
    Set aLb = Nothing
    
End Sub

Private Sub MSFlexGrid1_Click()

    '---> Afficher les valeurs pour cette condition
    
    Dim aCdt As clsCondition
    Dim aValCdt As clsValueCdt
    
    '-> Quitter si pas de conditions
    If Me.MSFlexGrid1.Rows = 1 Then Exit Sub
    
    '-> Pointer sur la condition sp�cifi�e
    Me.MSFlexGrid1.Col = 1
    Set aCdt = Me.aRow.Conditions("CDT_" & UCase$(Trim(Me.MSFlexGrid1.Text)) & "_" & GetProgicielString)
    
    '-> Init du Browse
    Me.MSFlexGrid2.Rows = 1
    
    '-> Afficher toutes les valeurs de cette condition
    For Each aValCdt In aCdt.ValueCdts
        '-> Ajouter une ligne dans le browse
        Me.MSFlexGrid2.Rows = Me.MSFlexGrid2.Rows + 1
        Me.MSFlexGrid2.Row = Me.MSFlexGrid2.Rows - 1
        '-> Cr�ation dans le browse
        DisplayValCdtInBrowse aValCdt
    Next

End Sub

Private Sub MSFlexGrid1_DblClick()

    Dim aCdt As clsCondition
    
    
    '-> Quitter si on n'est pas au moins sur la seconde ligne
    If Me.MSFlexGrid1.Row < 2 Then Exit Sub
    
    '-> Pointer sur la condition
    Me.MSFlexGrid1.Col = 1
    Set aCdt = Me.aRow.Conditions("CDT_" & UCase$(Trim(Me.MSFlexGrid1.Text)) & "_" & GetProgicielString)
    
    '-> Modification de l'op�rande de concataination
    If aCdt.TypeCdt = 0 Then
        aCdt.TypeCdt = 1
    Else
        aCdt.TypeCdt = 0
    End If
    
    '-> Modifier le libell�
    Me.MSFlexGrid1.Col = 0
    Me.MSFlexGrid1.Text = OperationCdt(aCdt.TypeCdt)

End Sub


Private Sub MSFlexGrid2_DblClick()

    Dim aCdt As clsCondition
    Dim aValCdt As clsValueCdt
    Dim Rep As String
    Dim aLb As Libelle
    
    '-> Ne rien faire si pas de valeurs pour cette condition
    If Me.MSFlexGrid2.Rows = 1 Then Exit Sub
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMLIG")
    
    '-> Pointer sur la condition sp�cifi�e
    Set aCdt = aRow.Conditions("CDT_" & UCase$(Me.MSFlexGrid1.TextArray(Me.MSFlexGrid1.Row * Me.MSFlexGrid1.Cols + 1)) & "_" & GetProgicielString)
    
    '-> Pointer sur la valeur de cette condition
    Set aValCdt = aCdt.ValueCdts("VALCDT_" & Me.MSFlexGrid2.Row)
    
    '-> Selon la colonne o� on a cliqu�
    Select Case Me.MSFlexGrid2.Col
    
        Case 0 'Op�rateur
            '-> Ne pas traiter la premi�re ligne : -> elle n'a pas d'op�randes
            If Me.MSFlexGrid2.Row = 1 Then Exit Sub
            
            '-> Inverser l'op�rande
            If aValCdt.Operande = 0 Then
                aValCdt.Operande = 1
            Else
                aValCdt.Operande = 0
            End If
            
        Case 1 'Type de condition
        
            '-> Faire + 1 sur la nature de la condition
            If aValCdt.Operation = 8 Then
                aValCdt.Operation = 1
            Else
                aValCdt.Operation = aValCdt.Operation + 1
            End If
            
            '-> Si op�rande <> 5 (Compris entre) -> Mettre la valeur maxi � blanc
            If aValCdt.Operation <> 5 Then aValCdt.ValMaxi = ""
                
        Case 2 'Val mini
            
            '-> Demander la valeur mini
            Rep = InputBox(aLb.GetCaption(16), aLb.GetCaption(17), aValCdt.ValMini)
            If Trim(Rep) = "�" Then
                aValCdt.ValMini = ""
            Else
                If Trim(Rep) = "" Then Exit Sub
                aValCdt.ValMini = Trim(Rep)
            End If
            
            '-> Positionner la valeur maxi si on est en operation <> 5
            If aValCdt.Operation <> 5 Then aValCdt.ValMaxi = ""
        
        Case 3 'Val Maxi
        
            '-> Ne pouvoir saisir la valeur maxi que si operation = 5
            If aValCdt.Operation <> 5 Then Exit Sub
        
            '-> Demander la valeur maxi
            Rep = InputBox(aLb.GetCaption(18), aLb.GetCaption(19), aValCdt.ValMaxi)
            If Trim(Rep) = "�" Then
                aValCdt.ValMaxi = ""
            Else
                If Trim(Rep) = "" Then Exit Sub
                aValCdt.ValMaxi = Trim(Rep)
            End If
        
    End Select
    
    '-> Dans tous les cas, refaire l'affichage de la ligne
    DisplayValCdtInBrowse aValCdt
    
    Set aLb = Nothing
    
End Sub



Private Sub Rtf_KeyPress(KeyAscii As Integer)

    '---> Gestion de l'�diteur de fonction
    
    If IsInserting Then Exit Sub
    
    Select Case KeyAscii
        Case 40, 41 'Parenth�ses
            Me.Rtf.SelColor = RGB(0, 255, 0)
        Case 8, 42, 43, 45, 47 'Signes math
            Me.Rtf.SelColor = RGB(0, 0, 255)
        Case 48 To 57 'Chiffres
            Me.Rtf.SelColor = 0
        Case Else 'Annulation
            KeyAscii = 0
    End Select

End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)

    Select Case KeyAscii
        Case 45, 61, 124, 167
            KeyAscii = 0
    End Select

End Sub

Private Sub Text3_KeyPress(KeyAscii As Integer)
    KeyAscii = GetIntAscii(KeyAscii)
End Sub

Private Sub DelCondition(Condition As String, Progiciel As String)

'---> Suppression d'une condition

    Dim aCdt As clsCondition
    
    '-> Pointer sur la condition
    Set aCdt = aRow.Conditions("CDT_" & Condition & "_" & Progiciel)
    
    '-> Supprimer toutes les valeurs de cette condition
    Do While aCdt.ValueCdts.Count <> 0
        aCdt.ValueCdts.Remove (1)
    Loop
    
    '-> Vider le browse des valeurs de condition
    Me.MSFlexGrid2.Rows = 1
    
    '-> Supprimer la condition de la ligne en cours
    aRow.Conditions.Remove ("CDT_" & Condition & "_" & Progiciel)
    
    '-> Supprimer la ligne du browse des conditions
    If Me.MSFlexGrid1.Rows = 2 Then
        Me.MSFlexGrid1.Rows = 1
    Else
        Me.MSFlexGrid1.RemoveItem (Me.MSFlexGrid1.Row)
    End If

End Sub


Private Sub AddCdtInBrowse(ColonneCode As String, Progiciel As String)

    '---> Cette proc�dure cr�er une ligne dans le browse des conditions
    
    Dim aCol As clsCol
    Dim aCdt As clsCondition
    Dim aTb As clsTableau
    Dim aValCdt As clsValueCdt
    Dim aDes As clsDescriptif
    Dim aProgiciel As clsProgiciel
    
    '-> Pointer sur la condition
    Set aCdt = aRow.Conditions("CDT_" & UCase$(Trim(ColonneCode)) & "_" & UCase$(Trim(Progiciel)))
    
    '-> Ajouter une ligne dans le browse
    Me.MSFlexGrid1.Rows = Me.MSFlexGrid1.Rows + 1
    
    '-> Get d'un pointeur vers le tableau
    Set aTb = Tableaux("TB_" & Me.TbCode)
    
    '-> Get d'un pointeur vers le progiciel
    Set aProgiciel = aTb.Progiciels("PRO_" & Progiciel)
    
    '-> Pointer sur l'objet de description sp�cifi�
    Set aDes = aProgiciel.Descriptifs("FIELD_" & aCdt.Colonne)
    
    '-> Mettre � jour le code de la colonne
    Me.MSFlexGrid1.TextArray((Me.MSFlexGrid1.Rows - 1) * Me.MSFlexGrid1.Cols + 1) = aCdt.Colonne
    Me.MSFlexGrid1.TextArray((Me.MSFlexGrid1.Rows - 1) * Me.MSFlexGrid1.Cols + 2) = aDes.Commentaire
    
    '-> Par d�faut ajouter la condition Logique "ET" si on est sur la seconde condition
    If Me.MSFlexGrid1.Rows > 2 Then
        '-> Ajouter le libell� dans la colonne
        Me.MSFlexGrid1.TextArray((Me.MSFlexGrid1.Rows - 1) * Me.MSFlexGrid1.Cols) = OperationCdt(aCdt.TypeCdt)
    End If
    
    '-> Vider le browse des valeurs pour cette condition
    Me.MSFlexGrid2.Rows = 1
    
    '-> Positionner la ligne sur le browse
    Me.MSFlexGrid1.Row = Me.MSFlexGrid1.Rows - 1
    Me.MSFlexGrid1.Col = 0

End Sub


Public Sub Initialisation()

    '---> Cette proc�dure intialise les propri�t�s d'une ligne
    
    Dim aCdt As clsCondition
    
    '-> Recharger la liste des progiciels
    Loadprogi
    
    '-> Propri�t�s de la ligne
    Me.Text1.Text = aRow.Code
    Me.Text2.Text = aRow.Libelle
    Me.Text3.Text = aRow.Hauteur
    Me.Check1.Value = Abs(CBool(aRow.Visible))
    
    '-> type de ligne
    TypeLig(aRow.TypeLig).Value = True
    TypeLig_Click aRow.TypeLig
    
    '-> Afficher la liste des conditions pour le premier logiciel attach� � cette ligne
    If Me.Combo1.ListCount <> 0 Then
        '-> S�lectionner le premier logiciel
        Me.Combo1.ListIndex = 0
    End If
    
    '-> Cr�er la liste des calculs
    If aRow.TypeLig = 1 Then
        '-> Ligne de type calcul
        IsInserting = True
        Me.Rtf.TextRTF = aRow.CalculRtf
    Else
        Me.Rtf.Text = ""
    End If
    
    IsInserting = False
    
    '-> Caption de la feuille MESSPROG
    Me.Caption = "Edition de la ligne : " & aRow.Code & " - " & aRow.Libelle

End Sub

Private Sub Command1_Click()

    '-> D�charger la feuille
    Unload Me

End Sub

Private Sub Text2_GotFocus()
    SelectTxtBox Me.Text2
End Sub

Private Sub Text3_GotFocus()
    SelectTxtBox Me.Text3
End Sub

Private Sub DelValueCdt()

    '---> Cette proc�dure supprime une valeur pour une condition
    
    Dim aCdt As clsCondition
    
    '-> Pointer sur la condition
    Set aCdt = aRow.Conditions("CDT_" & GetProgicielString)
    
    '-> supprimer la derni�re valeur de la condition
    aCdt.ValueCdts.Remove (aCdt.ValueCdts.Count)
    
    '-> Enlever une ligne du browse
    If Me.MSFlexGrid2.Rows = 2 Then
        Me.MSFlexGrid2.Rows = 1
    Else
        Me.MSFlexGrid2.Rows = Me.MSFlexGrid2.Rows - 1
    End If

End Sub

Private Sub AddValueCdt()

    '---> Cette proc�dure ajoute une valeur pour une condition
    
    Dim aCdt As clsCondition
    Dim aValCdt As clsValueCdt
    
    '-> Cr�er une nouvelle valeur pour cette condition
    Set aValCdt = New clsValueCdt
    
    '-> Pointer sur la condition
    Set aCdt = aRow.Conditions("CDT_" & UCase$(Me.MSFlexGrid1.TextArray(Me.MSFlexGrid1.Row * Me.MSFlexGrid1.Cols + 1)) & "_" _
                               & GetProgicielString)
    
    '-> AJouter dans la collection
    aCdt.ValueCdts.Add aValCdt, "VALCDT_" & aCdt.ValueCdts.Count + 1
    
    '-> Ajouter un ligne dans le browse des valeurs
    Me.MSFlexGrid2.Rows = Me.MSFlexGrid2.Rows + 1
    Me.MSFlexGrid2.Row = Me.MSFlexGrid2.Rows - 1
    
    '-> Ajout dans le browse
    DisplayValCdtInBrowse aValCdt

End Sub

Private Sub DisplayValCdtInBrowse(aValCdt As clsValueCdt)

    '-> Ajouter la concataination ET/OU
    If Me.MSFlexGrid2.Row > 1 Then
        '-> Ajout par d�faut de ET
        Me.MSFlexGrid2.TextArray(Me.MSFlexGrid2.Row * Me.MSFlexGrid2.Cols) = OperationCdt(aValCdt.Operande)
        
    End If
    
    '-> Ajout du type de condition
    Me.MSFlexGrid2.TextArray(Me.MSFlexGrid2.Row * Me.MSFlexGrid2.Cols + 1) = Operandes(aValCdt.Operation)
    
    '-> Valeur mini
    Me.MSFlexGrid2.TextArray(Me.MSFlexGrid2.Row * Me.MSFlexGrid2.Cols + 2) = aValCdt.ValMini
    
    '-> Valeur maxi
    Me.MSFlexGrid2.TextArray(Me.MSFlexGrid2.Row * Me.MSFlexGrid2.Cols + 3) = aValCdt.ValMaxi

End Sub


Private Sub Toolbar3_ButtonClick(ByVal Button As MSComctlLib.Button)

    Dim Msg As String
    Dim aLb As Libelle
    
    '-> Charge les libelles
    Set aLb = Libelles("FRMLIG")
    
    '-> Selon le button
    Select Case Button.Key
        Case "ADD" 'Insertion du code d'une ligne dans la formule
            AddRowInFormule
            
        Case "CHECK" 'V�rification de la syntaxe MESSPROG
            If Not CheckFormule(Me.Rtf.Text, Tableaux("TB_" & UCase$(Trim(Me.TbCode)))) Then
                MsgBox aLb.GetCaption(20) & Chr(13) & strError, vbCritical + vbOKOnly, aLb.GetCaption(7)
                Exit Sub
            End If
            
    End Select

    Set aLb = Nothing
    
End Sub

Private Sub AddRowInFormule()

    '---> Cette proc�dure ajoute une ligne dans la formule en cours
    
    Dim MyRow As clsRow
    Dim ToInsert As String
    
    '-> vider la variable d'�change
    strRetour = ""
    
    '-> Afficher la liste des lignes existantes
    Set frmListeCol.aTb = Tableaux("TB_" & Me.TbCode)
    frmListeCol.TypeDisplay = 1
    frmListeCol.ObjectInit = aRow.Code
    frmListeCol.Init
    frmListeCol.Show vbModal
    If strRetour = "" Then Exit Sub
    
    '-> Traiter le retour
    If Entry(1, strRetour, Chr(0)) = "OBJECT" Then
        '-> On a renvoy� le nom d'une colonne
        ToInsert = "$LIG�" & Entry(2, strRetour, Chr(0)) & "$"
    Else
        '-> On a renvoy� un champ et son progiciel
        ToInsert = "$PRO�" & Entry(2, strRetour, Chr(0)) & "�" & Entry(3, strRetour, Chr(0)) & "$"
    End If
    
    '-> Ins�rer dans la colonne
    IsInserting = True
    Me.Rtf.SelColor = RGB(255, 0, 0)
    Me.Rtf.SelText = ToInsert
    IsInserting = False
    Me.Rtf.SetFocus
    
    '-> Vider la variable de retour
    strRetour = ""

End Sub

Private Sub TypeLig_Click(Index As Integer)

    Select Case Index
        Case 0 'Detail
            Me.SSTab1.TabEnabled(1) = True
            Me.SSTab1.TabEnabled(2) = False
        Case 1 'Cumul
            Me.SSTab1.TabEnabled(1) = False
            Me.SSTab1.TabEnabled(2) = True
        Case 2 'S�paration
            Me.SSTab1.TabEnabled(1) = False
            Me.SSTab1.TabEnabled(2) = False
    End Select 'Selon la nature de la ligne
    
    '-> Mettre � jour l'indicateur de type de ligne
    IdTypeLig = Index

End Sub

Private Sub Loadprogi(Optional SelectPro As String)

    '---> Cette proc�dure charge la liste des progiciels autoris�s pour un applicatif
    
    Dim i As Integer
    Dim aTb As clsTableau
    Dim aProgiciel As clsProgiciel
    
    '-> Vider la combo
    Me.Combo1.Clear
    
    '-> Get d'un pointeur vers le tableau
    Set aTb = Tableaux("TB_" & UCase$(Trim(Me.TbCode)))
    
    '-> Charger la liste des progiciels
    For Each aProgiciel In aTb.Progiciels
        '-> Ajouter le progiciel dans la liste
        Me.Combo1.AddItem aProgiciel.Code & "-" & Progiciels("PRO_" & aProgiciel.Code).Libelle
        '-> S�lectionner le progiciel sp�cifi�
        If SelectPro = aProgiciel.Code Then Me.Combo1.ListIndex = Me.Combo1.ListCount - 1
    Next

End Sub


Private Sub DisplayCdtByProgiciel()

    '---> Cette proc�dure affiche toutes les conditions li�es � une ligne et un progiciel
    
    Dim aCdt As clsCondition
    Dim Progiciel As String
    
    '-> Ne rien faire si pas de progiciel de sp�cifi�
    If Me.Combo1.ListIndex = -1 Then Exit Sub
    
    '-> R�cup�rer le code du progiciel sp�cifi�
    Progiciel = GetProgicielString
    
    '-> Virer toutes les lignes des browse
    Me.MSFlexGrid1.Rows = 1
    Me.MSFlexGrid2.Rows = 1
    
    '-> Analyser les conditions d'une ligne
    For Each aCdt In aRow.Conditions
        '-> Tester si on est sur le bon progiciel
        If aCdt.Progiciel = Progiciel Then
            '-> On ajoute dans le browse
            AddCdtInBrowse aCdt.Colonne, aCdt.Progiciel
        End If
    Next 'Pour toutes les conditions de la ligne
    
    '-> S�lectionner la premi�re condition s'il y a en a
    If Me.MSFlexGrid1.Rows <> 1 Then
        Me.MSFlexGrid1.Col = 1
        Me.MSFlexGrid1.Row = 1
        MSFlexGrid1_Click
    End If 'S'il y a des conditions

End Sub

Private Function GetProgicielString() As String

    GetProgicielString = UCase$(Trim(Entry(1, Me.Combo1.List(Me.Combo1.ListIndex), "-")))

End Function
