VERSION 5.00
Begin VB.Form frmAdd 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   1365
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   3780
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1365
   ScaleWidth      =   3780
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox Text3 
      Height          =   285
      Left            =   1080
      TabIndex        =   4
      Top             =   960
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Height          =   375
      Left            =   2640
      Picture         =   "frmAdd.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   960
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   3240
      Picture         =   "frmAdd.frx":038A
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   960
      Width           =   495
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   1080
      TabIndex        =   3
      Top             =   600
      Width           =   2655
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   1080
      TabIndex        =   1
      Top             =   240
      Width           =   2655
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   960
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1095
   End
End
Attribute VB_Name = "frmAdd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Pour gestion des libell�s
Public TypeAdd As Integer
Public ModifField As String

Private Sub Command1_Click()
    
    Dim aLb As Libelle
    
    Set aLb = Libelles("FRMADD")
    
    '-> Tester que le code soit saisi
    If Trim(Me.Text1.Text) = "" Then 'MESSPROG
        MsgBox aLb.GetCaption(1), vbExclamation + vbOKOnly, aLb.GetCaption(2)
        Me.Text1.SetFocus
        Exit Sub
    End If
    
    '-> Si on est en ajout de champ , v�rifier que la zone 3 soit num�rique
    If Me.TypeAdd = 3 Or Me.TypeAdd = 4 Then
        If Not IsNumeric(Me.Text3.Text) Then 'MESSPROG
            MsgBox aLb.GetCaption(3), vbExclamation + vbOKOnly, aLb.GetCaption(2)
            Me.Text3.SetFocus
            Exit Sub
        End If
        '-> V�rifier que l'entr�e soit diff�rente de 0
        If CLng(Me.Text3.Text) = 0 Then 'MESSPROG
            MsgBox aLb.GetCaption(4), vbExclamation + vbOKOnly, aLb.GetCaption(2)
            Me.Text3.SetFocus
            Exit Sub
        End If
        
        '-> Renvoyer les s�lections de l'utilisateur
        strRetour = UCase$(Trim(Me.Text1.Text)) & Chr(0) & Trim(Me.Text2.Text) & Chr(0) & Trim(Me.Text3.Text)
    Else
        '-> Renvoyer les s�lections de l'utilisateur
        strRetour = UCase$(Trim(Me.Text1.Text)) & Chr(0) & Trim(Me.Text2.Text)
    End If
        
    '-> D�charger la feuille
    Unload Me
    
    Set aLb = Nothing
    
End Sub

Private Sub Command2_Click()

    '-> Quitter
    strRetour = ""
    Unload Me

End Sub

Private Sub Form_Load()

    '---> Titre de la fen�tre selon le type d'objet que l'on ajoute
    Dim aLb As Libelle
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMADD")
    
    Select Case TypeAdd 'MESSPROG
        Case 0 'Ajout d'une ligne
            Me.Caption = aLb.GetCaption(5)
        
        Case 1 'Ajout d'un colonne
            Me.Caption = aLb.GetCaption(6)
            
        Case 2 'Ajout d'un tableau
            Me.Caption = aLb.GetCaption(7)
            
        Case 3, 4  'Ajout d'un champ dans uin descriptif
            Me.Label3.Visible = True
            Me.Text3.Visible = True
            Me.Label3.Caption = aLb.GetCaption(8)
            If Me.TypeAdd = 4 Then
                Me.Text1.Text = ModifField
                Me.Text1.Enabled = False
                Me.Caption = aLb.GetCaption(9)
            Else
                Me.Caption = aLb.GetCaption(10)
            End If
    
    End Select
    
    '-> Libell� des labels
    Me.Label1.Caption = aLb.GetCaption(11)
    Me.Label2.Caption = aLb.GetCaption(12)
    
    '-> Tooltip des boutons
    Me.Command1.ToolTipText = aLb.GetCaption(13)
    Me.Command2.ToolTipText = aLb.GetCaption(14)
    
    Set aLb = Nothing

End Sub

Private Sub Text1_GotFocus()
    SelectTxtBox Me.Text1
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)

    Select Case KeyAscii
        Case 45, 61, 124, 167
            KeyAscii = 0
        Case 13
            Me.Text2.SetFocus
    End Select

End Sub

Private Sub Text2_GotFocus()
    SelectTxtBox Me.Text2
End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)

    Select Case KeyAscii
        Case 45, 61, 124, 167
            KeyAscii = 0
        Case 13
            Command1_Click
    End Select

End Sub

Private Sub Text3_GotFocus()
    SelectTxtBox Me.Text3
End Sub

Private Sub Text3_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Command1_Click
End Sub
