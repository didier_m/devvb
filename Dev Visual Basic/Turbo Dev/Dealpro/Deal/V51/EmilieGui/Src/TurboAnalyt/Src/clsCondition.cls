VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCondition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'**************************
'* Gestion des conditions *
'**************************

'-> Code progiciel de rattachement
Public Progiciel As String
'-> Code de la colonne associ�e � cette condition
Public Colonne As String
'-> Collection des valeurs possibles pour cette condition
Public ValueCdts As Collection
'-> Indique la concataination logique entre 2 conditions
Public TypeCdt As Integer '-> 0 ET, 1 OU

Private Sub Class_Initialize()

    '-> Initialiser la collection des valeurs potentielles
    Set ValueCdts = New Collection

End Sub
