VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmOuvrir 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selection d'un tableau a ouvrir"
   ClientHeight    =   1260
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4620
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1260
   ScaleWidth      =   4620
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command3 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2520
      TabIndex        =   5
      Top             =   840
      Width           =   975
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   360
      Top             =   720
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Command1 
      Caption         =   "OK"
      Height          =   375
      Left            =   3600
      TabIndex        =   1
      Top             =   840
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4575
      Begin VB.CommandButton Command2 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   4080
         Picture         =   "frmOuvrir.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1320
         TabIndex        =   2
         Top             =   240
         Width           =   2655
      End
      Begin VB.Label Label1 
         Caption         =   "Fichier tableau"
         ForeColor       =   &H00000000&
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "frmOuvrir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub Command1_Click()
    '-> On fusionne les donn�es tableau avec les donn�es de base du fichier ASCII
    
    Dim aLb As Libelle
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMOUVRIR")
    
    If Me.Text1.Text = "" Then
        MsgBox aLb.GetCaption(1), vbExclamation
        Exit Sub
    End If
    
    '-> On retourne le nom du tableau a ouvrir
    strRetour = Trim(Me.Text1.Text)
    
    Set aLb = Nothing
    
    Unload Me
    
End Sub

Private Sub Command2_Click()
    '-> Recherche du fichier tableau a ouvrir
    
    Dim aTb As clsTableau
    Dim aLb As Libelle
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMOUVRIR")
    
    '-> Ouverture � partir d'un fichier ASCII MESSPROG
    Me.CommonDialog1.DialogTitle = aLb.GetCaption(2)
    Me.CommonDialog1.FileName = ""
    Me.CommonDialog1.Filter = aLb.GetCaption(3)
    Me.CommonDialog1.ShowOpen
    
    
    '-> V�rifier que le fichier sp�cifi� ne soit pas d�ja ouvert
    For Each aTb In Tableaux
        If UCase$(aTb.FileName) = UCase$(Me.CommonDialog1.FileName) Then 'MESSPROG
            MsgBox aLb.GetCaption(4), vbExclamation + vbOKOnly, aLb.GetCaption(5)
            Exit Sub
        End If
    Next
    
    strRetour = Me.CommonDialog1.FileName
    Me.Text1.Text = Me.CommonDialog1.FileName
    Set aLb = Nothing
    
End Sub

Private Sub Command3_Click()
    '-> On ferme
    Unload Me
End Sub
