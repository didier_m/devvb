VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmParamXL 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   3900
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   3420
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3900
   ScaleWidth      =   3420
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame3 
      Caption         =   "Cellule"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   0
      TabIndex        =   9
      Top             =   2640
      Width           =   3375
      Begin VB.TextBox txtCellule 
         Height          =   285
         Left            =   480
         TabIndex        =   12
         Top             =   480
         Width           =   2655
      End
      Begin VB.Label Label4 
         Caption         =   "R�f�rence d'une cellule de d�part"
         Height          =   255
         Left            =   600
         TabIndex        =   13
         Top             =   240
         Width           =   2415
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Classeur"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   3375
      Begin VB.TextBox txtClasseur 
         Enabled         =   0   'False
         Height          =   285
         Left            =   120
         TabIndex        =   7
         Top             =   480
         Width           =   2655
      End
      Begin VB.CommandButton cmdFiles 
         Height          =   255
         Left            =   2880
         Picture         =   "frmParamXL.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   480
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "Inserer dans un classeur existant "
         Height          =   255
         Left            =   360
         TabIndex        =   8
         Top             =   240
         Width           =   2775
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Feuille"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   0
      TabIndex        =   2
      Top             =   960
      Width           =   3375
      Begin VB.TextBox txtNew 
         Height          =   285
         Left            =   360
         TabIndex        =   10
         Top             =   1200
         Width           =   2655
      End
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   3000
         Top             =   960
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.ComboBox cmbFeuilles 
         Enabled         =   0   'False
         Height          =   315
         Left            =   360
         TabIndex        =   3
         Top             =   480
         Width           =   2655
      End
      Begin VB.Label Label3 
         Caption         =   "Saisie d'une nouvelle feuille "
         Height          =   255
         Left            =   720
         TabIndex        =   11
         Top             =   960
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "Feuilles existantes dans le classeur "
         Height          =   255
         Left            =   415
         TabIndex        =   4
         Top             =   240
         Width           =   2535
      End
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Height          =   255
      Left            =   2280
      TabIndex        =   1
      Top             =   3600
      Width           =   975
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Annuler"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   3600
      Width           =   975
   End
End
Attribute VB_Name = "frmParamXL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub cmdCancel_Click()
    '--> On sort du parametrage
    strRetour = "N"
    Me.Hide
End Sub

Private Sub cmdFiles_Click()
    Dim aLb As Libelle

On Error GoTo GestError
    
    '---> Selection d'un classeur existant
    Dim wrkExcel As Excel.Application
    Dim wrkClasseur As Excel.Workbook
    Dim wrkFeuille As Excel.Worksheet
    
    '-> Chargement des messprog
    Set aLb = Libelles("FRMPARAMXL")
    
    '-> Ouverture � partir d'un fichier ASCII MESSPROG
    Me.CommonDialog1.DialogTitle = aLb.GetCaption(3)
    Me.CommonDialog1.FileName = ""
    Me.CommonDialog1.Filter = aLb.GetCaption(4)
    Me.CommonDialog1.ShowOpen
        
    '-> On recupere le classeur selectionn�
    If Me.CommonDialog1.FileName <> "" Then
        Me.txtClasseur.Text = Me.CommonDialog1.FileName
        
        '-> On remplit le combo avec les feuilles du classeur
        Set wrkExcel = New Excel.Application
        
        '-> On declare un nouveau classeur dans excel
        Set wrkClasseur = wrkExcel.Workbooks.Open(Me.CommonDialog1.FileName)
        
        '-> On parcours les feuilles pour les recenser dans le combo
        For Each wrkFeuille In wrkClasseur.Sheets
            Me.cmbFeuilles.AddItem wrkFeuille.Name
        Next
        '-> On se positionne sur la premiere feuille
        If Me.cmbFeuilles.ListCount <> 0 Then
            Me.cmbFeuilles.Enabled = True
            Me.cmbFeuilles.ListIndex = 0
        End If
    End If
    
GestError:
    
    '-> Quitter Excel
    If Not wrkExcel Is Nothing Then wrkExcel.Quit
    Set wrkExcel = Nothing
    Set wrkClasseur = Nothing
    Set wrkFeuille = Nothing
    Set aLb = Nothing
    
End Sub

Private Sub cmdOk_Click()
    '---> On retourne les valeurs de parametrage saisie
    Dim RefCol As String
    Dim RefLig As String
    Dim CellOk As Boolean
    Dim aLb As Libelle
    
    '-> On charge les messprog
    Set aLb = Libelles("FRMPARAMXL")
    
    Me.Caption = aLb.GetCaption(1)
    
    '-> teste si reference cellule est correcte
    If Me.txtCellule.Text <> "" Then
        CellOk = True
        Me.txtCellule.Text = UCase$(Me.txtCellule.Text)
        RefCol = Mid$(Me.txtCellule.Text, 1, 1)
        RefLig = Mid$(Me.txtCellule.Text, 2)
        
        '- teste que l'on ait bien une lettre entre "A" et "Z" pour la reference de colonne
        If RefCol > "Z" And RefCol < "A" Then CellOk = False
        '-> teste que l'on ait bien un chiffre en reference ligne
        If Not IsNumeric(RefLig) Then CellOk = False
        
        If CellOk = False Then
            MsgBox aLb.GetCaption(2)
            Me.txtCellule.SelStart = 0
            Me.txtCellule.SelLength = Len(Me.txtCellule.Text)
            Me.txtCellule.SetFocus
            Exit Sub
        End If
        
    End If
    
    strRetour = Trim(Me.txtClasseur.Text) & "|" & Trim(Me.cmbFeuilles.Text) & "|" & Trim(Me.txtCellule.Text) & "|" & Trim(Me.txtNew.Text)
  
    Me.Hide
    Set aLb = Nothing
End Sub


Private Sub Form_Unload(Cancel As Integer)
    strRetour = "N"
End Sub
