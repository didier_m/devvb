VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmParam 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   1410
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   3870
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   94
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   258
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox Check1 
      Caption         =   "Exporter la cl� progiciel"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   960
      Value           =   1  'Checked
      Width           =   2415
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   3360
      Picture         =   "frmParam.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   960
      Width           =   495
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Height          =   375
      Left            =   2760
      Picture         =   "frmParam.frx":0442
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   960
      Width           =   495
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   3960
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   3735
   End
   Begin VB.Label Label1 
      Caption         =   "Choix du progiciel :"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   3375
   End
End
Attribute VB_Name = "frmParam"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Pointeur vers le tableau
Public aTb As clsTableau

Private Sub Command1_Click()

    Dim aProgiciel As clsProgiciel
    Dim aDes As clsDescriptif
    Dim Rep As VbMsgBoxResult
    Dim HdlFile As Integer
    Dim strTri As String
    Dim strNonTri As String
    Dim i As Integer
    Dim aLb As Libelle
    
    On Error GoTo EndError
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMPARAM")
    
    '-> V�rifier qu'un progiciel est affect�
    If Me.Combo1.ListIndex = -1 Then 'MESSPROG
        MsgBox aLb.GetCaption(1), vbExclamation + vbOKOnly, aLb.GetCaption(2)
        Me.Combo1.SetFocus
        Exit Sub
    End If
    
    '-> Afficher le choix du fichier MESSPROG
    Me.CommonDialog1.DialogTitle = aLb.GetCaption(3)
    Me.CommonDialog1.CancelError = True
    Me.CommonDialog1.Filter = aLb.GetCaption(4)
    Me.CommonDialog1.ShowSave
    
    '-> V�rifier que le fichier sp�cifi� n'existe pas d�ja
    If Dir$(Me.CommonDialog1.FileName, vbNormal) <> "" Then   'MESSPROG
        Rep = MsgBox(aLb.GetCaption(5), vbQuestion + vbYesNo, aLb.GetCaption(6))
        If Rep = vbNo Then Exit Sub
    End If
            
    '-> R�cup�rer le progiciel s�lection
    Set aProgiciel = aTb.Progiciels(Me.Combo1.ListIndex + 1)
    
    '-> V�rifier qu'il y a un descriptif � exporter
    If aProgiciel.Descriptifs.Count = 0 Then  'MESSPROG
        MsgBox aLb.GetCaption(7), vbExclamation + vbOKOnly, aLb.GetCaption(8)
        Exit Sub
    End If
    
    '-> Obtenir un handle vers un fichier
    HdlFile = FreeFile
    Open Me.CommonDialog1.FileName For Output As #HdlFile
    
    '-> Export de la cl� progiciel
    If Me.Check1.Value Then Print #HdlFile, "%%PARAMANALYT%%" & UCase$(Trim(aProgiciel.Code))
    
    '-> Trier son descriptif
    For Each aDes In aProgiciel.Descriptifs
        If strNonTri = "" Then
            strNonTri = Format(aDes.Entree, "000000000") & "�" & aDes.Field
        Else
            strNonTri = strNonTri & "|" & Format(aDes.Entree, "000000000") & "�" & aDes.Field
        End If
    Next
    strTri = Tri(strNonTri, "|", "�")
    
    '-> Imprimer le descro
    For i = 1 To NumEntries(strTri, "|")
        '-> Pointer usr le descriptif
        Set aDes = aProgiciel.Descriptifs("FIELD_" & Entry(i, strTri, "|"))
        '-> Imprimer le descriptif MESSPROG
        Print #HdlFile, "Field : " & Format(aDes.Field, "@@@@@@@@@@@@@@@@@@@@@@@@@") & " Libell� : " & Format(aDes.Commentaire, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@") & " Entr�e : " & aDes.Entree
    Next 'Pour tous les descriptifs
    
    '-> Fermer le fichier
    Close #HdlFile
    
    '-> Afficher un message MESSPROG
    MsgBox aLb.GetCaption(9), vbInformation + vbOKOnly, aLb.GetCaption(10)
    
    '-> On libere le pointeur
    Set aLb = Nothing
    
    Exit Sub
    
EndError:


End Sub

Private Sub Command2_Click()
    Unload Me
End Sub

Private Sub Form_Load()

    Dim aProgiciel As clsProgiciel
    
    '-> Titre de la feuille MESSPROG
    Me.Caption = "Export du param�trage progiciel"
    
    '-> Charger la liste des progiciels affect�s � ce tableau
    For Each aProgiciel In aTb.Progiciels
        Me.Combo1.AddItem aProgiciel.Code & " - " & Progiciels("PRO_" & aProgiciel.Code).Libelle
    Next 'Pour tous les progiciels

End Sub

