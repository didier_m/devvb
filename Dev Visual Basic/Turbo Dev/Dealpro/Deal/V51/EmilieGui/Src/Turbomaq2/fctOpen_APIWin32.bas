Attribute VB_Name = "APIWin32"
'-> Structures pour GDI
Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

'-> API pour gestion d'une fen�tre
Public Declare Function GetClientRect& Lib "user32" (ByVal hwnd As Long, lpRect As RECT)

'-> Afficher la liste des disques connect�s
Public Declare Function GetLogicalDriveStrings& Lib "kernel32" Alias "GetLogicalDriveStringsA" (ByVal nBufferLength As Long, ByVal lpBuffer As String)
'-> R�cup�re le type d'un lecteur
Public Declare Function GetDriveType& Lib "kernel32" Alias "GetDriveTypeA" (ByVal nDrive As String)
'-> Constante de type de lecteur
Public Const DRIVE_CDROM& = 5
Public Const DRIVE_FIXED& = 3
Public Const DRIVE_REMOTE& = 4
Public Const DRIVE_REMOVABLE& = 2

'-> R�cup�rer des informations sur un disque
Public Declare Function GetVolumeInformation& Lib "kernel32" Alias "GetVolumeInformationA" (ByVal lpRootPathName As String, ByVal lpVolumeNameBuffer As String, ByVal nVolumeNameSize As Long, lpVolumeSerialNumber As Long, lpMaximumComponentLength As Long, lpFileSystemFlags As Long, ByVal lpFileSystemNameBuffer As String, ByVal nFileSystemNameSize As Long)
Public Declare Function GetDiskFreeSpace& Lib "kernel32" Alias "GetDiskFreeSpaceA" (ByVal lpRootPathName As String, lpSectorsPerCluster As Long, lpBytesPerSector As Long, lpNumberOfFreeClusters As Long, lpTotalNumberOfClusters As Long)

'-> API pour affichage d'une page de propri�t�s
Private Type SHELLEXECUTEINFO
             cbSize As Long
             fMask As Long
             hwnd As Long
             lpVerb As String
             lpFile As String
             lpParameters As String
             lpDirectory As String
             nShow As Long
             hInstApp As Long
             ' Optional fields
             lpIDList As Long
             lpClass As String
             hkeyClass As Long
             dwHotKey As Long
             hIcon As Long
             hProcess As Long
   End Type
   
'---> Gestion de l'envoie de mail
Private Declare Function ShellExecuteEx Lib "shell32" (lpSEI As SHELLEXECUTEINFO) As Long
Private Const SEE_MASK_INVOKEIDLIST = &HC
Private Const SW_SHOW = 5



Public Sub ShowFileProperties(ByVal ps_FileName As String)
   
         Dim lu_ShellExUDT As SHELLEXECUTEINFO
         
         With lu_ShellExUDT
             .hwnd = hwnd
             .lpVerb = "properties"
             .lpFile = ps_FileName
             .fMask = SEE_MASK_INVOKEIDLIST
             .cbSize = Len(lu_ShellExUDT)
         End With
         
         Call ShellExecuteEx(lu_ShellExUDT)
   
   End Sub

