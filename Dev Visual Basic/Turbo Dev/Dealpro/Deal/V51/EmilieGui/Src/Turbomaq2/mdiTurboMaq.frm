VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm mdiTurboMaq 
   BackColor       =   &H8000000C&
   Caption         =   "TurboMaq Version 2.0"
   ClientHeight    =   7380
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   8160
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picSplit 
      Align           =   3  'Align Left
      BorderStyle     =   0  'None
      Height          =   7380
      Left            =   2880
      MousePointer    =   9  'Size W E
      ScaleHeight     =   7380
      ScaleWidth      =   105
      TabIndex        =   3
      Top             =   0
      Width           =   105
   End
   Begin VB.PictureBox picNaviga 
      Align           =   3  'Align Left
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   7380
      Left            =   0
      ScaleHeight     =   492
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   192
      TabIndex        =   0
      Top             =   0
      Width           =   2880
      Begin VB.PictureBox picTitre 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         Height          =   90
         Left            =   240
         ScaleHeight     =   6
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   128
         TabIndex        =   2
         Top             =   840
         Width           =   1920
      End
      Begin MSComctlLib.TreeView TreeNaviga 
         Height          =   1455
         Left            =   240
         TabIndex        =   1
         Top             =   1440
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   2566
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin VB.Image imgClose 
         Height          =   165
         Left            =   2400
         Picture         =   "mdiTurboMaq.frx":0000
         Top             =   240
         Width           =   165
      End
   End
   Begin VB.Menu mnuFichier 
      Caption         =   "Fichier"
      Begin VB.Menu mnuExit 
         Caption         =   "Quitter"
      End
   End
   Begin VB.Menu mnuFenetre 
      Caption         =   "Fen�tre"
      Begin VB.Menu mnuStructure 
         Caption         =   "Fen�tre de structure"
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
   End
End
Attribute VB_Name = "mdiTurboMaq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---> Cette variable indique si la feuille de navigation est active
Private IsNaviga As Boolean

Private Sub imgClose_Click()

Call mnuStructure_Click

End Sub

Private Sub MDIForm_Load()

'-> Au demarrage, la fen�tre de structure est pr�sente
IsNaviga = True
Me.mnuStructure.Checked = True

End Sub

Private Sub mnuExit_Click()
Form1.Show
End Sub

Private Sub mnuStructure_Click()

'-> Afficher ou ne pas afficher le volet expmloration de spool
IsNaviga = Not IsNaviga
Me.mnuStructure.Checked = IsNaviga
Me.picSplit.Visible = IsNaviga
Me.picNaviga.Visible = IsNaviga

End Sub

Private Sub picSplit_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

Dim apt As POINTAPI
Dim Res As Long

If Button = vbLeftButton Then
    '-> R�cup�rer la position du curseur
    Res = GetCursorPos(apt)
    '-> Convertir en coordonn�es clientes
    Res = ScreenToClient(Me.hwnd, apt)
    '-> Tester les positions mini et maxi
    If apt.X > Me.picNaviga.ScaleX(2, 7, 3) And apt.X < Me.picNaviga.ScaleX(Me.Width, 1, 3) - Me.picNaviga.ScaleX(2, 7, 3) Then Me.picNaviga.Width = Me.picNaviga.ScaleX(apt.X, 3, 1)
End If

End Sub

Private Sub picTitre_Resize()

'---> Dessiner la barre de titre
Me.picTitre.Line (0, 0)-(Me.picTitre.Width - 1, 0), RGB(255, 255, 255)
Me.picTitre.Line (0, 1)-(Me.picTitre.Width - 1, 1), RGB(255, 255, 255)
Me.picTitre.Line (1, 1)-(Me.picTitre.Width - 1, 1), &HE0E0E0
Me.picTitre.Line (0, 3)-(Me.picTitre.Width - 1, 3), RGB(255, 255, 255)
Me.picTitre.Line (0, 4)-(Me.picTitre.Width - 1, 4), RGB(255, 255, 255)
Me.picTitre.Line (1, 4)-(Me.picTitre.Width - 1, 4), &HE0E0E0

End Sub

Private Sub picNaviga_Resize()

Dim aRect As RECT
Dim Res As Long

On Error Resume Next

'-> R�cup�rer la taille de la zone cliente
Res = GetClientRect(Me.picNaviga.hwnd, aRect)

'-> Positionner l'entete de la fen�tre
Me.picTitre.Left = 3
Me.picTitre.Top = 7
Me.picTitre.Width = aRect.Right - 6 - imgClose.Width
Me.imgClose.Left = aRect.Right - imgClose.Width
Me.imgClose.Top = Me.picTitre.Top - 2

'-> Positionner le treeview
Me.TreeNaviga.Left = Me.picTitre.Left
Me.TreeNaviga.Top = Me.picTitre.Top * 2 + Me.picTitre.Height
Me.TreeNaviga.Width = Me.picTitre.Width + Me.imgClose.Width + 6
Me.TreeNaviga.Height = Me.picNaviga.ScaleY(Me.picNaviga.Height, 1, 3) - 21

End Sub

