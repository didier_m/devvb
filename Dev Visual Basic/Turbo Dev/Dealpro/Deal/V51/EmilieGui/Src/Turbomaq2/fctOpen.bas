Attribute VB_Name = "fctOpen"
Option Explicit

'-> Positionner sur 1 si on est en affichage de disque
Public NiveauDisk As Boolean

'-> Indique le path du r�pertoire Turbo
Public TurboPathIni As String
Public Lecteur As String
Public Ident As String
Public Version As String

'-> Collection des clients
Public Clients As Collection

'-> Collection des progiciels
Public Progiciels As Collection

Public Sub DisplayFileObject(aPath As String, aNode As Node)

Dim FileName As String
Dim ToAdd As Boolean
Dim aNode2 As Node
Dim IsFolder As Boolean
Dim i As Integer
Dim Extension As String
Dim iTemX As ListItem
Dim MyDate As Date
Dim FormatDate As String
Dim NomFichier As String

On Error GoTo GestError

frmOpen.Enabled = False
frmOpen.MousePointer = 11

'-> Analyse de tous les fichiers
FileName = Dir$(aPath & "*.*", vbDirectory Or vbNormal)

'-> Vider le listview
frmOpen.ListView3.ListItems.Clear

'-> V�rifier si on doit cr�er les nodes de sous  niveaux
If aNode.Children = 0 Then
    ToAdd = True
Else
    ToAdd = False
End If

Do While FileName <> ""
    '-> Ne pas afficher le command.com
    If FileName = "." Or FileName = ".." Then GoTo NextDir
    '-> Tester si c'est un dossier ou un fichier
    If (GetAttr(aPath & FileName) And vbDirectory) = vbDirectory Then
        '-> Indiquer que c'est un r�pertoire
        IsFolder = True
        '-> C'est un dossier donc ajouter dans le listview avec la bonne icone
        frmOpen.ListView3.ListItems.Add , "FOLDER|" & aPath & FileName, FileName, "FOLDER", "FOLDER"
    Else
        '-> Indiquer que ce n'est pas un r�pertoire
        IsFolder = False
        '-> Tester que le fichier soit de type Maquette graphique
        i = InStrRev(FileName, ".")
        Extension = Mid$(FileName, i + 1, Len(FileName) - i)
        NomFichier = Mid$(FileName, 1, i - 1)
        If UCase$(Trim(Extension)) = "MAQGUI" Then
            '-> On ajoute
            Set iTemX = frmOpen.ListView3.ListItems.Add(, "FILE|" & aPath & FileName, NomFichier, "MAQ", "MAQ")
            '-> On ajoute pour la partie d�tail
            iTemX.SubItems(frmOpen.ListView3.ColumnHeaders("TYPE").SubItemIndex) = "Maquette Turbo"
            iTemX.SubItems(frmOpen.ListView3.ColumnHeaders("SIZE").SubItemIndex) = Format(FileLen(aPath & FileName) / 1024, "#.00 Ko")
            iTemX.SubItems(frmOpen.ListView3.ColumnHeaders("SORTSIZE").SubItemIndex) = Format(FileLen(aPath & FileName), "0000000000000000000000000000.00")
            MyDate = FileDateTime(aPath & FileName)
            FormatDate = Format(Year(MyDate), "0000") & _
                         Format(Month(MyDate), "00") & _
                         Format(Day(MyDate), "00") & _
                         Format(Hour(MyDate), "00") & _
                         Format(Minute(MyDate), "00") & _
                         Format(Second(MyDate), "00")
            iTemX.SubItems(frmOpen.ListView3.ColumnHeaders("DATE").SubItemIndex) = FileDateTime(aPath & FileName)
            iTemX.SubItems(frmOpen.ListView3.ColumnHeaders("SORTDATE").SubItemIndex) = Format(FormatDate, "0000000000000000000000000000")
        End If
    End If
    '-> Tester si on doit afficher dans le treeview
    If ToAdd Then
        '-> N'ajouter que si c'est un folder
        If Not IsFolder Then GoTo NextDir
        Set aNode2 = frmOpen.TreeView1.Nodes.Add(aNode.Key, 4, "FOLDER|" & aPath & FileName, FileName, "FOLDER")
        aNode2.ExpandedImage = "OPENFOLDER"
        aNode2.Tag = "FOLDER"
    End If
    
NextDir:
    '-> Analyser lz prochaine entr�e
    FileName = Dir
Loop

'-> Arranger la vue
frmOpen.ListView3.Arrange = lvwAutoTop

If aNode.Key = "POSTE" Then
    NiveauDisk = True
Else
    NiveauDisk = False
End If

'-> Indiquer que l'on n'est plus au niveau disque
If aNode.Parent.Key <> "POSTE" Then
    frmOpen.Label4.Caption = Mid$(aPath, 1, Len(aPath) - 1)
Else
    frmOpen.Label4.Caption = aPath
End If



'-> Quitter la proc�dure
GoTo EndProg

GestError:
    Select Case Err.Number
        Case 52 'Nom au lecteur incorrecte
            MsgBox "Impossible d'acc�der � ce disque ou r�pertoire.", vbCritical + vbOKOnly, "Erreur"
    End Select
    
EndProg:

    frmOpen.Enabled = True
    frmOpen.MousePointer = 0


End Sub

Public Sub LoadDrive(ToTreeView As Boolean)


'-> Charger dans le tree la le poste de travail et les disques connect�s
Dim Res As Long
Dim lpBuffer As String
Dim i As Integer

Dim aDisk As String
Dim TypeDrive As Long
Dim DiskText As String

Dim Image As String
Dim aNode As Node

Dim VolumeName As String
Dim SerialNumber As Long
Dim ComponentLenght As Long
Dim SystemFlags As Long
Dim SystemNameBuffer As String
Dim iTemX As ListItem
Dim TypeItem As String

Dim BytesParSecteur As Long
Dim SecteurByClust As Long
Dim FreeClust As Long
Dim NbClust As Long
Dim FreeBytes As Double



'-> Vider le listView de ses icones
frmOpen.ListView3.ListItems.Clear

'-> Charger la liste des disques connect�s
lpBuffer = Space$(2000)
Res = GetLogicalDriveStrings&(Len(lpBuffer), lpBuffer)
lpBuffer = Mid$(lpBuffer, 1, Res)

If ToTreeView Then
    '-> Ajouter l'icone du poste de travail
    Set aNode = frmOpen.TreeView1.Nodes.Add(, , "POSTE", "Poste de travail", "POSTE")
    aNode.Tag = "POSTE"
    aNode.Expanded = True
    aNode.Selected = True
End If

'-> Analyser la liste des diques connect�s
For i = 1 To NumEntries(lpBuffer, Chr(0))
    '-> D�finition du disque
    aDisk = Entry(i, lpBuffer, Chr(0))
    If Trim(aDisk) = "" Then Exit For
    '-> R�cup�ration du type de lecteur
    TypeDrive = GetDriveType(aDisk)
    '-> Recup�rer les informations sur le type de disque
    VolumeName = Space$(255)
    SystemNameBuffer = Space$(250)
    Res = GetVolumeInformation(aDisk, VolumeName, Len(VolumeName), SerialNumber, ComponentLenght, SystemFlags, SystemNameBuffer, Len(SystemNameBuffer))
    '-> R�cup�rer la taille du disque
    Res = GetDiskFreeSpace(aDisk, SecteurByClust, BytesParSecteur, FreeClust, NbClust)
    FreeBytes = CLng(SecteurByClust) * CLng(NbClust)
    FreeBytes = FreeBytes * CLng(BytesParSecteur)
    '-> tronquer l 'anti slash de fin
    If Right$(aDisk, 1) = "\" Then aDisk = Mid$(aDisk, 1, Len(aDisk) - 1)
    '-> Afficher la bonne icone
    Select Case TypeDrive
        Case DRIVE_CDROM 'CDrom
            Image = "CDROM"
            DiskText = "Lecteur CD Rom ("
            TypeItem = "Lecteur CD Rom"
        Case DRIVE_FIXED 'disque dur
            Image = "DDUR"
            DiskText = "Disque local ("
            TypeItem = "Disque local"
        Case DRIVE_REMOTE 'Disque r�seau
            Image = "DNET"
            DiskText = "Disque r�seau ("
            TypeItem = "Disque r�seau"
        Case DRIVE_REMOVABLE 'Disquette
            Image = "DISQUETTE"
            DiskText = "Lecteur de disquette ("
            TypeItem = "Lecteur de disquette"
    End Select
    '-> Afficher le nom du disque s'il est renseign�
    If Trim(Entry(1, VolumeName, Chr(0))) <> "" Then DiskText = Trim(Entry(1, VolumeName, Chr(0))) & " ("
    
    '-> AJouter le node dans le treeview
    If ToTreeView Then
        Set aNode = frmOpen.TreeView1.Nodes.Add("POSTE", 4, "FOLDER|" & aDisk, DiskText & aDisk & ")", Image)
        aNode.Tag = "FOLDER"
        aNode.Sorted = True
        
    End If
    
    '-> Ajouter l'objet dans le listview
    Set iTemX = frmOpen.ListView3.ListItems.Add(, "FOLDER|" & aDisk, DiskText & aDisk & ")", Image, Image)
    '-> Ajouter son descriptif dans la colonne qui va bien
    iTemX.SubItems(frmOpen.ListView3.ColumnHeaders("TYPE").SubItemIndex) = TypeItem
    iTemX.SubItems(frmOpen.ListView3.ColumnHeaders("SIZE").SubItemIndex) = Format((FreeBytes / 1073741824), "#.00") & " Go"  'Expressionen G�
     
Next

'-> R�organiser les icones
frmOpen.ListView3.Arrange = lvwAutoTop

'-> Indiquer le r�pertoire en cours
frmOpen.Label4.Caption = "Poste de travail"

End Sub

Public Sub GetListApp()

'---> Cette proc�dure affiche la liste des applicatifs ainsi que des clients contenus dans le Turbomaq.ini

Dim Res As Long
Dim ListApp As String
Dim OneApp As String
Dim LibelApp As String
Dim RubApp As String
Dim PathApp As String
Dim lpBuffer As String
Dim SectionName As String
Dim ClientBuffer As String
Dim ParamBuffer As String

Dim i As Integer, j As Integer
Dim iTemX As ListItem
Dim aClient As Client
Dim aProgiciel As Progiciel

On Error Resume Next

'-> R�cup�rer la liste des applicatifs de deal
ListApp = Space$(20000)
Res = GetPrivateProfileString("PROGICIELS", "APP", "", ListApp, Len(ListApp), TurboPathIni & "\Turbomaq.ini")
If Res = 0 Then
    '-> On ne trouve pas la liste des logiciels
    frmOpen.SSTab1.TabEnabled(0) = False
    Exit Sub
End If
ListApp = Mid$(ListApp, 1, Res)

'-> Analyse de la liste des applicatifs
For i = 1 To NumEntries(ListApp, "|")
    '-> Recup du nom de l'applicatif
    OneApp = Entry(i, ListApp, "|")
    '-> R�cup�rer son lib�ll�
    LibelApp = Space$(255)
    Res = GetPrivateProfileString(OneApp, "LIBEL", "", LibelApp, Len(LibelApp), TurboPathIni & "\turbomaq.ini")
    If Res = 0 Then
        LibelApp = "Sans Libell�"
    Else
        LibelApp = Mid$(LibelApp, 1, Res)
    End If
    '-> R�cup�rer la liste de ses rubriques
    RubApp = Space$(2000)
    Res = GetPrivateProfileString(OneApp, "Rubriques-01", "", RubApp, Len(RubApp), TurboPathIni & "\turbomaq.ini")
    If Res = 0 Then
        RubApp = ""
    Else
        RubApp = Mid$(RubApp, 1, Res)
    End If
    '-> R�cup�rer le path
    PathApp = Space$(250)
    Res = GetPrivateProfileString(OneApp, "Path", "", PathApp, Len(PathApp), TurboPathIni & "\turbomaq.ini")
    If Res = 0 Then
        PathApp = ""
    Else
        '-> Replace par les valeurs
        PathApp = Mid$(PathApp, 1, Res)
        PathApp = Replace(UCase$(PathApp), "$LECTEUR$", Lecteur)
        PathApp = Replace(UCase$(PathApp), "$VERSION$", Version)
        PathApp = Replace(UCase$(PathApp), "$IDENT$", Ident)
    End If
    '-> Ajouter dans le choix des clients
    Set iTemX = frmOpen.ListView2.ListItems.Add(, PathApp, OneApp & Chr(13) & LibelApp, "DEAL")
    iTemX.Tag = RubApp
    '-> Cr�er un objet progiciel
    Set aProgiciel = New Progiciel
    aProgiciel.Name = Trim(OneApp)
    aProgiciel.Libel = LibelApp
    aProgiciel.Rubriques = RubApp
    '-> Ajout dans la collection
    Progiciels.Add aProgiciel, "PROGICIEL|" & UCase$(Trim(OneApp))
Next 'Pour tous les applicatifs de deal



'-> On ajoute maintenant la liste des clients
lpBuffer = Space$(20000)
Res = GetPrivateProfileString(vbNullString, "", "", lpBuffer, Len(lpBuffer), TurboPathIni & "\turbomaq.ini")
If Res = 0 Then Exit Sub

'-> Analyse de la liste des clients
For i = 1 To NumEntries(lpBuffer, Chr(0))
    '-> Get du nom de la section
    SectionName = Entry(i, lpBuffer, Chr(0))
    If Trim(SectionName) = "" Then Exit For
    '-> Selon le type de section
    Select Case Trim(UCase$(SectionName))
        Case "PARAM", "PROGICIELS"
        Case Else
            '-> V�rifier que ce n'est pas un applicatif
            If IsAppDeal(SectionName, ListApp) Then GoTo NextSection
            '-> Cr�er un nouvel Applicatif
            Set aClient = New Client
            '-> Ident du client
            aClient.Ident = Trim(UCase$(SectionName))
            '-> Libel du client
            ClientBuffer = Space$(2000)
            Res = GetPrivateProfileString(SectionName, "LIBEL", "", ClientBuffer, Len(ClientBuffer), TurboPathIni & "\turbomaq.ini")
            If Res <> 0 Then aClient.Libel = Mid$(ClientBuffer, 1, Res)
            '-> Path du client
            ClientBuffer = Space$(2000)
            Res = GetPrivateProfileString(SectionName, "PATH", "", ClientBuffer, Len(ClientBuffer), TurboPathIni & "\turbomaq.ini")
            If Res <> 0 Then
                aClient.Path = Mid$(ClientBuffer, 1, Res)
                aClient.Path = Replace(UCase$(aClient.Path), "$LECTEUR$", Lecteur)
                aClient.Path = Replace(UCase$(aClient.Path), "$IDENT$", SectionName)
                aClient.Path = Replace(UCase$(aClient.Path), "$VERSION$", Version)
            End If

            '-> Liste des applicatifs
            ClientBuffer = Space$(2000)
            Res = GetPrivateProfileString(SectionName, "PROGICIELS", "", ClientBuffer, Len(ClientBuffer), TurboPathIni & "\turbomaq.ini")
            If Res <> 0 Then
                '-> Faire le replace
                ClientBuffer = Mid$(ClientBuffer, 1, Res)
                ClientBuffer = Replace(UCase$(ClientBuffer), "$LECTEUR$", Lecteur)
                ClientBuffer = Replace(UCase$(ClientBuffer), "$IDENT$", SectionName)
                ClientBuffer = Replace(UCase$(ClientBuffer), "$VERSION$", Version)
                '-> V�rifier si on trouve le fichier ini
                If Dir$(ClientBuffer, vbNormal) <> "" Then
                    PathApp = Space$(2000)
                    Res = GetPrivateProfileString("LOGICAL", "$PROGICIEL$", "", PathApp, Len(PathApp), ClientBuffer)
                    If Res <> 0 Then
                        PathApp = Mid$(PathApp, 1, Res)
                        '-> Ajout de la liste des logiciels
                        aClient.Progiciel = PathApp
                    End If
                End If 'Si on trouve le fichier param.ini
            End If
            '-> Ajouter la classe dans la collection
            Clients.Add aClient, "CLIENT|" & aClient.Ident
            
            '-> Ajouter l'icone dans le listeview
            frmOpen.ListView4.ListItems.Add , "CLIENT|" & aClient.Ident, aClient.Ident & Chr(13) & aClient.Libel, "CLIENT"

    End Select 'Selon la section
NextSection:
Next '-> Pour toutes les sections dans le turbomaq

End Sub

Public Sub InitVar()

'---> Cette proc�dure analyse le path de l'applicatif pour donn�r une valeur aux variables _
Lecteur , Ident , Version ,

Dim aPath As String
Dim DirName As String
Dim i As Integer
Dim j As Integer

'-> Get du path de l'application
aPath = App.Path

'-> analyse � la recherche de la cl� EMILIEGUI
For i = 1 To NumEntries(aPath, "\")
    DirName = Entry(i, aPath, "\")
    If UCase$(DirName) = "EMILIEGUI" Then
        '-> Version avant EMILIGUI
        Version = Entry(i - 1, aPath, "\")
        '-> Ident avant VERSION
        Ident = Entry(i - 2, aPath, "\")
        Exit For
    ElseIf UCase$(DirName) = "DEALPRO" Then
        j = InStr(1, UCase$(App.Path), "DEALPRO")
        If j <> 0 Then
            '-> Lecteur en pr�mi�re entr�e
            Lecteur = Mid$(App.Path, 1, j - 2)
        End If
    End If
Next

End Sub

Private Function IsAppDeal(NameToCheck As String, ListApp As String) As Boolean

'---> Cette proc�dure

Dim i As Integer

For i = 1 To NumEntries(ListApp, "|")
    If UCase$(Trim(NameToCheck)) = UCase$(Trim(Entry(i, ListApp, "|"))) Then
        IsAppDeal = True
        Exit Function
    End If
Next

'-> Renvoyer une valeur de non
IsAppDeal = False


End Function
