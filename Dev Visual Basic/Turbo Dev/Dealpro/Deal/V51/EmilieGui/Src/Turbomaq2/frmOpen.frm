VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmOpen 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ouverture d'une maquette"
   ClientHeight    =   7860
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12030
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   524
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   802
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   7860
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   12015
      _ExtentX        =   21193
      _ExtentY        =   13864
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   529
      TabCaption(0)   =   "Applications"
      TabPicture(0)   =   "frmOpen.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label5"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label6"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Toolbar1"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "ListView4"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "ListView2"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "ListView1"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "ImageList3"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).ControlCount=   9
      TabCaption(1)   =   "Ouvrir"
      TabPicture(1)   =   "frmOpen.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "ImageList4"
      Tab(1).Control(1)=   "Toolbar2"
      Tab(1).Control(2)=   "ListView3"
      Tab(1).Control(3)=   "TreeView1"
      Tab(1).Control(4)=   "Label4"
      Tab(1).Control(5)=   "Label3"
      Tab(1).ControlCount=   6
      Begin MSComctlLib.ImageList ImageList4 
         Left            =   -69840
         Top             =   2400
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   8
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":0038
               Key             =   "DISQUETTE"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":0192
               Key             =   "CDROM"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":02EC
               Key             =   "DDUR"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":0446
               Key             =   "DNET"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":05A0
               Key             =   "POSTE"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":0B3A
               Key             =   "FOLDER"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":0C94
               Key             =   "OPENFOLDER"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":0DEE
               Key             =   "MAQ"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.Toolbar Toolbar2 
         Height          =   330
         Left            =   -64920
         TabIndex        =   7
         Top             =   405
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   582
         ButtonWidth     =   609
         ButtonHeight    =   582
         Style           =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "DIR"
               Object.ToolTipText     =   "Niveau pr�c�dent"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.ToolTipText     =   "Affichage"
               ImageIndex      =   3
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   4
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "ICO"
                     Text            =   "Grandes Icones"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "SICO"
                     Text            =   "Petites icones"
                  EndProperty
                  BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "LIST"
                     Text            =   "Liste"
                  EndProperty
                  BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "DET"
                     Text            =   "D�tail"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.ToolTipText     =   "Propri�t�s du fichier"
               ImageIndex      =   4
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.ListView ListView3 
         Height          =   6975
         Left            =   -71595
         TabIndex        =   6
         Top             =   810
         Width           =   8535
         _ExtentX        =   15055
         _ExtentY        =   12303
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HoverSelection  =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList3"
         SmallIcons      =   "ImageList4"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   1
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "NOM"
            Text            =   "Nom"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "SIZE"
            Text            =   "Taille"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Key             =   "TYPE"
            Text            =   "Type"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Key             =   "DATE"
            Text            =   "Modifi� le"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Key             =   "SORTSIZE"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Key             =   "SORTDATE"
            Object.Width           =   0
         EndProperty
      End
      Begin MSComctlLib.TreeView TreeView1 
         Height          =   7455
         Left            =   -74970
         TabIndex        =   5
         Top             =   360
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   13150
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         ImageList       =   "ImageList4"
         BorderStyle     =   1
         Appearance      =   1
      End
      Begin MSComctlLib.ImageList ImageList3 
         Left            =   6480
         Top             =   2280
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   8
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":2AC8
               Key             =   "RUB"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":37A2
               Key             =   "DISQUETTE"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":3BF4
               Key             =   "CDROM"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":4046
               Key             =   "DDUR"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":4498
               Key             =   "DNET"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":48EA
               Key             =   "FOLDER"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":4D3C
               Key             =   "MAQ"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmOpen.frx":5A16
               Key             =   "APP"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   7000
         Left            =   3390
         TabIndex        =   1
         Top             =   810
         Width           =   8565
         _ExtentX        =   15108
         _ExtentY        =   12356
         Arrange         =   2
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         HoverSelection  =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList2"
         SmallIcons      =   "ImageList3"
         ColHdrIcons     =   "ImageList2"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "CAT"
            Text            =   "Cat�gorie"
            Object.Width           =   14517
         EndProperty
      End
      Begin MSComctlLib.ListView ListView2 
         Height          =   3255
         Left            =   30
         TabIndex        =   2
         Top             =   810
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   5741
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         HoverSelection  =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList2"
         ForeColor       =   -2147483640
         BackColor       =   8421504
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.ListView ListView4 
         Height          =   3375
         Left            =   30
         TabIndex        =   10
         Top             =   4440
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   5953
         Arrange         =   1
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         HoverSelection  =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList2"
         ForeColor       =   -2147483640
         BackColor       =   8421504
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   330
         Left            =   10080
         TabIndex        =   13
         Top             =   405
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   582
         ButtonWidth     =   609
         ButtonHeight    =   582
         Style           =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "DIR"
               Object.ToolTipText     =   "Niveau pr�c�dent"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.ToolTipText     =   "Affichage"
               ImageIndex      =   3
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   4
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "ICO"
                     Text            =   "Grandes Icones"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "SICO"
                     Text            =   "Petites icones"
                  EndProperty
                  BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "LIST"
                     Text            =   "Liste"
                  EndProperty
                  BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "DET"
                     Text            =   "D�tail"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "PROP"
               Object.ToolTipText     =   "Propri�t�s du fichier"
               ImageIndex      =   4
            EndProperty
         EndProperty
      End
      Begin VB.Label Label6 
         Caption         =   "Clients Deal :"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   4140
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Applicatifs Deal :"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   480
         Width           =   1935
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   -70560
         TabIndex        =   9
         Top             =   480
         Width           =   5535
      End
      Begin VB.Label Label3 
         Caption         =   "R�pertoire : "
         Height          =   255
         Left            =   -71520
         TabIndex        =   8
         Top             =   480
         Width           =   975
      End
      Begin VB.Label Label2 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   4440
         TabIndex        =   4
         Top             =   480
         Width           =   5535
      End
      Begin VB.Label Label1 
         Caption         =   "R�pertoire : "
         Height          =   255
         Left            =   3480
         TabIndex        =   3
         Top             =   480
         Width           =   975
      End
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   3360
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   48
      ImageHeight     =   48
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOpen.frx":66F0
            Key             =   "MAQ"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOpen.frx":83CA
            Key             =   "RUB"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOpen.frx":A0A4
            Key             =   "DEAL"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOpen.frx":BD7E
            Key             =   "CLIENT"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOpen.frx":DA58
            Key             =   "APP"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3720
      Top             =   6480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOpen.frx":F732
            Key             =   "NEW"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOpen.frx":FCCC
            Key             =   "PARENT"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOpen.frx":FE26
            Key             =   "PROP"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOpen.frx":103C0
            Key             =   "PROPFILE"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmOpen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



'-> Niveau d'affichage : -1 -> client , 0 -> applicatif , 1 -> rubriques , 2 -> maquettes
Dim Level As Integer
Dim lvProgiciel As String
Dim lvRubrique As String
Dim lvOrigine As String

Private Sub Command2_Click()
    Unload Me
End Sub

Private Sub Form_Load()

'-> Init de la collection des clients
Set Clients = New Collection

'-> Init de la collection des progiciels
Set Progiciels = New Collection

'-> Afficher la liste des nodes dans le treeview
LoadDrive True

'-> Simuler un double click sur le poste de travail
TreeView1_DblClick

'-> Positionner le niveau
NiveauDisk = True

'-> R�cup�rer le path du r�pertoire turbo
TurboPathIni = GetTurboIniPath

If TurboPathIni = "" Then
    '-> Bloquer l'onglet de recherche via un path client
    Me.SSTab1.TabEnabled(0) = False
Else
    '-> Initialisation des variable des path
    InitVar
    '-> V�rifier que toutes les valeurs soient charg�es
    If Lecteur = "" Or Version = "" Or Ident = "" Then
        Me.SSTab1.TabEnabled(0) = False
        Exit Sub
    End If
    '-> Lancer le chargement de la liste des diff�rents clients et des diff�rents progiciels
    GetListApp
End If



End Sub

Private Sub ListView1_Click()

Dim KeyProgi As String

'-> quitter si pas d'items
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Traitement selon la nature de l'affichage
Select Case Entry(1, Me.ListView1.SelectedItem.Tag, "|")
    Case "RUBRIQUE"
        '-> Indiquer la rubrique en cours
        lvRubrique = Entry(2, Me.ListView1.SelectedItem.Tag, "|")
        '-> Afficher la liste des maquettes
        DisplayMaq Entry(2, Me.ListView1.SelectedItem.Tag, "|")
        Level = 1
    Case "PROGICIEL"
        '-> Afficher la liste des rubriques
        KeyProgi = Me.ListView1.SelectedItem.Tag
        DisplayRub Entry(2, KeyProgi, "|")
        '-> Indiquer le progiciel en cours
        lvProgiciel = Entry(2, KeyProgi, "|")
        '-> Indiquer le niveau en cours
        Level = 0
End Select


End Sub

Private Sub ListView1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

'-> Trier
If Me.ListView1.SortOrder = lvwDescending Then
    Me.ListView1.SortOrder = lvwAscending
Else
    Me.ListView1.SortOrder = lvwDescending
End If
Me.ListView1.Sorted = True

End Sub


Private Sub DisplayRub(Progiciel As String)

'---> Cette proc�dure affiche les rubriques d'un progiciel d'un client

'---> Afficher la liste des rubriques pour un applicatif donn�
Dim i As Integer
Dim iTemX As ListItem
Dim aProgiciel As Progiciel

'-> Nettoyer le listView
Me.ListView1.ListItems.Clear

'-> Recup�rer un pointeur vers un progiciel
Set aProgiciel = GetProgiciel(Progiciel)

'-> Tester le retour
If aProgiciel Is Nothing Then Exit Sub

'-> Cr�er autant d'icones que de rubriques
For i = 1 To NumEntries(aProgiciel.Rubriques, "|")
    Set iTemX = Me.ListView1.ListItems.Add(, , Entry(2, Entry(i, aProgiciel.Rubriques, "|"), "@"), "RUB", "RUB")
    iTemX.Tag = "RUBRIQUE|" & Entry(1, Entry(i, aProgiciel.Rubriques, "|"), "@")
Next

'-> R�organiser les icones
Me.ListView1.Arrange = lvwAutoTop



End Sub

Private Sub DisplayMaq(Rubrique As String)

'---> Cette proc�dure analyse un r�pertoire � la recherche des maquettes de la rubriques

Dim FileName As String
Dim MyPath As String
Dim Res As Long
Dim lpBuffer As String
Dim aItem As ListItem
Dim NomFichier As String
Dim i As Integer

'-> Nettoyer le listview
Me.ListView1.ListItems.Clear

'-> Get du path
MyPath = Me.Label2.Caption

'-> Rajouter un "\" � la fin
If Right(MyPath, 1) <> "\" Then MyPath = MyPath & "\"

'-> Analyse du r�pertoire
FileName = Dir$(MyPath & "*.maqgui", vbNormal)

Do While FileName <> ""
    '-> R�cup�rer le nom du fichier
    i = InStrRev(FileName, ".")
    NomFichier = Mid$(FileName, 1, i - 1)
    '-> R�cup�rer la rubrique associ�e au fichier
    lpBuffer = Space$(200)
    Res = GetPrivateProfileString("PROGICIEL", "\RUB", "", lpBuffer, Len(lpBuffer), MyPath & FileName)
    If Res <> 0 Then
        lpBuffer = Mid$(lpBuffer, 1, Res)
        '-> Comparer
        If UCase$(Trim(lpBuffer)) = UCase$(Trim(Rubrique)) Then
            '-> Rajouter dans la liste des maquettes
            Set aItem = Me.ListView1.ListItems.Add(, , NomFichier, "MAQ", "MAQ")
            aItem.Tag = "MAQUETTE|" & FileName
        End If
    End If
    FileName = Dir
Loop

End Sub


Private Sub ListView1_KeyDown(KeyCode As Integer, Shift As Integer)

If Me.ListView1.SelectedItem Is Nothing Then Exit Sub
If Entry(1, Me.ListView1.SelectedItem.Tag, "|") <> "RUBRIQUE" Then Exit Sub

If Shift <> 1 Then Exit Sub
If KeyCode <> 121 Then Exit Sub

MsgBox "Cl� de la rubrique : " & Entry(2, Me.ListView1.SelectedItem.Tag, "|"), vbInformation + vbOKOnly, "Message"
            
End Sub



Private Sub ListView1_KeyPress(KeyAscii As Integer)

Dim aButton As MSComctlLib.Button

If KeyAscii = 13 Then ListView1_Click
If KeyAscii = 8 Then
    Set aButton = Me.Toolbar1.Buttons("DIR")
    Toolbar1_ButtonClick aButton
End If

End Sub

Private Sub ListView2_Click()

'---> Afficher la liste des rubriques pour un applicatif donn�
Dim i As Integer
Dim iTemX As ListItem

'-> Quitter si pas de s�lections
If Me.ListView2.SelectedItem Is Nothing Then Exit Sub

'-> Nettoyer le listView
Me.ListView1.ListItems.Clear

'-> Ne rien faire si pas de rubriques
If Me.ListView2.SelectedItem.Tag = "" Then Exit Sub

'-> Cr�er autant d'icones que de rubriques
For i = 1 To NumEntries(Me.ListView2.SelectedItem.Tag, "|")
    Set iTemX = Me.ListView1.ListItems.Add(, , Entry(2, Entry(i, Me.ListView2.SelectedItem.Tag, "|"), "@"), "RUB", "RUB")
    iTemX.Tag = "RUBRIQUE|" & Entry(1, Entry(i, Me.ListView2.SelectedItem.Tag, "|"), "@")
Next

'-> Mettre � jour le path
Me.Label2.Caption = Me.ListView2.SelectedItem.Key

'-> R�organiser les icones
Me.ListView1.Arrange = lvwAutoTop

'-> Indiquer le niveau en cours
Level = 0

'-> Indiquer l'applicatif en cours
lvProgiciel = Trim(UCase$(Entry(1, Me.ListView2.SelectedItem.Text, Chr(13))))

'-> Indiquer l'origine de l'affichage
lvOrigine = "APP"

End Sub



Private Sub ListView2_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then ListView2_Click

End Sub

Private Sub ListView3_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)



'-> rediriger vers le tri
If ColumnHeader.Key = "SIZE" Then
    Set ColumnHeader = Me.ListView3.ColumnHeaders("SORTSIZE")
ElseIf ColumnHeader.Key = "DATE" Then
    Set ColumnHeader = Me.ListView3.ColumnHeaders("SORTDATE")
End If

'-> Trier
If Me.ListView3.SortOrder = lvwAscending Then
    Me.ListView3.SortOrder = lvwDescending
Else
    Me.ListView3.SortOrder = lvwAscending
End If
Me.ListView3.SortKey = ColumnHeader.Index - 1
Me.ListView3.Sorted = True


End Sub

Private Sub ListView3_DblClick()

Dim aNode As Node
Dim MyPath As String

'---> Afficher le sous r�pertoire di on click sur un sous r�pertoire
Select Case Entry(1, Me.ListView3.SelectedItem.Key, "|")

    Case "FOLDER"
        '-> Pointer sur le node sp�cifique
        Set aNode = Me.TreeView1.Nodes(Me.ListView3.SelectedItem.Key)
        aNode.Selected = True
        aNode.Expanded = True
        '-> Afficher les sous niveaux
        DisplayFileObject Entry(2, Me.ListView3.SelectedItem.Key, "|") & "\", aNode
End Select

End Sub

Private Sub ListView3_KeyPress(KeyAscii As Integer)

Dim aButton As MSComctlLib.Button

If KeyAscii = 13 Then ListView3_DblClick
If KeyAscii = 8 Then
    Set aButton = Me.Toolbar2.Buttons("DIR")
    Toolbar2_ButtonClick aButton
End If
    
End Sub



Private Sub ListView4_Click()

'---> Afficher la liste des rubriques pour un applicatif donn�
Dim i As Integer
Dim iTemX As ListItem
Dim aClient As Client
Dim aProgiciel As Progiciel

'-> Quitter si pas de s�lections
If Me.ListView4.SelectedItem Is Nothing Then Exit Sub

'-> Nettoyer le listView
Me.ListView1.ListItems.Clear

'-> Pointer sur le client sp�cifi�
Set aClient = Clients(Me.ListView4.SelectedItem.Key)

'-> Afficher la liste des progiciels affect�s au client
For i = 1 To NumEntries(aClient.Progiciel, ",")
    Set aProgiciel = GetProgiciel(Entry(i, aClient.Progiciel, ","))
    If aProgiciel Is Nothing Then GoTo NextProgi
    '-> Cr�er l'icone dans le liste view
    Set iTemX = Me.ListView1.ListItems.Add(, "PROGICIEL|" & UCase$(aProgiciel.Name), aProgiciel.Name & Chr(13) & aProgiciel.Libel, "APP")
    iTemX.Tag = "PROGICIEL|" & UCase$(aProgiciel.Name)
NextProgi:
Next

'-> Mettre � jour le path des maquettes
Me.Label2.Caption = aClient.Path

'-> Indiquer le niveau en cours
Level = -1
lvOrigine = "CLIENT"
End Sub


Private Function GetProgiciel(Name As String) As Progiciel

'---> Cette proc�dure retourne un objet progiciel

Dim aProgiciel As Progiciel

On Error GoTo GestError

'-> Pointer sur l'objet
Set aProgiciel = Progiciels("PROGICIEL|" & UCase$(Trim(Name)))

Set GetProgiciel = aProgiciel
Exit Function

GestError:
    Set GetProgiciel = Nothing

End Function

Private Sub ListView4_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then ListView4_Click

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Select Case Button.Key

    Case "PROP"
        '-> V�rifier qu'il y a des objets
        If Me.ListView1.SelectedItem Is Nothing Then Exit Sub
        
        '-> V�rifier que l'on est sur un fichier
        If Entry(1, Me.ListView1.SelectedItem.Tag, "|") <> "MAQUETTE" Then Exit Sub
        
        '-> Afficher la page de propri�t� de la maquette
        ShowFileProperties Me.Label2.Caption & Me.ListView1.SelectedItem.Text & ".maqgui"
    Case "DIR"
        Select Case Level '-> Selon le n iveau d'affichage en cours
            Case -1 '-> On a cliqu� sur un client ne pas remonter
                
            Case 0 '-> On a cliqu� sur un applicatif : remonter si origine = CLIENT
                If lvOrigine = "CLIENT" Then ListView4_Click
            Case 1 '-> Les maquettes sont affich�es remonter dans tous les cas
                DisplayRub lvProgiciel
                Level = 0
        End Select
End Select

End Sub

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)

Select Case ButtonMenu.Key
    Case "ICO"
        Me.ListView1.View = lvwIcon
    Case "SICO"
        Me.ListView1.View = lvwSmallIcon
    Case "LIST"
        Me.ListView1.View = lvwList
    Case "DET"
        Me.ListView1.View = lvwReport
End Select

End Sub

Private Sub Toolbar2_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim NewFilename As String
Dim i As Integer
Dim aNode As Node
Dim IsDrive As Boolean

Select Case Button.Key
    Case "DIR"
        '-> Ne rien faire si on est au niveau disque
        If Me.TreeView1.SelectedItem.Key = "POSTE" Then
            IsDrive = True
        Else
            If Me.TreeView1.SelectedItem.Parent.Key = "POSTE" Then IsDrive = True
        End If
        
        If IsDrive Then
            '-> Afficher la liste des disques
            LoadDrive False
            '-> S�lectionner l'icone
            Set aNode = Me.TreeView1.Nodes("POSTE")
            aNode.Selected = True
            aNode.Expanded = True
            '-> Quitter la proc�dure
            Exit Sub
        End If
        '->Il faut remonter d'un niveau
        i = InStrRev(Me.Label4.Caption, "\")
        NewFilename = Mid$(frmOpen.Label4.Caption, 1, i - 1)
        '-> Pointer sur le node
        Set aNode = Me.TreeView1.Nodes("FOLDER|" & NewFilename)
        aNode.Selected = True
        aNode.Expanded = True
        '-> Afficher le nouveau path
        DisplayFileObject NewFilename & "\", aNode
        
End Select

End Sub

Private Sub Toolbar2_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)



Select Case ButtonMenu.Key
    Case "ICO"
        Me.ListView3.View = lvwIcon
    Case "SICO"
        Me.ListView3.View = lvwSmallIcon
    Case "LIST"
        Me.ListView3.View = lvwList
    Case "DET"
        Me.ListView3.View = lvwReport
End Select

End Sub


Private Sub TreeView1_DblClick()

'-> D�tecter le node sur lequel on vient de cliquer
If Me.TreeView1.SelectedItem Is Nothing Then Exit Sub

'-> Selon la touche que l'on vient de cliquer
Select Case Me.TreeView1.SelectedItem.Tag
    Case "POSTE"
        '-> Il faut rafficher la liste des disques dans le listView
        LoadDrive False
        '-> Indiquer que l'on est au niveau disque
        NiveauDisk = True
    Case "FOLDER"
        '-> Afficher la liste des r�pertoires dans  le listview
        DisplayFileObject Entry(2, Me.TreeView1.SelectedItem.Key, "|") & "\", Me.TreeView1.SelectedItem
End Select

'-> Afficher les sous nodes
Me.TreeView1.SelectedItem.Expanded = True


End Sub

Private Sub TreeView1_KeyPress(KeyAscii As Integer)

Select Case KeyAscii
    Case 8, 13
        TreeView1_DblClick
End Select

End Sub
