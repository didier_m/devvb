Attribute VB_Name = "fctCom"
'-> Variables de param�trage
Public PathSpool As String
Public PathLog As String
Public PathArchive As String
Public PathNT As String
Public PathUNIX As String
Public PathImpression As String
Public Extension As String
Public IniPath  As String '---> Emplacement + nom du fichier TURBODISPATCH.INI

'-> APi pour gestion des fichiers
Public Declare Function CloseHandle& Lib "kernel32" (ByVal hObject As Long)
Public Declare Function OpenFile& Lib "kernel32" (ByVal lpFileName As String, lpReOpenBuff As OFSTRUCT, ByVal wStyle As Long)
Public Declare Function GetFileSize& Lib "kernel32" (ByVal hFile As Long, lpFileSizeHigh As Long)

'-> Constantes de gestion des fichiers
Public Const OFS_MAXPATHNAME& = 128
Public Const OF_EXIST& = &H4000
Public Const OF_READ& = &H0
Public Const FILE_SHARE_READ& = &H1
Public Const FILE_SHARE_WRITE& = &H2
Public Const CREATE_ALWAYS& = 2
Public Const CREATE_NEW& = 1
Public Const FILE_ATTRIBUTE_ARCHIVE& = &H20
Public Const FILE_ATTRIBUTE_COMPRESSED& = &H800
Public Const FILE_ATTRIBUTE_DIRECTORY& = &H10
Public Const FILE_ATTRIBUTE_HIDDEN& = &H2
Public Const FILE_ATTRIBUTE_NORMAL& = &H80
Public Const FILE_ATTRIBUTE_READONLY& = &H1
Public Const FILE_ATTRIBUTE_SYSTEM& = &H4
Public Const FILE_ATTRIBUTE_TEMPORARY& = &H100
Public Const GENERIC_WRITE& = &H40000000
Public Const GENERIC_READ& = &H80000000
Public Const GENERIC_ALL& = &H10000000
Public Const GENERIC_EXECUTE& = &H20000000

'-> Structure pour gestion des fichiers
Public Type OFSTRUCT
    cBytes As Byte
    fFixedDisk As Byte
    nErrCode As Integer
    Reserved1 As Integer
    Reserved2 As Integer
    szPathName(OFS_MAXPATHNAME) As Byte
End Type

'-> API pour gestion des messages
Public Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)

'-> Constantes de message
Public Const WM_CLOSE& = &H10
Public Const WM_PAINT& = &HF

'-> Pour lecture des private DATAS des objets
Public Declare Function SetWindowText Lib "user32" Alias "SetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String) As Long
Public Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long) As Long



'-> API de lecture des fichiers au format "*.ini"
Public Declare Function GetPrivateProfileSection& Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)


'-> Api gestion des fichiers tempo
Public Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Public Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long


'-> Api pour gestion des fichiers

'-> API pour interface
Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Public Declare Function GetClientRect& Lib "user32" (ByVal hwnd As Long, lpRect As RECT)

'-> Gestion du press papier
Public Const WM_CUT = &H300
Public Const WM_PASTE = &H302
Public Const WM_COPY = &H301

'-> Pour ouverture du site DEAL INFORMATIQUE
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long


Public Function Initialisation() As Boolean

Dim Res As Long
Dim lpBuffer As String

'-> Charger le propath d'analyse en fonction de l'environnement stabilis� ou dvl
If InStr(1, UCase$(App.Path), "\EMILIEGUI\SRC") <> 0 Then
    IniPath = "M:\DealVB\Dev Visual Basic\Turbo Dev\Turbo"
Else
    CreatePath IniPath
End If
IniPath = IniPath & "\TurboDispatch.ini"

'-> V�rifier que le fichier INI existe bien
If Dir$(IniPath, vbNormal) = "" Then
    Initialisation = False
    Exit Function
End If

'-> Charger les param�trages
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PARAM", "LOGDIRECTORY", "", lpBuffer, Len(lpBuffer), IniPath)
PathLog = Mid$(lpBuffer, 1, Res)

lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PARAM", "SPOOLDIRECTORY", "", lpBuffer, Len(lpBuffer), IniPath)
PathSpool = Mid$(lpBuffer, 1, Res)

lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PARAM", "ARCHIVEDIRECTORY", "", lpBuffer, Len(lpBuffer), IniPath)
PathArchive = Mid$(lpBuffer, 1, Res)

lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PARAM", "IMPDIRECTORY", "", lpBuffer, Len(lpBuffer), IniPath)
PathImpression = Mid$(lpBuffer, 1, Res)

lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PARAM", "EXTENSION", "", lpBuffer, Len(lpBuffer), IniPath)
Extension = Mid$(lpBuffer, 1, Res)

lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PARAM", "PATHNT", "", lpBuffer, Len(lpBuffer), IniPath)
PathNT = Mid$(lpBuffer, 1, Res)

lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PARAM", "PATHUNIX", "", lpBuffer, Len(lpBuffer), IniPath)
PathUNIX = Mid$(lpBuffer, 1, Res)

'-> Renvoyer une valeur de succ�s
Initialisation = True

End Function

