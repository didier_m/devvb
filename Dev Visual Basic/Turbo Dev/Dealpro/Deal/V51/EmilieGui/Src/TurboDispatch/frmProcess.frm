VERSION 5.00
Begin VB.Form frmProcess 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Process Turbodispatch"
   ClientHeight    =   2595
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8625
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2595
   ScaleWidth      =   8625
   Begin VB.CommandButton Command1 
      Caption         =   "Arreter le process"
      Height          =   255
      Left            =   6480
      TabIndex        =   9
      Top             =   2160
      Width           =   2055
   End
   Begin VB.PictureBox PicTraitement 
      AutoRedraw      =   -1  'True
      Height          =   255
      Left            =   2040
      ScaleHeight     =   195
      ScaleWidth      =   6435
      TabIndex        =   4
      Top             =   720
      Width           =   6495
   End
   Begin VB.PictureBox PicCom 
      Height          =   255
      Left            =   4560
      ScaleHeight     =   195
      ScaleWidth      =   1635
      TabIndex        =   0
      Top             =   2160
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2040
      TabIndex        =   8
      Top             =   1680
      Width           =   6495
   End
   Begin VB.Label Label5 
      Caption         =   "Fichier data en cours :"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   1680
      Width           =   1695
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2040
      TabIndex        =   6
      Top             =   1200
      Width           =   1695
   End
   Begin VB.Label Label3 
      Caption         =   "Analyse de la ligne  :"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label Label2 
      Caption         =   "Traitement : "
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   1335
   End
   Begin VB.Label lblFichierPilote 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2040
      TabIndex        =   2
      Top             =   240
      Width           =   6495
   End
   Begin VB.Label Label1 
      Caption         =   "Fichier pilote : "
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   1455
   End
End
Attribute VB_Name = "frmProcess"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Indique si le process est termin�
Private IsRunningProcess As Boolean

'-> Pointeur vers la classe associ�e
Public aProcess As clsProcess

'-> Indique si on ouvre la gestion des logs
Public EditLog As Boolean

'-> Pour gestion de la temporisation
Dim NbLu As Long
Public NbTotal As Long


Private Sub Command1_Click()

Dim aLb As Libelle
Dim Rep As VbMsgBoxResult

'-> Demander confirmation
Set aLb = Libelles("FRMPROCESS")
Rep = MsgBox(aLb.GetCaption(7), vbExclamation + vbYesNo, aLb.GetToolTip(7))
If Rep = vbNo Then Exit Sub

'-> Envoyer un message de fin de traitement
SetWindowText Me.aProcess.HdlCom, "PROCESS_EXIT"
SendMessage Me.aProcess.HdlCom, WM_PAINT, 0, 0

End Sub

Private Sub Form_Load()

Dim aLb As Libelle

'-> Indiquer que le process tourne
IsRunningProcess = True

'-> Gestion des messpogs
Set aLb = Libelles("FRMPROCESS")
Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Label2.Caption = aLb.GetCaption(5)
Me.Label3.Caption = aLb.GetCaption(3)
Me.Label5.Caption = aLb.GetCaption(4)
Me.Command1.Caption = aLb.GetCaption(6)

'-> R�cup�rer la taille en octet du fichier
NbLu = 0


End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

If IsRunningProcess Then
    Cancel = 1
    Me.Hide
End If


End Sub

Private Sub PicCom_Paint()

'---> Ce picture BOX sert � la communication avec le process d�tach�

'-> Liste des MACROS POSSIBLES
' RETURN_HANDLE -> Handle de com du process
' SIZE_LIGNE_PILOTE -> Taille en octets des lignes lues du fichier pilote
' NUM_LIGNE_PILOTE -> Num�ro de la ligne en cours de lecture
' FILE_NAME_DATA -> Nom du fichier de donn�es en cours de lecture
' PROCESS_EXIT -> Fin du process d�ttach�

Dim Res As Long
Dim lpBuffer As String


'-> R�cupere la private data du picturebox de communication
lpBuffer = Space$(2000)
Res = GetWindowText(Me.PicCom.hwnd, lpBuffer, Len(lpBuffer))

'-> Supprimer la private data du process
SetWindowText Me.PicCom.hwnd, ""

'-> Quitter si pas de messages
If Trim(lpBuffer) = "" Then Exit Sub


lpBuffer = Mid$(lpBuffer, 1, Res)
'-> Traiter le message selon sa nature
Select Case Entry(1, lpBuffer, "|")

    Case "RETURN_HANDLE"
        '-> Mettre � jour le handle de communication du process d�tach�
        Me.aProcess.HdlCom = CLng(Entry(2, lpBuffer, "|"))
        
    Case "FILE_NAME_DATA"
        '-> Mettre � jour le nom du fichier
        Me.Label6.Caption = Entry(2, lpBuffer, "|")
        
    Case "NUM_LIGNE_PILOTE"
        '-> Mettre � jour le num�ro de la ligne en cours de traitement
        Me.Label4.Caption = Entry(2, lpBuffer, "|")
        
    Case "SIZE_LIGNE_PILOTE"
        '-> Mettre � jour la zone lue
        NbLu = NbLu + CLng(Entry(2, lpBuffer, "|"))
        '-> Redessiner
        DrawLoadding Me.PicTraitement, NbLu, NbTotal
        DoEvents
    
    Case "PROCESS_EXIT" '-> fermeture de l'agent
                                    
        '-> Supprimer du browse en cours
        If IsListeProcess Then aProcess.RemoveToBrowse
        
        '-> D�charger cette feuille
        IsRunningProcess = False
        SendMessage Me.hwnd, WM_CLOSE, 0, 0
              
        '-> Afficher la gestion des LOG si necessaire
        If Me.EditLog Then
            If Not IsGestLog Then Load frmGestLog
            frmGestLog.EditLog aProcess.FichierLog
            frmGestLog.Visible = True
            frmGestLog.ZOrder
        End If
              
        '-> Supprimer la classe dans la collection
        Process.Remove Me.aProcess.Key
        
        '-> Tester s'il y a d'autres PROCESS pour lib�rer la gestion des param�trages
        If Process.Count = 0 Then IsBatchRunning = False
        
        
End Select 'Selon la nature du message

End Sub

