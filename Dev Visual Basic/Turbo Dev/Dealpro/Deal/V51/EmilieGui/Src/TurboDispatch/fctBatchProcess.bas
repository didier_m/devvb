Attribute VB_Name = "fctBatchProcess"
Option Explicit

'-> Variable necessaires � la communication
Public hdlCommunication As Long

'-> Variables de traitement
Public PiloteFile As String '-> Fichier pilote
Dim IsLog As Boolean '-> Indique si on doit faire la mise � jour du fichier des logs
Public IsArchive As Boolean '-> Indique si on doit archiver le fichier pilote apr�s utilisation
Public Imprimante As String '-> Cl� d'impression

Dim hdlLog As Integer '-> Handle du fichier LOG
Dim hdlfile As Integer '-> Handle de lecture du fichier pilote
Dim NBLig As Long '-> Ligne en cours du fichier pilote

'---> Gestion des formats d'impression TURBO
Dim MaqString As String '-> format 1 : %%GUI%%
Dim IsNewFormat As Boolean '-> Positionner sur TRUE si format [SPOOL] [/SPOOL]
Dim MaqFile As String '-> Stockage de la maquette au format2
Dim BreakData As String '-> Cl� de rupture de la maquette pour publipostage

'-> Premi�re zone du fichier pilote
Dim SendZone As String
Dim ListeFileOut As String

Sub Main()

'-> Point d'entr�e du programme

'-> DESCRIPTIF DE LA LIGNE DE COMMANDE
'HDLCOM|PILOTEFILE|ISLOG|ISARCHIVE|CONTROL_KEY ( %%GUI%% )|IMPRIMANTE
On Error GoTo GestError

'-> V�rifier la ligne de commande
If Trim(Command$) = "" Then End

'-> analyse de la ligne de connmandes
If NumEntries(Command$, "|") <> 6 Then End

'-> V�rification de la cl� de controle
If Entry(5, Command$, "|") <> "%%GUI%%" Then End

'-> traitement du handle de commande
If Entry(1, Command$, "|") = "" Then End
If Not IsNumeric(Entry(1, Command$, "|")) Then End
hdlCommunication = CLng(Entry(1, Command$, "|"))

'-> R�cup�ration des variables de param�trage
If Not Initialisation Then End

'-> R�cup�ration du nom de fichier
If Entry(2, Command$, "|") = "" Then End
PiloteFile = Entry(2, Command$, "|")

'-> V�rifier que le fichier existe
If Dir$(PathSpool & PiloteFile, vbNormal) = "" Then End

'-> Test sur le lop
If Entry(3, Command$, "|") = "1" Then
    IsLog = True
Else
    IsLog = False
End If

'-> Test sur l'archivage
If Entry(4, Command$, "|") = "1" Then
    IsArchive = True
Else
    IsArchive = False
End If

'-> R�cup�ration de l'imprimante
'Imprimante = Entry(6, Command$, "|")


'-> Tout est OK : Charger la feuille en m�moire
Load frmProcessDispatch

'-> Renvoyer le handle de communication
SetWindowText hdlCommunication, "RETURN_HANDLE|" & frmProcessDispatch.PicCom.hwnd
SendMessage hdlCommunication, WM_PAINT, 0, 0

'-> Lancer le programme d'analyse du fichier pilote
ExecProcess

GestError:
    End

End Sub

Private Sub ExecProcess()

'---> Cette proc�dure r�alise l'op�ration de publipostage

Dim Ligne As String
Dim Param As String
Dim Fichier As String
Dim i As Integer
Dim Res As Long

'-> Ouverture du fichier des LOG
Call OpenLOG

'-> V�rifier qu'il n'existe pas une version pr�c�dente du fichier pilote
If Dir$(PathImpression & PiloteFile, vbNormal) <> "" Then
    '-> supprimer le fichier car il existe d�ja
    Kill PathImpression & PiloteFile
End If

'-> Prendre le fichier pilote et le d�placer dans le r�pertoire d'impression
Name PathSpool & PiloteFile As PathImpression & PiloteFile

'-> Obtenir un handle de fichier pour ouvrir
hdlfile = FreeFile
Open PathImpression & PiloteFile For Input As #hdlfile

'-> Traitement du fichier en mode s�quenciel
Do While Not EOF(hdlfile)
    '-> Rendre la main � la CPU
    DoEvents
    '-> Lecture de la ligne
    Line Input #hdlfile, Ligne
    '-> Incr�menter le compteur de lignes
    NBLig = NBLig + 1
    '-> Envoyer l'information � la console sur la ligne en cours de traitement _
    et sur la taille du fichier qui est lue
    Res = SetWindowText(hdlCommunication, "NUM_LIGNE_PILOTE|" & NBLig)
    Res = SendMessage(hdlCommunication, WM_PAINT, 0, 0)
    Res = SetWindowText(hdlCommunication, "SIZE_LIGNE_PILOTE|" & Len(Ligne))
    Res = SendMessage(hdlCommunication, WM_PAINT, 0, 0)
       
    '-> Passer � la ligne suivante si ligne blanche
    If Trim(Ligne) = "" Then GoTo NextLig
    '-> Imprimer la ligne que l'on va traiter
    MajLOG "BEGINPILOTELIGNE=" & NBLig & "�" & Ligne
    
    '-> Envoyer un log d'erreur si pas de fichier � analyser
    If Trim(Entry(2, Ligne, "|")) = "" Then
        '-> Envpoyer un LOG
        MajLOG "DATAFILE=NOFILE�"
        '-> Passer � la ligne suivante
        GoTo NextLig
    End If
    
    '-> Mise � jour de la premi�re zone
    SendZone = Trim(Entry(1, Ligne, "|"))
    
    '-> Remettre � blanc la liste des fichiers joints
    ListeFileOut = ""
    
    '-> R�cup�rer les diff�rents fichiers � analyser
    For i = 2 To NumEntries(Ligne, "|")
        '-> R�cup�rer les param�tres d'impression du fichier
        Param = Entry(1, Entry(i, Ligne, "|"), "�")
        Fichier = Entry(2, Entry(i, Ligne, "|"), "�")
        '-> Passer � la ligne suivante si nom de fichier � blanc
        If Trim(Fichier) = "" Then GoTo NextLig
        '-> Lancer la proc�dure d'impression de ce fichier
        Call CreateImpression(Param, Fichier)
    Next 'Pour tous les fichiers de la ligne
    
    '-> Envoyer un mail Maintenant avec la liste des fichiers joints
    If Entry(1, Imprimante, "�") = "OUT" Then
        SendToOutLook
    ElseIf Entry(1, Imprimante, "�") = "WWW" Then
        SendToInternet
    End If
    
NextLig:
Loop 'Boucle d'analyse du fichier pilote

'-> Fermer le fichier pilote
Close #hdlfile


'-> Faire l'archivage
If IsArchive Then
    '-> V�rifier qu'il n'existe pas un fichier dans le r�pertoire d'archivage
    If Dir$(PathArchive & PiloteFile, vbNormal) <> "" Then Kill PathArchive & PiloteFile
    '-> Copier le fichier Pilote dans le r�pertoire d'archivage
    Name PathImpression & PiloteFile As PathArchive & PiloteFile
    '-> Mettre � jour le log
    MajLOG "ARCHIVAGE=YES"
Else
    MajLOG "ARCHIVAGE=NO"
End If

'-> Mettre � jour les log
MajLOG "END=" & Now

'-> Fermer le fichier des log
CloseLOG

'-> Securite
Reset

'-> Renvoyer un message de fermture � la console
SetWindowText hdlCommunication, "PROCESS_EXIT"
SendMessage hdlCommunication, WM_PAINT, 0, 0

'-> Fin du programme
End

End Sub

Private Sub CreateImpression(Param As String, Fichier As String)

'---> Cette proc�dure r�alise l'impression du fichier apr�s recherche des param�trages

'-> Indiquer � la console que l'on traite un nouveau fichier ainsi que la taille de ce fichier

On Error GoTo GestError

Dim i As Long
Dim Ligne As String
Dim hdlData As Integer
Dim LigLu As Long
Dim TailleTotale As Long
Dim TailleLue As Long
Dim Page1 As String
Dim IsSelection As Boolean
Dim Tempfile As String
Dim hdlTemp As Integer
Dim Breaks As Collection
Dim aBreak As clsBreak
Dim DefBreak As String
Dim IsCurrentPageAnalysed As Boolean
Dim aLigne As clsSpoolLigne
Dim Lignes As Collection
Dim IsPageToPrint As Boolean
Dim Res As Long

'-> V�rifier que l'on trouve le fichier des LOG
If Dir$(PathSpool & Fichier, vbNormal) = "" Then
    '-> Inscrire un LOG
    MajLOG "DATAFILE=NOMATCH�" & PathSpool & Fichier & " sp�cifi� � la ligne " & NBLig
    '-> Quitter la proc�dure
    Exit Sub
End If

'-> Inscrire un LOG
MajLOG "DATAFILE=MATCH�" & PathSpool & Fichier

'-> Envoyer � la console le nom du fichier en cours de lecture
Res = SetWindowText(hdlCommunication, "FILE_NAME_DATA|" & PathSpool & Fichier)
Res = SendMessage(hdlCommunication, WM_PAINT, 0, 0)

'-> Ouvrir le fichier
hdlData = FreeFile
Open PathSpool & Fichier For Input As #hdlData

'-> Mettre � jour le conteneur RTF
frmProcessDispatch.RtfFile.Text = ""

'-> Analyse de l'entete de la maquette : sortir de la proc�dure si erreur dans l'entete
If Not AnalyseEntete(hdlData, LigLu) Then GoTo EndData

'-> Indiquer les ruptures analys�es
MajLOG "VALUERUPTURE=" & Param

'-> Initialiser la collection des breaks
Set Breaks = New Collection

'-> Initialiser la collection des lignes dans la page
Set Lignes = New Collection

'-> Retraitement des donn�es de BREAK
For i = 1 To NumEntries(BreakData, "�")
    '-> R�cup�ration de la data
    DefBreak = Entry(i, BreakData, "�")
    '-> Cr�ation d'un nouvel objet de rupture
    Set aBreak = New clsBreak
    If Entry(1, DefBreak, "@") = "ST" Then
        '-> Traitement Section
        aBreak.IdString = "ST-" & UCase$(Entry(1, Entry(2, DefBreak, "@"), "|")) & "(TXT-SECTION)"
        aBreak.Field = UCase$(Entry(3, Entry(2, DefBreak, "@"), "|"))
    Else
        '-> Traitement du tableau
        aBreak.IdString = "TB-" & UCase$(Entry(1, Entry(2, DefBreak, "@"), "|")) & "(BLK-" & UCase$(Entry(2, Entry(2, DefBreak, "@"), "|")) & ")"
        aBreak.Field = UCase$(Entry(4, Entry(2, DefBreak, "@"), "|"))
    End If
    aBreak.ValueToMatch = UCase$(Trim(Entry(i, Param, "�")))
    '-> Ajout dans la collection
    Breaks.Add aBreak, "B" & i
Next

'-> Par d�faut, premi�re page non aalys�e
IsCurrentPageAnalysed = False

'-> Traitement s�quentiel du fichier de donn�es en lecture
Do While Not EOF(hdlData)
    '-> Rendre la main � la CPU
    DoEvents
    '-> Lecture de la ligne
    Line Input #hdlData, Ligne
    
    '-> Passer � la ligne suivante si ligne blanche
    If Trim(Ligne) = "" Then GoTo NextLig
    
    '-> Ne traiter que les lignes qui commencent par un "[
    If Left$(Ligne, 1) <> "[" Then GoTo NextLig
    
    '-> Traiter certains cas
    Select Case UCase$(Ligne)
        Case "[GARDEOPEN]"
            IsSelection = True
            GoTo NextLig
        Case "[GARDECLOSE]"
            IsSelection = False
            GoTo NextLig
        Case "[PAGE]"
            '-> Fin de la page : analyse du contenu des ruptures
            If AnalyseRupture(Breaks, Param, Lignes) Then
                '-> Toutes les conditions sont remplies : transf�rer le contenu de la page vers le RTF final
                For Each aLigne In Lignes
                    frmProcessDispatch.RtfFile.Text = frmProcessDispatch.RtfFile.Text & Chr(13) & Chr(10) & aLigne.Ligne
                Next
                '-> Imprimer un saut de page
                frmProcessDispatch.RtfFile.Text = frmProcessDispatch.RtfFile.Text & Chr(13) + Chr(10) & "[PAGE]"
                '-> Indiquer qu'il y a au moins une page � analyser
                IsPageToPrint = True
            End If '-> Si les confitions de ruptures sont s�lectionn�es
            '-> Remettre � blanc la collection des lignes dans
            Do While Lignes.Count <> 0
                Lignes.Remove (1)
            Loop
            '-> Indiquer que la page en cours est analys�e
            IsCurrentPageAnalysed = True
            '-> Analyser la ligne suivante
            GoTo NextLig
    End Select

    '-> Si on est en cours de s�lection ne pas lire la igne
    If IsSelection Then GoTo NextLig

    '-> Mettre � jour le contenu de la page dans la collection
    Set aLigne = New clsSpoolLigne
    aLigne.Ligne = Ligne
    Lignes.Add aLigne, "LIG" & Lignes.Count
    
    '-> Indiquer que l'on travaille sur une nouvelle page
    IsCurrentPageAnalysed = False

NextLig:
Loop 'Boucle de lecture du fichier de donn�es

'-> Fermer le fichier ASCII
Close #hdlData

'-> Tester si la page est analys�e
If Not IsCurrentPageAnalysed Then
    If AnalyseRupture(Breaks, Param, Lignes) Then
        '-> Toutes les conditions sont remplies : transf�rer le contenu de la page vers le RTF final
        For Each aLigne In Lignes
            frmProcessDispatch.RtfFile.Text = frmProcessDispatch.RtfFile.Text & Chr(13) & Chr(10) & aLigne.Ligne
        Next
        '-> Indiquer qu'il y a au moins une page � analyser
        IsPageToPrint = True
    End If
End If '-> Si la page en cours n'est pas analys�e

'-> Cr�er un spool d'impression s'il 'y a de quoi imprimer
If IsPageToPrint Then
    '-> Cr�ation d'un nom de fichier temporaire
    Tempfile = Year(Now) & "-" & Format(Month(Now), "00") & "-" & Format(Day(Now), "00") & "-" & Format(Hour(Now), "00") & "-" & Format(Minute(Now), "00") & "-" & Format(Second(Now), "00") & ".turbo"
    '-> Enregistrer le RTF sous ce nom
    frmProcessDispatch.RtfFile.SaveFile PathImpression & Tempfile, rtfText
    '-> MAJ de log
    MajLOG "SPOOL=OK�" & PathImpression & Tempfile
    '-> Envoyer vers l'imprimante
    Select Case UCase$(Entry(1, Imprimante, "�"))
        Case "IMP"
            If UCase$(Entry(2, Imprimante, "�")) = "ECRAN" Then
                Shell App.Path & "\TurboGraph.exe " & "ECRAN|" & PathImpression & Tempfile, vbNormalFocus
            Else
                '-> Lancer l'impression
                Shell App.Path & "\TurboGraph.exe " & Entry(2, Imprimante, "�") & "$DIRET$1|" & PathImpression & Tempfile, vbNormalFocus
            End If
            MajLOG "PRINTFILE=" & Entry(2, Imprimante, "�")
        Case "OUT", "WWW"
            '-> Ajouter � la liste des mails
            If ListeFileOut = "" Then
                ListeFileOut = PathImpression & Tempfile
            Else
                ListeFileOut = ListeFileOut & ";" & PathImpression & Tempfile
            End If
        Case Else
            
            
    End Select
Else
    '-> Indiquer un log d'erreur
    MajLOG "SPOOL=NO�"
End If


Exit Sub

GestError:
    
        '-> Erreur lors de l'acces au fichier
        MajLOG "DATAFILEERROR= Err.Number & " - " & Err.Description"
EndData:
        '-> Fermer le fichier
        Close #hdlData
    

End Sub
Private Sub SendToOutLook()

'---> Cette proc�dure cr�er un mail et l'envoie sur outLook

Dim Out As Object
Dim aMail As Object
Dim i As Integer

On Error Resume Next

'-> Cr�er Outlook
Set Out = CreateObject("Outlook.Application")
Set aMail = Out.CreateItem(0)

'-> Param�trage
If ListeFileOut <> "" Then
    For i = 1 To NumEntries(ListeFileOut, ";")
        aMail.Attachments.Add Entry(i, ListeFileOut, ";")
    Next
End If
aMail.To = SendZone
aMail.Subject = Entry(2, Imprimante, "�")

'-> Charger le corp du message
If Trim(Entry(3, Imprimante, "�")) <> "" Then
    If Dir$(Entry(3, Imprimante, "�"), vbNormal) <> "" Then
        frmProcessDispatch.RTFSend.Text = ""
        frmProcessDispatch.RTFSend.LoadFile Entry(3, Imprimante, "�")
        'frmProcessDispatch.RTFSend.Text = frmProcessDispatch.RTFSend.Text & Chr(13) & Chr(10) & ""
        aMail.Body = frmProcessDispatch.RTFSend.TextRTF
    End If
End If

'-> Envoyer le mail
aMail.Send

'-> Mettre � jour le LOG
MajLOG "SENDOUT=" & SendZone & "  " & ListeFileOut


End Sub
Private Sub SendToInternet()

'---> Cette focntion envoie sur Internet

Dim aMap As MAPIMessages
Dim i As Integer

'-> Initialisation de la session Internet
If Not InitMAPISession Then
    '-> Mettre � jour un log
    MajLOG "SENDNET=Erreur dans l'initialisation de la session Internet"
    Exit Sub
End If

Set aMap = frmProcessDispatch.MAPIMessages1

aMap.MsgIndex = -1

'-> Charger le corp du message
If Trim(Entry(3, Imprimante, "�")) <> "" Then
    If Dir$(Entry(3, Imprimante, "�"), vbNormal) <> "" Then
        frmProcessDispatch.RTFSend.Text = ""
        frmProcessDispatch.RTFSend.LoadFile Entry(3, Imprimante, "�")
        aMap.MsgNoteText = frmProcessDispatch.RTFSend.Text & Chr(13) & Chr(10)
    End If
End If


'-> Envoyer le mail
With aMap
    .RecipAddress = SendZone
    .MsgSubject = Entry(2, Imprimante, "�")
End With

'If frmProcessDispatch.RTFSend.Text <> "" Then aMap.AttachmentPosition = Len(frmProcessDispatch.RTFSend.Text) - 1

If ListeFileOut <> "" Then
    For i = 1 To NumEntries(ListeFileOut, ";")
        aMap.AttachmentIndex = aMap.AttachmentCount
        aMap.AttachmentPathName = Entry(i, ListeFileOut, ";")
    Next
End If
    
'-> Envoyer le message
aMap.Send False

MajLOG "SENDNET=" & SendZone & ListeFileOut

End Sub

Private Function InitMAPISession() As Boolean

'---> Cette proc�dure initialise la session MAPI

On Error GoTo ErrorLogon

'-> Ne pas cr�er de session MAPI s'il y en a d�ja eu une
If frmProcessDispatch.MAPISession1.NewSession Then
    '-> Ne pas cr�er de session
Else
    With frmProcessDispatch.MAPISession1
        .DownLoadMail = False '-> Ne pas charger les e-emails � l'ouverture de la session
        .LogonUI = True '-> Indique si une bo�te de dialogue est affich�e au moment de l'ouverture d'une session
        .SignOn '-> Connecte l'utilisateur au compte indiqu� par les propri�t�s UserName et Password.
        .NewSession = True '-> Indique qu'une session MAPI est en cours
        frmProcessDispatch.MAPIMessages1.SessionID = .SessionID '-> Affecter au message l'ID de session MAPI
    End With
End If

InitMAPISession = True

Exit Function

ErrorLogon:
    InitMAPISession = False

End Function

Private Function AnalyseRupture(Breaks As Collection, Param As String, Lignes As Collection) As Boolean

'---> Cette fonction analyse l'objet TEMPORTF et v�rifier les ruptures

Dim aBreak As clsBreak
Dim i As Integer, j As Integer
Dim Ok As Boolean
Dim TempLig As String
Dim PosX As Integer
Dim ValueField As String

'-> Gestion des erreurs
On Error GoTo GestError

'-> Initialiser la recherche � Ok
Ok = True

'-> analyse de toutes les conditions de ruptutre
For Each aBreak In Breaks
    '-> Rendre la main � la CPU
    DoEvents
    '-> Debase le crit�re n'est pas Ok
    aBreak.IsOk = False
    '-> V�rifier si on le trouve dans la page
    For i = 1 To Lignes.Count
        '-> Get de la lige
        TempLig = Lignes(i).Ligne
        '-> Recherche de la chaine de caract�re
        If InStr(1, UCase$(TempLig), aBreak.IdString) <> 0 Then
            '-> Rechercher la pr�sence du # sp�cifi�
            If InStr(1, UCase$(TempLig), "^" & aBreak.Field) <> 0 Then
                '-> R�cup�ration du prochain ^ ou } de fin de ligne
                PosX = InStr(InStr(1, UCase$(TempLig), "^" & aBreak.Field) + 1, UCase$(TempLig), "^")
                If PosX = 0 Then
                    '-> Recherche du caract�re de fin de ligne
                    PosX = InStr(InStr(1, UCase$(TempLig), "^" & aBreak.Field) + 1, UCase$(TempLig), "}")
                End If
                '-> Si posX=0 Erreur sur la ligne
                If PosX <> 0 Then
                    '-> Get de la valeur du champ
                    ValueField = UCase$(Trim(Mid$(TempLig, InStr(1, UCase$(TempLig), "^" & aBreak.Field) + 5, PosX - (InStr(1, UCase$(TempLig), "^" & aBreak.Field) + 5))))
                    '-> Comparer avec la valeur recherche
                    If aBreak.ValueToMatch = ValueField Then
                        '-> Indiquer que le crit�re est OK
                        aBreak.IsOk = True
                        '-> Analyser le crit�re suivant
                        Exit For
                    End If
                End If 'Si on a trouv� le contenu du #
            End If
        End If 'Si on a trouv� une chaine de correspondance
    Next 'Pour toutes les lignes du spool
Next 'Pour toutes les conditions de ruptures

'-> Faire une v�rification que tous les crit�res sont OK
For Each aBreak In Breaks
    If Not aBreak.IsOk Then
        '-> Envoyer une erreur
        AnalyseRupture = False
        Exit Function
    End If
Next

'-> Renvoyer une valeur de succ�s
AnalyseRupture = True

GestError:
    Exit Function

End Function
Private Function AnalyseEntete(hdlData As Integer, LigLu As Long) As Boolean

'---> Cette function analyse l'entete du spool _
soit � la recherche de la maquette soit pour get de la maquette

'-> Le traitement du spool au format %%GUI%% est d�plac� dans la sous routine _
AnalyseEntete_Format1

Dim Ligne As String
Dim FindSpool As Boolean
Dim MaqLect As Boolean
Dim FindBreak As Boolean
Dim ToPrint As Boolean
Dim TempMaq As String

On Error GoTo GestError

'-> Remettre � blanc la variable de lecture des maquettes format2
MaqString = ""

Do While Not EOF(hdlData)
    '->Lecture de la ligne
    Line Input #hdlData, Ligne
    '-> Passer � la ligne suivante si ligne � blanc
    If Trim(Ligne) = "" Then GoTo NextLig
    '-> Tester si on trouve %%GUI%%
    If InStr(1, UCase$(Ligne), "%%GUI%%") <> 0 Then
        '-> On est dans le format 1 de turboMaq
        IsNewFormat = False
        '-> R�cup�rer le nom de la maquette
        TempMaq = Mid$(Ligne, 8, Len(Ligne) - 7)
        '-> V�rifier si on est en path UNIX ou non
        If PathUNIX <> "" Then
            If InStr(1, TempMaq, "/") <> 0 Then '-> On a trouv� des "/" donc on est en path Unix
                '-> V�rifier que la cl� de substitution NT est OK
                If PathNT = "" Then
                    '-> Path Unix et pas de cortrespondance NT
                    MajLOG "ERROR=Param�trage incorrect : PathUnix et pas de correspondance NT"
                    '-> Quitter la fonction
                    AnalyseEntete = False
                    Exit Function
                Else '-> On est en path UNIX : faire la correspondance avec NT
                    TempMaq = Replace(UCase$(TempMaq), UCase$(PathUNIX), PathNT)
                    TempMaq = Replace(TempMaq, "/", "\")
                End If 'Si on a une correspondance UNIX-> NT
            Else
                '-> On est path NT RAS
            End If 'Selon la nature du path
        Else '-> PathUnix � blanc -> On est path NT
            '-> V�rifier que c'est bien un path NT
            If InStr(1, TempMaq, "/") <> 0 Then
                '-> Erreur pathUnix et pas de correspondances
                MajLOG "ERROR=Pour la convertion UNIX -> NT , le parama�trage du fichier INI doit comprendre une cle PATHUNIX et PATHNT"
                AnalyseEntete = False
                '-> Quitter la fonction
                Exit Function
            End If 'Si on trouve un path Unix
        End If 'Si on est en  path unix ou non
        '-> Analyser l'entete
        AnalyseEntete = AnalyseEntete_Format1(TempMaq)
        '-> Quitter la proc�dure
        Exit Function
    End If
    '-> Ne traiter que les lignes qui commencent par "["
    'If InStr(1, Ligne, "[") = 0 Then GoTo NextLig
    '-> Chercher un cl� SPOOL
    If Not FindSpool Then
        '-> Chercher le d�but de d�finition d'un spool
        If UCase$(Trim(Ligne)) = "[SPOOL]" Then
            '-> Insiquer que l'on a trouv� un spool
            FindSpool = True
            '-> Indiquer que ce fichier de donn�es est au format2
            IsNewFormat = True
            '-> Mettre � jour le RTF
            frmProcessDispatch.RtfFile.Text = "[SPOOL]"
            '-> Indiquer que l'on doit mettre � jour la maquette
            ToPrint = True
        End If
        '-> Analyse de la ligne suivante
        GoTo NextLig
    End If
    '-> Recherche d'une cl� de lecture d'un spool
    If MaqLect Then
        '-> Analyse de la cl� de fin de lecture
        If UCase$(Entry(1, Ligne, "�")) = "\PUBLIPOSTAGE" Then
            '-> Indiquer que l'on a trouv� la rupture
            FindBreak = True
            '-> Mettre � jour la variable globale
            BreakData = Entry(2, Ligne, "�")
        End If
        '-> Quitter si on tombe sur la cl� de fin de maquette
        If UCase$(Ligne) = "[MAQCHAR]" Then ToPrint = False
        If UCase$(Ligne) = "[/MAQ]" Then Exit Do
        '-> Concatainer dans le RTF
        If ToPrint Then frmProcessDispatch.RtfFile.Text = frmProcessDispatch.RtfFile.Text & Chr(13) + Chr(10) & Ligne
    Else
        '-> Chercher la cl� d'ouverture de la maquette
        If UCase$(Trim(Ligne)) = "[MAQ]" Then
            MaqLect = True
            '-> Mettre � jour le RTF
            frmProcessDispatch.RtfFile.Text = frmProcessDispatch.RtfFile.Text & Chr(13) + Chr(10) & "[MAQ]"
            '-> Indiquer dans le spool que l'on � trouv� la maquette
            MajLOG "MAQGUI=SPOOL�"
        End If
        '-> Analyse de la ligne suivante
        GoTo NextLig
    End If
            
NextLig:
Loop 'Boucle d'analyse de l' entete du fichier

'-> Mettre le Tag de fin de lecture du RTF
frmProcessDispatch.RtfFile.Text = frmProcessDispatch.RtfFile.Text & Chr(13) + Chr(10) & "[/MAQ]"

'-> V�rifier si on a trouv� la cl� de publipostage
If Not FindBreak Then
    '-> Imprimer un LOG
    MajLOG "RUPTURE=NOMATCH�"
    '-> Renvoyer une valeur d'erreur
    AnalyseEntete = False
    '-> Quitter la fonction
    Exit Function
End If

'-> V�rifier si la cle de rupture est renseign�e
If Trim(BreakData) = "" Then
    '-> Imprimer un LOG
    MajLOG "RUPTURE=BLANK�"
    '-> Renvoyer une valeur d'erreur
    AnalyseEntete = False
    '-> Quitter la fonction
    Exit Function
End If

'-> Renvoyer une valeur de succ�s
AnalyseEntete = True

'-> Imprimer un LOG de succ�s
MajLOG "RUPTURE=MATCH�" & BreakData

'-> Quitter la proc�dure
Exit Function

GestError:

    '-> Erreur : renvoyer une erreur
    MajLOG "MAQGUI=ERROR�" & Err.Number & " - " & Err.Description
    '-> Renvoyer une valeur d'erreur
    AnalyseEntete = False
    


End Function

Private Function AnalyseEntete_Format1(ByVal NomMaquette As String) As Boolean

'---> Cette proc�dure analyse la maquette

Dim hdlMaq As Integer
Dim Ligne As String
Dim FindTag As Boolean

On Error GoTo GestError

'-> V�rifier que la maquette existe bien
If Dir$(NomMaquette, vbNormal) = "" Then
    '-> Imprimer un log d'erreur
    MajLOG "MAQGUI=NOMATCH�" & NomMaquette
    '-> Quitter la fonction
    AnalyseEntete_Format1 = False
    Exit Function
End If

'-> Indiquer la maquette trait�e
MajLOG "MAQGUI=MATCH�" & NomMaquette

'-> Ouverture de la maquette
hdlMaq = FreeFile
Open NomMaquette For Input As #hdlMaq

'-> Lecture s�quentielle de maquette
Do While Not EOF(hdlMaq)
    '-> Lecture de la ligne
    Line Input #hdlMaq, Ligne
    '-> Passer � la ligne suivante si ligne = ""
    If Trim(Ligne) = "" Then GoTo NextLig
    '-> Selon la nature de la ligne
    Select Case UCase$(Entry(1, Ligne, "�"))
        Case "\PUBLIPOSTAGE"
            '-> Indiquer que l'on a trouv� la cl�
            FindTag = True
            '-> Get des cl� de rupture
            BreakData = Entry(2, Ligne, "�")
            '-> Sortir de la proc�dure
            Exit Do
        Case "\DEFENTETE"
            If UCase$(Entry(2, Ligne, "�")) = "END" Then Exit Do
    End Select
    
NextLig:
Loop

'-> fermer le fichier de la maquette
Close #hdlMaq

'-> V�rifier si on a trouv� la cl� de rupture
If Not FindTag Then
    '-> Imprimer un LOG
    MajLOG "RUPTURE=NOMATCH�"
    '-> Renvoyer une valeur d'erreur
    AnalyseEntete_Format1 = False
    '-> Quitter la fonction
    Exit Function
End If

'-> V�rifier que la cl� de rupture est renseign�e
If Trim(BreakData) = "" Then
    '-> Imprimer un LOG
    MajLOG "RUPTURE=BLANCK�"
    '-> Renvoyer une valeur d'erreur
    AnalyseEntete_Format1 = False
    '-> Quitter la fonction
    Exit Function
End If

'-> Mettre � jour le fichier RTF
frmProcessDispatch.RtfFile.LoadFile NomMaquette, rtfText
frmProcessDispatch.RtfFile.Text = "[SPOOL]" & Chr(13) & Chr(10) & "[MAQ]" & Chr(13) & Chr(10) & frmProcessDispatch.RtfFile.Text & Chr(13) & Chr(10) & "[/MAQ]"
'frmProcessDispatch.RtfFile.Text = "%%GUI%%" & NomMaquette

'-> tout est OK : renvoyer une valeur de succ�s
AnalyseEntete_Format1 = True

'-> Positionner la variable qui indique l'enplacement de la maquette
MaqString = NomMaquette

'-> Imprimer un LOG de succ�s
MajLOG "RUPTURE=MATCH�" & BreakData

'-> Quitter la proc�dure
Exit Function

GestError:

    '-> Erreur : renvoyer une erreur
    MajLOG "MAQGUI=ERROR�" & Err.Number & " - " & Err.Description
    '-> Renvoyer une valeur d'erreur
    AnalyseEntete_Format1 = False
    '-> Fermer le fichier si necessaire
    If hdlMaq <> 0 Then Close #hdlMaq
    

End Function

Public Sub MajLOG(ByVal LogToprint As String)

'---> Cette fonction met � jour le fichier LOG

'-> Quitter si pas acc�s au log
If Not IsLog Then Exit Sub

'-> Imprimer la chaine de traitement
Print #hdlLog, LogToprint


End Sub

Private Sub OpenLOG()

'---> Cette fonction ouvre le fichier des logs

On Error Resume Next

Dim Tempfile As String

If Not IsLog Then Exit Sub

'-> Cr�er le nom du fichier LOG
Tempfile = Entry(1, PiloteFile, ".")

'-> Ouverture d'un fichier LOG
hdlLog = FreeFile
Open PathLog & Tempfile & ".log" For Output As #hdlLog

'-> Ecrire les commentaires de d�but
Print #hdlLog, "BEGIN=" & Now
Print #hdlLog, "PILOTEFILE=" & PathSpool & PiloteFile

End Sub

Public Sub CloseLOG()

On Error Resume Next

'---> Cette proc�dure ferme le fichier des LOG
If Not IsLog Then Exit Sub
Close #hdlLog

End Sub
