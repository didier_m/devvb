VERSION 5.00
Begin VB.Form frmPrint 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Choix d'une imprimante de destination "
   ClientHeight    =   2745
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6735
   Icon            =   "frmPrint.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2745
   ScaleWidth      =   6735
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "Sélectionner"
      Height          =   375
      Left            =   5160
      TabIndex        =   1
      Top             =   2280
      Width           =   1455
   End
   Begin VB.ListBox List1 
      Height          =   2010
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   6615
   End
End
Attribute VB_Name = "frmPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

If Me.List1.ListIndex = -1 Then
    MsgBox "Veuillez sélectionner une imprimante", vbExclamation + vbOKOnly, "Erreur"
    Me.List1.SetFocus
    Exit Sub
End If

StrRetour = Me.List1.Text
Unload Me

End Sub

Private Sub Form_Load()

Dim x As Printer

'---> Charger la liste des imprimantes

For Each x In Printers
    Me.List1.AddItem x.DeviceName
    If x.DeviceName = Printer.DeviceName Then Me.List1.Selected(Me.List1.ListCount - 1) = True
Next

End Sub
