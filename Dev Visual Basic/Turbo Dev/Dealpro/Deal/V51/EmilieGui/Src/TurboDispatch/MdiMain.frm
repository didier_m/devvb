VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm MdiMain 
   BackColor       =   &H8000000C&
   Caption         =   "TurboDispatch"
   ClientHeight    =   5460
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   9720
   Icon            =   "MdiMain.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2400
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiMain.frx":030A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiMain.frx":075C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiMain.frx":0A76
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiMain.frx":0EC8
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiMain.frx":11E2
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9720
      _ExtentX        =   17145
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PARAM"
            Object.ToolTipText     =   "Feuille de param�trage"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "RUN"
            Object.ToolTipText     =   "Lancer un traitement"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "LISTPROCESS"
            Object.ToolTipText     =   "Liste des process actifs"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "LOG"
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "Fichier"
      Begin VB.Menu mnuExit 
         Caption         =   "Quitter"
      End
   End
   Begin VB.Menu mnufenetre 
      Caption         =   "Fen�tre"
      Begin VB.Menu mnuMosH 
         Caption         =   "Mosa�que &horizontale"
      End
      Begin VB.Menu mnuMosV 
         Caption         =   "Mosa�que &verticale"
      End
      Begin VB.Menu mnuCascade 
         Caption         =   "En &cascade"
      End
      Begin VB.Menu mnuIcones 
         Caption         =   "R�organiser les icones"
      End
   End
   Begin VB.Menu mnuApp 
      Caption         =   "?"
      Begin VB.Menu mnuApropos 
         Caption         =   ""
      End
      Begin VB.Menu mnuAide 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "MdiMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub MDIForm_Load()

'---> Point d'entr�e du programme

Dim TempDirectory As String
Dim aLb As Libelle
Dim Res As Long
Dim lpBuffer As String




On Error Resume Next
    
RessourcePath = GetPath(App.Path)
    
    
frmstart.Show vbModal

'-> GESTION DES MESSPROGS
CreateMessProg "TurboDispatch-" & Format(IdLangue, "00") & ".lng", True
CreateMessProg "Common-" & Format(IdLangue, "00") & ".lng", True

    
'-> Pointer sur la fgueille des libelles
Set aLb = Libelles("MDIMAIN")

Me.Caption = aLb.GetCaption(10)
Me.mnuFile.Caption = aLb.GetCaption(1)
Me.mnuExit.Caption = aLb.GetCaption(4)
Me.mnufenetre.Caption = aLb.GetCaption(5)
Me.mnuMosH.Caption = aLb.GetCaption(6)
Me.mnuMosV.Caption = aLb.GetCaption(7)
Me.mnuCascade.Caption = aLb.GetCaption(8)
Me.mnuIcones.Caption = aLb.GetCaption(9)
Me.Toolbar1.Buttons("PARAM").ToolTipText = aLb.GetCaption(11)
Me.Toolbar1.Buttons("RUN").ToolTipText = aLb.GetCaption(12)
Me.Toolbar1.Buttons("LISTPROCESS").ToolTipText = aLb.GetCaption(16)
Me.Toolbar1.Buttons("LOG").ToolTipText = aLb.GetCaption(18)
Me.mnuApropos.Caption = aLb.GetCaption(19)
Me.mnuAide.Caption = aLb.GetCaption(20)
    
'-> Param�trage
If Not Initialisation() Then
    '-> Message d'erreur
    MsgBox aLb.GetCaption(13) & IniPath, vbCritical + vbOKOnly, aLb.GetToolTip(13)
    '-> Quitter le programme
    End
End If
    
'-> Au d�mmarage il n'y a pas de batch qui tournent
IsBatchRunning = False

'-> Initialiser la collection des process
Set Process = New Collection


End Sub

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)

'-> Demande de fin de console

Dim aLb As Libelle
Dim Rep As VbMsgBoxResult

'-> Pointer sur la classe libell�
Set aLb = Libelles("MDIMAIN")


'-> V�rifier qu'il n'y a pas de process en cours
If Process.Count <> 0 Then
    MsgBox aLb.GetCaption(14), vbExclamation + vbOKOnly, aLb.GetToolTip(14)
    Cancel = 1
    Exit Sub
End If

'-> Demander confirmation
Rep = MsgBox(aLb.GetCaption(15), vbQuestion + vbYesNo, aLb.GetToolTip(15))
If Rep = vbNo Then
    Cancel = 1
    Exit Sub
End If

'-> Terminer le programe
End
    

End Sub

Private Sub mnuApropos_Click()

AproposDe.Show vbModal

End Sub

Private Sub mnuCascade_Click()

Me.Arrange vbCascade

End Sub


Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuIcones_Click()

Me.Arrange vbArrangeIcons

End Sub

Private Sub mnuMosH_Click()

Me.Arrange vbTileHorizontal

End Sub

Private Sub mnuMosV_Click()

Me.Arrange vbTileVertical

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim aLb As Libelle

Set aLb = Libelles("MDIMAIN")

Select Case Button.Key
    Case "PARAM"
        frmParam.Show vbModal
    Case "RUN"
        frmRun.Show
        frmRun.ZOrder
    Case "LISTPROCESS"
        frmListeProcess.Show
        frmListeProcess.ZOrder
    Case "LOG"
        frmGestLog.Show
    



End Select

End Sub

