VERSION 5.00
Begin VB.Form frmParam 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "TuboDispatch"
   ClientHeight    =   3570
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7455
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3570
   ScaleWidth      =   7455
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "Param�trage : "
      Height          =   3375
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   7335
      Begin VB.TextBox Text3 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2280
         TabIndex        =   14
         Top             =   720
         Width           =   4575
      End
      Begin VB.TextBox Text7 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2280
         TabIndex        =   12
         Top             =   1440
         Width           =   4575
      End
      Begin VB.TextBox Text6 
         Height          =   285
         Left            =   2280
         TabIndex        =   10
         Top             =   2520
         Width           =   4575
      End
      Begin VB.TextBox Text5 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2280
         TabIndex        =   8
         Top             =   1080
         Width           =   4575
      End
      Begin VB.TextBox Text4 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2280
         TabIndex        =   6
         Top             =   360
         Width           =   4575
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   2280
         TabIndex        =   4
         Top             =   2160
         Width           =   4575
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2280
         TabIndex        =   2
         Top             =   1800
         Width           =   4575
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Enregistrer"
         Height          =   255
         Left            =   5400
         TabIndex        =   1
         Top             =   3000
         Width           =   1455
      End
      Begin VB.Label Label7 
         Caption         =   "R�pertoire des LOG : "
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   720
         Width           =   1575
      End
      Begin VB.Image Image4 
         Height          =   240
         Left            =   6960
         Picture         =   "frmDisplay.frx":0000
         Top             =   720
         Width           =   240
      End
      Begin VB.Label Label3 
         Caption         =   "R�pertoire d'impression : "
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   1440
         Width           =   2055
      End
      Begin VB.Image Image6 
         Height          =   240
         Left            =   6960
         Picture         =   "frmDisplay.frx":014A
         Top             =   1440
         Width           =   240
      End
      Begin VB.Label Label2 
         Caption         =   "Entension des fichiers :"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   2520
         Width           =   2055
      End
      Begin VB.Image Image5 
         Height          =   240
         Left            =   6960
         Picture         =   "frmDisplay.frx":0294
         Top             =   1080
         Width           =   240
      End
      Begin VB.Label Label1 
         Caption         =   "R�pertoire d'archivage :"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   1080
         Width           =   2055
      End
      Begin VB.Image Image2 
         Height          =   240
         Left            =   720
         Picture         =   "frmDisplay.frx":03DE
         Top             =   3000
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Label Label11 
         Caption         =   "R�pertoire des spools : "
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   1815
      End
      Begin VB.Image Image3 
         Height          =   240
         Left            =   6960
         Picture         =   "frmDisplay.frx":0528
         Top             =   360
         Width           =   240
      End
      Begin VB.Label Label10 
         Caption         =   "Path UNIX  :"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   2160
         Width           =   2055
      End
      Begin VB.Label Label6 
         Caption         =   "Path NT :"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   1800
         Width           =   2055
      End
      Begin VB.Image Image1 
         Height          =   240
         Left            =   6960
         Picture         =   "frmDisplay.frx":0672
         Top             =   1800
         Width           =   240
      End
   End
End
Attribute VB_Name = "frmParam"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub Command2_Click()

'---> Enregistrer les param�trages du TurboDispath

Dim Res As Long

'-> Renregistrer le fichier
Res = WritePrivateProfileString("PARAM", "LOGDIRECTORY", Me.Text3.Text, IniPath)
Res = WritePrivateProfileString("PARAM", "SPOOLDIRECTORY", Me.Text4.Text, IniPath)
Res = WritePrivateProfileString("PARAM", "ARCHIVEDIRECTORY", Me.Text5.Text, IniPath)
Res = WritePrivateProfileString("PARAM", "IMPDIRECTORY", Me.Text7.Text, IniPath)
Res = WritePrivateProfileString("PARAM", "PATHNT", Me.Text1.Text, IniPath)
Res = WritePrivateProfileString("PARAM", "PATHUNIX", Me.Text2.Text, IniPath)
Res = WritePrivateProfileString("PARAM", "EXTENSION", Me.Text6.Text, IniPath)


'-> Mettre � jour les variables globales
PathLog = Me.Text3.Text
PathSpool = Me.Text4.Text
PathArchive = Me.Text5.Text
PathNT = Me.Text1.Text
PathUNIX = Me.Text2.Text
Extension = Me.Text6.Text
PathImpression = Me.Text7.Text

MsgBox "Param�trage enregistr�", vbInformation + vbOKOnly, "Confirmation"

End Sub

Private Sub Form_Load()

Dim Res As Long
Dim lpBuffer As String
Dim aLb As Libelle

'On Error Resume Next

'-> gestion des libell�s
Set aLb = Libelles("FRMPARAM")

Me.Caption = aLb.GetCaption(8)
Me.Frame2.Caption = aLb.GetCaption(1)
Me.Label7.Caption = aLb.GetCaption(2)
Me.Label11.Caption = aLb.GetCaption(3)
Me.Label6.Caption = aLb.GetCaption(4)
Me.Label10.Caption = aLb.GetCaption(5)
Me.Command2.Caption = aLb.GetCaption(6)
Me.Image1.ToolTipText = aLb.GetCaption(7)
Me.Image3.ToolTipText = Me.Image1.ToolTipText
Me.Image4.ToolTipText = Me.Image1.ToolTipText
Me.Image5.ToolTipText = Me.Image1.ToolTipText
Me.Image6.ToolTipText = Me.Image1.ToolTipText
Me.Label1.Caption = aLb.GetCaption(10)
Me.Label2.Caption = aLb.GetCaption(11)


'-> Afficher les valeurs des champs
Me.Text3.Text = PathLog
Me.Text4.Text = PathSpool
Me.Text5.Text = PathArchive
Me.Text1.Text = PathNT
Me.Text2.Text = PathUNIX
Me.Text6.Text = Extension
Me.Text7.Text = PathImpression

'-> Bloquage s'il y a des traityements en cours
If IsBatchRunning Then BloqueParam
    
End Sub
Public Sub BloqueParam()

Me.Image1.Picture = Me.Image2.Picture
Me.Image1.Enabled = False
Me.Image4.Picture = Me.Image2.Picture
Me.Image4.Enabled = False
Me.Image3.Picture = Me.Image2.Picture
Me.Image3.Enabled = False
Me.Image5.Picture = Me.Image2.Picture
Me.Image5.Enabled = False
Me.Image6.Picture = Me.Image2.Picture
Me.Image6.Enabled = False
Me.Command2.Enabled = False
Me.Text2.Enabled = False
Me.Text6.Enabled = False


End Sub


Private Sub Image1_DblClick()

Dim Rep As String

Rep = GetPathForm(Me.hwnd, False)
If Trim(Rep) <> "" Then Me.Text1.Text = Rep

'-> Ajouter un \ � la fin
If Right(Me.Text1.Text, 1) <> "\" Then Me.Text1.Text = Me.Text1.Text & "\"

End Sub

Private Sub Image2_DblClick()


If Dir$(Me.Text3.Text & "TurboDispatch.log", vbNormal) = "" Then
    MsgBox "Pas de fichier des LOG � visualiser", vbExclamation + vbOKOnly, "Message"
    Exit Sub
End If

'-> Bloquer les menus
frmEditor.mnuEdition.Enabled = False
frmEditor.mnuOpen.Enabled = False
frmEditor.mnuSave.Enabled = False
frmEditor.mnuSaveAs.Enabled = False

'-> Charger le fichier
frmEditor.RichTextBox1.LoadFile Me.Text3.Text & "TurboDispatch.log"

'-> Afficher la feuille
frmEditor.Show vbModal


End Sub





Private Sub Image3_DblClick()

Dim Rep As String

Rep = GetPathForm(Me.hwnd, False)
If Trim(Rep) <> "" Then Me.Text4.Text = Rep

'-> Ajouter un \ � la fin
If Right(Me.Text4.Text, 1) <> "\" Then Me.Text4.Text = Me.Text4.Text & "\"


End Sub

Private Sub Image4_DblClick()

Dim Rep As String

Rep = GetPathForm(Me.hwnd, False)
If Trim(Rep) <> "" Then Me.Text3.Text = Rep

'-> Ajouter un \ � la fin
If Right(Me.Text3.Text, 1) <> "\" Then Me.Text3.Text = Me.Text3.Text & "\"

End Sub


Private Sub Image5_DblClick()

Dim Rep As String

Rep = GetPathForm(Me.hwnd, False)
If Trim(Rep) <> "" Then Me.Text5.Text = Rep

'-> Ajouter un \ � la fin
If Right(Me.Text5.Text, 1) <> "\" Then Me.Text5.Text = Me.Text5.Text & "\"


End Sub

Private Sub Image6_DblClick()

Dim Rep As String

Rep = GetPathForm(Me.hwnd, False)
If Trim(Rep) <> "" Then Me.Text7.Text = Rep

'-> Ajouter un \ � la fin
If Right(Me.Text7.Text, 1) <> "\" Then Me.Text7.Text = Me.Text7.Text & "\"

End Sub
