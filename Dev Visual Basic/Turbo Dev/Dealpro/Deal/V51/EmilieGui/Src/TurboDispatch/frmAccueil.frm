VERSION 5.00
Begin VB.Form frmstart 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   3870
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5805
   LinkTopic       =   "Form1"
   ScaleHeight     =   3870
   ScaleWidth      =   5805
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   3735
      Left            =   80
      TabIndex        =   0
      Top             =   30
      Width           =   5625
      Begin VB.Frame Frame2 
         Caption         =   "Choix de la langue"
         Height          =   1335
         Left            =   120
         TabIndex        =   5
         Top             =   2160
         Width           =   2175
         Begin VB.Image Image3 
            Height          =   480
            Index           =   0
            Left            =   120
            Picture         =   "frmAccueil.frx":0000
            ToolTipText     =   "Fran�ais"
            Top             =   240
            Width           =   480
         End
         Begin VB.Image Image3 
            Height          =   480
            Index           =   1
            Left            =   120
            Picture         =   "frmAccueil.frx":0442
            ToolTipText     =   "Anglais"
            Top             =   720
            Width           =   480
         End
         Begin VB.Image Image3 
            Height          =   480
            Index           =   2
            Left            =   840
            Picture         =   "frmAccueil.frx":0884
            ToolTipText     =   "Allemand"
            Top             =   240
            Width           =   480
         End
         Begin VB.Image Image3 
            Height          =   480
            Index           =   3
            Left            =   840
            Picture         =   "frmAccueil.frx":0CC6
            ToolTipText     =   "Portugais"
            Top             =   720
            Width           =   480
         End
      End
      Begin VB.PictureBox Picture1 
         BackColor       =   &H00FFFFFF&
         Height          =   1335
         Left            =   120
         Picture         =   "frmAccueil.frx":1108
         ScaleHeight     =   1275
         ScaleWidth      =   1395
         TabIndex        =   1
         Top             =   240
         Width           =   1455
      End
      Begin VB.Line Line4 
         X1              =   3360
         X2              =   3720
         Y1              =   2280
         Y2              =   2280
      End
      Begin VB.Line Line3 
         X1              =   4440
         X2              =   4800
         Y1              =   2280
         Y2              =   2280
      End
      Begin VB.Line Line2 
         X1              =   4080
         X2              =   4080
         Y1              =   2640
         Y2              =   2880
      End
      Begin VB.Line Line1 
         X1              =   4080
         X2              =   4080
         Y1              =   1680
         Y2              =   1920
      End
      Begin VB.Shape Shape6 
         Height          =   735
         Left            =   4800
         Top             =   1920
         Width           =   735
      End
      Begin VB.Shape Shape5 
         Height          =   735
         Left            =   3720
         Top             =   2880
         Width           =   735
      End
      Begin VB.Shape Shape4 
         Height          =   735
         Left            =   2640
         Top             =   1920
         Width           =   735
      End
      Begin VB.Shape Shape3 
         Height          =   735
         Left            =   3720
         Top             =   960
         Width           =   735
      End
      Begin VB.Shape Shape2 
         Height          =   735
         Left            =   3720
         Top             =   1920
         Width           =   735
      End
      Begin VB.Image Image6 
         Height          =   480
         Left            =   4920
         Picture         =   "frmAccueil.frx":65AA
         Top             =   2040
         Width           =   480
      End
      Begin VB.Image Image5 
         Height          =   240
         Left            =   3840
         Picture         =   "frmAccueil.frx":68B4
         Top             =   1080
         Width           =   240
      End
      Begin VB.Image Image7 
         Height          =   480
         Left            =   3840
         Picture         =   "frmAccueil.frx":6CF6
         Top             =   3000
         Width           =   480
      End
      Begin VB.Image Image4 
         Height          =   480
         Left            =   2760
         Picture         =   "frmAccueil.frx":75C0
         Top             =   2040
         Width           =   480
      End
      Begin VB.Shape Shape1 
         Height          =   1935
         Left            =   3000
         Top             =   1320
         Width           =   2175
      End
      Begin VB.Image Image2 
         Height          =   480
         Left            =   3840
         Picture         =   "frmAccueil.frx":7E8A
         Top             =   2040
         Width           =   480
      End
      Begin VB.Label Label2 
         Caption         =   "Gestionnaire de publipostage au format TURBO"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   1680
         Width           =   2535
      End
      Begin VB.Label Label1 
         Caption         =   "Outil de publipostage"
         Height          =   375
         Left            =   1800
         TabIndex        =   3
         Top             =   840
         Width           =   2895
      End
      Begin VB.Image Image1 
         Height          =   165
         Left            =   5400
         Picture         =   "frmAccueil.frx":8194
         Top             =   180
         Width           =   165
      End
      Begin VB.Label Label4 
         Caption         =   "Deal Informatique"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1800
         TabIndex        =   2
         Top             =   240
         Width           =   3255
      End
   End
   Begin VB.Shape Shape7 
      Height          =   3855
      Left            =   0
      Top             =   0
      Width           =   5775
   End
End
Attribute VB_Name = "frmstart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Image1_Click()

End

End Sub

Private Sub Image3_Click(Index As Integer)

'---> V�rifier que les fichiers existent

Dim strId As String

strId = Format(Index + 1, "00")

If Dir$(RessourcePath & "TurboDispatch-" & strId & ".lng", vbNormal) = "" Then
    MsgBox "Impossible de trouver le fichier langue sp�cifi�", vbCritical + vbOKOnly, "Erreur"
    Exit Sub
End If

If Dir$(RessourcePath & "Common-" & strId & ".lng", vbNormal) = "" Then
    MsgBox "Impossible de trouver le fichier langue sp�cifi�", vbCritical + vbOKOnly, "Erreur"
    Exit Sub
End If

IdLangue = Index + 1
Unload Me




End Sub
