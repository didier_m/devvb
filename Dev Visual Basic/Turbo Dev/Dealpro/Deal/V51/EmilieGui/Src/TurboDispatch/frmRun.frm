VERSION 5.00
Begin VB.Form frmRun 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   6090
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5280
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6090
   ScaleWidth      =   5280
   Begin VB.CheckBox Check3 
      Caption         =   "Ouvrir le fichier LOG � la fin du traitement"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   5160
      Width           =   4335
   End
   Begin VB.CheckBox Check2 
      Caption         =   "Archiver le fichier � la fin du traitement"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   4800
      Width           =   3135
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Lancer le traitement"
      Height          =   375
      Left            =   2280
      TabIndex        =   4
      Top             =   5640
      Width           =   2895
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Mettre � jour le fichier LOG"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   4440
      Width           =   2415
   End
   Begin VB.FileListBox File1 
      Height          =   3600
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   5055
   End
   Begin VB.Label Label1 
      Caption         =   "S�lection du fichier � analyser : "
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   240
      Width           =   2415
   End
End
Attribute VB_Name = "frmRun"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

Dim aLb As Libelle
Dim aProcess As clsProcess
Dim aFrm As FrmProcess

Set aLb = Libelles("FRMRUN")

'-> V�rifier si un fichier est s�lectionn�
If Me.File1.ListIndex = -1 Then
    MsgBox aLb.GetCaption(9), vbExclamation + vbOKOnly, aLb.GetToolTip(9)
    Exit Sub
End If

'-> V�rifier que l programme de process d�ttach� est l�
If Dir$(App.Path & "\ProcessDispatch.exe", vbNormal) = "" Then
    MsgBox aLb.GetCaption(10), vbExclamation + vbOKOnly, aLb.GetToolTip(10)
    Exit Sub
End If

'-> Afficher le choix de l'imprimante
StrRetour = ""
frmDestination.Show vbModal

If StrRetour = "" Then Exit Sub

    
'-> Impl�menter un nouvel objet
Set aProcess = New clsProcess

'-> Setting des donn�es
aProcess.DateDebut = Now
aProcess.FichierPilote = Me.File1.List(Me.File1.ListIndex)
If Me.Check1.Value = 1 Then aProcess.IsLog = True
If Me.Check2.Value = 1 Then aProcess.IsArchive = True
aProcess.Key = "KEY|" & IdProcess
aProcess.FichierLog = PathLog & Entry(1, Me.File1.Filename, ".") & ".log"

'-> Ajouter dans la collection
Process.Add aProcess, "KEY|" & IdProcess

'-> Incr�menter le compteur de process
IdProcess = IdProcess + 1

'-> Charger une feuille de propri�t�
Set aFrm = New FrmProcess
Set aFrm.aProcess = aProcess
Set aProcess.FrmProcess = aFrm

'-> Mettre � jour la variable globale qui indique qu'il y a un traitement en cours
IsBatchRunning = True

'-> R�cup�rer la taille du ficher
aFrm.NbTotal = FileLen(PathSpool & Me.File1.List(Me.File1.ListIndex))

'-> Lancer le process de dispatch
aProcess.HdlProcess = Shell(App.Path & "\ProcessDispatch.exe " & aFrm.PicCom.hwnd & "|" & Me.File1.List(Me.File1.ListIndex) & "|" & Me.Check1.Value & "|" & Me.Check2.Value & "|%%GUI%%|" & StrRetour, vbNormalFocus)
    
'-> Affichage des param�tres
aFrm.Visible = True
aFrm.Caption = aFrm.Caption & aProcess.HdlProcess
aFrm.lblFichierPilote = PathSpool & aProcess.FichierPilote
    
'-> Indiquer si on doit ouvrir la feuille des LOG
If Me.Check3.Value = 1 Then aFrm.EditLog = True
    
'-> Afficher la lisgne dans le browse
If IsListeProcess Then aProcess.AddToBrowse
    
'-> Faire un raffraichissement
Me.File1.Refresh
    
End Sub

Private Sub Form_Load()

Dim aLb As Libelle

On Error GoTo GestError

'-> Gestion des messprogs
Set aLb = Libelles("FRMRUN")
Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Check1.Caption = aLb.GetCaption(3)
Me.Check2.Caption = aLb.GetCaption(5)
Me.Check3.Caption = aLb.GetCaption(13)
Me.Command1.Caption = aLb.GetCaption(4)

'-> V�rifier si on trouve le r�pertoire d'analyse des spools
If Dir$(PathSpool, vbDirectory) <> "" Then
    If GetAttr(PathSpool) <> vbDirectory Then
        MsgBox aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetToolTip(6)
        Me.Command1.Enabled = False
    Else
        '-> Setting du r�peroire d'analyse
        Me.File1.Path = PathSpool
        Me.File1.Filename = Extension
    End If
Else
    MsgBox aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetToolTip(6)
    Me.Command1.Enabled = False
End If

'-> V�rifier si on trouve le r�pertoire des LOG
If Dir$(PathLog, vbDirectory) <> "" Then
    If GetAttr(PathLog) <> vbDirectory Then
        MsgBox aLb.GetCaption(7), vbCritical + vbOKOnly, aLb.GetToolTip(7)
        Me.Check1.Value = 0
        Me.Check1.Enabled = False
        Me.Check3.Value = 0
        Me.Check3.Enabled = False
    Else
        Me.Check1.Value = 1
        Me.Check3.Value = 1
    End If
Else
    MsgBox aLb.GetCaption(7), vbCritical + vbOKOnly, aLb.GetToolTip(7)
    Me.Check1.Value = 0
    Me.Check1.Enabled = False
    Me.Check3.Value = 0
    Me.Check3.Enabled = False
End If

'-> V�rifier si on trouve le r�pertoire d'archivage
If Dir$(PathArchive, vbDirectory) <> "" Then
    If GetAttr(PathArchive) <> vbDirectory Then
        MsgBox aLb.GetCaption(8), vbCritical + vbOKOnly, aLb.GetToolTip(8)
        Me.Check2.Value = 0
        Me.Check2.Enabled = False
    Else
        Me.Check2.Value = 1
    End If
Else
    MsgBox aLb.GetCaption(8), vbCritical + vbOKOnly, aLb.GetToolTip(8)
    Me.Check2.Value = 0
    Me.Check2.Enabled = False
End If

'-> V�rifier si on trouve le r�pertoire d'impression
If Dir$(PathImpression, vbDirectory) <> "" Then
    If GetAttr(PathImpression) <> vbDirectory Then
        MsgBox aLb.GetCaption(11), vbCritical + vbOKOnly, aLb.GetToolTip(11)
        Me.Command1.Enabled = False
    End If
Else
    MsgBox aLb.GetCaption(11), vbCritical + vbOKOnly, aLb.GetToolTip(11)
    Me.Command1.Enabled = False
End If

'-> S�lectionner un fichier par d�faut
If Me.File1.ListCount <> 0 Then Me.File1.Selected(0) = True


Exit Sub

GestError:

    MsgBox "Erreur lors du chargement. Veuillez v�rifier les r�pertoires", vbCritical + vbOKOnly, "Erreur"
    Me.Command1.Enabled = False
    

End Sub
