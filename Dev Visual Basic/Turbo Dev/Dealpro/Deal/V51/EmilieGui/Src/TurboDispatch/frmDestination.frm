VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmDestination 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   7410
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6750
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7410
   ScaleWidth      =   6750
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2280
      Top             =   5400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDestination.frx":0000
            Key             =   "TURBO"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDestination.frx":08DA
            Key             =   "OUTLOOK"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDestination.frx":0BF4
            Key             =   "INTERNET"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDestination.frx":14CE
            Key             =   "PRINTER"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDestination.frx":1DA8
            Key             =   "SEND"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7335
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6735
      _ExtentX        =   11880
      _ExtentY        =   12938
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   1058
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmDestination.frx":21FA
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Shape1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "ListView1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Command1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmDestination.frx":2216
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Command2"
      Tab(1).Control(1)=   "CommonDialog1"
      Tab(1).Control(2)=   "Command3"
      Tab(1).Control(3)=   "RichTextBox1"
      Tab(1).Control(4)=   "Text2"
      Tab(1).Control(5)=   "Image5"
      Tab(1).Control(6)=   "Image4"
      Tab(1).Control(7)=   "Image3"
      Tab(1).Control(8)=   "Image2"
      Tab(1).Control(9)=   "Image1"
      Tab(1).Control(10)=   "Label2"
      Tab(1).Control(11)=   "Label1"
      Tab(1).Control(12)=   "Shape2"
      Tab(1).ControlCount=   13
      Begin VB.CommandButton Command2 
         Caption         =   "Envoyer"
         Height          =   975
         Left            =   -74760
         Picture         =   "frmDestination.frx":2232
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   5040
         Width           =   855
      End
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   -73320
         Top             =   5400
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Envoyer"
         Height          =   975
         Left            =   -74760
         Picture         =   "frmDestination.frx":253C
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   6120
         Width           =   855
      End
      Begin RichTextLib.RichTextBox RichTextBox1 
         Height          =   5655
         Left            =   -73800
         TabIndex        =   5
         Top             =   1440
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   9975
         _Version        =   393217
         TextRTF         =   $"frmDestination.frx":2E06
      End
      Begin VB.TextBox Text2 
         Height          =   350
         Left            =   -73800
         TabIndex        =   3
         Top             =   960
         Width           =   5295
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Imprimer"
         Height          =   375
         Left            =   4680
         TabIndex        =   2
         Top             =   6720
         Width           =   1815
      End
      Begin MSComctlLib.ListView ListView1 
         Height          =   5535
         Left            =   240
         TabIndex        =   1
         Top             =   960
         Width           =   6255
         _ExtentX        =   11033
         _ExtentY        =   9763
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         GridLines       =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.Image Image5 
         Height          =   480
         Left            =   -74760
         Picture         =   "frmDestination.frx":2EE0
         Top             =   4080
         Width           =   480
      End
      Begin VB.Image Image4 
         Height          =   480
         Left            =   -74760
         Picture         =   "frmDestination.frx":31EA
         Top             =   3480
         Width           =   480
      End
      Begin VB.Image Image3 
         Height          =   480
         Left            =   -74760
         Picture         =   "frmDestination.frx":34F4
         Top             =   2880
         Width           =   480
      End
      Begin VB.Image Image2 
         Height          =   480
         Left            =   -74760
         Picture         =   "frmDestination.frx":37FE
         Top             =   2280
         Width           =   480
      End
      Begin VB.Image Image1 
         Height          =   480
         Left            =   -74760
         Picture         =   "frmDestination.frx":3C40
         Top             =   1680
         Width           =   480
      End
      Begin VB.Label Label2 
         Caption         =   "Message : "
         Height          =   315
         Left            =   -74760
         TabIndex        =   7
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Objet : "
         Height          =   315
         Left            =   -74760
         TabIndex        =   4
         Top             =   960
         Width           =   855
      End
      Begin VB.Shape Shape2 
         Height          =   6375
         Left            =   -74880
         Top             =   840
         Width           =   6495
      End
      Begin VB.Shape Shape1 
         Height          =   6375
         Left            =   120
         Top             =   840
         Width           =   6495
      End
   End
End
Attribute VB_Name = "frmDestination"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()

Dim aLb As Libelle

Set aLb = Libelles("FRMDESTINATION")

'-> V�rifier qu'une imprimante soit s�lectionn�e
If Me.ListView1.SelectedItem Is Nothing Then
    MsgBox aLb.GetCaption(12), vbExclamation + vbOKOnly, aLb.GetToolTip(12)
    Me.ListView1.SetFocus
    Exit Sub
End If
    
'-> Cr�er la chaine de retour
StrRetour = "IMP�" & Me.ListView1.SelectedItem.Text

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command2_Click()

Dim TempFile As String

'-> Obtenir un nom de fichier temporaire
TempFile = GetTempFileNameVB("WWW")

StrRetour = "OUT�" & Me.Text2.Text & "�"

'-> Enregistrer le fichier RTF si <> ""
If Trim(Me.RichTextBox1.Text) <> "" Then
    Me.RichTextBox1.SaveFile TempFile
    StrRetour = StrRetour & TempFile
End If

'-> D�charger la feuille
Unload Me


End Sub

Private Sub Command3_Click()

Dim TempFile As String

'-> Obtenir un nom de fichier temporaire
TempFile = GetTempFileNameVB("WWW")

StrRetour = "WWW�" & Me.Text2.Text & "�"

'-> Enregistrer le fichier RTF si <> ""
If Trim(Me.RichTextBox1.Text) <> "" Then
    Me.RichTextBox1.SaveFile TempFile
    StrRetour = StrRetour & TempFile
End If

'-> D�charger la feuille
Unload Me


End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim x As Printer


Set aLb = Libelles("FRMDESTINATION")
Me.Caption = aLb.GetCaption(1)

Set Me.SSTab1.TabPicture(0) = Me.ImageList1.ListImages("TURBO").Picture
Set Me.SSTab1.TabPicture(1) = Me.ImageList1.ListImages("SEND").Picture
Me.SSTab1.TabCaption(0) = aLb.GetCaption(2)
Me.SSTab1.TabCaption(1) = aLb.GetCaption(11)

Me.Image1.ToolTipText = aLb.GetCaption(6)
Me.Image2.ToolTipText = aLb.GetCaption(7)
Me.Image3.ToolTipText = aLb.GetCaption(8)
Me.Image4.ToolTipText = aLb.GetCaption(9)
Me.Image5.ToolTipText = aLb.GetCaption(10)

'-> Charger la liste des imprimantes
For Each x In Printers
    Me.ListView1.ListItems.Add , , x.DeviceName, "TURBO"
Next

'-> Ajouter l'imprimante ecrna
Me.ListView1.ListItems.Add , , "ECRAN", "TURBO"

Me.Command1.Caption = aLb.GetCaption(5)
Me.Command2.Caption = aLb.GetCaption(4)
Me.Command3.Caption = aLb.GetCaption(3)
Me.Command2.Enabled = IsOutLook


End Sub

Private Function IsOutLook() As Boolean

'---> Cette proc�dure d�termine si OutLook est install�
Dim aObject As Object

On Error GoTo GestError

Set aObject = CreateObject("Outlook.Application")
IsOutLook = True

Exit Function

GestError:
    IsOutLook = False
    




End Function


Private Sub Image1_Click()

Me.CommonDialog1.FontName = Me.RichTextBox1.Font.Name
Me.CommonDialog1.FontBold = Me.RichTextBox1.Font.Bold
Me.CommonDialog1.FontItalic = Me.RichTextBox1.Font.Italic
Me.CommonDialog1.FontSize = Me.RichTextBox1.Font.Size
Me.CommonDialog1.Flags = cdlCFBoth
Me.CommonDialog1.ShowFont

Me.RichTextBox1.SelFontName = Me.CommonDialog1.FontName
Me.RichTextBox1.SelBold = Me.CommonDialog1.FontBold
Me.RichTextBox1.SelItalic = Me.CommonDialog1.FontItalic
Me.RichTextBox1.SelFontSize = Me.CommonDialog1.FontSize


End Sub

Private Sub Image2_Click()

Me.CommonDialog1.ShowColor
Me.RichTextBox1.SelColor = Me.CommonDialog1.Color

End Sub

Private Sub Image3_Click()

Me.RichTextBox1.SelAlignment = 0

End Sub

Private Sub Image4_Click()

Me.RichTextBox1.SelAlignment = 2

End Sub

Private Sub Image5_Click()

Me.RichTextBox1.SelAlignment = 1


End Sub
