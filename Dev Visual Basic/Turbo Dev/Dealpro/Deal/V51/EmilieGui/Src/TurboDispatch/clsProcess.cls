VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsProcess"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Classe qui d�termine l'�volution d'un process de dispatching
Public HdlCom As Long
Public HdlProcess As Long
Public FichierPilote As String
Public DateDebut As String
Public FrmProcess As FrmProcess
Public IsLog As Boolean
Public IsArchive As Boolean
Public IDFlex As Long '-> Num�ro de la ligne dans le tableau de r�capitulatif
Public Key As String
Public FichierLog As String


Public Sub AddToBrowse()

Dim aLb As Libelle
Dim i As Integer

On Error Resume Next

Set aLb = Libelles("CMM")

'-> Ajouter une ligne
frmListeProcess.Flex.Rows = frmListeProcess.Flex.Rows + 1
'-> Se positionner
frmListeProcess.Flex.Row = frmListeProcess.Flex.Rows - 1
frmListeProcess.Flex.Col = 0
'-> Ajouter l'icone
Set frmListeProcess.Flex.CellPicture = frmListeProcess.Image1.Picture
frmListeProcess.Flex.RowHeight(frmListeProcess.Flex.Row) = frmListeProcess.ScaleY(30, 3, 1)
'-> Ajouter la date
frmListeProcess.Flex.Col = 1
frmListeProcess.Flex.Text = DateDebut
'-> Ajouter le nom du fichier pilote
frmListeProcess.Flex.Col = 2
frmListeProcess.Flex.Text = PathSpool & FichierPilote
'-> Ajouter le Log
frmListeProcess.Flex.Col = 3
If IsLog Then
    frmListeProcess.Flex.Text = aLb.GetCaption(1)
Else
    frmListeProcess.Flex.Text = aLb.GetCaption(2)
End If
'-> Ajouter l'archivage
frmListeProcess.Flex.Col = 4
If IsArchive Then
    frmListeProcess.Flex.Text = aLb.GetCaption(1)
Else
    frmListeProcess.Flex.Text = aLb.GetCaption(2)
End If
'-> Ajouter le Process ID
frmListeProcess.Flex.Col = 5
frmListeProcess.Flex.Text = HdlProcess
'-> Ajouter la cl� d'acc�s de la classe
frmListeProcess.Flex.Col = 6
frmListeProcess.Flex.Text = Key

'-> Indiquer qu'elle ligne on a ajout� dans le browse
IDFlex = frmListeProcess.Flex.Row

End Sub

Public Sub RemoveToBrowse()

'---> Cette proc�dure supprimer la ligen du browse attach�e � un process

Dim i As Integer

'-> si une seule ligne
If frmListeProcess.Flex.Rows = 2 Then
    frmListeProcess.Flex.Rows = 1
    Exit Sub
End If

For i = 0 To frmListeProcess.Flex.Rows - 1
    If frmListeProcess.Flex.TextArray(i * frmListeProcess.Flex.Cols + 5) = CStr(HdlProcess) Then
        frmListeProcess.Flex.RemoveItem (i)
        Exit Sub
    End If
Next



End Sub
