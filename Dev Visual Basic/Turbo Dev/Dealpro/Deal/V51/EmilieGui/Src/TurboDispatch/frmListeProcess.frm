VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmListeProcess 
   Caption         =   "Form1"
   ClientHeight    =   6060
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9585
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   404
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   639
   Begin MSFlexGridLib.MSFlexGrid Flex 
      Height          =   2295
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   4048
      _Version        =   393216
      FixedCols       =   0
      ScrollBars      =   2
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   0
      Picture         =   "frmListeProcess.frx":0000
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
End
Attribute VB_Name = "frmListeProcess"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Flex_DblClick()

Dim aProcess As clsProcess

'---> Afficher la feuille de param�trage
If Me.Flex.Col <> 0 Then Exit Sub

'-> Pointer sur la classe
Set aProcess = Process(Me.Flex.TextArray(Me.Flex.Row * Me.Flex.Cols + 6))
aProcess.FrmProcess.Visible = True
aProcess.FrmProcess.ZOrder


End Sub

Private Sub Form_Load()

Dim aProcess As clsProcess
Dim aLb As Libelle


'-> Indiquer que la feuille est charg�e
IsListeProcess = True

'-> Dimensionnenment des colonnes
Me.Flex.Cols = 7

'-> Gestion des messprogs
Set aLb = Libelles("FRMLISTEPROCESS")
Me.Caption = aLb.GetCaption(7)

Me.Flex.Row = 0
For i = 0 To 5
    Me.Flex.Col = i
    Me.Flex.Text = aLb.GetCaption(i + 1)
Next

'-> Supprimer lderni�rer ligne
Me.Flex.Rows = 1

'-> Charger la liste des process
For Each aProcess In Process
    aProcess.AddToBrowse
Next

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

'-> Indiquer que la feuille n'est plus active
IsListeProcess = False



End Sub

Private Sub Form_Resize()

Dim aRect As RECT
Dim Res As Long
Dim i As Integer
Dim Tempo As Single

On Error Resume Next

'-> Positionner le Flex Grid
Res = GetClientRect(Me.hwnd, aRect)
Me.Flex.Left = 0
Me.Flex.Top = 0
Me.Flex.Width = aRect.Right
Me.Flex.Height = aRect.Bottom

'-> Retailler le contenu des colonnes
Res = GetClientRect(Me.Flex.hwnd, aRect)
Me.Flex.ColWidth(0) = Me.ScaleX(50, 3, 1)
Me.Flex.ColWidth(1) = Me.ScaleX(aRect.Right * 0.15, 3, 1)
Me.Flex.ColWidth(2) = Me.ScaleX(aRect.Right * 0.3, 3, 1)
Me.Flex.ColWidth(3) = Me.ScaleX(aRect.Right * 0.2, 3, 1)
Me.Flex.ColWidth(4) = Me.ScaleX(aRect.Right * 0.2, 3, 1)
For i = 0 To 4
    Tempo = Tempo + Me.Flex.ColWidth(i)
    Me.Flex.ColAlignment(i) = 4
Next

Me.Flex.ColWidth(5) = Me.ScaleX(aRect.Right, 3, 1) - Tempo
Me.Flex.ColAlignment(5) = 4

End Sub

Private Sub Form_Unload(Cancel As Integer)

'-> Indiquer  que la feuille n'est pas en cours de chargement
IsListeProcess = False

End Sub
