VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmGestLog 
   Caption         =   "Form1"
   ClientHeight    =   7740
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13380
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   516
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   892
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   5520
      Top             =   2160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestLog.frx":0000
            Key             =   "TURBO"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGestLog.frx":059A
            Key             =   "WARNING"
         EndProperty
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   7095
      Left            =   3720
      TabIndex        =   2
      Top             =   480
      Width           =   9255
      _ExtentX        =   16325
      _ExtentY        =   12515
      _Version        =   393216
      Rows            =   1
      Cols            =   3
      FixedRows       =   0
      FixedCols       =   0
      ScrollBars      =   2
   End
   Begin VB.FileListBox File1 
      Height          =   7110
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   3495
   End
   Begin VB.Label Label2 
      Caption         =   "Descriptif du fichier LOG :"
      Height          =   255
      Left            =   3720
      TabIndex        =   3
      Top             =   120
      Width           =   2895
   End
   Begin VB.Label Label1 
      Caption         =   "Veuillez s�lectionner un fichier LOG :"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2895
   End
End
Attribute VB_Name = "frmGestLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub EditLog(ByVal Filename As String)


Dim hdlFile As Integer
Dim Ligne As String
Dim aLb As Libelle

On Error GoTo GestError

Me.Enabled = False
Me.MousePointer = 11

'-> Remettre � 0 le Flex
Me.MSFlexGrid1.Rows = 0

'-> Ouverture du fichier
hdlFile = FreeFile
Open Filename For Input As #hdlFile

'-> Pointer sur la classe libelle
Set aLb = Libelles("FRMLOG")

'-> Analyse du fichier
Do While Not EOF(hdlFile)
    '-> lecture de la ligne
    Line Input #hdlFile, Ligne
    If Trim(Ligne) = "" Then GoTo NextLig
    '-> Ajouter une ligne et se positionner dessus
    Me.MSFlexGrid1.Rows = Me.MSFlexGrid1.Rows + 1
    Me.MSFlexGrid1.Row = Me.MSFlexGrid1.Rows - 1
    '-> Positionner toul=jourts sur la premi�re colonne
    Me.MSFlexGrid1.Col = 0
    '-> Selon le type de ligne
    Select Case UCase$(Entry(1, Ligne, "="))
        Case "BEGIN"
            Me.MSFlexGrid1.Text = aLb.GetCaption(2)
            Me.MSFlexGrid1.Col = 1
            Me.MSFlexGrid1.Text = Entry(2, Ligne, "=")
        Case "PILOTEFILE"
            Me.MSFlexGrid1.Text = aLb.GetCaption(3)
            Me.MSFlexGrid1.Col = 1
            Me.MSFlexGrid1.Text = Entry(2, Ligne, "=")
        Case "BEGINPILOTELIGNE"
            Me.MSFlexGrid1.Text = aLb.GetCaption(5) & Entry(1, Entry(2, Ligne, "="), "�")
            Me.MSFlexGrid1.Col = 1
            Me.MSFlexGrid1.Text = Entry(2, Entry(2, Ligne, "="), "�")
        Case "DATAFILE"
            Me.MSFlexGrid1.Text = aLb.GetCaption(6)
            Me.MSFlexGrid1.Col = 1
            Select Case (Entry(1, Entry(2, Ligne, "="), "�"))
                Case "NOFILE"
                    Me.MSFlexGrid1.Text = aLb.GetCaption(7)
                Case "NOMATCH"
                    Me.MSFlexGrid1.Text = aLb.GetCaption(8) & Entry(2, Entry(2, Ligne, "="), "�")
                Case "MATCH"
                    Me.MSFlexGrid1.CellFontUnderline = True
                    Me.MSFlexGrid1.CellForeColor = &HC00000
                    Me.MSFlexGrid1.Text = Entry(2, Entry(2, Ligne, "="), "�")
                    Me.MSFlexGrid1.Col = 2
                    Me.MSFlexGrid1.Text = "FILE"
                    
            End Select
        Case "MAQGUI"
            Me.MSFlexGrid1.Text = aLb.GetCaption(9)
            Me.MSFlexGrid1.Col = 1
            Select Case (Entry(1, Entry(2, Ligne, "="), "�"))
                Case "SPOOL"
                    Me.MSFlexGrid1.Text = aLb.GetCaption(10)
                Case "NOMATCH"
                    Me.MSFlexGrid1.Text = aLb.GetCaption(11) & Entry(2, Entry(2, Ligne, "="), "�")
                Case "MATCH"
                    Me.MSFlexGrid1.Text = Entry(2, Entry(2, Ligne, "="), "�")
                Case "ERROR"
                    Me.MSFlexGrid1.Text = aLb.GetCaption(11) & Entry(2, Entry(2, Ligne, "="), "�")
                    '-> Positionner l'icone
                    Me.MSFlexGrid1.Col = 0
                    Set Me.MSFlexGrid1.CellPicture = Me.ImageList1.ListImages("WARNING").Picture
            End Select
        Case "RUPTURE"
            Me.MSFlexGrid1.Text = aLb.GetCaption(15)
            Me.MSFlexGrid1.Col = 1
            Select Case (Entry(1, Entry(2, Ligne, "="), "�"))
                Case "NOMATCH"
                    Me.MSFlexGrid1.Text = aLb.GetCaption(13)
                Case "NOMATCH"
                    Me.MSFlexGrid1.Text = aLb.GetCaption(14)
                Case "MATCH"
                    Me.MSFlexGrid1.Text = Entry(2, Entry(2, Ligne, "="), "�")
            End Select
        Case "VALUERUPTURE"
            Me.MSFlexGrid1.Text = aLb.GetCaption(16)
            Me.MSFlexGrid1.Col = 1
            Me.MSFlexGrid1.Text = Entry(2, Ligne, "=")
        Case "SPOOL"
            Me.MSFlexGrid1.Text = aLb.GetCaption(17)
            Me.MSFlexGrid1.Col = 1
            Select Case (Entry(1, Entry(2, Ligne, "="), "�"))
                Case "OK"
                    Me.MSFlexGrid1.Text = Entry(2, Entry(2, Ligne, "="), "�")
                    Me.MSFlexGrid1.CellFontUnderline = True
                    Me.MSFlexGrid1.CellForeColor = &HC00000
                    Me.MSFlexGrid1.Col = 2
                    Me.MSFlexGrid1.Text = "TURBO"
                Case "NO"
                    Me.MSFlexGrid1.Text = aLb.GetCaption(18)
            End Select
        Case "PRINTFILE"
            Me.MSFlexGrid1.Text = aLb.GetCaption(25)
            Me.MSFlexGrid1.Col = 1
            Me.MSFlexGrid1.Text = Entry(2, Entry(2, Ligne, "="), "�")
        
        Case "SENDOUT"
            Me.MSFlexGrid1.Text = aLb.GetCaption(26)
            Me.MSFlexGrid1.Col = 1
            Me.MSFlexGrid1.Text = Entry(2, Entry(2, Ligne, "="), "�")
         
        Case "SENDNET"
            Me.MSFlexGrid1.Text = aLb.GetCaption(27)
            Me.MSFlexGrid1.Col = 1
            Me.MSFlexGrid1.Text = Entry(2, Entry(2, Ligne, "="), "�")
        Case "ARCHIVAGE"
            Me.MSFlexGrid1.Text = aLb.GetCaption(24)
            Me.MSFlexGrid1.Col = 1
            If UCase$(Trim(Entry(2, Ligne, "="))) = "YES" Then
                Me.MSFlexGrid1.Text = aLb.GetCaption(19)
            Else
                Me.MSFlexGrid1.Text = aLb.GetCaption(20)
            End If
            
        Case "END"
            Me.MSFlexGrid1.Text = aLb.GetCaption(21)
            Me.MSFlexGrid1.Col = 1
            Me.MSFlexGrid1.Text = Entry(2, Ligne, "=")

    End Select
NextLig:
Loop 'Boucle d'analyse du fichier PILOTE

GestError:

Me.Enabled = True
Me.MousePointer = 0

'-> Fermer le fichier
Close #hdlFile


End Sub

Private Sub File1_DblClick()

EditLog PathLog & Me.File1.Filename

End Sub

Private Sub Form_Load()

Dim aLb As Libelle

'-> Indiquer que la feuille est ouverte
IsGestLog = True

'-> pointer sur la classe des messages
Set aLb = Libelles("FRMLOG")

Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(22)
Me.Label2.Caption = aLb.GetCaption(23)

'-> V�rifier que le r�pertoire des LOG est actif
If Dir$(PathLog, vbDirectory) <> "" Then
    If GetAttr(PathLog) <> vbDirectory Then
        Me.File1.Enabled = False
    Else
        Me.File1.Path = PathLog
        Me.File1.Pattern = "*.LOG"
    End If
Else
    Me.File1.Enabled = False
End If

Me.MSFlexGrid1.Rows = 0
For i = 0 To Me.MSFlexGrid1.Cols - 1
    Me.MSFlexGrid1.ColAlignment(i) = 1
Next
End Sub

Private Sub Form_Resize()

'-> Donner une taille aux colonnes
Dim aRect As RECT
Dim Res As Long

On Error Resume Next

Res = GetClientRect(Me.hwnd, aRect)
Me.MSFlexGrid1.Width = aRect.Right - Me.MSFlexGrid1.Left
Me.MSFlexGrid1.Height = aRect.Bottom - Me.MSFlexGrid1.Top

Me.File1.Height = Me.MSFlexGrid1.Height

Res = GetClientRect(Me.MSFlexGrid1.hwnd, aRect)
Me.MSFlexGrid1.ColWidth(0) = Me.ScaleX(4, 7, 1)

Me.MSFlexGrid1.ColWidth(1) = Me.ScaleX(aRect.Right, 3, 1) - Me.MSFlexGrid1.ColWidth(0)


End Sub

Private Sub Form_Unload(Cancel As Integer)

'-> Indiquer que la feuille est ferm�e
IsGestLog = False


End Sub

Private Sub MSFlexGrid1_DblClick()

Dim aFrm As frmEditor
Dim aLb As Libelle

On Error Resume Next

'-> Quitter si pas de ligne
If Me.MSFlexGrid1.Rows = 0 Then Exit Sub

'-> V�rifier que le fichier existe encore
Me.MSFlexGrid1.Col = 1
If Dir$(Me.MSFlexGrid1.Text, vbNormal) = "" Then
    MsgBox aLb.GetCaption(28), vbExclamation + vbOKOnly, aLb.GetToolTip(28)
    Exit Sub
End If

'-> Se d�placer en derni�re colonne
Me.MSFlexGrid1.Col = 2
If Me.MSFlexGrid1.Text = "TURBO" Then
    Me.MSFlexGrid1.Col = 1
    Shell App.Path & "\TurboGraph.exe " & Me.MSFlexGrid1.Text, vbNormalFocus
Else
    Set aFrm = New frmEditor
    Me.MSFlexGrid1.Col = 1
    aFrm.RichTextBox1.LoadFile Me.MSFlexGrid1.Text
    aFrm.CurrentFile = Me.MSFlexGrid1.Text
    aFrm.IsSaved = True
    aFrm.Show
End If

End Sub
