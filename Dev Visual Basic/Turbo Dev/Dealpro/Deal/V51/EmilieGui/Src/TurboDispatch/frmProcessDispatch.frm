VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Begin VB.Form frmProcessDispatch 
   Caption         =   "Form1"
   ClientHeight    =   3660
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   2535
   Icon            =   "frmProcessDispatch.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3660
   ScaleWidth      =   2535
   StartUpPosition =   3  'Windows Default
   Begin MSMAPI.MAPIMessages MAPIMessages1 
      Left            =   2040
      Top             =   2880
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
   Begin MSMAPI.MAPISession MAPISession1 
      Left            =   2040
      Top             =   1920
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DownloadMail    =   -1  'True
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin RichTextLib.RichTextBox RTFSend 
      Height          =   615
      Left            =   2040
      TabIndex        =   2
      Top             =   960
      Width           =   495
      _ExtentX        =   873
      _ExtentY        =   1085
      _Version        =   393217
      TextRTF         =   $"frmProcessDispatch.frx":0582
   End
   Begin RichTextLib.RichTextBox RtfFile 
      Height          =   3015
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   5318
      _Version        =   393217
      TextRTF         =   $"frmProcessDispatch.frx":0677
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   1800
      Top             =   120
   End
   Begin VB.PictureBox PicCom 
      Height          =   495
      Left            =   120
      ScaleHeight     =   435
      ScaleWidth      =   1635
      TabIndex        =   0
      Top             =   240
      Width           =   1695
   End
End
Attribute VB_Name = "frmProcessDispatch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ToUnload As Boolean
Private Sub PicCom_Paint()

Dim Res As Long
Dim lpBuffer As String

On Error Resume Next

'-> R�cupere la private data du picturebox de communication
lpBuffer = Space$(2000)
Res = GetWindowText(Me.PicCom.hwnd, lpBuffer, Len(lpBuffer))

'-> Supprimer la private data du process
SetWindowText Me.PicCom.hwnd, ""

'-> Quitter si pas de messages
If Trim(lpBuffer) = "" Then Exit Sub

'-> Supprimer le dernier chr(0)
lpBuffer = Mid$(lpBuffer, 1, Res)

'-> Traitement du message
Select Case Entry(1, lpBuffer, "|")
    
    Case "PROCESS_EXIT"
        '-> Demande de fermeture
        
        '-> Mettre � jour le fichier LOG
        MajLOG "ARCHIVAGE=NO"
        MajLOG "END=" & Now
                
        
        '-> Fermer le fichier des LOG
        CloseLOG
    
        '-> Fermer tous les fichiers ouvers
        Reset
        
        '-> Renvoyer un message de fermture � la console
        SetWindowText hdlCommunication, "PROCESS_EXIT"
        SendMessage hdlCommunication, WM_PAINT, 0, 0
                                        
        ToUnload = True
        
       

End Select '-> Selon le message

End Sub

Private Sub Timer1_Timer()

DoEvents
If ToUnload Then End

End Sub
