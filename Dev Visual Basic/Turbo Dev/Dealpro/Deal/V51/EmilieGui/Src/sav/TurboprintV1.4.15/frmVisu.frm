VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmVisu 
   AutoRedraw      =   -1  'True
   Caption         =   "Form1"
   ClientHeight    =   4500
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   11475
   Icon            =   "frmVisu.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4500
   ScaleWidth      =   11475
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog OpenDlg 
      Left            =   7680
      Top             =   1440
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox Page 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      DrawWidth       =   6
      ForeColor       =   &H80000008&
      Height          =   1695
      Left            =   840
      ScaleHeight     =   113
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   238
      TabIndex        =   3
      Top             =   960
      Visible         =   0   'False
      Width           =   3570
   End
   Begin VB.HScrollBar Horizontal 
      Height          =   255
      Left            =   2400
      TabIndex        =   2
      Top             =   3600
      Width           =   1335
   End
   Begin VB.VScrollBar Vertical 
      Height          =   1095
      Left            =   5640
      TabIndex        =   1
      Top             =   2400
      Width           =   255
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6240
      Top             =   1680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   11
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":08CA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":15A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":227E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":2F58
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":3C32
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":490C
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":55E6
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":5900
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":65DA
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":72B4
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":7F8E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Wrappable       =   0   'False
      Appearance      =   1
      ImageList       =   "ImageList1"
      DisabledImageList=   "ImageList2"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   15
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PageGarde"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PageSelection"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Precedent"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Atteindre"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Suivant"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "DernierePage"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
            Object.Width           =   15
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Imprimer"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
            Object.Width           =   15
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Annuler"
            ImageIndex      =   8
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Quitter"
            ImageIndex      =   9
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "APropos"
            ImageIndex      =   10
         EndProperty
         BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ReLoad"
            ImageIndex      =   11
         EndProperty
      EndProperty
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   615
         Left            =   7200
         ScaleHeight     =   615
         ScaleWidth      =   3975
         TabIndex        =   4
         Top             =   0
         Width           =   3975
         Begin VB.Label Label4 
            Height          =   255
            Left            =   2040
            TabIndex        =   8
            Top             =   360
            Width           =   1935
         End
         Begin VB.Label Label3 
            Caption         =   "Label3"
            Height          =   255
            Left            =   120
            TabIndex        =   7
            Top             =   360
            Width           =   1815
         End
         Begin VB.Label Label2 
            Height          =   255
            Left            =   2040
            TabIndex        =   6
            Top             =   120
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Label1"
            Height          =   255
            Left            =   120
            TabIndex        =   5
            Top             =   120
            Width           =   1695
         End
      End
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   0
      Top             =   1000
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":A740
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":B41A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":C0F4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":CDCE
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":DAA8
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":E782
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":F45C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":F776
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":10450
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVisu.frx":1112A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFichier 
      Caption         =   ""
      Begin VB.Menu mnuLoadFile 
         Caption         =   ""
      End
      Begin VB.Menu MnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmVisu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Activate()
     AffichePage
End Sub

Private Sub Form_Load()

'-> Charger les tooltips des boutons
Dim aLb As Libelle

Set aLb = Libelles("FRMVISU")
Me.Toolbar1.Buttons("PageGarde").ToolTipText = aLb.GetCaption(1)
Me.Toolbar1.Buttons("PageSelection").ToolTipText = aLb.GetCaption(2)
Me.Toolbar1.Buttons("Precedent").ToolTipText = aLb.GetCaption(3)
Me.Toolbar1.Buttons("Atteindre").ToolTipText = aLb.GetCaption(4)
Me.Toolbar1.Buttons("Suivant").ToolTipText = aLb.GetCaption(5)
Me.Toolbar1.Buttons("DernierePage").ToolTipText = aLb.GetCaption(9)
Me.Toolbar1.Buttons("Imprimer").ToolTipText = aLb.GetCaption(6)
Me.Toolbar1.Buttons("Annuler").ToolTipText = aLb.GetCaption(7)
Me.Toolbar1.Buttons("Quitter").ToolTipText = aLb.GetCaption(8)
Me.Toolbar1.Buttons("APropos").ToolTipText = aLb.GetCaption(10)
Me.Toolbar1.Buttons("ReLoad").ToolTipText = aLb.GetCaption(19)
Me.Label1.Caption = aLb.GetCaption(13)
Me.Label3.Caption = aLb.GetCaption(14)
Me.Caption = aLb.GetCaption(11)

Set aLb = Libelles("MENU")
Me.mnuFichier.Caption = aLb.GetCaption(1)
Me.mnuLoadFile.Caption = aLb.GetCaption(2)
Me.mnuExit.Caption = aLb.GetCaption(3)


Set aLb = Nothing

End Sub

Public Sub Form_Resize()

Dim Rect1 As RECT
Dim Rect2 As RECT
Dim Rect3 As RECT
Dim Rect4 As RECT
Dim Res As Long
Dim hMask As Single
Dim vMask As Single


On Error Resume Next

Me.Horizontal.Value = 0
Me.Vertical.Value = 0

'-> Essayer de toujours centr� la feuille
If Me.Page.Width < Me.Width Then _
    Me.Page.Left = (Me.Width - Me.Page.Width) / 2

'-> Positionner la feuille en haut apres la seconde barre de d�filement
Me.Page.Top = Me.Toolbar1.Top + Me.Toolbar1.Height + Me.ScaleY(1, 7, 1)

'-> R�cup�ration des coordonn�es de la feuille
Res = GetClientRect(Me.hwnd, Rect1)
Res = GetClientRect&(Me.Page.hwnd, Rect2)
Res = GetClientRect&(Me.Horizontal.hwnd, Rect3)
Res = GetClientRect&(Me.Vertical.hwnd, Rect4)

'---> Traitement horizontal
If Me.Toolbar1.Height + Me.Toolbar1.Height + Me.Horizontal.Height < Me.ScaleY(Rect1.Bottom - (Rect3.Bottom - Rect3.Top), 3, 1) Then
    Me.Horizontal.Top = Me.ScaleY(Rect1.Bottom - (Rect3.Bottom - Rect3.Top), 3, 1)
Else
    Me.Horizontal.Top = Me.Toolbar1.Height + Me.Horizontal.Height
End If

Me.Horizontal.Left = 0
Me.Horizontal.ZOrder
Me.Horizontal.Width = Me.ScaleX(Rect1.Right, 3, 1) - Vertical.Width

hMask = Rect1.Right - Rect2.Right + Rect2.Left - GetSystemMetrics(45) * 4
vMask = Rect1.Bottom - Rect2.Bottom + Rect2.Top - GetSystemMetrics(45) * 4 - ScaleY(Me.Toolbar1.Height + Me.Toolbar1.Height, 1, 3)

If hMask < 0 Then
    Me.Page.Left = 120
    Me.Horizontal.Max = Me.ScaleX(hMask, 3, 1) - Me.Vertical.Width * 2
    Me.Horizontal.Min = Me.Page.Left
    Me.Horizontal.Value = Me.Page.Left
    Me.Horizontal.SmallChange = (Abs(Me.Horizontal.Max - Me.Horizontal.Min) / 10) + 1
    Me.Horizontal.LargeChange = Me.Horizontal.SmallChange * 5
    Me.Horizontal.Visible = True
Else
    Me.Horizontal.Visible = False
End If

Me.Vertical.Left = Me.ScaleX(Rect1.Right - (Rect4.Right - Rect4.Left), 3, 1)
Me.Vertical.Top = Me.Toolbar1.Height
Me.Vertical.Height = Me.ScaleY(Rect1.Bottom, 3, 1) - Me.Toolbar1.Height - Me.Horizontal.Height
Me.Vertical.ZOrder


If vMask < 0 Then
    'Me.page.Top = 1080
    Me.Vertical.Max = Me.ScaleY(vMask, 3, 1) - Me.Horizontal.Height * 2
    Me.Vertical.Min = Me.Page.Top
    Me.Vertical.Value = Me.Page.Top
    Me.Vertical.SmallChange = (Abs(Me.Vertical.Max - Me.Vertical.Min) / 10) + 1
    Me.Vertical.LargeChange = Me.Vertical.SmallChange * 5
    Me.Vertical.Visible = True
Else
    Me.Vertical.Visible = False
End If

'-> Ajuster
If Me.Horizontal.Visible = True And Not (Me.Vertical.Visible) Then Me.Horizontal.Width = Me.Horizontal.Width + Me.Vertical.Width
If Me.Vertical.Visible = True And Not (Me.Horizontal.Visible) Then Me.Vertical.Height = Me.Vertical.Height + Me.Horizontal.Height

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
End Sub

Private Sub Horizontal_Change()
    Me.Page.Left = Me.Horizontal.Value
    Me.Toolbar1.ZOrder
End Sub

Private Sub mnuExit_Click()
    End
End Sub

Public Sub mnuLoadFile_Click()
    
Dim lpBuffer As String
Dim Res As Long
Dim IniPath As String
Dim Extension As String

On Error Resume Next

'-> R�cup�ration du r�pertoire temporaire
If InStr(1, UCase$(App.Path), "\EMILIEGUI\SRC") <> 0 Then
    IniPath = "D:\ERP\TURBO"
Else
    CreatePath IniPath
End If
IniPath = IniPath & "\TurboGraph.ini"

lpBuffer = Space$(500)
Res = GetEnvironmentVariable("DEALTEMPO", lpBuffer, Len(lpBuffer))

'-> V�rifier que le r�pertoire existe
If Dir$(lpBuffer, vbDirectory) = "" Then
    MsgBox aLb.GetCaption(7), vbCritical + vbOKOnly, aLb.GetToolTip(7)
End If

'-> Supprimer le chr(0) li� � la fin de la requette
lpBuffer = Mid$(lpBuffer, 1, Res)

'-> Intialiser le r�pertoire d'ouverture
Me.OpenDlg.InitDir = lpBuffer

'-> R�cup�rer le code op�rateur pour l'extension par d�faut
lpBuffer = Space$(500)
Res = GetEnvironmentVariable("DEALOPERAT", lpBuffer, Len(lpBuffer))
Extension = "*.*"
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    Extension = lpBuffer
End If
Me.OpenDlg.FileName = Extension
Me.OpenDlg.DefaultExt = Extension
Me.OpenDlg.Filter = Extension

'-> Flags d'ouverture
Me.OpenDlg.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly Or _
                   cdlOFNPathMustExist

'-> Afficher la feuille
Me.OpenDlg.ShowOpen

'-> Tester le retour
If Me.OpenDlg.FileName = "" Then Exit Sub

'-> Charger le nom du fichier
NomFichier = Me.OpenDlg.FileName
For i = 1 To frmVisu.Toolbar1.Buttons.Count
    frmVisu.Toolbar1.Buttons(i).Enabled = True
Next

InitImpression
OpenMaq
Start
AffichePage

Exit Sub

GestError:
    
    Exit Sub
    
End Sub

Public Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim Rep As String
Dim aTb As Tableau
Dim aBlock As Block
Dim i As Integer
Dim aLb As Libelle
Dim Temp As Integer

On Error GoTo GestError

Select Case Button.Key

    Case "PageGarde"
    
        OldPage = PageCours
        Display_PageGardeSelection -1
        PageCours = -1
        
    Case "PageSelection"
        OldPage = PageCours
        Display_PageGardeSelection 0
        PageCours = 0
        
    Case "Precedent"
        OldPage = PageCours
        PageCours = PageCours - 1
        Select Case PageCours
            Case -1, 0
                Display_PageGardeSelection PageCours
            Case Else
                PrintPage PageCours
        End Select
        
    Case "Atteindre"
        Set aLb = Libelles("FRMVISU")
        Rep = InputBox(aLb.GetCaption(12), aLb.GetToolTip(12), PageCours)
        If Rep <> "" Then
            If Not IsNumeric(Rep) Then Exit Sub
            If CInt(Rep) = PageCours Then Exit Sub
            If CInt(Rep) > NbPage Then Exit Sub
            If CInt(Rep) < 1 Then
                PrintPageGS Rep
            Else
                PrintPage Rep
            End If
            
            OldPage = PageCours
        PageCours = CInt(Rep)
        End If
        Set aLb = Nothing
        
    Case "Suivant"
        OldPage = PageCours
        PageCours = PageCours + 1
        Select Case PageCours
            Case -1, 0
                Display_PageGardeSelection PageCours
            Case Else
                PrintPage PageCours
        End Select
        
    Case "DernierePage"
        OldPage = PageCours
        PageCours = NbPage
        PrintPage NbPage
        
    Case "Imprimer"
        '-> Afficher le choix des imprimantes
        frmPrint.Show vbModal
        '-> Tester le retour
        If PrintOk Then
            '-> Setting du p�riph�rique
            Set Sortie = Printer
            Sortie.ScaleMode = 3
            '-> Il faut initaliser chaque block pour le calcul des pixels
            For Each aTb In aMaq.Tableaux
                For Each aBlock In aTb.Blocks
                    InitBlock aBlock, aBlock.NomTb
                Next
            Next
            '-> Afficher la temporisation
            frmTempo.Show
            frmTempo.ToUnload = False
            DoEvents
            '-> Lancer l'impression
            For i = 1 To nCopies
                Select Case PrintMode
                    Case 1 'Page cours
                        PageMin = PageCours
                        PageMax = PageCours
                    Case 2 'Page Min/Max
                    Case 3 'Fichier
                        If aMaq.PageGarde Then
                            '-> Imprimer la page de garde
                            PrintPageGS -1
                            Printer.NewPage
                        End If
                        If aMaq.PageSelection Then
                            '-> Imprimer la page des s�lections
                            PrintPageGS 0
                            Printer.NewPage
                        End If
                        '-> Indiquer les pages
                        PageMin = 1
                        PageMax = NbPage
                End Select
                '-> Imprimer
                For j = PageMin To PageMax
                    '-> Imprimer la page
                    If j < 1 Then
                        PrintPageGS j
                    Else
                        PrintPage j
                    End If
                    '-> Gestion du saut de page
                    If i < nCopies Then
                        Printer.NewPage
                    Else
                        If j <> PageMax Then
                            Printer.NewPage
                        Else
                            Exit For
                        End If
                    End If
                Next
            Next
            '-> Fermer la tempo
            frmTempo.ToUnload = True
            Unload frmTempo
            '-> Terminer l'impression$
            Printer.EndDoc
            '-> P�riph de sortie = visu
            Set Sortie = frmVisu.Page
            '-> R�initialiser les blocks
            For Each aTb In aMaq.Tableaux
                For Each aBlock In aTb.Blocks
                    InitBlock aBlock, aBlock.NomTb
                Next
            Next
            '-> Initialiser le retour impression
            PrintOk = False
        End If
        
    Case "Annuler"
    
        Temp = PageCours
        Select Case OldPage
            Case -1, 0
                Display_PageGardeSelection OldPage
            Case Else
                PrintPage OldPage
        End Select
        PageCours = OldPage
        OldPage = Temp
        
    Case "Quitter"
        Unload Me
    Case "APropos"
        '-> Lancer l'aide en ligne
        Shell App.Path & "\HelpDeal.exe DEAL|EMILIE|1|TURBO-NAVIGATE", vbMaximizedFocus
        'AproposDe.Show vbModal
    
    Case "ReLoad"
    
        '-> V�rifier s'il on trouve toujours la maquette
        If Dir$(NomMaquette) <> "" Then
            '-> decharger la maquette
            UnloadMaquette
            '-> La recharger
            OpenMaq
            Start
            frmVisu.Show
            
        End If
    
End Select

'-> Gestion de l'�tat des boutons
GestPage
AffichePage

Exit Sub

GestError:

    Select Case Err.Number
    
        Case 32755
        
    End Select

End Sub

Private Sub Vertical_Change()
    Me.Page.Top = Me.Vertical.Value
    Me.Toolbar1.ZOrder
End Sub


Private Sub AffichePage()


Dim aLb As Libelle

    Me.Label2.Caption = NbPage
    Select Case PageCours
        Case -1
            Set aLb = Libelles("FRMVISU")
            Me.Label4.Caption = aLb.GetCaption(16)
            Set aLb = Nothing
        Case 0
            Set aLb = Libelles("FRMVISU")
            Me.Label4.Caption = aLb.GetCaption(15)
            Set aLb = Nothing
        Case Else
            Me.Label4.Caption = PageCours
    End Select

End Sub
