VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Spool"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Classe servant � d�finir un spool � l'int�rieur du fichier

'-> Indique le num�ro du spool dans le fichier
Public Num_Spool As Integer

'-> Nom de la maquette ASCII pour les cles %%GUI%%
Public MaquetteASCII As String

'-> D�finition ASCII de la maquette � utiliser
Private pMaquette As String

'-> D�finition OBJET de la maquette
Public Maquette As Maquette

'-> Indique si le spool poss�de une page de s�lection
Public IsSelectionPage As Boolean

'-> Indique si on peut visualiser le fichier SPool ou non ( pb de lecture sur le chargement de la maquette
Public Ok As Boolean

'-> Nombre de pages dans la spool
Public NbPage As Integer

'-> D�finition interne des pages : la page 0 �tant la page de s�lection
Private pPages() As String

'-> D�finition des pages de d�tail
Private pLigneDetails() As String

'-> Nombre de lignes de d�tail
Public NbLigDetail As Long

'-> Cl� d'acces du spool dans la collection
Public Key As String

'-> Pour affichage dans le treeview du Spool
Public SpoolText As String

'-> Pour affichage : Nom du fichier Rattach�
Public FileName As String

'-> pour affichage des pages en cours
Public CurrentPage As Integer

'-> Tableau de gestion des erreurs sur chargement de la maquette
Private pErrors() As String
Public NbError  As Integer

'-> Tableau de gestion des erreurs par page
Private pPageErrors() As String

'-> Feuille de visu associ�e au spool
Public frmDisplay As frmDisplaySpool

'-> Titre de l'acc�s au d�tail
Public TitreAccesDetail As String
Private IsTitre As Boolean

Public Function AddLigneDetail(LigneDet As String)

'---> Cette proc�dure ajoute une ligne de d�tail dans le spool

'-> Gestion du titre
If Not IsTitre Then
    TitreAccesDetail = LigneDet
    IsTitre = True
    Exit Function
End If
    
'-> Elargir la matrice des lignes de d�tail
NbLigDetail = NbLigDetail + 1
ReDim Preserve pLigneDetails(NbLigDetail)

'-> Ajouter la d�finition
pLigneDetails(NbLigDetail) = LigneDet

End Function

Public Function GetLigDetail(Ligne As Long) As String
GetLigDetail = pLigneDetails(Ligne)
End Function

Public Function AddPage() As Integer

'---> Cette fonction ajoute une page dans la matrice des pages et retourne le num de la page

'-> Elargir la matrice des pages
NbPage = NbPage + 1
ReDim Preserve pPages(NbPage)

'-> Elargir en m�me temps la matrice des erreurs par page
ReDim Preserve pPageErrors(NbPage)

AddPage = NbPage

End Function

Public Sub SetPage(ByVal PageNumber As Integer, ByVal Ligne As String)

'---> Cette proc�dure stock une ligne dans la matrice des pages
If pPages(PageNumber) = "" Then
    pPages(PageNumber) = Ligne
Else
    pPages(PageNumber) = pPages(PageNumber) & Chr(0) & Ligne
End If

End Sub

Public Sub SetErrorPage(ByVal PageNumber As Integer, ByVal Error As String)

If pPageErrors(PageNumber) = "" Then
    pPageErrors(PageNumber) = Error
Else
    pPageErrors(PageNumber) = pPageErrors(PageNumber) & Chr(0) & Error
End If

End Sub
Public Sub InitErrorPage(ByVal PageNumber As Integer)

On Error Resume Next
pPageErrors(PageNumber) = ""

End Sub


Public Function GetPage(ByVal PageNumber As Integer) As String

'---> Cette fonction retourne le contenu d'une page
GetPage = pPages(PageNumber)

End Function

Public Sub SetMaq(ByVal LigneMaq As String)

'---> Cette proc�dure ajoute une ligne dans la d�finition de la maquette
If pMaquette = "" Then
    pMaquette = LigneMaq
Else
    pMaquette = pMaquette & Chr(0) & LigneMaq
End If
    
End Sub

Public Function GetMaq() As String

'---> Cette fonction retourne le descriptif ASCII de la maquette ( chr(0) )
GetMaq = pMaquette

End Function

Public Function SetErrorMaq(ByVal LibelError As String)

'---> Cette proc�dure incr�mente le comptage des erreurs

NbError = NbError + 1
ReDim Preserve pErrors(NbError)
pErrors(NbError) = LibelError

'-> Indiquer qu'il ya une erreur sur la lecture de la maquette
Ok = False

End Function

Public Function GetErrorMaq(ByVal nError As Integer) As String

'---> Cette prco�dure retourne une erreur
GetErrorMaq = pErrors(nError)

End Function

Private Sub Class_Initialize()

'-> Initialiser la matrice pour page 0 Si page de s�lection
ReDim pPages(0)

'-> Initialiser la matrice des erreurs
ReDim pErrors(0)

''-> Par d�faut page en cours = 1
'Me.CurrentPage = 1

End Sub

Public Sub InitDisplayPage()

'---> cette proc�dure intialise les dimensions de la page pour impression

'-> Ne rien faire s'il y a des erreurs fatales
If Me.NbError <> 0 Then Exit Sub
'-> masquer la feuille
Me.frmDisplay.Page.Visible = False
'-> effacer la feuille
Me.frmDisplay.Page.Cls
'-> Redimmensionner la page � la taille de la maquette
Me.frmDisplay.Page.Width = Me.frmDisplay.ScaleX(Maquette.Largeur, 7, 3)
Me.frmDisplay.Page.Height = Me.frmDisplay.ScaleY(Maquette.Hauteur, 7, 3)
'-> Intialiser des positions d'affichage
Me.frmDisplay.LargeurX = Me.frmDisplay.Page.Width
Me.frmDisplay.MgX = Me.frmDisplay.ScaleX(1, 7, 3)
Me.frmDisplay.HauteurY = Me.frmDisplay.Page.Height
Me.frmDisplay.MgY = Me.frmDisplay.ScaleY(1, 7, 3)
'-> Masquer les barres de d�filement
Me.frmDisplay.HScroll1.Visible = False
Me.frmDisplay.VScroll1.Visible = False
'-> Initialiser les valeurs de d�filement
Me.frmDisplay.HScroll1.SmallChange = Me.frmDisplay.ScaleX(1, 7, 3)
Me.frmDisplay.HScroll1.LargeChange = Me.frmDisplay.ScaleX(5, 7, 3)
Me.frmDisplay.VScroll1.SmallChange = Me.frmDisplay.ScaleY(1, 7, 3)
Me.frmDisplay.VScroll1.LargeChange = Me.frmDisplay.ScaleY(5, 7, 3)
'-> Masquer le picturebox d'angle
Me.frmDisplay.picAngle.Visible = False
'-> Masquer �galement le treeview
Me.frmDisplay.TreeView1.Visible = False
End Sub

Public Function GetErrorPage(ByVal PageToGet As Integer) As String

'---> Cette fonction retoune les erreurs d'un pages
GetErrorPage = pPageErrors(PageToGet)

End Function

Public Sub DisplayInterfaceByPage(ByVal PageToDisplay As Integer)

'---> Cette proc�dure est utilis�e uniquement en mode visu pour afficher la bonne repr�sentation de la page

Dim i As Integer

'-> Test sur l'impression des erreurs fatales
If Me.NbError <> 0 Then
    '-> Rendre le treeview visible
    Me.frmDisplay.TreeView1.Visible = True
    '-> Masquer la barre d'outils
    Me.frmDisplay.Toolbar1.Visible = False
    '-> Vider le treeveiew
    Me.frmDisplay.TreeView1.Nodes.Clear
    '-> Charger la liste des erreurs
    For i = 1 To NbError
        Me.frmDisplay.TreeView1.Nodes.Add , , "ERR|" & i, Entry(i, pErrors(i), Chr(0)), "Warning"
    Next 'Pour toutes les erreurs
    Exit Sub
End If


If pPageErrors(PageToDisplay) <> "" Then
    '-> Rendre le treeview visible
    Me.frmDisplay.TreeView1.Visible = True
    '-> Masquer la barre d'outils
    Me.frmDisplay.Toolbar1.Visible = False
    '-> Vider le treeveiew
    Me.frmDisplay.TreeView1.Nodes.Clear
    '-> Charger la liste des erreurs
    For i = 1 To NumEntries(pPageErrors(PageToDisplay), Chr(0))
        Me.frmDisplay.TreeView1.Nodes.Add , , "ERR|" & i, Entry(i, pPageErrors(PageToDisplay), Chr(0)), "Warning"
    Next 'Pour toutes les erreurs
Else
    '-> Masquer le treeview
    Me.frmDisplay.TreeView1.Visible = False
    '-> Afficher la barre d'outils
    Me.frmDisplay.Toolbar1.Visible = True
    '-> Positionner la page au bon endroit
    Me.frmDisplay.HScroll1.Value = Me.frmDisplay.HScroll1.Min
    Me.frmDisplay.VScroll1.Value = Me.frmDisplay.VScroll1.Min
    '-> Afficher la feuille
    Me.frmDisplay.Page.Visible = True
    '-> Forcer le resize de la feuille pour affichage et gestion des scrollBars
    Me.frmDisplay.Form_Resize
End If 'S'il y a des erreurs sur cette page

'-> afficher la page
Me.frmDisplay.WindowState = vbMaximized
Me.frmDisplay.Show


End Sub

