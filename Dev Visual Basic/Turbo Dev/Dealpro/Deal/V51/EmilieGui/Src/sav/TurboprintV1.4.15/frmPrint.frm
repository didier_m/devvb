VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "COMCT232.OCX"
Begin VB.Form frmPrint 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3240
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7395
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3240
   ScaleWidth      =   7395
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   1440
      Top             =   2640
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Command3 
      Height          =   375
      Left            =   6120
      TabIndex        =   12
      Top             =   2760
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      Height          =   375
      Left            =   4800
      TabIndex        =   11
      Top             =   2760
      Width           =   1215
   End
   Begin VB.Frame Frame3 
      Height          =   1455
      Left            =   3840
      TabIndex        =   8
      Top             =   1080
      Width           =   3495
      Begin VB.TextBox Text3 
         Height          =   300
         Left            =   1920
         TabIndex        =   9
         Text            =   "1"
         Top             =   240
         Width           =   615
      End
      Begin ComCtl2.UpDown UpDown1 
         Height          =   300
         Left            =   2535
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   240
         Width           =   255
         _ExtentX        =   423
         _ExtentY        =   529
         _Version        =   327681
         BuddyControl    =   "Text3"
         BuddyDispid     =   196612
         OrigLeft        =   2520
         OrigTop         =   391
         OrigRight       =   2760
         OrigBottom      =   626
         Max             =   500
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.Frame Frame4 
         Height          =   1455
         Left            =   0
         TabIndex        =   17
         Top             =   0
         Width           =   3495
         Begin VB.CheckBox Check1 
            Alignment       =   1  'Right Justify
            Height          =   195
            Left            =   1320
            TabIndex        =   18
            Top             =   1200
            Width           =   2055
         End
         Begin VB.Image Image2 
            Height          =   600
            Left            =   120
            Picture         =   "frmPrint.frx":0000
            Top             =   480
            Width           =   1620
         End
         Begin VB.Label Label5 
            Height          =   255
            Left            =   120
            TabIndex        =   19
            Top             =   240
            Width           =   1695
         End
      End
      Begin VB.Label Label4 
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   1695
      End
      Begin VB.Image Image1 
         Height          =   600
         Left            =   120
         Picture         =   "frmPrint.frx":4FC2
         Top             =   720
         Width           =   1620
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1455
      Left            =   60
      TabIndex        =   2
      Top             =   1080
      Width           =   3675
      Begin VB.OptionButton Option4 
         Height          =   255
         Left            =   1800
         TabIndex        =   20
         Top             =   360
         Width           =   1695
      End
      Begin VB.OptionButton Option3 
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   1695
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   2760
         TabIndex        =   6
         Top             =   720
         Width           =   495
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   1800
         TabIndex        =   5
         Top             =   720
         Width           =   495
      End
      Begin VB.OptionButton Option2 
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   720
         Width           =   1215
      End
      Begin VB.OptionButton Option1 
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.Label Label3 
         Height          =   255
         Left            =   2400
         TabIndex        =   16
         Top             =   720
         Width           =   375
      End
      Begin VB.Label Label2 
         Height          =   255
         Left            =   1440
         TabIndex        =   14
         Top             =   720
         Width           =   375
      End
   End
   Begin VB.Frame Frame1 
      Height          =   975
      Left            =   60
      TabIndex        =   0
      Top             =   3
      Width           =   7275
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   720
         TabIndex        =   1
         Top             =   360
         Width           =   6375
      End
      Begin VB.Label Label1 
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'-> Indique le fichier en cours de traitement
Public FichierName As String
'-> Indique la cl� du spool actuel
Public SpoolKey As String
Public IsGetPrinter As Boolean


Public Sub InitialisationGetPrint()

'---> Cette proc�dure initialise la liste desz imprimantes

On Error Resume Next

Dim lpBuffer As String
Dim Res As Long

'-> Pointer sur la classe libell�
Set aLb = Libelles("FRMPRINTVISU")

'-> Rajouter en entete la visualisation �cran 'MESSPROG
Me.Combo1.AddItem aLb.GetCaption(17), 0
IsGetPrinter = True

'-> Bloquer la nature � imprimer
Me.Option2.Enabled = False
Me.Option3.Enabled = False

'-> Bloquer les zones de saisie
Me.Text1.Enabled = False
Me.Text2.Enabled = False
Me.Label2.Enabled = False
Me.Label3.Enabled = False

'-> Chercher une variable d'environnement
lpBuffer = Space$(50)
Res = GetEnvironmentVariable("TURBODEFAULT", lpBuffer, Len(lpBuffer))
If Res = 0 Then
    '-> Si on doit s�lectionner l'imprimante par d�faut
    lpBuffer = GetIniString("PRINTER", "DEFAULT", TurboGraphIniFile, False)
    If UCase$(Trim(lpBuffer)) = "SCREEN" Then Me.Combo1.ListIndex = 0
Else
    If UCase$(Trim(Mid$(lpBuffer, 1, Res))) = "SCREEN" Then Me.Combo1.ListIndex = 0
End If

End Sub

Private Sub Command2_Click()

'---> Renvoyer une valeur

On Error Resume Next

Dim aLb As Libelle

'-> Pointer sur la classe libelle
Set aLb = Libelles("FRMPRINTVISU")

'-> V�rifier q'une imprimante soit saisie
If Me.Combo1.ListIndex = -1 Then
    MsgBox aLb.GetCaption(13), vbCritical + vbOKOnly, aLb.GetToolTip(13)
    Me.Combo1.SetFocus
    Exit Sub
End If

If Me.Option2.Value Then
    '-> si page mini s�lectionn�e v�rifier les zones
    If Trim(Me.Text1.Text) = "" Then
        MsgBox aLb.GetCaption(15), vbCritical + vbOKOnly, aLb.GetToolTip(15)
        Me.Text1.SetFocus
        Exit Sub
    End If
    
    '-> si page mini s�lectionn�e v�rifier les zones
    If Trim(Me.Text2.Text) = "" Then
        MsgBox aLb.GetCaption(16), vbCritical + vbOKOnly, aLb.GetToolTip(15)
        Me.Text2.SetFocus
        Exit Sub
    End If
End If

'-> V�rifier le nombre de copies
If Trim(Me.Text3.Text) = "" Then
    MsgBox aLb.GetCaption(14), vbCritical + vbOKOnly, aLb.GetToolTip(14)
    Me.Text3.SetFocus
    Exit Sub
End If

'-> Imprimante
If Me.Combo1.ListIndex = 0 And IsGetPrinter Then
    strRetour = "DEALVIEW"
Else
    strRetour = Me.Combo1.Text
End If

'-> Nature d'impression
If Me.Option1.Value Then
    strRetour = strRetour & "|" & 1
ElseIf Me.Option2.Value Then
    strRetour = strRetour & "|" & 2 & "�" & Me.Text1.Text & "�" & Me.Text2.Text
Else
    If Me.Option3.Value Then
        strRetour = strRetour & "|" & 3
    Else
        strRetour = strRetour & "|" & 4
    End If
End If

'-> nombre de copies
strRetour = strRetour & "|" & Me.Text3.Text

'-> Teste si impression recto verso
If Me.Check1.Value = 1 Then
    strRetour = strRetour & "|RV"
Else
    strRetour = strRetour & "|NORV"
End If
'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command3_Click()
    Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Command2_Click

End Sub

Private Sub Form_Load()

Dim aPrint As Printer
Dim aLb As Libelle

On Error Resume Next

'-> Charger la liste des imprimantes
For Each aPrint In Printers
    Me.Combo1.AddItem aPrint.DeviceName
    If aPrint.DeviceName = Printer.DeviceName Then _
        Me.Combo1.ListIndex = Me.Combo1.ListCount - 1
Next

'-> Pointer vers la classe des libelles
Set aLb = Libelles("FRMPRINTVISU")

'-> Charger les libell�s
Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Frame2.Caption = aLb.GetCaption(3)
Me.Option1.Caption = aLb.GetCaption(4)
Me.Option2.Caption = aLb.GetCaption(5)
Me.Label2.Caption = aLb.GetCaption(6)
Me.Label3.Caption = aLb.GetCaption(7)
Me.Option3.Caption = aLb.GetCaption(8)
Me.Frame3.Caption = aLb.GetCaption(9)
Me.Label4.Caption = aLb.GetCaption(10)
Me.Command2.Caption = aLb.GetCaption(11)
Me.Command3.Cancel = True
Me.Command3.Caption = aLb.GetCaption(12)
Me.Check1.Caption = aLb.GetCaption(19)
Me.Option4.Caption = aLb.GetCaption(20)

End Sub

Private Sub Option1_Click()

Me.Text1.Enabled = False
Me.Text2.Enabled = False
Me.Label2.Enabled = False
Me.Label3.Enabled = False

End Sub

Private Sub Option2_Click()

Me.Text1.Enabled = True
Me.Text2.Enabled = True
Me.Label2.Enabled = True
Me.Label3.Enabled = True

End Sub

Private Sub Option3_Click()

Option1_Click

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)

If KeyAscii < 48 Or KeyAscii > 57 Then
    If KeyAscii <> 8 Then KeyAscii = 0
End If

End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)

If KeyAscii < 48 Or KeyAscii > 57 Then
    If KeyAscii <> 8 Then KeyAscii = 0
End If

End Sub

Private Sub Text3_KeyPress(KeyAscii As Integer)

If KeyAscii < 48 Or KeyAscii > 57 Then
    If KeyAscii <> 8 Then KeyAscii = 0
End If

End Sub

Private Sub UpDown1_Change()

Me.Text3.SelStart = 0
Me.Text3.SelLength = Len(Me.Text3.Text)
Me.Text3.SetFocus

End Sub

