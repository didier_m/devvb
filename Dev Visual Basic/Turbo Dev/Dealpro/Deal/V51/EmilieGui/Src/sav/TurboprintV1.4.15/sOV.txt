Private Sub PrintObjRtf(PositionX As Long, PositionY As Long, _
                        aObject As Object, ByVal DataFields As String, _
                        DebutRangX As Long, DebutRangY As Long, Optional MargeInterne As Single)


'---> Cette fonction imprime un cadre ou une section ( Code RTF + Bordures )

Dim aField() As String
Dim aRtf As RichTextBox
Dim PosX As Long, PosY As Long
Dim i As Integer
Dim Champ As String
Dim RTFvalue As String
Dim fr As FORMATRANGE
Dim lTextOut As Long, lTextAmt As Long, Res As Long
Dim aPoint As POINTAPI
Dim aRect As RECT
Dim hdlPen As Long
Dim oldPen As Long
Dim hdlBrush As Long
Dim oldBrush As Long
Dim NbChamp As Integer
Dim Temp1 As Long
Dim Temp2 As Long
Dim ClipRgn As Long
Dim OldClip As Long
Dim Pix1X As Long, Pix1Y As Long

On Error Resume Next

'On Error GoTo GestErr

'-> Mettre l'�diteru RTF au bonnes dimensions
Set aRtf = frmLib.Rtf(aObject.IdRTf)
aRtf.Width = frmLib.ScaleX(aObject.Largeur, 7, 1)
aRtf.Height = frmLib.ScaleY(aObject.Hauteur, 7, 1)

'-> R�cup�ration du code RTF
RTFvalue = aRtf.TextRTF

ErrorLibelle = "Pb dans la cr�ation de la liste des champs associ�s � : " & aObject.Nom & " DatFields -> " & DataFields

'-> Cr�er une base de champs
If Trim(DataFields) = "" Then
Else
    '-> R�cup�ration du nombre de champs
    NbChamp = NumEntries(DataFields, "^")
    For i = 2 To NbChamp
        Champ = Entry(i, DataFields, "^")
        ReDim Preserve aField(1 To 2, i - 1)
        aField(1, i - 1) = "^" & Mid$(Champ, 1, 4)
        aField(2, i - 1) = RTrim(Mid$(Champ, 5, Len(Champ) - 1))
    Next
    '-> Faire un remplacement des champs par leur valeur
    For i = 1 To NbChamp - 1
        aRtf.TextRTF = Replace(aRtf.TextRTF, aField(1, i), aField(2, i))
    Next
    '-> Redonner au controle RTF sa valeur
End If

ErrorLibelle = "Pb dans le calcul des positions X et Y de l'objet : " & aObject.Nom

'-> Calcul des positions d'impression de l'objet selon le type d'objet � imprimer
If TypeOf aObject Is Section Then
    Select Case aObject.AlignementLeft
        Case 2 'Marge gauche
            PosX = Sortie.ScaleX(aMaq.MargeLeft, 7, 1)
        Case 4 'Centr�
            PosX = Sortie.ScaleX((aMaq.Largeur - aObject.Largeur) / 2, 7, 1)
        Case 3 'Marge Droite
            PosX = Sortie.ScaleX(aMaq.Largeur - aObject.Largeur - aMaq.MargeLeft, 7, 1)
        Case 5 'Sp�cifi�
            PosX = Sortie.ScaleX(aObject.Left, 7, 1)
    End Select
    Select Case aObject.AlignementTop
        Case 1 'Libre
            PosY = Sortie.ScaleY(PositionY, 3, 1)
        Case 2 'Marge haut
            PosY = Sortie.ScaleY(aMaq.MargeTop, 7, 1)
        Case 4 'Centr�
            PosY = Sortie.ScaleY((PageHauteur - aObject.Hauteur) / 2, 7, 1)
        Case 3 'Marge bas
            PosY = Sortie.ScaleY(PageHauteur - aObject.Hauteur - aMaq.MargeTop, 7, 1)
        Case 5 'Sp�cifi�
            PosY = Sortie.ScaleY(aObject.Top, 7, 1)
    End Select

    '-> Tenir compte des variations des marges internes
    PosX = PosX - Sortie.ScaleX(MargeX, 3, 1)
    If aObject.AlignementTop <> 1 Then PosY = PosY - Sortie.ScaleY(MargeY, 3, 1)

    '-> Setting du rect de dessin
    aRect.Left = Sortie.ScaleX(PosX, 1, 3)
    aRect.Top = Sortie.ScaleY(PosY, 1, 3)
    aRect.Right = Sortie.ScaleX(aObject.Largeur, 7, 3) + Sortie.ScaleX(PosX, 1, 3)
    aRect.Bottom = Sortie.ScaleY(aObject.Hauteur, 7, 3) + Sortie.ScaleY(PosY, 1, 3)

    '-> Sauvagarder la position de d�but du rang
    DebutRangX = PosX
    DebutRangY = PosY
    
ElseIf TypeOf aObject Is Cadre Then
    '-> Il faut modifier la position X et Y du cadre
    PosX = DebutRangX + Sortie.ScaleX(aObject.Left, 7, 1)
    PosY = DebutRangY + Sortie.ScaleY(aObject.Top, 7, 1)
    '-> Setting du rect de dessin
    aRect.Left = Sortie.ScaleX(PosX, 1, 3)
    aRect.Top = Sortie.ScaleY(PosY, 1, 3)
    aRect.Right = Sortie.ScaleX(aObject.Largeur, 7, 3) + Sortie.ScaleX(PosX, 1, 3)
    aRect.Bottom = Sortie.ScaleY(aObject.Hauteur, 7, 3) + Sortie.ScaleY(PosY, 1, 3)
End If


ErrorLibelle = "Pb dans le dessin du fond de l'objet : " & aObject.Nom

'-> Imprimer le fond de l'objet
hdlBrush = CreateSolidBrush(aObject.BackColor)
hdlPen = CreatePen(PS_NULL, 1, QBColor(15))

'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(Sortie.hdc, hdlBrush)
oldPen = SelectObject(Sortie.hdc, hdlPen)

'-> Dessin de l'objet
Res = Rectangle(Sortie.hdc, aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)

ErrorLibelle = "Pb dans le dessin des bordures de : " & aObject.Nom

'-> Dessin du contour de l'objet si necessaire
If TypeOf aObject Is Section Then
    If aObject.Contour Then
        hdlBrush = CreateSolidBrush(0)
        FrameRect Sortie.hdc, aRect, hdlBrush
    End If
ElseIf TypeOf aObject Is Cadre Then

'    '-> Tenir compte de la largeur du trait
'    Temp2 = frmLib.ScaleX(aObject.LargeurTrait, 3, 1)
'    Temp2 = Sortie.ScaleX(Temp2, 1, 3)
'
'    Pix1X = frmLib.ScaleX(1, 3, 1)
'    Pix1X = Sortie.ScaleX(Pix1X, 1, 3)
'
'    Pix1Y = frmLib.ScaleY(1, 3, 1)
'    Pix1Y = Sortie.ScaleY(Pix1Y, 1, 3)
'
'    '-> cr�er un stylo de dessin
'    hdlPen = CreatePen(PS_SOLID, Temp2, 0)
'    SelectObject Sortie.hdc, hdlPen
'
'    '-> S�lectionner le rectangle comme zone de clipping
'    ClipRgn = CreateRectRgn(aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
'    OldClip = SelectClipRgn(Sortie.hdc, ClipRgn)
'
'    '-> Bordure gauche
'    If aObject.Gauche Then
'        '-> Dessin des bordures
'        Temp1 = Fix(Temp2 / 2)
'        MoveToEx Sortie.hdc, aRect.Left + Temp1, aRect.Top, aPoint
'        LineTo Sortie.hdc, aRect.Left + Temp1, aRect.Bottom - 1
'
'    End If
'    '-> Bordure haut
'    If aObject.Haut Then
'        Temp1 = Fix(Temp2 / 2)
'        MoveToEx Sortie.hdc, aRect.Left, aRect.Top + Temp1, aPoint
'        LineTo Sortie.hdc, aRect.Right, aRect.Top + Temp1
'    End If
'    '-> Bordure Droite
'    If aObject.Droite Then
'        If (Temp2 / 2) = Fix(Temp2 / 2) Then
'            Temp1 = Temp2 / 2
'        Else
'            Temp1 = (Temp2 + 1) / 2
'        End If
'        MoveToEx Sortie.hdc, aRect.Right - Temp1, aRect.Top, aPoint
'        LineTo Sortie.hdc, aRect.Right - Temp1, aRect.Bottom
'    End If
'    '-> Bordure Bas
'    If aObject.Bas Then
'        If (Temp2 / 2) = Fix(Temp2 / 2) Then
'            Temp1 = Temp2 / 2
'        Else
'            Temp1 = (Temp2 + 1) / 2
'        End If
'        MoveToEx Sortie.hdc, aRect.Left, aRect.Bottom - Temp1, aPoint
'        LineTo Sortie.hdc, aRect.Right, aRect.Bottom - Temp1
'    End If
'
'    '-> Restaurer la zone de clipping
'    If OldClip <> 0 Then
'        SelectClipRgn Sortie.hdc, OldClip
'        DeleteObject ClipRgn
'    End If
'
'End If
'
''-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
'If oldPen <> 0 Then
'    SelectObject Sortie.hdc, oldPen
'    DeleteObject hdlPen
'End If
'
'If oldBrush <> 0 Then
'    SelectObject Sortie.hdc, oldBrush
'    DeleteObject hdlBrush
'End If


    '-> cr�er un stylo de dessin
    hdlPen = CreatePen(PS_SOLID, 1, 0)
    SelectObject Sortie.hdc, hdlPen
    '-> Bordure gauche
    If aObject.Gauche Then
        MoveToEx Sortie.hdc, aRect.Left, aRect.Top, aPoint
        LineTo Sortie.hdc, aRect.Left, aRect.Bottom - 1
    End If
    '-> Bordure haut
    If aObject.Haut Then
        MoveToEx Sortie.hdc, aRect.Left, aRect.Top, aPoint
        LineTo Sortie.hdc, aRect.Right - 1, aRect.Top
    End If
    '-> Bordure Droite
    If aObject.Droite Then
        MoveToEx Sortie.hdc, aRect.Right - 1, aRect.Top, aPoint
        LineTo Sortie.hdc, aRect.Right - 1, aRect.Bottom
    End If
    '-> Bordure Bas
    If aObject.Bas Then
        MoveToEx Sortie.hdc, aRect.Left, aRect.Bottom - 1, aPoint
        LineTo Sortie.hdc, aRect.Right - 1, aRect.Bottom - 1
    End If
End If

'-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
If oldPen <> 0 Then
    SelectObject Sortie.hdc, oldPen
    DeleteObject hdlPen
End If

If oldBrush <> 0 Then
    SelectObject Sortie.hdc, oldBrush
    DeleteObject hdlBrush
End If

ErrorLibelle = "Pb dans l'impression du texte RTF de l'objet : " & aObject.Nom

fr.hdc = Sortie.hdc
fr.hdcTarget = Sortie.hdc
fr.chrg.cpMin = 0
fr.chrg.cpMax = -1

'-> Intialisation du rectangle destination
fr.rc.Left = Sortie.ScaleX(MargeInterne, 7, 1) + PosX
fr.rc.Top = Sortie.ScaleX(MargeInterne, 7, 1) + PosY
fr.rc.Right = Sortie.ScaleX(aObject.Largeur, 7, 1) + PosX
fr.rc.Bottom = Sortie.ScaleY(aObject.Hauteur, 7, 1) + PosY

'-> Initialisation du rectangle de source
fr.rcPage.Left = 0
fr.rcPage.Top = 0
fr.rcPage.Right = Sortie.ScaleX(aObject.Largeur, 7, 1)
fr.rcPage.Bottom = Sortie.ScaleY(aObject.Hauteur, 7, 1)

'-> Faire un setting du mode de restitution
Res = SetMapMode(Sortie.hdc, MM_TEXT)


'-> initialisation des variables de pointage de texte
lTextOut = 0
lTextAmt = SendMessage(aRtf.hwnd, WM_GETTEXTLENGTH, 0, 0)

'-> Impression du Rtf
Do While lTextOut < lTextAmt
    lTextOut = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, fr)
    If lTextOut < lTextAmt Then
        fr.chrg.cpMin = lTextOut
        fr.chrg.cpMax = -1
    End If
Loop

ErrorLibelle = "Impossible de lib�rer les ressources  :" & aObject.Nom

'-> Lib�rer la ressource associ�e au RTF : VERSION COMPILEE
Res = SendMessageBis(aRtf.hwnd, EM_FORMATRANGE, -1, 0)

'-> VERSION INTERPRETEE
'Res = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, Null)

'-> Redonner son vrai contenu au controle RTF
aRtf.TextRTF = RTFvalue

'-> Lib�rer le pointeur sur le controle RTF
Set aRtf = Nothing

Exit Sub

GestErr:

    ErrorCode = 28
    Call GestError


End Sub
