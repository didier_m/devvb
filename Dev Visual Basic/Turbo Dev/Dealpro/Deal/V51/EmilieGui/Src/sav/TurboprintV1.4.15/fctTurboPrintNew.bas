Attribute VB_Name = "fctTurboPrintNew"
Option Explicit

'---> Indique si on est en version Entreprise (0) ou profressionelle (1)
Public VersionTurbo As Integer
Public TurboGraphIniFile As String
Public Tm_PictureIniFile As String
Public TurboMaqIniFile As String
Public FichierLangue As String
Public IndexLangue As Integer
Public AskForLangue As Boolean

'---> Pour gestion des Messporg : version Internet
Public IsAutoVersion As Boolean

'---> Pour gestion des p�riph�riques de sortie
Public Mode As Integer '-> Indique le mode de consultation envoy� sur la ligne de commande
Public Sortie As Object '-> Contient le p�riph�rique de sortie
Public MargeX As Integer '-> Marge interne d'un DC axe X pour impression papier
Public MargeY As Integer '-> Marge interne d'un DC axe Y pour impression papier
Public TempPrinter As Printer '-> Objet de type printer temporaire pour test de validit� imprimante
Public SpoolSize As Long '-> Indique le nombre de page par job d'impression
Public SpoolPrint As Long '-> Indique le nombre de pages imprim�es dans le spool en cours

'---> Pour ouverture des fichiers
Public OpenPath As String 'Indique le path d�faut
Public FirstOpen As Boolean 'N'en tenir compte que la premi�re fois

'---> Pour impression de la page des s�lections
Public Const SelectRTFKey = "%%SELECTIONRTF%%@@THIERRY"

'---> Pour dessin de la temporisation
Public TailleLue As Long
Public TailleTotale As Long

'---> Pour export Excel
Public ExcelOk As Boolean

'---> Pour envoie Internet
Public OutLookOk As Boolean
Public NetParam As String
Public IsCryptedFile As Boolean

'---> Pour envoie vers NOTEPAD ou WORDPAD
Public EditorPath As String

'---> Pour echange entre les feuilles
Public strRetour As String

'---> Pour cr�ation dynamique des controles
Public IdRTf As Integer '-> Contient l'index du prochain Controle RTF qui sera charg�
Public IdBmp As Integer '-> Contient l'index du prochain controle PictureBox qui sera cr��

'---> Structure de controle de dessin d'une cellule d'un objet tableau
Public Type FormatedCell
    Ok As Boolean
    nbDec As Integer
    strFormatted As String
    Value As Double
    idNegatif As Integer
End Type

'---> Liste des collections pour gestion du multi Spool
Public Fichiers As Collection

'---> Gestion des paths
Public TempDirectory As String

'---> Pour gestion de la cr�ation des blocks de tableau
Dim sCol() As String
Dim sLigne() As String
Dim LigLue As Integer

'---> Pour gestion des envoies par messagerie
Public OrigineMail As Integer '-> Indique la provenance de l'envoie
 '-> Valeurs : 0 -> OutLook depuis Visu _
               1 -> Internet depuis visu _
               2 -> OutLook depuis commande _
               3 -> Internet depuis commande
Public ExportMail As Integer '-> Indique la nature de l'export
'-> Valeurs : 0 -> Page en Cours, 1 -> Spool en cours , Fichier en cours
Public SpoolKeyMail As String '-> Cl� du spool en cours depuis visu
Public FileKeyMail As String '-> Cl� du fihcier en cours de visu
Public PageNumMail As Integer '-> Num�ro de la page que l'on d�sire envoyer
Public SendOutLook As Boolean '-> Indique si on doit afficher ou envoyer le mail vers OutLook

Public IsMouchard As Boolean

'-> Pour gestion des
Public KillSpool As Boolean
Public IsPDF As Boolean
Public PdfDestinationFile As String

Public Sub GetPrinterList(PrintFile As String)

'---> Cette proc�dure affiche la liste des imprimante pour la s�lection

Dim NomImp As String
Dim NbCopies As Integer
Dim LigneCommande As String


'-> Vider la variable d'�change
strRetour = ""

'-> Charger avec la liste des imprimantes
Load frmPrint

'-> Initialiser avec la fonction qui va bien
frmPrint.InitialisationGetPrint

'-> Afficher la feuille
frmPrint.Show vbModal
If strRetour = "" Then End

'-> Get du nom de l'imprimante
NomImp = Entry(1, strRetour, "|")
NbCopies = CInt(Entry(3, strRetour, "|"))

If UCase$(NomImp) = "DEALVIEW" Then
    LigneCommande = "ECRAN$" & IndexLangue & "|" & PrintFile
Else
    LigneCommande = NomImp & "~DIRECT~" & NbCopies & "|" & PrintFile
End If

'-> Lancer le turbo avec la nouvelle
Shell App.Path & "\TurboGraph.exe " & LigneCommande
End

End Sub

Private Function InitTurboVersion()

Dim strPath As String
Dim i As Integer
Dim Res As Long
Dim lpBuffer As String
Dim TempFileName As String

'->  D�tection de la version Professionelle: Pr�sence du fichier Turbo.ver et Path

On Error Resume Next

'-> Recherche du fichier Version :
If Dir$(App.Path & "\Turbo.ver") = "" Then
    '-> On ne trouve pas le fichier Ver donc on est en version Internet.
    VersionTurbo = 1 'Version internet
Else
    '-> Analyse du path
    If InStr(1, UCase$(App.Path), "\EMILIEGUI\") <> 0 Then
        VersionTurbo = 0 'Version Path v51
    Else
        VersionTurbo = 1 'Version Internet
    End If
End If

'-> Par d�faut on est en fran�ais
IndexLangue = 1

'-> On est en version Entreprise : recherche du path des fichiers Ini et des fichiers Ressources
If VersionTurbo = 1 Then 'Version internet
    TurboGraphIniFile = "" '-> Pas d'acc�s au fichier TurboGrpah.ini
    Tm_PictureIniFile = "" '-> Pas d'acc�s au fichier Tm_picture.ini
    TurboMaqIniFile = "" '-> Pas d'acc�s au fichier TurboMaq.ini
    FichierLangue = "" '-> Pas de fichiers Langue - > Charger en fichier Ressource
    AskForLangue = True '-> Demander le fichier Langue
    Exit Function
Else 'Verdion Entreprise

    '-> Cr�taion du path du fichier Langue
    i = InStr(UCase$(App.Path), "\EMILIEGUI\")
    FichierLangue = Mid$(App.Path, 1, i + 10) & "MQT"
    
    '-> Recherche du r�pertoire Turbo ou path dans le r�pertoire applicatif
    If Dir$(App.Path & "\TurboMaq.ini") <> "" Then strPath = UCase$(App.Path & "\")
    strPath = UCase$(App.Path)
    i = InStr(strPath, "DEALPRO\")
    strPath = Mid$(strPath, 1, i - 1)
    strPath = strPath & "Turbo\"

    '-> Setting du fichier TurboGrpah.ini
    TurboGraphIniFile = strPath & "TurboGraph.ini"
    If Dir$(TurboGraphIniFile, vbNormal) = "" Then
        TurboGraphIniFile = ""
    Else
        TempFileName = GetTempFileNameVB("INI")
        CopyFile TurboGraphIniFile, TempFileName, 0
        TurboGraphIniFile = TempFileName
    End If

    '-> Setting du fichier TM_Picture.ini
    Tm_PictureIniFile = strPath & "Tm_Picture.ini"
    If Dir$(Tm_PictureIniFile, vbNormal) = "" Then
        Tm_PictureIniFile = ""
    Else
        TempFileName = GetTempFileNameVB("INI")
        CopyFile Tm_PictureIniFile, TempFileName, 0
        Tm_PictureIniFile = TempFileName
    End If
    
    '-> Setting du fichier TurboMaq.ini
    TurboMaqIniFile = strPath & "TurboMaq.ini"
    If Dir$(TurboMaqIniFile, vbNormal) = "" Then
        TurboMaqIniFile = ""
    Else
        TempFileName = GetTempFileNameVB("INI")
        CopyFile TurboMaqIniFile, TempFileName, 0
        TurboMaqIniFile = TempFileName
    End If
    
End If

'-> Initialisation de la version entreprise

'-> Recherche du fichier Langue
If TurboGraphIniFile <> "" Then
    lpBuffer = GetIniString("PARAM", "LANGUE", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then
        '-> Tester si la zone est num�rique
        If IsNumeric(lpBuffer) Then
            IndexLangue = CInt(lpBuffer)
            AskForLangue = False
        Else
            '-> Il faut demander la langue de l'utilisateur
            AskForLangue = True
            IndexLangue = 0
        End If
    Else
        '-> Demander le fichier langue � charger
        AskForLangue = True
    End If
    
    '-> Taille du spool
    lpBuffer = GetIniString("PARAM", "SPOOLSIZE", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then
        If IsNumeric(lpBuffer) Then SpoolSize = CLng(lpBuffer)
    End If
    
    '-> R�pertoire d'export HTML
    lpBuffer = GetIniString("PARAM", "EXPORTHTML", TurboGraphIniFile, False)
    If lpBuffer <> "NULL" Then
        PathToExport = Trim(lpBuffer)
        '-> V�rifier si on ne demande pas de travailler dans le r�pertoire de travail de Progress
        If UCase$(Trim(Entry(1, lpBuffer, "$"))) = "ENV" Then
            '-> R�cup�rer la variable d'environnement
            lpBuffer = GetVariableEnv(Entry(2, lpBuffer, "$"))
            '-> V�rifier la valeur
            If Trim(lpBuffer) <> "" Then
                If Dir$(lpBuffer, vbDirectory) <> "" Then
                    PathToExport = lpBuffer
                Else
                    PathToExport = ""
                End If
            Else
                PathToExport = ""
            End If
        Else
            If Dir$(PathToExport, vbDirectory) = "" Then PathToExport = ""
        End If
    End If
End If 'Si on a acc�s au fichier Ini

End Function

Public Function GetVariableEnv(strVariable As String) As String

Dim Res As Long
Dim lpBuffer As String

lpBuffer = Space$(500)
Res = GetEnvironmentVariable(strVariable, lpBuffer, Len(lpBuffer))
If Res <> 0 Then
    '-> Faire un trim sur le buffer
    lpBuffer = Mid$(lpBuffer, 1, Res)
Else
    lpBuffer = ""
End If

'-> Retouner la valeur
GetVariableEnv = lpBuffer

End Function



Private Sub Initialisation()

'---> Cette proc�dure est charg�e d'initialiser les diff�rentes variables n�c�ssaires au moteur d'impression

Dim Res As Long
Dim lpBuffer As String

'-> Attention, il existe 2 versions du TURBO.

'-> Version Professionelle  -> Libre
'-> Version Entreprise - > Dans architecture

'-> Initialiser les index de cr�ation des prochains objets RichTextFormat et BITMAP
IdRTf = 1
IdBmp = 1

'-> Initialiser la collection des fichiers ouverts dans le TurboGraph
Set Fichiers = New Collection

'-> R�cup�ration des informations de formatage num�rique
lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, lpBuffer, Len(lpBuffer))
SepDec = Mid$(lpBuffer, 1, Res - 1)
lpBuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
SepMil = Mid$(lpBuffer, 1, Res - 1)

'-> R�cup�ration du r�pertoire d'ouverture par d�faut
lpBuffer = Space$(255)
Res = GetEnvironmentVariable("DEALTEMPO", lpBuffer, Len(lpBuffer))
If Res <> 0 Then
    OpenPath = Mid$(lpBuffer, 1, Res)
    If Dir$(OpenPath, vbDirectory) = "" Then OpenPath = ""
End If

'-> R�cup�rer la path du r�pertoire temporaire de windows
TempDirectory = GetTempFileNameVB("", True)

'-> Lancer l'initialisation de la gestion des versions du Turbo
Call InitTurboVersion

End Sub


Public Sub Main()

'---> Point d'entr�e du nouveau module

Dim lpBuffer As String
Dim Res As Long
Dim LigneCommande As String
Dim Param1 As String
Dim DeviceName As String
Dim ModeName As String
Dim aPrinter As Printer
Dim RectoVerso As Boolean
Dim FindPrinter As Boolean
Dim Ligne As String
Dim i As Integer, j As Integer, k As Integer
Dim PageDebut As Integer
Dim aPath As String
Dim Tempo As String
Dim TempFileName As String
Dim NomFichier As String
Dim aFichier As Fichier
Dim aSpool As Spool
Dim CountPage As Integer

'-> Pour argument sur la ligne de command epour IE, OutLook , HTML
Dim pFile As String
Dim pTo As String
Dim pCopies As String
Dim pObject As String
Dim pBody As String
Dim pBodyFile As String
Dim pFormat As String
Dim pMode As String
Dim pSpoolName As String
Dim pDir As String
Dim KeyValue As String
Dim pValue As String
Dim pRun As String
Dim pCrypt As String

Dim Mouchard As String
Dim hdlMouchard As Integer
Dim MyCodeLangue As String


' Descriptif de la ligne de commande
'[Param1]|[Param2]

'************************************
'* Envoie de fichier sur imprimante *
'************************************

'PRINTER$DIRECT[$NBCOPIES]|Fichier
'PRINTER$BATCH[$NBCOPIES]|Fichier
'ECRAN|Fichier
'DEFAULT|FICHIER

' \\DEALNT16\HP LaserJet 4000 Maintenance$DIRECT|C:\Travail\OF02362.lgr
' \\DELL_177\EPSON Stylus COLOR 900$DIRECT|C:\Travail\OF02362.lgr
' DIRECT|C:\Travail\OF02362.lgr
' Acrobat PdfWriter~DIRECT~1|D:\Travailv51\infusio.turbo*FileName*Kill[0/1]

'*********************************************************
'* Envoie de spool sur le net, Messagerie ou export HTML *
'*********************************************************

'OUTLOOK|FILE= TO= COPIES= OBJET= BODY= BODYFILE= FORMAT= MODE= SPOOLNAME= CRYPT=
'INTERNET|FILE= TO= COPIES= OBJET= BODY= BODYFILE= FORMAT= MODE= SPOOLNAME= CRYPT=
'HTML|FILE= DIR= MODE= SPOOLNAME= RUN=

' S�parateur $
' FORMAT : 0 -> Format Turbo , 1 -> Format HTML
' CRYPT : indique si le spool doit �tre crypt� : valable que pour le format Turbo
' FLAG pour le format HTML
' MODE : 0 -> Utiliser un navigateur , 1 -> Fichier unique , 2-> Simples Pages
' SPOOLNAME : 0 -> Pad de nom de spool , 1 -> Inclure le nom du spool
' DIR : R�pertoire d'export des fichiers HTML
' Si cet argument est omis , l'argument lu sera celui du fichier Turbograph.ini, Section PARAM , Cl� ExportHTML
' RUN =  0 -> Non , 1 -> OUI

'Ligne commande Export HTML
'HTML|file=D:\travailv51\test.cot$mode=0$spoolname=1run=0

'Ligne Commande Envoie Internet format Turbo
'INTERNET|File=D:\travailv51\test.cot$to=t.cot@deal.fr$objet=Test messagerie$body=Voici le fichier deamnd�$format=0
'avec fichier joint pour le body
'INTERNET|File=D:\travailv51\test.cot$to=t.cot@deal.fr$objet=Test messagerie$bodyfile=d:\travailv51\body.txt$format=0

'Ligne Commande Envoie Internet format HTML
'INTERNET|File=D:\travailv51\test.cot$to=t.cot@deal.fr$objet=Test messagerie$body=Voici le fichier deamnd�$format=1
'avec fichier joint pour le body
'INTERNET|File=D:\travailv51\test.cot$to=t.cot@deal.fr$objet=Test messagerie$bodyfile=d:\travailv51\body.txt$format=1

'Ligne Commande Envoie Outlook format Turbo
'OUTLOOK|File=D:\travailv51\test.cot$to=thierry cot$objet=Test messagerie$body=Voici le fichier demand�$format=0
'avec fichier joint pour le body
'OUTLOOK|File=D:\travailv51\test.cot$to=thierry cot$objet=Test messagerie$bodyfile=d:\travailv51\body.txt$format=0

'Ligne Commande Envoie Outlook format HTML
'OUTLOOK|File=D:\travailv51\test.cot$to=thierry cot$objet=Test messagerie$body=Voici le fichier demand�$format=1
'avec fichier joint pour le body
'OUTLOOK|File=D:\travailv51\test.cot$to=thierry cot$objet=Test messagerie$bodyfile=d:\travailv51\body.txt$format=1


'-> Activer la gestion des erreurs
On Error GoTo InterceptError

IsMouchard = False
If IsMouchard Then

    Mouchard = GetTempFileNameVB("DBG")
    hdlMouchard = FreeFile
    Open Mouchard For Output As #hdlMouchard
    MsgBox Mouchard
End If

'-> Lancer l'initialisation
Call Initialisation

'-> Si on est en version internet
If VersionTurbo = 1 Then
    '-> Charger les ressources en locale
    LoadInternetMessProg
Else
    '-> V�rifier que l'on trouve le fichier langue
    If Dir$(FichierLangue & "\TurboPrint1-" & Format(IndexLangue, "00") & ".lng", vbNormal) = "" Then
        '-> On ne trouve pas le fichier : Charger la ressource en interne
        LoadInternetMessProg
    Else
        '-> Charger le fichier messprog
        RessourcePath = FichierLangue & "\"
        If Not (CreateMessProg("TurboPrint1-" & Format(IndexLangue, "00") & ".lng", True)) Then GoTo InterceptError
    End If
End If 'Selon la version


'********************************************************************************
'* ANALYSE DE LA LIGNE DE COMMANDE ET GESTION DES DIFFERENTS MODES D'IMPRESSION *
'********************************************************************************

'-> R�cup�ration de la ligne de commande
LigneCommande = Command()
LigneCommande = LTrim(LigneCommande)

'-> Supprimer les Quotters s'il y en a
If InStr(1, LigneCommande, """") = 1 Then _
    LigneCommande = Mid$(LigneCommande, 2, Len(LigneCommande) - 1)
If InStr(1, LigneCommande, """") <> 0 Then _
LigneCommande = Mid$(LigneCommande, 1, Len(LigneCommande) - 1)


'-> Initialisation des param�tres par d�faut
Mode = 1 'Mode ECRAN
nCopies = 1 'Nombre de copies


If Trim(LigneCommande) <> "" Then

    '-> Recherche d'un "|"
    If InStr(1, LigneCommande, "|") <> 0 Then
    
        '-> R�cup�ration du nom de fichier pass� en param�tre
        NomFichier = Entry(2, LigneCommande, "|")
        
        '-> R�cup�ration des param�tres d'impression
        Param1 = Entry(1, LigneCommande, "|")
        
        '-> Recup du parametre recto verso
        If Trim(UCase$(Entry(3, LigneCommande, "|"))) = "RV" Then RectoVerso = True
        
        '-> Tester si on a pass� le code op�rateur sur la ligne
        If InStr(1, Param1, "$") <> 0 Then
            '-> Si entry 1 = "ECRAN, on a pass� le code langue
            If UCase$(Trim(Entry(1, Param1, "$"))) = "ECRAN" Or UCase$(Trim(Entry(1, Param1, "$"))) = "GETPRINTERS" Then
                '-> R�cup�rer le code langue sp�cifi�
                MyCodeLangue = Trim(Entry(2, Param1, "$"))
                If IsNumeric(MyCodeLangue) Then
                    '-> V�rifier si on trouve le fichier langue associ�
                    RessourcePath = FichierLangue & "\"
                    Do While Libelles.Count <> 0
                        Libelles.Remove (1)
                    Loop
                    '-> V�rifier que l'on trouve le fichier langue
                    If Dir$(FichierLangue & "\TurboPrint1-" & Format(MyCodeLangue, "00") & ".lng", vbNormal) = "" Then
                        '-> On ne trouve pas le fichier : Charger la ressource en interne
                        LoadInternetMessProg
                    Else
                        '-> Charger le fichier messprog
                        RessourcePath = FichierLangue & "\"
                        If Not (CreateMessProg("TurboPrint1-" & Format(MyCodeLangue, "00") & ".lng", True)) Then GoTo InterceptError
                    End If
                    '-> Poser les bonnes valeurs
                    If UCase$(Trim(Entry(1, Param1, "$"))) = "GETPRINTERS" Then
                        Param1 = "GETPRINTERS"
                        IndexLangue = CInt(MyCodeLangue)
                    End If
                End If '-> Si on a pass� un code langue correcte
            End If 'Si on est en mode �cran
        End If
                
        '-> Cas de l'internet et du HTML
        If UCase$(Param1) = "INTERNET" Then
            Mode = 4
            GoTo Suite
        ElseIf UCase$(Param1) = "OUTLOOK" Then
            Mode = 5
            GoTo Suite
        ElseIf UCase$(Param1) = "HTML" Then
            Mode = 6
            GoTo Suite
        ElseIf UCase$(Param1) = "GETPRINTERS" Then
            GetPrinterList NomFichier
        ElseIf UCase$(Entry(1, Param1, "~")) = "ACROBAT PDFWRITER" Or UCase$(Entry(1, Param1, "~")) = "ACROBAT DISTILLER" Or UCase$(Entry(1, Param1, "~")) = "WIN2PDF" Then
            '-> Indiquer que l'on est en mode PDF
            IsPDF = True
            '-> Tester si on a sp�cifi� le nom du fichier PDF
            If InStr(1, NomFichier, "*") = 0 Then
                '-> Composer le nom du fichier PDF
                PdfDestinationFile = NomFichier & ".pdf"
                '-> On ne supprime pas
                KillSpool = False
            Else
                '-> R�cup�rer le fichier Destination
                PdfDestinationFile = Entry(2, NomFichier, "*")
                '-> Si on doit supprimer ou non
                If Trim(Entry(3, NomFichier, "*")) = "1" Then
                    KillSpool = True
                Else
                    KillSpool = False
                End If
                '-> Recomposer le nom du fichier
                NomFichier = Entry(1, NomFichier, "*")
            End If 'Si on a specifi� le nom du fichier PDF
            '-> Modifier le Registre si necessaire
            If Trim(PdfDestinationFile) <> "" Then
                If UCase$(Entry(1, Param1, "~")) = "WIN2PDF" Then
                    SetWin2PDFWriterFileName PdfDestinationFile
                Else
                    SetPdfWriterFileName PdfDestinationFile
                End If
            End If
        End If
                                 
        '-> V�rifier que le fichier existe
        If Trim(Dir$(NomFichier, vbNormal)) <> "" Then
                        
            '-> Traiter la ligne de commande pour les imprimantes
            If UCase$(Param1) <> "ECRAN" Then
                '-> Tester si on a trouv� un ~
                If InStr(1, Param1, "~") <> 0 Then
                    DeviceName = Entry(1, Param1, "~") 'Nom de l'imprimante
                    ModeName = UCase$(Entry(2, Param1, "~")) 'Mode d'impression BATCH/DIRECT
                    If IsMouchard Then Print #hdlMouchard, "Param�trage avec des ~"
                    If NumEntries(Param1, "~") = 3 Then
                        '-> Le nombre de copie est sp�cifi�
                        Tempo = Entry(3, Param1, "~")
                        If IsNumeric(Tempo) Then nCopies = CInt(Tempo)
                    End If
                Else
                    DeviceName = Entry(1, Param1, "$") 'Nom de l'imprimante
                    ModeName = UCase$(Entry(2, Param1, "$")) 'Mode d'impression BATCH/DIRECT
                    If IsMouchard Then Print #hdlMouchard, "Param�trage avec des $"
                    If NumEntries(Param1, "$") = 3 Then
                        '-> Le nombre de copie est sp�cifi�
                        Tempo = Entry(3, Param1, "$")
                        If IsNumeric(Tempo) Then nCopies = CInt(Tempo)
                    End If
                End If
                
                'Tester si on demande d'imprimer sur l'imprimante par d�faut
                If UCase$(Trim(DeviceName)) = "DEFAULT" Then DeviceName = Printer.DeviceName
                
                If IsMouchard Then
                    Print #hdlMouchard, "Recherche de l'imprimante : " & DeviceName
                    Print #hdlMouchard, "Mode d'impression : " & ModeName
                End If
                
                '-> Modifier le printer pour le mettre en ad�quation avec la demande
                FindPrinter = False
                
                For Each TempPrinter In Printers
                    If IsMouchard Then
                        Print #hdlMouchard, "On analyse l'imprimante : " & TempPrinter.DeviceName
                        Print #hdlMouchard, "On cherche l'imprimante : " & DeviceName
                    End If
                    If UCase$(TempPrinter.DeviceName) = UCase$(DeviceName) Then
                        FindPrinter = True
                        Set Printer = TempPrinter
                        If IsMouchard Then Print #hdlMouchard, "Imprimante trouv�e : " & Printer.DeviceName
                        Exit For
                    End If
                Next 'Pour toutes les imprimantes
                
                '-> V�rifier si imprimante Ok
                If FindPrinter Then
                    '-> On a trouver le printer sp�cifi� : s�lectionner un mode d'impression
                    Select Case UCase$(ModeName)
                        Case "BATCH"
                            Mode = 3
                        Case Else 'Mode DIRECT ou inconnu
                            Mode = 2
                    End Select
                End If 'Si on a trouv� un printer
                
                '-> Tester si le nombre de copies est renseign�
                If NumEntries(Param1, "$") = 3 Then
                    '-> Le nombre de copie est sp�cifi�
                    Tempo = Entry(3, Param1, "$")
                    If IsNumeric(Tempo) Then nCopies = CInt(Tempo)
                    
                End If 'Si le nombre de copie est sp�cifi�
            End If 'Si on passe un mode <> �cran
        End If 'Si on trouve le fichier pass� en param�tre
    Else
        '-> Pas de "|" donc le paramettre est sens� �tre un fichier : v�rifier s'il existe
        If Dir$(LigneCommande, vbNormal) <> "" Then
            NomFichier = LigneCommande
        Else
            NomFichier = ""
        End If
    End If 'S'il y a un "|"
End If 'Si la ligne de commande est vide
        
'****************************************************************
'* ANALYSE DU FICHIER ASCII POUR CREATION DES DIFFERENTS OBJETS *
'****************************************************************

Suite:

'-> Dans tous les cas, charger la feuille bibliotheque : elle servira � stocker les diff�rents objets
Load frmLib

'******************************
'* TRAITEMENT DE L'IMPRESSION *
'******************************

If IsMouchard Then
    Print #hdlMouchard, "Mode d'impression : " & Mode
    Close #hdlMouchard
End If

'Prise en charge des fichiers ZIP
NomFichier = GetUnzipFileName(NomFichier)



Select Case Mode

    Case 1 '-> Impression ECRAN
    
        '-> Afficher dans un premier temps l'interface de chargement
        MDIMain.Show
        '-> Charger le fichier dans l'interface
        If NomFichier <> "" Then
            '-> Bloquer la feuille MDI
            MDIMain.Enabled = False
            '-> Afficher le fichier
            DisplayFileGUI NomFichier
            '-> Debloquer la feuille MDI
            MDIMain.Enabled = True
        End If
            
    Case 2, 3  '-> Impression en mode direct ,  Impression en batch
    
        '-> Setting du p�riph�rique de sortie
        Set Sortie = Printer
        
        '-> Passer le printer en mode pixel
        Printer.ScaleMode = 3
                
        '-> Lancer l'analyse du spool
        Call AnalyseFileToPrint(NomFichier)
        
        '-> Pointer sur l'objet fichier
        Set aFichier = Fichiers(UCase$(NomFichier))
        
        '-> Pour le nombre de copies sp�cifi�
        For j = 1 To nCopies
            '-> Lancer l'impression des pages
            For Each aSpool In aFichier.Spools
                '-> Initialisation du mode de l'imprimante
                If aSpool.Maquette.Orientation = 1 Then
                    Printer.Orientation = 1
                Else
                    Printer.Orientation = 2
                End If
                
                '-> PIERROT DUPLEX selon que portrait ou paysage
                Printer.Duplex = 1
                If RectoVerso Then
                    If Printer.Orientation = 1 Then
                        Printer.Duplex = 2
                    Else
                        Printer.Duplex = 3
                    End If
                End If
                
                '-> Initialiser le printer
                Printer.Print ""
                
                '-> cause bug microsoft on definit bien le fond transparent
                SetBkMode Sortie.hdc, 1
                
                '-> Initialiser le compteur de page imprim� dans le spool
                SpoolPrint = 0
                            
                '-> Tester s'il ya des erreurs
                If aSpool.NbError = 0 Then
                    '-> Tester si on doit imprimer la page des s�lections
                    If aSpool.IsSelectionPage Then
                        CountPage = 0
                    Else
                        CountPage = 1
                    End If
                    For i = CountPage To aSpool.NbPage
                        '-> Tester s'il y a des erreurs sur la page sp�cifi�e
                        If aSpool.GetErrorPage(i) <> "" Then
                            '-> R�cup�rer la liste des erreurs
                            Tempo = aSpool.GetErrorPage(i)
                            '-> Imprrmer la liste des erreurs
                            For k = 1 To NumEntries(Tempo, Chr(0))
                                Printer.Print Entry(i, Tempo, Chr(0))
                            Next '-> Pour toutes les erreurs
                        Else
                            '-> Tester la taille du lot de page
                            If SpoolSize <> 0 Then
                                If SpoolPrint = SpoolSize Then
                                    '-> Terminer le document en cours
                                    Printer.EndDoc
                                    '-> Initialisation du mode de l'imprimante
                                    If aSpool.Maquette.Orientation = 1 Then
                                        Printer.Orientation = 1
                                    Else
                                        Printer.Orientation = 2
                                    End If
                                    
                                    '-> Initialiser le printer
                                    Printer.Print ""
                                    '-> Initialiser le compteur de page imprim� dans le spool
                                    SpoolPrint = 0
                                End If 'Si on atteint le nombre limitte de page
                            End If 'Si param�trage du spooler par lot
                            '-> Imprimer la page
                            PrintPageSpool aSpool, i
                            '-> Incr�menter le compteur de page imprim�
                            SpoolPrint = SpoolPrint + 1
                        End If
                        '-> Envoyer un saut de page si pas derni�re page
                        If i <> aSpool.NbPage Then Printer.NewPage
                    Next 'Pour toutes les pages
                Else
                    '-> Imprrmer la liste des erreurs
                    For k = 1 To aSpool.NbError
                        Printer.Print aSpool.GetErrorMaq(k)
                    Next '-> Pour toutes les erreurs
                End If
                '-> Fin du document pour ce spool
                Printer.EndDoc
            Next
        Next 'Pour toutes les copies
        
        '-> Supprimer les fichiers ini Temporaires
        Call EndProgTurbo
        
        '-> Si on est en mode PDF et que l'on doit supprimer la source
        If IsPDF And KillSpool Then Kill NomFichier
        
        '-> Fin du programme
        End
        
    Case 4, 5, 6 'INTERNET, OUTLOOK , HTML
    
        '-> Initialiser le p�riph�rique de sortie
        Set Sortie = frmLib.PicObj(0)
    
        '-> Analyse des param�tres pass�s en ligne de commande
        For i = 1 To NumEntries(NomFichier, "$")
            KeyValue = UCase$(Trim(Entry(1, Entry(i, NomFichier, "$"), "=")))
            pValue = Trim(Entry(2, Entry(i, NomFichier, "$"), "="))
            
            Select Case KeyValue
                Case "FILE"
                    pFile = pValue
                Case "TO"
                    pTo = pValue
                Case "COPIES"
                    pCopies = pValue
                Case "OBJET"
                    pObject = pValue
                Case "BODY"
                    pBody = pValue
                Case "BODYFILE"
                    pBodyFile = pValue
                Case "FORMAT"
                    pFormat = pValue
                Case "MODE"
                    pMode = pValue
                Case "SPOOLNAME"
                    pSpoolName = pValue
                Case "DIR"
                    pDir = pValue
                Case "RUN"
                    pRun = pValue
                Case "CRYPT"
                    pRun = pValue
            End Select
        Next
    
        '-> Dans tous les cas, v�rifier si le fichier pass� en argument existe
        If Dir$(pFile, vbNormal) = "" Then End
        
        '-> Setting du nom de fichier
        FileToExport = GetFileName(pFile)
        FileToExport = GetSpoolName(FileToExport) & ".html"
        
        '-> Setting de la cl� d'acc�s au fichier format HTML
        FileKeyExport = UCase$(Trim(pFile))

        '-> Setting de la cl� d'acces au fichier export Messagerie
        FileKeyMail = FileKeyExport

        '-> En mode  Internet, Outlook
        If Mode = 4 Or Mode = 5 Then
            '-> v�rifier si destinataire saisi
            If Trim(pTo) = "" Then End
            '-> V�rifier le format par d�faut
            If Not IsNumeric(pFormat) Then
                '-> Turbo par d�faut
                pFormat = "0"
            Else
                '-> Convertir
                If CLng(pFormat) <> 1 Then
                    pFormat = "0" 'Turbo par def
                End If
            End If
        End If
        
        '-> Tester si crypter ou non
        If Not IsNumeric(pCrypt) Then
            IsCryptedFile = False
        Else
            If CLng(pCrypt) <> "1" Then
                IsCryptedFile = False
            Else
                IsCryptedFile = True
            End If
        End If
        
        '-> Pour format HTML : format par d�faut Fichier Unique
        If pFormat = "1" Then
            If Not IsNumeric(pMode) Then
                '-> Fichier unique
                pMode = "1"
            Else
                If CLng(pMode) < 0 Or CLng(pMode) > 2 Then
                    pMode = "1"
                End If
            End If
        Else
            pFormat = "0"
        End If
        
        '-> type de navigation
        If pMode <> "" Then NavigationType = CInt(pMode)
        
        '-> Export du fichier dans sa totalit�
        ExportType = 2
        ExportMail = 2
            
        '-> Inclure le nom du spool
        If pSpoolName = "" Then
            pSpoolName = "0"
        Else
            If Not IsNumeric(pSpoolName) Then
                pSpoolName = "0"
            Else
                If CLng(pSpoolName) <> 0 And CLng(pSpoolName) <> 1 Then pSpoolName = "0"
            End If
        End If
        
        '-> Inclusion des noms des spools
        IncluseSpoolNumber = CBool(pSpoolName)
    
        '-> Run after
        If IsNumeric(pRun) Then
            If CLng(pRun) = 1 Then
                RunAfterExport = True
            Else
                RunAfterExport = False
            End If
        Else
            RunAfterExport = False
        End If
            
        '-> R�pertoire d'export si export Format HTML
        If Param1 = "HTML" Then
            If pDir = "" Then
                If PathToExport = "" Then
                    End
                Else
                    If GetAttr(PathToExport) And vbDirectory <> vbDirectory Then End
                End If
            Else
                If GetAttr(pDir) And vbDirectory <> vbDirectory Then
                    End
                Else
                    PathToExport = pDir
                End If
            End If
        
            '-> Traitement du "\" du fin de path
            If Right$(PathToExport, 1) <> "\" Then PathToExport = PathToExport & "\"
        End If
    
        '-> Pas de temporisation �cran
        IsTempo = False
        
        '-> Setting de l'origine export
        OrigineExport = 3
        OrigineMail = OrigineExport
        
        '-> Cr�ation du mod�le objet en m�moire
        AnalyseFileToPrint pFile
        
        '-> Lancement du traitement
        If Mode = 6 Then
            '-> Mode Export HTML
            CreateEditionHTML
        Else
            If Mode = 4 Then 'Internet
                '-> Setting de la variable d'export
                NetParam = pTo & Chr(0) & pObject & Chr(0) & pBody
                If pFormat = "0" Then 'Format Turbo
                    SendToInternet FileKeyExport, pBodyFile
                Else 'Format HTML
                    CreateHTMLToMessagerie False, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile
                End If
            Else 'Outlook
                '-> Envoyer le mail
                SendOutLook = True
                If pFormat = "0" Then 'Format Turbo
                    SendToOutLook FileKeyExport, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile
                Else 'Format HTML
                    CreateHTMLToMessagerie True, pTo & Chr(0) & pCopies & Chr(0) & pObject & Chr(0) & pBody & Chr(0) & pBodyFile
                End If
            End If
        End If
            
        '-> End fin du traitement
        End
        
End Select

Exit Sub


InterceptError:


    
    If Err.Number <> 482 Then MsgBox "Erreur dans la procedure MAIN" & Chr(13) & Err.Number & " - " & Err.Description, vbCritical + vbOKOnly, "Deal Informatique"
        
    '-> Appeler la gestion des erreurs
    End

End Sub



Private Sub SetPdfWriterFileName(FileName As String)

'---> Cette proc�dure initialise la cl� qui va bien dans le registre
'Acrobat PdfWriter~DIRECT~1|D:\Travailv51\2001-07-06-16-55-22.turbo

'-> Setting se sa valeur
SaveString HKEY_CURRENT_USER, "SoftWare\Adobe\Acrobat PDFWriter", "PDFFileName", FileName

End Sub

'*** PIERROT : modif pour gerer le soft : WIN2PDF
Private Sub SetWin2PDFWriterFileName(FileName As String)

'---> Cette proc�dure initialise la cl� qui va bien dans le registre
'Acrobat PdfWriter~DIRECT~1|D:\Travailv51\2001-07-06-16-55-22.turbo

'-> Setting se sa valeur
SaveString HKEY_CURRENT_USER, "SoftWare\Dane Prairie Systems\Win2PDF", "PDFFileName", FileName

End Sub
'*** PIERROT : modif pour gerer le soft : WIN2PDF

Private Sub GestError(ByVal ErrorCode As Integer, Optional strError As String, Optional MySpool As Spool)

'---> Cette proc�dure  effectue la gestion centralis�e des erreurs

Dim aLb As Libelle
Dim aFichier As Fichier
Dim aSpool As Spool
Dim Tempo As String
Dim aNode As Node

'-> Pointer sur le libelle Gestion des Erreurs
Set aLb = Libelles("ERRORS")
    
'-> Si on est en mode Visu : Cr�er l'icone du fichier dans le treeview et celle de l'erreur
If Mode = 1 Then
    Select Case ErrorCode 'Les erreurs 1 et 5 ne sont pas fatales -> Trait�es dans la proc�dure d'origine
        Case 2 '2-> Impossible d'ouvrir le fichier
            '-> Cr�er un nouveau Fichier
            Set aFichier = New Fichier
            aFichier.FileName = strError
            Fichiers.Add aFichier, UCase$(Trim(strError))
            '-> Cr�er un nouveau Spool
            Set aSpool = aFichier.AddSpool
            aSpool.FileName = strError
            '-> Ajouter une erreur dans le spool
            aSpool.SetErrorMaq Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", strError)
        Case 4, 6
            '-> Erreur lors de la lecture d'une ligne du fichier
            '-> Pointer sur l'objet fichier correspondant au fichier
            Set aFichier = Fichiers(UCase(Trim(strError)))
            
            '-> S'il n'y a pas de spools : il faut en cr�er un
            If aFichier.Spools.Count = 0 Then
                Set aSpool = aFichier.AddSpool
                aSpool.FileName = strError
            End If
            
            '-> Ajouter l'erreur dans le spool
            aSpool.SetErrorMaq Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", strError)
        Case 7, 8, 9, 10
            '-> Gestion des erreurs dans la proc�dure MAQLECT
            '-> Pointer sur le fichier
            Set aFichier = Fichiers(UCase$(Trim(Entry(1, strError, "|"))))
            '-> Pointer sur le spool : pr�ciser en entry 3
            Set aSpool = MySpool
            '-> Faire le setting d'une erreur
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", aSpool.FileName)
            Tempo = Replace(Tempo, "$MAQ$", aSpool.MaquetteASCII)
            Tempo = Replace(Tempo, "$INI$", Entry(3, strError, "|"))
            aSpool.SetErrorMaq Tempo
        Case 11, 12
            '-> Gestion des erreurs dans la proc�dure INITCOM
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", strError)
            Tempo = Replace(Tempo, "$TABLEAU$", strError)
            Tempo = Replace(Tempo, "$MAQ$", MySpool.MaquetteASCII)
            MySpool.SetErrorMaq Tempo
        Case 14, 15, 16
            '-> Gestion des erreurs dans la proc�dure INITCOM
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$TABLEAU$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$MAQ$", MySpool.MaquetteASCII)
            MySpool.SetErrorMaq Tempo
        Case 17
            '-> Gestion des erreurs dans la proc�dure INITFILETEMPORTF
            MySpool.SetErrorMaq aLb.GetCaption(ErrorCode)
        Case 18
            '-> Gestion des erreurs dans la proc�dure INITMAQENTETE
            MySpool.SetErrorMaq aLb.GetCaption(ErrorCode) & " " & strError
        Case 19
            '-> Gestion des erreurs dans la proc�dure CREATEBMPOBJ
            MySpool.SetErrorMaq aLb.GetCaption(ErrorCode)
        Case 20, 21, 22
            '-> Gestion des erreurs dans la proc�dure CREATEBMPOBJ
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
            If NumEntries(strError, "|") > 3 Then
                Tempo = Replace(Tempo, "$HAUTEUR$", Entry(3, strError, "|"))
                Tempo = Replace(Tempo, "$LARGEUR$", Entry(4, strError, "|"))
            Else
                Tempo = Replace(Tempo, "$FICHIER$", Entry(3, strError, "|"))
            End If
            MySpool.SetErrorMaq Tempo
        Case 23
            '-> Gestion des erreurs dans la procedure CREATECADREOBJ
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 24
            '-> Gestion des erreurs dans la proc�dure CreateRTFCadre
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", Entry(3, strError, "|"))
            Tempo = Replace(Tempo, "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 25
            '-> Gestion des erreurs dans la proc�dure CreateRTFSection
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$FICHIER$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$SECTION$", Entry(1, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 26
            '-> Gestion des erreurs dans la proc�dure CreateSectionObj
            MySpool.SetErrorMaq Replace(aLb.GetCaption(ErrorCode), "$SECTION$", strError)
        Case 27
            '-> Gestion des erreurs dans la proc�dure InitSectionEntete
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$LIG$", Entry(2, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 28, 29
            '-> Gestion des erreurs dans la proc�dure InitBmpEntete et InitCadreEntete
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$CADRE$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$LIG$", Entry(3, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 30
            '-> Gestion des erreurs dans la procedure InitTableauEntete
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$LIG$", Entry(2, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 31
            '-> Gestion des erreurs dans la procedure InitBlockEntete
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
            Tempo = Replace(Tempo, "$LIG$", Entry(3, strError, "|"))
            MySpool.SetErrorMaq Tempo
         Case 32, 33, 34
            '-> Gestion des erreurs dans la proc�dure InitCreateBlock et InitBlock (34)
            Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
            Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
            If ErrorCode = 33 Then Tempo = Replace(Tempo, "$LIG$", Entry(3, strError, "|"))
            MySpool.SetErrorMaq Tempo
        Case 35, 36, 37, 38, 39, 40, 41
            '-> Gestion des erreurs dans la proc�dure d'affichage d'une page : MODE VISU
            Set aNode = MDIMain.TreeNaviga.Nodes(UCase$(Trim(MySpool.FileName)) & "�" & UCase$(Trim(MySpool.Key)) & "�PAGE|" & MySpool.CurrentPage)
            aNode.Image = "Warning"
            aNode.Tag = "ERROR"
                        
            '-> Faire le setting de l'erreur
            Select Case ErrorCode
                Case 35, 36
                    MySpool.SetErrorPage MySpool.CurrentPage, Replace(aLb.GetCaption(ErrorCode), "$SECTION$", strError)
                Case 37
                    MySpool.SetErrorPage MySpool.CurrentPage, Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", strError)
                Case 38
                    MySpool.SetErrorPage MySpool.CurrentPage, Replace(aLb.GetCaption(ErrorCode), "$OBJECT$", strError)
                Case 39
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$SECTION$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BMP$", Entry(2, strError, "|"))
                    MySpool.SetErrorPage MySpool.CurrentPage, Tempo
                Case 40
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
                    MySpool.SetErrorPage MySpool.CurrentPage, Tempo
                Case 41
                    Tempo = Replace(aLb.GetCaption(ErrorCode), "$TABLEAU$", Entry(1, strError, "|"))
                    Tempo = Replace(Tempo, "$BLOCK$", Entry(2, strError, "|"))
                    Tempo = Replace(Tempo, "$CELLULE$", Entry(3, strError, "|"))
                    MySpool.SetErrorPage MySpool.CurrentPage, Tempo
            End Select
            
            '-> Afficher la page
            MySpool.DisplayInterfaceByPage (MySpool.CurrentPage)
    End Select
Else
    '-> On est en mode impression : initialiser une page : imprimer une erreur
    
End If








End Sub


Public Sub AnalyseFileToPrint(ByVal NomFichier As String)

'---> Cette proc�dure charge le ficher � analyser, cr�er la collection des spools et les maquettes

Dim hdlFile As Integer ' handle d'ouverture de fichier
Dim Ligne As String 'lecture d'une ligne du fichier ascii
Dim aSpool As Spool 'pointeur vers un objet spool
Dim NomMaquette As String 'nom de la maquette � ouvrir pour ancienne version
Dim aFichier As Fichier 'Pointeur vers un objet fichier
Dim LectureMaquette As Boolean 'Indique que l'on est en train de lire la d�finition de la maquette
Dim LectureSelection As Boolean 'Indique que l'on est en train de lire une page de s�lection
Dim LectureRTF As Boolean 'indique que l'on est en train de lire la d�finition RTF d'une maquette
Dim hdlRtf As Integer 'handle du fichier RTF dans lequel on �crit

Dim IsSpool As Boolean 'cette variable indique que l'on est en cours de cr�ation d'un spool
Dim Page As Integer 'Compteur de page par spool
Dim FindMaqChar As Boolean 'indique de plus charger les lignes de la maquettes car on analyse la maquette caract�re
Dim Res As Long
Dim hdlFichier As Long
Dim aOf As OFSTRUCT
Dim ErrorCode As Integer
Dim i As Integer

Dim IsAccDet As Boolean  '-> Indique que l'on est dans le fichier acc�s d�tail

On Error GoTo GestError

ErrorCode = 1
'-> R�cup�rer la taille du fichier que l'on charge
hdlFichier = OpenFile(NomFichier, aOf, OF_READ)
If hdlFichier <> -1 Then
    Res = GetFileSize(hdlFichier, 0)
    TailleTotale = Res
    Res = CloseHandle(hdlFichier)
End If

'-> Initialisation de la taille lue
TailleLue = 0

ErrorCode = 2
'-> Obtenir un handle de fichier et ouvrir le spool � imprimer
hdlFile = FreeFile
Open NomFichier For Input As #hdlFile

'-> Cr�er un nouvel objet Fichier et l'ajouter dans la collection
Set aFichier = New Fichier
aFichier.FileName = NomFichier

Fichiers.Add aFichier, Trim(UCase$(NomFichier))

'-> Boucle d'analyse du fichier
Do While Not EOF(hdlFile)
    ErrorCode = 4
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> Decrypter la source
    Ligne = DeCrypt(Ligne)
    '-> Analyse des diff�rents cas que l'on peut rencontrer
    If InStr(1, Ligne, "%%GUI%%") <> 0 Then
        '-> R�cup�rer le nom de la maquette
        NomMaquette = Trim(Mid$(Ligne, 8, Len(Ligne) - 7))
        '-> Cr�er un nouvel objet Spool
        Set aSpool = aFichier.AddSpool
        '-> Indiquer le num�ro du spool
        aSpool.Num_Spool = aFichier.Spools.Count
        aSpool.FileName = aFichier.FileName
        aSpool.MaquetteASCII = NomMaquette
        aSpool.AddPage
        '-> Ancienne version : charger la maquette depuis son adresse
        aFichier.IsOldVersion = True
        MaqLect aSpool, NomMaquette
        '-> Indiquer que l'on est en cr�ation d'un spool
        IsSpool = True
        '-> Premi�re page
        Page = 1
        '-> Ajouter une nouvelle page dans le spool
        'aSpool.AddPage
        '-> RAZ des variables de positionnement
        LectureMaquette = False
        LectureSelection = False
    Else 'si on n'a pas trouv� %%GUI%%
        Select Case Trim(UCase$(Ligne))
            Case "[SPOOL]"
                '-> Cr�ation d'un nouveau spool
                Set aSpool = aFichier.AddSpool
                aSpool.AddPage
                '-> Indiquer le nom du fichier maitre
                aSpool.FileName = aFichier.FileName
                '-> Indiquer le num�ro du spool
                aSpool.Num_Spool = aFichier.Spools.Count
                '-> Indiquer que l'on est en cr�ation de spool
                IsSpool = True
                '-> Indiquer que l'on n'est plus dans l'acc�s au d�tail
                IsAccDet = False
                '-> On n'est pas encore en train de lire la maquette
                LectureMaquette = False
                LectureSelection = False
                FindMaqChar = False
                '-> Init du compteur de page
                Page = 1
                '-> Init de la premi�re page
                'aSpool.AddPage
            Case "[/SPOOL]"
                '-> Indiquer que 'on n'est plus dans un spool
                IsSpool = False
            Case "[MAQ]"
                LectureMaquette = True
                LectureSelection = False
            Case "[/MAQ]"
                LectureMaquette = False
            Case "[MAQCHAR]"
                FindMaqChar = True
            Case "[GARDEOPEN]"
                LectureSelection = True
                LectureMaquette = False
                '-> Indiquer  dans le spool en cours qu'il ya une page de s�lection
                aSpool.IsSelectionPage = True
            Case "[GARDECLOSE]"
                LectureSelection = False
            Case "[PAGE]"
                '-> Incr�menter le compteur de page
                Page = Page + 1
                '-> Ajouter une nouvelle page dans le spool
                aSpool.AddPage
            Case "[DETAIL]"
                '-> On passe en mode lecture sur le spool en cours
                IsAccDet = True
            Case "[/DETAIL]"
                '-> Fin du mode lecture de l'acc�s au d�tail
                IsAccDet = False
            Case Else
                '-> Ne traiter la ligne que si on est dans un spool
                If IsSpool Then
                    '-> Selon la nature du traitement
                    If IsAccDet Then
                        '-> Ne rien faire si on est en cours de lecture acces d�tail
                        If Trim(Ligne) <> "" Then aSpool.AddLigneDetail Ligne
                    ElseIf LectureMaquette Then
                        '-> Ajouter une ligne dans la d�finition de la maquette
                        If Not FindMaqChar Then aSpool.SetMaq Ligne
                    ElseIf LectureSelection Then
                        '-> Ajouter une ligne dans la page de s�lection
                        aSpool.SetPage 0, Ligne
                    Else
                        '-> ajouter une ligne de datas dans la page en cours
                        aSpool.SetPage Page, Ligne
                    End If 'Selon la nature de la ligne
                End If 'si on est dans un spool
        End Select 'Selon la ligne que l'on est en train de lire
    End If 'si on a trouv� %%GUI%%
    
    '-> Gestion de la temprisation
    If Mode = 1 Then
        TailleLue = TailleLue + Len(Ligne) + 2
        DrawWait
    End If
Loop 'Boucle d'analyse du fichier

ErrorCode = 5
'-> Fermer le fichier ascii
Close #hdlFile

'-> Si on n' a pas trouv� de spool, en cr�er un pour l'erreur
If aFichier.Spools.Count = 0 Then
    '-> D�router sur la gestion d'erreur
    ErrorCode = 6
    GoTo GestError
End If

'-> Supprimer la derni�re page si elle est � blanc
For Each aSpool In aFichier.Spools
    For i = aSpool.NbPage To 1 Step -1
        If Trim(aSpool.GetPage(i)) = "" Then
            aSpool.NbPage = aSpool.NbPage - 1
        Else
            '-> Ne supprimer qe les pages de fin qui sont blanches
            Exit For
        End If
    Next
Next

'-> Gestion de la temporisation
If Mode = 1 Then
    MDIMain.StatusBar1.Refresh
    '-> R�initialiser les variables de lecture
    TailleLue = 0
    TailleTotale = aFichier.Spools.Count
End If

'-> G�n�rer les maquettes tous les spools
For Each aSpool In aFichier.Spools
    If Mode = 1 Then
        TailleLue = TailleLue + 1
        DrawWait
    End If
    InitCom aSpool
Next

'-> Vider la toolbar
If Mode = 1 Then MDIMain.StatusBar1.Refresh

Exit Sub

GestError:
        
        
    '-> Traitement de l'erreur
    Select Case ErrorCode
        Case 1
            '-> Pb lors de la r�cup�ration de la taille du fichier : ne rien faire
            Resume Next
        Case 2, 3, 4, 6
            '-> Erreur fatale
            Call GestError(ErrorCode, NomFichier)
        Case 5
            '-> Erreur lors de la fermture du fichier ASCII : Fermer le fichier
            Reset
            '-> Rendre la main
    End Select
                                

End Sub

Private Function OpenRTFSpool(ByVal RtfName As String) As Integer

'---> Ouverture d'un spool pour stocker le contenu RTF de la section/cadre

Dim RtfFile As String
Dim hdlRtf As Integer

'-> R�cup�ration du r�pertoire temporaire de windows
RtfFile = GetTempFileNameVB("", True)
hdlRtf = FreeFile
Open RtfFile & "\" & RtfName For Output As #hdlRtf

'-> Renvoyer son handle
OpenRTFSpool = hdlRtf

End Function




Private Sub MaqLect(ByRef aSpool As Spool, ByVal aMaq As String)

'---> Cette proc�dure charge le fichier ASCII d'une maquette dans un objet spool : compatibilite ancienne version

Dim hdlFile As Integer
Dim TmpFile As String
Dim Ligne As String
Dim ErrorCode As Integer
Dim TmpError As String

On Error GoTo GestError

'-> Setting du nom de la maquette dans l'objet spool
aSpool.MaquetteASCII = aMaq

'-> V�rifier que le fichier Maquette est bien pr�sent
ErrorCode = 7
TmpError = aSpool.FileName & "|" & aMaq
If Dir$(aMaq, vbNormal) = "" Then
    '-> Setting d'une erreur
    Call GestError(7, TmpError, aSpool)
    '-> Sortir de la proc�dure
    Exit Sub
End If

'-> Cr�er un fichier Tempo et faire une copie de la maquette
ErrorCode = 8
TmpFile = GetTempFileNameVB("MAQ")
CopyFile aMaq, TmpFile, False
    

ErrorCode = 9
'-> Ouvrir le fichier Tempo
hdlFile = FreeFile
Open TmpFile For Input As #hdlFile

'-> Transf�rer la maquette dans l'objet Spool
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    ErrorCode = 10
    Line Input #hdlFile, Ligne
    If Trim(Ligne) = "" Then GoTo NextLigne
    '-> Decrypter la maquette
    Ligne = DeCrypt(Ligne)
    '-> Quitter si on arrive � la d�finition de la maquette caract�re
    If UCase$(Trim(Ligne)) = "[MAQCHAR]" Then Exit Do
    '-> Transfert de la ligne
    aSpool.SetMaq (Ligne)
NextLigne:
Loop

ErrorCode = 11
'-> Fermer le fichier ASCII
Close #hdlFile

ErrorCode = 12
'-> Supprimer le fichier Tempo
Kill TmpFile

'-> Indiquer que la maquette est OK
aSpool.Ok = True

Exit Sub

GestError:

    '-> Traiter l'erreur selon le cas
    Select Case ErrorCode
        Case 7
            Call GestError(ErrorCode, TmpError, aSpool)
                                    
        Case 11, 12  '-> Erreurs r�cup�rables
            '-> Fermer tous les fichiers ouverts
            Reset
            '-> Continuer
            Resume Next
        Case Else
            '-> Appeler la gestion des erreurs
            Call GestError(ErrorCode, aSpool.FileName & "|" & aMaq & "|" & TmpFile, aSpool)
    End Select

End Sub

Private Sub InitCom(aSpool As Spool)

'---> Cette proc�dure initialise pour un fichier donn� la maquette associ�e

Dim Ligne As String '-> Pour lecture d'une ligne de maquette
Dim i As Long '-> Compteur de ligne
Dim SwitchLect As Integer
'-> Valeur de SwitchLect :
    '0  -> Nothing
    '1  -> Entete Maquette
    '2  -> Section PROGICIEL
    '3  -> Section de Texte : Propri�t�s
    '4  -> D�finition BMP
    '5  -> Definition d'un Cadre
    '6  -> Definition RTF d'un cadre
    '7  -> D�finition RTF d'une section
    '8  -> D�fintion d'un tableau
    '9  -> D�fintion d'un block de tableau
    '10 -> D�fintion d'une cellule de tableau
    '11 -> Ecran de s�lection

Dim LibError As String '-> Libelle si erreur
Dim TempoStr As String '-> Variable de stockage tempo

'-> D�finition du model COM
Dim aSection As Section
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim aTb As Tableau
Dim aBlock As Block
Dim aCell As Cellule
Dim ErrorCode As Integer
Dim TmpError As String

On Error GoTo GestError

'-> Type de ligne par d�faut
SwitchLect = 0

'-> Initialisation de la maquette
Set aSpool.Maquette = New Maquette

'-> Analyse de la maquette propre au Spool
For i = 1 To NumEntries(aSpool.GetMaq, Chr(0))
    '-> R�cup�ration de la d�finition de la ligne
    Ligne = Entry(i, aSpool.GetMaq, Chr(0))
    '-> Tester le premier caract�re de la ligne
    If Left$(Ligne, 1) = "[" Then
        '-> Analyse du type d'objet
        If UCase$(Mid$(Ligne, 2, 2)) = "NV" Then
            '-> Si on attaque le descriptif OBJECT ou la definition TRF
            If UCase$(Entry(3, Ligne, "-")) = "STD]" Then
                '-> Cr�ation du nouvel objet Section de texte
                Set aSection = New Section
                aSection.Nom = Entry(2, Ligne, "-")
                '-> Ajout dans la collection
                ErrorCode = 11
                TmpError = UCase$(Trim(aSection.Nom))
                aSpool.Maquette.Sections.Add aSection, UCase$(Trim(aSection.Nom))
                '-> Ligne de d�finition d'une section de texte
                SwitchLect = 3
            ElseIf UCase$(Entry(3, Ligne, "-")) = "RTF]" Then
                '-> Initialiser pour la section en cours un fichier et l'ouvrir
                TempoStr = InitFileTempoRtf(aSpool)
                aSection.TempoRTF = CInt(Entry(1, TempoStr, "|"))
                aSection.TempoRtfString = Entry(2, TempoStr, "|")
                '-> Ligne de d�finition RTF de la section
                SwitchLect = 7
            End If
        ElseIf UCase$(Mid$(Ligne, 2, 2)) = "TB" Then
            '-> Cr�ation du nouvel objet tableau
            Set aTb = New Tableau
            aTb.Nom = Mid$(Entry(2, Ligne, "-"), 1, Len(Entry(2, Ligne, "-")) - 1)
            aTb.IdAffichage = aSpool.Maquette.AddOrdreAffichage("TB-" & UCase$(Trim(aTb.Nom)))
            '-> Ajout dans la collection
            ErrorCode = 12
            TmpError = UCase$(Trim(aTb.Nom))
            
            aSpool.Maquette.Tableaux.Add aTb, UCase$(Trim(aTb.Nom))
            '-> Positionner le pointeur de lecture
            SwitchLect = 8
        Else
            If UCase$(Trim(Ligne)) = "[PROGICIEL]" Then
                '-> analyse de la cle PROGICIEL de la maquette
                SwitchLect = 2
            End If
        End If '-> Si [
    Else
    'ElseIf InStr(1, Ligne, "\") <> 0 Then
        Select Case UCase$(Trim(Ligne))
            Case "\DEFENTETE�BEGIN"
                '-> D�finition de l'entete de la maquette
                SwitchLect = 1
            Case "\DEFENTETE�END"
                '-> Envoyer dans la proc�dure d'ajout adaptation des dimensions
                InitMaqEntete aSpool.Maquette, Ligne, aSpool
                '-> Fin de la d�inition de l'entete de la maquette
                SwitchLect = 0
            Case "\BMP�END"
                '-> Lancer la cr�ation physique de l'objet BMP
                CreateBmpObj aBmp, aSpool
                '-> Fin de d�fintion d'un objet BMP : repositionner sur pointeur de section
                SwitchLect = 3
            Case "\CADRE�END"
                '-> Lancer la cr�ation physique du cadre
                CreateCadreObj aCadre, aSpool
                '-> Fin de d�finition d'un objet cadre : repositionner sur pointeur de section
                SwitchLect = 3
            Case "\CADRERTF�BEGIN"
                '-> Initialiser le fichier RTF pour lecture
                TempoStr = InitFileTempoRtf(aSpool)
                aCadre.TempoRTF = CInt(Entry(1, TempoStr, "|"))
                aCadre.TempoRtfString = Entry(2, TempoStr, "|")
                '-> D�but de d�finition RTF d'un cadre
                SwitchLect = 6
            Case "\CADRERTF�END"
                '-> Fermer le fichier tempo RTF affect�
                ErrorCode = 13
                Close #aCadre.TempoRTF
                '-> Lancer la g�n�ration physique du contenu RTF du cadre
                CreateRTFCadre aCadre, aSpool
                '-> Fin de d�fintion RTF d'un cadre : repositionner sur pointeur de section
                SwitchLect = 3
            Case "\RTF�END"
                '-> Fermer le fichier tempo RTF affect�
                ErrorCode = 13
                Close #aSection.TempoRTF
                '-> Lancer la g�n�ration physique du contenu RTF de la section de texte
                CreateRTFSection aSection, aSpool
                '-> Fin de d�finition RTF d'une section
                SwitchLect = 0
            Case "\TABLEAU�END"
                '-> Fin de d�finition d'un tableau
                SwitchLect = 0
            Case "\RANG�END"
                '-> Fin de d�finition d'une section de texte
                SwitchLect = 0
            Case Else
                Select Case SwitchLect
                    Case 1 'Analyse d'une ligne d'entete
                        InitMaqEntete aSpool.Maquette, Ligne, aSpool
                    Case 2 'Analyse de la section PROGICIEL
                        If UCase$(Trim(Entry(1, Ligne, "="))) = "\PROG" Then
                            aSpool.Maquette.Progiciel = Entry(2, Ligne, "=")
                        ElseIf UCase$(Trim(Entry(1, Ligne, "="))) = "\CLI" Then
                            aSpool.Maquette.Client = Entry(2, Ligne, "=")
                        End If
                    Case 3 'On est dans une section de texte : analyse des tag <CADRE> et <BMP>  et <TEXTE>
                        Select Case UCase$(Trim(Entry(1, Ligne, "�")))
                            Case "\BMP"
                                '-> Cr�ation d'un nouvel objet BMP
                                Set aBmp = New ImageObj
                                aBmp.Nom = Entry(2, Ligne, "�")
                                aBmp.IdOrdreAffichage = aSection.AddOrdreAffichage("BMP-" & UCase$(Trim(aBmp.Nom)))
                                aBmp.SectionName = aSection.Nom
                                '-> Ajout dans la collection des Bmps
                                ErrorCode = 14
                                TmpError = aSection.Nom & "|" & UCase$(Trim(aBmp.Nom))
                                aSection.Bmps.Add aBmp, UCase$(Trim(aBmp.Nom))
                                '-> Positionnement du pointeur de lecture
                                SwitchLect = 4
                            Case "\CADRE"
                                '-> Cr�ation d'un nouvel objet cadre
                                Set aCadre = New Cadre
                                aCadre.Nom = Entry(2, Ligne, "�")
                                aCadre.IdAffichage = aSection.AddOrdreAffichage("CDR-" & UCase$(Trim(aCadre.Nom)))
                                aCadre.SectionName = aSection.Nom
                                ErrorCode = 15
                                TmpError = aSection.Nom & "|" & UCase$(Trim(aCadre.Nom))
                                aSection.Cadres.Add aCadre, UCase$(Trim(aCadre.Nom))
                                '-> Positionnement du pointeur de lecture
                                SwitchLect = 5
                            Case "\TEXTE"
                                '-> Fin de d�finition d'un objet Section: Cr�ation de la repr�sentation physique
                                CreateSectionObj aSection, aSpool
                            Case Else
                                '->Dans ce cas, on lit une propri�t� de la section: ajouter dans la d�fintion de la section
                                InitSectionEntete aSpool, aSection, Ligne
                        End Select 'Selon la nature de la ligne
                    Case 4
                        '-> Ligne de d�finition d'un BMP
                        InitBmpEntete aBmp, aSpool, Ligne
                    Case 5
                        '-> Ligne de d�finition d'un cadre
                        InitCadreEntete aCadre, aSpool, Ligne
                    Case 6
                        '-> Imprimer la ligne RTF du cadre dans son fichier TEMPO
                        Print #aCadre.TempoRTF, Ligne
                    Case 7
                        '-> Imprimer la ligne RTF de la section texte dans son fichier tempo
                        Print #aSection.TempoRTF, Ligne
                    Case 8
                        If UCase$(Trim(Entry(1, Ligne, "�"))) = "\BEGIN" Then
                            '-> Cr�ation d'un nouvel objet Block
                            Set aBlock = New Block
                            aBlock.Nom = Entry(2, Ligne, "�")
                            aBlock.NomTb = Trim(UCase$(aTb.Nom))
                            aBlock.AlignementLeft = 3
                            aBlock.Left = 0
                            aBlock.IdOrdreAffichage = aTb.AddOrdreAffichage("BL-" & UCase$(Trim(aBlock.Nom)))
                            '-> Ajouter le block dans la collection des tableaux
                            ErrorCode = 16
                            TmpError = aSpool.MaquetteASCII & "|" & Trim(UCase$(aTb.Nom)) & UCase$(Trim(aBlock.Nom))
                            aTb.Blocks.Add aBlock, "BL-" & UCase$(Trim(aBlock.Nom))
                            '-> Initialiser les matrices des lignes et colonnes
                            Erase sCol()
                            Erase sLigne()
                            LigLue = 1
                            '-> Postionnement du pointeur de lecture sur l'analyse des bloks
                            SwitchLect = 9
                        Else
                            '-> Ajouter � la d�finition du tableau en cours
                            InitTableauEntete aTb, aSpool, Ligne
                        End If
                    Case 9
                        If UCase$(Trim(Entry(1, Ligne, "�"))) = "\END" Then
                            '-> Fin de d�finition d'un objet block : lancer les initialisations et les cr�ations
                            InitCreateBlock aBlock, aTb, aSpool
                            '-> Se repositionner sur la lecture du tableau
                            SwitchLect = 8
                        Else
                            '-> Analyse d'un ligne d'un block
                            InitBlockEntete aBlock, aTb, aSpool, Ligne
                        End If
                End Select 'Selon la nature de la lecture de la ligne
        End Select
    End If '-> Si premier caract�re est un [
Next 'Pour toutes les lignes de la maquette


'-> G�n�ration ici de la section de texte pour la page des s�lections
'-> Cr�er nouvel objet RTF
Set aSection = New Section
aSection.Largeur = aSpool.Maquette.Largeur - aSpool.Maquette.MargeLeft * 2
aSection.Hauteur = aSpool.Maquette.Hauteur - aSpool.Maquette.MargeTop * 2
aSection.AlignementLeft = 2
aSection.AlignementTop = 2
aSection.BackColor = &HFFFFFF

'-> Charger un nouvel �diteur RTF dans la biblioth�que
Load frmLib.Rtf(IdRTf)
frmLib.Rtf(IdRTf).Font.Name = "Lucida Console"
aSection.IdRTf = IdRTf

'-> Incr�menter le compteur d'objet RTF
IdRTf = IdRTf + 1

'-> Ajouter dans la collection des secstion de la maquette
aSpool.Maquette.Sections.Add aSection, SelectRTFKey
                

Exit Sub

GestError:
    
    If ErrorCode = 13 Then
        Reset
        Resume Next
    Else
        Call GestError(ErrorCode, TmpError, aSpool)
    End If

End Sub
Private Function InitFileTempoRtf(ByRef aSpool As Spool) As String

'---> Cette fonction creer un fichier tempo pour lecture des RTF et renvoie :
' Handle|NomFichier

On Error GoTo GestError

Dim TempFile As String
Dim hdlFile As String

TempFile = GetTempFileNameVB("RTF")
hdlFile = FreeFile
Open TempFile For Output As #hdlFile

InitFileTempoRtf = hdlFile & "|" & TempFile

Exit Function

GestError:
    Call GestError(17, , aSpool)
    
End Function

Private Sub CreateRTFSection(ByRef aSection As Section, aSpool As Spool)

'---> Cette proc�dure charge le contenu du fichier TEmpoRTF adns l'objet affect� et le supprime
Dim ErrorCode As Integer

On Error GoTo GestError

'-> Charger le fichier RTF
frmLib.Rtf(aSection.IdRTf).LoadFile aSection.TempoRtfString
ErrorCode = -1
'-> Supprimer le fichier
Kill aSection.TempoRtfString

Exit Sub

GestError:
    
    If ErrorCode = -1 Then Resume Next
    Call GestError(25, aSection.Nom & "|" & aSection.TempoRtfString, aSpool)
    

End Sub

Private Sub CreateRTFCadre(ByRef aCadre As Cadre, aSpool As Spool)

'---> Cette proc�dure charge le contenu du fichier TEmpoRTF adns l'objet affect� et le supprime

On Error GoTo GestError

Dim ErrorCode As Integer

'-> Charger le fichier RTF
frmLib.Rtf(aCadre.IdRTf).LoadFile aCadre.TempoRtfString

'-> Supprimer le fichier
ErrorCode = -1
Kill aCadre.TempoRtfString

Exit Sub

GestError:

    If ErrorCode = -1 Then Resume Next
    Call GestError(24, aCadre.SectionName & "|" & aCadre.Nom & "|" & aCadre.TempoRtfString, aSpool)
    


End Sub


Private Sub InitMaqEntete(aMaq As Maquette, Ligne As String, aSpool As Spool)

'---> Cette proc�dure effectue le setting des propri�t�s de l'entete de la maquette

Dim DefLigne As String
Dim ValueLigne As String
Dim Tempo As Single
Dim ErrorCode As Integer

On Error GoTo GestError

'-> Tester que la ligne commence par un "\"
If InStr(1, Ligne, "\") = 0 Then Exit Sub

'-> R�cup�ration des valeurs
DefLigne = Entry(1, Ligne, "�")
ValueLigne = Entry(2, Ligne, "�")
'-> Traiter la valeur de la ligne
Select Case UCase$(DefLigne)
    Case "\NOM"
        aMaq.Nom = ValueLigne
    Case "\HAUTEUR"
        aMaq.Hauteur = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aMaq.Largeur = CSng(Convert(ValueLigne))
    Case "\MARGETOP"
        aMaq.MargeTop = CSng(Convert(ValueLigne))
    Case "\MARGELEFT"
        aMaq.MargeLeft = CSng(Convert(ValueLigne))
    Case "\ORIENTATION"
        aMaq.Orientation = CInt(ValueLigne)
    Case "\PAGEGARDE" 'Version 2.0
        If UCase$(ValueLigne) = "VRAI" Then
            aMaq.PageGarde = True
        Else
            aMaq.PageGarde = False
        End If
    Case "\PAGESELECTION" 'Version 2.0
        If UCase$(ValueLigne) = "VRAI" Then
            aMaq.PageSelection = True
        Else
            aMaq.PageSelection = False
        End If
    Case "\DEFENTETE"
        '-> Ajuster les dimensions
        If aMaq.Orientation <> 1 Then
            Tempo = aMaq.Largeur
            aMaq.Largeur = aMaq.Hauteur
            aMaq.Hauteur = Tempo
        End If
    Case "\NAVIGAHTML"
        '-> Setting du navigateur
        aMaq.NavigaHTML = Trim(ValueLigne)
        
End Select

Exit Sub

'-> Gestion des erreurs
GestError:

    Call GestError(18, Ligne, aSpool)
    

End Sub

Private Sub InitSectionEntete(ByRef aSpool As Spool, ByRef aSection As Section, Ligne As String)

'---> Cette proc�dure effectue le setting des propri�t�s d'une section de texte

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\CONTOUR"
        If CInt(ValueLigne) = 0 Then
            aSection.Contour = False
        Else
            aSection.Contour = True
        End If
    Case "\HAUTEUR"
        aSection.Hauteur = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aSection.Largeur = CSng(Convert(ValueLigne))
    Case "\ALIGNEMENTTOP"
        aSection.AlignementTop = CInt(ValueLigne)
    Case "\TOP"
        aSection.Top = CSng(Convert(ValueLigne))
    Case "\ALIGNEMENTLEFT"
        aSection.AlignementLeft = CInt(ValueLigne)
    Case "\LEFT"
        aSection.Left = CSng(Convert(ValueLigne))
    Case "\BACKCOLOR"
        aSection.BackColor = CLng(ValueLigne)
    Case "\TEXTE"
End Select

Exit Sub

GestError:
    
    Call GestError(27, aSection.Nom & "|" & Ligne, aSpool)

End Sub

Private Sub InitBmpEntete(aBmp As ImageObj, aSpool As Spool, Ligne As String)

'---> Cette proc�dure effectue le setting des propri�t�s d'un objet BMP

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\LEFT"
        aBmp.Left = CSng(Convert(ValueLigne))
    Case "\TOP"
        aBmp.Top = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aBmp.Largeur = CSng(Convert(ValueLigne))
    Case "\HAUTEUR"
        aBmp.Hauteur = CSng(Convert(ValueLigne))
    Case "\FICHIER"
        aBmp.Fichier = ValueLigne
    Case "\CONTOURBMP"
        If CInt(ValueLigne) = 1 Then
            aBmp.Contour = True
        Else
            aBmp.Contour = False
        End If
    Case "\VARIABLE"
        If CInt(ValueLigne) = 1 Then
            aBmp.isVariable = True
        Else
            aBmp.isVariable = False
        End If
    Case "\USEASSOCIATION"
        If CInt(ValueLigne) = 1 Then
            aBmp.UseAssociation = True
        Else
            aBmp.UseAssociation = False
        End If
    Case "\PATH"
        aBmp.Path = ValueLigne
End Select

Exit Sub

GestError:
    
    Call GestError(28, aBmp.SectionName & "|" & aBmp.Nom & "|" & Ligne, aSpool)
    

End Sub

Private Sub InitCadreEntete(ByRef aCadre As Cadre, ByRef aSpool As Spool, Ligne As String)

'---> Cette proc�dure effectue le setting des propri�t�s d'un cadre

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\LEFT"
        aCadre.Left = CSng(Convert(ValueLigne))
    Case "\TOP"
        aCadre.Top = CSng(Convert(ValueLigne))
    Case "\DISTANCE" 'Equivalent � MargeInterne
        aCadre.MargeInterne = CSng(Convert(ValueLigne))
    Case "\HAUTEUR"
        aCadre.Hauteur = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aCadre.Largeur = CSng(Convert(ValueLigne))
    Case "\LARGEURBORDURE"
            aCadre.LargeurTrait = CInt(ValueLigne)
    Case "\ROUNDRECT"
            aCadre.IsRoundRect = CInt(ValueLigne)
    Case "\CONTOUR"
        '-> Bordure haut
        If Entry(1, ValueLigne, ",") = "1" Then
            aCadre.Haut = True
        Else
            aCadre.Haut = False
        End If
        '-> Bordure bas
        If Entry(2, ValueLigne, ",") = "1" Then
            aCadre.Bas = True
        Else
            aCadre.Bas = False
        End If
        '-> Bordure gauche
        If Entry(3, ValueLigne, ",") = "1" Then
            aCadre.Gauche = True
        Else
            aCadre.Gauche = False
        End If
        '-> Bordure droite
        If Entry(4, ValueLigne, ",") = "1" Then
            aCadre.Droite = True
        Else
            aCadre.Droite = False
        End If
    Case "\COULEUR"
        aCadre.BackColor = CLng(ValueLigne)
End Select

Exit Sub

GestError:

    Call GestError(29, aCadre.SectionName & "|" & aCadre.Nom & "|" & Ligne, aSpool)
    

End Sub

Private Sub CreateSectionObj(ByRef aSection As Section, ByRef aSpool As Spool)

'---> Cette proc�dure cr�er les diff�rents objets pour impression de la section

On Error GoTo GestError

'-> Cr�ation d'un contenuer RTF
Load frmLib.Rtf(IdRTf)
aSection.IdRTf = IdRTf

'-> Appliquer les propri�t�s
frmLib.Rtf(IdRTf).BackColor = aSection.BackColor
If aSection.Contour Then
    frmLib.Rtf(IdRTf).BorderStyle = rtfFixedSingle
Else
    frmLib.Rtf(IdRTf).BorderStyle = rtfNoBorder
End If

'-> Incr�menter le compteur d'objets RTF
IdRTf = IdRTf + 1
Exit Sub

GestError:

    Call GestError(26, aSection.Nom, aSpool)

End Sub

Private Sub CreateCadreObj(ByRef aCadre As Cadre, ByRef aSpool As Spool)

'---> Cette proc�dure cr�er une instance d'un objet RTF pour impression du cadre

On Error GoTo GestError

'-> charger un nouvel objet du type RTF
Load frmLib.Rtf(IdRTf)
aCadre.IdRTf = IdRTf

'-> Setting des prori�t�s
frmLib.Rtf(IdRTf).BackColor = aCadre.BackColor
frmLib.Rtf(IdRTf).Visible = True

'-> Incr�menter le compteur d'objets
IdRTf = IdRTf + 1

Exit Sub

GestError:

    Call GestError(23, aCadre.SectionName & "|" & aCadre.Nom, aSpool)
    


End Sub

Private Sub CreateBmpObj(ByRef aBmp As ImageObj, ByRef aSpool As Spool)

'---> Cette proc�dure cr�er une instance d'un objet picturebox pour impression du BMP

Dim Res As Long
Dim lpBuffer As String
Dim TempoStr As String
Dim Version As String
Dim Lecteur As String
Dim i As Integer
Dim ErrorCode As Integer
Dim TmpError As String


On Error GoTo GestError

'-> Chargement de la repr�sentation physique
ErrorCode = 19
TmpError = ""
Load frmLib.PicObj(IdBmp)

'-> Contour du Bmp
If aBmp.Contour Then
    frmLib.PicObj(IdBmp).BorderStyle = 1
Else
    frmLib.PicObj(IdBmp).BorderStyle = 0
End If

'-> Donner ses dimensions au Bmp
ErrorCode = 20
TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Largeur & "|" & aBmp.Hauteur
frmLib.PicObj(IdBmp).Height = frmLib.ScaleX(aBmp.Hauteur, 7, 1)
frmLib.PicObj(IdBmp).Width = frmLib.ScaleY(aBmp.Largeur, 7, 1)


'-> Selon la version
If VersionTurbo = 0 Then 'On est en version Pathv51

    '-> V�rifier que l'on trouve le fichier Ini
    If TurboMaqIniFile = "" Then GoTo AfterLoading

    '-> Gestion des paths pour les adresses des images
    lpBuffer = Space$(1000)
    Select Case UCase$(aBmp.Path)
        Case "PARAM"
            Res = GetPrivateProfileString("PARAM", "PATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
            TempoStr = "Emilie"
        Case "APP"
            Res = GetPrivateProfileString(aSpool.Maquette.Progiciel, "PATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
            TempoStr = aSpool.Maquette.Progiciel
        Case "CLIENT"
            Res = GetPrivateProfileString(aSpool.Maquette.Client, "PATH", "", lpBuffer, Len(lpBuffer), TurboMaqIniFile)
            TempoStr = aSpool.Maquette.Client
    End Select 'Selon le path de l'image
    
    If Res = 0 Then
        lpBuffer = ""
    Else
        lpBuffer = Mid$(lpBuffer, 1, Res)
    End If

    ErrorCode = 21
    
    '-> R�cup�ration de la version
    i = GetEntryIndex(App.Path, "DEALPRO", "\")
    Version = Entry(i + 2, App.Path, "\")
    
    '-> R�cup�ration du nom de lectuer logique
    i = InStr(1, UCase$(App.Path), "DEALPRO")
    Lecteur = Mid$(App.Path, 1, i - 2)
    
    '-> Faire un remplacement des variables par leur adresse
    lpBuffer = Replace(UCase$(lpBuffer), "$LECTEUR$", Lecteur)
    lpBuffer = Replace(UCase$(lpBuffer), "$VERSION$", Version)
    lpBuffer = Replace(UCase$(lpBuffer), "$IDENT$", TempoStr)

    '-> Cr�ation de l'image
    If aBmp.isVariable Then
        aBmp.IsAutosize = True
    Else
        aBmp.IsAutosize = False
        If Trim(lpBuffer) <> "" Then
            '-> V�rifier si on trouve l'image ou qu'elle est rens�ign�e
            If lpBuffer <> "" Then
                If Dir$(lpBuffer & aBmp.Fichier) <> "" Then
                    ErrorCode = 22
                    TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Fichier
                    frmLib.PicObj(IdBmp).Picture = LoadPicture(lpBuffer & aBmp.Fichier)
                    aBmp.Fichier = lpBuffer & aBmp.Fichier
                Else
                    aBmp.Fichier = ""
                End If 'Si on trouve l'image
            Else
                aBmp.Fichier = ""
            End If
        Else
            aBmp.Fichier = ""
        End If 'Si le path de l'image a �t� trouv�
    End If
Else
    '-> On est en version professionnelle
    If aBmp.isVariable Then
        aBmp.IsAutosize = True
    Else
        aBmp.IsAutosize = False
        If Trim(aBmp.Fichier) <> "" Then
            '-> Rechercher l'image dans le sous r�pertoire Images
            lpBuffer = App.Path & "\Images\" & aBmp.Fichier
            '-> Tester que l'on trouve l'image
            If Dir$(lpBuffer, vbNormal) <> "" Then
                '-> Essayer de charger l'image
                ErrorCode = 22
                TmpError = aBmp.SectionName & "|" & aBmp.Nom & "|" & aBmp.Fichier
                frmLib.PicObj(IdBmp).Picture = LoadPicture(lpBuffer)
                aBmp.Fichier = lpBuffer
            Else
                '-> impossible de trouver le fichier associ�
                aBmp.Fichier = ""
            End If
        Else 'La propri�t� fichier est � blanc
            aBmp.Fichier = ""
        End If
    End If
End If 'Selon la version

AfterLoading:

'-> Positionner l'objet
frmLib.PicObj(IdBmp).Top = frmLib.PicObj(IdBmp).ScaleY(aBmp.Top, 7, 1)
frmLib.PicObj(IdBmp).Left = frmLib.PicObj(IdBmp).ScaleX(aBmp.Left, 7, 1)
frmLib.PicObj(IdBmp).AutoSize = aBmp.IsAutosize

'-> Enregistrer l'index du picture box associ�
aBmp.IdPic = IdBmp

'-> Rendre l'objet visible
frmLib.PicObj(IdBmp).Visible = True

'-> Incr�menter
IdBmp = IdBmp + 1


Exit Sub

GestError:

    Call GestError(ErrorCode, TmpError, aSpool)
    

End Sub

Private Sub InitTableauEntete(ByRef aTb As Tableau, ByRef aSpool As Spool, Ligne As String)

'---> Cette proc�dure effectue le settin g des prorpi�t�s d'un tableau

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\LARGEURTB"
        aTb.Largeur = CSng(Convert(ValueLigne))
    Case "\ORIENTATIONTB"
        aTb.Orientation = CInt(ValueLigne)
End Select


Exit Sub



GestError:
    Call GestError(30, aTb.Nom & "|" & Ligne, aSpool)
    


End Sub

Private Sub InitBlockEntete(ByRef aBlock As Block, aTb As Tableau, aSpool As Spool, Ligne As String)

'---> Cette proc�dure effectue le setting des propri�t�s d'un block

Dim DefLigne As String
Dim ValueLigne As String

On Error GoTo GestError

If InStr(1, Ligne, "\") = 0 Then Exit Sub

 '-> R�cup�rer les valeurs
DefLigne = UCase$(Entry(1, Ligne, "�"))
ValueLigne = Entry(2, Ligne, "�")

'-> Analyse
Select Case DefLigne
    Case "\ALIGNEMENT" 'Garder pour compatibilit� 1.00
        If CInt(ValueLigne) = 0 Then 'Marge gauche
            aBlock.AlignementLeft = 2
        ElseIf CInt(ValueLigne) = 1 Then 'Marge Droite
            aBlock.AlignementLeft = 4
        ElseIf CInt(ValueLigne) = 2 Then 'Centr�
            aBlock.AlignementLeft = 3
        ElseIf CInt(ValueLigne) = 3 Then 'Alignement Libre
            aBlock.AlignementLeft = 5 'La valeur Left est aliment�e par la valeur "\DISTANCE"
        End If
    Case "\ALIGNLEFT" 'Version 2.00 de "\Alignement"
        aBlock.AlignementLeft = CInt(ValueLigne)
    Case "\LEFT" 'Valeur de AlignLeft quand il est sp�cifi� : valeur 5
        aBlock.Left = CSng(Convert(ValueLigne))
    Case "\ALIGNTOP" 'Version  2.00 de "\AlignementBlockVertical"
        aBlock.AlignementTop = CInt(ValueLigne)
    Case "\TOP" 'Valeur de AlignTop quand il est sp�cifi� : valeur 5
        aBlock.Top = CSng(Convert(ValueLigne))
    Case "\LARGEUR"
        aBlock.Largeur = CSng(Convert(ValueLigne))
    Case "\HAUTEUR"
        aBlock.Hauteur = CSng(Convert(ValueLigne))
    Case "\LIGNE"
        aBlock.NbLigne = CInt(ValueLigne)
        ReDim sLigne(1 To CInt(ValueLigne))
    Case "\COLONNE"
        aBlock.NbCol = CInt(ValueLigne)
        ReDim sCol(1 To CInt(ValueLigne))
    Case "\EXPORTLIG"
        aBlock.ListeExcel = ValueLigne
    Case "\ACCESDET"
        aBlock.KeyAccesDet = Trim(ValueLigne)
    Case "\COL"
        sLigne(LigLue) = Ligne
        LigLue = LigLue + 1
End Select

Exit Sub

GestError:
    Call GestError(31, aTb.Nom & "|" & aBlock.Nom & "|" & Ligne, aSpool)

End Sub
Private Sub InitCreateBlock(ByRef aBlock As Block, ByRef aTb As Tableau, ByRef aSpool As Spool)

'---> Cette proc�dure cr�er les diff�rentes cellules � partir des matrices sCol et sLigne

Dim aCell As Cellule
Dim i As Integer
Dim j As Integer
Dim DefLigne As String
Dim DefCellule As String
Dim DefBordure As String
Dim ErrorCode As Integer
Dim AccesDetail As String
Dim ListeCell As String

On Error GoTo GestError

'-> Initialiser les colonnes et les lignes
aBlock.Init

'-> Gestion des exports des lignes vers Excel Attention , cette propri�t� _
ne contient que la liste des lignes non exportables vers EXCEL
If Trim(aBlock.ListeExcel) <> "" Then
    For i = 1 To NumEntries(aBlock.ListeExcel, "|")
        aBlock.SetExportExcel CInt(Entry(i, aBlock.ListeExcel, "|")), False
    Next
End If

'-> Alimenter les Matrices de ligne te de colonne et cr�ation des cellules
For i = 1 To LigLue - 1
    '-> R�cup�ration de la d�finition de la ligne
    ErrorCode = 32
    DefLigne = sLigne(i)
    '-> Setting de la hauteur de ligne
    aBlock.SetHauteurLig i, CSng(Convert(Entry(2, DefLigne, "�")))
    '-> Recup�rer QUE la liste des colonnes
    DefLigne = Entry(3, DefLigne, "�")
    For j = 1 To aBlock.NbCol
        ErrorCode = 33
        '-> Recup de la d�finition de la colonne
        DefCellule = Entry(j, DefLigne, "|")
        '-> Setting de la largeur
        aBlock.SetLargeurCol j, CSng(Convert(Entry(3, DefCellule, ";")))
        '-> Cr�ation de la cellule associ�e
        Set aCell = New Cellule
        aCell.Ligne = i
        aCell.Colonne = j
        '-> Ajout dans le tableau
        aBlock.Cellules.Add aCell, "L" & i & "C" & j
        '-> setting des propri�t�s
        aCell.CellAlign = CInt(Entry(1, DefCellule, ";"))
        aCell.Contenu = Entry(2, DefCellule, ";")
        '-> Initialisation des champs
        aCell.InitChamp
        '-> Gestion des bordures
        DefBordure = Entry(4, DefCellule, ";")
        If UCase$(Entry(1, DefBordure, ",")) = "VRAI" Then
            aCell.BordureHaut = True
        Else
            aCell.BordureHaut = False
        End If
        If UCase$(Entry(2, DefBordure, ",")) = "VRAI" Then
            aCell.BordureBas = True
        Else
            aCell.BordureBas = False
        End If
        If UCase$(Entry(3, DefBordure, ",")) = "VRAI" Then
            aCell.BordureGauche = True
        Else
            aCell.BordureGauche = False
        End If
        If UCase$(Entry(4, DefBordure, ",")) = "VRAI" Then
            aCell.BordureDroite = True
        Else
            aCell.BordureDroite = False
        End If
        '-> Propri�t�s de Font
        aCell.FontName = Entry(5, DefCellule, ";")
        aCell.FontSize = CSng(Convert(Entry(6, DefCellule, ";")))
        If UCase$(Trim(Entry(7, DefCellule, ";"))) = "VRAI" Then
            aCell.FontBold = True
        Else
            aCell.FontBold = False
        End If
        If UCase$(Trim(Entry(8, DefCellule, ";"))) = "VRAI" Then
            aCell.FontItalic = True
        Else
            aCell.FontItalic = False
        End If
        If UCase$(Trim(Entry(9, DefCellule, ";"))) = "VRAI" Then
            aCell.FontUnderline = True
        Else
            aCell.FontUnderline = False
        End If
        aCell.FontColor = CLng(Entry(10, DefCellule, ";"))
        If CLng(Entry(11, DefCellule, ";")) = 999 Then
            aCell.BackColor = 16777215
            aCell.FondTransparent = True
        Else
            aCell.BackColor = CLng(Entry(11, DefCellule, ";"))
            aCell.FondTransparent = False
        End If
        '-> AutoAjust
        If UCase$(Entry(12, DefCellule, ";")) = "VRAI" Then
            aCell.AutoAjust = True
        Else
            aCell.AutoAjust = False
        End If
        '-> Format et Type
        DefBordure = Entry(13, DefCellule, ";")
        If Entry(1, DefBordure, "@") <> "" Then aCell.TypeValeur = CInt(Entry(1, DefBordure, "@"))
        aCell.Msk = Entry(2, DefBordure, "@")
        If NumEntries(DefBordure, "@") = 3 Then aCell.PrintZero = True
        
        '-> Export vers Excel Fusion
        If NumEntries(DefCellule, ";") > 13 Then
            '-> R�cup�rer le param�trage
            DefBordure = Entry(14, DefCellule, ";")
            aCell.IsFusion = CBool(Entry(1, DefBordure, "@"))
            aCell.ColFusion = CInt(Entry(2, DefBordure, "@"))
        End If
        
        '-> Impl�mnter le mod�le objet associ� � excel
        aCell.TurboToExcel
        
    Next 'Pour toute les colonnes
Next 'Pour toutes les lignes

'-> Gestion de l'acc�s au d�tail
If aBlock.KeyAccesDet <> "" Then
    '-> Basculer
    AccesDetail = aBlock.KeyAccesDet
    '-> Setting des param�tres du block
    aBlock.UseAccesDet = True
    aBlock.KeyAccesDet = Entry(1, AccesDetail, "�")
    '-> Setting des cellules servant � l'acc�s au d�tail
    ListeCell = Entry(2, AccesDetail, "�")
    For i = 1 To NumEntries(ListeCell, "|")
        '-> Pointeur vers une cellule
        Set aCell = aBlock.Cellules("L" & Entry(1, Entry(i, ListeCell, "|"), "-") & "C" & Entry(2, Entry(i, ListeCell, "|"), "-"))
        '-> forcer le format
        aCell.FontColor = RGB(0, 0, 255)
        aCell.FontUnderline = True
        '-> Indiquer qu'elle sert d'acc�s au d�tail
        aCell.UseAccesDet = True
    Next 'Pour toutes les d�finitions de cellules
End If 'S'il y a l'acc�s au d�tail sur ce block

'-> Convertion des dimensions des cellules en pixels
InitBlock aBlock, aSpool


Exit Sub

GestError:
    If ErrorCode = 32 Then
        Call GestError(ErrorCode, aTb.Nom & "|" & aBlock.Nom, aSpool)
    Else
        Call GestError(ErrorCode, aTb.Nom & "|" & aBlock.Nom & "|" & DefCellule, aSpool)
    End If
    


End Sub

Public Sub EndProgTurbo()

'---> Cette proc�dure doit �tre appell�e oblogatoirement � la fin du programme

On Error Resume Next

'-> Suppression des fichiers TEMPO des paths
If TurboGraphIniFile <> "" Then Kill TurboGraphIniFile
If Tm_PictureIniFile <> "" Then Kill Tm_PictureIniFile
If TurboMaqIniFile <> "" Then Kill TurboMaqIniFile

End Sub

Public Sub InitBlock(ByVal BlockCours As Block, aSpool As Spool)

'---> Cette proc�dure initialise les matrices Lignes (), Colonnes () et Cells _
en partant des largeurs de colonnes et hauteur de lignes

Dim i As Integer
Dim j As Integer
Dim NbCol As Integer
Dim NbLig As Integer
'-> Dimensions en CM
Dim HauteurCm As Single
Dim LargeurCm As Single
'-> Convertion en Pixels pour dessin
Dim HauteurPix As Long
Dim LargeurPix As Long
Dim hPix As Long
Dim lPix As Long
'-> Matrices des lignes et colonnes
Dim Lignes() As Long
Dim Colonnes() As Long

Dim LargeurColPix As Long
Dim HauteurLigPix As Long

Dim aCell As Cellule
Dim PosX As Long
Dim PosY As Long

On Error GoTo ErrorOpen

'-> R�cup�ration des propri�t�s
NbCol = BlockCours.NbCol
NbLig = BlockCours.NbLigne
HauteurCm = BlockCours.Hauteur
LargeurCm = BlockCours.Largeur

'-> Convertion en pixels
If TypeOf Sortie Is PictureBox Then
    '-> Ecran
    LargeurPix = CLng(frmLib.ScaleX(LargeurCm, 7, 3))
    HauteurPix = CLng(frmLib.ScaleY(HauteurCm, 7, 3))
Else
    'Printer.Copies = 1
    LargeurPix = CLng(Printer.ScaleX(LargeurCm, 7, 3))
    HauteurPix = CLng(Printer.ScaleY(CDbl(HauteurCm), 7, 3))
End If

'-> Affecter la valeur au block
BlockCours.LargeurPix = LargeurPix
BlockCours.HauteurPix = HauteurPix

'-> Calcul des dimensions de base
ReDim Lignes(1 To NbLig)
ReDim Colonnes(1 To NbCol)

'-> Largeur de base des colonnes
For i = 1 To NbCol
    Colonnes(i) = Fix((BlockCours.GetLargeurCol(i) / LargeurCm) * ((LargeurPix - NbCol + 1)))
    lPix = lPix + Colonnes(i)
Next

'-> Hauteur de base des lignes
For i = 1 To NbLig
    Lignes(i) = Fix((BlockCours.GetHauteurLig(i) / HauteurCm) * (HauteurPix - (NbLig + 1)))
    hPix = hPix + Lignes(i)
Next

Dim aCumul As Double
Dim bCumul  As Long
Dim a As Double

'-> Lissage des colonnes
bCumul = 1

For i = 1 To NbCol
    '-> Largeur en pixel normale
    aCumul = aCumul + BlockCours.GetLargeurCol(i)
    LargeurColPix = CInt(Sortie.ScaleX(aCumul, 7, 3))
    bCumul = bCumul + Colonnes(i) + 1
    a = LargeurColPix - bCumul
    Colonnes(i) = Colonnes(i) + a
    bCumul = LargeurColPix
Next

'-> Lissage des lignes
aCumul = 0
bCumul = 1

For i = 1 To NbLig
    '-> Hauteur de pixel normale
    aCumul = aCumul + BlockCours.GetHauteurLig(i)
    HauteurLigPix = CInt(Sortie.ScaleY(aCumul, 7, 3))
    bCumul = bCumul + Lignes(i) + 1
    a = HauteurLigPix - bCumul
    Lignes(i) = Lignes(i) + a
    bCumul = HauteurLigPix
Next

'-> Pixel de d�part
PosY = 1
PosX = 1

For i = 1 To NbCol
    For j = 1 To NbLig
        '-> Mettre � jour les valeurs des cellules
        Set aCell = BlockCours.Cellules("L" & j & "C" & i)
        aCell.X1 = PosX
        aCell.Y1 = PosY
        aCell.X2 = PosX + Colonnes(i) + 1
        aCell.Y2 = PosY + Lignes(j) + 1
        '-> Incr�menter le compteur de position
        PosY = PosY + Lignes(j) + 1
    Next 'pour toutes les lignes
    '-> initialisation des valeurs
    PosX = PosX + Colonnes(i) + 1
    PosY = 1
Next 'Pour toutes les colonnes

'-> Lib�rer le pointeur de cellule
Set aCell = Nothing

'-> Sortir de la fonction
Exit Sub

ErrorOpen:

    Call GestError(34, BlockCours.NomTb & "|" & BlockCours.Nom, aSpool)

End Sub

Public Sub DisplayFileGUI(ByVal FileName As String)

'---> Cette proc�dure charge et affiche un fichier en particulier

Dim aNode As Node
Dim aNode2 As Node
Dim aNode3 As Node
Dim aFichier As Fichier
Dim aSpool As Spool
Dim i As Integer
Dim aLb As Libelle
Dim aFrm As frmDisplaySpool

On Error Resume Next

'-> Bloquer l'interface
MDIMain.Enabled = False
Screen.MousePointer = 11

'-> Positionner le p�riph�rique d'impression sur un objet de type PictureBox
Set Sortie = frmLib.PicObj(0)


'-> Lancer le chargement et l'analyse du fichier s�lectionn�
Call AnalyseFileToPrint(FileName)

'-> Cr�er son icone dans le treeview
Set aNode = MDIMain.TreeNaviga.Nodes.Add(, , UCase$(Trim(FileName)), FileName, "Close")
aNode.Tag = "FICHIER"
aNode.ExpandedImage = "Open"
aNode.Expanded = True

'-> Pointer sur la classe libell�
Set aLb = Libelles("MDIMAIN")

'-> Pointer sur le fichier
Set aFichier = Fichiers(UCase$(Trim(FileName)))

'-> Cr�er une icone par spool
For Each aSpool In aFichier.Spools
    Set aNode2 = MDIMain.TreeNaviga.Nodes.Add(aNode.Key, 4, aNode.Key & "�" & aSpool.Key, aSpool.SpoolText, "Spool")
    aNode2.Tag = "SPOOL"
    '-> Soit la liste des erreurs
    If aSpool.NbError = 0 Then
        '-> Afficher une page pour la liste des s�lections utilisateur
        If aSpool.IsSelectionPage Then
            Set aNode3 = MDIMain.TreeNaviga.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�PAGE|0", aLb.GetCaption(12) & "S�lection", "Page")
            aNode3.Tag = "PAGE"
        End If
        
        '-> Afficher une icone par page
        For i = 1 To aSpool.NbPage
            Set aNode3 = MDIMain.TreeNaviga.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�PAGE|" & i, aLb.GetCaption(12) & i, "Page")
            aNode3.Tag = "PAGE"
        Next
    Else
        '-> Afficher l'icone de la page d'erreur
        Set aNode3 = MDIMain.TreeNaviga.Nodes.Add(aNode2.Key, 4, aNode2.Key & "�ERROR", aLb.GetCaption(11), "Warning")
        aNode3.Tag = "ERROR"
    End If 'S'il y a des erreurs
Next 'Pour tous les spools

'-> Afficher le premier Spool dans l'interface
Set aSpool = aFichier.Spools(1)

'-> Cr�er une nouvelle instance
Set aFrm = New frmDisplaySpool
'-> Affect� le spool
Set aFrm.aSpool = aSpool
'-> Affecter la feuille au spool
Set aSpool.frmDisplay = aFrm
'-> Imprimer si necessaire
If aSpool.NbError <> 0 Then
    aSpool.DisplayInterfaceByPage (1)
Else
    '-> Imprimer la premi�re page
    PrintPageSpool aSpool, 1
    '-> Afficher la feuille en fonction du r�sultat de l'impression
    aSpool.DisplayInterfaceByPage (1)
End If

GestError:
    '-> Bloquer l'interface
    MDIMain.Enabled = True
    Screen.MousePointer = 0


End Sub


Public Sub PrintPageSpool(ByRef aSpool As Spool, ByVal PageToPrint As Integer)

'---> Cette proc�dure imprime une page d'un spool

Dim i As Integer, j As Integer
Dim PositionX As Long
Dim PositionY As Long
Dim IsPaysage As Boolean
Dim Ligne As String
Dim TypeObjet As String
Dim NomObjet As String
Dim TypeSousObjet As String
Dim NomSousObjet As String
Dim Param As String
Dim DataFields As String
Dim FirstObj As Boolean
Dim aNode As Node
Dim ErrorCode As Integer
Dim aSection As Section
Dim X As Control

On Error GoTo GestError

'-> Initialisation du p�riph�rique selon la nature de la sortie
If TypeOf Sortie Is PictureBox Then
    ErrorCode = 35
    '-> Vider les erreurs de page
    aSpool.InitErrorPage PageToPrint
    '-> Modifier la page courrante de l'objet Spool
    aSpool.CurrentPage = PageToPrint
    '-> Mode Visualisation �cran
    aSpool.InitDisplayPage
    '-> Positionner le pointeur de sortie vers le picturebox de la feuille
    Set Sortie = aSpool.frmDisplay.Page
    '-> Supprimer les labels d'acc�s au d�tail
    For Each X In aSpool.frmDisplay.Controls
        If UCase$(X.Name) = "LBLACCESDET" Then
            If X.Index <> 0 Then Unload aSpool.frmDisplay.lblAccesDet(X.Index)
        End If
    Next
    '-> Gestion de l'icone de la page s�lectionn�e
    If aSpool.IsSelectionPage Then
        j = 0
    Else
        j = 1
    End If
    For i = j To aSpool.NbPage
        '-> Tester si une page est en erreur ou non avant de modifier sa page
        If MDIMain.TreeNaviga.Nodes(UCase(Trim(aSpool.FileName)) & "�" & UCase$(Trim(aSpool.Key)) & "�PAGE|" & i).Tag <> "ERROR" Then
            If i = PageToPrint Then
                MDIMain.TreeNaviga.Nodes(UCase(Trim(aSpool.FileName)) & "�" & UCase$(Trim(aSpool.Key)) & "�PAGE|" & i).Image = "PageSelected"
            Else
                MDIMain.TreeNaviga.Nodes(UCase(Trim(aSpool.FileName)) & "�" & UCase$(Trim(aSpool.Key)) & "�PAGE|" & i).Image = "Page"
            End If
        End If
    Next
    '-> Modifier l'affichage de la page en cours
    aSpool.frmDisplay.DisplayCurrentPage
    '-> Bloquer la feuille
    aSpool.frmDisplay.Enabled = False
    '-> Gestion de la temporisation
    TailleLue = 0
    TailleTotale = NumEntries(aSpool.GetPage(PageToPrint), Chr(0))
    '-> Bloquer l'interface
    MDIMain.Enabled = False
    Screen.MousePointer = 11
Else
    '-> Mode Impression direct ou batch sur une imprimante
End If

'-> Initialiser le RTF d'impression de la page des s�lections
If PageToPrint = 0 Then
    '-> Pointer sur la section texte
    Set aSection = aSpool.Maquette.Sections(SelectRTFKey)
    '-> Vider le RTF associ�
    frmLib.Rtf(aSection.IdRTf).Text = ""
End If

'-> R�cup�ration des marges internes du contexte de p�riph�rique
MargeX = GetDeviceCaps(Sortie.hdc, PHYSICALOFFSETX)
MargeY = GetDeviceCaps(Sortie.hdc, PHYSICALOFFSETY)

'-> Initialiser la position X , y du pointeur sur les marges du document
PositionY = -MargeY + Sortie.ScaleY(aSpool.Maquette.MargeTop, 7, 3)
PositionX = -MargeX + Sortie.ScaleX(aSpool.Maquette.MargeLeft, 7, 3)

'-> Indiquer l'�tat du premier objet que l'on trouve
FirstObj = True

'-> Lecture des lignes de la page
For i = 1 To NumEntries(aSpool.GetPage(PageToPrint), Chr(0))
    '-> Gestion de la temporisation
    If Mode = 1 Then
        TailleLue = i
        DrawWait
    End If
    '-> R�cup�ration de la ligne en cours
    Ligne = Trim(Entry(i, aSpool.GetPage(PageToPrint), Chr(0)))
    '-> Ne rien imprimer si page � blanc
    If Trim(Ligne) = "" Then GoTo NextLigne
    
    If PageToPrint = 0 Then
        frmLib.Rtf(aSection.IdRTf).Text = frmLib.Rtf(aSection.IdRTf).Text & Chr(13) & Chr(10) & Ligne
        frmLib.Rtf(aSection.IdRTf).SelStart = 0
        frmLib.Rtf(aSection.IdRTf).SelLength = Len(frmLib.Rtf(aSection.IdRTf).Text)
        frmLib.Rtf(aSection.IdRTf).SelFontName = "Lucida Console"
    Else
        If InStr(1, Ligne, "[") = 1 Then
            '-> r�cup�ration des param�tres
            AnalyseObj Ligne, TypeObjet, NomObjet, TypeSousObjet, NomSousObjet, Param, DataFields
            '-> Selon le type d'objet
            If UCase$(TypeObjet) = "ST" Then
                If Not PrintSection(NomObjet, Param, DataFields, FirstObj, PositionX, PositionY, aSpool) Then Exit For
                FirstObj = False
            ElseIf UCase$(TypeObjet) = "TB" Then
                If Not PrintTableau(NomObjet, Param, DataFields, PositionX, PositionY, NomSousObjet, FirstObj, aSpool) Then Exit For
                FirstObj = False
            End If
        End If 'Si premier caract�re = "["
    End If 'Si on imprime la page de s�lection
NextLigne:
Next 'Pour toutes les lignes de la page

'-> Tester si le bas de page est d�pass�
If Sortie.ScaleY(PositionY, 3, 7) > aSpool.Maquette.Hauteur Then
    Sortie.PaintPicture frmLib.pibPage.Picture, Sortie.ScaleX(0.5, 7, 3), Sortie.ScaleY(aSpool.Maquette.Hauteur, 7, 3) - Sortie.ScaleY(frmLib.pibPage.Height, 1, 3) - Sortie.ScaleY(1, 7, 3)
End If 'Si on a d�pass� le bas de page

'-> Imprimer la page de s�lection
If PageToPrint = 0 Then
    '-> imprimer la section
    PrintObjRtf 0, 0, aSection, "", 0, 0, aSpool
End If

'-> R�afficher la page si on est en mode Visu
If TypeOf Sortie Is PictureBox Then
    '-> D�bloquer la feuille
    aSpool.frmDisplay.Enabled = True
End If

'-> Gestion de la temporisation
If Mode = 1 Then
    MDIMain.StatusBar1.Refresh
    '-> Bloquer l'interface
    MDIMain.Enabled = True
    Screen.MousePointer = 0
End If

Exit Sub
    
       
GestError:
    MsgBox Err.Number & " " & Err.Description
    '-> Gestion de la temporisation
    If Mode = 1 Then
        MDIMain.StatusBar1.Refresh
        '-> Bloquer l'interface
        MDIMain.Enabled = True
        Screen.MousePointer = 0
    End If

    '-> Setting d'une erreur
    Call GestError(ErrorCode, CStr(PageToPrint), aSpool)
    
    

End Sub


Private Function PrintBlock(ByRef NomSousObjet As String, ByRef Param As String, _
                        ByRef DataFields As String, ByVal PosX As Long, _
                        ByRef PosY As Long, ByRef aTb As Tableau, ByVal Ligne As Integer, ByRef aSpool As Spool) As Long

'---> Cette Proc�dure imprime un block de ligne

Dim aBlock As Block
Dim Champ As String
Dim aField() As String
Dim i As Long
Dim aCell As Cellule
Dim HauteurLigPix As Long
Dim ListeField() As String
Dim Tempo

On Error GoTo GestBlock

'-> Pointer sur le block de tableau � �diter
Set aBlock = aTb.Blocks("BL-" & UCase$(NomSousObjet))

'-> Initaliser le block
InitBlock aBlock, aSpool

'-> Calculer la modification des positions X et Y des blocks de ligne. _
Les coordonn�es de chaque cellule �tant calcul�e sur un X et Y = 0 de base

Select Case aBlock.AlignementLeft
    Case 2 '-> Marge gauche
        PosX = Sortie.ScaleX(aSpool.Maquette.MargeLeft, 7, 3)
    Case 3 '-> Centr�
        PosX = Sortie.ScaleX((aSpool.Maquette.Largeur - aBlock.Largeur) / 2, 7, 3)
    Case 4 '-> Marge droite
        PosX = Sortie.ScaleX(aSpool.Maquette.Largeur - aBlock.Largeur - aSpool.Maquette.MargeLeft, 7, 3)
    Case 5 '-> Sp�cifi�
        PosX = Sortie.ScaleX(aBlock.Left, 7, 3)
End Select

'-> Tenir compte de la marge interne
PosX = PosX - MargeX

If aBlock.AlignementTop = 5 And Ligne <> 1 Then
Else
    Select Case aBlock.AlignementTop
        Case 1 '-> Libre
            '-> RAS le pointeur est bien positionn�
        Case 2 '-> Marge haut
            PosY = Sortie.ScaleY(aSpool.Maquette.MargeTop, 7, 3)
        Case 3 '-> Centr�
            PosY = Sortie.ScaleY((aSpool.Maquette.Hauteur - aBlock.Hauteur) / 2, 7, 3)
        Case 4 '-> Marge Bas
            PosY = Sortie.ScaleY(aSpool.Maquette.Hauteur - aBlock.Hauteur, 7, 3)
        Case 5 '-> Sp�cifi�
            PosY = Sortie.ScaleY(aBlock.Top, 7, 3)
    End Select

    '-> Modifier l'alignement
    If aBlock.AlignementTop <> 1 Then PosY = PosY - MargeY

End If

'-> Impression du block de ligne
For i = 1 To aBlock.NbCol
    '-> Pointer sur la cellule � dessiner
    Set aCell = aBlock.Cellules("L" & Ligne & "C" & i)
    '-> Remplacer les champs par leur valeur
    aCell.ReplaceField DataFields
    '-> Imprimer la cellule
    If Not DrawCell(aCell, PosX, PosY, aSpool, aTb.Nom, NomSousObjet) Then GoTo GestBlock
    '-> R�cup�rer la hauteur de la ligne
    HauteurLigPix = aCell.Y2 - aCell.Y1
Next


'-> forcer le rafraichissement
If TypeOf Sortie Is PictureBox Then Sortie.Refresh

If Ligne = aBlock.NbLigne Then PosY = PosY + 1
PrintBlock = PosY + HauteurLigPix

Exit Function

GestBlock:
    
    Call GestError(40, aTb.Nom & "|" & NomSousObjet, aSpool)
    PrintBlock = -9999
    
    

End Function


Private Function IsGoodFont(ByVal MyFont As String) As String

'---> Cette fonction d�termine si une font est valide
On Error GoTo GestError

frmLib.FontName = MyFont
IsGoodFont = MyFont

Exit Function

GestError:

    IsGoodFont = "Arial"
    

End Function


Public Function DrawCell(ByRef aCell As Cellule, ByVal PosX As Long, ByVal PosY As Long, ByRef aSpool As Spool, ByRef NomTb As String, Optional NomBlock As String) As Boolean

'---> Fonction qui dessine une cellule

Dim hdlPen As Long
Dim hdlBrush As Long
Dim hdlBordure As Long
Dim hdlFont As Long
Dim OldPen As Long
Dim oldBrush As Long
Dim oldFont As Long
Dim aPoint As POINTAPI
Dim aRect As RECT
Dim Res As Long
Dim aFt As FormatedCell
Dim Xtemp
Dim PrintSigne As Boolean
Dim Couleur As Long
Dim aLbl As Label
Dim aTb As Tableau
Dim aBlock As Block


On Error GoTo GestErr

Dim X1 As Long, X2 As Long, Y1 As Long, Y2 As Long

X1 = aCell.X1 + PosX
X2 = aCell.X2 + PosX
Y1 = PosY
Y2 = aCell.Y2 + PosY - aCell.Y1

'-> Cr�ation des objets GDI pour dessin des cellules
hdlBrush = CreateSolidBrush(aCell.BackColor)
hdlPen = CreatePen(PS_NULL, 1, QBColor(15))
hdlBordure = CreatePen(PS_SOLID, 1, &HE0E0E0)
'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(Sortie.hdc, hdlBrush)

'-> Dessin de la cellule
OldPen = SelectObject(Sortie.hdc, hdlPen)
If Not aCell.FondTransparent Then Res = Rectangle(Sortie.hdc, X1, Y1, X2, Y2)

'-> Setting du Rect de la cellule
aRect.Left = X1
aRect.Top = Y1
aRect.Right = X2 - 1
aRect.Bottom = Y2 - 1


Dim pDrawText As Long
Dim Rect2 As RECT
Dim DifL As Long
Dim DifH As Long
Dim IsCellAjust As Boolean
Dim hdlRgn As Long
Dim AlignBase(1 To 2, 3 To 6) As Long

'-> Appliquer les options de font
Sortie.FontName = IsGoodFont(aCell.FontName)
Sortie.FontSize = aCell.FontSize
Sortie.FontBold = aCell.FontBold
Sortie.FontItalic = aCell.FontItalic
Sortie.FontUnderline = aCell.FontUnderline
Sortie.ForeColor = aCell.FontColor

'***********************************
'* Dessin du contenu de la cellule *
'***********************************
    
'-> R�cup�rer l'option de d'alignement interne
IsCellAjust = aCell.AutoAjust
    
'-> Faire une copie de Rect
Rect2.Left = aRect.Left
Rect2.Right = aRect.Right
Rect2.Top = aRect.Top
Rect2.Bottom = aRect.Bottom
    
'-> Dans un premier temps, calculer la taille necessaire pour afficher si ajustement automatique
If IsCellAjust Then _
    DrawText Sortie.hdc, aCell.ContenuCell, Len(aCell.ContenuCell), Rect2, DT_CALCRECT Or DT_WORDBREAK
   
'-> R�cup�ration des diff�rences de largeur et de hauteur
DifL = (aRect.Right - aRect.Left) - (Rect2.Right - Rect2.Left)
DifH = (aRect.Bottom - aRect.Top) - (Rect2.Bottom - Rect2.Top)
    
'-> Calcul des alignements de base
AlignBase(1, 3) = Rect2.Left + DifL / 2
AlignBase(2, 3) = Rect2.Right + DifL / 2

AlignBase(1, 4) = Rect2.Left + DifL
AlignBase(2, 4) = Rect2.Right + DifL

AlignBase(1, 5) = Rect2.Top + DifH / 2
AlignBase(2, 5) = Rect2.Bottom + DifH / 2

AlignBase(1, 6) = Rect2.Top + DifH
AlignBase(2, 6) = Rect2.Bottom + DifH
        
'-> Alignement interne dans le rectangle
Select Case aCell.CellAlign
    
    Case 1
        If IsCellAjust Then
            pDrawText = DT_LEFT
            '-> Pas necessaire de modifier les alignements
        Else
            pDrawText = DT_LEFT Or DT_TOP
        End If
        
    Case 2
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_CENTER Or DT_TOP
        End If
    
    Case 3
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_RIGHT Or DT_TOP
        End If
                
    Case 4
        If IsCellAjust Then
            pDrawText = DT_LEFT
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
        Else
            pDrawText = DT_VCENTER Or DT_LEFT
        End If
    
    Case 5
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_VCENTER Or DT_CENTER
        End If
    
    Case 6
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Top = AlignBase(1, 5)
            Rect2.Bottom = AlignBase(2, 5)
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_VCENTER Or DT_RIGHT
        End If
    
    Case 7
        If IsCellAjust Then
            pDrawText = DT_LEFT
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
        Else
            pDrawText = DT_BOTTOM Or DT_LEFT
        End If
    
    Case 8
        If IsCellAjust Then
            pDrawText = DT_CENTER
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
            Rect2.Left = AlignBase(1, 3)
            Rect2.Right = AlignBase(2, 3)
        Else
            pDrawText = DT_BOTTOM Or DT_CENTER
        End If
    
    Case 9
        If IsCellAjust Then
            pDrawText = DT_RIGHT
            Rect2.Top = AlignBase(1, 6)
            Rect2.Bottom = AlignBase(2, 6)
            Rect2.Left = AlignBase(1, 4)
            Rect2.Right = AlignBase(2, 4)
        Else
            pDrawText = DT_BOTTOM Or DT_RIGHT
        End If
    
End Select
    
If IsCellAjust Then
    pDrawText = pDrawText Or DT_WORDBREAK
Else
    pDrawText = pDrawText Or DT_SINGLELINE
End If
    

'-> Gestion des formats num�rics
Select Case aCell.TypeValeur
    Case 0 '-> RAS
        PrintSigne = False
    Case 1 '-> Caract�re
        '-> Faire un susbstring
        aCell.ContenuCell = Mid$(aCell.ContenuCell, 1, CInt(aCell.Msk))
        PrintSigne = False
    Case 2 '-> Numeric
'-> V�rifier s'il y a quelquechose � formater
        If Trim(aCell.ContenuCell) = "" Then
        Else
             aFt = FormatNumTP(Trim(aCell.ContenuCell), aCell.Msk, NomBlock, "L" & aCell.Ligne & "C" & aCell.Colonne)
            '-> Rajouter Trois blancs au contenu de la cellule
            aFt.strFormatted = aFt.strFormatted & "   "
            If aFt.Ok Then
                '-> Mettre e rouge si necessaire
                If aFt.Value < 0 And aFt.idNegatif > 1 Then Sortie.ForeColor = QBColor(12)
                '-> affecter le contenu de la cellule
                aCell.ContenuCell = aFt.strFormatted
                '-> Si c'est un 0 mettre � blanc si pas imprimer
                If CDbl(aCell.ContenuCell) = 0 And Not (aCell.PrintZero) Then
                    aCell.ContenuCell = ""
                Else
                    If aFt.Value < 0 Then
                        If aFt.idNegatif = 0 Or aFt.idNegatif = 3 Then
                            aCell.ContenuCell = "- " & aCell.ContenuCell
                            PrintSigne = False
                        ElseIf aFt.idNegatif = 1 Or aFt.idNegatif = 4 Then
                            PrintSigne = True
                        End If
                    End If
                End If
            Else
                '-> Ne pas imprimer  de signe
                PrintSigne = False
            End If
        End If
End Select


'-> Faire de rect la zone de clipping en cours
hdlRgn = CreateRectRgn&(aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
SelectClipRgn Sortie.hdc, hdlRgn

'-> Postionnement du rectangle de dessin
DrawText Sortie.hdc, aCell.ContenuCell, Len(aCell.ContenuCell), Rect2, pDrawText Or DT_NOPREFIX

If PrintSigne Then DrawText Sortie.hdc, "- ", 2, Rect2, pDrawText Or DT_NOPREFIX

'-> Faire de la fen�tre entiere la zone de clipping
SelectClipRgn Sortie.hdc, 0

'-> Dessin des bordures de la cellule
SelectObject Sortie.hdc, hdlBordure

'-> Dessin de la bordure Bas
If aCell.BordureBas Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureBas Then
Else
    DrawBordure X1 - 1, Y2 - 1, X2, Y2 - 1, Couleur
End If

'-> Dessin de la bordure haut
If aCell.BordureHaut Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureHaut Then
Else
    DrawBordure X1 - 1, Y1 - 1, X2, Y1 - 1, Couleur
End If


'-> Dessin de la bordure gauche
If aCell.BordureGauche Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureGauche Then
Else
    DrawBordure X1 - 1, Y1 - 1, X1 - 1, Y2, Couleur
End If

'-> Dessin de la bordure Droite
If aCell.BordureDroite Then
    Couleur = QBColor(0)
Else
    Couleur = aCell.BackColor
End If
If aCell.FondTransparent And Not aCell.BordureDroite Then
Else
    DrawBordure X2 - 1, Y1 - 1, X2 - 1, Y2, Couleur
End If

'-> Gestion des angles des bordures
If aCell.BordureGauche Or aCell.BordureHaut Then
    '-> dessiner le coins sup�rieur gauche
    Couleur = 0
    DrawBordure X1 - 1, Y1 - 1, X1 - 1, Y1, Couleur
End If

If aCell.BordureDroite Or aCell.BordureHaut Then
    '-> Dessiner le coin sup�rieur droit
    Couleur = 0
    DrawBordure X2 - 1, Y1 - 1, X2 - 1, Y1, Couleur
End If

If aCell.BordureBas Or aCell.BordureGauche Then
    '-> Dessin du coin inf�rieur gauche
    Couleur = 0
    DrawBordure X1 - 1, Y2 - 1, X1, Y2 - 1, Couleur
End If

If aCell.BordureDroite Or aCell.BordureBas Then
    '-> Dessin du coin inf�rieur droit
    Couleur = 0
    DrawBordure X2 - 1, Y2 - 1, X2 - 1, Y2, Couleur
End If

'-> Supprimer la r�gion
DeleteObject hdlRgn
               
'-> Res�lectionner l'ancien stylo
If OldPen <> 0 Then
    SelectObject Sortie.hdc, OldPen
    DeleteObject hdlPen
End If

'-> Res�lectionner l'ancien pinceau
If oldBrush <> 0 Then
    SelectObject Sortie.hdc, oldBrush
    DeleteObject hdlBrush
End If

'-> Supprimer le stylo de dessin des bordures
DeleteObject hdlBordure

'-> Positionnement du label pour l'acces au d�tail
If aCell.UseAccesDet Then
    '-> V�rifier que l'on soit en visu
    If TypeOf Sortie Is PictureBox Then
        '-> cr�er un nouveau Label
        Load aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count + 1)
        '-> Positionner l'objet
        aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count).Left = X1
        aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count).Width = X2 - X1
        aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count).Top = Y1
        aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count).Height = Y2 - Y1
        '-> Acc�der au tableau de premier niveau
        Set aTb = aSpool.Maquette.Tableaux(UCase$(NomTb))
        '-> Pointer sur le block de tableau � �diter
        Set aBlock = aTb.Blocks("BL-" & UCase$(NomBlock))
        '-> Positionner la cl� d'acc�s au d�tail
        aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count).Tag = aCell.KeyAccesDet & Chr(0) & Entry(2, Entry(1, aBlock.KeyAccesDet, "�"), "|")
        '-> Le rendre visible
        aSpool.frmDisplay.lblAccesDet(aSpool.frmDisplay.lblAccesDet.Count).Visible = True
    End If 'Si on est en vosu ou en impression
End If 'Si on a acc�s au d�tail

'-> Renvoyer une valeur de succ�s
DrawCell = True

Exit Function

GestErr:

    Call GestError(41, NomTb & "|" & NomBlock & "|" & "L" & aCell.Ligne & "C" & aCell.Colonne, aSpool)
    

End Function

Private Sub DrawBordure(ByVal Move1, Move2, Line1, Line2, Couleur)

Dim hdlBordure As Long
Dim aPoint As POINTAPI
Dim Res As Long
Dim Old As Long

hdlBordure = CreatePen(PS_SOLID, 1, Couleur)
Old = SelectObject(Sortie.hdc, hdlBordure)
Res = MoveToEx(Sortie.hdc, Move1, Move2, aPoint)
LineTo Sortie.hdc, Line1, Line2
SelectObject Sortie.hdc, Old
DeleteObject hdlBordure


End Sub


Public Sub AnalyseObj(ByVal Ligne As String, TypeObjet As String, _
                       ByRef NomObjet As String, ByRef TypeSousObjet As String, _
                       ByRef NomSousObjet As String, ByRef Param As String, _
                       ByRef DataFields As String)

Dim Param1 As String
Dim Param2 As String
Dim Param3 As String
Dim Param4 As String

'---> Procedure qui r�cupre les d�finitions des objets dans une ligne
On Error Resume Next

If Ligne = "" Then Exit Sub

Param1 = Entry(1, Ligne, "]")
Param2 = Entry(2, Ligne, "]")
Param3 = Entry(2, Ligne, "{")

Param1 = Mid$(Param1, 2, Len(Param1) - 2)
'-> R�cup�ration du champ de donn�es
DataFields = Mid$(Param3, 1, Len(Param3) - 1)
'-> R�cup�ration des param�tres d'application
Param = Mid$(Param2, 2, Len(Param2) - 1)
'-> R�cup�ration du nom des objets
Param2 = Entry(1, Param1, "(")
Param3 = Entry(2, Param1, "(")

'-> Objet de premier niveau
TypeObjet = Entry(1, Param2, "-")
NomObjet = Entry(2, Param2, "-")

'-> Objet de second niveau
TypeSousObjet = Entry(1, Param3, "-")
NomSousObjet = Entry(2, Param3, "-")


End Sub
Private Function PrintSection(ByVal SectionName As String, ByRef Param As String, _
                         ByRef DataFields As String, ByRef FirstObj As Boolean, _
                         ByRef PositionX As Long, ByRef PositionY As Long, ByRef aSpool As Spool) As Boolean

'---> Cette proc�dure est charg�e d'imprimer une section

Dim aSection As Section
Dim i As Integer
Dim NomObjet As String
Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim DebutRangX As Long
Dim DebutRangY As Long

On Error GoTo GestError



'-> Pointer sur la section � imprimer
Set aSection = aSpool.Maquette.Sections(UCase$(SectionName))

'-> Imprimer la section
If Not PrintObjRtf(PositionX, PositionY, aSection, DataFields, DebutRangX, DebutRangY, aSpool) Then GoTo GestError

'-> Impression des objets associ�s dans l'ordre d'affichage
For i = 1 To aSection.nEntries - 1
    NomObjet = aSection.GetOrdreAffichage(i)
    If UCase$(Entry(1, NomObjet, "-")) = "CDR" Then
        '-> Pointer sur le cadre
        Set aCadre = aSection.Cadres(UCase$(Entry(2, NomObjet, "-")))
        '-> Imprimer l'objet
        If Not PrintObjRtf(PositionX, PositionY, aCadre, DataFields, DebutRangX, DebutRangY, aSpool, aCadre.MargeInterne) Then GoTo GestError
        '-> Liberer le pointeur
        Set aCadre = Nothing
    ElseIf UCase$(Entry(1, NomObjet, "-")) = "BMP" Then
        Set aBmp = aSection.Bmps(UCase$(Entry(2, NomObjet, "-")))
        If Not PrintBmp(aBmp, DebutRangX, DebutRangY, DataFields, aSpool) Then GoTo GestError
    End If
Next

'-> Positionner le pointeur de position apr�s l'�dition de la section
PositionY = Sortie.ScaleY(aSection.Hauteur, 7, 3) + PositionY 'Sortie.ScaleX(DebutRangY, 1, 3)

'-> Liberer les pointeurs
Set aSection = Nothing

PrintSection = True

Exit Function

GestError:

    Call GestError(36, aSection.Nom, aSpool)
    PrintSection = False

End Function

Private Function PrintBmp(aBmp As ImageObj, DebutRangX As Long, DebutRangY As Long, ByVal DataFields As String, ByRef aSpool As Spool) As Boolean

'---> Impression d'un BMP

Dim PosX As Long
Dim PosY As Long
Dim aPic As PictureBox
Dim Champ As String
Dim NomFichier As String
Dim aRect As RECT
Dim hdlPen As Long
Dim OldPen As Long
Dim hdlBrush As Long
Dim oldBrush As Long
Dim i As Integer


On Error GoTo GestErr


'-> Il faut modifier la position X et Y du cadre
PosX = Sortie.ScaleX(DebutRangX, 1, 3) + Sortie.ScaleX(aBmp.Left, 7, 3)
PosY = Sortie.ScaleY(DebutRangY, 1, 3) + Sortie.ScaleY(aBmp.Top, 7, 3)

'-> Imprimer le bmp
Set aPic = frmLib.PicObj(aBmp.IdPic)
If aBmp.isVariable Then
    '-> Venir charg� le BMP associ�
    '-> R�cup�ration du nombre de champs
    For i = 2 To NumEntries(DataFields, "^")
        Champ = Entry(i, DataFields, "^")
        If UCase$(aBmp.Fichier) = "^" & UCase$(Mid$(Champ, 1, 4)) Then
            '-> R�cup�ration du nom du fichier
            NomFichier = RTrim(Mid$(Champ, 5, Len(Champ) - 1))
            '-> Test de l'utilisation de l'association
            If aBmp.UseAssociation Then
                '-> R�cup�rer le nom du fichier dans le fichier ini
                NomFichier = GetPictureAssociation(NomFichier)
            End If
            Exit For
        End If
    Next
    '-> V�rifier que le fichier existe
    If Dir$(NomFichier) = "" Or Trim(NomFichier) = "" Then
    Else
        aPic.Picture = LoadPicture(NomFichier)
        Sortie.PaintPicture aPic.Picture, PosX, PosY
    End If
Else
    If Trim(aBmp.Fichier) = "" Then
    Else
        If Dir$(aBmp.Fichier, vbNormal) = "" Then
        Else
            Sortie.PaintPicture aPic.Picture, PosX, PosY
        End If
    End If
End If

'-> Dessiner la bordure si necessaire
If aBmp.Contour Then
    hdlBrush = CreateSolidBrush(0)
    aRect.Left = PosX
    aRect.Top = PosY
    aRect.Right = aRect.Left + Sortie.ScaleX(aPic.Width, 1, 3)
    aRect.Bottom = aRect.Top + Sortie.ScaleY(aPic.Height, 1, 3)
    FrameRect Sortie.hdc, aRect, hdlBrush
    DeleteObject hdlBrush
End If

Set aPic = Nothing

'-> Renvoyer une valeur de succ�s
PrintBmp = True

Exit Function

GestErr:

    Call GestError(39, aBmp.SectionName & "|" & aBmp.Nom, aSpool)
    PrintBmp = False
    

End Function

Private Function PrintObjRtf(PositionX As Long, PositionY As Long, _
                        aObject As Object, ByVal DataFields As String, _
                        DebutRangX As Long, DebutRangY As Long, ByRef aSpool As Spool, Optional MargeInterne As Single) As Boolean


'---> Cette fonction imprime un cadre ou une section ( Code RTF + Bordures )

Dim aField() As String
Dim aRtf As RichTextBox
Dim PosX As Long, PosY As Long
Dim i As Integer
Dim Champ As String
Dim RTFValue As String
Dim fr As FORMATRANGE
Dim lTextOut As Long, lTextAmt As Long, Res As Long
Dim aPoint As POINTAPI
Dim aRect As RECT
Dim hdlPen As Long
Dim OldPen As Long
Dim hdlBrush As Long
Dim oldBrush As Long
Dim NbChamp As Integer
Dim IsCadre As Boolean
Dim IsBordure As Boolean

On Error GoTo GestErr


'-> Mettre l'�diteur RTF aux bonnes dimensions
Set aRtf = frmLib.Rtf(aObject.IdRTf)

aRtf.Width = frmLib.ScaleX(aObject.Largeur, 7, 1)
aRtf.Height = frmLib.ScaleY(aObject.Hauteur, 7, 1)

'-> R�cup�ration du code RTF
RTFValue = aRtf.TextRTF

'-> Cr�er une base de champs
If Trim(DataFields) = "" Then
Else
    '-> R�cup�ration du nombre de champs
    NbChamp = NumEntries(DataFields, "^")
    For i = 2 To NbChamp
        Champ = Entry(i, DataFields, "^")
        ReDim Preserve aField(1 To 2, i - 1)
        aField(1, i - 1) = "^" & Mid$(Champ, 1, 4)
        aField(2, i - 1) = RTrim(Mid$(Champ, 5, Len(Champ) - 1))
    Next
    '-> Faire un remplacement des champs par leur valeur
    For i = 1 To NbChamp - 1
        aRtf.TextRTF = Replace(aRtf.TextRTF, aField(1, i), aField(2, i))
    Next
End If

'-> Calcul des positions d'impression de l'objet selon le type d'objet � imprimer
If TypeOf aObject Is Section Then
    Select Case aObject.AlignementLeft
        Case 2 'Marge gauche
            PosX = Sortie.ScaleX(aSpool.Maquette.MargeLeft, 7, 1)
        Case 4 'Centr�
            PosX = Sortie.ScaleX((aSpool.Maquette.Largeur - aObject.Largeur) / 2, 7, 1)
        Case 3 'Marge Droite
            PosX = Sortie.ScaleX(aSpool.Maquette.Largeur - aObject.Largeur - aSpool.Maquette.MargeLeft, 7, 1)
        Case 5 'Sp�cifi�
            PosX = Sortie.ScaleX(aObject.Left, 7, 1)
    End Select
    Select Case aObject.AlignementTop
        Case 1 'Libre
            PosY = Sortie.ScaleY(PositionY, 3, 1)
        Case 2 'Marge haut
            PosY = Sortie.ScaleY(aSpool.Maquette.MargeTop, 7, 1)
        Case 4 'Centr�
            PosY = Sortie.ScaleY((aSpool.Maquette.Hauteur - aObject.Hauteur) / 2, 7, 1)
        Case 3 'Marge bas
            PosY = Sortie.ScaleY(aSpool.Maquette.Hauteur - aObject.Hauteur - aSpool.Maquette.MargeTop, 7, 1)
        Case 5 'Sp�cifi�
            PosY = Sortie.ScaleY(aObject.Top, 7, 1)
    End Select

    '-> Tenir compte des variations des marges internes
    PosX = PosX - Sortie.ScaleX(MargeX, 3, 1)
    If aObject.AlignementTop <> 1 Then PosY = PosY - Sortie.ScaleY(MargeY, 3, 1)

    '-> Setting du rect de dessin
    aRect.Left = Sortie.ScaleX(PosX, 1, 3)
    aRect.Top = Sortie.ScaleY(PosY, 1, 3)
    aRect.Right = Sortie.ScaleX(aObject.Largeur, 7, 3) + Sortie.ScaleX(PosX, 1, 3)
    aRect.Bottom = Sortie.ScaleY(aObject.Hauteur, 7, 3) + Sortie.ScaleY(PosY, 1, 3)

    '-> Sauvagarder la position de d�but du rang
    DebutRangX = PosX
    DebutRangY = PosY
    
ElseIf TypeOf aObject Is Cadre Then
    '-> Il faut modifier la position X et Y du cadre
    PosX = DebutRangX + Sortie.ScaleX(aObject.Left, 7, 1)
    PosY = DebutRangY + Sortie.ScaleY(aObject.Top, 7, 1)
    '-> Setting du rect de dessin
    aRect.Left = Sortie.ScaleX(PosX, 1, 3)
    aRect.Top = Sortie.ScaleY(PosY, 1, 3)
    aRect.Right = Sortie.ScaleX(aObject.Largeur, 7, 3) + Sortie.ScaleX(PosX, 1, 3)
    aRect.Bottom = Sortie.ScaleY(aObject.Hauteur, 7, 3) + Sortie.ScaleY(PosY, 1, 3)
    '-> Indiquer que c'est un cadre
    IsCadre = True
End If


'-> Imprimer le fond de l'objet
hdlBrush = CreateSolidBrush(aObject.BackColor)
hdlPen = CreatePen(PS_NULL, 1, QBColor(15))

'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(Sortie.hdc, hdlBrush)
OldPen = SelectObject(Sortie.hdc, hdlPen)

'-> Dessin de l'objet
If IsCadre Then
    If Not aObject.IsRoundRect Then
        Res = Rectangle(Sortie.hdc, aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
    Else
        '-> Imprimer les bordures
        PrintBordure aObject, aRect
        IsBordure = True
    End If
        
Else
    Res = Rectangle(Sortie.hdc, aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
End If

'-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
If OldPen <> 0 Then
    SelectObject Sortie.hdc, OldPen
    DeleteObject hdlPen
End If

If oldBrush <> 0 Then
    SelectObject Sortie.hdc, oldBrush
    DeleteObject hdlBrush
End If

'-> Initialiser la srtucture Formatrange
fr.hdc = Sortie.hdc
fr.hdcTarget = Sortie.hdc
fr.chrg.cpMin = 0
fr.chrg.cpMax = -1

'-> Intialisation du rectangle destination
fr.rc.Left = Sortie.ScaleX(MargeInterne, 7, 1) + PosX
fr.rc.Top = Sortie.ScaleX(MargeInterne, 7, 1) + PosY
fr.rc.Right = Sortie.ScaleX(aObject.Largeur, 7, 1) + PosX - Sortie.ScaleX(MargeInterne, 7, 1)
fr.rc.Bottom = Sortie.ScaleY(aObject.Hauteur * 2, 7, 1) + PosY - Sortie.ScaleY(MargeInterne, 7, 1)

'-> Initialisation du rectangle de source
fr.rcPage.Left = 0
fr.rcPage.Top = 0
fr.rcPage.Right = Sortie.ScaleX(aObject.Largeur, 7, 1)
fr.rcPage.Bottom = Sortie.ScaleY(aObject.Hauteur, 7, 1)

'-> Faire un setting du mode de restitution
Res = SetMapMode(Sortie.hdc, MM_TEXT)

'-> initialisation des variables de pointage de texte
lTextOut = 0
lTextAmt = SendMessage(aRtf.hwnd, WM_GETTEXTLENGTH, 0, 0)

'-> Impression du Rtf
Do While lTextOut < lTextAmt
    lTextOut = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, fr)
    If lTextOut < lTextAmt Then
        fr.chrg.cpMin = lTextOut
        fr.chrg.cpMax = -1
    End If
Loop

'-> Lib�rer la ressource associ�e au RTF : VERSION COMPILEE
Res = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, Null)
If Res = 0 Then
    'MsgBox "Erreur dans la lib�ration du context"
    Res = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, vbNullString)
    'MsgBox "Apr�s Seconde tentative " & Res
End If

'-> VERSION INTERPRETEE
'Res = SendMessage(aRtf.hwnd, EM_FORMATRANGE, -1, Null)


'-> Imprimer les bordures
If Not IsBordure Then PrintBordure aObject, aRect

'-> Redonner son vrai contenu au controle RTF
aRtf.TextRTF = RTFValue
aRtf.Refresh

'-> Lib�rer le pointeur sur le controle RTF
Set aRtf = Nothing

'-> Renvoyer une valeur de suvv�s
PrintObjRtf = True

Exit Function

GestErr:

    If Err.Number = 11 Then
        PrintObjRtf = True
        Exit Function
    End If

    Call GestError(38, aObject.Nom, aSpool)
    PrintObjRtf = False
    


End Function

Private Sub PrintBordure(aObject As Object, aRect As RECT)

Dim Temp1 As Long
Dim Temp2 As Long
Dim ClipRgn As Long
Dim OldClip As Long
Dim Pix1X As Long
Dim Pix1Y As Long
Dim aPoint As POINTAPI

Dim hdlBrush As Long
Dim oldBrush As Long
Dim hdlPen As Long
Dim OldPen As Long
Dim Res As Long

On Error Resume Next

'-> Dessin du contour de l'objet si necessaire
If TypeOf aObject Is Section Then
    If aObject.Contour Then
        hdlBrush = CreateSolidBrush(0)
        FrameRect Sortie.hdc, aRect, hdlBrush
    End If
ElseIf TypeOf aObject Is Cadre Then


    '-> Tenir compte de la largeur du trait
    Temp2 = frmLib.ScaleX(aObject.LargeurTrait, 3, 1)
    Temp2 = Sortie.ScaleX(Temp2, 1, 3)

    Pix1X = frmLib.ScaleX(1, 3, 1)
    Pix1X = Sortie.ScaleX(Pix1X, 1, 3)

    Pix1Y = frmLib.ScaleY(1, 3, 1)
    Pix1Y = Sortie.ScaleY(Pix1Y, 1, 3)

    '-> cr�er un stylo de dessin
    hdlPen = CreatePen(PS_SOLID, Temp2, 0)
    SelectObject Sortie.hdc, hdlPen
    
    '-> Cr�er le fond
    hdlBrush = CreateSolidBrush(aObject.BackColor)
    oldBrush = SelectObject(Sortie.hdc, hdlBrush)
    
    '-> S�lectionner le rectangle comme zone de clipping
    ClipRgn = CreateRectRgn(aRect.Left, aRect.Top, aRect.Right, aRect.Bottom)
    SelectClipRgn Sortie.hdc, ClipRgn

    If aObject.IsRoundRect Then
        If Temp2 / 2 <> Fix(Temp2 / 2) Then Temp2 = Temp2 + 1
        
        Pix1X = frmLib.ScaleX(50, 3, 1)
        Pix1X = Sortie.ScaleX(Pix1X, 1, 3)

        Pix1Y = frmLib.ScaleY(50, 3, 1)
        Pix1Y = Sortie.ScaleY(Pix1Y, 1, 3)
        Res = RoundRect(Sortie.hdc, aRect.Left + Temp2 / 2, aRect.Top + Temp2 / 2, aRect.Right - Temp2 / 2, aRect.Bottom - Temp2 / 2, Pix1X, Pix1Y)
    Else
    
        '-> Bordure gauche
        If aObject.Gauche Then
            '-> Dessin des bordures
            Temp1 = Fix(Temp2 / 2)
            MoveToEx Sortie.hdc, aRect.Left + Temp1, aRect.Top, aPoint
            LineTo Sortie.hdc, aRect.Left + Temp1, aRect.Bottom - 1
        End If
        
        '-> Bordure haut
        If aObject.Haut Then
            Temp1 = Fix(Temp2 / 2)
            MoveToEx Sortie.hdc, aRect.Left, aRect.Top + Temp1, aPoint
            LineTo Sortie.hdc, aRect.Right, aRect.Top + Temp1
        End If
        
        '-> Bordure Droite
        If aObject.Droite Then
            If (Temp2 / 2) = Fix(Temp2 / 2) Then
                Temp1 = Temp2 / 2
            Else
                Temp1 = (Temp2 + 1) / 2
            End If
            MoveToEx Sortie.hdc, aRect.Right - Temp1, aRect.Top, aPoint
            LineTo Sortie.hdc, aRect.Right - Temp1, aRect.Bottom
        End If
        
        '-> Bordure Bas
        If aObject.Bas Then
            If (Temp2 / 2) = Fix(Temp2 / 2) Then
                Temp1 = Temp2 / 2
            Else
                Temp1 = (Temp2 + 1) / 2
            End If
            MoveToEx Sortie.hdc, aRect.Left, aRect.Bottom - Temp1, aPoint
            LineTo Sortie.hdc, aRect.Right, aRect.Bottom - Temp1
        End If
        
    End If
    
    '-> Restituer la zone de clipping
    OldClip = CreateRectRgn(0, 0, Sortie.ScaleWidth, Sortie.ScaleHeight)
    SelectClipRgn Sortie.hdc, OldClip
    DeleteObject ClipRgn
        
End If

'-> S�lectionner les anciens objets et supprimer les objets GDI non utilis�s
If OldPen <> 0 Then
    SelectObject Sortie.hdc, OldPen
    DeleteObject hdlPen
End If

If oldBrush <> 0 Then
    SelectObject Sortie.hdc, oldBrush
    DeleteObject hdlBrush
End If



End Sub



Public Function FormatNumTP(ByVal ToFormat As String, ByVal Msk As String, Optional NomBlock As String, Optional Cell As String) As FormatedCell

'---> Cette fonction a pour but d'analyser une chaine de caract�re et de la retouner
'formatt�e en s�parateur de milier et en nombre de d�cimal

Dim strFormat As String
Dim strTempo As String
Dim strPartieEntiere As String
Dim StrPartieDecimale As String
Dim i As Integer, j As Integer
Dim nbDec As Integer
Dim strSep As String
Dim idNegatif As Integer
Dim Masque As String
Dim Tempo As String
Dim FindSepDec As Boolean

On Error GoTo GestErr

For i = Len(ToFormat) To 1 Step -1
    If IsNumeric(Mid$(ToFormat, i, 1)) Then
        Tempo = Mid$(ToFormat, i, 1) & Tempo
    Else
        '-> Gestion du s�parateur d�cimal
        If Mid$(ToFormat, i, 1) = "." Or Mid$(ToFormat, i, 1) = "," Then
            If Not FindSepDec Then
                Tempo = SepDec & Tempo
                FindSepDec = True
            End If
        Else
            Tempo = Mid$(ToFormat, i, 1) & Tempo
        End If
    End If
Next 'Pour tous les caract�res

ToFormat = Tempo

If Trim(ToFormat) = "" Then
Else
    '-> Tester si on envoie une zone num�rique
    If Not IsNumeric(ToFormat) Then
        FormatNumTP.Ok = False
        Exit Function
    End If
End If

'-> Charger le masque de la cellule
nbDec = CInt(Entry(1, Msk, "�"))
If Entry(2, Msk, "�") <> "" Then
    strSep = SepMil 'Entry(2, Msk, "�")
Else
    strSep = ""
End If
idNegatif = CInt(Entry(3, Msk, "�"))

FormatNumTP.Ok = True
FormatNumTP.nbDec = nbDec
FormatNumTP.Value = CDbl(ToFormat)
FormatNumTP.idNegatif = idNegatif

'-> Analyse si d�cimale
If nbDec = 0 Then
    '-> Arrondir
    strTempo = CStr(CDbl(ToFormat))
    Masque = "#########################################0"
Else
    Masque = "#########################################0." & String(nbDec, "0")
    strTempo = ToFormat
End If

'-> Construction d'un masque assez grand pour formatter n'importe qu'elle zone
strTempo = Format(Abs(strTempo), Masque)

'-> Construction de la partie enti�re
If nbDec <> 0 Then
    strPartieEntiere = Mid$(strTempo, 1, InStr(1, strTempo, SepDec) - 1)
    StrPartieDecimale = SepDec & Mid$(strTempo, InStr(strTempo, SepDec) + 1, nbDec)
Else
    strPartieEntiere = strTempo
    StrPartieDecimale = ""
End If

j = 1
For i = Len(strPartieEntiere) To 1 Step -1
    strFormat = Mid$(strTempo, i, 1) & strFormat
    If j = 3 And i <> 1 Then
        strFormat = strSep & strFormat
        j = 1
    Else
        j = j + 1
    End If
Next

FormatNumTP.strFormatted = strFormat & StrPartieDecimale

Exit Function

GestErr:
    FormatNumTP.Ok = False
End Function

Private Function PrintTableau(ByRef NomObjet As String, ByRef Param As String, _
                         ByRef DataFields As String, ByRef PositionX As Long, _
                         ByRef PositionY As Long, ByRef NomSousObjet As String, ByRef FirstObj As Boolean, ByRef aSpool As Spool) As Boolean

Dim aTb As Tableau
Dim nLig As Integer


On Error GoTo GestError

'-> Pointer sur le tableau pass� en argument
Set aTb = aSpool.Maquette.Tableaux(UCase$(NomObjet))

'-> R�cup�ration de la ligne de tableau � imprimer
nLig = CInt(Entry(2, Param, "\"))

'-> Imprimer le block de ligne
PositionY = PrintBlock(NomSousObjet, Param, DataFields, PositionX, PositionY, aTb, nLig, aSpool)

'-> Quitter si valeur d'erreur
If PositionY = -9999 Then GoTo GestError

'-> Renvoyer une valeur de succ�s
PrintTableau = True

Exit Function

GestError:
    Call GestError(37, aTb.Nom, aSpool)
    PrintTableau = False


End Function

Public Function GetPictureAssociation(ValueToSearch As String) As String

Dim Res As Long
Dim lpBuffer As String

On Error GoTo GestError

'-> Ne traiter l'association d'image qu'en version PathV51
If VersionTurbo = 1 Then Exit Function

'-> V�rifier que l'on ait bien acc�s au fichier TM_Picture.ini
If Tm_PictureIniFile = "" Then Exit Function

lpBuffer = Space$(5000)
Res = GetPrivateProfileString("IMAGES", ValueToSearch, "", lpBuffer, Len(lpBuffer), Tm_PictureIniFile)
If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
    '-> Cr�er la liste des mots cl�s
    GetPathMotCles
    '-> Faire le remplacement
    ReplacePathMotCles lpBuffer
    GetPictureAssociation = lpBuffer
End If

Exit Function

GestError:
    GetPictureAssociation = ""

End Function

Public Sub CloseFichier(ByVal FileToclose As String)

'---> Cette d�charge le fichier sp�cifi�

Dim aFichier As Fichier
Dim aSpool As Spool


On Error Resume Next

'-> Pointer sur le fichier sp�cifi�
Set aFichier = Fichiers(UCase$(Trim(FileToclose)))

'-> supprimer toutes les pages actives
For Each aSpool In aFichier.Spools
    If Not aSpool.frmDisplay Is Nothing Then Unload aSpool.frmDisplay
Next

'-> Supprimer l'objet de la collection des fichiers
Fichiers.Remove (UCase$(Trim(FileToclose)))

'-> Supprimer les nodes
MDIMain.TreeNaviga.Nodes.Remove (UCase$(Trim(FileToclose)))

End Sub



Public Sub DrawWait()

'---> Proc�dure qui dessine la temporisation

Dim hdlBrush As Long
Dim hdlPen As Long
Dim oldBrush As Long
Dim OldPen As Long
Dim Res As Long
Dim hdc As Long
Dim StartPanel As Long

On Error Resume Next

'-> Get du Dc du pannel
hdc = GetDC(MDIMain.StatusBar1.hwnd)

'-> Cr�ation des objets GDI pour dessin des cellules
hdlBrush = CreateSolidBrush(&HFF8080)
hdlPen = CreatePen(PS_NULL, 1, 0)

'-> S�lection du pinceau dans le Contexte
oldBrush = SelectObject(hdc, hdlBrush)

'-> Dessin de la cellule
OldPen = SelectObject(hdc, hdlPen)

'-> Depart du dessin
StartPanel = frmLib.ScaleX(MDIMain.StatusBar1.Panels(1).Width, 1, 3) + 4

'-> Dessin du rectangle
Res = Rectangle(hdc, StartPanel, 3, StartPanel + ((TailleLue / TailleTotale) * (frmLib.ScaleX(MDIMain.StatusBar1.Panels(2).Width, 1, 3) - 2)), 17)

'-> Liberer les objets GDI
SelectObject hdc, oldBrush
SelectObject hdc, OldPen
DeleteObject hdlPen
DeleteObject hdlBrush
ReleaseDC MDIMain.StatusBar1.hwnd, hdc

End Sub
    
Private Sub LoadInternetMessProg()

Dim aLb As Libelle
Dim strLib As String

'********************
'* MESSPROG MDIMAIN *
'********************

Set aLb = New Libelle
strLib = "1=&Fichier" & Chr(0) & "2=&Ouvrir" & Chr(0) & "3=&Imprimer" & Chr(0) & _
         "4=&Quitter" & Chr(0) & "5=F&en�tre" & Chr(0) & "6=&Navigation" & Chr(0) & _
         "7=Mosa�que &horizontale" & Chr(0) & "8=Mosa�que &verticale" & Chr(0) & _
         "9=&En cascade" & Chr(0) & "10=Visionneuse DEAL INFORMATIQUE." & Chr(0) & _
         "11=Erreur sur le fichier." & Chr(0) & "12=Page N� : " & Chr(0) & "13=Fermer le fichier" & Chr(0) & _
         "14=Imprimer" & Chr(0) & "15=Propri�t�s" & Chr(0) & "16=Envoyer Vers" & Chr(0) & _
         "17=Internet" & Chr(0) & "18=Messagerie" & Chr(0) & "19=Editeur de texte" & Chr(0) & _
         "20=&R�organiser les icones" & Chr(0) & "21=A propos de la visionneuse DEAL Informatique" & Chr(0) & _
         "22=Supprimer" & Chr(0) & "23=D�finitivement" & Chr(0) & "24=Envoyer � la poubelle" & Chr(0) & "25=Le fichier existe d�ja : d�sirez-vous l'�craser ?�Confirmation"

'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "MDIMAIN"

'******************
'* Ac�s au d�tail *
'******************

Set aLb = New Libelle
strLib = "1=Impossible de trouver le fichier d�tail sp�cifi�D�tail" & Chr(0) & "2=Cr�ation de l'acc�s au d�tail en cours..." & _
          Chr(0) & "3=Acc�s au d�tail"

'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "ACCESDET"

'********************
'* MESSPROG FRMMAIL *
'********************
Set aLb = New Libelle
strLib = "1=Envoyer un fichier" & Chr(0) & "2=G�n�ral" & Chr(0) & "3=Envoyer la page en cours" & Chr(0) & _
         "4=Envoyer le spool en entier" & Chr(0) & "5=Crypter le fichier" & Chr(0) & "6=Internet" & Chr(0) & _
         "7=OutLook" & Chr(0) & "8=Destinataire" & Chr(0) & "9=Sujet" & Chr(0) & "10=Message" & Chr(0) & _
         "11=Envoyer" & Chr(0) & "12=Police" & Chr(0) & "13=Couleur" & Chr(0) & "14=Aligner � gauche" & Chr(0) & _
         "15=Centrer" & Chr(0) & "16=Aligner � droite" & Chr(0) & "17=Veuillez saisir le destinataire du message�Erreur" & Chr(0) & _
         "18=Envoyer le fichier en entier" & Chr(0) & "19=Options" & Chr(0) & "20=Format du fichier" & Chr(0) & _
         "21=Utiliser le format Turbo" & Chr(0) & "22=Utiliser le format HTML" & Chr(0) & "23=Navigation" & Chr(0) & _
         "24=Cr�er une page de navigation" & Chr(0) & "25=G�n�rer un seul fichier contenant toutes les pages" & Chr(0) & _
         "26=G�n�rer de simples pages" & Chr(0) & "27=Inclure le num�ro du spool dans le nom des pages"
         
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "FRMMAIL"
         
'*************************
'* MESSPROG FRMINTERNET  *
'*************************
         
Set aLb = New Libelle
strLib = "1=Exporter au format HTML" & Chr(0) & "2=Export :" & Chr(0) & "3=Exporter la page en cours" & Chr(0) & _
         "4=Exporter le spool en entier" & Chr(0) & "5=Options : " & Chr(0) & "6=Cr�er une page de navigation" & Chr(0) & _
         "7=Lancer � la fin du traitement" & Chr(0) & "8=Traitement : " & Chr(0) & "9=Cr�ation de la page : " & Chr(0) & _
         "10=Exporter" & Chr(0) & "11=Navigation : " & Chr(0) & "12=Utiliser une barre de navigation" & Chr(0) & "13=Utiliser une liste de pages" & Chr(0) & _
         "14=Exporter le fichier en entier" & Chr(0) & "15=Inclure le num�ro du spool dans le nom des pages" & Chr(0) & _
         "16=G�n�rer un seul fichier contenant toutes les pages" & Chr(0) & "17=G�n�rer de simples pages" & Chr(0) & _
         "18=Fichier : " & Chr(0) & "19=R�pertoire d'export" & Chr(0) & "20=Nom du fichier � g�n�rer" & Chr(0) & _
         "21=Nom de fichier incorrect" & Chr(0) & "22=Exporter les bordures"
         
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "FRMINTERNET"
                  
'*************************
'* MESSPROG FRMVISUSPOOL *
'*************************

Set aLb = New Libelle
strLib = "1=Page de s�lection" & Chr(0) & "2=Premi�re page" & Chr(0) & "3=Page pr�c�dente" & Chr(0) & _
         "4=Atteindre une page" & Chr(0) & "5=Page suivante" & Chr(0) & "6=Derni�re page" & Chr(0) & _
         "7=Imprimer" & Chr(0) & "8=Envoyer un message" & Chr(0) & "9=Exporter au format HTML" & Chr(0) & _
         "10=Aide" & Chr(0) & "11=Spool : � du fichier : " & Chr(0) & "12=Imprimer le fichier" & Chr(0) & _
         "13=Imprimer le spool" & Chr(0) & "14=Imprimer la page"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "FRMVISUSPOOL"
         
'**********************
'* MESSPROG STATUSBAR *
'**********************
Set aLb = New Libelle
strLib = "1=Lecture du fichier $FICHIER$ en cours ..." & Chr(0) & "2=Initialisation des maquettes ..." & Chr(0) & _
         "3=Lecture de la page en cours ..."
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "STATUSBAR"

'*******************
'* MESSPROG ERRORS *
'*******************
Set aLb = New Libelle
strLib = "1=" & Chr(0) & _
         "2=Impossible d'ouvrir le fichier $FICHIER$ pour lecture." & Chr(0) & _
         "3=Erreur lors de la cr�ation de l'objet de la classe Fichier pour le fichier $FICHIER$." & Chr(0) & _
         "4=Impossible de lire le fichier ASCII apr�s ouverture." & Chr(0) & "5=" & Chr(0) & _
         "6=Impossible de trouver un spool dans fichier sp�cifi� $FICHIER$." & Chr(0) & _
         "7=Impossible de trouver la maquette $MAQ$ du fichier $FICHIER$." & Chr(0) & _
         "8=Erreur systeme : Impossible de cr�er un fichier temporaire pour lecture de la maquette $MAQ$." & Chr(0) & _
         "9=Impossible d'ouvrir le fichier temporaire $INI$ de la maquette $MAQ$." & Chr(0) & _
         "10=Erreur lors de la lecture du fichier temporaire $INI$ de la maquette $MAQ$." & Chr(0) & _
         "11=Erreur dans la cr�ation d'un objet de type Section : $SECTION$ de la maquette $MAQ$." & _
         "12=Erreur dansla cr�ation d'un  objet de type Tableau : $TABLEAU$ de la maquette $MAQ$." & Chr(0) & "13=" & Chr(0) & _
         "14=Erreur dans la cr�ation d'un BMP : $BMP$ de la section : $SECTION$ de la maquette $MAQ$." & Chr(0) & _
         "15=Erreur dans la cr�ation d'un Cadre : $CADRE$ de la section : $SECTION$ de la maquette $MAQ$." & Chr(0) & _
         "16=Erreur dans la cr�ation d'un block : $BLOCK$ du tableau $TABLEAU$ de la maquette $MAQ$." & Chr(0) & _
         "17=Impossible de cr�er un fichier temporaire pour un objet RTF." & Chr(0) & _
         "18=Erreur dans l'initialisation de l'entete de la maquette : " & Chr(0) & _
         "19=Erreur dans l'initialisation d'un nouveau conteneur BMP." & Chr(0) & _
         "20=Erreur dans le setting des dimensions d'un objet BMP : $BMP$ de la section : $SECTION$. Hauteur : $HAUTEUR$   Largeur : $LARGEUR$." & Chr(0) & _
         "21=Erreur dans le setting du propath d'une image." & Chr(0) & _
         "22=Erreur lors du chargement du fichier : $FICHIER$ du bmp : $BMP$ de la section : $SECTION$." & Chr(0) & _
         "23=Erreur lors de la cr�ation de la repr�sentation de l'objet cadre $CADRE$ de la section $SECTION$." & Chr(0) & _
         "24=Erreur lors du chargement du fichier temporaire $FICHIER$ du cadre : $CADRE$ de la section : $SECTION$." & Chr(0) & _
         "25=Erreur lors du chargement du fichier temporaire $FICHIER$ de la section : $SECTION$." & Chr(0) & _
         "26=Erreur lors de la cr�ation de la repr�sentation physique de la section : $SECTION$." & Chr(0) & _
         "27=Erreur lors de l'initialisation de la section $SECTION$ pour la valeur de ligne : $LIG$." & Chr(0)
         
         
strLib = strLib & "28=Erreur lors de l'initialisation de l'entete de l'image : $BMP$ de la section $SECTION$ pour la valeur : $LIG$" & Chr(0) & Chr(0) & _
                  "29=Erreur lors de l'initialisation de l'entete du cadre : $CADRE$ de la section $SECTION$ pour la valeur : $LIG$" & Chr(0) & _
                  "30=Erreur lors de l'initialisation du tableau $TABLEAU$ pour la valeur : $LIG$." & Chr(0) & _
                  "31=Erreur lors de l'initialisation du block $BLOCK$ pour le tableau $TABLEAU$ pour la valeur $LIG$." & Chr(0) & _
                  "32=Erreur lors de la lecture d'une ligne de d�finition du block $BLOCK$ du tableau $TABLEAU$." & Chr(0) & _
                  "33=Erreur lors de la cr�ation des cellule du block $BLOCK$ du tableau $TABLEAU$ pour la valeur $LIG$." & Chr(0) & _
                  "34=Erreur lors de l'initialisation des dimensions des cellules du block : $BLOCK$ du tableau $TABLEAU$" & Chr(0) & _
                  "35=Erreur dans l'initialisation de la page � imprimer" & Chr(0) & _
                  "36=Erreur dans l'initialisation de l'impression de la section : $SECTION$." & Chr(0) & _
                  "37=Erreur dans l'initialisation de l'impression du tableau : $TABLEAU$." & Chr(0) & _
                  "38=Erreur dans l'impression d'un objet RTF : $OBJECT$." & Chr(0) & _
                  "39=Erreur dans l'impression de l'image : $BMP$ de la section : $SECTION$." & Chr(0) & _
                  "40=Erreur dans l'initialisation du block : $BLOCK$ du tableau $TABLEAU$" & Chr(0) & _
                  "41=Erreur dans le dessin de la cellule : $CELLULE$ du block : $BLOCK$ du tableau $TABLEAU$." & Chr(0) & _
                  "42=Erreur durant l'impression du fichier.�Erreur fatale d'impression"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "ERRORS"

'********************
'* MESSPROG MESSAGE *
'********************
Set aLb = New Libelle

strLib = "1=Quitter maintenant ?�Confirmation" & Chr(0) & "2=Fichier d�ja charg�.�Impossible de charger le fichier." & Chr(0) & _
         "3=Num�ro de page incorrect.�Erreur" & Chr(0) & "4=Impossible de se connecter � Ms OutLook.�Impossible d'envoyer un message." & Chr(0) & _
         "5=Impossible de cr�er un nouveau message dans Ms OutLook.�Impossible de cr�er un message." & Chr(0) & _
         "6=Impossible de joinde le fichier sp�cifi� : $FICHIER$.�Erreur." & Chr(0) & _
         "7=Envoyer la page en cours [Oui] ou le spool en entier [Non] ?�Question" & Chr(0) & _
         "8=Veuillez saisir le destinataire du message.�Erreur" & Chr(0) & "9=Impossible d'envoyer le message sur Internet.�Erreur" & Chr(0) & _
         "10=Impossible d'initialiser la session de messagerie.�Erreur" & Chr(0) & "11=Crypter le fichier ?�Question"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "MESSAGE"

'********************
'* MESSPROG BOUTONS *
'********************
Set aLb = New Libelle

strLib = "1=&Ok" & Chr(0) & "2=&Annuler" & Chr(0) & "3=&Envoyer"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "BOUTONS"

'***************
'* FRMNETSEND  *
'***************
Set aLb = New Libelle
strLib = "1=Envoyer un e-mail" & Chr(0) & "2=Url : " & Chr(0) & "3=Message : " & Chr(0) & _
         "4=Objet : " & Chr(0) & "5=Carnet d'adresses" & Chr(0) & "6=Crypter le fichier associ� :"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "FRMNETSEND"
    
'***************
'* FRMEDITOR  *
'***************
Set aLb = New Libelle
strLib = "1=&Fichier" & Chr(0) & "2=&Ouvrir" & Chr(0) & "3=&Enregistrer" & Chr(0) & _
         "4=En&registrer sous" & Chr(0) & "5=&Imprimer" & Chr(0) & "6=&Quitter" & Chr(0) & _
         "7=&Edition" & Chr(0) & "8=Couper" & Chr(0) & "9=Copier" & Chr(0) & _
         "10=Coller" & Chr(0) & "11=Rechercher" & Chr(0) & "12=A propos de ..." & Chr(0) & _
         "13=Chercher : " & Chr(0) & "14=Forma&t" & Chr(0) & "15=&Police" & Chr(0) & _
         "16=Envoie du fichier : " & Chr(0) & "17=Envoie du spool :" & Chr(0) & "18=Envoie de la page : "
         
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "FRMEDITOR"

'***************
'* FRMFONT     *
'***************
Set aLb = New Libelle
strLib = "1=Choix d'une police" & Chr(0) & "2=Police :" & Chr(0) & "3=Style de police :" & Chr(0) & _
         "4=Taille : " & Chr(0) & "5=Normal" & Chr(0) & "6=Italique" & Chr(0) & "7=Gras" & Chr(0) & _
         "8=Gras Italique"

'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "FRMFONT"

'**************************
'* MESSPROG FRMSEARCHPAGE *
'**************************
Set aLb = New Libelle

strLib = "1=Atteindre une page" & Chr(0) & "Num�ro de la page :"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "FRMSEARCHPAGE"


'*************************
'* MESSPROG D'IMPRESSION *
'*************************
Set aLb = New Libelle
strLib = "1=S�lection d'une imprimante" & Chr(0) & "2=Nom : " & Chr(0) & "3=Entendue de l'impression : " & Chr(0) & _
        "4=Imprimer le fichier" & Chr(0) & "5=Pages" & Chr(0) & "6=de :" & Chr(0) & "7=a :" & Chr(0) & _
        "8=Page en cours" & Chr(0) & "9=Copies" & Chr(0) & "10=" & Chr(0) & "11=&OK" & Chr(0) & "12=&Annuler" & Chr(0) & _
        "13=Veuillez s�lectionner une imprimante�Erreur" & Chr(0) & "14=Veuillez indiquer le nombre de copies�Erreur" & Chr(0) & _
        "15=Veuillez indiquer la page mini � imprimer�Erreur" & Chr(0) & "16=Veuillez indiquer la page maxi � imprimer�Erreur" & Chr(0) & _
        "17=Visualisation �cran" & Chr(0) & _
        "18=Propri�t�s" & Chr(0) & _
        "19=Imprimer en Recto-Verso" & Chr(0) & "20=Spool en cours"
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "FRMPRINTVISU"
        
'*********************
'* EXPORT VERS EXCEL *
'*********************
Set aLb = New Libelle

strLib = "1=Export vers Excel" & Chr(0) & "2=Etendue de l'export:" & Chr(0) & "3=Gestion des feuilles" & Chr(0) & _
        "4=Cr�er une feuille � chaque page" & Chr(0) & "5=Regrouper toutes les pages sur une seule feuille" & Chr(0) & _
        "6=Attention : la dur�e du traitement de l'export vers MircoSoft Excel est largement conditionn�e par les options de mise en forme des cellules." & Chr(0) & _
        "7=Maquette" & Chr(0) & "8=Propri�t�s d'export : " & Chr(0) & "9=Tableau :" & Chr(0) & _
        "10=Block :" & Chr(0) & "11=Ligne :" & Chr(0) & "12=N'exporter qu'une seule fois" & Chr(0) & _
        "13=R�f�rence" & Chr(0) & "14=Utiliser un classeur de r�f�rence" & Chr(0) & "15=Nommer la feuille" & Chr(0) & _
        "16=Nommer la plage de cellules" & Chr(0) & "17=Sp�cifier la cellule de d�part" & Chr(0) & "18=Traitement" & Chr(0) & _
        "19=Traitement en cours" & Chr(0) & "20=Exporter le format" & Chr(0) & "21=Valeur de propri�t� incorrecte" & Chr(0) & _
        "22=Erreur dans le param�trage des pages � imprimer" & Chr(0) & "23=Erreur" & Chr(0) & "24=Veuillez saisir le classeur de r�f�rence" & Chr(0) & _
        "25=Impossible de trouver le fichier" & Chr(0) & "26=Veuillez saisir le nom de l'onglet de classeur" & Chr(0) & "27=Veuillez saisir le nom de la plage de donn�es" & Chr(0) & _
        "28=Veuillez saisir la cellule de d�part" & Chr(0) & "29=Il est obligatoire de donner un nom � la feuille lorsque l'on exporte toutes les pages dans une seule feuille." & Chr(0) & _
        "30=Classeur Excel" & Chr(0) & "31=Mod�le de classeur" & Chr(0) & "32=Erreur dans l'ouverture du classeur de r�f�rence" & Chr(0) & "33=Nom de fichier incorrect" & Chr(0) & "34=Visualisation �cran" & Chr(0) & _
        "35=G�n�ral" & Chr(0) & "36=Spool en entier" & Chr(0) & "37=Descriptifs des �l�m�nts � exporter" & Chr(0) & "38=Page" & Chr(0) & _
        "39=Export : " & Chr(0) & "40=Formatage : "
'-> Setting des messprog
aLb.SetKeys strLib
'-> Ajout dans la collection
Libelles.Add aLb, "FRMEXCEL"


End Sub


Public Function IsOutLook() As Boolean

'---> Cette proc�dure V�rifier si outlook est install�

Dim aObj As Object

On Error GoTo GestError

Set aObj = CreateObject("Outlook.Application")

Set aObj = Nothing
IsOutLook = True


Exit Function

GestError:
    IsOutLook = False

End Function



Public Function InitMAPISession() As Boolean

'---> Cette proc�dure initialise la session MAPI

Dim aLb As Libelle

On Error GoTo ErrorLogon

'-> Ne pas cr�er de session MAPI s'il y en a d�ja eu une
If frmLib.MAPISession1.NewSession Then
    '-> Ne pas cr�er de session
Else
    With frmLib.MAPISession1
        .DownLoadMail = False '-> Ne pas charger les e-emails � l'ouverture de la session
        .LogonUI = True '-> Indique si une bo�te de dialogue est affich�e au moment de l'ouverture d'une session
        .SignOn '-> Connecte l'utilisateur au compte indiqu� par les propri�t�s UserName et Password.
        .NewSession = True '-> Indique qu'une session MAPI est en cours
        frmLib.MAPIMessages1.SessionID = .SessionID '-> Affecter au message l'ID de session MAPI
    End With
End If

InitMAPISession = True

Exit Function

ErrorLogon:

If Err.Number = 32003 Then
    '-> Ne rien faire : l'utilisateur a cliqu� sur annuler
Else
    '-> Pointer sur la classe message
    Set aLb = Libelles("MESSAGE")
    MsgBox aLb.GetCaption(10), vbCritical + vbOKOnly, aLb.GetCaption(10)
End If

InitMAPISession = False

End Function

Public Sub SendToInternet(ByVal FileToSend As String, Optional pBodyFile As String)

'---> Cette proc�dure envoie un e-mail sur Internet via le ctrl MAPI

Dim aLb As Libelle
Dim hdlBody As Integer
Dim Ligne As String

On Error GoTo GestError

'-> Intialiser la session MAPI
If Not InitMAPISession Then Exit Sub

'-> V�rifier la pr�sence du fichier � linker
If Dir$(FileToSend, vbNormal) = "" Then Exit Sub

'-> Cr�er un nouveau message
With frmLib.MAPIMessages1
    .MsgIndex = -1
    .AddressEditFieldCount = 2
    .Compose
    .RecipAddress = Entry(1, NetParam, Chr(0))
    .MsgSubject = Entry(2, NetParam, Chr(0))
    .AttachmentPathName = FileToSend
End With
    
'-> Corp du message
If Trim(Entry(3, NetParam, Chr(0))) = "" Then
    '-> V�rifier si un fichier BODY file existe
    If pBodyFile <> "" Then
        '-> V�rifier que le fichier existe
        If Dir$(pBodyFile, vbNormal) <> "" Then
            '-> Ouverture du fichier
            hdlBody = FreeFile
            Open pBodyFile For Input As #hdlBody
            Do While Not EOF(hdlBody)
                Line Input #hdlBody, Ligne
                If frmLib.MAPIMessages1.MsgNoteText = "" Then
                    frmLib.MAPIMessages1.MsgNoteText = Ligne
                Else
                    frmLib.MAPIMessages1.MsgNoteText = frmLib.MAPIMessages1.MsgNoteText & Chr(13) + Chr(10) & Ligne
                End If
            Loop
            '-> Fermer le fichier
            Close #hdlBody
        End If 'Si fichier bodyfile existe
    End If 'Si fichier bodyfile sp�cifi�
Else
    '-> Setting du body
    frmLib.MAPIMessages1.MsgNoteText = Entry(3, NetParam, Chr(0))
End If
    
    
If frmLib.MAPIMessages1.MsgNoteText <> "" Then
    frmLib.MAPIMessages1.AttachmentPosition = Len(frmLib.MAPIMessages1.MsgNoteText) - 1
Else
    frmLib.MAPIMessages1.AttachmentPosition = 0
End If
    
frmLib.MAPIMessages1.Send False
    
Exit Sub

GestError:

    '-> Ne pas afficher de message d'erreur si on vient de la ligne de commande
    If OrigineMail = 1 Then
        MsgBox "Erreur pendant l'envoir du mail :" & Chr(13) & Err.Number & "  " & Err.Description, vbCritical + vbOKOnly, "Erreur"
    End If

End Sub


Public Sub SendToOutLook(ByVal FileToSend As String, Optional pParam As String)

'---> Cette proc�dure ouvre OutLook, cr�er un nouveau message , et joint le fichier Sp�cifi�

Dim aOutLook As Object
Dim aMail As Object
Dim ErrorCode As Integer
Dim aLb As Libelle
Dim i As Integer
Dim hdlBody As Integer
Dim Ligne As String

'-> pParam pour envoie depuis CDE avec
' TO + chr(0) + COPIES + chr(0) + OBJET + chr(0) + BODY + chr(0) + BODYFILE

On Error GoTo GestError

'-> V�rifier que le fichier existe
If Dir$(FileToSend, vbNormal) = "" Then
    ErrorCode = 3
    GoTo GestError
End If

'-> Obtenir un pointeur vers OutLook
ErrorCode = 1
Set aOutLook = CreateObject("Outlook.Application")

'-> Cr�er un nouveau message
ErrorCode = 2
Set aMail = aOutLook.createitem(0)

'-> Afficher le corps du message
If OrigineMail = 0 Then
    '-> Envoie depuis la page de visu
    If frmMail.RichTextBox1.Text <> "" Then aMail.Body = frmMail.RichTextBox1.TextRTF
Else
    '-> Setting des param�tres
    aMail.To = Entry(1, pParam, Chr(0))
    aMail.CC = Entry(2, pParam, Chr(0))
    aMail.Subject = Entry(3, pParam, Chr(0))
    '-> Setting du body
    If Trim(Entry(4, pParam, Chr(0))) = "" Then
        '-> V�rifier si un fichier pour body est donn�
        If Trim(Entry(5, pParam, Chr(0))) <> "" Then
            If Dir$(Trim(Entry(5, pParam, Chr(0))), vbNormal) <> "" Then
                '-> Ouvrir le fichier body
                hdlBody = FreeFile
                Open Trim(Entry(5, pParam, Chr(0))) For Input As #hdlBody
                Do While Not EOF(hdlBody)
                    '-> Lecture de la ligne
                    Line Input #hdlBody, Ligne
                    '-> Ajout dans le body du text
                    If aMail.Body <> "" Then
                        aMail.Body = aMail.Body & Chr(13) + Chr(10) & Ligne
                    Else
                        aMail.Body = Ligne
                    End If
                Loop
                '-> Fermer le fichier
                Close #hdlBody
            End If 'Si fichier body existe
        End If 'Si fichier body sp�cifi�
    Else
        aMail.Body = Entry(4, pParam, Chr(0))
    End If 'Si pas de body sp�cifi�
End If

'-> Attacher le fichier sp�cifi�
ErrorCode = 3
aMail.Attachments.Add FileToSend

'-> Afficher le mail
ErrorCode = 4
If SendOutLook Then
    aMail.Send
Else
    aMail.Display
End If

'-> Lib�rer les pointeurs
Set aMail = Nothing
Set aOutLook = Nothing

Exit Sub

GestError:

    '-> N'afficher les messages d'erreur que si on est en visu
    If OrigineMail = 0 Then
    
        '-> Pointer sur la classe libelle
        Set aLb = Libelles("MESSAGE")
                     
        '-> Erreur lors de la connexion : envoyer un message d'erreur
        Select Case ErrorCode
            Case 1
                '-> Afficher un message d'erreur
                MsgBox aLb.GetCaption(4), vbCritical + vbOKOnly, aLb.GetToolTip(4)
            Case 2
                '-> Afficher un message d'erreur
                MsgBox aLb.GetCaption(5), vbCritical + vbOKOnly, aLb.GetToolTip(5)
                '-> Lib�rer le pointeur d'outlook
                Set aOutLook = Nothing
            Case 3
                '-> Afficher un message d'erreur
                MsgBox Replace(aLb.GetCaption(6), "$FICHIER$", FileToSend), vbCritical + vbOKOnly, aLb.GetToolTip(6)
                '-> Lib�rer le pointeur d'outlook
                Set aMail = Nothing
                Set aOutLook = Nothing
        
        End Select
    End If

End Sub


Public Sub CreateFileToSend()

'---> Cette fonction cr�er le fichier � linker pour export OutLook ou Internet
Dim aFichier As Fichier
Dim aSpool As Spool
Dim SpoolName As String

On Error GoTo GestError

'-> Pointer sur l'objet Fichier
Set aFichier = Fichiers(FileKeyMail)

'-> Pointer sur 'objet spool
If SpoolKeyMail <> "" Then Set aSpool = aFichier.Spools(SpoolKeyMail)
    
'-> Analyse selon l'origine
Select Case ExportMail

    Case 0, 1
        If ExportMail = 0 Then
            '-> Envoie de la page en cours
            SpoolName = CreateDetail(aSpool, PageNumMail)
        Else
            '-> Envoie du spool en cours
            SpoolName = CreateDetail(aSpool, -1)
        End If
        
        If OrigineMail = 0 Or OrigineMail = 2 Then
            '-> Envoie dans OutLook
            SendToOutLook SpoolName
        Else
            '-> Envoie dans Internet via MAPI
            SendToInternet SpoolName
        End If
        
    Case 2 '-> Envoie du fichier en cours
        If OrigineMail = 0 Or OrigineMail = 2 Then
            '-> Envoie du spool en entier vers OutLook
            SendToOutLook aFichier.FileName
        Else
            '-> Envoie du spool vers Internet
            SendToInternet aFichier.FileName
        End If
    
End Select

GestError:


End Sub

Public Function CreateDetail(ByRef aSpool As Spool, ByVal Page As Integer, Optional PageMin As Integer, Optional PageMax As Integer) As String

'---> Cette fonction cr�er un fichier tempo et retourne le nom du fichier � linker

Dim TempFile As String
Dim hdlFile As Integer
Dim DefMaq As String
Dim i As Integer
Dim PageMini As Integer
Dim PageMaxi As Integer

On Error GoTo GestError

'-> Obtenir un nom de fichier tempotaire
TempFile = Year(Now) & "-" & Format(Month(Now), "00") & "-" & Format(Day(Now), "00") & "-" & Format(Hour(Now), "00") & "-" & Format(Minute(Now), "00") & "-" & Format(Second(Now), "00") & ".turbo"
TempFile = GetTempFileNameVB("WWW", True) & TempFile

'-> Obtenir un handle de fichier
hdlFile = FreeFile

'-> Ouverture du fichier ascii et �criture des pages
Open TempFile For Output As #hdlFile

'-> Ecriture de la balise Spool
If IsCryptedFile Then
    Print #hdlFile, Crypt("[SPOOL]")
Else
    Print #hdlFile, "[SPOOL]"
End If

'-> Il faut dans un premier temps �crire la maquette
If IsCryptedFile Then
    Print #hdlFile, Crypt("[MAQ]")
Else
    Print #hdlFile, "[MAQ]"
End If

'-> R�cup�ration de la maquette
DefMaq = aSpool.GetMaq

'-> Impression de la maquette
For i = 1 To NumEntries(DefMaq, Chr(0))
    If IsCryptedFile Then
        Print #hdlFile, Crypt(Entry(i, DefMaq, Chr(0)))
    Else
        Print #hdlFile, Entry(i, DefMaq, Chr(0))
    End If
Next 'Pour toutes les lignes de la maquette

'-> Tag de fin de maquette
If IsCryptedFile Then
    Print #hdlFile, Crypt("[/MAQ]")
Else
    Print #hdlFile, "[/MAQ]"
End If

'-> Impression du spool en entier
If Page = -1 Or Page = -2 Then
    If Page = -1 Then
        '-> Indiquer la premi�re page � imprimer
        If aSpool.IsSelectionPage = True Then
            '-> indiquer qu'il faut impimer � partir de la page 0
            PageMini = 0
        Else
            '-> Imprimer � partir de la page 1
            PageMini = 1
        End If
        
        '-> Indiquer la page maxi
        PageMaxi = aSpool.NbPage
        
    Else
        '-> Indiquer la premi�re page � imprimer
        PageMini = PageMin
        PageMaxi = PageMax
        
    End If
    '-> On imprimer toutes les pages
    For i = PageMini To PageMaxi
        '-> Imprimer la tag d'ouverture de la page de s�lection
        If i = 0 Then
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[GARDEOPEN]")
            Else
                Print #hdlFile, "[GARDEOPEN]"
            End If
        End If
        
        '-> Impression de la page
        PrintPageToSpool aSpool, hdlFile, i
        
        '-> Imprimer le tag de fin de s�lection si page = 0
        If i = 0 Then
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[GARDECLOSE]")
            Else
                Print #hdlFile, "[GARDECLOSE]"
            End If
        '-> Imprimer un saut de page si pas derni�re page
        ElseIf i <> aSpool.NbPage Then
            If IsCryptedFile Then
                Print #hdlFile, Crypt("[PAGE]")
            Else
                Print #hdlFile, "[PAGE]"
            End If
        End If
    Next
Else
    '-> Imprimer la tag de la page de s�lection
    If Page = 0 Then
        If IsCryptedFile Then
            Print #hdlFile, Crypt("[GARDEOPEN]")
        Else
            Print #hdlFile, "[GARDEOPEN]"
        End If
    End If

    '-> Imprimer que la page d�sir�e
    PrintPageToSpool aSpool, hdlFile, Page
    
    '-> Imprimer la tag de fermture de la page de s�lection
    If Page = 0 Then
        If IsCryptedFile Then
            Print #hdlFile, Crypt("[GARDECLOSE]")
        Else
            Print #hdlFile, "[GARDECLOSE]"
        End If
    End If
    
End If

'-> Tag de fin de spool
If IsCryptedFile Then
    Print #hdlFile, Crypt("[/SPOOL]")
Else
    Print #hdlFile, "[/SPOOL]"
End If

'-> Fermer le fichier ouvert
Close #hdlFile

'-> Renvoyer le nom du fichier
CreateDetail = TempFile

Exit Function

GestError:

    '-> Renovyer une valeur ""
    CreateDetail = ""

End Function

Private Sub PrintPageToSpool(ByRef aSpool As Spool, ByVal hdlFile As Integer, ByVal PageToPrint As Integer)

'---> Cette fonction imprime le contenu d'une page d'un spool

Dim i As Integer
    
Dim DefPage As String

'-> Recup�ration de la d�finition de la page
DefPage = aSpool.GetPage(PageToPrint)

'-> Impression de la page
For i = 1 To NumEntries(DefPage, Chr(0))
    If IsCryptedFile Then
        Print #hdlFile, Crypt(Entry(i, DefPage, Chr(0)))
    Else
        Print #hdlFile, Entry(i, DefPage, Chr(0))
    End If
Next

End Sub

Public Sub DeleteFile(FileToDelete As String, ActionFlag As Long)
 
'---> Cette proc�dure supprime un fichier

Dim SHFileOp As SHFILEOPSTRUCT
Dim Res As Long
 
'-> Ajouter un caractere de fin de string
FileToDelete = FileToDelete & Chr$(0)
 
'-> Setting de la structure
With SHFileOp
   .wFunc = FO_DELETE
   .pFrom = FileToDelete
   .fFlags = ActionFlag
End With
 
'-> suppression du fichier
Res = SHFileOperation(SHFileOp)

'-> Si on n' a pas fait annuler : virer le fichier de l'interface
If SHFileOp.fAborted = False Then CloseFichier MDIMain.TreeNaviga.SelectedItem.Key

End Sub

Public Sub DrawTempo(aPic As PictureBox)

aPic.Line (0, 0)-((TailleLue / TailleTotale) * aPic.ScaleWidth, aPic.ScaleHeight), &HC00000, BF

End Sub
