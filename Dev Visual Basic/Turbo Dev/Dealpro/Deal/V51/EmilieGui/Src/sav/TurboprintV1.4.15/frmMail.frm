VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmMail 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7320
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6120
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7320
   ScaleWidth      =   6120
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   5775
      Left            =   60
      TabIndex        =   3
      Top             =   1440
      Width           =   6015
      _ExtentX        =   10610
      _ExtentY        =   10186
      _Version        =   393216
      TabHeight       =   882
      WordWrap        =   0   'False
      TabPicture(0)   =   "frmMail.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Image1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Image2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Image5"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Image4(1)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "strUrl"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "strObjet"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "strMessage"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).ControlCount=   7
      TabPicture(1)   =   "frmMail.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Shape2"
      Tab(1).Control(1)=   "Shape1"
      Tab(1).Control(2)=   "Image6"
      Tab(1).Control(3)=   "Image11"
      Tab(1).Control(4)=   "Image10"
      Tab(1).Control(5)=   "Image9"
      Tab(1).Control(6)=   "Image8"
      Tab(1).Control(7)=   "Image7"
      Tab(1).Control(8)=   "Image4(0)"
      Tab(1).Control(9)=   "RichTextBox1"
      Tab(1).ControlCount=   10
      TabPicture(2)   =   "frmMail.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame2"
      Tab(2).Control(1)=   "Check1"
      Tab(2).Control(2)=   "Shape3"
      Tab(2).ControlCount=   3
      Begin VB.Frame Frame2 
         Height          =   3975
         Left            =   -74520
         TabIndex        =   10
         Top             =   1320
         Width           =   5055
         Begin VB.Frame Frame4 
            Enabled         =   0   'False
            Height          =   1815
            Left            =   360
            TabIndex        =   15
            Top             =   2040
            Width           =   4455
            Begin VB.CheckBox Check2 
               Height          =   255
               Left            =   240
               TabIndex        =   19
               Top             =   1440
               Value           =   1  'Checked
               Width           =   4095
            End
            Begin VB.OptionButton Option4 
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   18
               Top             =   360
               Width           =   3735
            End
            Begin VB.OptionButton Option5 
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   17
               Top             =   720
               Value           =   -1  'True
               Width           =   3975
            End
            Begin VB.OptionButton Option6 
               CausesValidation=   0   'False
               Enabled         =   0   'False
               Height          =   255
               Left            =   240
               TabIndex        =   16
               Top             =   1080
               Width           =   2415
            End
         End
         Begin VB.PictureBox PicHtml 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   360
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   13
            Top             =   1200
            Width           =   735
            Begin VB.Image Image12 
               Height          =   480
               Left            =   120
               Picture         =   "frmMail.frx":0054
               Top             =   50
               Width           =   480
            End
         End
         Begin VB.PictureBox picTurbo 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   360
            ScaleHeight     =   585
            ScaleWidth      =   705
            TabIndex        =   11
            Top             =   480
            Width           =   735
            Begin VB.Image Image3 
               Height          =   480
               Left            =   120
               Picture         =   "frmMail.frx":091E
               Top             =   50
               Width           =   480
            End
         End
         Begin VB.Shape Sel 
            BorderColor     =   &H000000FF&
            BorderWidth     =   2
            Height          =   655
            Left            =   350
            Top             =   465
            Width           =   775
         End
         Begin VB.Label Label2 
            Height          =   255
            Left            =   1320
            TabIndex        =   14
            Top             =   1395
            Width           =   3255
         End
         Begin VB.Label Label1 
            Height          =   255
            Left            =   1320
            TabIndex        =   12
            Top             =   675
            Width           =   3375
         End
      End
      Begin VB.CheckBox Check1 
         Height          =   195
         Left            =   -74520
         TabIndex        =   9
         Top             =   960
         Width           =   2055
      End
      Begin RichTextLib.RichTextBox RichTextBox1 
         Height          =   3855
         Left            =   -73920
         TabIndex        =   7
         Top             =   1800
         Width           =   4590
         _ExtentX        =   8096
         _ExtentY        =   6800
         _Version        =   393217
         Enabled         =   -1  'True
         TextRTF         =   $"frmMail.frx":11E8
      End
      Begin VB.TextBox strMessage 
         Height          =   3615
         Left            =   1200
         MultiLine       =   -1  'True
         TabIndex        =   6
         Top             =   1920
         Width           =   4455
      End
      Begin VB.TextBox strObjet 
         Height          =   285
         Left            =   1200
         TabIndex        =   5
         Top             =   1440
         Width           =   4455
      End
      Begin VB.TextBox strUrl 
         Height          =   285
         Left            =   1200
         TabIndex        =   4
         Top             =   840
         Width           =   4455
      End
      Begin VB.Shape Shape3 
         Height          =   4815
         Left            =   -74760
         Top             =   720
         Width           =   5535
      End
      Begin VB.Image Image4 
         Height          =   480
         Index           =   1
         Left            =   240
         Picture         =   "frmMail.frx":126A
         ToolTipText     =   "Envoyer"
         Top             =   5040
         Width           =   480
      End
      Begin VB.Image Image4 
         Height          =   480
         Index           =   0
         Left            =   -74760
         Picture         =   "frmMail.frx":1574
         ToolTipText     =   "Envoyer"
         Top             =   5040
         Width           =   480
      End
      Begin VB.Image Image7 
         Height          =   480
         Left            =   -73200
         Picture         =   "frmMail.frx":187E
         ToolTipText     =   "Police"
         Top             =   840
         Width           =   480
      End
      Begin VB.Image Image8 
         Height          =   480
         Left            =   -72720
         Picture         =   "frmMail.frx":1B88
         ToolTipText     =   "Couleur de la police"
         Top             =   840
         Width           =   480
      End
      Begin VB.Image Image9 
         Height          =   480
         Left            =   -71880
         Picture         =   "frmMail.frx":1E92
         ToolTipText     =   "Gauche"
         Top             =   840
         Width           =   480
      End
      Begin VB.Image Image10 
         Height          =   480
         Left            =   -71400
         Picture         =   "frmMail.frx":219C
         ToolTipText     =   "Centrer"
         Top             =   840
         Width           =   480
      End
      Begin VB.Image Image11 
         Height          =   480
         Left            =   -70920
         Picture         =   "frmMail.frx":24A6
         ToolTipText     =   "Droite"
         Top             =   840
         Width           =   480
      End
      Begin VB.Image Image6 
         Height          =   480
         Left            =   -74640
         Picture         =   "frmMail.frx":27B0
         ToolTipText     =   "Corps du message"
         Top             =   1920
         Width           =   480
      End
      Begin VB.Shape Shape1 
         BackStyle       =   1  'Opaque
         Height          =   735
         Left            =   -73320
         Top             =   720
         Width           =   3015
      End
      Begin VB.Image Image5 
         Height          =   480
         Left            =   360
         Picture         =   "frmMail.frx":2ABA
         ToolTipText     =   "Corps du message"
         Top             =   1920
         Width           =   480
      End
      Begin VB.Image Image2 
         Height          =   480
         Left            =   360
         Picture         =   "frmMail.frx":2DC4
         ToolTipText     =   "Sujet"
         Top             =   1320
         Width           =   480
      End
      Begin VB.Image Image1 
         Height          =   480
         Left            =   360
         MouseIcon       =   "frmMail.frx":30CE
         MousePointer    =   99  'Custom
         Picture         =   "frmMail.frx":33D8
         ToolTipText     =   "Destinataire"
         Top             =   720
         Width           =   480
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H00404040&
         BackStyle       =   1  'Opaque
         Height          =   735
         Left            =   -73260
         Top             =   780
         Width           =   3015
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   6015
      Begin VB.OptionButton Option3 
         Height          =   255
         Left            =   2760
         TabIndex        =   8
         Top             =   360
         Value           =   -1  'True
         Width           =   2415
      End
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   4560
         Top             =   600
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   5160
         Top             =   360
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   2
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMail.frx":36E2
               Key             =   "WWW"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmMail.frx":39FC
               Key             =   "OUTLOOK"
            EndProperty
         EndProperty
      End
      Begin VB.OptionButton Option2 
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   720
         Width           =   2415
      End
      Begin VB.OptionButton Option1 
         Height          =   195
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   2295
      End
   End
End
Attribute VB_Name = "frmMail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Fichier � imprimer
Public aSpool As Spool
Public FromVisu As Boolean
Public lvSend As Integer
Private FormatHtml As Boolean

Private Sub Form_Load()

Dim aLb As Libelle

On Error Resume Next

'-> Gestion des messages
Set aLb = Libelles("FRMMAIL")

Me.Caption = aLb.GetCaption(1)
Me.Frame1.Caption = aLb.GetCaption(2)
Me.Option1.Caption = aLb.GetCaption(3)
Me.Option2.Caption = aLb.GetCaption(4)
Me.Check1.Caption = aLb.GetCaption(5)
Me.SSTab1.TabCaption(0) = "     " & aLb.GetCaption(6)
Me.SSTab1.TabCaption(1) = "     " & aLb.GetCaption(7)
Me.Image1.ToolTipText = aLb.GetCaption(8)
Me.Image2.ToolTipText = aLb.GetCaption(9)
Me.Image5.ToolTipText = aLb.GetCaption(10)
Me.Image6.ToolTipText = Me.Image5.ToolTipText
Me.Image4(0).ToolTipText = aLb.GetCaption(11)
Me.Image4(1).ToolTipText = aLb.GetCaption(11)
Me.Image7.ToolTipText = aLb.GetCaption(12)
Me.Image8.ToolTipText = aLb.GetCaption(13)
Me.Image9.ToolTipText = aLb.GetCaption(14)
Me.Image10.ToolTipText = aLb.GetCaption(15)
Me.Image11.ToolTipText = aLb.GetCaption(16)
Me.Option3.Caption = aLb.GetCaption(18)
Me.SSTab1.TabCaption(2) = aLb.GetCaption(19)
Me.Frame2.Caption = aLb.GetCaption(20)
Me.Label1.Caption = aLb.GetCaption(21)
Me.Label2.Caption = aLb.GetCaption(22)
Me.Frame4.Caption = aLb.GetCaption(23)
Me.Option4.Caption = aLb.GetCaption(24)
Me.Option5.Caption = aLb.GetCaption(25)
Me.Option6.Caption = aLb.GetCaption(26)
Me.Check2.Caption = aLb.GetCaption(27)

'-> Charger les images
Set Me.SSTab1.TabPicture(0) = Me.ImageList1.ListImages("WWW").Picture
Set Me.SSTab1.TabPicture(1) = Me.ImageList1.ListImages("OUTLOOK").Picture

'-> S�lectionner le premier onglet
Me.SSTab1.Tab = 0

'-> Bloquer l'onglet OutLook si pas disponible
Me.SSTab1.TabEnabled(1) = OutLookOk

''-> si on vient du menu, bloquer les options
If Not FromVisu Then
    Select Case Me.lvSend
        Case 0 'Fichier
            Me.Option1.Value = False
            Me.Option2.Value = False
            Me.Option3.Value = True
        Case 1 'Spool
            Me.Option1.Value = False
            Me.Option2.Value = True
            Me.Option3.Value = False
        Case 2 'Page
            Me.Option1.Value = True
            Me.Option2.Value = False
            Me.Option3.Value = False

    End Select
    Me.Option1.Enabled = False
    Me.Option2.Enabled = False
    Me.Option3.Enabled = False
End If


End Sub

Private Sub Image1_Click()

On Error GoTo GestError

'---> Initialiser la session MAPI
Call InitMAPISession

'-> Afficher le carnet d'adresse
frmLib.MAPIMessages1.Compose
frmLib.MAPIMessages1.Show False

'-> Afficher le destinataire si saisi
If frmLib.MAPIMessages1.RecipAddress <> "" Then Me.strUrl = frmLib.MAPIMessages1.RecipAddress

GestError:

End Sub


Private Sub Image10_Click()

Me.RichTextBox1.SelAlignment = 2

End Sub

Private Sub Image11_Click()

Me.RichTextBox1.SelAlignment = 1

End Sub

Private Sub Image12_Click()
    PicHtml_Click
End Sub

Private Sub Image3_Click()
    picTurbo_Click
End Sub

Private Sub Image4_Click(Index As Integer)

'---> Envoyer un message vers Internet

Dim aLb As Libelle

On Error Resume Next

'-> V�rifier l'URL si on envoie sur WWW
If Index = 1 Then
    '-> V�rifier sue l'url soit saisi
    If Trim(Me.strUrl.Text) = "" Then
        '-> pointer sur la classe message
        Set aLb = Libelles("FRMMAIL")
        MsgBox aLb.GetCaption(17), vbExclamation + vbOKOnly, aLb.GetToolTip(17)
        Me.strUrl.SetFocus
        Exit Sub
    End If
End If

'-> Indiquer si on crypt le fichier
If Me.Check1.Value = 1 Then
    IsCryptedFile = True
Else
    IsCryptedFile = False
End If

'-> Ne mettre � jour les variables de traitement que si on vient de la visu et pas du menu
If FromVisu Then
    '-> Cl� du spool et du fichier
    SpoolKeyMail = aSpool.Key
    FileKeyMail = Trim(UCase$(aSpool.FileName))
    '-> Nature de l'export
    If Me.Option1.Value Then '-> Export de la page
        PageNumMail = aSpool.CurrentPage
        ExportMail = 0
    ElseIf Me.Option2.Value Then '-> Export du spool
        ExportMail = 1
    Else '-> Export du fichier
        ExportMail = 2
    End If
End If

'-> Dans tous les cas, indiquer l'origine de l'envoie
If Index = 1 Then
    OrigineMail = 1
    '->NETPARAM : URL + chr(0) + OBJET + chr(0) + BODY
    NetParam = Me.strUrl.Text & Chr(0) & Me.strObjet.Text & Chr(0) & Me.strMessage.Text
Else
    OrigineMail = 0
End If
    
'-> Afficher le mail dans tous les cas si on va vers OutLook
SendOutLook = False

'-> Positionner les variables d'environnement d'export HTML
If FormatHtml Then
    '-> Positionnement de la navigationHTML
    If Me.Option4.Value Then
        NavigationType = 0 'FrameSet
    ElseIf Me.Option5.Value Then
        NavigationType = 1 'Fichier unique
    Else
        NavigationType = 2 'Pages
    End If
    
    '-> Inclure le num�ro de page
    IncluseSpoolNumber = CBool(Me.Check2.Value)
    
    '-> Envoyer le mail
    If Index = 1 Then
        '-> Envoie sur Internet
        CreateHTMLToMessagerie False
    Else
        '-> Envoie sur OutLook
        CreateHTMLToMessagerie True
    End If
    
Else
    '->Envoyer sur le r�seau
    CreateFileToSend
End If

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Image7_Click()

Me.CommonDialog1.FontName = Me.RichTextBox1.Font.Name
Me.CommonDialog1.FontBold = Me.RichTextBox1.Font.Bold
Me.CommonDialog1.FontItalic = Me.RichTextBox1.Font.Italic
Me.CommonDialog1.FontSize = Me.RichTextBox1.Font.Size
Me.CommonDialog1.Flags = cdlCFBoth
Me.CommonDialog1.ShowFont

Me.RichTextBox1.SelFontName = Me.CommonDialog1.FontName
Me.RichTextBox1.SelBold = Me.CommonDialog1.FontBold
Me.RichTextBox1.SelItalic = Me.CommonDialog1.FontItalic
Me.RichTextBox1.SelFontSize = Me.CommonDialog1.FontSize

End Sub

Private Sub Image8_Click()


Me.CommonDialog1.ShowColor
Me.RichTextBox1.SelColor = Me.CommonDialog1.Color

End Sub

Private Sub Image9_Click()

Me.RichTextBox1.SelAlignment = 0

End Sub

Private Sub Label1_Click()
    picTurbo_Click
End Sub

Private Sub Label2_Click()
    PicHtml_Click
End Sub


Private Sub PicHtml_Click()

Me.Sel.Left = Me.PicHtml.Left - 20
Me.Sel.Top = Me.PicHtml.Top - 20
FormatHtml = True
Me.Check1.Value = 0
Me.Check1.Enabled = False

'-> Bloquer la gestion Internet
Me.Frame4.Enabled = True
Me.Option4.Enabled = True
Me.Option5.Enabled = True
Me.Option6.Enabled = True


End Sub

Private Sub picTurbo_Click()

Me.Sel.Left = Me.picTurbo.Left - 20
Me.Sel.Top = Me.picTurbo.Top - 20
FormatHtml = False
Me.Check1.Enabled = True

'-> Bloquer la gestion Internet
Me.Frame4.Enabled = False
Me.Option4.Enabled = False
Me.Option5.Enabled = False
Me.Option6.Enabled = False


End Sub

