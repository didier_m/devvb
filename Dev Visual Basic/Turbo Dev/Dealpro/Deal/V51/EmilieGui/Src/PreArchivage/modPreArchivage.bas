Attribute VB_Name = "modPreArchivage"
Option Explicit


'---> Variables issues de la lecture du fichier d'initialisation
Public LogPath As String 'R�pertoire des logs
Public ScriptPath As String 'R�pertoire des scripts
Public Scripts As Collection 'Collection des scripts ouverts

'***    API WIN32   ***

'-> API de lecture des fichiers au format "*.ini"
Public Declare Function GetPrivateProfileSection& Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)

'-> API Pour gestion du temps
Public Declare Function GetTickCount& Lib "kernel32" ()

'-> Liste des champs sp�ciaux utilis�s pour la cr�ation des spools et des noms
'%JOUR%  -> Jour en cours sur 2
'%MOIS%  -> Mois en cours sur 2
'%ANNEE% -> Ann�e en cours sur 4
'%ANNEECRS% -> Ann�e en cours sur 2
'%HEURE% -> Heure en cours sur 2
'%MINUTE% -> Minute en cours sur 2
'%SECONDE% -> Seconde en cours sur 2
'%TICKS% -> Chrono
Public Const SpeTypeRef = "%JOUR%MOIS%ANNEE%ANNEECRS%HEURE%MINUTE%SECONDE%TICKS%"



Public Function GetSpeTypeInString(strRef As String) As String

'---> Cette fonction recherche dans une chaine les mots cl�s pr�definis
' et les remplace par leur valeur

Dim iCour As Integer
Dim iStart As Integer
Dim iEnd As Integer
Dim keyWord As String

Do
    '-> Calcul des positions
    iStart = InStr(1, strRef, "%")
    If iStart = 0 Then Exit Do
    iEnd = InStr(iStart + 1, strRef, "%")
    If iEnd = -1 Then Exit Do
    '-> R�cup�ration de la valeur
    keyWord = Mid$(strRef, iStart, iEnd - iStart + 1)
    '-> Remplacer sa valeur
    strRef = Replace(strRef, keyWord, GetSpeKeywordValue(keyWord))
Loop

'-> Retourner la valeur
GetSpeTypeInString = strRef


End Function

Private Function GetSpeKeywordValue(keyWord As String) As String

'---> Cette fonction retourne la valeur d'un mot cle particulier
Select Case UCase$(Trim(keyWord))
    Case "%ANNEE%"
        GetSpeKeywordValue = Format(Year(Now), "0000")
    Case "%ANNEECRS%"
        GetSpeKeywordValue = Format(Year(Now), "00")
    Case "%MOIS%"
        GetSpeKeywordValue = Format(Month(Now), "00")
    Case "%JOUR%"
        GetSpeKeywordValue = Format(Day(Now), "00")
    Case "%HEURE%"
        GetSpeKeywordValue = Format(Hour(Now), "00")
    Case "%MINUTE%"
        GetSpeKeywordValue = Format(Minute(Now), "00")
    Case "%SECONDE%"
        GetSpeKeywordValue = Format(Second(Now), "00")
    Case "%TICKS%"
        
End Select

End Function

Public Function GetIniString(AppName As String, lpKeyName As String, IniFile As String, Error As Boolean) As String

Dim Res As Long
Dim lpBuffer As String

'-> Fonction r�cup�re une chaine de caract�re dans un fichier d'initialisation
'-> Max 2000 Octets
'-> Retourne NULL si on trouve ou erreur

On Error GoTo GestError

lpBuffer = Space$(2000)
Res = GetPrivateProfileString(AppName, lpKeyName, "NULL", lpBuffer, Len(lpBuffer), IniFile)
lpBuffer = Entry(1, lpBuffer, Chr(0))
If lpBuffer = "NULL" Then
    If Error Then
        MsgBox "Impossible de trouver la cl� : " & lpKeyName & vbCrLf & _
               "dans la section : " & AppName & vbCrLf & "du fichier : " & _
                IniFile, vbCritical + vbOKOnly, "Erreur de lecture"
    End If
Else
    lpBuffer = Mid$(lpBuffer, 1, Res)
End If

GetIniString = lpBuffer

Exit Function

GestError:
    MsgBox "Erreur dans la lecture de la cl� : " & lpKeyName & vbCrLf & _
               "dans la section : " & AppName & vbCrLf & "du fichier : " & _
                IniFile & vbCrLf & "Erreur N� : " & Err.Number & " - " & Err.Description, vbCritical + vbOKOnly, "Erreur de lecture"
    GetIniString = "NULL"
End Function
Public Function DirecoryExist(DirName As String) As Boolean

'---> Fonction qui v�rifie l'existence d'un r�pertoire

On Error GoTo GestError

If (GetAttr(DirName) And vbDirectory) Then
    DirecoryExist = True
Else
GestError:
    DirecoryExist = False
End If

End Function

Public Function FileExist(Fichier As String) As Boolean

'---> Fonction qui v�rifie l'existence d'un fichier
On Error GoTo GestError

If Dir$(Fichier) <> "" Then FileExist = True
Exit Function

GestError:
    FileExist = False

End Function
Public Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Long
Dim PosEnCour As Long
Dim i As Long
Dim CHarDeb As Long
Dim CharEnd As Long

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
    

End Function

Public Sub SelectTextBox(ByRef oText As TextBox)

'---> Proc�dure qui s�lectionne le contenu d'une zone de texte

On Error Resume Next
oText.SelStart = 0
oText.SelLength = Len(oText.Text)

End Sub

Public Function IsLegalName(ByVal NewName As String, Optional Tiret As Boolean) As Boolean

'---> Fonction qui v�rifie le contenu d'un nom pour y supprimer tous les caract�res interdits
' Caract�res interdits : \ / : * " < > | -

Dim FindBad As Boolean

If InStr(1, NewName, "\") <> 0 Then FindBad = True
If InStr(1, NewName, "/") <> 0 Then FindBad = True
If InStr(1, NewName, ":") <> 0 Then FindBad = True
If InStr(1, NewName, "*") <> 0 Then FindBad = True
If InStr(1, NewName, """") <> 0 Then FindBad = True
If InStr(1, NewName, "<") <> 0 Then FindBad = True
If InStr(1, NewName, ">") <> 0 Then FindBad = True
If InStr(1, NewName, "|") <> 0 Then FindBad = True
If Tiret And InStr(1, NewName, "-") <> 0 Then FindBad = True

IsLegalName = Not FindBad

End Function

Public Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Variant

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Long
Dim i As Long
Dim PosAnalyse As Long

On Error GoTo GestError

PosAnalyse = 1

Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1

Exit Function

GestError:

    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
    

End Function

