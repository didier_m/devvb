VERSION 5.00
Begin VB.MDIForm MainApp 
   Appearance      =   0  'Flat
   BackColor       =   &H8000000C&
   Caption         =   "Gestionnaire de scripts"
   ClientHeight    =   8175
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   9705
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   2  'CenterScreen
   Begin VB.Menu mnuFile 
      Caption         =   "Fichier"
      Begin VB.Menu menuOpenScript 
         Caption         =   "Ouvrir un script"
         Shortcut        =   ^O
      End
      Begin VB.Menu mnuCreateScript 
         Caption         =   "Cr�er un script"
         Shortcut        =   ^N
      End
      Begin VB.Menu menuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnQuite 
         Caption         =   "Quitter"
         Shortcut        =   ^Q
      End
   End
End
Attribute VB_Name = "MainApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub MDIForm_Load()

'---> Point d'entr�e du programme

'-> Lecture du fichier d'initialisation et setting des variables
Call InitFromFile(App.Path & "\app.ini")

'-> Initialiser la collection des scripts
Set Scripts = New Collection

End Sub


Private Sub InitFromFile(FileName As String)

'---> Fonction effectue la lecture du fichier d'initialisation

'-> Existence du fichier
If Not FileExist(FileName) Then
    MsgBox "Impossible de trouver le fichier d'initialisation : " & vbCrLf & FileName, vbCritical + vbOKOnly, "Fin du programme"
    End
End If

'-> R�cup�ration du r�pertoire des logs
LogPath = GetIniString("PARAM", "LOGDIRECTORY", FileName, True)
If LogPath = "NULL" Then End
If Not DirecoryExist(LogPath) Then
    MsgBox "Impossible de trouver le r�pertoire des logs : " & vbCrLf & LogPath, vbCritical + vbOKOnly, "Fin du programme"
    End
End If

'-> R�cup�ration du r�pertoire des scripts
ScriptPath = GetIniString("PARAM", "SCRIPTDIRECTORY", FileName, True)
If ScriptPath = "NULL" Then End
If Not DirecoryExist(ScriptPath) Then
    MsgBox "Impossible de trouver le r�pertoire des scripts : " & vbCrLf & ScriptPath, vbCritical + vbOKOnly, "Fin du programme"
    End
End If



End Sub

Private Sub menuOpenScript_Click()

'---> Menu Ouverture des scripts


End Sub

Private Sub mnuCreateScript_Click()

Dim oScript As clsScript
Dim oFrm As FrmScript

'-> Feuille de saisie
Set oFrm = New FrmScript
Call oFrm.CreateNewScript
oFrm.Show

End Sub
