VERSION 5.00
Begin VB.Form FrmScript 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   6885
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7560
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6885
   ScaleWidth      =   7560
   Begin VB.Frame Frame1 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   2160
      TabIndex        =   3
      Top             =   1800
      Width           =   4935
      Begin VB.OptionButton ScriptTABLE 
         BackColor       =   &H80000005&
         Caption         =   "Ligne de tableau"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   120
         Width           =   2055
      End
      Begin VB.OptionButton ScriptTEXT 
         BackColor       =   &H80000005&
         Caption         =   "Zone de Texte"
         Height          =   255
         Left            =   2400
         TabIndex        =   5
         Top             =   120
         Width           =   2655
      End
   End
   Begin VB.OptionButton ScriptFormatTURBO 
      BackColor       =   &H80000005&
      Caption         =   "Format TURBO"
      Height          =   255
      Left            =   2280
      TabIndex        =   14
      Top             =   5880
      Width           =   2055
   End
   Begin VB.OptionButton ScriptFormatPDF 
      BackColor       =   &H80000005&
      Caption         =   "Format PDF"
      Height          =   255
      Left            =   4560
      TabIndex        =   28
      Top             =   5880
      Width           =   2655
   End
   Begin VB.CheckBox ScriptFileExist 
      BackColor       =   &H80000005&
      Caption         =   "Ecraser le fichier"
      Height          =   255
      Left            =   5280
      TabIndex        =   13
      Top             =   5040
      Width           =   1935
   End
   Begin VB.TextBox ScriptListeField 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2280
      TabIndex        =   7
      Text            =   "C:\travail\%ANNEE%\%MOIS%\%JOUR%\%%HEURE%\%MINUTE%\%SECONDE%"
      Top             =   2640
      Width           =   4935
   End
   Begin VB.TextBox ScriptObjectName 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2280
      TabIndex        =   6
      Top             =   2280
      Width           =   4935
   End
   Begin VB.TextBox ScriptExportFileName 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2280
      TabIndex        =   11
      Top             =   4680
      Width           =   2775
   End
   Begin VB.TextBox ScriptExportSuffixe 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2280
      TabIndex        =   12
      Top             =   5040
      Width           =   2775
   End
   Begin VB.TextBox ScriptExportPrefix 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2280
      TabIndex        =   10
      Top             =   4320
      Width           =   2775
   End
   Begin VB.CheckBox ScriptDirMiss 
      BackColor       =   &H80000005&
      Caption         =   "Cr�er s'il n'existe pas"
      Height          =   255
      Left            =   5280
      TabIndex        =   8
      Top             =   3480
      Width           =   1935
   End
   Begin VB.TextBox ScriptExportDirectory 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   360
      TabIndex        =   9
      Top             =   3840
      Width           =   6855
   End
   Begin VB.TextBox ScriptPath 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   285
      Left            =   2280
      TabIndex        =   2
      Top             =   1080
      Width           =   4935
   End
   Begin VB.TextBox ScriptDescro 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2280
      TabIndex        =   1
      Top             =   720
      Width           =   4935
   End
   Begin VB.TextBox ScriptName 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2280
      TabIndex        =   0
      Top             =   360
      Width           =   2775
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "Format d'export : "
      Height          =   255
      Left            =   360
      TabIndex        =   27
      Top             =   5880
      Width           =   2295
   End
   Begin VB.Shape Shape4 
      BackStyle       =   1  'Opaque
      Height          =   615
      Left            =   120
      Top             =   5640
      Width           =   7335
   End
   Begin VB.Label LinkCancel 
      Caption         =   "Annuler"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   5400
      MouseIcon       =   "FrmScript.frx":0000
      MousePointer    =   99  'Custom
      TabIndex        =   16
      Top             =   6360
      Width           =   855
   End
   Begin VB.Label LinkSave 
      Caption         =   "Enregistrer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   6480
      MouseIcon       =   "FrmScript.frx":0CCA
      MousePointer    =   99  'Custom
      TabIndex        =   15
      Top             =   6360
      Width           =   975
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "Champs de rupture :"
      Height          =   255
      Left            =   360
      TabIndex        =   26
      Top             =   2640
      Width           =   1695
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "Nom de l'objet : "
      Height          =   255
      Left            =   360
      TabIndex        =   25
      Top             =   2280
      Width           =   1335
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Type d'objet de rupture : "
      Height          =   255
      Left            =   360
      TabIndex        =   24
      Top             =   1920
      Width           =   2295
   End
   Begin VB.Shape Shape3 
      BackStyle       =   1  'Opaque
      Height          =   1455
      Left            =   120
      Top             =   1680
      Width           =   7335
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Nom du fichier :"
      Height          =   255
      Left            =   360
      TabIndex        =   23
      Top             =   4680
      Width           =   1455
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Pr�fixe :"
      Height          =   255
      Left            =   360
      TabIndex        =   22
      Top             =   4320
      Width           =   975
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Suffixe :"
      Height          =   255
      Left            =   360
      TabIndex        =   21
      Top             =   5040
      Width           =   975
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "R�pertoire d'export des fichiers : "
      Height          =   255
      Left            =   360
      TabIndex        =   20
      Top             =   3480
      Width           =   2295
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Emplacement du script : "
      Height          =   255
      Left            =   360
      TabIndex        =   19
      Top             =   1080
      Width           =   1815
   End
   Begin VB.Shape Shape2 
      BackStyle       =   1  'Opaque
      Height          =   2295
      Left            =   120
      Top             =   3240
      Width           =   7335
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Description du script : "
      Height          =   255
      Left            =   360
      TabIndex        =   18
      Top             =   720
      Width           =   1575
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nom du script : "
      Height          =   255
      Left            =   360
      TabIndex        =   17
      Top             =   360
      Width           =   1335
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   1455
      Left            =   120
      Top             =   120
      Width           =   7335
   End
End
Attribute VB_Name = "FrmScript"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> ObjectScript Affect�
Private oScript As clsScript

Public Sub CreateNewScript()

'---> Fonction appel�e par la fen�tre MDI lors de la cr�ation d'un nouveau script

'-> Nouvel Objet
Set oScript = New clsScript
Call Scripts.Add(oScript, "SCRIPT|" & Me.hWnd)

'-> Affichage
Call LoadScript

End Sub

Public Sub EditScript(FileName As String)

'---> Fonction appel�e par la fen�tre MDI lors de l'ouvertue d'un script existant

End Sub


Private Sub LoadScript()

'-> Proc�dure qui charge une feuille avec le script en cours

'-> Titre de la feuille
Me.Caption = oScript.ScriptName

'-> Header
Me.ScriptName = oScript.ScriptName
Me.ScriptDescro = oScript.ScriptDescription
Me.ScriptPath = oScript.ScriptFileName

'-> Rupture
If oScript.BreakingObjectType = "TEXT" Then
    Me.ScriptTEXT.Value = True
Else
    Me.ScriptTABLE.Value = True
End If
Me.ScriptObjectName.Text = oScript.BreakingObjectName
Me.ScriptListeField.Text = oScript.BreakingObjectField

'-> Export
Me.ScriptDirMiss.Value = Abs(CInt(oScript.CreateDirOnMissingDir))
Me.ScriptExportDirectory.Text = oScript.ExportDirectory
Me.ScriptExportPrefix.Text = oScript.ExportPrefix
Me.ScriptExportSuffixe.Text = oScript.ExportSuffix
Me.ScriptExportFileName.Text = oScript.ExportFileName
Me.ScriptFileExist.Value = Abs(CInt(oScript.OverhideOnExistingFile))

'-> Type d'export
If oScript.ExportFormat = "TURBO" Then
    Me.ScriptFormatTURBO.Value = True
Else
    Me.ScriptFormatPDF.Value = True
End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

'---> Enregistrement si necessaire THIERRY

End Sub

Private Sub Form_Unload(Cancel As Integer)

'---> Supprimer le script de la collection des scripts
Call Scripts.Remove("SCRIPT|" & Me.hWnd)

End Sub

Private Sub LinkCancel_Click()

'-> THIERRY




End Sub

Private Sub LinkSave_Click()

'---> Procedure de contr�le et d'enregistrement d'un script

Dim i As Integer
Dim Fault As Boolean
Dim FullName As String

'-> Nom de l'objet
If Trim(Me.ScriptName.Text) = "" Then
    MsgBox "Nom du script obligatoire", vbCritical + vbOKOnly, "Erreur"
    Me.ScriptName.SetFocus
    Exit Sub
End If
    
'-> V�rification de la structure du nom
If Not IsLegalName(Me.ScriptName.Text) Then
    MsgBox "Nom du script incorrect", vbCritical + vbOKOnly, "Erreur"
    Me.ScriptName.SetFocus
    Exit Sub
End If
        
'-> Nom de l'objet obligatoire
If Trim(Me.ScriptObjectName.Text) = "" Then
    MsgBox "Nom de l'objet de rupture", vbCritical + vbOKOnly, "Erreur"
    Me.ScriptObjectName.SetFocus
    Exit Sub
End If
    
'-> Liste des champs de rupture obligatoire
If Trim(Me.ScriptListeField.Text) = "" Then
    MsgBox "Veuillez sp�cifier au moins un champ de rupture", vbCritical + vbOKOnly, "Erreur"
    Me.ScriptListeField.SetFocus
    Exit Sub
End If

'-> Strucutre des champs
For i = 1 To NumEntries(Me.ScriptListeField.Text, ",")
    Fault = False
    If Not IsNumeric(Entry(i, Me.ScriptListeField.Text, ",")) Then Fault = True
    If Len(Entry(i, Me.ScriptListeField.Text, ",")) <> 4 Then Fault = True
    If Fault Then
        MsgBox "Tous les champs de rupture doivent �tre num�riques sur 4 caract�res", vbCritical + vbOKOnly, "Erreur"
        Me.ScriptListeField.SetFocus
        Exit Sub
    End If
Next

'-> R�pertoire des fichiers d'export
If Trim(Me.ScriptExportDirectory.Text) = "" Then
    MsgBox "Veuillez sp�cifier le r�pertoire d'export", vbCritical + vbOKOnly, "Erreur"
    Me.ScriptExportDirectory.SetFocus
    Exit Sub
End If
    
'-> Concataination de Prefixe + Nom + Suffixe
FullName = Trim(Me.ScriptExportPrefix.Text) & Trim(Me.ScriptExportFileName.Text) & Trim(Me.ScriptExportSuffixe.Text)
If Trim(FullName) = "" Then
    MsgBox "Nom de fichier d'export incorrect", vbCritical + vbOKOnly, "Erreur"
    Me.ScriptExportPrefix.SetFocus
    Exit Sub
End If

'-> Setting des donn�es dans l'objet
oScript.ScriptName = Trim(Me.ScriptName.Text)
oScript.ScriptDescription = Trim(Me.ScriptDescro.Text)
If Me.ScriptTABLE.Value Then
    oScript.BreakingObjectType = "TABLE"
Else
    oScript.BreakingObjectType = "TEXT"
End If
oScript.BreakingObjectName = Trim(UCase$(Me.ScriptObjectName.Text))
oScript.BreakingObjectField = Trim(Me.ScriptListeField.Text)
oScript.CreateDirOnMissingDir = Me.ScriptDirMiss.Value
oScript.ExportDirectory = Trim(Me.ScriptExportDirectory.Text)
oScript.ExportPrefix = Trim(Me.ScriptExportPrefix.Text)
oScript.ExportFileName = Trim(Me.ScriptExportFileName.Text)
oScript.ExportSuffix = Trim(Me.ScriptExportSuffixe.Text)
oScript.OverhideOnExistingFile = Me.ScriptFileExist.Value
If Me.ScriptFormatTURBO Then
    oScript.ExportFormat = "TURBO"
Else
    oScript.ExportFormat = "PDF"
End If
'-> S�rialisation de l'objet
'oScript.SaveScript ScriptPath ,

End Sub
Private Sub ScriptDescro_GotFocus()
Call SelectTextBox(Me.ScriptDescro)
End Sub
Private Sub ScriptExportDirectory_GotFocus()
Call SelectTextBox(Me.ScriptExportDirectory)
End Sub
Private Sub ScriptExportFileName_GotFocus()
Call SelectTextBox(Me.ScriptExportFileName)
End Sub
Private Sub ScriptListeField_GotFocus()
Call SelectTextBox(Me.ScriptListeField)
End Sub
Private Sub ScriptExportSuffixe_GotFocus()
Call SelectTextBox(Me.ScriptExportSuffixe)
End Sub
Private Sub ScriptName_GotFocus()
Call SelectTextBox(Me.ScriptName)
End Sub
Private Sub ScriptExportPrefix_GotFocus()
Call SelectTextBox(Me.ScriptExportPrefix)
End Sub
Private Sub ScriptObjectName_GotFocus()
Call SelectTextBox(Me.ScriptObjectName)
End Sub
