VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsScript"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'---> Classe de description d'un script

'*** PROPRIETES ***
Private OpenMode As Integer '0->Create , 1->Update
Public ScriptFileName As String
Private DateCreate As Date
Private DateLastSave As Date
Public ToSave As Boolean

'[HEADER]
Public ScriptName As String
Public ScriptDescription As String
'[BODY]
Public ExportDirectory As String
Public ExportFileName As String
Public ExportPrefix As String
Public ExportSuffix As String
Public ExportFormat As String 'TURBO,PDF
'[BREAK]
Public BreakingObjectType As String 'TEXT, TABLE
Public BreakingObjectName As String
Public BreakingObjectField As String 'Matrice s�para�e par des ","
'[CONFIRM]
Public CreateDirOnMissingDir As Boolean
Public OverhideOnExistingFile As Boolean

'*** METHODES ***
Public Function ReadScript(DirectoryName As String, FileName As String) As Boolean

'---> Fonction qui lit un fichier script et met � jour les propri�t�s


End Function

Public Function SaveScript(DirectoryName As String, FileName As String) As Boolean

'---> Fonction qui enregistre un fichier script

'-> Check si r�pertoire saisi

'-> Check si n om de fichier OK

'-> Check des mots cl�s : v�rifier si les mots cl�s saisie sont r�f�renc�s

End Function

Private Function EvalKeyWord(strToEval As String) As Boolean

'---> Cette fonction v�rifie si tous les mots cl�s saisis sont OK

End Function


Private Sub Class_Initialize()

'-> L'instanciation d'une nouvelle classe pose toujours le mode d'ouverture en cr�ate
OpenMode = 0
CreateDirOnMissingDir = True
OverhideOnExistingFile = True
ToSave = True
ScriptFileName = "**** Non enregistr� ***"
ScriptName = "Nouveau Script"
BreakingObjectType = "TEXT"
ExportFormat = "TURBO"

End Sub
