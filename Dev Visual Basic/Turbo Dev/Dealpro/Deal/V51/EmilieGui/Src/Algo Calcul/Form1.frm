VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   2655
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8055
   LinkTopic       =   "Form1"
   ScaleHeight     =   2655
   ScaleWidth      =   8055
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   495
      Left            =   1080
      TabIndex        =   1
      Top             =   1680
      Width           =   2175
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   480
      TabIndex        =   0
      Text            =   "=-(1800*(12*((5+-4)/-12)*2))-(12+56/-6)"
      Top             =   480
      Width           =   7215
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub Command1_Click()

Dim StringToAnalyse As String

StringToAnalyse = Me.Text1.Text
MsgBox Calcul(StringToAnalyse)

End Sub

Public Function Calcul(ByVal StringToAnalyse As String) As String

Dim Division As Integer
Dim Operat As String
Dim OpenP As Integer
Dim CloseP As Integer
Dim Ope1 As String
Dim Ope2 As String


'-> Il faut remplacer les parenth�ses par leur contenu
Do While InStrRev(StringToAnalyse, "(")

    '-> R�cup�rer la position de la derni�re parenth�ses ouverte
    OpenP = InStrRev(StringToAnalyse, "(")

    '-> R�cup�rer la position de la premi�re parenth�se ferm�e � partir de celle-ci
    CloseP = InStr(OpenP, StringToAnalyse, ")")
 
    '-> Supprimer les parenth�ses
    StringToAnalyse = ExcludeP(OpenP, CloseP, StringToAnalyse)
Loop

'-> Effectuer les "/"
StringToAnalyse = ExecCalculByOperande(StringToAnalyse, "/")

'-> Effectuer les "*"
StringToAnalyse = ExecCalculByOperande(StringToAnalyse, "*")

'-> Gestion du --
Do While InStr(1, StringToAnalyse, "--") <> 0
    Division = InStr(1, StringToAnalyse, "--")
    If Division = 2 Then
        StringToAnalyse = Replace(StringToAnalyse, "--", "", 1)
    Else
        StringToAnalyse = Replace(StringToAnalyse, "--", "+", Division)
    End If
Loop

'-> Effectuer les additions
StringToAnalyse = ExecCalculByOperande(StringToAnalyse, "+")

'-> Effectuer les soustractions
StringToAnalyse = ExecCalculByOperande(StringToAnalyse, "-")

Calcul = StringToAnalyse
End Function

Private Function ExecCalculByOperande(StrToCheck As String, Operande As String) As String


Dim Position As Integer
Dim Pos1 As Integer
Dim Pos2 As Integer
Dim Ope1 As String
Dim Ope2 As String

'-> Effectuer toutes les multiplications
Do While IsOperande(StrToCheck, Operande) <> 0
    '-> V�rifier dans un permier temps les multiplications
    Position = IsOperande(StrToCheck, Operande)
    '-> R�cup�rer la position du premier op�rande
    Ope1 = GetOperande1(Position, Pos1, StrToCheck)
    If Trim(Ope1) = "" Then Exit Do
    '-> R�cup�rer la position du second op�rande
    Ope2 = GetOperande2(Position, Pos2, StrToCheck)
    '-> Effectuer le calcul
    StrToCheck = Replace(StrToCheck, Mid$(StrToCheck, Pos1, Pos2 - Pos1 + 1), ExecCalcul(Ope1, Ope2, Operande))
Loop

ExecCalculByOperande = StrToCheck

End Function

Private Function ReplaceCalcul(ByVal PositionOpe As Integer, ByRef StrCible As String) As String

'---> Cette proc�dure remplace le contenu d'une parenth�se


Dim Ope1 As String
Dim Ope2 As String
Dim PosD As Integer
Dim PosF As Integer
Dim StrToReplace As String

'-> R�cup�rer les op�randes � analyser
Ope1 = GetOperande1(PositionOpe, PosD, StrCible)
Ope2 = GetOperande2(PositionOpe, PosF, StrCible)

'-> R�cup�rer la zone � remplacer
StrToReplace = Mid$(StrCible, PosD, PosF - PosD + 1)

'-> Effectuer le remplacement
ReplaceCalcul = Replace(StrCible, StrToReplace, ExecCalcul(Ope1, Ope2, Mid$(StrCible, PositionOpe, 1)))

End Function

Private Function ExcludeP(OpenP As Integer, CloseP As Integer, ByRef StrCible As String) As String

'---> Cette proc�dure supprime toute les parenth�ses et effectue le calcul interne

Dim StrToReplace As String
Dim i As Integer, j As Integer
Dim Ope1 As String
Dim Ope2 As String
Dim Operat As String
Dim OperatPos As Integer
Dim StrTempo As String
Dim FindMulti As Boolean

'-> R�cup�rer dans un premier temps la valeur � remplacer
StrToReplace = Mid$(StrCible, OpenP, CloseP - OpenP + 1)

'-> V�rifier si on trouve une op�rande interne
i = IsOperande(StrToReplace)
If i = 0 Then
    '-> Pas d'op�rande, remplacer simplement la valeur
    ExcludeP = Replace(StrCible, StrToReplace, Mid$(StrToReplace, 2, Len(StrToReplace) - 2))
    Exit Function
End If

'-> Garder en m�moire la valeur � recherhcher
StrTempo = StrToReplace

'-> Analyser le contenu des parenth�ses pour effecuer le calcul interne

Do
    '-> V�rifier les multiplications et les divisions
    OperatPos = IsOperande(StrToReplace, "/")
    If OperatPos = 0 Then OperatPos = IsOperande(StrToReplace, "*")
    If OperatPos = 0 Then OperatPos = IsOperande(StrToReplace)
    If OperatPos = 0 Then Exit Do
    Operat = Mid$(StrToReplace, OperatPos, 1)
    '-> R�cup�rer la position du premier op�rande
    Ope1 = GetOperande1(OperatPos, i, StrToReplace)
    '-> R�cup�rer la position du second op�rande
    Ope2 = GetOperande2(OperatPos, j, StrToReplace)
    If Ope1 = "" Then Exit Do
    '-> Effectuer le calcul
    StrToReplace = Replace(StrToReplace, Mid$(StrToReplace, i, j - i + 1), ExecCalcul(Ope1, Ope2, Operat))
Loop

'-> Supprimer les parenth�ses
StrToReplace = Mid$(StrToReplace, 2, Len(StrToReplace) - 2)

'-> Renvoyer la valeur de la chaine
ExcludeP = Replace(StrCible, StrTempo, StrToReplace)

End Function

Private Function GetOperande1(OpePos As Integer, ByRef PosD As Integer, ByRef StrCible As String) As String

'---> Cette fonction r�cup�rande la partie gauche d'une op�ration

'-> OpePos indique la position de l'�l�ment de caclcul

PosD = CheckChar(OpePos, -1, StrCible) + 1
GetOperande1 = Mid$(StrCible, PosD, OpePos - PosD)

End Function

Private Function GetOperande2(OpePos As Integer, ByRef PosF As Integer, ByRef StrCible As String) As String

'---> Cette fonction r�cup�rande la partie droite d'une op�ration

PosF = CheckChar(OpePos, 1, StrCible) - 1
GetOperande2 = Mid$(StrCible, OpePos + 1, PosF - OpePos)

End Function


Private Function ExecCalcul(Ope1 As String, Ope2 As String, Operation As String) As Double

Dim TempString As String
Dim P1 As String
Dim P2 As String
Dim P3 As String
Dim P4 As String
Dim Ope As Integer

If Not IsNumeric(Ope1) Or Not IsNumeric(Ope2) Then
    ExecCalcul = 0
    Exit Function
End If

Select Case Operation
    Case "+"
        TempString = CStr(CDbl(Ope1) + CDbl(Ope2))
    Case "-"
        TempString = CStr(CDbl(Ope1) - CDbl(Ope2))
    Case "*"
        TempString = CStr(CDbl(Ope1) * CDbl(Ope2))
    Case "/"
        TempString = CStr(CDbl(Ope1) / CDbl(Ope2))
End Select

'-> Si valeur de retout avec des exposants
If InStr(1, TempString, "E") <> 0 Then
    '-> R�cup�rer les op�randes
    P1 = Mid$(TempString, 1, InStr(1, TempString, "E") - 1)
    P2 = Mid$(TempString, InStr(1, TempString, "E") + 2, Len(TempString) - InStr(1, TempString, "E"))
    '-> Replacer la virgule
    P3 = Mid$(P1, 1, InStr(1, P1, ",") - 1)
    P4 = Mid$(P1, InStr(1, P1, ",") + 1, Len(P1) - InStr(1, P1, ",") - 1)
    '-> V�rifer si on a un signe n�gatif
    If CLng(P3) < 0 Then
        Ope = -1
        P3 = CStr(Abs(CLng(P3)))
    Else
        Ope = 1
    End If
    '-> Gestion de l'exposant
    P1 = "1" & String(CInt(P2), "0")
    P3 = P3 / CLng(P1) * Ope
    '-> Renvoyer la chaine totale
    TempString = P3 & P4
End If
    
'->Renvoyer la valeur
ExecCalcul = CDbl(TempString)


End Function

Private Function IsOperande(ByRef StrCheck As String, Optional OpToCheck As String) As Integer

'---> Cette fonction analyse si on trouve une op�rande dans une zone de texte

Dim i As Integer

'-> V�rifier si on passe un op�rande � rechercher dans un premier temps
If OpToCheck <> "" Then
    i = InStr(1, StrCheck, OpToCheck)
    IsOperande = i
    Exit Function
End If

'-> Recherche du signe +
i = InStr(1, StrCheck, "+")
If i <> 0 Then
    IsOperande = i
    Exit Function
End If
    
'-> Recherche du signe -
i = InStr(1, StrCheck, "-")
If i <> 0 Then
    IsOperande = i
    Exit Function
End If

'-> Recherche du signe *
i = InStr(1, StrCheck, "*")
If i <> 0 Then
    IsOperande = i
    Exit Function
End If

'-> Recherche du signe /
i = InStr(1, StrCheck, "/")
If i <> 0 Then
    IsOperande = i
    Exit Function
End If


End Function

Private Function CheckChar(ByVal Position As Integer, Sens As Integer, ByRef StrToCheck As String) As Integer


Dim i As Integer
Dim aChar As String

Dim aStep As Integer
Dim Limitte As Integer

If Sens = -1 Then
    aStep = -1
    Limitte = 1
Else
    aStep = 1
    Limitte = Len(StrToCheck)
End If

i = Position
Do While i <> Limitte
    aChar = Mid$(StrToCheck, i + aStep, 1)
    Select Case aChar
        Case "/", "+", "*", "-", "(", ")", "="
            If aStep = 1 Then '-> Test si on monte
                '-> Test pour les valeurs n�gatives
                If aChar = "-" And i = Position Then
                    i = i + aStep
                Else
                    CheckChar = i + aStep
                    Exit Function
                End If
            Else
                If i <> 1 Then
                    If aChar = "-" Then
                        '-> On teste le carcat�re d'apr�s pour voir sa nature
                        aChar = Mid$(StrToCheck, i - 1, 1)
                        Select Case aChar
                            Case "/", "+", "*", "-", "(", ")", "="
                                CheckChar = i - 2
                            Case Else
                                CheckChar = i + aStep
                        End Select
                    Else
                        CheckChar = i + aStep
                    End If
                Else
                    CheckChar = i + aStep
                End If
                Exit Function
            End If
        Case Else
            i = i + aStep
    End Select
Loop

If Sens = 1 Then Limitte = Limitte + 1
CheckChar = Limitte

End Function
