Attribute VB_Name = "fctDealPrinterPath"
Option Explicit

'-> Entete [PARAM]
Public IsSyncho As Boolean 'Indique si on acc�s � la synchro
Public IsLog As Boolean 'Indique si on a acc�s � la gestion des log
Public FilenameStop As String 'Indique le nom du fichier Par d�faut pour arret du batch

'-> Gestion des Erreurs [ERROR]
Public IsError As Boolean
Public ErrorDirectory As String
Public ExitOnBadPrinter As Boolean

'-> Synchronisation [SYNCHRO]
Public SynchroDirectory As String
Public SynchroFilename As String
Public SynchroDelai As Long

'-> Analyse des demandes d'impression [PRINT]
Public ScanDirectory As String
Public ScanExtension As String
Public ScanDelai As Long

'-> Pour gestion des log [LOG]
Public LogDirectory As String
Public LogFilename As String

'-> Pour gestion des paths
Public PathTurboFile As String
Public PathPdfFile As String

'-> Handle du fchier Log
Private hdlFile As Integer

'-> Si on utilise des imprimantes obligatoires
Public UsePrinter As Boolean
'-> Liste des imprimantes obligatoires
Public ListPrinter As String
'-> Fichier Ini
Public IniFileName As String

Sub Main()

'---> Point d'entr�e du programme

Dim Temp
Dim lpBuffer As String
Dim Res As Long
Dim X As Printer
Dim GetPid As String
Dim hdlProcess As Long
Dim Exitcode As Long
Dim strPrinters As String
Dim i As Integer
Dim DefPrinter As String
Dim IsBadPrinter As Boolean
Dim ErrorPrinter As Boolean

On Error Resume Next

'-> R�cup�rer le nom de la machine
lpBuffer = Space$(255)
Res = GetComputerName(lpBuffer, Len(lpBuffer))
lpBuffer = Entry(1, lpBuffer, Chr(0))

'-> Composer le nom du fichier Ini � analyser
IniFileName = LCase$(App.Path & "\dpb" & lpBuffer & ".ini")

'-> Tester si on trouve le fichier
If Dir$(IniFileName) = "" Then
    MsgBox "Impossible de trouver le fichier ini de param�trage : " & vbCrLf & IniFileName, vbCritical + vbOKOnly, "Erreur"
    End
End If

'-> Positionnement des valeurs par d�faut
IsSyncho = False
IsLog = False
FilenameStop = "DealPrinterBatch.Stop"
SynchroDelai = 60000  '1 mn
ScanExtension = "*.dpb"
ScanDelai = 10000 '10 secondes
LogDirectory = App.Path
LogFilename = "DealPrinterBatch.Log"

'**********
'* Entete *
'**********

'-> Synchro
Temp = GetIniString("PARAM", "SYNCHRO", IniFileName, False)
If Temp = "1" Then IsSyncho = True

'-> Log
Temp = GetIniString("PARAM", "LOG", IniFileName, False)
If Temp = "1" Then IsLog = True

'-> FilenameStop
Temp = GetIniString("PARAM", "FILENAMESTOP", IniFileName, False)
If Temp <> "" And IsLegalName(Temp) Then FilenameStop = Temp

'-> Path des fichiers Turbo
Temp = GetIniString("PARAM", "PATHTURBOFILE", IniFileName, False)
If Trim(Temp) <> "" Then
    If (GetAttr(Temp) And vbDirectory) = vbDirectory Then PathTurboFile = Temp
End If 'Si on a sp�cifi� un path pour les spools Turbo

'-> Path des fichiers Pdf
Temp = GetIniString("PARAM", "PATHPDFFILE", IniFileName, False)
If Trim(Temp) <> "" Then
    If (GetAttr(Temp) And vbDirectory) = vbDirectory Then PathPdfFile = Temp
End If 'Si on a sp�cifi� un path pour les spools Turbo

'***********************
'* Gestion des erreurs *
'***********************

'-> Exit on Bad Printer
Temp = GetIniString("ERROR", "EXITONBADPRINTER", IniFileName, False)
If Trim(Temp) = "1" Then ExitOnBadPrinter = True

'-> R�pertoire des erreurs
'-> R�pertoire d'�criture des log
Temp = GetIniString("ERROR", "DIRECTORY", IniFileName, False)
If Trim(Temp) = "" Then
    IsError = False
Else
    '-> Faire un replcae de la cl� $APP*
    Temp = Replace(Temp, "$APP$", App.Path)
    '-> V�rifier si on trouve le r�pertoire
    If (GetAttr(Temp) And vbDirectory) <> vbDirectory Then
        IsError = False
    Else
        ErrorDirectory = Temp
        If Right$(ErrorDirectory, 1) <> "\" Then ErrorDirectory = ErrorDirectory & "\"
        IsError = True
    End If
End If

'*******************
'* Synchronisation *
'*******************

'-> R�pertoire de synchronisation
Temp = GetIniString("SYNCHRO", "DIRECTORY", IniFileName, False)
If Trim(Temp) = "" Then
    IsSyncho = False
Else
    '-> V�rifier si on trouve le r�pertoire
    If (GetAttr(Temp) And vbDirectory) <> vbDirectory Then
        IsSyncho = False
    Else
        SynchroDirectory = Temp
        If Right$(SynchroDirectory, 1) <> "\" Then SynchroDirectory = SynchroDirectory & "\"
    End If
End If
        
'-> Fichier de synchronisation
Temp = GetIniString("SYNCHRO", "FILENAME", IniFileName, False)
If Trim(Temp) = "" Then
    IsSyncho = False
Else
    If Not IsLegalName(Temp) Then
        IsSyncho = False
    Else
        SynchroFilename = Temp
    End If
End If

'-> Delai de synchronisation
Temp = GetIniString("SYNCHRO", "DELAI", IniFileName, False)
If IsNumeric(Temp) Then SynchroDelai = CLng(Temp)

'********
'* Scan *
'********

'-> R�pertoire d'impression
Temp = GetIniString("SCAN", "DIRECTORY", IniFileName, False)
If Trim(Temp) = "" Then
    MsgBox "R�pertoire d'analyse des fichiers non renseign�", vbCritical + vbOKOnly, "Erreur"
    End
End If
'-> V�rifier si on trouve le r�pertoire
If (GetAttr(Temp) And vbDirectory) <> vbDirectory Then
    MsgBox "R�pertoire d'analyse des fichiers non valide : " & Chr(13) & Temp, vbCritical + vbOKOnly, "Erreur"
    End
End If
'-> Affecter la variable
ScanDirectory = Temp
If Right$(ScanDirectory, 1) <> "\" Then ScanDirectory = ScanDirectory & "\"

'-> Extension
Temp = GetIniString("SCAN", "EXTENSION", IniFileName, False)
If Trim(Temp) <> "" Then ScanExtension = Trim(Temp)

'-> Delai analyse
Temp = GetIniString("SCAN", "DELAI", IniFileName, False)
If IsNumeric(Temp) Then ScanDelai = CInt(Temp)
        
'*******
'* Log *
'*******

'-> R�pertoire d'�criture des log
Temp = GetIniString("LOG", "DIRECTORY", IniFileName, False)
If Trim(Temp) = "" Then
    IsLog = False
Else
    '-> V�rifier si on trouve le r�pertoire
    If (GetAttr(Temp) And vbDirectory) <> vbDirectory Then
        IsLog = False
    Else
        LogDirectory = Temp
        If Right$(LogDirectory, 1) <> "\" Then LogDirectory = LogDirectory & "\"
    End If
End If
        
'-> Fichier des log
Temp = GetIniString("LOG", "FILENAME", IniFileName, False)
If Trim(Temp) <> "" Then
    If Not IsLegalName(Temp) Then
        IsLog = False
    Else
        LogFilename = Temp
    End If
End If

'-> V�rifier si un Pid est sp�cifi� dans le fichier ini
GetPid = GetIniString("PID", "HANDLE", IniFileName, False)
If Trim(GetPid) <> "" Then
    '-> Ouverure du process sp�cifi�
    hdlProcess = OpenProcess(PROCESS_QUERY_INFORMATION, False, CLng(GetPid))
    '-> V�rifier si le PID est toujours valide
    GetExitCodeProcess hdlProcess, Exitcode
    If Exitcode = STILL_ACTIVE Then
        '-> Le process sp�cifi� est toujours actif : Iniquer dans le Log
        If IsLog Then
            '-> Ouverture du fichier Log
            hdlFile = FreeFile
            Open LogDirectory & LogFilename For Append As #hdlFile
            '-> Indiquer la date et heure
            Print #hdlFile, "**************************************"
            Print #hdlFile, "*                                    *"
            Print #hdlFile, "*           DealPrinterBatch         *"
            Print #hdlFile, "*                                    *"
            Print #hdlFile, "*                                    *"
            Print #hdlFile, "**************************************"
            Print #hdlFile, "****** Debut de session du programme DealPrintBatch ******"
            Print #hdlFile, "Date : " & Now
            '-> Fin de session
            Print #hdlFile, "****** Fin de session du programme DealPrintBatch ******"
            Print #hdlFile, "Programme d�ja en cours de travail"
            Print #hdlFile, "PID : " & GetPid
            '-> Fermer le fichier
            Close #hdlFile
            '-> Fin du programme
            End
        End If
    End If
End If 'Si un Pid est sp�cifi�

'-> Mettre � jour le fichier Ini avec le PID du programme en cours
SetIniString "PID", "HANDLE", IniFileName, CStr(GetCurrentProcessId)

'-> R�cup�ration de la liste des imprimantes obligatoires
strPrinters = GetIniString("PRINTERS", "", IniFileName, False)
If Trim(strPrinters) = "" Then
    '-> Poser les variables
    UsePrinter = False
    ListPrinter = ""
Else
    '-> Cr�er la liste des imprimantes
    For i = 1 To NumEntries(strPrinters, Chr(0))
        '-> Get d'un d�finition d'imprimante
        DefPrinter = Trim(Entry(i, strPrinters, Chr(0)))
        If Trim(DefPrinter) = "" Then Exit For
        '-> Ajouter � la liste des imprimantes
        If ListPrinter = "" Then
            ListPrinter = UCase$(Trim(Entry(2, DefPrinter, "=")))
        Else
            ListPrinter = ListPrinter & Chr(0) & UCase$(Trim(Entry(2, DefPrinter, "=")))
        End If
    Next 'Pour toutes les imprimantes par d�faut
    '-> Indiquer que l'on utilise les imprimantes obligatoires
    UsePrinter = True
End If

'-> Ouvrir un fichier LOG si necessaire
If IsLog Then
    '-> Ouverture
    hdlFile = FreeFile
    Open LogDirectory & LogFilename For Append As #hdlFile
    '-> Imprimer l'entete
    Print #hdlFile, "**************************************"
    Print #hdlFile, "*                                    *"
    Print #hdlFile, "*           DealPrinterBatch         *"
    Print #hdlFile, "*                                    *"
    Print #hdlFile, "*                                    *"
    Print #hdlFile, "**************************************"
    Print #hdlFile, "****** Debut de session du programme DealPrintBatch ******"
    Print #hdlFile, "Date : " & Now
    Print #hdlFile, "****** Etat des variables ****** "
    Print #hdlFile, "Synchronisation : " & CStr(IsSyncho)
    Print #hdlFile, "Gestion des log : " & CStr(IsLog)
    Print #hdlFile, "FileNameStop : " & FilenameStop
    Print #hdlFile, "PathTurboFile : " & PathTurboFile
    Print #hdlFile, "PathPdfFile : " & PathPdfFile
    Print #hdlFile, "Synchro Directory : " & SynchroDirectory
    Print #hdlFile, "Synchro FileName : " & SynchroFilename
    Print #hdlFile, "Synchro Delai : " & SynchroDelai
    Print #hdlFile, "Scan Directory : " & ScanDirectory
    Print #hdlFile, "Scan Extension : " & ScanExtension
    Print #hdlFile, "Scan Delai : " & ScanDelai
    Print #hdlFile, "Log Directory : " & LogDirectory
    Print #hdlFile, "Log Filename : " & LogFilename
    Print #hdlFile, "ExitOnBadPrinter : " & CStr(ExitOnBadPrinter)
    Print #hdlFile, "ErrorDirectory : " & ErrorDirectory
    Print #hdlFile, "****** Liste des imprimantes disponibles******"
    '-> Analyse des imprimantes
    For Each X In Printers
        Print #hdlFile, "Printer : " & X.DeviceName & " - Port : " & X.Port
    Next 'Pour toutes les imprimantes
    '-> Imprimantes obligatoires
    Print #hdlFile, "****** Imprimantes  obligatoires******"
    Print #hdlFile, "Utilisation des imprimantes obligatoires : " & CStr(UsePrinter)
End If

'-> Tester si on utilise les imprimantes obligatoires
If UsePrinter Then
    '-> imprimer la liste des imprimantes obligatoires
    For i = 1 To NumEntries(ListPrinter, Chr(0))
        '-> V�rifier si on trouve l'imprimante
        ErrorPrinter = IsGetPrinter(Entry(i, ListPrinter, Chr(0)))
        '-> Si Erreur ou non
        If Not ErrorPrinter Then IsBadPrinter = True
        '-> Imprimer le nom de l'imprimante
        If IsLog Then Print #hdlFile, "Imprimante : " & Entry(i, ListPrinter, Chr(0)) & " :  Imprimante trouv�e : " & CStr(ErrorPrinter)
    Next 'Pour toutes les imprimantes obligatoires
End If 'Si on utilise une imprimante obligatoire

'-> Tester si on a trouv� toutes les imprimantes
If IsBadPrinter Then
    '-> Imprimer une trace de log si necessaire
    If IsLog Then Print #hdlFile, "Impossible de trouver une imprimante obligatoire :  Fin du programme"
    '-> Supprimer le PID
    SetIniString "PID", "HANDLE", IniFileName, ""
    '-> Terminer le programme
    End
End If 'Si on a trouv� une imprimante obligatoire non r�f�renc�e
    
'-> Indiquer le d�but du traitement
If IsLog Then Print #hdlFile, "****** Traitements ******"

'-> Charger la feuille de synchronisation
If IsSyncho Then
    '-> Cr�er le fichier de  synchronistation
    SetIniString "DEAL", "TIME", SynchroDirectory & SynchroFilename, CStr(Now)
    '-> Lancer la feuille de temporisation
    Load frmSynchronisation
    '->Poser le D�lai de Temporisation
    frmSynchronisation.Timer1.Interval = SynchroDelai
End If

'-> charger la feuille d'analyse
Load frmPrint
frmPrint.Timer1.Interval = ScanDelai

End Sub

Public Function IsGetPrinter(strPrinter As String) As Boolean

Dim X As Printer

'-> Analyse des imprimantes
For Each X In Printers
    If UCase$(Trim(X.DeviceName)) = UCase$(Trim(strPrinter)) Then
        IsGetPrinter = True
        Exit Function
    End If
Next 'Pour toutes les imprimantes

End Function

Public Sub PrintLog(strToPrint As String)

'---> Mise � jour du fichier Log
If Not IsLog Then Exit Sub

Print #hdlFile, strToPrint

End Sub

Public Function IsFile(ByVal NomFichier As String) As Boolean

Dim hdlFile As Long
Dim aOf As OFSTRUCT

'-> R�cup�ration des infos pour ProgressBar
hdlFile = OpenFile(NomFichier, aOf, OF_EXIST)
If hdlFile <> -1 Then IsFile = True

End Function
