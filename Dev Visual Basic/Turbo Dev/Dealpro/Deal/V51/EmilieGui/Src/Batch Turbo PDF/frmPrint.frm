VERSION 5.00
Begin VB.Form frmPrint 
   Caption         =   "Form1"
   ClientHeight    =   1170
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5835
   Icon            =   "frmPrint.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1170
   ScaleWidth      =   5835
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Left            =   1560
      Top             =   240
   End
End
Attribute VB_Name = "frmPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Timer1_Timer()

'-> Variables locales
Dim TurboFile As String
Dim PdfFile As String
Dim KillFile As String
Dim PrinterName As String
Dim NbCopies As Integer
Dim FileName As String
Dim Temp As String
Dim LogLigne As String
Dim CommandLigne As String
Dim IsErrorFile As Boolean
Dim IsFatalError As Boolean

'On Error Resume Next

'-> V�rifier si on trouve un fichier de fin de travail
If Dir$(ScanDirectory & FilenameStop) <> "" Then
    '-> Butter le fichier de demande de fin
    Kill ScanDirectory & FilenameStop
    '-> Supprimer le fichier de synchronisation
    Kill SynchroDirectory & SynchroFilename
    '-> Supprimer le PID dans le fichier
    SetIniString "PID", "HANDLE", IniFileName, ""
    '-> Inscrire la fin du log
    Call PrintLog("****** Arret du Batch " & Now & " *****")
    '-> Fermer toutes les fen�tres
    Unload frmSynchronisation
    Unload Me
    End
End If

'-> Bloquer le Timer
Me.Timer1.Enabled = False

'-> Faire une analyse du r�pertoire
FileName = Dir$(ScanDirectory & ScanExtension)
Do While FileName <> ""
    '-> RAZ des variables
    IsErrorFile = False
    IsFatalError = False
    '-> Lecture des informations
    TurboFile = GetIniString("BATCH", "TURBOFILE", ScanDirectory & FileName, False)
    '-> Get du printername
    PrinterName = GetIniString("BATCH", "PRINTERNAME", ScanDirectory & FileName, False)
    '-> Get du nom Pdf
    PdfFile = GetIniString("BATCH", "PDFFILE", ScanDirectory & FileName, False)
    '-> Get du kill
    KillFile = GetIniString("BATCH", "KILLFILE", ScanDirectory & FileName, False)
    '-> R�cup�rer le nombre de copies
    Temp = GetIniString("BATCH", "NBCOPIES", ScanDirectory & FileName, False)
    '-> Raz de la avriable de Log
    Call PrintLog("---> Taitement du fichier : " & ScanDirectory & FileName)
    Call PrintLog("Date : " & Now)
    '-> Test sur le fichier Turbo
    Call PrintLog("Fichier Turbo : " & PathTurboFile & TurboFile)
    '-> Tester que l'on trouve le fichier turbo de r�f�rence
    If Not IsFile(PathTurboFile & TurboFile) Then
        '->Poser un Log
        Call PrintLog("Impossible de trouver le fichier Turbo. Fin du traitement")
        '-> Indiquer que le fichier est en erreur
        IsErrorFile = True
        '-> Analyse du fichier suivant
        GoTo NextFile
    Else
        Call PrintLog("Fichier trouv�")
    End If
    '-> Test du printername
    Call PrintLog("Printername : " & PrinterName)
    '-> Tester que cela soit un printervalide
    If Not IsGetPrinter(PrinterName) Then
        '-> Poser un Log
        Call PrintLog("Imprimante non valide. Fin du traitement")
        '-> Indiquer que le fichier est en erreur
        IsErrorFile = True
        '-> Tester si c'est une imprimante par d�faut
        IsFatalError = IsObligatoirePrinter(PrinterName)
        '-> Analyse du Log suivant
        GoTo NextFile
    Else
        Call PrintLog("Printer valide")
    End If
    '-> Tester si on est dans le cas d'une imprimante PDF
    If UCase$(Trim(PrinterName)) = "ACROBAT PDFWRITER" Or UCase$(Trim(PrinterName)) = "WIN2PDF" Then
        Call PrintLog("Impression PDF : Oui")
        '-> Tester si le nom est sp�cifi�
        If Trim(PdfFile) = "" Then
            PdfFile = PathPdfFile & TurboFile & ".pdf"
        Else
            PdfFile = PathPdfFile & PdfFile
        End If
        '-> Imprimer le Log
        Call PrintLog("PdfFile : " & PdfFile)
        '-> Faire un Tests si = 1
        If KillFile = "1" Then
            Call PrintLog("Suppression du fichier Turbo : Oui")
        Else
            KillFile = "0"
            Call PrintLog("Suppression du fichier Turbo : Non")
        End If
        '-> Cr�ation de la chaine de commande
        CommandLigne = UCase$(Trim(PrinterName)) & "~DIRECT~1|" & PathTurboFile & TurboFile & "*" & PdfFile & "*" & KillFile
    Else
        '-> Indiquer que l'on n'est pas sur une impression PDF
        Call PrintLog("Impression PDF : Non")
        If IsNumeric(Temp) Then
            NbCopies = CInt(Temp)
        Else
            NbCopies = 1
        End If
        '-> Cr�ation de la chaine de commande
        CommandLigne = PrinterName & "~DIRECT~" & NbCopies & "|" & PathTurboFile & TurboFile
    End If
    
    '-> Impression
    Call PrintLog("Ligne de commande Turbo : " & CommandLigne)
    '-> Run de l'impression
    Shell App.Path & "\TurboGraph.exe  " & CommandLigne, vbNormalFocus
    
NextFile:
    '-> Si on est en erreur ou non
    If IsErrorFile Then
        '-> Tester si on acc�s aux erreurs
        If IsError Then
            '-> Copier le fichier en erreur
            FileCopy ScanDirectory & FileName, ErrorDirectory & "ERROR_" & CStr(Abs(GetTickCount)) & "_" & FileName
        End If 'Si on a acc�s aux erreurs
    End If

    '-> Suppression du fichier
    Kill ScanDirectory & FileName
    
    '-> Tester si on doit s'auto d�truire
    If IsFatalError And IsErrorFile Then
        '-> Supprimer le PID
        SetIniString "PID", "HANDLE", IniFileName, ""
        '-> Imprimer un Log
        Call PrintLog("Erreur sur imprimante obligatoire : arret du batch � " & Now)
        '-> Supprimer le fichier de synchronisation
        Kill SynchroDirectory & SynchroFilename
        '-> D�charger la feuille de synchronisation
        Unload frmSynchronisation
        '-> Fin du programme
        End
    End If 'Si on doit

    '-> Analyse de l'entr�e suivante
    FileName = Dir
    
Loop

'-> D�bloquer le timer
Me.Timer1.Enabled = True

End Sub

Private Function IsOkFile(strFile As String) As Boolean

'---> Cette proc�dure r�cup�re un attibut du fichier


End Function

Private Function IsObligatoirePrinter(strPrinter As String) As Boolean

Dim i As Integer

'---> Analyse de la liste des imprimantes obligatoure

'-> Ne rien faire si pas de liste d'imprimante
If Trim(ListPrinter) = "" Then Exit Function

'-> Si on utilise des imprimantes obligatoires
If UsePrinter Then
    '-> V�rifier si on trouve cette imprimante
    For i = 1 To NumEntries(ListPrinter, Chr(0))
        '-> Tester s'il y a ad�quation
        If UCase$(Trim(Entry(i, ListPrinter, Chr(0)))) = UCase$(Trim(strPrinter)) Then
            IsObligatoirePrinter = True
            Exit Function
        End If 'Si on a trouv� l'imprimante
    Next 'Pour toutes les imprimantes obligatoires
End If 'Si on utilise une imprimante par d�faut

End Function
