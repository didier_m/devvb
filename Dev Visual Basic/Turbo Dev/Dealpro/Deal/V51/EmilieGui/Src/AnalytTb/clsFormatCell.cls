VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsFormatCell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'**************************************
'* Classe de formattage d'une cellule *
'**************************************

Public FontName As String
Public FontSize As Single
Public FontBold As Boolean
Public FontItalic As Boolean
Public FontUnderline As Boolean
Public FontColor As Long
Public BackColor As Long
Public Format As Integer '0 Libre
                         '1 Texte +  libelle
                         '2 Numeric
Public Libelle As String
Public NbDec As String
Public UseSepMil As Boolean
Public Absolute As Boolean
Public Pourcentage As Boolean
Public UseColorNeg As Boolean
Public ColorNeg As Long
Public InterneAlign As Integer

Private Sub Class_Initialize()

    '-> Initialisation par d�faut du format d'une cellule
    BackColor = QBColor(15)
    FontName = "Times New Roman"
    FontSize = 10
    FontBold = False
    FontUnderline = False
    FontItalic = False
    Absolute = False
    Pourcentage = False
    Format = 0  '-> Pas de format sp�cifi�
    ColorNeg = 0
    UseColorNeg = False
    Libelle = ""
    InterneAlign = 5
    NbDec = 0

End Sub
