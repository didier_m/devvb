Attribute VB_Name = "fctanalyTb"
'**********************************
'* Module de gestion des tableaux *
'**********************************

Option Explicit
'
''-> Variable d'�change
'Public strRetour As String

'-> Collection des tableaux ouverts dans l'�diteur
Public Tableaux As Collection
'-> Collection des cumuls sur axes d'analyse
Public Axes As Collection
'-> Collection des cumuls sur axes detail
Public Details As Collection

'-> API pour gestion des SPLITS
Public Type POINTAPI
    x As Long
    y As Long
End Type
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function ScreenToClient Lib "user32" (ByVal hwnd As Long, lpPoint As POINTAPI) As Long
Public Declare Function ClientToScreen Lib "user32" (ByVal hwnd As Long, lpPoint As POINTAPI) As Long

'-> Pouyr export vers Turbo
Const ConstCell = "ALIGN;CONTENU;LARGEUR;VRAI,BORDUREBAS,VRAI,VRAI;FONT;FONTSIZE;BOLD;ITALIC;UNDERLINE;FORECOLOR;BACKCOLOR;FAUX;0@0"

'-> Collection des fichiers ASCII � int�grer
Public Files As Collection

'-> fichier tableau � charger
Public TabFileName As String

'-> Indique que l'on charge un tableau unique
Dim UniqueMode As Boolean

'-> Variable temporaire qui permet d'acc�der au param�trage du tableau
Public aTb As clsTableau

'-> Variable qui pointe vers la feuille de navigation
Public FrmNavigaCumul As frmNaviga

'-> Variable qui pointe vers la feuille de details
Public FrmNavigaDetail As frmNaviga


Sub Main()

 '-> Initialisation des param�tres de r�pertoire
Call Initialisation

'-> Initialiser la collection des tableaux
Set Tableaux = New Collection
'-> Initialisation des collections de cumuls
Set Axes = New Collection
Set Details = New Collection

'-> Initialisation des parametres regionnaux ( separateurs decimales et milliers etc.... )
InitConvertion
'-> Afficher la feuille principale
MDIMain.Show
'-> Afficher la feuille de choix
'frmOpenTab.Show vbModal
frmStart.Show vbModal

'-> Lancer la cr�ation de la palette de navigationb
RunStructure


End Sub

Private Sub RunStructure()

'----> Cette proc�dure analyse les fichiers ASCII

Dim aNode As Node

'On Error GoTo GestError

'-> Bloquer la mise � jour de l'�cran
LockWindowUpdate MDIMain.hwnd

'-> Charger dans un premier temps une instance du tableau pour acc�s � son param�trage
Set aTb = LoadTb(TabFileName)

'-> D�terminer si on est mode unique ou non
If Not aTb.UseAxe And Not aTb.UseDetail Then
    '-> Indiquer que l'on est en mode unique
    UniqueMode = True
    '-> Charger son tableau en mode unique
    LoadUniqueMode
    '-> Quitter la proc�dure
    Exit Sub
End If
        
'-> Cr�er les feuilles de navigation
If aTb.UseAxe Then
    '-> Initialiser la feuille
    Set FrmNavigaCumul = New frmNaviga
    '-> Ajouter le node Root
    Set aNode = FrmNavigaCumul.TreeView1.Nodes.Add(, , "ROOT", aTb.LibAxe, "CLOSE")
    aNode.ExpandedImage = "OPEN"
    FrmNavigaCumul.Caption = aTb.LibAxe
    '-> Trier
    aNode.Sorted = True
    aNode.Expanded = True
    FrmNavigaCumul.IsCumul = True
End If
If aTb.UseDetail Then
    '-> Initialiser la feuille
    Set FrmNavigaDetail = New frmNaviga
    FrmNavigaDetail.Caption = aTb.LibDet
    '-> Ajouter le node Root
    Set aNode = FrmNavigaDetail.TreeView1.Nodes.Add(, , "ROOT", aTb.LibDet, "CLOSE")
    aNode.ExpandedImage = "OPEN"
    '-> Trier
    aNode.Sorted = True
    aNode.Expanded = True
    FrmNavigaDetail.IsCumul = False
End If

'-> Lancer l'analyse des diff�rents fichiers ASCII
CreateStructure

GestError:

'-> D�bloquer la mise � jour de l'�cran
LockWindowUpdate 0


End Sub

Private Sub CreateStructure()

'---> Cette proc�dure analyse les diff�rents fichiers ASCII pour cr�er _
les palettes de navigation

Dim hdlFile As Integer
Dim FileName As String
Dim Progiciel As String
Dim i As Long, j As Long
Dim Ligne As String
Dim FindEntete As Boolean
Dim aProgiciel As clsProgiciel

'-> Int�grer tous les fichiers
For i = 1 To Files.Count

    '-> Get du fichier � int�grer
    FileName = Entry(1, Files(i), "|")
    Progiciel = Entry(2, Files(i), "|")
        
    '-> V�rifier que le fichier existe
    If Dir$(FileName) = "" Then GoTo NextFile
    
    '-> Pointer sur le progiciel
    Set aProgiciel = aTb.Progiciels("PRO_" & Progiciel)
    
    '-> Ouvrir le fichier ascii
    hdlFile = FreeFile
    Open FileName For Input As #hdlFile
    
    '-> Lecture de l'entete
    FindEntete = False
    
    '-> Init du compteur de lignes
    j = 0
    
    '-> Lecture s�quentielle du fichier
    Do While Not EOF(hdlFile)
        '-> Lecture de la ligne
        Line Input #hdlFile, Ligne
        j = j + 1
        '-> Ne pas traiter les lignes blanches
        If Trim(Ligne) = "" Then GoTo NextLig
        '-> Int�grer que si on a trouv� la cl� d'entete
        If Not FindEntete Then
            '-> Tester si on est sur la ligne d'entete
            If UCase$(Trim(Ligne)) = "[DATA]" Then FindEntete = True
            GoTo NextLig
        End If
        '-> Int�ger la ligne
        IntegrateLigneInStructure Ligne, aProgiciel, j, i
NextLig:
    Loop 'Lecture s�quentielle du fichier
    '-> Fermer le fichier
    Close #hdlFile
NextFile:
Next 'Pour tous les fichiers � int�grer

End Sub
Private Sub IntegrateLigneInStructure(Ligne As String, aProgiciel As clsProgiciel, IndexLigne As Long, IndexFichierAscii As Long)

'---> Cette proc�dure int�gre une ligne dans la collection des cumuls et des d�tails
    
Dim aCumul As clsCumul
Dim CumulToAdd As clsCumul
Dim i As Integer
Dim CreateCumul As Boolean
Dim DefCumul As String
Dim aNode As Node
Dim CheckAxe As Boolean
    
Dim RefAxe As String '-> Cumul de la ligne
Dim RefDet As String '-> Acc�s au d�tail
    
'-> R�cup�rer le param�trage de l'axe hierarchique
If aProgiciel.MatriceAxe <> "" Then
    For i = 1 To aTb.NvAxeAnalyse
        '-> Getdu niveau de l'axe
        DefCumul = UCase$(Trim(Entry(CInt(Entry(i, aProgiciel.MatriceAxe, "|")), Ligne, "|")))
        If Trim(DefCumul) = "" Then Exit For
        '-> Cumul dans la variable
        If RefAxe = "" Then
            RefAxe = DefCumul
        Else
            RefAxe = RefAxe & "|" & DefCumul
        End If
    Next 'Pour tous les niveaux de l'axe
End If 'Si l'axe est pr�sent dans le fihcier ASCII
        
'-> R�cup�ration de la cl� d�tail que du premier niveau
If aProgiciel.MatriceDet <> "" Then
    For i = 1 To aTb.NvAxeDetail
        '-> Get de la valeur du cumul
        DefCumul = UCase$(Trim(Entry(CInt(Entry(i, aProgiciel.MatriceDet, "|")), Ligne, "|")))
        If Trim(DefCumul) = "" Then Exit For
        '-> Cumul dans la variable
        If RefDet = "" Then
            RefDet = DefCumul
        Else
            RefDet = RefDet & "|" & DefCumul
        End If
    Next 'Pour tous les niveaux de l'axe
End If 'S'il y a un acces vers le d�tail

'-> Int�gration de la ligne pour gestion des cumuls
If aProgiciel.MatriceAxe <> "" Then
    '-> Pour tous les niveaux de l'axe
    For i = 1 To aTb.NvAxeAnalyse
        '-> Get de la valeur du cumul
        DefCumul = Trim(Entry(CInt(Entry(i, aProgiciel.MatriceAxe, "|")), Ligne, "|"))
        If Trim(DefCumul) = "" Then GoTo GetDet
        '-> V�rifier si le cumul existe
        If i = 1 Then
            CreateCumul = IsCumul(DefCumul, Nothing, True)
        Else
            CreateCumul = IsCumul(DefCumul, aCumul, True)
        End If
        '-> Si on doit cr�er le cumul ou non
        If CreateCumul Then
            '-> Cr�ation d'un nouveau cumul*
            Set CumulToAdd = New clsCumul
            '-> Mise � jour de sa valeur
            CumulToAdd.Key = DefCumul
            '-> Ajouter dans le cumul
            If i = 1 Then
                '-> On ajoute directe dans la collection
                Axes.Add CumulToAdd, "CUM_" & UCase$(Trim(DefCumul))
                '-> Cr�er un nouveau node
                Set aNode = FrmNavigaCumul.TreeView1.Nodes.Add("ROOT", 4, "ROOT|" & UCase$(Trim(DefCumul)), DefCumul, "CLOSE")
                aNode.ExpandedImage = "OPEN"
                '-> Indiquer que l'on est sur un node de cumul
                aNode.Tag = "CUMUL"
                '-> Trier
                aNode.Sorted = True
            Else
                '-> On ajoute au cumul en cours
                aCumul.Cumuls.Add CumulToAdd, "CUM_" & UCase$(Trim(DefCumul))
                Set aNode = FrmNavigaCumul.TreeView1.Nodes.Add(aNode.Key, 4, aNode.Key & "|" & UCase$(Trim(DefCumul)), DefCumul, "CLOSE")
                aNode.ExpandedImage = "OPEN"
                '-> Indiquer que l'on est sur un node de cumul
                aNode.Tag = "CUMUL"
                '-> Trier
                aNode.Sorted = True
            End If
            '-> RAZ de la variable
            CreateCumul = False
            '-> Pointer sur le cumul
            Set aCumul = CumulToAdd
        Else
            '-> le cumul existe d�ja, pointer dessus
            If i = 1 Then
                '-> On pointe directe dans la collection
                Set aCumul = Axes("CUM_" & Trim(UCase$(DefCumul)))
                '-> On pointe sur le node associ�
                Set aNode = FrmNavigaCumul.TreeView1.Nodes("ROOT|" & UCase$(Trim(DefCumul)))
            Else
                '-> On pointe sur le cumul en cours
                Set aCumul = aCumul.Cumuls("CUM_" & Trim(UCase$(DefCumul)))
                '-> Pointer sur le node associ�
                Set aNode = FrmNavigaCumul.TreeView1.Nodes(aNode.Key & "|" & UCase$(Trim(DefCumul)))
            End If
        End If 'Si le cumul existe d�ja
        '-> Ajouter l'index de la ligne en cours dans le cumul en cours
        aCumul.Lignes.Add aProgiciel.Code & Chr(0) & CStr(IndexLigne) & Chr(0) & IndexFichierAscii
        '-> Indiquer pour ce niveau l'acc�s au d�tail associ�
        aCumul.AddKeyLink RefDet
    Next 'Pour tous le niveaux de l'axe
Else
    '-> Analyse de l'axe hierarchique � partir de l'axe de d�tail
    CheckAxe = True
End If 'Si on doit int�grer la matrice des axes

GetDet:

'-> Int�gration de la ligne pour gestion des axes de d�tail
If aProgiciel.MatriceDet <> "" Then
    '-> Pour tous les niveaux de l'axe
    For i = 1 To aTb.NvAxeDetail
        '-> Get de la valeur du cumul
        DefCumul = Trim(Entry(CInt(Entry(i, aProgiciel.MatriceDet, "|")), Ligne, "|"))
        If DefCumul = "" Then Exit Sub
        '-> V�rifier si le cumul existe
        If i = 1 Then
            CreateCumul = IsCumul(DefCumul, Nothing, False)
        Else
            CreateCumul = IsCumul(DefCumul, aCumul, False)
        End If
        '-> Si on doit cr�er le cumul ou non
        If CreateCumul Then
            '-> Cr�ation d'un nouveau cumul*
            Set CumulToAdd = New clsCumul
            '-> Mise � jour de sa valeur
            CumulToAdd.Key = DefCumul
            '-> Ajouter l'index de la ligne en cours
            'CumulToAdd.Lignes.Add aProgiciel.Code & Chr(0) & CStr(j) & Chr(0) & IndexFichierAscii
            If i = 1 Then
                '-> On ajoute directe dans la collection
                Details.Add CumulToAdd, "CUM_" & UCase$(Trim(DefCumul))
                '-> Cr�er un nouveau node
                Set aNode = FrmNavigaDetail.TreeView1.Nodes.Add("ROOT", 4, "ROOT|" & UCase$(Trim(DefCumul)), DefCumul, "CLOSE")
                aNode.ExpandedImage = "OPEN"
                '-> Indiquer que l'on est sur un node de cumul
                aNode.Tag = "CUMUL"
                '-> Trier
                aNode.Sorted = True
            Else
                '-> On ajoute au cumul en cours
                aCumul.Cumuls.Add CumulToAdd, "CUM_" & UCase$(Trim(DefCumul))
                Set aNode = FrmNavigaDetail.TreeView1.Nodes.Add(aNode.Key, 4, aNode.Key & "|" & UCase$(Trim(DefCumul)), DefCumul, "CLOSE")
                aNode.ExpandedImage = "OPEN"
                '-> Indiquer que l'on est sur un node de cumul
                aNode.Tag = "CUMUL"
                '-> Trier
                aNode.Sorted = True
            End If
            '-> RAZ de la variable
            CreateCumul = False
            '-> Pointer sur le cumul
            Set aCumul = CumulToAdd
        Else
            '-> le cumul existe d�ja, pointer dessus
            If i = 1 Then
                '-> On pointe directe dans la collection
                Set aCumul = Details("CUM_" & Trim(UCase$(DefCumul)))
                '-> On pointe sur le node associ�
                Set aNode = FrmNavigaDetail.TreeView1.Nodes("ROOT|" & UCase$(Trim(DefCumul)))
            Else
                '-> On pointe sur le cumul en cours
                Set aCumul = aCumul.Cumuls("CUM_" & Trim(UCase$(DefCumul)))
                '-> Pointer sur le node associ�
                Set aNode = FrmNavigaDetail.TreeView1.Nodes(aNode.Key & "|" & UCase$(Trim(DefCumul)))
            End If
        End If 'Si le cumul existe d�ja
        '-> Ajouter l'index de la ligne en cours dans le cumul en cours
        aCumul.Lignes.Add aProgiciel.Code & Chr(0) & CStr(IndexLigne) & Chr(0) & IndexFichierAscii
        '-> Ajouter le line vers le d�tail
        aCumul.AddKeyLink RefAxe
    Next 'Pour tous les niveaux de l'axe de d�tail
End If 'Si on doit int�grer la matrice des d�tails

'-> Si le progciel ne contient pas l'axe hierarchique -> mettre � jour l'axe hierarchique en partant de l'axe de d�tail
If CheckAxe Then
    If RefDet <> "" Then
        '-> Si on a r�cup�r� l'axe hierarchique dans le cumul en cours
        If aCumul.KeyAxe.Count <> 0 Then
            '-> R�cup�rer l'axe
            RefAxe = aCumul.KeyAxe(1)
            '-> Quitter si axe = ""
            If RefAxe = "" Then Exit Sub
        Else
            Exit Sub
        End If 'Si pour le d�tail en cours on a l'axe hierarchique associ�
        
        '-> Analyse de l'axe hierarchique
        For i = 1 To NumEntries(RefAxe, "|")
            '-> Pointer sur le cumul
            If i = 1 Then
                Set aCumul = Axes("CUM_" & Entry(i, RefAxe, "|"))
            Else
                Set aCumul = aCumul.Cumuls("CUM_" & Entry(i, RefAxe, "|"))
            End If
            '-> Ajouter l'index de la ligne en cours dans le cumul en cours
            aCumul.Lignes.Add aProgiciel.Code & Chr(0) & CStr(IndexLigne) & Chr(0) & IndexFichierAscii
            '-> Ajouter le line vers le d�tail
            aCumul.AddKeyLink RefAxe
        Next 'Pour tous les niveaux de l'axe
    End If 'Si on a r�cup�r� l'axe de d�tail
End If 'Si on doit cr�er l'axe hierarchique depuis l'axe de d�tail

End Sub

Private Function IsCumul(DefCumul As String, CumulRef As clsCumul, IsCumulAxe As Boolean) As Boolean

'---> Cette proc�dure v�rifie

Dim aCumul As clsCumul

On Error GoTo GestError

'-> Selon que l'analyse les cumuls ou les details
If IsCumulAxe Then
    '-> Pointer sur le premier niveau ou non
    If CumulRef Is Nothing Then
        Set aCumul = Axes("CUM_" & UCase$(Trim(DefCumul)))
    Else
        Set aCumul = CumulRef.Cumuls("CUM_" & UCase$(Trim(DefCumul)))
    End If
Else
    '-> Pointer sur le premier niveau ou non
    If CumulRef Is Nothing Then
        Set aCumul = Details("CUM_" & UCase$(Trim(DefCumul)))
    Else
        Set aCumul = CumulRef.Cumuls("CUM_" & UCase$(Trim(DefCumul)))
    End If
End If 'Selon qui on attaque

'-> Renvoyer une valeur de succ�s
IsCumul = False

'-> Quitter la proc�dure
Exit Function

GestError:
    IsCumul = True
End Function

Private Sub LoadUniqueMode()

'---> Cette proc�dure charge toutes les feuilles dans un seul est unique tableau

Dim hdlFile As Integer
Dim FileName As String
Dim i As Integer

'-> Int�grer tous les fichiers
For i = 1 To Files.Count


Next 'Pour tous les fichiers


End Sub

Public Sub DisplayTB(CumulDef As String, IsCumul As Boolean)

'---> Cette proc�dure affiche  un tableau de bord

Dim aCumul As clsCumul
Dim i As Long
Dim aFrm As frmTableau
Dim hdlFile As Integer
Dim Ligne As String

On Error Resume Next

'-> Attention la cl� d'acc�s du cumul vient du treeveiw
' elle commence par ROOT|
For i = 2 To NumEntries(CumulDef, "|")
    '-> Pointer sur le cumul
    If i = 2 Then
        If IsCumul Then
            Set aCumul = Axes("CUM_" & Entry(i, CumulDef, "|"))
        Else
            Set aCumul = Details("CUM_" & Entry(i, CumulDef, "|"))
        End If
    Else
        Set aCumul = aCumul.Cumuls("CUM_" & Entry(i, CumulDef, "|"))
    End If
Next

'-> V�rifier si la feuille n'est pas active
If aCumul.HdlForm <> 0 Then
    '-> Envoyer un message d'activation
    SendMessage aCumul.HdlForm, WM_CHILDACTIVATE, 0, 0
    '-> Quitter la proc�dure
    Exit Sub
End If

'-> Pointeur de souris
Screen.MousePointer = 11
'-> Bloquer l'�cran
MDIMain.Enabled = False
'-> Mise � jour de l'�cran
LockWindowUpdate MDIMain.hwnd

'-> Afficher la feuille de temporisation
frmTempo.Show

'-> Initialisation des variables de temporisation
TailleLue = 0
TailleTotale = aCumul.Lignes.Count

'-> Le cumul n'est pas en cours d'affichage, cr�er une nouvelle instance du tableau
Set aFrm = New frmTableau

'-> Initialiser son cumul
Set aFrm.aCumul = aCumul

'-> Attention : ne charger le tableau que si n'est pas charg�
If Not aCumul.aTb Is Nothing Then GoTo DisplayTableau

'-> Cr�er un nouveau tableau
Set aCumul.aTb = LoadTb(TabFileName)

'-> Titre de la feuille
If IsCumul Then
    frmTempo.Caption = aCumul.aTb.LibAxe & " - " & aCumul.Key
Else
    frmTempo.Caption = aCumul.aTb.LibDet & " - " & aCumul.Key
End If
DoEvents

'-> Cr�ation de ses niveaux de cumul
CreateCumul aCumul

'-> Mise � jour des libelles
frmTempo.Label1.Caption = "Dessin du tableau en cours ..."
frmTempo.Label1.Refresh
DoEvents

'-> Int�gration des colonnes et des lignes de calcul
CreateCalcul aCumul

'-> Code de reprise
DisplayTableau:

'-> Ne pas dessiner les objets cach�s
aCumul.aTb.DisplayVisibleObject = False

'-> Mettre � jour le libelle
frmTempo.Label1.Caption = "Dessin du tableau en cours ..."
frmTempo.Picture1.Cls
frmTempo.Refresh
DoEvents

'-> Get des valeurs de temporisation
TailleLue = 0
TailleTotale = aCumul.aTb.Lignes.Count + aCumul.aTb.Colonnes.Count

'-> Dessiner le tableau
InitLoadingTb aCumul.aTb, aFrm.MSFlexGrid1, , True, True

'-> Initialiser
If IsCumul Then
    aFrm.Init 1
Else
    aFrm.Init 0
End If

'-> Affecter le handle de la feuille au tableau
aCumul.HdlForm = aFrm.hwnd

'-> Afficher la feuille
aFrm.Show

'-> D�charger la feuille de temporisation
Unload frmTempo

GestError:

    '-> Pointeur de souris
    Screen.MousePointer = 0
    '-> Bloquer l'�cran
    MDIMain.Enabled = True
    '-> Mise � jour de l'�cran
    LockWindowUpdate 0


End Sub

Public Sub CreateCumul(aCumul As clsCumul)

'---> Cette proc�dure analyse toutes les lignes d'un cumul pour alimenter les tableaux

Dim Ligne As String
Dim IndexFichier As Integer
Dim IndexLigLue As Long
Dim IndexLigne As Long
Dim Fichier As String
Dim Progiciel As String
Dim i As Long, j As Long
Dim hdlFile As Long

'-> Libell� de temporisation
frmTempo.Label1.Caption = "Analyse des fichiers en cours ..."
DoEvents

'-> Analyse de toutes les lignes associ�e � ce cumul
For i = 1 To aCumul.Lignes.Count
    '-> Dessin de la temporisation
    TailleLue = i
    DrawWait TailleTotale, TailleLue, frmTempo.Picture1
    DoEvents
    '-> get de la ligne
    Ligne = aCumul.Lignes(i)
    '-> R�cup�rer le num�ro de la ligne du fichier � int�grer
    IndexLigne = CLng(Entry(2, Ligne, Chr(0)))
    '-> V�rifier si le fichier ascii en cours de traitement est le m�me
    If CInt(Entry(3, Ligne, Chr(0))) <> IndexFichier Then
        '-> Fermer l'ancien fichier
        If hdlFile <> 0 Then Close #hdlFile
        '-> Get de l'index fichier
        IndexFichier = CInt(Entry(3, Ligne, Chr(0)))
        '-> Get du fichier associ�
        Fichier = Entry(1, Files(IndexFichier), "|")
        '-> Get du progiciel affect�
        Progiciel = Entry(2, Files(IndexFichier), "|")
        '-> Ouvrir le fichier
        hdlFile = FreeFile
        Open Fichier For Input As #hdlFile
        '-> Remettre le compteur de ligne � 0
        IndexLigLue = 0
    End If
    '-> Get de la ligne dans le fichier ascii
    Do While Not EOF(hdlFile)
        '-> Lecture de la ligne
        Line Input #hdlFile, Ligne
        '-> Incr�menter le compteur de lignes
        IndexLigLue = IndexLigLue + 1
        '-> Si c'est la bonne ligne
        If IndexLigLue = IndexLigne Then
            '-> Innt�gration de la ligne
            IntegrateLigneInCumul aCumul, Ligne, Progiciel
            '-> Quiter la boucle
            Exit Do
        End If
    Loop 'Pour toutes les lignes du fichier ascii
Next 'Pour toutes les lignes associ�es � ce cumul

'-> Fermer le dernier Fichier ASCII ouvert
If hdlFile <> 0 Then Close #hdlFile

End Sub

Private Sub IntegrateLigneInCumul(aCumul As clsCumul, Ligne As String, Progiciel As String)

'---> Cette proc�dure analyse une ligne de cumul pour l'int�grer dans le cumul en cours
Dim aCell As clsCell
Dim aCol As clsCol
Dim aRow As clsRow
Dim aCdt As clsCondition
Dim aValCdt As clsValueCdt
Dim ValueToTest As String
Dim ValueCdtMin As String
Dim ValueCdtMax As String
Dim CdtOk As Boolean
Dim ValueCdtOk As Boolean
Dim aProgiciel As clsProgiciel
Dim i As Integer, j As Integer
Dim OkToIntegrate As Boolean


'-> Pointer sur le progiciel sp�cifi�
Set aProgiciel = aCumul.aTb.Progiciels("PRO_" & Progiciel)

'-> Analyse de toutes les lignes du tableau
For Each aRow In aCumul.aTb.Lignes
    '-> Initialiser le compteur de condition car quand on est en mode ET, il faut �valuer au moins 2 cdts
    i = 0
    '-> De base on int�gre
    OkToIntegrate = True
    '-> Ne Prendre en compte que les ligne de d�tail
    If aRow.TypeLig <> 0 Then GoTo NextRow
    '-> Analyser toutes les conditions affect�es � ce progiciel
    For Each aCdt In aRow.Conditions
        '-> Ne tester que les conditions pour le progiciel en cours
        If aCdt.Progiciel <> Progiciel Then GoTo NextCondition
        '-> R�cup�rer la valeur dans la ligne qui correspond au champ sp�cifi� dans la condition
        ValueToTest = Trim(Entry(aProgiciel.Descriptifs("FIELD_" & aCdt.Colonne).Entree, Ligne, "|"))
        '-> Init le compteur de valeur de condition
        j = 0
        '-> De base on ajoute la condition
        CdtOk = True
        '-> Evaluation de toutes les valeurs pour cett condition
        For Each aValCdt In aCdt.ValueCdts
            '-> Incr�menter le compteur de valeur condition
            j = j + 1
            '-> Evaluation de la valeur
            ValueCdtOk = CompareConditionValues(UCase$(Trim(ValueToTest)), UCase$(Trim(aValCdt.ValMini)), UCase$(Trim(aValCdt.ValMaxi)), aValCdt.Operation)
            '-> Evalue les valeurs de conditions entre elles selon qu'elles sont marqu�es par un 'ET' ou par un 'OU'
            Select Case aValCdt.Operande
                Case 0 ' ET
                    '-> Si les valeurs de la condition sont rassembl�es avec un ET et que l'une d'entre elles est fausse alors on sort dessuite
                    If ValueCdtOk = False Then
                        '-> Indiquer que cette condition n'est pas bonne
                        CdtOk = False
                        '-> On teste si on a evalue au moins 2 cdt pour sortir ( en effet : faux 'ET' XXXX fera toujours faux )
                        If j > 1 Then Exit For
                    End If
                Case 1 ' OU
                    '-> Si les valeurs de la condition sont rassembl�es avec un OU alors on concatene les resultats des tests
                    CdtOk = CdtOk + ValueCdtOk
            End Select '-> Selon si les valeurs de condition sont imbriqu�es avec ET ou OU
        Next 'Pour toutes les valeurs de cette condition
        
         '-> Meme evaluation des conditions entre elles selon qu'elles sont imbriqu�es par 'ET' ou par 'OU'
        Select Case aCdt.TypeCdt
            Case 0 ' ET
                '-> Si les conditions sont concatenees avec des ET et qu'une d'entre elles est fausse alors on sort � faux
                If CdtOk = False Then
                    OkToIntegrate = False
                    '-> On teste si on a evalue au moins 2 cdt pour sortir ( en effet : faux 'ET' XXXX fera toujours faux )
                    If i > 1 Then Exit For
                End If
            Case 1 ' OU
                '-> Si les conditions sont concaten�es avec un OU alors on concatene les resultats des tests
                OkToIntegrate = OkToIntegrate + CdtOk
        End Select '-> Selon si les conditions sont imbriqu�es avec ET ou OU
        
NextCondition:
    Next 'Pour toutes les conditions
    
    '-> On teste si on doit int�grer la ligne dans le tableau
    If OkToIntegrate Then
        '-> Analyse de toutes les colonnes de la ligne
        For Each aCol In aCumul.aTb.Colonnes
            '-> Tester que le progiciel soit le m�me
            If aCol.Progiciel <> Progiciel Then GoTo NextCell
            '-> Lire la valeur associ� dans le fichier ASCII
            ValueToTest = Trim(Entry(aProgiciel.Descriptifs("FIELD_" & aCol.Field).Entree, Ligne, "|"))
            ValueToTest = Convert(ValueToTest)
            '-> Pointer sur la cellule
            Set aCell = aRow.Cellules("CELL_" & aCol.Code)
            '-> Mettre � jour sa valeur
            If aCell.ValueCell <> "" Then
                aCell.ValueCell = Format(CDbl(aCell.ValueCell) + CDbl(ValueToTest), "0.00")
            Else
                aCell.ValueCell = Format(CDbl(ValueToTest), "0.00")
            End If
NextCell:
        Next 'Pour toutes les colonnes
    End If 'Si on int�gre ou non la ligne ASCII dans cette ligne du tableau
NextRow:
Next 'Pour toutes les lignes du tableau en cours

End Sub

Private Function CompareConditionValues(ByRef Value As String, ByRef ValueRefMin As String, ByRef ValueRefMax As String, ByRef Operation As Integer) As Boolean
    
'---> Permet de faire les comparaison entre la valeur du fichier ascii et les differentes valeurs _
de la condition en cour d'evaluation en fonction des operandes specifie dans la condition

Dim ValueConditionOk As Boolean

'-> On  part du principe que la condition est OK
ValueConditionOk = True
'-> On teste la valeur selon son operande
Select Case Operation
    Case 1 '-> egal
        If Value <> ValueRefMin Then ValueConditionOk = False
    Case 2 '-> different
        If Value = ValueRefMin Then ValueConditionOk = False
    Case 3 '-> inferieur
        If Value >= ValueRefMin Then ValueConditionOk = False
    Case 4 '-> superieur
        If Value <= ValueRefMin Then ValueConditionOk = False
    Case 5 '-> compris entre
        If Value <= ValueRefMin Then ValueConditionOk = False
        If Value >= ValueRefMax Then ValueConditionOk = False
    Case 6 '-> contenant
        If InStr(Value, ValueRefMin) = 0 Then ValueConditionOk = False
    Case 7 '-> ne contenant pas
        If InStr(Value, ValueRefMin) <> 0 Then ValueConditionOk = False
    Case 8 '-> commencant par
        If Left$(Value, Len(ValueRefMin)) <> Left$(ValueRefMin, Len(ValueRefMin)) Then ValueConditionOk = False
End Select 'Selon la nature de la condition

'-> On retourne la valeur
CompareConditionValues = ValueConditionOk
    
End Function

Public Sub CreateCalcul(aCumul As clsCumul)

'---> Cette proc�dure �value les lignes et les colonnes de cumul

Dim aCol As clsCol
Dim aRow As clsRow
Dim CalculString As String
Dim DefCalcul As String

'-> Analyse des colonnes du tableaux
For Each aCol In aCumul.aTb.Colonnes
    '-> Ne traiter que les colonnes de calcul
    If aCol.TypeCol <> 1 Then GoTo NextCol
    '-> R�cup�rer sa formule de calcul
    DefCalcul = aCol.Calcul
    '-> Effectuer le calcul pour cette colonne
    For Each aRow In aCumul.aTb.Lignes
        '-> Ne traiter que les lignes de d�tail
        If aRow.TypeLig = 0 Then
            '-> Effectuer le calcul
            CalculString = AnalyseCalculBis(DefCalcul, aCumul, aRow.Code)
            '-> Formatter si necessaire
            If CalculString <> "" Then CalculString = Format(CalculString, "0.00")
            '-> Affecter la valeur � la cellule si cellule <> 0
            If CDbl(CalculString) <> 0 Then aRow.Cellules("CELL_" & aCol.Code).ValueCell = CalculString
        End If
    Next 'Pour toutes les lignes dans le tableau
NextCol:
Next 'Pour toutes les colonnes

'-> Analyse de toutes les lignes dans le tableau
For Each aRow In aCumul.aTb.Lignes
    '-> Ne traiter que les lignes de d�tail
    If aRow.TypeLig <> 1 Then GoTo NextRow
    '-> R�cup�rer sa formule de calcul
    DefCalcul = aRow.Calcul
    '-> Analyse de toutes les colonnes
    For Each aCol In aCumul.aTb.Colonnes
        '-> Ne pas traiter les colonnes de s�paration
        If aCol.TypeCol <> 2 Then
            '-> Effectuer le calcul
            CalculString = AnalyseCalculBis(DefCalcul, aCumul, aCol.Code)
            '-> Formatter si necessaire
            If CalculString <> "" Then CalculString = Format(CalculString, "0.00")
            '-> Affecter la valeur � la cellule si cellule <> 0
            If CDbl(CalculString) <> 0 Then aRow.Cellules("CELL_" & aCol.Code).ValueCell = CalculString
        End If 'Si ce n'est pas une colonne de s�paration
    Next 'Pour toutes les colonnes
NextRow:
Next 'Pour toutes les lignes

End Sub
