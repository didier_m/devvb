VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmSearch 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   6765
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   7530
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6765
   ScaleWidth      =   7530
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   6255
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   7515
      _ExtentX        =   13256
      _ExtentY        =   11033
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      ShowFocusRect   =   0   'False
      TabCaption(0)   =   "Filtre sur le tableau "
      TabPicture(0)   =   "frmSearch.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame3"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Filtre sur l'axe g�ographique"
      TabPicture(1)   =   "frmSearch.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame1 
         Height          =   5655
         Left            =   -74880
         TabIndex        =   24
         Top             =   480
         Width           =   7335
         Begin VB.ListBox List2 
            Height          =   645
            Left            =   2160
            TabIndex        =   10
            Top             =   1320
            Width           =   1935
         End
         Begin VB.TextBox Text4 
            Height          =   285
            Left            =   5280
            TabIndex        =   11
            Top             =   1320
            Width           =   1935
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Left            =   5280
            TabIndex        =   12
            Top             =   1680
            Width           =   1935
         End
         Begin VB.ComboBox Combo1 
            Height          =   315
            Left            =   2160
            TabIndex        =   9
            Top             =   960
            Width           =   5055
         End
         Begin VB.CheckBox Check1 
            Caption         =   "Chercher � travers l'axe"
            Height          =   255
            Left            =   240
            TabIndex        =   25
            Top             =   0
            Value           =   1  'Checked
            Width           =   2295
         End
         Begin VB.CommandButton Command3 
            Caption         =   "&Ajouter"
            Height          =   255
            Left            =   600
            TabIndex        =   13
            Top             =   1680
            Width           =   1455
         End
         Begin MSComctlLib.ListView ListView3 
            Height          =   3495
            Left            =   120
            TabIndex        =   14
            Top             =   2040
            Width           =   7095
            _ExtentX        =   12515
            _ExtentY        =   6165
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Niveau"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Filtre"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Valeur mini"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Valeur maxi"
               Object.Width           =   2540
            EndProperty
         End
         Begin VB.Label Label9 
            Caption         =   "Filtre :"
            Height          =   255
            Left            =   600
            TabIndex        =   28
            Top             =   1320
            Width           =   1455
         End
         Begin VB.Label Label8 
            Caption         =   "Valeur mini : "
            Height          =   255
            Left            =   4200
            TabIndex        =   27
            Top             =   1320
            Width           =   1335
         End
         Begin VB.Label Label2 
            Caption         =   "Valeur maxi :"
            Height          =   255
            Left            =   4200
            TabIndex        =   26
            Top             =   1680
            Width           =   1335
         End
         Begin VB.Label Label1 
            Caption         =   "Niveau de l'axe : "
            Height          =   255
            Left            =   600
            TabIndex        =   8
            Top             =   960
            Width           =   1335
         End
      End
      Begin VB.Frame Frame3 
         Height          =   5655
         Left            =   120
         TabIndex        =   17
         Top             =   480
         Width           =   7335
         Begin VB.TextBox Text7 
            Height          =   285
            Left            =   2160
            TabIndex        =   0
            Top             =   240
            Width           =   5055
         End
         Begin VB.ListBox List1 
            Height          =   645
            Left            =   2160
            TabIndex        =   3
            Top             =   1320
            Width           =   1935
         End
         Begin VB.TextBox Text2 
            Height          =   285
            Left            =   5280
            TabIndex        =   4
            Top             =   1320
            Width           =   1935
         End
         Begin VB.TextBox Text3 
            Height          =   285
            Left            =   5280
            TabIndex        =   5
            Top             =   1680
            Width           =   1935
         End
         Begin VB.ComboBox Combo3 
            Height          =   315
            Left            =   2160
            TabIndex        =   2
            Top             =   960
            Width           =   5055
         End
         Begin VB.CommandButton Command2 
            Caption         =   "&Ajouter"
            Height          =   255
            Left            =   600
            TabIndex        =   6
            Top             =   1680
            Width           =   1455
         End
         Begin VB.ComboBox Combo2 
            Height          =   315
            Left            =   2160
            TabIndex        =   1
            Top             =   600
            Width           =   5055
         End
         Begin MSComctlLib.ListView ListView2 
            Height          =   3495
            Left            =   120
            TabIndex        =   7
            Top             =   2040
            Width           =   7095
            _ExtentX        =   12515
            _ExtentY        =   6165
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   5
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Ligne"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Colonne"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Filtre"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Valeur mini"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Text            =   "Valeur maxi"
               Object.Width           =   2540
            EndProperty
         End
         Begin VB.Label Label10 
            Caption         =   "Affaire : "
            Height          =   255
            Left            =   600
            TabIndex        =   23
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label4 
            Caption         =   "Filtre :"
            Height          =   255
            Left            =   600
            TabIndex        =   22
            Top             =   1320
            Width           =   1455
         End
         Begin VB.Label Label5 
            Caption         =   "Valeur mini : "
            Height          =   255
            Left            =   4200
            TabIndex        =   21
            Top             =   1320
            Width           =   1335
         End
         Begin VB.Label Label6 
            Caption         =   "Valeur maxi :"
            Height          =   255
            Left            =   4200
            TabIndex        =   20
            Top             =   1680
            Width           =   1335
         End
         Begin VB.Label Label7 
            Caption         =   "Colonne : "
            Height          =   255
            Left            =   600
            TabIndex        =   19
            Top             =   960
            Width           =   1455
         End
         Begin VB.Label Label3 
            Caption         =   "Ligne"
            Height          =   255
            Left            =   600
            TabIndex        =   18
            Top             =   600
            Width           =   1455
         End
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Chercher"
      Height          =   375
      Left            =   5760
      TabIndex        =   15
      Top             =   6360
      Width           =   1695
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuDel 
         Caption         =   "Supprimer"
      End
   End
End
Attribute VB_Name = "frmSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Public Sub Init()

Dim i As Integer
Dim X As ListItem
Dim aRow As clsRow
Dim aCol As clsCol

'-> Titre de la feuille
Me.Caption = "Recherche"

'-> Ajouter une ligne par niveau de l'axe hi�rarchique
If aTb.UseAxe Then
    '-> Pour tous les niveaux de l'axe
    For i = 1 To aTb.NvAxeAnalyse
        '-> Ajouter une entr�e
        Me.Combo1.AddItem Entry(i, aTb.LibNvAxe, "|")
    Next 'Pour tous les niveaux de l'axe
End If 'Si on utilise un axe


'-> Ajout de la liste des lignes
For Each aRow In aTb.Lignes
    Me.Combo2.AddItem aRow.Libelle
Next

'-> Ajout de la liste des colonnes
For Each aCol In aTb.Colonnes
    Me.Combo3.AddItem aCol.Libelle
Next

'-> Ajouter la liste des filtres
Me.List1.AddItem "= Egal "
Me.List1.AddItem "<> Diff�rent"
Me.List1.AddItem "< Inf�rieur"
Me.List1.AddItem "> Sup�rieur"
Me.List1.AddItem "Compris entre"
Me.List1.AddItem "Contenant"
Me.List1.AddItem "Ne contenant pas"
Me.List1.AddItem "Commen�ant par"

Me.List2.AddItem "= Egal "
Me.List2.AddItem "<> Diff�rent"
Me.List2.AddItem "< Inf�rieur"
Me.List2.AddItem "> Sup�rieur"
Me.List2.AddItem "Compris entre"
Me.List2.AddItem "Contenant"
Me.List2.AddItem "Ne contenant pas"
Me.List2.AddItem "Commen�ant par"

'-> S�lections par d�faut
Me.Combo1.ListIndex = 0
Me.Combo2.ListIndex = 0
Me.Combo3.ListIndex = 0
Me.List1.ListIndex = 0
Me.List2.ListIndex = 0

'-> Format des listview
FormatListView Me.ListView2

End Sub



Private Sub Command1_Click()

'-> Charger la feuille de recherche
Load frmResult

'-> Initialiser
If frmSearch.Check1.Value = 1 Then
    frmResult.Init frmSearch.Combo2.ListIndex, frmSearch.Combo1.ListIndex + 1
Else
    frmResult.Init frmSearch.Combo2.ListIndex, 0
End If

frmResult.SearchData

Unload Me

'-> Afficher la feuille
frmResult.Show vbModal

End Sub


Private Sub Command2_Click()

Dim X As ListItem

Set X = Me.ListView2.ListItems.Add(, , Me.Combo2.Text)
X.SubItems(1) = Me.Combo3.Text
X.SubItems(2) = Me.List1.Text
X.SubItems(3) = Me.Text2.Text
X.SubItems(4) = Me.Text3.Text

FormatListView Me.ListView2


End Sub

Private Sub Command3_Click()

Dim X As ListItem

Set X = Me.ListView3.ListItems.Add(, , Me.Combo1.Text)
X.SubItems(1) = Me.List2.Text
X.SubItems(2) = Me.Text4.Text
X.SubItems(3) = Me.Text1.Text

FormatListView Me.ListView3


End Sub

Private Sub ListView2_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

If Button <> vbRightButton Then Exit Sub

If Me.ListView2.ListItems.Count = 0 Then Exit Sub

strRetour = ""
Me.PopupMenu Me.mnuPopup

If strRetour = "DEL" Then Me.ListView2.ListItems.Remove (Me.ListView2.SelectedItem.Index)

End Sub


Private Sub ListView3_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

If Button <> vbRightButton Then Exit Sub

If Me.ListView3.ListItems.Count = 0 Then Exit Sub

strRetour = ""
Me.PopupMenu Me.mnuPopup

If strRetour = "DEL" Then Me.ListView3.ListItems.Remove (Me.ListView3.SelectedItem.Index)

End Sub

Private Sub mnuDel_Click()
strRetour = "DEL"
End Sub

