VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCumul"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'***************************************
' Classe des cumuls
'***************************************

'-> Definition d'une collection des sous-cumuls
Public Cumuls As Collection
'-> Definition d'une collection de valeurs
Public Lignes As Collection
'-> Valeur de rupture du cumul
Public Key As String
'-> Handle de la feuille associ�e
Public HdlForm As Long
'-> Pointeur  vers le tableau associ� � ce cumul
Public aTb As clsTableau
'-> Cl� d'acc�s vers l'axe
Public KeyAxe As Collection


Private Sub Class_Initialize()
    Set Lignes = New Collection
    Set Cumuls = New Collection
    Set KeyAxe = New Collection
End Sub

Private Sub Class_Terminate()
    Set Lignes = Nothing
    Set Cumuls = Nothing
End Sub

Public Sub AddKeyLink(Ligne As String)

'---> Ajouter une r�f�rence pour faire le lien crois� entre l'axe et le d�tail

Dim i As Long

'-> Ne pas traiter les lignes � blanc
If Trim(Ligne) = "" Then Exit Sub

'-> On v�rifier si ce niveau d'axe n'est pas d�ja actualiser
For i = 1 To Me.KeyAxe.Count
    If UCase$(Trim(Me.KeyAxe(i))) = UCase$(Trim(Ligne)) Then
        '-> La ligne existe d�ja ne pas ajouter
        Exit Sub
    End If
Next 'Pour toutes les lignes de r�f�rence

'-> Ajouter dans la colleciton
Me.KeyAxe.Add Ligne

End Sub
