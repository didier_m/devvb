Attribute VB_Name = "fctanalyt"
Option Explicit

'-> Collection des tableaux ouverts dans l'�diteur
Public Tableaux As Collection
'-> Collection des cumuls sur axes d'analyse
Public AXES As Collection
'-> Collection des cumuls sur axes detail
Public DETAILS As Collection

'-> Variable d'�change entre les feuilles
Public strRetour As String

'-> Indique si la fen�tre de structure est affich�e
Public IsNaviga As Boolean

'-> API pour gestion des SPLITS
Public Type POINTAPI
    X As Long
    Y As Long
End Type
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function ScreenToClient Lib "user32" (ByVal hwnd As Long, lpPoint As POINTAPI) As Long


Sub main()

'---> Ligne de commande

'COMMANDE + | + CODETB +  | + FICHIER SCRIPT

'COMMANDE : EDIT -> pour �diter un tableau en particulier

Dim LigneCommande As String
Dim CodeTb As String
Dim FileName As String
Dim Commande As String
Dim Index As Integer

'-> Initialiser la collection des tableaux
Set Tableaux = New Collection
'-> Initialisation des collections de cumuls
Set AXES = New Collection
Set DETAILS = New Collection

'-> Recup du path de l'applic
RessourcePath = GetPath(App.Path)

'-> Chargement des messages multilangue de turboanalyt
If Not CreateMessProg("TurboAnalyt-0" & Index + 1 & ".lng") Then GoTo GestError

'-> Initialisation des parametres regionnaux ( separateurs decimales et milliers etc.... )
InitConvertion

'-> R�cup�rer la ligne de commande
LigneCommande = Trim(Command$)

'-> Initialisation des param�tres de r�pertoire
Call Initialisation

'-> Eclater la ligne de commande
Commande = Entry(1, LigneCommande, "|")
CodeTb = Entry(2, LigneCommande, "|")
FileName = Entry(3, LigneCommande, "|")

'-> Masquer la zone de navigation
IsNaviga = True

'-> Selon la ligne de commande
Select Case Commande

    Case ""
        MDIMain.Show
        '-> debut de code pierrot

End Select 'Selon la ligne de commande



Exit Sub
GestError:
    MsgBox "Probleme dans le chargement des libelles multi-langue...", vbExclamation
    End
    

End Sub

Public Sub CopyLig(ByRef LigDest As clsRow, ByRef LigRef As clsRow)

'---> Cette proc�dure effectue une copie d'une classe ligne

Dim aCell As clsCell
Dim CellRef As clsCell
Dim aFormat As clsFormatCell
Dim FormatRef As clsFormatCell
Dim aCdt As clsCondition
Dim CdtRef As clsCondition
Dim aValueCdt As clsValueCdt
Dim ValueCdtRef As clsValueCdt

'-> Copie des propri�t�s g�n�rales de la ligne
LigDest.Calcul = LigRef.Calcul
LigDest.CalculRtf = LigRef.CalculRtf
LigDest.Hauteur = LigRef.Hauteur
LigDest.HauteurTwips = LigRef.HauteurTwips
LigDest.Libelle = LigRef.Libelle
LigDest.TypeLig = LigRef.TypeLig
LigDest.Visible = LigRef.Visible

'-> Copier les collections de cellules
For Each CellRef In LigRef.Cellules
    '-> Cr�er une nouvelle cellule
    Set aCell = New clsCell
    '-> Code de la colonne associ�e
    aCell.ColonneCode = CellRef.ColonneCode
    '-> Dupliquer le format de la cellule
    CopyFormat aCell.FormatCell, CellRef.FormatCell
    '-> Ajouter dans mla collection
    LigDest.Cellules.Add aCell, "CELL_" & aCell.ColonneCode
Next 'Pour toutes les cellules de r�f�rence

'-> Copier les collections des conditions
For Each CdtRef In LigRef.Conditions
    '-> Cr�er une nouvelle condition
    Set aCdt = New clsCondition
    '-> Dupliquer ses propri�t�s
    aCdt.Colonne = CdtRef.Colonne
    aCdt.TypeCdt = CdtRef.TypeCdt
    aCdt.Progiciel = CdtRef.Progiciel
    '-> Copier ses valeurs de conditions
    For Each ValueCdtRef In CdtRef.ValueCdts
        '-> Cr�er une nouvelle valeur de condition
        Set aValueCdt = New clsValueCdt
        '-> Duppliquer ses propri�t�s
        aValueCdt.Operande = ValueCdtRef.Operande
        aValueCdt.Operation = ValueCdtRef.Operation
        aValueCdt.ValMaxi = ValueCdtRef.ValMaxi
        aValueCdt.ValMini = ValueCdtRef.ValMini
        '-> Ajouter dans les valeurs de cette condition
        aCdt.ValueCdts.Add aValueCdt, "VALCDT_" & aCdt.ValueCdts.Count + 1
    Next 'Pour toutes les valeurs de cette condition
    '-> Ajouter dans la collection des conditions
    LigDest.Conditions.Add aCdt, "CDT_" & UCase$(Trim(aCdt.Colonne)) & "_" & UCase$(Trim(aCdt.Progiciel))
Next 'Pour toutes les conditions


End Sub

Public Sub CopyCol(ByRef ColDest As clsCol, ByRef ColRef As clsCol)

'---> Cette proc�dure effectue une copie d'une classe colonne

ColDest.Largeur = ColRef.Largeur
ColDest.LargeurTwips = ColRef.LargeurTwips
ColDest.Libelle = ColRef.Libelle
ColDest.Calcul = ColRef.Calcul
ColDest.CalculRtf = ColRef.CalculRtf
ColDest.TypeCol = ColRef.TypeCol
ColDest.Visible = ColRef.Visible
ColDest.Progiciel = ColRef.Progiciel
ColDest.Field = ColRef.Field


End Sub

Public Function GetColor(aCmg As CommonDialog) As Long

'---> Cette proc�dure retoune uyne couleur

On Error GoTo EndError

'-> Afficher la boite de dialogue
aCmg.CancelError = True
aCmg.ShowColor
GetColor = aCmg.Color

Exit Function

EndError:

    GetColor = -12345

End Function

Public Sub CopyFormat(ByRef FormatDest As clsFormatCell, ByRef FormatRef As clsFormatCell)

'---> Cette proc�dure effecture une copie de format

FormatDest.Absolute = FormatRef.Absolute
FormatDest.BackColor = FormatRef.BackColor
FormatDest.ColorNeg = FormatRef.ColorNeg
FormatDest.FontBold = FormatRef.FontBold
FormatDest.FontColor = FormatRef.FontColor
FormatDest.FontItalic = FormatRef.FontItalic
FormatDest.FontName = FormatRef.FontName
FormatDest.FontSize = FormatRef.FontSize
FormatDest.FontUnderline = FormatRef.FontUnderline
FormatDest.Format = FormatRef.Format
FormatDest.Libelle = FormatRef.Libelle
FormatDest.NbDec = FormatRef.NbDec
FormatDest.Pourcentage = FormatRef.Pourcentage
FormatDest.UseColorNeg = FormatRef.UseColorNeg
FormatDest.UseSepMil = FormatRef.UseSepMil
FormatDest.InterneAlign = FormatRef.InterneAlign

End Sub

Public Function SaveTb(aTb As clsTableau) As Boolean

'---> Cette proc�dure enregistre un tableau dans un fichier ASCII
'-> Enregistrer le tableau � partir de sa propri�t� fileName

Dim StrTempo As String
Dim i As Integer
Dim aRow As clsRow
Dim aCol As clsCol
Dim aProgiciel As clsProgiciel
Dim aDes As clsDescriptif

'-> Supprimer le fichier
If Dir$(aTb.FileName, vbNormal) <> "" Then Kill aTb.FileName

'-> Enregistrer l'entete du tableau
WritePrivateProfileString "PARAM_ENTETE", "NAME", aTb.Name, aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "LIBEL", aTb.Libel & " ", aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "MATRICECOL", aTb.MatriceCol & " ", aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "MATRICELIG", aTb.MatriceLig & " ", aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "LARGEURENT", CStr(aTb.LargeurEnt), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "HAUTEURENT", CStr(aTb.HauteurEnt), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "LIBENT", aTb.LibEnt & " ", aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "GRIDLINES", CStr(aTb.pGridLines), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "GRIDLINESFIXED", CStr(aTb.pGridLinesFixed), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "APPEARANCE", CStr(aTb.pAppearance), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "BORDERSTYLE", CStr(aTb.pBorderStyle), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "GRIDLINEWIDTH", CStr(aTb.pGridLineWidth), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "BACKCOLORBKG", CStr(aTb.pBackColorBkg), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "BACKCOLORSEL", CStr(aTb.pBackColorSel), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "BACKCOLORFIXED", CStr(aTb.pBackColorFixed), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "GRIDCOLOR", CStr(aTb.pGridColor), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "GRIDCOLORFIXED", CStr(aTb.pGridColorFixed), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "ALLOWUSERRESIZING", CStr(aTb.pAllowUserResizing), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "HEADERBOLD", CStr(aTb.pHeaderBold), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "PLARGEUR", CStr(aTb.pLargeur), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "PHAUTEUR", CStr(aTb.pHauteur), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "USEAXE", CStr(CInt(aTb.UseAxe)), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "LIBAXE", aTb.LibAxe & " ", aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "NVAXEANALYSE", CStr(aTb.NvAxeAnalyse), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "USEDETAIL", CStr(CInt(aTb.UseDetail)), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "LIBDETAIL", aTb.LibDet & " ", aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "NVAXEDETAIL", CStr(aTb.NvAxeDetail), aTb.FileName

'-> Formattage des entetes du tableau
WritePrivateProfileString "PARAM_ENTETE", "FORMATENTETELIG", SaveFormatCell(aTb.FormatEnteteLig), aTb.FileName
WritePrivateProfileString "PARAM_ENTETE", "FORMATENTETECOL", SaveFormatCell(aTb.FormatEnteteCol), aTb.FileName

'-> Progiciels
StrTempo = ""
If aTb.Progiciels.Count <> 0 Then
    '-> Enregistrement de la liste des progiciels affect�s au tableau
    For Each aProgiciel In aTb.Progiciels
        '-> Mettre � jour la liste des progiciels
        If StrTempo = "" Then
            StrTempo = aProgiciel.Code
        Else
            StrTempo = StrTempo & "|" & aProgiciel.Code
        End If
        '-> Enregsitr� son descriptif
        For Each aDes In aProgiciel.Descriptifs
            WritePrivateProfileString "PROGICIEL_" & aProgiciel.Code, "FIELD|" & aDes.Field, aDes.Commentaire & "|" & aDes.Field & "|" & CStr(aDes.Entree), aTb.FileName
        Next 'Pour tous les �l�ments du descripif
        '-> Si axe d'analyse
        WritePrivateProfileString "PROGICIEL_" & aProgiciel.Code, "ISAXEANALYSE|", CStr(CInt(aProgiciel.IsAxeAnalyse)), aTb.FileName
        If aProgiciel.IsAxeAnalyse Then
            '-> Enregistr� chaque niveau de l'axe
            For Each aDes In aProgiciel.AxeAnalyses
                WritePrivateProfileString "PROGICIEL_" & aProgiciel.Code, "AXENIVEAU|" & aDes.Niveau, aDes.Field, aTb.FileName
            Next 'Pour tous les niveaux de l'axe hierarchique
        End If 'Si le progiciel continet l'acc�s hierarchique
        '-> Si acc�s au d�tail
        WritePrivateProfileString "PROGICIEL_" & aProgiciel.Code, "ISAXEDET|", CStr(CInt(aProgiciel.IsAxeDet)), aTb.FileName
        If aProgiciel.IsAxeDet Then
            '-> Enregistrer chaque niveau de l'acc�s au d�tail
            For Each aDes In aProgiciel.AxeDetails
                WritePrivateProfileString "PROGICIEL_" & aProgiciel.Code, "DETNIVEAU|" & aDes.Niveau, aDes.Field, aTb.FileName
            Next 'Pour tous les niveaux de l'acc�s au d�tail
        End If 'Si on utilise l'acc�s au d�tail
    Next 'Pour tous les progiciels
    '-> Mettre � jour la liste des progiciels affect�s � ce tableau
    WritePrivateProfileString "PARAM_ENTETE", "PROGICIELS", StrTempo, aTb.FileName
End If 'Sil y a des progiciels affect�s au tableau

'-> Enregistrement des lignes du tableau
For Each aRow In aTb.Lignes
    '-> Enregistrer la ligne
    SaveRow aRow, aTb.FileName
Next 'Pour toutes les lignes su tableau

'-> Enregistrer les colonnes du tableau
For Each aCol In aTb.Colonnes
    '-> Enregistrer la colonne
    SaveCol aCol, aTb.FileName
Next 'Pour toutes les colonnes

End Function

Private Function SaveCol(aCol As clsCol, Fichier As String)

'---> Cette fonction enregistre dans un fichier le param�trage d'un colonne

Dim StrTempo As String

'-> Code
StrTempo = "CODE�" & UCase$(aCol.Code)

'-> Libelle
StrTempo = StrTempo & "|LIBELLE�" & aCol.Libelle

'-> Visible
StrTempo = StrTempo & "|VISIBLE�" & CStr(CInt(aCol.Visible))

'-> TypeCol
StrTempo = StrTempo & "|TYPECOL�" & CStr(aCol.TypeCol)

'-> Largeur
StrTempo = StrTempo & "|LARGEUR�" & CStr(aCol.Largeur)

'-> LargeurTwips
StrTempo = StrTempo & "|LARGEURTWIPS�" & CStr(aCol.LargeurTwips)

'-> Calcul
StrTempo = StrTempo & "|CALCUL�" & aCol.Calcul

'-> Position
StrTempo = StrTempo & "|POSITION�" & CStr(aCol.Position)

'-> Champ affect�
StrTempo = StrTempo & "|FIELD�" & aCol.Field

'-> Progiciel
StrTempo = StrTempo & "|PROGICIEL�" & aCol.Progiciel

'-> Enregistrer dans le fichier ini
WritePrivateProfileString "PARAM_ENTETE", "COL_" & UCase$(aCol.Code), StrTempo, Fichier

End Function

Private Function SaveRow(aRow As clsRow, Fichier As String)

'---> Cette fonction enregistre dans un fichier le param�trage d'une ligne

Dim StrTempo As String
Dim aCell As clsCell
Dim aCdt As clsCondition

'-> Param g�n�ral de la ligne
WritePrivateProfileString "ROW_" & aRow.Code, "ROW_CODE", aRow.Code, Fichier
WritePrivateProfileString "ROW_" & aRow.Code, "ROW_LIBELLE", aRow.Libelle, Fichier
WritePrivateProfileString "ROW_" & aRow.Code, "ROW_TYPELIG", CStr(aRow.TypeLig), Fichier
WritePrivateProfileString "ROW_" & aRow.Code, "ROW_HAUTEUR", CStr(aRow.Hauteur), Fichier
WritePrivateProfileString "ROW_" & aRow.Code, "ROW_HAUTEURTWIPS", CStr(aRow.HauteurTwips), Fichier
WritePrivateProfileString "ROW_" & aRow.Code, "ROW_CALCUL", aRow.Calcul, Fichier
WritePrivateProfileString "ROW_" & aRow.Code, "ROW_VISIBLE", CStr(CInt(aRow.Visible)), Fichier
WritePrivateProfileString "ROW_" & aRow.Code, "ROW_POSITION", CStr(aRow.Position), Fichier

'-> Enregistrer les cellules
For Each aCell In aRow.Cellules
    WritePrivateProfileString "ROW_" & aRow.Code, "CELL_" & UCase$(aCell.ColonneCode), SaveFormatCell(aCell.FormatCell), Fichier
Next 'Pour toutes les cellules

'-> Enregistrer les conditions
For Each aCdt In aRow.Conditions
    WritePrivateProfileString "ROW_" & aRow.Code, "CDT_" & CStr(aCdt.Colonne) & "�" & CStr(aCdt.TypeCdt) & "�" & CStr(aCdt.Progiciel), SaveCdt(aCdt), Fichier
Next 'Pour toutes les conditions

End Function

Private Function SaveCdt(aCdt As clsCondition) As String

Dim aValCdt As clsValueCdt
Dim StrTempo As String

'-> Cr�er une chaine avec toutes les valeurs pour cette condition
For Each aValCdt In aCdt.ValueCdts
    If StrTempo = "" Then
        StrTempo = aValCdt.Operande & "�" & aValCdt.Operation & "�" & aValCdt.ValMini & "�" & aValCdt.ValMaxi
    Else
        StrTempo = StrTempo & "|" & aValCdt.Operande & "�" & aValCdt.Operation & "�" & aValCdt.ValMini & "�" & aValCdt.ValMaxi
    End If
Next 'Pour toutes les valeurs de cette condition

'-> Renvoyer les valeurs de cette condition
SaveCdt = StrTempo

End Function

Private Function SaveFormatCell(aFormatCell As clsFormatCell) As String

'---> Cette proc�dure renvoie une chaine d'enregistrement qui correspond � une classe de formattage

Dim StrTempo As String

'-> Police
StrTempo = "FONTNAME�" & aFormatCell.FontName

'-> Taille de la police
StrTempo = StrTempo & "|FONTSIZE�" & CStr(aFormatCell.FontSize)

'-> Gras
StrTempo = StrTempo & "|FONTBOLD�" & CStr(CInt(aFormatCell.FontBold))

'-> Italic
StrTempo = StrTempo & "|FONTITALIC�" & CStr(CInt(aFormatCell.FontItalic))

'-> Souslign�
StrTempo = StrTempo & "|FONTUNDERLINE�" & CStr(CInt(aFormatCell.FontUnderline))

'-> FontColor
StrTempo = StrTempo & "|FONTCOLOR�" & CStr(aFormatCell.FontColor)

'-> Couleur de fond
StrTempo = StrTempo & "|BACKCOLOR�" & CStr(aFormatCell.BackColor)

'-> Format de la cellule
StrTempo = StrTempo & "|FORMAT�" & CStr(aFormatCell.Format)

'-> Libelle
StrTempo = StrTempo & "|LIBELLE�" & aFormatCell.Libelle

'-> Decimales
StrTempo = StrTempo & "|DEC�" & CStr(aFormatCell.NbDec)

'-> Utilisation d'un s�parateur de milier
StrTempo = StrTempo & "|SEPMIL�" & CStr(CInt(aFormatCell.UseSepMil))

'-> Valeur absolue
StrTempo = StrTempo & "|ABS�" & CStr(CInt(aFormatCell.Absolute))

'-> %
StrTempo = StrTempo & "|%�" & CStr(CInt(aFormatCell.Pourcentage))

'-> Utilisation couleur n�gative
StrTempo = StrTempo & "|USECOLORNEG�" & CStr(CInt(aFormatCell.UseColorNeg))

'-> Couleur N�gative
StrTempo = StrTempo & "|COLORNEG�" & CStr(aFormatCell.ColorNeg)

'-> Alignement Interne de la cellule
StrTempo = StrTempo & "|INTERNEALIGN�" & CStr(aFormatCell.InterneAlign)

'-> Renvoyer la chaine de caract�re
SaveFormatCell = StrTempo

End Function

Public Function VerifDescro(aTb As clsTableau) As Boolean

'---> Cette proc�dure le param�trage dun tableau au niveau de l'axe hierarchique ou de l'acc�s au d�tail

Dim aProgiciel As clsProgiciel
Dim aDes As clsDescriptif
Dim FindOne As Boolean
Dim aLb As Libelle

'-> On charge les libelles
Set aLb = Libelles("FCTANALYT")

'-> Il faut v�rifier que le param�trage de l'utilisation de l'axe hierarchique est Ok
If aTb.UseAxe Then
    '-> V�rifier qu'il y a au moin un progiciel dans le tableau qui exporte l'axe hierarchique
    If aTb.Progiciels.Count = 0 Then GoTo ErrorProgiAxe
        
    '-> V�rifier qu'il ay au moins un progiciel qui exporte l'axe hierarchique
    FindOne = False
    For Each aProgiciel In aTb.Progiciels
        If aProgiciel.IsAxeAnalyse Then
            FindOne = True
            Exit For
        End If
    Next 'Pour tous les progiciels affect�s au tableau
    If Not FindOne Then GoTo ErrorProgiAxe
    
    '-> V�rifier que tous les niveaux de l'axe soient bien respect�s
    If aProgiciel.AxeAnalyses.Count <> aTb.NvAxeAnalyse Then GoTo ErrorProgiAxe
    
    '-> Pour tous les progiciels, v�rifier que ceux qui exportent l'axe soient bien respect�s
    For Each aProgiciel In aTb.Progiciels
        If aProgiciel.IsAxeAnalyse Then
            If aProgiciel.AxeAnalyses.Count <> aTb.NvAxeAnalyse Then
                strError = aLb.GetCaption(1) & aProgiciel.Code & " : " & Progiciels("PRO_" & aProgiciel.Code).Libelle
                GoTo ErrorProgiParam
            End If
        End If 'Si on utilise l'axe
    Next 'Pour tous les progiciels
End If 'Si on utilise un axe hierarchique

'-> V�rifier l'acc�s au d�tail
If aTb.UseDetail Then
    '-> On utilise l'acc�s au d�tail : tous les progiciels doivent l'utiliser
    For Each aProgiciel In aTb.Progiciels
        If Not aProgiciel.IsAxeDet Then
            strError = aLb.GetCaption(2) & aProgiciel.Code & " : " & Progiciels("PRO_" & aProgiciel.Code).Libelle
            GoTo ErrorProgiParam
        End If
    Next 'Pour tous les progiciels
    
    '-> V�rifier pour tous les progiciels que l'acc�s au d�tail est coh�rent par rapport � son parm�trage
    For Each aProgiciel In aTb.Progiciels
        If aProgiciel.AxeDetails.Count <> aTb.NvAxeDetail Then
            strError = aLb.GetCaption(3) & aProgiciel.Code & " : " & Progiciels("PRO_" & aProgiciel.Code).Libelle
            GoTo ErrorProgiParam
        End If
    Next 'Pour tous les progiciels
End If

'-> Renvoyer une valeur de succ�s
VerifDescro = True

Exit Function

ErrorProgiAxe: 'MESSPROG
    MsgBox aLb.GetCaption(4), vbCritical + vbOKOnly, aLb.GetCaption(5)
    Exit Function
    
ErrorProgiParam: 'MESSPROG
    MsgBox strError, vbCritical + vbOKOnly, aLb.GetCaption(6)
    Exit Function
    
End Function


