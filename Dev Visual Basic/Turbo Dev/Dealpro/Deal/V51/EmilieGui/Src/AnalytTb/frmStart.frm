VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmStart 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Report"
   ClientHeight    =   7710
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12015
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7710
   ScaleWidth      =   12015
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Height          =   615
      Left            =   10440
      Picture         =   "frmStart.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   7080
      Width           =   735
   End
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   11280
      Picture         =   "frmStart.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   7080
      Width           =   735
   End
   Begin MSComctlLib.ListView ListView3 
      Height          =   3615
      Left            =   4800
      TabIndex        =   5
      Top             =   3360
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   6376
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Libell�"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView ListView2 
      Height          =   2415
      Left            =   4800
      TabIndex        =   3
      Top             =   480
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   4260
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      Checkboxes      =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Fichier"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Progiciel"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Origine"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   6495
      Left            =   0
      TabIndex        =   0
      Top             =   480
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   11456
      View            =   1
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Code"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Libelle"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3360
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmStart.frx":0E14
            Key             =   "TAB"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmStart.frx":1AEE
            Key             =   "FILE"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label3 
      Caption         =   "S�lection des donn�es � int�grer : "
      Height          =   255
      Left            =   4800
      TabIndex        =   4
      Top             =   120
      Width           =   3615
   End
   Begin VB.Label Label2 
      Caption         =   "S�lection de l'axe de consultation : "
      Height          =   255
      Left            =   4800
      TabIndex        =   2
      Top             =   3000
      Width           =   3615
   End
   Begin VB.Label Label1 
      Caption         =   "Liste des tableaux disponibles : "
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   3495
   End
End
Attribute VB_Name = "frmStart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ToUnload As Boolean '-> Indique que les seclections sont OK pour d�chargement normal


Private Sub Command1_Click()

Dim X As ListItem
Dim FindOne As Boolean

'-> Faire ici les v�rification
If Me.ListView1.SelectedItem Is Nothing Then 'MESSPROG
    MsgBox "Veuillez s�lectionner un tableau.", vbExclamation + vbOKOnly, "Erreur"
    Me.ListView1.SetFocus
    Exit Sub
End If

'-> V�rifier qu'il y a au moins un fichier de donn�es de s�lectionner
For Each X In Me.ListView2.ListItems
    If X.Selected = True Then
        FindOne = True
        Exit For
    End If
Next
If Not FindOne Then
    MsgBox "Veuillez s�lectionner un fichier de donn�es.", vbExclamation + vbOKOnly, "Erreur"
    Me.ListView2.SetFocus
    Exit Sub
End If

'-> Ajouter aussi un axe s'il est s�lectionn�
If Not Me.ListView3.SelectedItem Is Nothing Then Files.Add Me.ListView3.SelectedItem.Tag

'-> Cr�er la liste des fichiers � int�grer
For Each X In Me.ListView2.ListItems
    If X.Checked Then
        '-> Ajouter dans la liste des fichiers � int�grer
        Files.Add X.Tag
    End If 'Si s�lectionn�
Next 'Pour tous les fichiers de donn�es

'-> Indiquer que l'on d�charge la feuille
ToUnload = True

'-> Setting de la variable de tableau
TabFileName = Me.ListView1.SelectedItem.Tag

'-> D�charger la feuille
Unload Me

End Sub

Private Sub Command2_Click()
Unload Me
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

Dim Rep As VbMsgBoxResult

'-> Demander confirmation
If Not ToUnload Then
    Rep = MsgBox("Quitter maintenant ?", vbQuestion + vbYesNo, "Confirmation")
    If Rep = vbNo Then
        Cancel = 1
        Exit Sub
    End If
    '-> Fin du programme
    End
End If


End Sub
Private Sub Form_Load()

'---> Charger la liste des tableaux de bords en cours

Dim FileName As String
Dim X As ListItem
Dim CodeTb As String
Dim Lib As String

'-> Anlayse de la liste des tableaux
FileName = Dir$(App.Path & "\Tableaux\*.data")
Do While FileName <> ""
    '-> R�cup�rer le code du tableau
    CodeTb = GetIniFileValue("PARAM_ENTETE", "NAME", App.Path & "\Tableaux\" & FileName)
    Lib = Trim(GetIniFileValue("PARAM_ENTETE", "LIBEL", App.Path & "\Tableaux\" & FileName))
    '-> Ajouter une entr�e dans le listview
    Set X = Me.ListView1.ListItems.Add(, , Lib, , "TAB")
    X.SubItems(1) = CodeTb
    '-> Sauvegarder son emplacement
    X.Tag = App.Path & "\Tableaux\" & FileName
    '-> Lire le fichier suivant
    FileName = Dir
Loop

'-> Initialiser la collection des fichiers charg�s
Set Files = New Collection

'-> Formatter le listview
FormatListView Me.ListView1

End Sub

Private Sub ListView1_DblClick()

'---> Afficher la liste des axes et des fichiers associ�s

Dim FileName As String
Dim X As ListItem
Dim CodeTb As String
Dim Lib As String
Dim Progiciel As String
Dim Origine As String

'-> Quitter si pas d'item s�lectionn�
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Bloquer la mise � jour de l'�cran
MDIMain.Enabled = False
Screen.MousePointer = 11
LockWindowUpdate MDIMain.hwnd

'-> Vider le listview des fichiers
Me.ListView2.ListItems.Clear

'-> Vider le listview des axes
Me.ListView3.ListItems.Clear

'-> Analyser le R�pertoire � la recherche des fichiers de donn�es pour ce tableau
FileName = Dir$(App.Path & "\Fichiers\*.data")
Do While FileName <> ""
    '-> R�cup�rer le code du tableau
    CodeTb = GetIniFileValue("PARAM", "TABLEAU", App.Path & "\Fichiers\" & FileName)
    '-> On v�rifie que le code tableau est le m�me
    If UCase$(Trim(CodeTb)) = UCase$(Trim(Me.ListView1.SelectedItem.SubItems(1))) Then
        '-> Get de son libell�
        Lib = GetIniFileValue("PARAM", "LIBEL", App.Path & "\Fichiers\" & FileName)
        '-> Get de son orgigine
        Origine = GetIniFileValue("PARAM", "ORIGINE", App.Path & "\Fichiers\" & FileName)
        '-> Get du progiciel d'affectation
        Progiciel = GetIniFileValue("PARAM", "PROGICIEL", App.Path & "\Fichiers\" & FileName)
        '-> V�rifier que le progiciel existe THIERRY et qu'il soit r�f�nc� dans le tableau
        If IsProgiciel(Progiciel) Then
            '-> Ajouter une entr�e dans le listview
            Set X = Me.ListView2.ListItems.Add(, , Lib)
            X.SubItems(1) = Progiciel
            X.SubItems(2) = Origine
            '-> Ajouter son emplacement dans le tag
            X.Tag = App.Path & "\Fichiers\" & FileName & "|" & Progiciel
        End If
    End If
    '-> Lire le fichier suivant
    FileName = Dir
Loop

'-> Formatter le listview
FormatListView Me.ListView2

'-> Analyser  le r�pertoire � la recherche des fichiers d'axe
FileName = Dir$(App.Path & "\Fichiers\*.axe")
Do While FileName <> ""
    '-> R�cup�rer le code du tableau
    CodeTb = GetIniFileValue("PARAM", "TABLEAU", App.Path & "\Fichiers\" & FileName)
    '-> On v�rifie que le code tableau est le m�me
    If UCase$(Trim(CodeTb)) = UCase$(Trim(Me.ListView1.SelectedItem.SubItems(1))) Then
        '-> Get de son libell�
        Lib = GetIniFileValue("PARAM", "LIBEL", App.Path & "\Fichiers\" & FileName)
        '-> Get du progiciel d'affectation
        Progiciel = GetIniFileValue("PARAM", "PROGICIEL", App.Path & "\Fichiers\" & FileName)
        '-> V�rifier que le progiciel existe THIERRY et qu'il soit r�f�nc� dans le tableau
        If IsProgiciel(Progiciel) Then
            '-> Ajouter une entr�e dans le listview
            Set X = Me.ListView3.ListItems.Add(, , Lib)
            '-> Ajouter son emplacement dans le tag
            X.Tag = App.Path & "\Fichiers\" & FileName & "|" & Progiciel
        End If
    End If
    '-> Lire le fichier suivant
    FileName = Dir
Loop

'-> Formatter le listview
FormatListView Me.ListView3

GestError:

    '-> D�bloquer la mise � jour de l'�cran
    MDIMain.Enabled = True
    Screen.MousePointer = 0
    LockWindowUpdate 0


End Sub
