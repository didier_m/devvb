VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmTableau 
   Caption         =   "Form1"
   ClientHeight    =   6900
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11280
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   460
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   752
   WindowState     =   2  'Maximized
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   4560
      Top             =   3120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   1575
      Left            =   0
      TabIndex        =   0
      Top             =   600
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   2778
      _Version        =   393216
      Rows            =   3
      WordWrap        =   -1  'True
      FocusRect       =   2
      AllowUserResizing=   3
      MouseIcon       =   "frmTableau.frx":0000
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Index           =   0
      Left            =   4560
      Top             =   960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   15
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":013A
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":024C
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":035E
            Key             =   "Properties"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":0470
            Key             =   "Macro"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":0582
            Key             =   "Delete"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":0694
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":07A6
            Key             =   "Cut"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":08B8
            Key             =   "Paste"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":09CA
            Key             =   "Help What's This"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":0ADC
            Key             =   "COL"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":0C36
            Key             =   "LIG"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":0D90
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":1267
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":1801
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTableau.frx":1B9B
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Height          =   330
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   10695
      _ExtentX        =   18865
      _ExtentY        =   582
      ButtonWidth     =   609
      ButtonHeight    =   582
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      ImageList       =   "imlToolbarIcons(0)"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   13
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SAVE"
            Object.ToolTipText     =   "Enregistrer le tableau"
            ImageKey        =   "Save"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PRINT"
            Object.ToolTipText     =   "Imprimer le tableau"
            ImageKey        =   "Print"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ADDROW"
            Object.ToolTipText     =   "Ins�rer une ligne"
            ImageKey        =   "LIG"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ADDCOL"
            Object.ToolTipText     =   "Ins�rer une colonne"
            ImageKey        =   "COL"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PROP"
            Object.ToolTipText     =   "Propri�t�s du tableau"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "AXE"
            Object.ToolTipText     =   "Axe de pr�sentation"
            ImageIndex      =   13
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "DESCRO"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "DISPLAY"
            Object.ToolTipText     =   "Affichage"
            ImageIndex      =   14
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "VIEWROW"
                  Text            =   "Afficher les lignes invisibles"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "VIEWLIBEL"
                  Text            =   "Afficher les libell�s"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EXPORTFILE"
            ImageIndex      =   15
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.ToolTipText     =   "Aide en ligne"
            Style           =   3
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "HELP"
            Object.ToolTipText     =   "Aide"
            ImageKey        =   "Help What's This"
         EndProperty
      EndProperty
   End
   Begin VB.Image Image5 
      Height          =   180
      Left            =   6720
      Picture         =   "frmTableau.frx":1CF5
      Top             =   1440
      Width           =   450
   End
   Begin VB.Image Image4 
      Height          =   450
      Left            =   6000
      Picture         =   "frmTableau.frx":21BF
      Top             =   1200
      Width           =   180
   End
   Begin VB.Image Image3 
      Height          =   240
      Left            =   7560
      Picture         =   "frmTableau.frx":26B9
      Top             =   1080
      Width           =   240
   End
   Begin VB.Image Image2 
      Height          =   450
      Left            =   6000
      Picture         =   "frmTableau.frx":2803
      Top             =   600
      Width           =   120
   End
   Begin VB.Image Image1 
      Height          =   120
      Left            =   6240
      Picture         =   "frmTableau.frx":297D
      Top             =   600
      Width           =   450
   End
End
Attribute VB_Name = "frmTableau"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Indique si on a fait des modifs et que l'on doit enregistrer avant de quitter
Dim ToSave As Boolean

'-> Pointeur vers l'objet tableau en cours
Dim aTb As clsTableau

'-> Pour d�tection de la ligne et de la colonne sur laquelle on click
Dim ClickX As Integer
Dim ClickY As Integer

'-> Pour Drag & Drop
Dim IsDrag As Boolean 'Indique qu'une op�ration de D&D est en cours (True)
Dim DrgCol As Boolean  'Indique si D&D sur une colonne (True) ou une ligne(False)
Dim MoveX As Integer 'Indique pour un D&D sur qu'elle colonne on se d�place
Dim MoveY As Integer 'Indique pour un D&D sur qu'elle ligne on se d�place
Dim OkDD As Boolean 'Indique si le lachage d'un DD d�clenche une op�ration de move
Dim OkDup As Boolean 'Indique si le lachage se fait sur une op�ration de Duplication

'-> Indique le node affect� � cet objet
Public aNodeKey As String

Private Sub GetDimensionsFlex()

    '---> Cette proc�dure analyse le flex grid pour redonner aux objets _
    lignes et colonnes leur hauteur respective
    
    Dim i As Integer
    
    '-> Redim des objets colonnes
    For i = 1 To Me.MSFlexGrid1.Cols - 1
        '-> Recup�rer un pointeru vers un objet colonne
        If aTb.GetCol(i).Visible Then
            aTb.SetLargeurCol i, Me.MSFlexGrid1.ColWidth(i), Me.ScaleX(Me.MSFlexGrid1.ColWidth(i), 1, 6)
        Else
            If aTb.DisplayVisibleObject Then
                aTb.SetLargeurCol i, Me.MSFlexGrid1.ColWidth(i), Me.ScaleX(Me.MSFlexGrid1.ColWidth(i), 1, 6)
            End If
        End If
    Next
    
    '-> Redim des objets lignes
    For i = 1 To Me.MSFlexGrid1.Rows - 1
        If aTb.GetRow(i).Visible Then
            aTb.SetHauteurLig i, Me.MSFlexGrid1.RowHeight(i), Me.ScaleY(Me.MSFlexGrid1.RowHeight(i), 1, 6)
        Else
            If aTb.DisplayVisibleObject Then
                aTb.SetHauteurLig i, Me.MSFlexGrid1.RowHeight(i), Me.ScaleY(Me.MSFlexGrid1.RowHeight(i), 1, 6)
            End If
        End If
    Next

End Sub


Public Sub Init(Optional aTbInit As clsTableau)

    '---> Cette proc�dure initialise la repr�sentation du tableau
    If aTbInit Is Nothing Then
        '-> Cr�ation d'un nouveau tableau
        Set aTb = New clsTableau
        aTb.Name = UCase$(Entry(1, strRetour, Chr(0)))
        aTb.Libel = Entry(2, strRetour, Chr(0))
        '-> AJouter dans la collection des tableaux
        Tableaux.Add aTb, "TB_" & UCase$(Trim(aTb.Name))
        '-> Indiquer que l'on doit enregistrer
        ToSave = True
        '-> Initialisation du FlexGrid
        Me.MSFlexGrid1.Cols = 1
        Me.MSFlexGrid1.Rows = 1
        '-> Mettre � jour le flexgrid
        aTb.DrawEntete aTb.DisplayCodeCol, Me.MSFlexGrid1
    Else
        '-> Initialiser le pointeur local
        Set aTb = aTbInit
        '-> Cr�er sa repr�sentation physique
        InitLoadingTb aTb, Me.MSFlexGrid1, MDIMain.RichTextBox1
        ToSave = False
    End If
    
    '-> Mettre � jour le libell�
    Me.Caption = Me.Caption & aTb.Name & " - " & aTb.Libel

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    Dim Rep As VbMsgBoxResult
    Dim aLb As Libelle
    
    '-> Chargement des messages
    Set aLb = Libelles("FRMTABLEAU")
    
    If ToSave Then
        '-> Demander la confirmation si on doit enregistrer MESSPROG
        Rep = MsgBox(aLb.GetCaption(4) & Chr(13) & aTb.Name, vbQuestion + vbYesNoCancel, aLb.GetCaption(5))
    
        '-> Annuler si necessaire
        If Rep = vbCancel Then
            Cancel = 1
            Exit Sub
        End If
    
        If Rep = vbYes Then SaveTb aTb
    End If
    
    '-> Supprimer l'icone du treeveiw
    MDIMain.TreeNaviga.Nodes.Remove (aNodeKey)
    
    '-> Supprimer de la collection
    Tableaux.Remove ("TB_" & Trim(UCase$(aTb.Name)))
    
    '-> On libere le pointeur
    Set aLb = Nothing

End Sub

Private Sub Form_Resize()

    On Error Resume Next
    
    Dim aRect As RECT
    
    GetClientRect Me.hwnd, aRect
    Me.MSFlexGrid1.Top = Me.Toolbar1.Height
    Me.MSFlexGrid1.Left = 0
    Me.MSFlexGrid1.Width = aRect.Right
    Me.MSFlexGrid1.Height = aRect.Bottom - Me.MSFlexGrid1.Top


End Sub

'---> Param�trage d'un tableau de bord
Private Sub Form_Load()

    Dim aLb As Libelle
    
    '-> Chargement des messages
    Set aLb = Libelles("FRMTABLEAU")
    
    '-> Titre de la fen�tre MESSPROG
    Me.Caption = aLb.GetCaption(1)
    
    '-> Init des menus MESSPROG
    Me.Toolbar1.Buttons("DISPLAY").ButtonMenus("VIEWROW").Text = aLb.GetCaption(2)
    Me.Toolbar1.Buttons("DISPLAY").ButtonMenus("VIEWLIBEL").Text = aLb.GetCaption(3)
    
    '-> Peut on imprimer
    Me.Toolbar1.Buttons("PRINT").Enabled = PrinterOK
    
    '-> On libere le pointeur memoire
    Set aLb = Nothing

End Sub

Private Sub MSFlexGrid1_DblClick()


    If ClickX <= 0 Then
        If ClickY <= 0 Then 'On est sur le coin sup�rieur gauche du browse
            '-> Editer l'entete
            DisplayProp
        Else
            ExecMenuLig "EDIT"
        End If
    Else
        If ClickY <= 0 Then '-> Editer une colonne
            ExecMenuCol "EDIT"
        Else '-> Edition d'une cellule
            EditCell
        End If
    End If
    
    '-> Indiquer que l'on doit enregistrer
    ToSave = True

End Sub
Private Sub ExecMenuCol(Operation As String)

    '-> Traiter l'info selon la nature du menu s�lectionn�
    
    Dim Rep As VbMsgBoxResult
    Dim aLb As Libelle
    
    '-> On charge les libelles de la form
    Set aLb = Libelles("FRMTABLEAU")
    
    Select Case Operation
        Case "EDIT" 'Editer une cellule
            '-> R�cup�rer les dimensions des cellules
            GetDimensionsFlex
            '-> Edition d'un colonne
            frmCol.TbCode = aTb.Name
            Set frmCol.aCol = aTb.GetCol(ClickX)
            frmCol.Initialisation
            frmCol.Show vbModal
            '-> Redessiner cette ligne
            aTb.DrawCol aTb.GetCol(ClickX).Code, Me.MSFlexGrid1, aTb.DisplayCodeCol
            '-> Indiquer que l'on doit sauvegarder
            ToSave = True
    
        Case "DEL" 'Suppression d'une colonne
            '-> Demander confirmation MESSPROG'
            Rep = MsgBox(aLb.GetCaption(6) & Chr(13) & aTb.GetCol(ClickX).Code & " : " & aTb.GetCol(ClickX).Libelle, vbExclamation + vbYesNo, aLb.GetCaption(7))
            If Rep = vbYes Then DelCol ClickX
    End Select
    
    '-> On libere le pointeur
    Set aLb = Nothing

End Sub

Private Sub DelCol(ByVal ColRef As Integer)

Dim aCol As clsCol
Dim i As Integer
Dim NbCol As Integer
Dim aRow As clsRow

'-> Bloquer la mise � jour de l'ocx
LockWindowUpdate Me.MSFlexGrid1.hwnd

'-> Get du nombre de colonnes
NbCol = Me.MSFlexGrid1.Cols - 1

'-> D�placer la colonne � la fin du tableau
Me.MSFlexGrid1.ColPosition(ColRef) = Me.MSFlexGrid1.Cols - 1

'-> Supprimer la colonne
Me.MSFlexGrid1.Cols = Me.MSFlexGrid1.Cols - 1

'-> Pour toutes les lignes , supprimer les cellules associ�es
For Each aRow In aTb.Lignes
    '-> Supprimer la cellule
    aRow.Cellules.Remove ("CELL_" & aTb.GetCol(ColRef).Code)
Next 'Pour toutes les lignes

'-> Supprimer l'objet colonne
aTb.Colonnes.Remove ("COL_" & aTb.GetCol(ColRef).Code)

'-> Mettre � jour la matrice
aTb.MatriceCol = DeleteEntry(aTb.MatriceCol, ColRef, "|")

'-> D�bloquer la mise � jour de l'�cran
LockWindowUpdate 0

'-> R�indexer les positions des colonnes dans le tableau
aTb.ReindexCol

'-> Indiquer que l'on doit enregistrer
ToSave = True

End Sub

Private Sub EditCell()

    Dim aRow As clsRow
    Dim aCell As clsCell
    Dim aCellDest As clsCell
    Dim ColDep As Integer
    Dim LigDep As Integer
    Dim ColMax As Integer
    Dim LigMax As Integer
    Dim i As Integer
    Dim j As Integer
    
    '-> Pointer sur la ligne sp�cifi�e
    Set aRow = aTb.GetRow(ClickY)
    Set frmFormatCell.aCell = aRow.Cellules("CELL_" & aTb.GetCol(ClickX).Code)
    frmFormatCell.InitTialisation
    '-> vider la variable de retour
    strRetour = ""
    '-> Pointer sur la cellule
    frmFormatCell.Show vbModal
    '-> Ne rien faire si on a annul�
    If strRetour = "" Then Exit Sub
    
    '-> Pointer sur la cellule
    Set aCell = aRow.Cellules("CELL_" & aTb.GetCol(ClickX).Code)
    
    '-> R�cup�rer les valeurs d'application
    ColDep = Me.MSFlexGrid1.Col
    LigDep = Me.MSFlexGrid1.Row
    LigMax = Me.MSFlexGrid1.RowSel
    ColMax = Me.MSFlexGrid1.ColSel
    '-> Bloquer la mise � jour de l'�cran
    LockWindowUpdate Me.MSFlexGrid1.hwnd
    '-> Appliquer le format � toutes les cellules s�mectionn�es
    For i = LigDep To LigMax
        '-> Pointer sur la ligne sp�cifi�e
        Set aRow = aTb.GetRow(i)
        For j = ColDep To ColMax
            '-> Pointer sur la cellule de destination
            Set aCellDest = aRow.Cellules("CELL_" & aTb.GetCol(j).Code)
            '-> Duplliquer le formattage de la cellule
            CopyFormat aCellDest.FormatCell, aCell.FormatCell
            '-> Appliquer la modification
            ExecFormatCell aCellDest.FormatCell, i, j, Me.MSFlexGrid1
        Next 'Pour toutes les colonnes
    Next 'Pour toutes les lignes
    
    '-> R�appliquer la solution
    Me.MSFlexGrid1.Row = LigDep
    Me.MSFlexGrid1.Col = ColDep
    Me.MSFlexGrid1.RowSel = LigMax
    Me.MSFlexGrid1.ColSel = ColMax
    '-> D�bloquer la mise � jour de l'�cran
    LockWindowUpdate 0

End Sub


Private Sub MSFlexGrid1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

    '-> R�cup�rer la ligne et la colonne
    ClickX = GetCol(Me.MSFlexGrid1, X)
    ClickY = GetRow(Me.MSFlexGrid1, Y)
    
    OkDD = False
    OkDup = False
    
    '*********************************************************
    '* Initialisation du D�placement Ligne / Colonne par D&D *
    '*********************************************************
    
    '-> Indiquer que l'on initialise une op�ration de gliss� d�plac�
    If Button = vbLeftButton Then
        '-> Ne pas autoriser un DD si on est sur l'entete
        If ClickX <= 0 Then
            If ClickY <= 0 Then 'On est sur le coin sup�rieur gauche du browse ne rien faire
                Exit Sub
            Else 'DD sur une ligne
                If Shift = 1 Then
                    '-> Duplication d'une ligne
                    OkDup = True
                Else
                    '-> Ne rien faire que si une seule ligne
                    If aTb.Lignes.Count = 1 Then Exit Sub
                End If
                '-> Initialiser une op�ration de D&D sur une ligne
                IsDrag = True
                '-> D&D sur une ligne
                DrgCol = False
            End If
        Else
            If ClickY <= 0 Then '-> D&D sur colonne
                If Shift = 1 Then
                    '-> Duplication d'une colonne
                    OkDup = True
                Else
                    '-> Ne rien faire si une seule colonne
                    If aTb.Colonnes.Count = 1 Then Exit Sub
                End If
                '-> Initialiser une op�ration de D&D sur une ligne
                IsDrag = True
                '-> D&D sur une colonne
                DrgCol = True
            Else 'on est sur une cellule
                '-> Ne rien faire
                Exit Sub
            End If
                
        End If  'Pour analyse de l'emplacement
    End If 'Si bouton gauche
        
    '***********************************
    '* Affichage des menus contextuels *
    '***********************************
    
    '-> Quitter si on pas cliqu� sur le bouton droit
    If Button <> vbRightButton Then Exit Sub
    
    '-> Vider la variable d'�change
    strRetour = ""
    
    '-> Afficher le bon menu contextuel
    If ClickX <= 0 Then
        If ClickY <= 0 Then 'On est sur le coin sup�rieur gauche du browse
        Else 'Menu Ligne
            Me.PopupMenu frmMenu.mnuTB
            ExecMenuLig strRetour
        End If
    Else
        If ClickY <= 0 Then '-> Menu Colonne
            '-> Afficher le menu contextuel
            Me.PopupMenu frmMenu.mnuTB
            '-> Selon le retour
            ExecMenuCol strRetour
        Else 'Menu Cellule
            '-> Modifier la cellule active si elle est positionn�e sur une ligne ou colonne
            If Me.MSFlexGrid1.Col <= 0 Then Me.MSFlexGrid1.Col = ClickX
            If Me.MSFlexGrid1.Row <= 0 Then Me.MSFlexGrid1.Row = ClickY
            '-> Afficher le menu contextuel
            Me.PopupMenu frmMenu.mnuCell
            '-> Tester le retour
            If strRetour = "FORMAT" Then EditCell
        End If
    End If

End Sub
Private Sub ExecMenuLig(Operation As String)

    Dim Rep As VbMsgBoxResult
    Dim aLb As Libelle
    
    
    '-> On charge les libelles multilangue
    Set aLb = Libelles("FRMTABLEAU")
    
    '---> Traiter l'info selon la nature du menu s�lectionn�
    Select Case Operation
    
        Case "EDIT" 'Editer une cellule
            '-> R�cup�rer les dimensions des cellules
            GetDimensionsFlex
            '-> Edition d'un ligne
            Set frmLig.aRow = aTb.GetRow(ClickY)
            frmLig.TbCode = UCase$(Trim(aTb.Name))
            frmLig.Initialisation
            frmLig.Show vbModal
            '-> Redessiner cette ligne
            aTb.DrawRow aTb.GetRow(ClickY).Code, Me.MSFlexGrid1, aTb.DisplayCodeCol
            '-> Indiquer que l'on doit sauvegarder
            ToSave = True
    
        Case "DEL" 'Suppression d'une cellule
            '-> Demander confirmation MESSPROG
            Rep = MsgBox(aLb.GetCaption(8) & Chr(13) & aTb.GetRow(ClickY).Code & " : " & aTb.GetRow(ClickY).Libelle, vbExclamation + vbYesNo, "Confirmation")
            If Rep = vbYes Then DelLig ClickY
    
    End Select
    
    '-> On libere le pointeur
    Set aLb = Nothing
    
End Sub

Private Sub DelLig(ByVal LigneRef As Integer)

    Dim aLig As clsRow
    Dim i As Integer
    Dim NbLig As Integer
    
    '-> Bloquer la mise � jour de l'ocx
    LockWindowUpdate Me.MSFlexGrid1.hwnd
    
    '-> Get du nombre de lignes
    NbLig = Me.MSFlexGrid1.Rows - 1
    
    '-> D�placer la ligne � la fin du tableau
    Me.MSFlexGrid1.RowPosition(LigneRef) = Me.MSFlexGrid1.Rows - 1
    
    '-> Supprimer la ligne
    Me.MSFlexGrid1.Rows = Me.MSFlexGrid1.Rows - 1
    
    '-> Supprimer l'objet ligne
    aTb.Lignes.Remove ("LIG_" & aTb.GetRow(LigneRef).Code)
    
    '-> Mettre � jour la matrice
    aTb.MatriceLig = DeleteEntry(aTb.MatriceLig, LigneRef, "|")
    
    '-> D�bloquer la mise � jour de l'�cran
    LockWindowUpdate 0
    
    '-> R�indexer les positions des lignes dans le tableau
    aTb.ReindexLig
    
    '-> Indiquer que l'on doit enregistrer
    ToSave = True

End Sub

Private Sub MSFlexGrid1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    '-> Ne faire quelquechose que si on a intialis� une op�ration de Drag/Drop
    If Not IsDrag Then Exit Sub
    
    '-> D�terminer l'emplacement sur lequel on se trouve
    MoveX = GetCol(Me.MSFlexGrid1, X)
    MoveY = GetRow(Me.MSFlexGrid1, Y)
    
    '-> Modifier le pointeur de la souris
    If DrgCol Then 'On bouge ou duplique une colonne
        If OkDup Then 'En duplication
            '-> Modifier le pointeur de la souris
            Set Me.MSFlexGrid1.MouseIcon = Me.Image4.Picture
            OkDD = True
        Else 'En d�placement
            '-> N'autoriser que si on bouge sur une colonne diff�rente de l'origine
            If MoveX = ClickX Then
                Set Me.MSFlexGrid1.MouseIcon = Me.Image3.Picture
                OkDD = False
            Else
                '-> Interdit sur la ligne pr�c�dente car on ins�rere toujours apr�s
                If MoveX = ClickX - 1 Then
                    Set Me.MSFlexGrid1.MouseIcon = Me.Image3.Picture
                    OkDD = False
                Else
                    Set Me.MSFlexGrid1.MouseIcon = Me.Image2.Picture
                    OkDD = True
                End If
            End If
        End If 'Si on bouge ou duplique
    Else 'On bouge une ligne
        If OkDup Then 'Duplication de ligne
            Set Me.MSFlexGrid1.MouseIcon = Me.Image5.Picture
            OkDD = True
        Else
            '-> N'autoriser que si on bouge sur une ligne diff�rente de l'origine
            If MoveY = ClickY Then
                Set Me.MSFlexGrid1.MouseIcon = Me.Image3.Picture
                OkDD = False
            Else
                '-> Interdit sur la ligne pr�c�dente car on ins�rere toujours apr�s
                If MoveY = ClickY - 1 Then
                    Set Me.MSFlexGrid1.MouseIcon = Me.Image3.Picture
                    OkDD = False
                Else
                    Set Me.MSFlexGrid1.MouseIcon = Me.Image1.Picture
                    OkDD = True
                End If
            End If
        End If 'Si on bouge ou duplique une ligne
    End If 'Selon si on bouge une ligne ou une colonne
    
    '-> Positionner le cursuer sur Custom
    Me.MSFlexGrid1.MousePointer = flexCustom

End Sub

Private Sub MSFlexGrid1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

    '-> On a relach� le bouton : fin de l'op�ration
    
    '-> Resituer le pointeur normal de la souris
    Me.MSFlexGrid1.MousePointer = flexDefault
    
    '-> Quitter si pas d'op�ration D&D
    If Not IsDrag Then Exit Sub
    
    '-> Ne rien faire si pas de D&D autoris�e
    If Not OkDD Then GoTo EndDD
    
    '-> Recup�rer les dimensions r�elles des lignes et colonnes
    GetDimensionsFlex
    
    '-> Traiter le DD
    If DrgCol Then 'On d�place une colonne
        If OkDup Then 'Duplication d'une colonne
            '-> Ne lancer la duplication que si la touche shift est appuy�e
            If Shift <> 1 Then GoTo EndDD
            DupCol ClickX, ClickX
        Else 'D�placement d'une colonne
            MoveCol ClickX, MoveX
        End If
    Else 'On d�place une ligne
        If OkDup Then 'Duplication d'une ligne
            '-> Ne lancer la duplication que si la touche shift est appuy�e
            If Shift <> 1 Then GoTo EndDD
            DupLig ClickY, MoveY
        Else 'D�placement ligne
            MoveLig ClickY, MoveY
        End If
    End If 'Si on d�place une ligne ou une colonne
    
    
EndDD:
    
    '-> Arreter le D&D
    IsDrag = False
    '-> Arreter la duplication
    OkDup = False

End Sub

Private Sub DupLig(ByVal IdOrg As Integer, ByVal IdDest As Integer)

    '---> Cette proc�dure duplique une ligne
    
    Dim aRow As clsRow
    Dim aNewRow As clsRow
    Dim CodeRow As String
    Dim LibelRow As String
    Dim aCol As clsCol
    Dim aCell As clsCell
    Dim aLb As Libelle
    
    On Error GoTo EndDup
    
    '-> Demander le nouveau code de la ligne
    strRetour = ""
    frmAdd.TypeAdd = 0
    frmAdd.Show vbModal
    If strRetour = "" Then Exit Sub
    
    '-> On charge les libelles multi-langue
    Set aLb = Libelles("FRMTABLEAU")
    
    '-> Recup�rer code et libel
    CodeRow = Entry(1, strRetour, Chr(0))
    LibelRow = Entry(2, strRetour, Chr(0))
    
    '-> V�rifier que le code soit correct
    If Not IsLegalName(CodeRow) Then    'MESSPROG
        MsgBox aLb.GetCaption(9) & Chr(13) & CodeRow, vbCritical + vbOKOnly, aLb.GetCaption(10)
        Exit Sub
    End If
    
    '-> V�rifier que le code n'existe pas d�ja
    If aTb.IsRowCode(CodeRow) Then 'MESSPROG
        MsgBox aLb.GetCaption(11), vbExclamation + vbOKOnly, aLb.GetCaption(10)
        Exit Sub
    End If
    
    '-> Bloquer la mise � jour de l'�cran
    LockWindowUpdate Me.MSFlexGrid1.hwnd
    MDIMain.MousePointer = 11
    MDIMain.Enabled = False
    
    '-> get d'un pointeur sur la ligne de r�f�rence
    Set aRow = aTb.GetRow(IdOrg)
    
    '-> Init d'un nouveau pointeur de type ligne
    Set aNewRow = New clsRow
    
    '-> Faire une duplication de la ligne de r�f�rence
    CopyLig aNewRow, aRow
    
    '-> Setting de son code
    aNewRow.Code = CodeRow
    aNewRow.Libelle = LibelRow
    
    '-> Ajouter dans la collection des lignes du tableau
    aTb.Lignes.Add aNewRow, "LIG_" & CodeRow
    
    '-> Ajout dans la matrice
    aTb.MatriceLig = AddEntryInMatrice(aTb.MatriceLig, IdDest + 1, CodeRow, "|")
    
    '-> R�indexer les positions de chaque ligne dans le tableau
    aTb.ReindexLig
    
    '-> Ajouter une ligne dans le FlexGrid : Index de la nouvelle ligne est toujours IdDest + 1
    Me.MSFlexGrid1.AddItem "", IdDest + 1
    'Me.MSFlexGrid1.RowHeight(IdDest + 1) = aNewRow.HauteurTwips
    
    '-> Dessiner l'entete de la ligne sp�cifi�e
    aTb.DrawRow CodeRow, Me.MSFlexGrid1, aTb.DisplayCodeCol
    
    '-> Appliquer le format � toutes les cellules de la ligne
    For Each aCell In aNewRow.Cellules
        '-> Dessiner la cellule sp�cifi�e
        ExecFormatCell aCell.FormatCell, aNewRow.Position, aTb.Colonnes("COL_" & aCell.ColonneCode).Position, Me.MSFlexGrid1
    Next 'Pour toutes les cellules dans la ligne
    
    '-> On libere le pointeur
    Set aLb = Nothing
    
EndDup:
    
    '-> D�bloquer l'�cran et restituer son pointeur
    MDIMain.MousePointer = 0
    MDIMain.Enabled = True
    '-> Lib�rer la mise � jour du flexgrid
    LockWindowUpdate 0
    
    '-> Indiquer que l'on doit sauvegarder
    ToSave = True
    
End Sub
Private Sub DupCol(ByVal IdOrg As Integer, ByVal IdDest As Integer)

    '---> Cette proc�dure duplique une colonne
    
    Dim aCol As clsCol
    Dim aNewCol As clsCol
    Dim CodeCol As String
    Dim LibelCol As String
    Dim aRow As clsRow
    Dim aCell As clsCell
    Dim aCellRef As clsCell
    Dim aLb As Libelle
    
    On Error GoTo EndDup
    
    '-> Demander le nouveau code de la colonne
    strRetour = ""
    frmAdd.TypeAdd = 1
    frmAdd.Show vbModal
    If strRetour = "" Then Exit Sub
    
    '-> On charge les libelles de la form
    Set aLb = Libelles("FRMTABLEAU")
    
    '-> Recup�rer code et libel
    CodeCol = Entry(1, strRetour, Chr(0))
    LibelCol = Entry(2, strRetour, Chr(0))
    
    '-> V�rifier que le code soit correct
    If Not IsLegalName(CodeCol) Then    'MESSPROG
        MsgBox aLb.GetCaption(12) & Chr(13) & CodeCol, vbCritical + vbOKOnly, aLb.GetCaption(10)
        Exit Sub
    End If
    
    '-> V�rifier que le code n'existe pas d�ja
    If aTb.IsColCode(CodeCol) Then 'MESSPROG
        MsgBox aLb.GetCaption(13), vbExclamation + vbOKOnly, aLb.GetCaption(10)
        Exit Sub
    End If
    
    '-> Bloquer la mise � jour de l'�cran
    LockWindowUpdate Me.MSFlexGrid1.hwnd
    MDIMain.MousePointer = 11
    MDIMain.Enabled = False
    
    '-> get d'un pointeur sur la colonne de r�f�rence
    Set aCol = aTb.GetCol(IdOrg)
    
    '-> Init d'un nouveau pointeur de type colonne
    Set aNewCol = New clsCol
    
    '-> Faire une duplication de la colonne de r�f�rence
    CopyCol aNewCol, aCol
    
    '-> Setting de son code
    aNewCol.Code = CodeCol
    aNewCol.Libelle = LibelCol
    
    '-> Ajouter dans la collection des colonnes du tableau
    aTb.Colonnes.Add aNewCol, "COL_" & CodeCol
    
    '-> Ajout dans la matrice
    aTb.MatriceCol = AddEntryInMatrice(aTb.MatriceCol, IdDest + 1, CodeCol, "|")
    
    '-> R�indexer les positions de chaque colonne dans le tableau
    aTb.ReindexCol
    
    '-> Ajouter une colonne dans le FlexGrid : Index de la nouvelle colonne est toujours IdDest + 1
    Me.MSFlexGrid1.Cols = Me.MSFlexGrid1.Cols + 1
    Me.MSFlexGrid1.ColPosition(Me.MSFlexGrid1.Cols - 1) = IdDest + 1
    
    '-> Ajouter une cellule dans toutes les lignes
    For Each aRow In aTb.Lignes
        '-> Cr�er une nouvelle cellule sur cette ligne
        Set aCell = New clsCell
        '-> Obtenir un pointeur vers la cellule de la colonne d'origine
        Set aCellRef = aRow.Cellules("CELL_" & aCol.Code)
        '-> Dupliquer le format de la cellule
        CopyFormat aCell.FormatCell, aCellRef.FormatCell
        '-> Modifier le code
        aCell.ColonneCode = aNewCol.Code
        '-> Ajouter dans la ligne
        aRow.Cellules.Add aCell, "CELL_" & aCell.ColonneCode
        '-> Dessiner la cellule sur chaque ligne
        ExecFormatCell aCell.FormatCell, aRow.Position, aNewCol.Position, Me.MSFlexGrid1
    Next
    
    '-> Dessiner l'entete de la colonne sp�cifi�e
    aTb.DrawCol CodeCol, Me.MSFlexGrid1, aTb.DisplayCodeCol
    
    '-> On libere le pointeur
    Set aLb = Nothing
    
EndDup:
    
    '-> D�bloquer l'�cran et restituer son pointeur
    MDIMain.MousePointer = 0
    MDIMain.Enabled = True
    '-> Lib�rer la mise � jour du flexgrid
    LockWindowUpdate 0
    '-> Indiquer que l'on doit sauvegarder
    ToSave = True

End Sub


Private Sub MoveLig(IdOrg As Integer, ByVal IdAfter As Integer)

    '---> Cette proc�dure d�place une ligne
    
    Dim CodeLig As String
    Dim Index As Long
    
    On Error GoTo EndMove
    
    '-> Bloquer la mise � jour de l'�cran
    LockWindowUpdate Me.MSFlexGrid1.hwnd
    
    '-> R�cup�rer le code de la ligne que l'on d�place
    CodeLig = UCase$(aTb.GetRow(IdOrg).Code)
    
    '-> D�rtermination de l'index de la nouvelle ligne
    If IdAfter > IdOrg Then
        Index = IdAfter
    Else
        Index = IdAfter + 1
    End If
    
    '-> D�placer la ligne au bon endroit
    Me.MSFlexGrid1.RowPosition(IdOrg) = Index
    
    '-> Supprimer l'entr�e de la matrice
    aTb.MatriceLig = DeleteEntry(aTb.MatriceLig, IdOrg, "|")
    
    '-> Recr�er l'entr�e au bon endroit dans la matrice
    aTb.MatriceLig = AddEntryInMatrice(aTb.MatriceLig, Index, CodeLig, "|")
    
    '-> R�indexer les positions des diff�rentes lignes dans le tableau
    aTb.ReindexLig
    
    '-> Indiquer que l'on doit enregistrer
    ToSave = True
    
EndMove:
        '-> D�bloquer la mise � jour de l'�cran
        LockWindowUpdate 0

End Sub

Private Sub MoveCol(IdOrg As Integer, ByVal IdAfter As Integer)

    '---> Cette proc�dure d�place une colonne
    
    Dim CodeCol As String
    Dim Index As Long
    
    On Error GoTo EndMove
    
    '-> Bloquer la mise � jour de l'�cran
    LockWindowUpdate Me.MSFlexGrid1.hwnd
    
    '-> R�cup�rer le code de la colonne que l'on d�place
    CodeCol = UCase$(aTb.GetCol(IdOrg).Code)
    
    '-> D�termination de l'index de la nouvelle colonne
    If IdAfter > IdOrg Then
        Index = IdAfter
    Else
        Index = IdAfter + 1
    End If
    
    '-> D�placer la colonne au bon endroit
    Me.MSFlexGrid1.ColPosition(IdOrg) = Index
    
    '-> Supprimer l'entr�e de la matrice
    aTb.MatriceCol = DeleteEntry(aTb.MatriceCol, IdOrg, "|")
    
    '-> Recr�er l'entr�e au bon endroit dans la matrice
    aTb.MatriceCol = AddEntryInMatrice(aTb.MatriceCol, Index, CodeCol, "|")
    
    '-> R�indexer les positions des diff�rentes colonnes dans le tableau
    aTb.ReindexCol
    
    '-> Indiquer que l'on doit enregistrer
    ToSave = True
    
EndMove:
        '-> D�bloquer la mise � jour de l'�cran
        LockWindowUpdate 0

End Sub


Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

    Select Case Button.Key
    
        Case "EXPORTFILE" 'Cr�er un fichier ASCII qui correspond � l'export du param�trage
            ExportFile
    
        Case "DESCRO" 'Gestionnaire de descriptif de fichier
            DisplayDescro
    
        Case "SAVE" 'Enregistrement du tableau
            SaveTableau
    
        Case "ADDCOL" 'Ajout d'une colonne
    
            '-> Saisie du code de la colonne
            strRetour = ""
            frmAdd.TypeAdd = 1
            frmAdd.Show vbModal
            If strRetour = "" Then Exit Sub
    
            '-> Lancer la proc�dure de cr�ation de l'objet
            Call AddColToBrowse
    
        Case "ADDROW" 'Ajout d'une ligne
    
            '-> Saisie du code de la ligne
            strRetour = ""
            frmAdd.TypeAdd = 0
            frmAdd.Show vbModal
            If strRetour = "" Then Exit Sub
    
            '-> lancer la proc�dure de cr�ation de la ligne
            Call AddRowToBrowse
    
        Case "PROP" 'Afficher la page de propri�t� des entetes
            Call DisplayProp
    
        Case "AXE" 'Afficher la page de propri�t� de l'axe
            Set frmAxeAnalyse.aTb = aTb
            frmAxeAnalyse.Initialisation
            frmAxeAnalyse.Show vbModal
    
            '-> Indiquer si on doit enregistrer
            If strRetour = "OK" Then
                ToSave = True
                strRetour = ""
            End If
    
    End Select 'Selon le bouton de la barre d'outils

End Sub

Private Sub ExportFile()

    '---> Cette proc�dure r�alise l'export vers un fichier ASCII du descriptif d'un progiciel
    
    
    '-> Setting du pointeur tableau
    Set frmParam.aTb = aTb
    
    '-> Afficher la feuille
    frmParam.Show vbModal


End Sub

Private Sub DisplayDescro()

    '---> Cette proc�dure affiche le descriptif d'un fihcier ascii associ� � un applicatif
    
    Dim i As Integer
    
    '-> Charger la feuille de gestion du descro en m�moire
    Set frmDescriptif.aTb = aTb
    
    '-> Afficher la feuille
    frmDescriptif.Show vbModal
    
    '-> Indiquer que l'on doit enregistrer
    ToSave = True

End Sub

Private Sub SaveTableau()

    Dim MyTb As clsTableau
    Dim Rep As VbMsgBoxResult
    Dim aLb As Libelle
    
    On Error GoTo SaveError
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMTABLEAU")
    
    '-> V�rifier le param�trage du tableau
    If Not VerifDescro(aTb) Then
        MsgBox aLb.GetCaption(14), vbCritical + vbOKOnly, aLb.GetCaption(10)
        Exit Sub
    End If
    
    '-> Demander le nom du fichier d'enregistrement MESSPROG
    Me.CommonDialog1.DialogTitle = aLb.GetCaption(15)
    Me.CommonDialog1.FileName = aTb.FileName
    Me.CommonDialog1.Filter = aLb.GetCaption(16)
    Me.CommonDialog1.DefaultExt = aLb.GetCaption(17)
    Me.CommonDialog1.ShowSave
    
    '-> V�rifier si le fichier existe d�ja
    If Dir$(Me.CommonDialog1.FileName, vbNormal) <> "" Then 'MESSPROG
        Rep = MsgBox(aLb.GetCaption(18), vbQuestion + vbYesNo, aLb.GetCaption(19))
        If Rep = vbNo Then Exit Sub
    End If
    
    '-> Setting de sa propri�t�
    aTb.FileName = Me.CommonDialog1.FileName
    
    '-> R�cup�rer les dimensions des lignes et colonnes
    GetDimensionsFlex
    
    '-> Lancer l'enregistrement du tableau
    SaveTb aTb
        
    '-> Indiquer que l'enregistrement est OK
    ToSave = False
    
    '-> On libere le pointeur
    Set aLb = Nothing
    
    Exit Sub
    
SaveError:



End Sub

Private Sub AddColToBrowse()

    '---> Cette proc�dure ajoute une colonne dans le browse
    
    Dim ColCode As String
    Dim ColLib As String
    Dim aCol As clsCol
    Dim aRow As clsRow
    Dim aCell As clsCell
    Dim aLb As Libelle
    
    '-> Get du code et du libell�
    ColCode = UCase$(Trim(Entry(1, strRetour, Chr(0))))
    ColLib = Trim(Entry(2, strRetour, Chr(0)))
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMTABLEAU")
    
    '-> V�rifier que le code soit correct
    If Not IsLegalName(ColCode) Then 'MESSPROG
        MsgBox aLb.GetCaption(20), vbCritical + vbOKOnly, aLb.GetCaption(10)
        Exit Sub
    End If
    
    '-> V�rifier qu'il y ait pas de "�"
    If InStr(1, ColCode, "�") <> 0 Then
        MsgBox aLb.GetCaption(20), vbCritical + vbOKOnly, aLb.GetCaption(10)
        Exit Sub
    End If
    
    '-> V�rifier qu'il n'existe pas d�ja MESSPROG
    If aTb.IsColCode(ColCode) Then
        MsgBox aLb.GetCaption(13), vbCritical + vbOKOnly, aLb.GetCaption(10)
        Exit Sub
    End If
    
    '-> Cr�er une nouvelle colonne
    Set aCol = New clsCol
    aCol.Code = ColCode
    aCol.Libelle = ColLib
    aCol.TypeCol = 0
    aCol.Visible = True
    aCol.LargeurTwips = Me.ScaleX(aCol.Largeur, 6, 1)
    
    '-> Ajout dans la collection
    aTb.Colonnes.Add aCol, "COL_" & ColCode
    
    '-> Ajout dans la matrice
    aTb.AddCol ColCode
    
    '-> Ajouter dans le flew grid
    Me.MSFlexGrid1.Cols = Me.MSFlexGrid1.Cols + 1
    Me.MSFlexGrid1.Col = Me.MSFlexGrid1.Cols - 1
    
    '-> Indiquer la position dans le flex
    aCol.Position = Me.MSFlexGrid1.Cols - 1
    
    '-> Ajouter une cellule par colonne
    For Each aRow In aTb.Lignes
        '-> Cr�er une cellule pour toutes les lignes
        Set aCell = New clsCell
        aCell.ColonneCode = UCase$(aCol.Code)
        '-> Ajouter dans la ligne
        aRow.Cellules.Add aCell, "CELL_" & aCell.ColonneCode
    Next
    
    '-> Indiquer que l'on doit enregistrer
    ToSave = True
    
    '-> Setting de la variable de l'�diteur
    Load frmCol
    frmCol.TbCode = aTb.Name
    Set frmCol.aCol = aCol
    
    '-> Initialisation de ses propri�t�s
    frmCol.Initialisation
    
    '-> Afficher la feuille
    frmCol.Show vbModal
    
    '-> Dessiner l'entete de la colonne
    aTb.DrawCol aCol.Code, Me.MSFlexGrid1, aTb.DisplayCodeCol

End Sub


Private Sub DisplayProp()

    '---> Cette proc�dure affiche le param�trage des entetes des lignes et colonnes'-> Appliquer les modifications
    
    '-> R�cup�rer la dimension de l'angle sup�rieru gauche
    aTb.LargeurEnt = Me.ScaleX(Me.MSFlexGrid1.ColWidth(0), 1, 6)
    aTb.HauteurEnt = Me.ScaleY(Me.MSFlexGrid1.RowHeight(0), 1, 6)
    aTb.LibEnt = Me.MSFlexGrid1.TextArray(0)
    
    '-> Setting de l'objet tableau
    Set frmParamEntete.aTb = aTb
    '-> Initialiser avec le param�trage
    frmParamEntete.Init
    '-> Afficher le feuille
    frmParamEntete.Show vbModal
    
    '-> Appliquer les modifications
    ExecTbParamOnFlex aTb, Me.MSFlexGrid1
    
    '-> Mettre � jour le libell� de l'angle
    Me.MSFlexGrid1.TextArray(0) = aTb.LibEnt
    
    '-> Mettre � jour le libell� de la feuille
    Me.Caption = Me.Caption & aTb.Name & " - " & aTb.Libel
    
    '-> Indiquer que l'on doit enregistrer
    ToSave = True

End Sub

Private Sub AddRowToBrowse()

    '---> Cette proc�dure ajoute une ligne dans le browse sp�cifi�
    
    Dim aRow As clsRow
    Dim CodeRow As String
    Dim LibelRow As String
    Dim aCol As clsCol
    Dim aCell As clsCell
    Dim aLb As Libelle
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMTABLEAU")
    
    '-> Recup�rer code et libel
    CodeRow = Entry(1, strRetour, Chr(0))
    LibelRow = Entry(2, strRetour, Chr(0))
    
    '-> V�rifier que le code soit correct
    If Not IsLegalName(CodeRow) Then    'MESSPROG
        MsgBox aLb.GetCaption(9) & Chr(13) & CodeRow, vbCritical + vbOKOnly, aLb.GetCaption(10)
        Exit Sub
    End If
    
    If InStr(1, CodeRow, "�") <> 0 Then 'MESSPROG
        MsgBox aLb.GetCaption(9) & Chr(13) & CodeRow, vbCritical + vbOKOnly, aLb.GetCaption(10)
        Exit Sub
    End If
    
    '-> V�rifier que le code n'existe pas d�ja
    If aTb.IsRowCode(CodeRow) Then 'MESSPROG
        MsgBox aLb.GetCaption(11), vbExclamation + vbOKOnly, aLb.GetCaption(10)
        Exit Sub
    End If
        
    '-> Cr�er une nouvelle Ligne
    Set aRow = New clsRow
    aRow.Code = CodeRow
    aRow.Libelle = LibelRow
    aRow.TypeLig = 0
    aRow.Visible = True
    
    '-> Ajout dans la collection
    aTb.Lignes.Add aRow, "LIG_" & CodeRow
    
    '-> Ajout dans la matrice
    aTb.AddRow CodeRow
    
    '-> Ajouter dans le flew grid
    Me.MSFlexGrid1.Rows = Me.MSFlexGrid1.Rows + 1
    Me.MSFlexGrid1.Row = Me.MSFlexGrid1.Rows - 1
    
    '-> Indiquer la position dans le flex
    aRow.Position = Me.MSFlexGrid1.Rows - 1
    
    '-> Ajouter une cellule par colonne
    For Each aCol In aTb.Colonnes
        '-> Cr�er une cellule dans la ligne pour chaque colonne
        Set aCell = New clsCell
        aCell.ColonneCode = aCol.Code
        '-> Ajouter dans la ligne
        aRow.Cellules.Add aCell, "CELL_" & aCell.ColonneCode
    Next
    
    '-> Indiquer que l'on doit enregistrer
    ToSave = True
    
    '-> Setting de la variable de l'�diteur
    Load frmLig
    Set frmLig.aRow = aRow
    
    '-> Initialisation de ses propri�t�s
    frmLig.TbCode = UCase$(Trim(aTb.Name))
    frmLig.Initialisation
    
    '-> Afficher la feuille
    frmLig.Show vbModal
    
    '-> Forcer le dessin de le l'entete de la colonne
    aTb.DrawRow CodeRow, Me.MSFlexGrid1, aTb.DisplayCodeCol
    
    '-> On libere le pointeur
    Set aLb = Nothing
    
End Sub


Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    
    Dim aLb As Libelle
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMTABLEAU")
    
    Select Case ButtonMenu.Key
    
        Case "VIEWROW"
            '-> Mettres � jour les dimensions des objets que si toutes lerz zones sont affich�es
            If aTb.DisplayVisibleObject Then GetDimensionsFlex
            '-> Inverser la propri�t�
            aTb.DisplayVisibleObject = Not (aTb.DisplayVisibleObject)
            '-> Modifier les libell�s des menus
            If aTb.DisplayVisibleObject Then
                ButtonMenu.Text = aLb.GetCaption(21)
            Else
                ButtonMenu.Text = aLb.GetCaption(22)
            End If
            '-> Masquer les lignes
            HiddeRow
            '-> Masquer les colonnes
            HiddeCol
            
        Case "VIEWLIBEL"
            '-> Inverser la propri�t�
            aTb.DisplayCodeCol = Not (aTb.DisplayCodeCol)
            '-> Modifier les libell�s des menus
            If aTb.DisplayCodeCol Then
                ButtonMenu.Text = aLb.GetCaption(23)
            Else
                ButtonMenu.Text = aLb.GetCaption(24)
            End If
            '-> R�cup�rer les dimensions du flex
            GetDimensionsFlex
            '-> Redessiner les entetes
            aTb.DrawEntete aTb.DisplayCodeCol, Me.MSFlexGrid1
    
    End Select
    
    '-> On libere le pointeur
    Set aLb = Nothing
    
End Sub

Private Sub HiddeRow()

    '---> Cette proc�dure Affiche ou masque les lignes invisibles
    
    Dim aRow As clsRow
    Dim i As Integer
    
    '-> Quitter si pas de lignes dans le tableau
    If aTb.Lignes.Count = 0 Then Exit Sub
    
    '-> Il faut afficher les lignes masqu�es
    For i = 1 To NumEntries(aTb.MatriceLig, "|")
        '-> R�cup�rer un pointeur vers la ligne
        Set aRow = aTb.GetRow(i)
        '-> Masquer ou afficher
        If Not aRow.Visible Then
            If aTb.DisplayVisibleObject Then
                Me.MSFlexGrid1.RowHeight(i) = aRow.HauteurTwips
            Else
                Me.MSFlexGrid1.RowHeight(i) = 0
            End If
        End If
    Next 'Pour toutes les lignes du tableau

End Sub

Private Sub HiddeCol()

    '---> Cette proc�dure Affiche ou masque les colonnes invisibles
    
    Dim aCol As clsCol
    Dim i As Integer
    
    '-> Quitter si pas de colonnes dans le tableau
    If aTb.Colonnes.Count = 0 Then Exit Sub
    
    '-> Il faut afficher les lignes masqu�es
    For i = 1 To NumEntries(aTb.MatriceCol, "|")
        '-> R�cup�rer un pointeur vers la colonne
        Set aCol = aTb.GetCol(i)
        '-> Masquer ou afficher
        If Not aCol.Visible Then
            If aTb.DisplayVisibleObject Then
                Me.MSFlexGrid1.ColWidth(i) = aCol.LargeurTwips
            Else
                Me.MSFlexGrid1.ColWidth(i) = 0
            End If
        End If
    Next 'Pour toutes les colonnes du tableau
    
    End Sub


