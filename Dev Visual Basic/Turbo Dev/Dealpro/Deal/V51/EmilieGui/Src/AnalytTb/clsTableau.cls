VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsTableau"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'---> D�finition d'une classe tableau

'-> Fichier d'enregistrement d'un tableau
Public FileName As String
'-> Code du tableau
Public Name As String
'-> Libell� associ� au tableau
Public Libel As String
'-> Matrice des colonnes du tableau
Public MatriceCol As String
'-> Matrice des lignes du tableau
Public MatriceLig As String
'-> Collections des colonnes
Public Colonnes As Collection
'-> Collection des lignes
Public Lignes As Collection

'-> Pour dessin du flexGrid
Public LargeurEnt As Long
Public HauteurEnt As Long
Public LibEnt As String

'-> Propri�t�s des ent�tes et du tableau
Public pGridLines As Integer
Public pGridLinesFixed As Integer
Public pAppearance As Integer
Public pBorderStyle As Integer
Public pGridLineWidth As Integer
Public pBackColorBkg As Long
Public pBackColorSel As Long
Public pGridColorFixed As Long
Public pGridColor As Long
Public pAllowUserResizing As Integer
Public pHeaderBold As Boolean

'-> Propri�t�s de format du tableau
Public pLargeur As Long
Public pHauteur As Long

Private pCouleurFd As Long

'-> Classe de param�trage des entetes de lignes
Public FormatEnteteLig As clsFormatCell
'-> Classe de param�trage des entetes de colonnes
Public FormatEnteteCol As clsFormatCell

'-> Pour gestion des affichages
Public DisplayVisibleObject As Boolean
Public DisplayCodeCol As Boolean

'-> Collection des progiciels
Public Progiciels As Collection
Public UseAxe As Boolean
Public LibAxe As String
Public UseDetail As Boolean
Public LibDet As String
Public NvAxeAnalyse As Integer
Public NvAxeDetail As Integer
Public LibNvAxe As String
Public LibNvDet As String


Public Sub AddRow(ByVal RowCode As String)

    '---> Cette proc�dure ajoute une ligne dans le tableau
    
    If MatriceLig = "" Then
        MatriceLig = UCase$(Trim(RowCode))
    Else
        MatriceLig = MatriceLig & "|" & UCase$(Trim(RowCode))
    End If

End Sub

Public Sub AddCol(ByVal ColCode As String)

    '---> Cette proc�dure ajoute une colonne dans le tableau
    
    If MatriceCol = "" Then
        MatriceCol = UCase$(Trim(ColCode))
    Else
        MatriceCol = MatriceCol & "|" & UCase$(Trim(ColCode))
    End If

End Sub

Public Function IsRowCode(RowCode As String) As Boolean

    '---> Proc�dure v�rifie si un code de ligne existe d�ja ou non
    
    Dim aRow As clsRow
    
    
    On Error GoTo EndError
    
    '-> Essayer de pointer sur l'objet ligne
    Set aRow = Me.Lignes("LIG_" & UCase$(Trim(RowCode)))
    
    IsRowCode = True
    
    Exit Function

EndError:

    IsRowCode = False

End Function

Public Function IsColCode(ColCode As String) As Boolean

'---> Proc�dure v�rifie si un code de ligne existe d�ja ou non

    Dim aCol As clsCol
    
    
    On Error GoTo EndError
    
    '-> Essayer de pointer sur l'objet ligne
    Set aCol = Me.Colonnes("COL_" & UCase$(Trim(ColCode)))
    
    IsColCode = True
    
    Exit Function

EndError:

    IsColCode = False

End Function

Private Sub Class_Initialize()

    '---> Initialiser les collections
    Set Colonnes = New Collection
    Set Lignes = New Collection
    Set Progiciels = New Collection
    
    '-> Initialiser les largeurs hauteurs
    LargeurEnt = 40
    HauteurEnt = 10
    
    '-> Initialisation des valeurs par d�faut du flex Grid
    pGridLines = 2 'Inset
    pGridLinesFixed = 2 'Inset
    pAppearance = 1 '3D
    pBorderStyle = 1 'Avec une bordure
    pGridLineWidth = 1 'Largeur des lignes et bordures par d�faut
    pBackColorBkg = &H808080
    pBackColorSel = &H8000000D
    pGridColorFixed = 0
    pGridColor = 0
    pAllowUserResizing = 3
    
    '-> Setting des formats d'entete
    Set FormatEnteteLig = New clsFormatCell
    Set FormatEnteteCol = New clsFormatCell
    
    '-> Init des valeurs par d�faut des ent�tes de colonnes
    FormatEnteteLig.FontBold = 0
    FormatEnteteLig.FontColor = 0
    FormatEnteteLig.FontItalic = 0
    FormatEnteteLig.FontUnderline = 0
    FormatEnteteLig.FontName = "Times New Roman"
    FormatEnteteLig.FontSize = 10
    FormatEnteteLig.InterneAlign = 5
    
    '-> Init des valeurs par d�faut des ent�tes de colonnes
    FormatEnteteCol.FontBold = 0
    FormatEnteteCol.FontColor = 0
    FormatEnteteCol.FontItalic = 0
    FormatEnteteCol.FontUnderline = 0
    FormatEnteteCol.FontName = "Times New Roman"
    FormatEnteteCol.FontSize = 10
    FormatEnteteCol.InterneAlign = 5
    pBackColorFixed = 12632256
    
    '-> Par d�faut on affiche les lignes � blanc
    DisplayVisibleObject = True
    DisplayCodeCol = False

End Sub


Public Property Get pBackColorFixed() As Long

    pBackColorFixed = pCouleurFd

End Property

Public Property Let pBackColorFixed(ByVal vNewValue As Long)

    pCouleurFd = vNewValue
    FormatEnteteLig.BackColor = vNewValue
    FormatEnteteCol.BackColor = vNewValue

End Property

Public Sub DrawRow(RowCode As String, aFlex As MSFlexGrid, DisplayCode As Boolean)

    '---> Cette proc�dure dessine l'entete d'une ligne de tableau
    
    Dim aRow As clsRow
    Dim aCell As clsCell
    Dim ColCode As String
    
    '-> Pointer sur la ligne sp�cifi�e
    Set aRow = Lignes("LIG_" & RowCode)
    
    '-> Mettre � jour le libell� de l'entete de la ligne
    If DisplayCode Then
        aFlex.TextArray(aRow.Position * aFlex.Cols) = aRow.Code
    Else
        aFlex.TextArray(aRow.Position * aFlex.Cols) = aRow.Libelle
    End If
    
    '-> Appliquer le formattage
    ExecFormatCell FormatEnteteLig, aRow.Position, 0, aFlex, True
    
    '-> Cas des lignes de cumul
    If aRow.TypeLig = 1 And Me.pHeaderBold Then aFlex.CellFontBold = True
        
    '-> Mettre � jour la hauteur de la ligne
    If aRow.Visible Then
        aFlex.RowHeight(aRow.Position) = aRow.HauteurTwips
    Else
        '-> Ne lui donner sa hauteru que si on est en affichage des objets masqu�s
        If Not DisplayVisibleObject Then
            aFlex.RowHeight(aRow.Position) = 0
        Else
            aFlex.RowHeight(aRow.Position) = aRow.HauteurTwips
        End If
    End If


End Sub

Public Sub DrawCol(ColCode As String, aFlex As MSFlexGrid, DisplayCode As Boolean)

    '---> Cette proc�dure dessine l'entete d'une colonne
    
    Dim aCol As clsCol
    
    
    
    '-> Pointer sur la ligne sp�cifi�e
    Set aCol = Colonnes("COL_" & ColCode)
    
    '-> Mettre � jour le libell� de l'entete de la colonne
    If DisplayCode Then
        aFlex.TextArray(aCol.Position) = aCol.Code
    Else
        aFlex.TextArray(aCol.Position) = aCol.Libelle
    End If
    
    '-> Formatter les cellules
    ExecFormatCell FormatEnteteCol, 0, aCol.Position, aFlex, True
    
    '-> Cas des colonnes de cumul
    If aCol.TypeCol = 1 And Me.pHeaderBold Then aFlex.CellFontBold = True
    
    '-> Mettre � jour la hauteur de la colonne
    If aCol.Visible Then
        aFlex.ColWidth(aCol.Position) = aCol.LargeurTwips
    Else
        If Not Me.DisplayVisibleObject Then
            aFlex.ColWidth(aCol.Position) = 0
        Else
            aFlex.ColWidth(aCol.Position) = aCol.LargeurTwips
        End If
    End If

End Sub


Public Sub DrawEntete(DisplayCode As Boolean, aFlex As MSFlexGrid)

    '---> Cette proc�dure redessine les entetes du tableau
    Dim aCol As clsCol
    Dim aRow As clsRow
    
    '-> Angle sup�rieur gauche du tableau
    aFlex.AllowUserResizing = pAllowUserResizing
    aFlex.Appearance = pAppearance
    aFlex.BackColorBkg = pBackColorBkg
    aFlex.BackColorFixed = pBackColorFixed
    aFlex.BackColorSel = pBackColorSel
    aFlex.BorderStyle = pBorderStyle
    aFlex.GridColorFixed = pGridColorFixed
    aFlex.GridColor = pGridColor
    aFlex.GridLines = pGridLines
    aFlex.GridLinesFixed = pGridLinesFixed
    aFlex.GridLineWidth = pGridLineWidth
    
    '-> Hauteur et largeur de l'entete gauche
    aFlex.RowHeight(0) = aFlex.Parent.ScaleY(HauteurEnt, 6, 1)
    aFlex.ColWidth(0) = aFlex.Parent.ScaleX(LargeurEnt, 6, 1)
    
    '-> Libelle de l'angle
    aFlex.TextArray(0) = Me.LibEnt
    
    '-> Redessiner les entetes de colonnes
    For Each aCol In Me.Colonnes
        Me.DrawCol aCol.Code, aFlex, DisplayCode
    Next 'Pour toutes les colonnes
    
    '-> Redessiner les entetes des lignes
    For Each aRow In Me.Lignes
        Me.DrawRow aRow.Code, aFlex, DisplayCode
    Next 'Pour toutes les colonnes

End Sub

Public Sub SetHauteurLig(IdLig As Integer, HauteurTwips As Long, Hauteur As Integer)

    '---> Cette proc�dure r�cup�re un pointeur syur un objet ligne et met � jour sa hauteur
    
    Dim aRow As clsRow
    
    '-> Get du pointeur
    Set aRow = GetRow(IdLig)
    '-> Mise � jour de la data
    aRow.HauteurTwips = HauteurTwips
    aRow.Hauteur = Hauteur

End Sub

Public Sub SetLargeurCol(IdCol As Integer, LargeurTwips As Long, Largeur As Integer)

    '---> Idem SetHauteurLig
    
    Dim aCol As clsCol
    
    '-> Get du pointeur
    Set aCol = GetCol(IdCol)
    '-> Mise � jour de la data
    aCol.LargeurTwips = LargeurTwips
    aCol.Largeur = Largeur

End Sub

Public Function GetCol(IdCol As Integer) As clsCol

    '---> Cette fonction retourne un objet colonne en fonction de la matrice
    
    '-> Get d'un pointeur
    If Me.Colonnes.Count = 0 Then
        Set GetCol = Nothing
    Else
        Set GetCol = Me.Colonnes("COL_" & Entry(IdCol, Me.MatriceCol, "|"))
    End If

End Function

Public Function GetRow(IdRow As Integer) As clsRow

    '---> Cette fonction retourne un objet ligne en fonction de la matrice
    
    '-> Get d'un pointeur
    If Me.Lignes.Count = 0 Then
        Set GetRow = Nothing
    Else
        Set GetRow = Me.Lignes("LIG_" & Entry(IdRow, Me.MatriceLig, "|"))
    End If

End Function


Public Sub ReindexLig()

    '---> Cette proc�dure r�affecte la propri�t� position de chaque objet ligne _
    en fonction de leur position dans la matrice
    
    Dim aRow As clsRow
    Dim i As Integer
    
    '-> Ne rien reindexer s'il n'y a plus de lignes
    If Me.MatriceLig = "" Then Exit Sub
    
    For i = 1 To NumEntries(Me.MatriceLig, "|")
        '-> Get d'un pointeur
        Set aRow = Me.Lignes("LIG_" & Entry(i, Me.MatriceLig, "|"))
        aRow.Position = i
    Next 'Pour toutes les lignes

End Sub


Public Sub ReindexCol()

    '---> Cette proc�dure r�affecte la propri�t� position de chaque objet colonne _
    en fonction de leur position dans la matrice
    
    Dim aCol As clsCol
    Dim i As Integer
    
    '-> Ne rien reindexer s'il n'y a plus de colonnes
    If Me.MatriceCol = "" Then Exit Sub
    
    For i = 1 To NumEntries(Me.MatriceCol, "|")
        '-> Get d'un pointeur
        Set aCol = Me.Colonnes("COL_" & Entry(i, Me.MatriceCol, "|"))
        aCol.Position = i
    Next 'Pour toutes les colonnes

End Sub
Public Sub AddProgiciel(Code As String)

    '---> Cette proc�dure ajoute un nouveau logiciel dans le tableau
    
    Dim aPro As clsProgiciel
    
    '-> V�rifier que ce progiciel n'existe pas d�ja
    If IsProgiciel(Code) Then Exit Sub
    
    '-> Ajouter le progiciel
    Set aPro = New clsProgiciel
    aPro.Code = Entry(2, Code, "_")
    aPro.Libelle = fctCommunanalyt.Progiciels(Code).Libelle
    
    Me.Progiciels.Add aPro, Code

End Sub
Public Function IsProgicielRef(Progiciel As String) As Boolean

    '---> Cette fonction a pour but de v�rifier s'il existe une r�f�rence _
    dans un objet � un progiciel
    
    Dim aRow As clsRow
    Dim aCol As clsCol
    Dim aCdt As clsCondition
    Dim aProgiciel As clsProgiciel
    
    '-> Rechercher une r�f�rence dans les lignes
    For Each aRow In Me.Lignes
        If aRow.TypeLig = 0 Then 'Ne chercher dans les conditions que si ligne de d�tail
            '-> rechercher dans les conditions
            For Each aCdt In aRow.Conditions
                If aCdt.Progiciel = Progiciel Then
                    'MESSPROG
                    strError = "Ligne : " & aRow.Code & " - " & aRow.Libelle & " Condition : " & aCdt.Colonne
                    IsProgicielRef = True
                    Exit Function
                End If
            Next 'Pour toutes les conditions
        ElseIf aRow.TypeLig = 1 Then   '-> Rechercher dans les formules de calcul
            If InStr(1, aRow.Calcul, "$PRO�" & Progiciel & "�") <> 0 Then
                strError = "Ligne : " & aRow.Code & " - " & aRow.Libelle & " dans la formule de calcul : " & Chr(13) & aRow.Calcul 'MESSPROG
                IsProgicielRef = True
                Exit Function
            End If
        End If 'Selon le type de lignes
    Next 'Pour toutes les lignes
    
    '-> Rechercher les r�f�rences dans les colonnes
    For Each aCol In Me.Colonnes
        If aCol.TypeCol = 0 Then 'Colonne li�e � un progiciel
            If aCol.Progiciel = Progiciel Then
                'MESSPROG
                strError = "Colonne : " & aCol.Code & " - " & aCol.Progiciel
                IsProgicielRef = True
                Exit Function
            End If
        ElseIf aCol.TypeCol = 1 Then 'Colonne de calcul
            If InStr(1, aCol.Calcul, "$PRO�" & Progiciel & "�") <> 0 Then
                strError = "Colonne : " & aCol.Code & " - " & aCol.Progiciel & " dans la formule de calcul : " & Chr(13) & aCol.Calcul 'MESSPROG
                IsProgicielRef = True
                Exit Function
            End If
        End If 'Selon le type de colonne
    Next 'Pour toutes les colonnes
    
    '-> Pointer sur le progiciel
    Set aProgiciel = Me.Progiciels("PRO_" & Progiciel)
    
    '-> V�rifier s'il n'est pas r�f�renc� dans l'axe hierarchique
    If aProgiciel.IsAxeAnalyse Then 'MESSPROG
        strError = "R�f�renc� dans l'utilisation de l'axe hi�rarchique"
        IsProgicielRef = True
        Exit Function
    End If
    
    '-> V�rifier s'il n'est pas r�f�renc� dans l'acc�s au d�tail
    If aProgiciel.IsAxeDet Then  'MESSPROG
        strError = "R�f�renc� dans l'utilisation de l'acc�s au d�tail"
        IsProgicielRef = True
        Exit Function
    End If
    
    '-> On n'a rien trouv�, renvoy� une valeur false
    IsProgicielRef = False

End Function
Public Function IsFieldRef(Progiciel As String, Field As String) As Boolean

    '---> Cette fonction a pour but de v�rifier si un champ est r�f�renc� dans un objet
    
    Dim aRow As clsRow
    Dim aCol As clsCol
    Dim aCdt As clsCondition
    Dim aPro As clsProgiciel
    Dim aDes As clsDescriptif
    
    '-> Rechercher une r�f�rence dans les lignes
    For Each aRow In Me.Lignes
        If aRow.TypeLig = 0 Then 'Ne chercher dans les conditions que si ligne de d�tail
            '-> Rechercher dans les conditions
            For Each aCdt In aRow.Conditions
                If aCdt.Progiciel = Progiciel Then
                    '-> On est dans le m�me progiciel : v�rifier s'il y a une ref au champ
                    If aCdt.Colonne = Field Then 'MESSPROG
                        strError = "Ligne :" & aRow.Code & " - " & aRow.Libelle & " Progiciel : " & aCdt.Progiciel & " Condition : " & aCdt.Colonne
                        IsFieldRef = True
                        Exit Function
                    End If
                End If
            Next 'Pour toutes les conditions
        ElseIf aRow.TypeLig = 1 Then   '-> Rechercher dans les formules de calcul 'MESSPROG
            If InStr(1, UCase$(aRow.Calcul), "$PRO�" & UCase$(Progiciel) & "�" & UCase$(Field) & "$") <> 0 Then
                strError = "LIGNE : " & aRow.Code & " - " & aRow.Libelle & " Formule de calcul : " & Chr(13) & aRow.Calcul
                IsFieldRef = True
                Exit Function
            End If
        End If 'Selon le type de lignes
    Next 'Pour toutes les lignes
    
    '-> Rechercher les r�f�rences dans les colonnes
    For Each aCol In Me.Colonnes
        If aCol.TypeCol = 0 Then 'Colonne li�e � un progiciel
            If aCol.Progiciel = Progiciel Then
                '-> On est dans le m�me progiciel : v�rifier le champ
                If aCol.Field = Field Then
                    'MESSPROG
                    strError = "Colonne : " & aCol.Code & " - " & aCol.Libelle & " Progiciel :" & aCol.Progiciel & " Field : " & aCol.Field
                    IsFieldRef = True
                    Exit Function
                End If
            End If
        ElseIf aCol.TypeCol = 1 Then 'Colonne de calcul
            If InStr(1, aCol.Calcul, "$PRO�" & UCase$(Progiciel) & "�" & UCase$(Field) & "$") <> 0 Then
                'MESSPROG
                strError = "Colonne : " & aCol.Code & " - " & aCol.Libelle & " Formule de calcul : " & aCol.Calcul
                IsFieldRef = True
                Exit Function
            End If
        End If 'Selon le type de colonne
    Next 'Pour toutes les colonnes
    
    '-> Get d'un pointeur vers le progiciel
    Set aPro = Progiciels("PRO_" & Progiciel)
    
    '-> V�rifier dans le param�trage de l'axe hierarchique
    For Each aDes In aPro.AxeAnalyses
        If aDes.Field = Field Then
            'MESSPROG
            strError = "Axe hierarchique du progiciel : " & aPro.Code & Progiciels("PRO_" & Progiciel).Libelle
            IsFieldRef = True
            Exit Function
        End If
    Next
    
    '-> V�rifier dans le param�trage de l'acc&s au d�tail
    For Each aDes In aPro.AxeDetails
        If aDes.Field = Field Then
            'MESSPROG
            strError = "Acc�s au d�tail  du progiciel : " & aPro.Code & Progiciels("PRO_" & Progiciel).Libelle
            IsFieldRef = True
            Exit Function
        End If
    Next
    
    '-> On n'a rien trouv�, renvoy� une valeur false
    IsFieldRef = False

End Function


Public Function IsProgiciel(Progiciel As String) As Boolean

    '---> Cette fonction d�termine si un progiciel est affect� � ce tableau
    
    Dim aPro As clsProgiciel
    
    On Error GoTo EndFunction
    
    '-> Essayer de poinbter sur le progiciel sp�cifi�
    Set aPro = Me.Progiciels(Progiciel)
    
    '-> Renvoyer une valeur de succ�s
    IsProgiciel = True
    
    Exit Function

EndFunction:
    '-> Renvoyer une valeur d'erreur
    IsProgiciel = False

End Function

Public Sub RemoveProgiciel(Progiciel As String)

    '---> Cette proc�dure supprime un progiciel des affectation
    
    Dim aPro As clsProgiciel
    Dim aDes As clsDescriptif
    
    '-> Get d'un pointeur vers le progiciel
    Set aPro = Me.Progiciels(Progiciel)
    
    '-> Supprimer tous les descriptifs et les axes
    Do While aPro.AxeAnalyses.Count <> 0
        aPro.AxeAnalyses.Remove (1)
    Loop
    Do While aPro.AxeDetails.Count <> 0
        aPro.AxeDetails.Remove (1)
    Loop
    Do While aPro.Descriptifs.Count <> 0
        aPro.Descriptifs.Remove (1)
    Loop
    
    '-> Supprimer du tableau
    Me.Progiciels.Remove (Progiciel)

End Sub

Public Function IsFieldInDescriptifProgiciel(Field As String, Progiciel As String) As Boolean

    '---> Cette fonction analyse le descriptif du progiciel sp�cifi� pour v�rifier que le champ n'existe pas d�ja
    
    Dim aPro As clsProgiciel
    Dim aDes As clsDescriptif
        
    On Error GoTo GestError
        
    '-> Get d'un pointeur vers le progiciel
    Set aPro = Me.Progiciels(Progiciel)
        
    '-> V�rifier si le progiciel ext affect� au tableau
    For Each aDes In aPro.Descriptifs
        If aDes.Field = Field Then
            IsFieldInDescriptifProgiciel = True
            Exit Function
        End If
    Next 'Pour tous les �l�ments du descriptif
        
GestError:
        
    '-> Indiquer qu'il n'existe pas
    IsFieldInDescriptifProgiciel = False

    

End Function
