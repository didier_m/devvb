VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmDescriptif 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   7845
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   9960
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   523
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   664
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   5400
      Top             =   6240
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDescriptif.frx":0000
            Key             =   "SELECTED"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDescriptif.frx":015A
            Key             =   "UNSELECTED"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   5535
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   9763
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   5730
      Left            =   4560
      TabIndex        =   0
      Top             =   0
      Width           =   5055
      _ExtentX        =   8916
      _ExtentY        =   10107
      _Version        =   393216
      Cols            =   3
      FixedCols       =   0
      ScrollBars      =   2
   End
End
Attribute VB_Name = "frmDescriptif"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Pointeur vers l'objet tableau de plus haut niveau
Public aTb As clsTableau

'-> Indique le progiciel en cours
Private IdProgi As String

Private Sub Form_Resize()

    Dim aRect As RECT
    
    On Error Resume Next
    
    '-> Get de la zone cliente
    GetClientRect Me.hwnd, aRect
    
    '-> Positionner le treeview
    Me.TreeView1.Left = 0
    Me.TreeView1.Top = 0
    Me.TreeView1.Height = aRect.Bottom - Me.TreeView1.Top
    Me.TreeView1.Width = aRect.Right / 2
    
    '-> Positionner le flex grid
    Me.MSFlexGrid1.Left = Me.TreeView1.Width
    Me.MSFlexGrid1.Top = Me.TreeView1.Top
    Me.MSFlexGrid1.Height = Me.TreeView1.Height
    Me.MSFlexGrid1.Width = Me.TreeView1.Width
    
    '-> Redimensionner les colonnes
    GetClientRect Me.MSFlexGrid1.hwnd, aRect
    Me.MSFlexGrid1.ColWidth(0) = Me.ScaleX(aRect.Right * (1.5 / 5), 3, 1)
    Me.MSFlexGrid1.ColWidth(1) = Me.ScaleX(aRect.Right / 2, 3, 1)
    Me.MSFlexGrid1.ColWidth(2) = Me.ScaleX(aRect.Right, 3, 1) - Me.MSFlexGrid1.ColWidth(0) - Me.MSFlexGrid1.ColWidth(1)

End Sub

Private Sub MSFlexGrid1_DblClick()

    '---> Modifier le libell� ou l'entry associ� � un champ
    
    '-> Quitter si pas de champ
    If Me.MSFlexGrid1.Rows = 1 Then Exit Sub
    
    '-> Quitter si click sur entete
    If Me.MSFlexGrid1.Row = 0 Then Exit Sub
    
    '-> Initialiser avec le code du champ s�lectionn�
    frmAdd.TypeAdd = 4
    frmAdd.ModifField = Me.MSFlexGrid1.TextArray(Me.MSFlexGrid1.Row * Me.MSFlexGrid1.Cols)
    frmAdd.Text2.Text = Me.MSFlexGrid1.TextArray(Me.MSFlexGrid1.Row * Me.MSFlexGrid1.Cols + 1)
    frmAdd.Text3.Text = Me.MSFlexGrid1.TextArray(Me.MSFlexGrid1.Row * Me.MSFlexGrid1.Cols + 2)
    frmAdd.Show vbModal
    
    '-> Mettre � jour si necessaire
    If strRetour = "" Then Exit Sub
    Me.MSFlexGrid1.TextArray(Me.MSFlexGrid1.Row * Me.MSFlexGrid1.Cols + 1) = Entry(2, strRetour, Chr(0))
    Me.MSFlexGrid1.TextArray(Me.MSFlexGrid1.Row * Me.MSFlexGrid1.Cols + 2) = Entry(3, strRetour, Chr(0))

End Sub

Private Sub Form_Load()

    On Error Resume Next
    
    Dim i As Integer
    Dim aNode As Node
    Dim aNode2 As Node
    Dim aProgiciel As clsProgiciel
    Dim aLb As Libelle
    
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMDESCRIPTIF")
    
    '-> Titre de la feuille MESSPROG
    Me.Caption = aLb.GetCaption(1)
    
    '-> titre des entetes MESSPROG
    Me.MSFlexGrid1.TextArray(0) = aLb.GetCaption(2)
    Me.MSFlexGrid1.TextArray(1) = aLb.GetCaption(3)
    Me.MSFlexGrid1.TextArray(2) = aLb.GetCaption(4)
    
    '-> Ajouter la liste des progiciels
    For Each aProgiciel In Progiciels  'MESSPROG
        '-> Ajouter le node du progiciel
        Set aNode = Me.TreeView1.Nodes.Add(, , "PRO_" & aProgiciel.Code, aProgiciel.Libelle)
        aNode.Tag = aLb.GetCaption(5)
        aNode.Expanded = True
        
        '-> V�rifier s'il est s�lectionn�
        If aTb.IsProgiciel("PRO_" & aProgiciel.Code) Then
            '-> Ajouter l'image
            aNode.Image = "SELECTED"
            '-> Pointer sur le progiciel
            Set aProgiciel = aTb.Progiciels("PRO_" & aProgiciel.Code)
        Else
            '-> Ajouter l'image
            aNode.Image = "UNSELECTED"
        End If
    Next
    
    '-> S�lectionner le premier node
    If Me.TreeView1.Nodes.Count <> 0 Then
        '-> Pointer sur le premier node
        Set Me.TreeView1.SelectedItem = Me.TreeView1.Nodes(1)
        '-> Positionner la cl�
        IdProgi = Entry(1, Me.TreeView1.SelectedItem.Key, "|")
        '-> Afficher le descro
        DisplayDescro IdProgi
    End If
    
    '-> On libere le pointeur
    Set aLb = Nothing
    
End Sub


Private Sub MSFlexGrid1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

    Dim ToDel As Boolean
    Dim aNode As Node
    Dim Rep As VbMsgBoxResult
    Dim aLb As Libelle
    
    '-> Ne rien faire si on ne clique pas sur avec le bouton droit
    If Button <> vbRightButton Then Exit Sub
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMDESCRIPTIF")
    
    '-> Ne rien faire s'il n'y a pas un progiciel de s�lectionn�
    If Me.TreeView1.SelectedItem Is Nothing Then
        MsgBox aLb.GetCaption(6), vbExclamation + vbOKOnly, aLb.GetCaption(7)
        Me.TreeView1.SetFocus
        Exit Sub
    End If
    
    '-> Pointer sur le node sp�cifi� par le progiciel
    Set aNode = Me.TreeView1.Nodes(IdProgi)
    
    '-> V�rifier que le node soit bien checked
    If aNode.Image = "UNSELECTED" Then  'MESSPROG
        '-> Demander de s�lectionner dans un premier temps le progiciel
        MsgBox aLb.GetCaption(8), vbExclamation + vbOKOnly, aLb.GetCaption(9)
        Me.TreeView1.SetFocus
        Exit Sub
    End If
    
    '-> Choix des menus
    If Me.MSFlexGrid1.Rows = 1 Then
        ToDel = False
    Else
        If Me.MSFlexGrid1.Row = 0 Then
            ToDel = False
        Else
            ToDel = True
        End If
    End If
    
    '-> Vider la variable de retour
    strRetour = ""
    
    '-> Afficher le menu contextuel
    frmMenu.mnuDelEntru.Enabled = ToDel
    Me.PopupMenu frmMenu.mnuDescroProgi
    
    '-> Selon la variable de retour
    Select Case strRetour
        Case "ADD" 'Ajout d'un champ dans le descriptif
            AddField
        Case "DEL" 'Suppression d'un champ dans le descriptif
            DelField
    End Select
    
    '-> Vider la variable de retour
    strRetour = ""
    
    Set aLb = Nothing
    
End Sub
Private Sub DelField()

    '---> Cette proc�dure supprime un node
    
    Dim aDes As clsDescriptif
    Dim aPro As clsProgiciel
    Dim Rep As VbMsgBoxResult
    Dim Field As String
    Dim aLb As Libelle
    
    '-> Quitter si necessaire
    If Me.MSFlexGrid1.Rows = 1 Then Exit Sub
    If Me.MSFlexGrid1.Row = 0 Then Exit Sub
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMDESCRIPTIF")
    
    '-> R�cup�rer le code du champ � supprimer
    Me.MSFlexGrid1.Col = 0
    Field = Me.MSFlexGrid1.Text
    
    '-> Demander confirmation avanr de supprimer MESSPROG
    Rep = MsgBox(aLb.GetCaption(10) & Field, vbQuestion + vbYesNo, aLb.GetCaption(11))
    If Rep = vbNo Then Exit Sub
    
    '-> V�rifier que ce champ n'est pas r�f�renc�
    If aTb.IsFieldRef(Entry(2, IdProgi, "_"), Field) Then 'MESSPROG
        MsgBox aLb.GetCaption(12) & Chr(13) & strError, vbCritical + vbOKOnly, aLb.GetCaption(7)
        Exit Sub
    End If
    
    '-> Pointer sur le progiciel
    Set aPro = aTb.Progiciels(IdProgi)
    
    '-> Supprimer le champ du r�f�renciel descriptif
    aPro.Descriptifs.Remove ("FIELD_" & Field)
    
    '-> Supprimer du browse
    If Me.MSFlexGrid1.Rows = 2 Then
        Me.MSFlexGrid1.Rows = 1
    Else
        Me.MSFlexGrid1.RemoveItem (Me.MSFlexGrid1.Row)
    End If
    
    Set aLb = Nothing
    
End Sub
Private Sub AddField()

    '---> cette proc�dure ajoute un champ dans un descriptif sp�cifi�
    
    Dim aDes As clsDescriptif
    Dim aPro As clsProgiciel
    Dim aLb As Libelle
    
    '-> Vider la variable de retour
    strRetour = ""
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMDESCRIPTIF")
    
    '-> Afficher la feuille de choix
    frmAdd.TypeAdd = 3
    frmAdd.Show vbModal
    
    '-> Quitter si retour = ""
    If strRetour = "" Then Exit Sub
    
    '-> V�rifier la legalit� du nom
    If Not IsLegalName(Entry(1, strRetour, Chr(0)), True) Then 'MESSPROG
        MsgBox aLb.GetCaption(13), vbCritical + vbOKOnly, aLb.GetCaption(7)
        Exit Sub
    End If
    
    '-> V�rifier que pour un descro particulier le code n'existe pas d�ja
    If aTb.IsFieldInDescriptifProgiciel(Entry(1, strRetour, Chr(0)), IdProgi) Then 'MESSPROG
        MsgBox aLb.GetCaption(14), vbExclamation + vbOKOnly, aLb.GetCaption(7)
        Exit Sub
    End If
    
    '-> Cr�er une nouvelle instance d'un objet descriptif
    Set aDes = New clsDescriptif
    
    '-> Init de ses propri�t�s
    aDes.Field = UCase$(Trim(Entry(1, strRetour, Chr(0))))
    aDes.Commentaire = Trim(Entry(2, strRetour, Chr(0)))
    aDes.Entree = CInt(Trim(Entry(3, strRetour, Chr(0))))
    aDes.Progiciel = Entry(2, IdProgi, "_")
    
    '-> Obtenir un pointeur vers le progiciel sp�cifi�
    Set aPro = aTb.Progiciels(IdProgi)
    
    '-> Ajouter le champ dans le tableau en cours
    aPro.Descriptifs.Add aDes, "FIELD" & "_" & aDes.Field
    
    '-> Mettre � jour le flex
    AddDesInFlex aDes
    
    '-> Positionner le focus sur le flex grid
    Me.MSFlexGrid1.SetFocus
    
    Set aLb = Nothing
    
End Sub

Private Sub AddDesInFlex(aDes As clsDescriptif)
    
    '-> Mettre � jour l'interface
    Me.MSFlexGrid1.Rows = Me.MSFlexGrid1.Rows + 1
    Me.MSFlexGrid1.Row = Me.MSFlexGrid1.Rows - 1
    Me.MSFlexGrid1.Col = 0
    Me.MSFlexGrid1.Text = aDes.Field
    Me.MSFlexGrid1.Col = 1
    Me.MSFlexGrid1.Text = aDes.Commentaire
    Me.MSFlexGrid1.Col = 2
    Me.MSFlexGrid1.Text = aDes.Entree

End Sub


Private Sub DisplayDescro(IdProgiciel As String)

    '---> Cette proc�dure r�alise l'affichage des champs d'un progiciels
    
    Dim aProgiciel As clsProgiciel
    Dim aDes As clsDescriptif
    
    '-> Vider dans un premier temps le flex
    Me.MSFlexGrid1.Rows = 1
    
    '-> V�rifier que ce progiciel soit bien r�f�renc� dans le tableau
    If Not aTb.IsProgiciel(IdProgiciel) Then Exit Sub
    
    '-> Get d'un pointeur vers le progiciel
    Set aProgiciel = aTb.Progiciels(IdProgiciel)
    
    '-> Le progiciel est r�f�renc� : afficher le descriptif
    For Each aDes In aProgiciel.Descriptifs
        '-> Ajouter dans le flex
        AddDesInFlex aDes
    Next 'Pour tous les objets descriptif


End Sub

Private Sub TreeView1_DblClick()
    
    Dim Rep As VbMsgBoxResult
    Dim Node As MSComctlLib.Node
    Dim aLb As Libelle
    
    '-> Quitter si pas de nodes
    If Me.TreeView1.SelectedItem Is Nothing Then Exit Sub
    
    Set aLb = Libelles("FRMDESCRIPTIF")
    
    '-> Pointer sur le node et le s�lectionner
    Set Node = Me.TreeView1.SelectedItem
    Node.Selected = True
    
    '->Mettre � jour la variable qui indique sur quel progiciel on a cliqu�
    IdProgi = Entry(1, Node.Key, "|")
    
    '-> Tester si on vient de cliquer sur un progiciel ou non
    If Node.Parent Is Nothing Then '-> On a cliqu� sur un progiciel
        '-> Le node est chequed : si ce progiciel n'est pas dans le tableau l'ajout�
        If Not aTb.IsProgiciel(IdProgi) Then
            '-> Ajouter une r�f�rence � ce progiciel dans le tableau
            aTb.AddProgiciel IdProgi
            Node.Image = "SELECTED"
        Else '-> On essaye de supprimer une ref vers un progiciel
            If aTb.IsProgicielRef(Entry(2, IdProgi, "_")) Then 'MESSPROG
                '-> Afficher un message d'erreur
                MsgBox aLb.GetCaption(15) & Chr(13) & strError, vbExclamation + vbOKOnly, aLb.GetCaption(7)
                GoTo DisplayAff
            Else
                '-> Il n'y a pas de r�f�rence : le supprimer MESSPROG
                Rep = MsgBox(aLb.GetCaption(16), vbQuestion + vbYesNo, aLb.GetCaption(11))
                If Rep = vbNo Then GoTo DisplayAff
                '-> Supprimer
                aTb.RemoveProgiciel (IdProgi)
                '-> Vider le browse
                Me.MSFlexGrid1.Rows = 1
                '-> Changer l'image
                Node.Image = "UNSELECTED"
            End If
        End If 'Si le progiciel sp�cifi� est r�f�renc�
    End If
    
    Set aLb = Nothing

DisplayAff:

'-> Afficher le descriptif de progiciel
DisplayDescro IdProgi

End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)

    '---> Cette proc�dure met � jour la variable interne qui indique sur quel progiciel on travail
    
    Dim aNode As Node
    
    '-> Tester que l'on ait bien cliqu� sur un node
    If Me.TreeView1.SelectedItem Is Nothing Then Exit Sub
    
    '-> Pointer sur le node
    Set aNode = Me.TreeView1.SelectedItem
    
    '->Mettre � jour la variable qui indique sur quel progiciel on a cliqu�
    IdProgi = Entry(1, aNode.Key, "|")
    
    '-> Afficher le descriptif de progiciel
    DisplayDescro IdProgi

End Sub

