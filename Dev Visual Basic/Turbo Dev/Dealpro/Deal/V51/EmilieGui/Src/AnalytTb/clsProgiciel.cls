VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsProgiciel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'***************************************
'* Classe de d�finition d'un progiciel *
'***************************************

Public Code As String
Public Libelle As String
Public Descriptifs As Collection
Public AxeAnalyses As Collection
Public AxeDetails As Collection
Public IsAxeAnalyse As Boolean
Public IsAxeDet As Boolean
Public MatriceAxe As String
Public MatriceDet As String

Private Sub Class_Initialize()

    '-> Initialiser le mod�le objet
    Set Descriptifs = New Collection
    Set AxeAnalyses = New Collection
    Set AxeDetails = New Collection

End Sub

Public Sub RemoveDescriptif(IdDescro As Integer)

    '---> Cette proc�dure supprime le descripif de l'axe (1) ou du d�tail (2)
    
    If IdDescro = 1 Then
        Do While AxeAnalyses.Count <> 0
            AxeAnalyses.Remove (1)
        Loop
    Else
        Do While AxeDetails.Count <> 0
            AxeDetails.Remove (1)
        Loop
    End If

End Sub
