VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmNaviga 
   Caption         =   "Form1"
   ClientHeight    =   7275
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4305
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   485
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   287
   Visible         =   0   'False
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4440
      Top             =   720
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNaviga.frx":0000
            Key             =   "OPEN"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNaviga.frx":059A
            Key             =   "CLOSE"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   1575
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   2778
      _Version        =   393217
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuFind 
         Caption         =   "Chercher"
      End
   End
End
Attribute VB_Name = "frmNaviga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public IsCumul As Boolean




Private Sub Form_Load()

Me.Width = 4425
Me.Height = 7680

End Sub

Private Sub Form_Resize()

On Error Resume Next

Dim aRect As RECT

GetClientRect Me.hwnd, aRect

Me.TreeView1.Left = 0
Me.TreeView1.Top = 0
Me.TreeView1.Height = aRect.Bottom
Me.TreeView1.Width = aRect.Right

End Sub

Private Sub mnuAnalyse_Click()
strRetour = "ANALYSE"
End Sub

Private Sub mnuFind_Click()
strRetour = "FIND"
End Sub

Private Sub TreeView1_DblClick()

'-> Ne rien faire si pas d'icones de s�lectionn�
If Me.TreeView1.SelectedItem Is Nothing Then Exit Sub

'-> Ne traiter que les nodes de cumul
If Me.TreeView1.SelectedItem.Tag = "" Then Exit Sub

'-> Afficher le tableau qui correpsond � ce cumul
DisplayTB Me.TreeView1.SelectedItem.Key, Me.IsCumul

End Sub

Private Sub TreeView1_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

If Button <> vbRightButton Then Exit Sub
If Me.TreeView1.SelectedItem Is Nothing Then Exit Sub

strRetour = ""
Me.PopupMenu Me.mnuPopup

If strRetour = "FIND" Then
    Load frmSearch
    frmSearch.Init
    frmSearch.Show vbModal
    Exit Sub
Else


End If
End Sub

