Attribute VB_Name = "fctCommunanalyt"
Option Explicit

'---> Ce module est utilis� par l'�diteur de tableau ainsi que par le moteur d'�dition

Public Type Progiciel
    Code As String
    Libelle As String
End Type

'-> Message windows pour activer une fen�tre fille
'Public Const WM_CHILDACTIVATE& = &H22

'-> Pour param�trage
Public ImpShell As String

'-> Indique si le moteur d'impression est OK
Public PrinterOK As Boolean

'-> Pour concataination des op�randes
Public OperationCdt(0 To 2) As String

'-> Pour gestion des op�rations sur condition
Public Operandes(1 To 8) As String

'-> Pour gestion des alignements internes des cellules
Public CellAlign(0 To 8) As String

'-> Pour gestion des progiciels
Public Progiciels As Collection

'-> Nom et emplacement du fichier ini associ�
Public IniFile As String

'-> Variable globale pour gestion des erreur
Public strError As String

'-> Path de l'applic
Public RessourcePath As String
Public ImpPath As String
Public BasePath As String

'-> Pour dessin de la temporisation
Public TailleLue As Long
Public TailleTotale As Long

Public Sub ExecTbParamOnFlex(aTb As clsTableau, aFlex As MSFlexGrid)

    '---> Cette proc�dure applique un param�trage tableau � un flexgrid
    
    On Error Resume Next
    
    Dim i As Integer
    Dim aLig As clsRow
    Dim aCol As clsCol
    
    '-> Param�trage g�n�ral
    aFlex.AllowUserResizing = aTb.pAllowUserResizing
    aFlex.Appearance = aTb.pAppearance
    aFlex.BackColorBkg = aTb.pBackColorBkg
    aFlex.BackColorFixed = aTb.pBackColorFixed
    aFlex.BackColorSel = aTb.pBackColorSel
    aFlex.BorderStyle = aTb.pBorderStyle
    aFlex.GridColorFixed = aTb.pGridColorFixed
    aFlex.GridColor = aTb.pGridColor
    aFlex.GridLines = aTb.pGridLines
    aFlex.GridLinesFixed = aTb.pGridLinesFixed
    aFlex.GridLineWidth = aTb.pGridLineWidth
    
    '-> Hauteur et largeur de l'entete gauche
    aFlex.RowHeight(0) = aFlex.Parent.ScaleY(aTb.HauteurEnt, 6, 1)
    aFlex.ColWidth(0) = aFlex.Parent.ScaleX(aTb.LargeurEnt, 6, 1)
    
    '-> Appliquer aux entetes des lignes
    For i = 0 To aFlex.Rows - 1
        ExecFormatCell aTb.FormatEnteteLig, i, 0, aFlex
    Next
    
    '-> Appliquer aux entetes de colonnes
    For i = 1 To aFlex.Cols - 1
        ExecFormatCell aTb.FormatEnteteCol, 0, i, aFlex
    Next
    
    '-> Rafraichir l'objet
    aFlex.Refresh

End Sub

Public Sub Initialisation()

    '---> Cette proc�dure lit les variables de param�trage dans le fichier TurboAnalyt.ini
    
    On Error GoTo TrapError
    
    Dim Rep As String
    Dim ErrorCode As Integer
    
    
    '-> Init
    IniFile = App.Path & "\TurboAnalyt.ini"
    
    '-> Moteur d'impression
    ErrorCode = 1
    Rep = GetIniFileValue("PARAM", "IMPSHELL", IniFile)
    If Rep <> "" Then
        '-> Replace de $APP$
        Rep = Replace(UCase$(Rep), "$APP$", App.Path)
        If Dir$(Rep, vbNormal) <> "" Then
            '-> Affectation OK
            ImpShell = Rep
            PrinterOK = True
        End If
    End If
    
    '-> Charger la liste des progiciels
    LoadProgiciel
    
    '-> Initialiser le tableau des op�randes
    Operandes(1) = "= Egal "
    Operandes(2) = "<> Diff�rent"
    Operandes(3) = "< Inf�rieur"
    Operandes(4) = "> Sup�rieur"
    Operandes(5) = "Compris entre"
    Operandes(6) = "Contenant"
    Operandes(7) = "Ne contenant pas"
    Operandes(8) = "Commen�ant par"
    
    '-> Pour gestion des conditions
    OperationCdt(0) = "ET"
    OperationCdt(1) = "OU"
    
    '-> Pour gestion des alignements internes
    CellAlign(0) = "Gauche, en haut"
    CellAlign(1) = "Gauche, au centre"
    CellAlign(2) = "Gauche, en bas"
    CellAlign(3) = "Centre, en haut"
    CellAlign(4) = "Centre, au centre"
    CellAlign(5) = "Centre, en bas"
    CellAlign(6) = "Droite, en haut"
    CellAlign(7) = "Droite, au centre"
    CellAlign(8) = "Droite, en bas"
    
    Exit Sub
    
TrapError:
    
        Select Case ErrorCode
            Case 1 'Pb sur l'imprimante
                PrinterOK = False
        End Select
        
End Sub
Private Sub LoadProgiciel()

    '---> Cette proc�dure charge la liste des progiciels
    
    Dim lpSection As String
    Dim lpKey As String
    Dim Res As Long
    Dim i As Integer
    Dim aProgiciel As clsProgiciel
    
    '-> Initialiser la collection des progiciels
    Set Progiciels = New Collection
    
    '-> R�cu�rer le contenu de la section progiciels
    lpSection = Space$(9000)
    Res = GetPrivateProfileSection("PROGICIELS", lpSection, Len(lpSection), IniFile)
    If Res <> 0 Then
        '-> Vider les blancs
        lpSection = Trim(lpSection)
        '-> R�cup�rer en boucle la liste de tous les progiciels
        For i = 1 To NumEntries(lpSection, Chr(0))
            '-> Get d'une d�finition
            lpKey = Trim(Entry(i, lpSection, Chr(0)))
            If lpKey = "" Then Exit Sub
            '-> cr�er une nouvelle instance d'un progiciel
            Set aProgiciel = New clsProgiciel
            aProgiciel.Code = UCase$(Trim(Entry(1, lpKey, "=")))
            aProgiciel.Libelle = Trim(Entry(2, lpKey, "="))
            '-> Ajouter dans la collection
            Progiciels.Add aProgiciel, "PRO_" & UCase$(Trim(aProgiciel.Code))
        Next 'Pour tous les progiciels
    End If

End Sub



Public Sub ExecFormatCell(aFormatCell As clsFormatCell, ByVal Lig As Integer, Col As Integer, aFlex As MSFlexGrid, Optional IsEntete As Boolean)
 
'-> Positionner la cellule du FlexGrid
aFlex.Col = Col
aFlex.Row = Lig

'-> Appliquer la couleur de font
aFlex.CellBackColor = aFormatCell.BackColor

'-> Appliquer la police
aFlex.CellFontName = aFormatCell.FontName

'-> Appliquer la taille de la police
aFlex.CellFontSize = aFormatCell.FontSize
                                                                    
'-> Appliquer les attributs de la police
aFlex.CellForeColor = aFormatCell.FontColor
aFlex.CellFontBold = aFormatCell.FontBold
aFlex.CellFontItalic = aFormatCell.FontItalic
aFlex.CellFontUnderline = aFormatCell.FontUnderline

'-> Doit'on mettre un libell�
If aFormatCell.Format = 1 Then
    aFlex.Text = aFormatCell.Libelle
End If

If IsEntete Then
    aFlex.CellAlignment = aFormatCell.InterneAlign - 1
    Exit Sub
End If

'-> Retraduire l'alignement interne de la cellule
Select Case aFormatCell.InterneAlign
    Case 1
        aFlex.CellAlignment = 0
    Case 2
        aFlex.CellAlignment = 3
    Case 3
        aFlex.CellAlignment = 6
    Case 4
        aFlex.CellAlignment = 1
    Case 5
        aFlex.CellAlignment = 4
    Case 6
        aFlex.CellAlignment = 7
    Case 7
        aFlex.CellAlignment = 2
    Case 8
        aFlex.CellAlignment = 5
    Case 9
        aFlex.CellAlignment = 8
End Select


End Sub

Public Function GetRow(ByRef Flex As MSFlexGrid, ByVal y As Long) As Integer

    Dim i As Long
    
    For i = 0 To Flex.Rows - 1
        If y >= Flex.RowPos(i) And y <= Flex.RowPos(i) + Flex.RowHeight(i) Then
            GetRow = i
            Exit Function
        End If
    Next

End Function

Public Function GetCol(ByRef Flex As MSFlexGrid, ByVal x As Long) As Integer

    Dim i As Long
    
    For i = 0 To Flex.Cols - 1
        If x >= Flex.ColPos(i) And x <= Flex.ColPos(i) + Flex.ColWidth(i) Then
            GetCol = i
            Exit Function
        End If
    Next

End Function

Public Function CheckFormule(Formule As String, aTb As clsTableau) As Boolean

    '---> Cette proc�dure est charg�e de v�rifier si la syntaxe de la formule est correcte
    
    '-> IsLig indique sile calcul traite une ligne ou une colonne
    
    Dim i As Integer
    Dim j As Integer
    Dim StrToCheck As String
    Dim ObjectNom As String
    Dim ObjectType As String
    Dim aCol As clsCol
    Dim aLig As clsRow
    Dim NbOpen As Integer
    Dim NbClose As Integer
    
    '-> Trapper les errors
    On Error GoTo BadFormule
    
    '-> Get du nombre de parenth�ses ouvertes et du nombre de parenth�ses ferm�e
    i = 1
    Do While InStr(i, Formule, "(") <> 0
        i = InStr(i, Formule, "(") + 1
        NbOpen = NbOpen + 1
    Loop
    
    i = 1
    Do While InStr(i, Formule, ")") <> 0
        i = InStr(i, Formule, ")") + 1
        NbClose = NbClose + 1
    Loop
    
    strError = "Le nombre des parenth�ses ouvertes (" & NbOpen & ") est diff�rent du nombre des parenth�ses ferm�es ( " & NbClose & ")"
    If NbOpen <> NbClose Then GoTo BadFormule
    
    '-> V�rification de la coh�rence entre les op�randes
    '-> Analyse des doublons par op�rande
    StrToCheck = "**" & Chr(0) & "*/" & Chr(0) & _
                 "*+" & Chr(0) & "*)" & Chr(0) & _
                 "+*" & Chr(0) & "+/" & Chr(0) & _
                 "+)" & Chr(0) & "++" & Chr(0) & _
                 "-+" & Chr(0) & "-/" & Chr(0) & _
                 "-)" & Chr(0) & "-*" & Chr(0) & _
                 "()" & Chr(0) & "(*" & Chr(0) & _
                 "(+" & Chr(0) & "(/" & Chr(0) & _
                 "/--" & Chr(0) & "+--" & Chr(0) & _
                 "*--" & Chr(0) & ")--" & Chr(0) & _
                 "--("
    
    For i = 1 To NumEntries(StrToCheck, Chr(0))
        j = InStr(1, Formule, Entry(i, StrToCheck, Chr(0)))
        strError = "Incoh�rence dans les op�randes."
        If j <> 0 Then GoTo BadFormule
    Next
    
    '-> V�rification des noms des objets dans la ligne
    j = 1
    Do
        '-> R�cup�rer les diff�rents noms des objets
        i = InStr(j, Formule, "$")
        If i = 0 Then Exit Do
        
        '-> R�cup�rer la fin du nom
        j = InStr(i + 1, Formule, "$")
        If j = 0 Then
            '-> Il y a un $ d'ouverture mais pas de fermeture -> Erreur
            GoTo BadFormule
        End If
        
        '-> R�cup�rer le nom
        ObjectNom = Mid$(Formule, i + 1, j - (i + 1))
        
        '-> R�cup�rer le type d'objet
        ObjectType = Entry(1, ObjectNom, "�")
        
        '-> Selon le type d'objet
        Select Case UCase$(ObjectType) 'MESSPROG
            Case "COL"
                strError = "Colonne non r�f�renc�e : " & Entry(2, ObjectNom, "�")
                Set aCol = aTb.Colonnes("COL_" & UCase$(Entry(2, ObjectNom, "�")))
            Case "LIG"
                strError = "Ligne non r�f�renc�e : " & Entry(2, ObjectNom, "�")
                Set aLig = aTb.Lignes("LIG_" & UCase$(Entry(2, ObjectNom, "�")))
            Case "PRO"
                strError = "Progiciel non r�f�renc� : " & Entry(2, ObjectNom, "�")
                If Not IsProgiciel(Entry(2, ObjectNom, "�")) Then GoTo BadFormule
                strError = "Champ non r�f�renc� " & Entry(3, ObjectNom, "�") & " dans le progiciel & " & Entry(2, ObjectNom, "�")
                If Not aTb.IsFieldInDescriptifProgiciel(UCase$(Entry(3, ObjectNom, "�")), "PRO_" & UCase$(Entry(2, ObjectNom, "�"))) Then GoTo BadFormule
            Case Else
                strError = "Type d'objet inconnu : " & ObjectType
                GoTo BadFormule
        End Select
             
        '-> D�placer le point d'analye vers l'octet suivant
        j = j + 1
    
    Loop
    
OkFormule:
    CheckFormule = True
    Exit Function
    
    
BadFormule:
    CheckFormule = False

End Function


Public Function LoadTb(Fichier As String, Optional aPic As PictureBox) As clsTableau

    '---> Cette fonction retourne un objet tableau � partir d'un fichier ASCII
    
    Dim Res As Long
    Dim aTb As clsTableau
    Dim aProgiciel As clsProgiciel
    Dim StrTempo As String
    Dim i As Integer
    Dim lpKey As String
    Dim lpBuffer As String
    Dim IsPic As Boolean
    
    '-> Tester si on doit dessiner la temporisation
    If Not aPic Is Nothing Then IsPic = True
    
    '-> Entete du tableau
    Set aTb = New clsTableau
    '-> Nom du tableau
    aTb.Name = GetIniFileValue("PARAM_ENTETE", "NAME", Fichier)
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 3, aPic
    '-> Libell�
    aTb.Libel = Trim(GetIniFileValue("PARAM_ENTETE", "LIBEL", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 6, aPic
    '-> LargeurEnt
    aTb.LargeurEnt = CLng(GetIniFileValue("PARAM_ENTETE", "LARGEURENT", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 9, aPic
    '-> HauteurEnt
    aTb.HauteurEnt = CLng(GetIniFileValue("PARAM_ENTETE", "HAUTEURENT", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 12, aPic
    '-> GridLines
    aTb.pGridLines = CLng(GetIniFileValue("PARAM_ENTETE", "GRIDLINES", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 15, aPic
    '-> GridlinesFixed
    aTb.pGridLinesFixed = CLng(GetIniFileValue("PARAM_ENTETE", "GRIDLINESFIXED", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 18, aPic
    '-> Appearance
    aTb.pAppearance = CInt(GetIniFileValue("PARAM_ENTETE", "APPEARANCE", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 21, aPic
    '-> BorderStyle
    aTb.pBorderStyle = CInt(GetIniFileValue("PARAM_ENTETE", "BORDERSTYLE", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 24, aPic
    '-> GridLineWidth
    aTb.pGridLineWidth = CLng(GetIniFileValue("PARAM_ENTETE", "GRIDLINEWIDTH", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 27, aPic
    '-> BackColorBkg
    aTb.pBackColorBkg = CLng(GetIniFileValue("PARAM_ENTETE", "BACKCOLORBKG", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 30, aPic
    '-> BackColorSel
    aTb.pBackColorSel = CLng(GetIniFileValue("PARAM_ENTETE", "BACKCOLORSEL", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 33, aPic
    '-> BackColorFixed
    aTb.pBackColorFixed = CLng(GetIniFileValue("PARAM_ENTETE", "BACKCOLORFIXED", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 36, aPic
    '-> GridColor
    aTb.pGridColor = CLng(GetIniFileValue("PARAM_ENTETE", "GRIDCOLOR", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 39, aPic
    '-> GridColorFixed
    aTb.pGridColorFixed = CLng(GetIniFileValue("PARAM_ENTETE", "GRIDCOLORFIXED", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 42, aPic
    '-> AllowUserResizing
    aTb.pAllowUserResizing = CInt(GetIniFileValue("PARAM_ENTETE", "ALLOWUSERRESIZING", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 45, aPic
    aTb.pHeaderBold = CBool(GetIniFileValue("PARAM_ENTETE", "HEADERBOLD", Fichier))
    '-> pLargeur
    aTb.pLargeur = CLng(GetIniFileValue("PARAM_ENTETE", "PLARGEUR", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 48, aPic
    '-> pHauteur
    aTb.pHauteur = CLng(GetIniFileValue("PARAM_ENTETE", "PHAUTEUR", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 51, aPic
    '-> Si on utilise l'acc�s � un axe hierarchique
    aTb.UseAxe = CBool(GetIniFileValue("PARAM_ENTETE", "USEAXE", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 54, aPic
    '-> Libell� de l'axe
    aTb.LibAxe = Trim(GetIniFileValue("PARAM_ENTETE", "LIBAXE", Fichier))
    '-> Libell� des niveaux de l'axe
    aTb.LibNvAxe = Trim(GetIniFileValue("PARAM_ENTETE", "LIBNVAXE", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 57, aPic
    '-> Niveau de l'axe
    aTb.NvAxeAnalyse = CInt(GetIniFileValue("PARAM_ENTETE", "NVAXEANALYSE", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 60, aPic
    '-> Si on utilise l'acc�s au d�tail
    aTb.UseDetail = CBool(GetIniFileValue("PARAM_ENTETE", "USEDETAIL", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 63, aPic
    '-> Libell� de l'acc�s au d�tail
    aTb.LibDet = Trim(GetIniFileValue("PARAM_ENTETE", "LIBDETAIL", Fichier))
    '-> Libell� des niveaux de l'acc�s au d�tail
    aTb.LibNvDet = Trim(GetIniFileValue("PARAM_ENTETE", "LIBNVDETAIL", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 66, aPic
    '-> Niveau de l'acc�s au d�tail
    aTb.NvAxeDetail = CInt(GetIniFileValue("PARAM_ENTETE", "NVAXEDETAIL", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 69, aPic
    '-> Charger la liste des progiciels
    StrTempo = Trim(GetIniFileValue("PARAM_ENTETE", "PROGICIELS", Fichier))
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 72, aPic
    '-> V�rifier si chaque progiciel  est r�f�renc�
    If StrTempo <> "" Then
        For i = 1 To NumEntries(StrTempo, "|")
            '-> V�rifier si le progiciel est r�f�renc�
            If Not IsProgiciel(Entry(i, StrTempo, "|")) Then     'MESSPROG
                MsgBox "Progiciel non r�f�renc�", vbCritical + vbOKOnly, "Erreur"
                Set aTb = Nothing
                Exit Function
            End If
            '-> Cr�er une nouvelle instance d'une classe progiciel
            Set aProgiciel = New clsProgiciel
            '-> Init du param�trage
            aProgiciel.Code = UCase$(Trim(Entry(i, StrTempo, "|")))
            '-> Affect� ce progiciel au tableau
            aTb.Progiciels.Add aProgiciel, "PRO_" & aProgiciel.Code
            '-> R�cup�rer le descriptif de ce progiciel
            InitProgicielDes aProgiciel, Fichier
        Next 'Pour tous les progiciels
    End If 'Sil y a des progiciels affect�s
    
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 80, aPic
    
    '-> FormatEnteteLig
    StrTempo = GetIniFileValue("PARAM_ENTETE", "FORMATENTETELIG", Fichier)
    Set aTb.FormatEnteteLig = SetFormatCell(StrTempo)
    
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 85, aPic
    
    '-> FormatEnteteCol
    StrTempo = GetIniFileValue("PARAM_ENTETE", "FORMATENTETECOL", Fichier)
    Set aTb.FormatEnteteCol = SetFormatCell(StrTempo)
    
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 90, aPic
    
    '-> MatriceCol
    aTb.MatriceCol = GetIniFileValue("PARAM_ENTETE", "MATRICECOL", Fichier)
    
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 95, aPic
    
    '-> MatriceLig
    aTb.MatriceLig = GetIniFileValue("PARAM_ENTETE", "MATRICELIG", Fichier)
    
    '-> Get pour toutes les colonnes de la cl� qui va bien
    If aTb.MatriceCol <> "" Then
        For i = 1 To NumEntries(aTb.MatriceCol, "|")
            '-> Get du code d'une colonne
            lpBuffer = Entry(i, aTb.MatriceCol, "|")
            '-> Get de sa sauvegarde dans le fichier ini
            strRetour = GetIniFileValue("PARAM_ENTETE", "COL_" & UCase$(lpBuffer), Fichier)
            '-> Cr�ation de la colonne
            InitCol strRetour, UCase$(lpBuffer), aTb
        Next 'pour toutes les colonnes
    End If 'S'il y a des colonnes
    
    '-> Cr�ation de toutes les lignes
    If aTb.MatriceLig <> "" Then
        For i = 1 To NumEntries(aTb.MatriceLig, "|")
            '-> Get du code d'une ligne
            lpBuffer = Entry(i, aTb.MatriceLig, "|")
            '-> Get de sa sauvegarde dans le fichier ini
            strRetour = GetIniFileValue("ROW_" & UCase$(lpBuffer), "ROW_" & UCase$(lpBuffer), Fichier, True)
            '-> Cr�ation de la ligne
            InitLig strRetour, UCase$(lpBuffer), aTb
        Next 'Pour toutes les lignes
    End If 'S'il y a des lignes
    
    '-> Mettre � jour le fichier
    aTb.FileName = Fichier
    
    '-> Dessin de la temporisation
    If IsPic Then DrawWait 100, 100, aPic
    
    '-> Renvoyer un pointeur
    Set LoadTb = aTb
    
End Function

Private Function InitLig(Param As String, RowCode As String, aTb As clsTableau)

    '---> Cette fonction analyse te cr�er une ligne en fonction du param�trage contenu _
    dans une section d'un fichier ini
    
    Dim lpParam As String
    Dim lpProp As String
    Dim lpValue As String
    Dim V1 As String
    Dim V2 As String
    Dim aRow As New clsRow
    Dim aCell As clsCell
    Dim aCdt As clsCondition
    Dim aValCdt As clsValueCdt
    Dim i As Integer, j As Integer
    
    '-> Initialisatin de l'objet de type ligne
    If Trim(Param) = "" Then
        aRow.Code = UCase$(RowCode)
        aRow.Libelle = "Error Loading"
    Else
        '-> analyse de toutes les cl�s de la section
        For i = 1 To NumEntries(Param, Chr(0))
            '-> Get de l'entr�e
            lpParam = Entry(i, Param, Chr(0))
            If Trim(lpParam) = "" Then GoTo NextProp
            '-> Eclater en cle/Valeur
            lpProp = Entry(1, lpParam, "=")
            lpValue = Entry(2, lpParam, "=")
            '-> Selon la propri�t� celulle ou ligne, condition
            If UCase$(Entry(1, lpProp, "_")) = "ROW" Then 'Ligne
                '-> Selon la propri�t�
                Select Case UCase$(lpProp)
                    Case "ROW_CODE"
                        aRow.Code = UCase$(lpValue)
                    Case "ROW_LIBELLE"
                        aRow.Libelle = lpValue
                    Case "ROW_TYPELIG"
                        aRow.TypeLig = CInt(lpValue)
                    Case "ROW_HAUTEUR"
                        aRow.Hauteur = CLng(lpValue)
                    Case "ROW_HAUTEURTWIPS"
                        aRow.HauteurTwips = CLng(lpValue)
                    Case "ROW_CALCUL"
                        aRow.Calcul = lpValue
                    Case "ROW_VISIBLE"
                        aRow.Visible = CBool(lpValue)
                    Case "ROW_POSITION"
                        aRow.Position = CInt(lpValue)
                End Select 'Selon la propri�t�
            ElseIf UCase$(Entry(1, lpProp, "_")) = "CDT" Then 'Condition
                '-> Cr�ation d'une nouvelle condition
                Set aCdt = New clsCondition
                aCdt.Colonne = UCase$(GetColName(lpProp, True))
                aCdt.TypeCdt = CInt(Entry(2, lpProp, "�"))
                aCdt.Progiciel = Trim(UCase$(Entry(3, lpProp, "�")))
                If Trim(lpValue) <> "" Then
                    '-> Get d'une valeur de condition
                    For j = 1 To NumEntries(lpValue, "|")
                        '-> Get d'une d�finition d'une valeur
                        lpParam = Entry(j, lpValue, "|")
                        '-> Tester que <> ""
                        If Trim(lpParam) = "" Then Exit For
                        '-> Cr�er une nouvelle condition
                        Set aValCdt = New clsValueCdt
                        aValCdt.Operande = CInt(Entry(1, lpParam, "�"))
                        aValCdt.Operation = CInt(Entry(2, lpParam, "�"))
                        aValCdt.ValMini = Entry(3, lpParam, "�")
                        aValCdt.ValMaxi = Entry(4, lpParam, "�")
                        '-> Ajouter la condition
                        aCdt.ValueCdts.Add aValCdt, "VALCDT_" & aCdt.ValueCdts.Count + 1
                    Next 'Pour toutes les valeurs de cette condition
                End If 'S'il y a des valeurs pour cette condition
                '-> ajouter la condition dans la ligne
                aRow.Conditions.Add aCdt, "CDT_" & UCase$(Trim(aCdt.Colonne)) & "_" & UCase$(Trim(aCdt.Progiciel))
            Else 'Cellule
                '-> cr�ation d'un nouvel objet cellule
                Set aCell = New clsCell
                aCell.ColonneCode = UCase$(GetColName(lpProp))
                Set aCell.FormatCell = SetFormatCell(lpValue)
                '-> Ajout dans la collection des cellules
                aRow.Cellules.Add aCell, "CELL_" & aCell.ColonneCode
            End If 'Si on attaque la propri�t� d'une cellule ou d'une colonne ou d'une condition
NextProp:
        Next 'Pour toutes les entr�es de la sectionb
    End If 'Si ligne de param�trage OK
    
    '-> Ajouter la ligne dans la collection
    aTb.Lignes.Add aRow, "LIG_" & aRow.Code

End Function


Private Function GetColName(Param As String, Optional IsCdt As Boolean) As String

    '---> R�cup�re le nom d'une colonne dans la cl� d'une cellule
    
    Dim i As Integer
    Dim j As Integer
    
    i = InStr(1, Param, "_")
    If IsCdt Then
        j = InStr(i, Param, "�")
        GetColName = Mid$(Param, i + 1, j - (i + 1))
    Else
        GetColName = Mid$(Param, i + 1, Len(Param) - i)
    End If


End Function
Private Function GetColCalcul(lpParam As String) As String

    '---> Cette proc�dure retourne les �l�ments de calcul d'une colonne
    
    Dim i As Integer
    
    i = InStr(1, lpParam, "�")
    If i = 0 Then
        GetColCalcul = lpParam
        Exit Function
    End If
    
    GetColCalcul = Mid$(lpParam, i + 1, Len(lpParam) - i + 1)


End Function
Private Function InitCol(Param As String, ColCode As String, aTb As clsTableau)

    '---> Cette proc�dure cr�er une colonne � partie de sa ligne de param�trage
    
    Dim aCol As New clsCol
    Dim i As Integer
    Dim lpProp As String
    Dim lpParam As String
    Dim lpValue As String
    
    '-> Tester que la chaine de param�trage ne soit pas vide
    If Trim(Param) = "" Then
        '-> Initialiser avec les valeurs par d�faut
        aCol.Code = ColCode
        aCol.Libelle = "Loading Error"
    Else
        '-> Lecture des propri�t�s
        For i = 1 To NumEntries(Param, "|")
            '-> Lecture d'une propri�t�
            lpParam = Entry(i, Param, "|")
            '-> Eclater en code, propri�t�
            lpProp = Entry(1, lpParam, "�")
            lpValue = Entry(2, lpParam, "�")
            '-> Selon la propri�t�
            Select Case UCase$(lpProp)
                Case "CODE"
                    aCol.Code = UCase$(Trim(lpValue))
                Case "LIBELLE"
                    aCol.Libelle = Trim(lpValue)
                Case "VISIBLE"
                    aCol.Visible = CBool(lpValue)
                Case "TYPECOL"
                    aCol.TypeCol = CInt(lpValue)
                Case "LARGEUR"
                    aCol.Largeur = CLng(lpValue)
                Case "LARGEURTWIPS"
                    aCol.LargeurTwips = CLng(lpValue)
                Case "CALCUL"
                    aCol.Calcul = GetColCalcul(lpParam)
                Case "FIELD"
                    aCol.Field = lpValue
                Case "POSITION"
                    aCol.Position = CInt(lpValue)
                Case "PROGICIEL"
                    aCol.Progiciel = lpValue
            End Select 'Selon la propri�t� utilis�e
        Next 'Pour toutes les propri�t�s
    End If
    
    '-> Ajouter dans la collection
    aTb.Colonnes.Add aCol, "COL_" & UCase$(aCol.Code)

End Function


Private Function SetFormatCell(Param As String) As clsFormatCell

    Dim i As Integer
    Dim lpParam As String
    Dim lpProp As String
    Dim lpValue As String
    Dim aFormatCell As clsFormatCell
    
    '-> Cr�ation d'une nouvelle instance
    Set aFormatCell = New clsFormatCell
    
    '-> Quitter si chaine de param�trage � vide
    If Trim(Param) = "" Then Exit Function
    
    '-> Initialiser avec la chaine de param�trage
    For i = 1 To NumEntries(Param, "|")
        '-> Get d'une propri�t�
        lpParam = Entry(i, Param, "|")
        '-> Propri�t� � blanc
        If Trim(lpParam) = "" Then GoTo NextProp
        '-> Eclater en propri�t� valeur
        lpProp = Entry(1, lpParam, "�")
        lpValue = Entry(2, lpParam, "�")
        '-> Selon la propri�t�
        Select Case UCase$(lpProp)
            Case "FONTNAME"
                aFormatCell.FontName = lpValue
            Case "FONTSIZE"
                aFormatCell.FontSize = CInt(lpValue)
            Case "FONTBOLD"
                aFormatCell.FontBold = CBool(lpValue)
            Case "FONTITALIC"
                aFormatCell.FontItalic = CBool(lpValue)
            Case "FONTUNDERLINE"
                aFormatCell.FontUnderline = CBool(lpValue)
            Case "FONTCOLOR"
                aFormatCell.FontColor = CLng(lpValue)
            Case "BACKCOLOR"
                aFormatCell.BackColor = CLng(lpValue)
            Case "FORMAT"
                aFormatCell.Format = CInt(lpValue)
            Case "LIBELLE"
                aFormatCell.Libelle = lpValue
            Case "DEC"
                aFormatCell.NbDec = CInt(lpValue)
            Case "SEPMIL"
                aFormatCell.UseSepMil = CBool(lpValue)
            Case "ABS"
                aFormatCell.Absolute = CBool(lpValue)
            Case "%"
                aFormatCell.Pourcentage = CBool(lpValue)
            Case "USECOLORNEG"
                aFormatCell.UseColorNeg = CBool(lpValue)
            Case "COLORNEG"
                aFormatCell.ColorNeg = CLng(lpValue)
            Case "INTERNEALIGN"
                aFormatCell.InterneAlign = CInt(lpValue)
        End Select 'Selon la propri�t�
NextProp:
    Next
    
    '-> Renvoyer la valeur
    Set SetFormatCell = aFormatCell

End Function

Public Sub InitLoadingTb(aTb As clsTableau, aFlex As MSFlexGrid, Optional aRtf As RichTextBox, Optional DrawValue As Boolean, Optional DrawTempo As Boolean)

    '---> Cette proc�dure est charg�e de cr�er la repr�sentation physique du tableau
    
    Dim i As Integer
    Dim aRow As clsRow
    Dim aCell As clsCell
    Dim aCol As clsCol
    
    '-> NbLigne et NbCol
    aFlex.Rows = aTb.Lignes.Count + 1
    aFlex.Cols = aTb.Colonnes.Count + 1
    
    '-> Hauteur de ligne
    For Each aRow In aTb.Lignes
        '-> Dessin de la temporisation
        If DrawTempo Then
            TailleLue = TailleLue + 1
            DrawWait TailleTotale, TailleLue, frmTempo.Picture1
        End If
        '-> Hauteur de la ligne
        aFlex.RowHeight(aRow.Position) = aRow.HauteurTwips
        '-> Formattage de chaque cellule
        For Each aCell In aRow.Cellules
            '-> R�cup�rer la position li�e par l'objet colonne
            Set aCol = aTb.Colonnes("COL_" & UCase$(aCell.ColonneCode))
            '-> Appliquer le formattage
            ExecFormatCell aCell.FormatCell, aRow.Position, aCol.Position, aFlex
            '-> Positionner la valeur
            If DrawValue Then
                '-> Positionner le valeur si <> 0
                If Trim(aCell.ValueCell) <> "" Then
                    If CDbl(aCell.ValueCell) <> 0 Then
                        aFlex.TextArray(aRow.Position * aFlex.Cols + aCol.Position) = aCell.ValueCell
                        If aCell.FormatCell.UseColorNeg And CDbl(aCell.ValueCell) < 0 Then
                            '-> Positionner la valeur
                            aFlex.Col = aCol.Position
                            aFlex.Row = aRow.Position
                            aFlex.CellForeColor = aCell.FormatCell.ColorNeg
                        End If 'Si on utilise une couleur <> si n�gatif THIERRY
                    End If 'Si <> 0
                End If 'Si <> ""
            End If
        Next 'Pour toutes les cellules de la ligne
        '-> Zone Rtf
        If aRow.TypeLig = 1 Then
            '-> Convertir
            If Not aRtf Is Nothing Then
                aRow.CalculRtf = SetRtfCalculColor(aRow.Calcul, aRtf)
            End If '-> Si on a renseign� le RTF
        End If 'S'il on est dans une ligne de calcul
    Next 'Pour toutes les lignes
    
    '-> Largeur de colonnes
    For Each aCol In aTb.Colonnes
        '-> Dessin de la temporisation
        If DrawTempo Then
            TailleLue = TailleLue + 1
            DrawWait TailleTotale, TailleLue, frmTempo.Picture1
        End If
        '-> Largeur de colonne
        aFlex.ColWidth(aCol.Position) = aCol.LargeurTwips
        '-> Zone RTF
        If aCol.TypeCol = 1 Then
            If Not aRtf Is Nothing Then
                aCol.CalculRtf = SetRtfCalculColor(aCol.Calcul, aRtf)
            End If '-> Si on a renseign� le RTF
        End If 'S'il on est dans une colonne de calcul
    Next
    '-> Dessiner les entetes
    aTb.DrawEntete aTb.DisplayCodeCol, aFlex

End Sub

Public Function SetRtfCalculColor(Calcul As String, aRtf As RichTextBox) As String

    '---> Cette fonction permet de coloriser
    
    Dim i As Integer, j As Integer
    Dim strName As String
    
    If Calcul = "" Then GoTo EndFunction
        
    '-> Charger le texte dans le RTF
    aRtf.Text = Calcul
    
    '-> Analyse de la chaine de caract�re
    For i = 1 To Len(Calcul)
        '-> Tester si on trouve un $
        If Mid$(Calcul, i, 1) = "$" Then
            '-> On a trouv� la chhaine d'ouverture d'un nom recherch� la chaine de fermeture
            j = InStr(i + 1, Calcul, "$")
            '-> R�cup�rer le nom
            aRtf.SelStart = i - 1
            aRtf.SelLength = (j - i) + 1
            aRtf.SelColor = RGB(255, 0, 0)
            i = j
        Else
            aRtf.SelStart = i - 1
            aRtf.SelLength = 1
            Select Case Asc(Mid$(Calcul, i, 1))
                Case 40, 41 'Parenth�ses
                    aRtf.SelColor = RGB(0, 255, 0)
                Case 42, 43, 45, 47 'Signes math
                    aRtf.SelColor = RGB(0, 0, 255)
                Case 48 To 57 'Chiffres
                    aRtf.SelColor = 0
            End Select 'Selon la valeur ASCII de chaque param�tre
        End If
    Next 'Pour tous les caract�res
    
EndFunction:
    
    '-> Renvoyer la valeur
    SetRtfCalculColor = aRtf.TextRTF
    aRtf.Text = ""

End Function

Public Function IsProgiciel(Code As String) As Boolean

    '---> Cette fonction v�rifier qu'un progiciel affect� � un tableau soit bien r�f�renc� dans la liste des progicels accessibles
    
    Dim aProgiciel As clsProgiciel
    
    On Error GoTo EndError
    
    '-> Essayer de pointer sur le progiciel sp�cifi�
    Set aProgiciel = Progiciels("PRO_" & UCase$(Trim(Code)))
    
    '-> Renvoyer une valeur de succ�s
    IsProgiciel = True
    
    Exit Function
    
EndError:
        '-> Renvoter une valeur d'erreur
        IsProgiciel = False


End Function

Private Function InitProgicielDes(aProgiciel As clsProgiciel, Fichier As String) As Boolean

    '---> Cette fonction analyse une section progiciel pour get de son descro et param�trage
    Dim lpSection As String
    Dim lpRef As String
    Dim lpKey As String
    Dim lpValue As String
    Dim i As Integer
    Dim Res As Long
    Dim aDes As clsDescriptif
    
    On Error GoTo EndError
    
    '-> R�cup�rer le contenu de la section
    lpSection = Space$(30000)
    Res = GetPrivateProfileSection("PROGICIEL_" & aProgiciel.Code, lpSection, Len(lpSection), Fichier)
    
    '-> Quitter si la section n'existe pas
    If Res = 0 Then
        '-> Renvoyer une valeur d'erreur
        InitProgicielDes = False
        '-> Quitter
        Exit Function
    End If
    
    '-> Analyse du contenu de la section
    For i = 1 To NumEntries(lpSection, Chr(0))
        '-> Get d'une r�f�rence
        lpRef = Trim(Entry(i, lpSection, Chr(0)))
        '-> Quitter si vide
        If Trim(lpRef) = "" Then Exit For
        '-> Eclater en cl� , valeur
        lpKey = Entry(1, lpRef, "=")
        lpValue = Entry(2, lpRef, "=")
        '-> Selon la nature de la cl�
        Select Case UCase$(Entry(1, lpKey, "|"))
            Case "FIELD" '-> On attaque un descriptif de champ
                '-> Initialiser un nouvel objet Desscriptif
                Set aDes = New clsDescriptif
                aDes.Commentaire = Entry(1, lpValue, "|")
                aDes.Field = UCase$(Trim(Entry(2, lpValue, "|")))
                aDes.Entree = CInt(Entry(3, lpValue, "|"))
                '-> Ajouter le descriptif
                aProgiciel.Descriptifs.Add aDes, "FIELD_" & aDes.Field
                
            Case "ISAXEANALYSE" 'Indique la pr�sence de l'axe hierarchique dans le progiciel
                aProgiciel.IsAxeAnalyse = CBool(CInt(lpValue))
            Case "AXENIVEAU"
                '-> Ajouter dans le descriptif de l'axe hierarchique
                Set aDes = New clsDescriptif
                aDes.Niveau = CInt(Entry(2, lpKey, "|"))
                aDes.Field = lpValue
                aProgiciel.AxeAnalyses.Add aDes, "NV_" & aDes.Niveau
                '-> Pointer sur le chalmp pour get de son entr�e
                If aProgiciel.MatriceAxe = "" Then
                    aProgiciel.MatriceAxe = aProgiciel.Descriptifs("FIELD_" & aDes.Field).Entree
                Else
                    aProgiciel.MatriceAxe = aProgiciel.MatriceAxe & "|" & aProgiciel.Descriptifs("FIELD_" & aDes.Field).Entree
                End If
            Case "ISAXEDET" 'Indique que l'on utilise l'acc�s au d�tail dans ce progiciel
                aProgiciel.IsAxeDet = CBool(CInt(lpValue))
            Case "DETNIVEAU"
                '-> Ajouter dans le descriptif de l'acc�s d�tail
                Set aDes = New clsDescriptif
                aDes.Niveau = CInt(Entry(2, lpKey, "|"))
                aDes.Field = lpValue
                aProgiciel.AxeDetails.Add aDes, "NV_" & aDes.Niveau
                '-> Pointer sur le chalmp pour get de son entr�e
                If aProgiciel.MatriceDet = "" Then
                    aProgiciel.MatriceDet = aProgiciel.Descriptifs("FIELD_" & aDes.Field).Entree
                Else
                    aProgiciel.MatriceDet = aProgiciel.MatriceDet & "|" & aProgiciel.Descriptifs("FIELD_" & aDes.Field).Entree
                End If
        End Select
    Next 'Pour toutes les cl� de la section
    
    '-> Tout c'est bien pass� renvoy� une valeur de succ�s
    InitProgicielDes = True
    
    Exit Function
    
EndError:
        '-> Renvoyer une valeur d'erreur
        InitProgicielDes = False

End Function

Public Sub DrawWait(TailleTotale As Long, TailleLue As Long, aPic As PictureBox)
    '---> Permet de faire avancer une barre d'avancement lors de l'analyse du fichier ASCII
    DoEvents
    aPic.Line (0, 0)-((TailleLue / TailleTotale) * aPic.ScaleWidth, aPic.ScaleHeight), &HC00000, BF
End Sub

Public Function GetPath(ByVal InitPath As String) As String

    '---> Cette fonction renvois le path de \MQT en fonction de l'endroit d'ou _
    est appell�e le programme ( utilis� pour faire la diff�rence en DVL et EXE)
    
    '-> La diff�rence se fait par : Si on trouve le fichier VBP : on est en DVL
    Dim Pos As Integer

    If Dir$(InitPath & "\*.vbp") = "" Then
        '-> On est en mode exe
        Pos = InStrRev(InitPath, "\")
        BasePath = Mid$(InitPath, 1, Pos)
        RessourcePath = BasePath & "Mqt\"
    Else
        '-> On est en mode interpr�t�
        RessourcePath = InitStringPath(0) & "\"
    End If
    
    GetPath = RessourcePath

End Function

Public Function InitStringPath(IsTurbo As Integer) As String

    '---> Cette proc�dure retourne un le path de MQT  si isturbo = 0 _
    ou le path du r�pertoire turbo = 1
    
    Dim TempPath As String
    Dim i As Integer, j As Integer
    Dim LastName As String
    
    
    '-> R�cup�rer le path en cours
    TempPath = App.Path
    
    If IsTurbo = 0 Then
        '-> Recherche du path MQT
        If InStr(1, UCase$(App.Path), "EMILIEGUI\SRC") <> 0 Then
            i = InStr(1, UCase$(App.Path), "EMILIEGUI\SRC")
            TempPath = Mid$(TempPath, 1, i - 1) & "EMILIEGUI\MQT"
            InitStringPath = TempPath
        Else
            TempPath = Replace(UCase$(App.Path), "EMILIEGUI\EXE", "EMILIEGUI\MQT")
            InitStringPath = TempPath & "\"
        End If
        RessourcePath = TempPath
    Else
        '-> Recherche du path TURBO
        i = InStr(1, UCase$(TempPath), "DEALPRO\")
        TempPath = Mid$(TempPath, 1, i - 1) & "TURBO"
        InitStringPath = TempPath
        
        Exit Function
    
    End If


End Function




