Attribute VB_Name = "fctPrintGrid"
'*****************************************************************
'***** MODULE DE GESTION DES IMPRESSIONS DE FLEXGRID SOUS TURBO
'*****************************************************************

'------------------------------------------------------------------------------------
'-- ATTENTION !!!!!!!!!!!!!!!!!!!!!!
' l'utilisation de ce module demande que le prog appelant contienne une picturebox
' pour toutes les conversions d'echelle de grandeur!!!!
'-------------------------------------------------------------------------------------



Option Explicit

Public LigneEntete(500) As String
Public IndEnt As Integer
Public LignePied(500) As String
Public IndPied As Integer

Private Sub PrintEntete(ByVal MaqTurbo As Integer, ByVal MiseEnPage As String, Entete As String)
'-> Fonction qui edite une entete d'edition au format TURBO
Dim i As Integer
Dim Largeur As Integer

'-> Saisie des param�trages entete

i = 0
IndEnt = 0
    
'-> SECTION 2
LigneEntete(IndEnt) = "[NV-SectionE-STD]"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Hauteur�3"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Largeur�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\RgChar�"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Contour�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\AlignementTop�1"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Top�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\AlignementLeft�4"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Left�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\BackColor�16777215"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Champs�"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Texte�End"
IndEnt = IndEnt + 1

'-> Carde 2
LigneEntete(IndEnt) = "\Cadre�Nouveau Cadre2"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Left�" & Trim(Str((Largeur - 15.34585) / 2))
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Top�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Distance�0.5"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Hauteur�2.513544"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Largeur�15.34585"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\LargeurBordure�2"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\RoundRect�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Couleur�8421504"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Contour�1,1,1,1"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Locked�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Masteralign�"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\MasterIndexAlign�PAGE-8-AlignPage8"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\SlaveAlign�"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\AlignX�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\AlignY�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Champs�"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Cadre�End"
IndEnt = IndEnt + 1
'-> Fin Cadre 2

'-> Titre saisit par l'utilisateur
LigneEntete(IndEnt) = "\CadreRtf�Begin"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "{\rtf1\ansi\ansicpg1252\deff0\deflang1036{\fonttbl{\f0\fnil\fcharset0 Times New Roman;}}"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\viewkind4\uc1\pard\qc\ul\b\f0\fs32 " & Entete & " \ulnone\b0\fs20"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\par }"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\CadreRtf�End"
IndEnt = IndEnt + 1

End Sub

Public Sub PrintFlexGrid(ByRef Flex As MSFlexGrid, PrinterPath As String, Spool As String, ByRef Echelle As PictureBox, ByVal TxtEntete As String, TxtPied As String, ReEdit As Boolean)
'-> Fonction qui imprime un flexgrid centr� sur une feuille A4
'-> Placer un picturebox invisible dans le programme appellant pour les conversion ( scalex )
'-> Spool = nom de fichier turbo
'-> printerpath = chemin de turbograph.exe pour visionner le spool

Dim MaqTurbo As Integer
Dim LigneFic As String
Dim LigneFic2(500) As String
Dim IndSaut As Integer
Dim i As Integer
Dim j As Integer
Dim Fichier As String
Dim Portrait As Boolean
Dim HauteurGrid As Long
Dim LargeurGrid As Long
Dim MargeTopEntete As Integer
Dim MiseEnPage As String
Dim Hauteur As Variant
Dim HauteurRef As Long
Dim LargeurRef As Long
Dim k As Integer
Dim TopPage As Boolean
Dim ListePages As String
Dim NoPage As Integer
Dim HauteurPied As Long

'-> Pointeur de souris en mode 'occup�'
Flex.Parent.MousePointer = flexHourglass
Flex.Parent.Enabled = False
LockWindowUpdate Flex.hwnd


NoPage = 1
IndSaut = 0

'-> On defint un spool aleatoire si l'utilisateur ne l'a pas precis�
If Spool = "" Then
    Spool = Replace(GetTempFileNameVB("EDI"), ".tmp", ".turbo")
End If

'-> On ouvre un fichier pour creer la maquette
MaqTurbo = FreeFile
Fichier = Spool
Open Fichier For Append As #MaqTurbo

'-> Calcul de la hauteur du grid
For i = 0 To Flex.Rows - 1
    HauteurGrid = HauteurGrid + Flex.RowHeight(i)
Next
'-> Calcul de la largeur grid
For i = 0 To Flex.Cols - 1
    LargeurGrid = LargeurGrid + Flex.ColWidth(i)
Next

'-> Definition du format de mise en page (portrait ou paysage )
Portrait = False
If Echelle.ScaleX(LargeurGrid, Echelle.ScaleMode, 7) <= 19 Then Portrait = True

'-> On envoie les renseignements de base pour la maquette (entete fichier)
LigneFic = "[SPOOL]"
Print #MaqTurbo, LigneFic
LigneFic = "[MAQ]"
Print #MaqTurbo, LigneFic
LigneFic = "\DefEntete�Begin"
Print #MaqTurbo, LigneFic
LigneFic = "\Version�2"
Print #MaqTurbo, LigneFic
LigneFic = "\FieldListe�"
Print #MaqTurbo, LigneFic
LigneFic = "\Nom�" & Spool
Print #MaqTurbo, LigneFic
LigneFic = "\Date�" & Now
Print #MaqTurbo, LigneFic
Print #MaqTurbo, "\Description�Pas de Description"
Print #MaqTurbo, "\Largeur�21"
Print #MaqTurbo, "\Hauteur�29.7"

'-> On decide si on place le grid en portrait ou paysage selon sa largeur
If Portrait = True Then
    Print #MaqTurbo, "\Orientation�1" '-> Portrait
    MiseEnPage = 1
    HauteurRef = 27.7
    LargeurRef = 19
Else
    Print #MaqTurbo, "\Orientation�0" '-> Paysage
    MiseEnPage = 0
    HauteurRef = 19
    LargeurRef = 27.7
End If

HauteurPied = HauteurRef

'-> Imprime une entete si une titre a ete passe en parametre
If TxtEntete <> "" Then
    '-> On lance la creation de l'entete dans le fichier ascii ( spool )
    PrintEntete MaqTurbo, MiseEnPage, TxtEntete
    '-> On decremente la hauteur de la zone d'impression de la feuille si pied de page
    HauteurRef = HauteurRef - 3
End If

Print #MaqTurbo, "\MargeTop�1"
If Portrait = True Then
    Print #MaqTurbo, "\MargeLeft�" & Str((21 - Echelle.ScaleX(LargeurGrid, Echelle.ScaleMode, 7)) / 2)  '-> On centre horizontalement
Else
    Print #MaqTurbo, "\MargeLeft�" & Str((29.7 - Echelle.ScaleX(LargeurGrid, Echelle.ScaleMode, 7)) / 2) '-> On centre horizontalement
End If

Print #MaqTurbo, "\Suite�"
Print #MaqTurbo, "\Report�"
Print #MaqTurbo, "\Entete�"
Print #MaqTurbo, "\Pied�"
Print #MaqTurbo, "\PageGarde�FAUX"
Print #MaqTurbo, "\PageSelection�FAUX"
Print #MaqTurbo, "\Publipostage�"
Print #MaqTurbo, "\DefEntete�End"
Print #MaqTurbo, "[PROGICIEL]"
Print #MaqTurbo, "\Prog=DEAL"
Print #MaqTurbo, "\Rub=AUTRES"
Print #MaqTurbo, "\Cli=DEAL"
Print #MaqTurbo, "[HTML]"
Print #MaqTurbo, "\Fichier�"
Print #MaqTurbo, "\Rupture�"
Print #MaqTurbo, "\HTML�End"    '-> Fin de la description entete maquette

'-> On envoie l'entete dans le fichier ascii
If TxtEntete <> "" Then
    For i = 0 To IndEnt
        Print #MaqTurbo, LigneEntete(i)
    Next
End If

Print #MaqTurbo, "[TB-Tableau]"
'-> On definit le tableau dans la maquette graphique
Print #MaqTurbo, "\LargeurTb�0"

'-> On decide de l'orientation du tableau en fonction de sa largeur
If Portrait = True Then
    Print #MaqTurbo, "\OrientationTB�1"
Else
    Print #MaqTurbo, "\OrientationTB�0"
End If

ListePages = ""
If TxtPied <> "" Then
    Hauteur = 2
Else
    Hauteur = 1
End If
For i = 0 To Flex.Rows - 1
    Flex.Row = i
    '-> On declare un nouveau block
    Print #MaqTurbo, "\Begin�Block" & Trim(Str(i))
    
    Print #MaqTurbo, "\Largeur�" & Trim(Str(Echelle.ScaleX(LargeurGrid, Echelle.ScaleMode, 7)))
    Print #MaqTurbo, "\Hauteur�" & Trim(Str(Echelle.ScaleX(Flex.RowHeight(i), Echelle.ScaleMode, 7)))
    
    '-> Teste si on est a la fin de la page avec les marges
    Hauteur = Hauteur + CVar(Echelle.ScaleX(Flex.RowHeight(i), Echelle.ScaleMode, 7))
    TopPage = False
    If Hauteur >= HauteurRef Then
        Hauteur = MargeTopEntete
        TopPage = True
        ListePages = ListePages & Trim(Str(Flex.Row + 1)) & ","
    End If
    
    Print #MaqTurbo, "\AlignTop�1"
    Print #MaqTurbo, "\Top�0"
    Print #MaqTurbo, "\AlignLeft�3"
    Print #MaqTurbo, "\Left�0"
    Print #MaqTurbo, "\MasterLink�"
    Print #MaqTurbo, "\SlaveLink�"
    Print #MaqTurbo, "\RgChar�"
    Print #MaqTurbo, "\Ligne�1"
    Print #MaqTurbo, "\Colonne�" & Flex.Cols
    Print #MaqTurbo, "\Varlig�"
    
    '-> Creation de la ligne de maquette en fonction des colonnes flexgrid
    LigneFic = "\Col�" & Trim(Str(Echelle.ScaleX(Flex.RowHeight(i), Echelle.ScaleMode, 7))) & "�"
    For j = 0 To Flex.Cols - 1
        Flex.Col = j
        '-> Alignement de la cellule
        If Flex.Col = 0 Or Flex.Row = 0 Then
            LigneFic = LigneFic & "1;"
        Else
            LigneFic = LigneFic & "8;"
        End If
        
        'LigneFic = LigneFic & Flex.CellAlignment & ";"
        
        '-> texte de la cellule avec gestion des sauts de ligne
        If InStr(1, Flex.Text, Chr(13)) = 0 Then
            LigneFic = LigneFic & Flex.Text & ";"
        Else
            LigneFic = LigneFic & Replace(Flex.Text, Chr(13) & Chr(10), " ", 1, Len(Flex.Text)) & ";"
        End If
        '-> Largeur de la cellule
        LigneFic = LigneFic & Trim(Str(Echelle.ScaleX(Flex.ColWidth(j), Echelle.ScaleMode, 7))) & ";"
        
        '-> Bordure de la cellule ( x 4 )
        If i >= 0 And i < Flex.Rows - 1 Then
            '-> Bordures intermediaires : toutes les bordures sauf le bas
            If TopPage = False Then
                LigneFic = LigneFic & "VRAI,FAUX,VRAI,VRAI" & ";"
            Else '-> On cree un ligne de bas de page
                TopPage = False
                '-> On declare un nouveau block
                LigneFic2(IndSaut) = "\Begin�BlockPAGE1"
                IndSaut = IndSaut + 1
                NoPage = NoPage + 1
                
                LigneFic2(IndSaut) = "\Largeur�" & Trim(Str(Echelle.ScaleX(LargeurGrid, Echelle.ScaleMode, 7)))
                IndSaut = IndSaut + 1
                LigneFic2(IndSaut) = "\Hauteur�" & Trim(Str(Echelle.ScaleX(Flex.RowHeight(i - 1), Echelle.ScaleMode, 7)))
                IndSaut = IndSaut + 1
                
                LigneFic2(IndSaut) = "\AlignTop�1"
                IndSaut = IndSaut + 1
                LigneFic2(IndSaut) = "\Top�0"
                IndSaut = IndSaut + 1
                LigneFic2(IndSaut) = "\AlignLeft�3"
                IndSaut = IndSaut + 1
                LigneFic2(IndSaut) = "\Left�0"
                IndSaut = IndSaut + 1
                LigneFic2(IndSaut) = "\MasterLink�"
                IndSaut = IndSaut + 1
                LigneFic2(IndSaut) = "\SlaveLink�"
                IndSaut = IndSaut + 1
                LigneFic2(IndSaut) = "\RgChar�"
                IndSaut = IndSaut + 1
                LigneFic2(IndSaut) = "\Ligne�1"
                IndSaut = IndSaut + 1
                LigneFic2(IndSaut) = "\Colonne�" & Flex.Cols
                IndSaut = IndSaut + 1
                LigneFic2(IndSaut) = "\Varlig�"
                IndSaut = IndSaut + 1
                
                LigneFic2(IndSaut) = "\Col�" & Trim(Str(Echelle.ScaleX(Flex.RowHeight(i - 1), Echelle.ScaleMode, 7))) & "�"
                For k = 0 To Flex.Cols - 1
                    Flex.Col = k
                    '-> Alignement de la cellule
                    LigneFic2(IndSaut) = LigneFic2(IndSaut) & Flex.CellAlignment & ";"
                    '-> texte de la cellule
                    LigneFic2(IndSaut) = LigneFic2(IndSaut) & ";"
                    '-> Largeur de la cellule
                    LigneFic2(IndSaut) = LigneFic2(IndSaut) & Trim(Str(Echelle.ScaleX(Flex.ColWidth(k), Echelle.ScaleMode, 7))) & ";"
                    '-> Bordure de la cellule ( x 4 )
                    LigneFic2(IndSaut) = LigneFic2(IndSaut) & "VRAI,VRAI,VRAI,VRAI" & ";"
                    LigneFic2(IndSaut) = LigneFic2(IndSaut) & Flex.CellFontName & ";"
            
                    LigneFic2(IndSaut) = LigneFic2(IndSaut) & Flex.CellFontSize & ";"
                    
                    '-> Cellule en gras
                    If Flex.CellFontBold = True Then
                        LigneFic2(IndSaut) = LigneFic2(IndSaut) & "VRAI;"
                    Else
                        LigneFic2(IndSaut) = LigneFic2(IndSaut) & "FAUX;"
                    End If
                    '-> cellule en italic
                    If Flex.CellFontItalic = True Then
                        LigneFic2(IndSaut) = LigneFic2(IndSaut) & "VRAI;"
                    Else
                        LigneFic2(IndSaut) = LigneFic2(IndSaut) & "FAUX;"
                    End If
                    '-> Cellule en souligne
                    If Flex.CellFontUnderline = True Then
                        LigneFic2(IndSaut) = LigneFic2(IndSaut) & "VRAI;"
                    Else
                        LigneFic2(IndSaut) = LigneFic2(IndSaut) & "FAUX;"
                    End If
                    
                    '-> Couleur de police de la cellule
                    LigneFic2(IndSaut) = LigneFic2(IndSaut) & Flex.CellForeColor & ";"
                    '-> Couleur de fond   de la cellule
                    If (Flex.FixedRows >= 1 And i < Flex.FixedRows) Or (Flex.FixedCols >= 1 And j < Flex.FixedCols) Then
                        LigneFic2(IndSaut) = LigneFic2(IndSaut) & Flex.BackColorFixed & ";"
                    Else
                        If Flex.CellBackColor <> 0 Then
                            LigneFic2(IndSaut) = LigneFic2(IndSaut) & Flex.CellBackColor & ";"
                        Else
                            LigneFic2(IndSaut) = LigneFic2(IndSaut) & Flex.BackColor & ";"
                        End If
                    End If
                    If InStr(1, Flex.Text, Chr(13)) = 0 Then
                        LigneFic2(IndSaut) = LigneFic2(IndSaut) & "FAUX;0@0@1|"
                    Else
                        LigneFic2(IndSaut) = LigneFic2(IndSaut) & "VRAI;0@0@1|"
                    End If
                Next k
                '-> Fin de block
                IndSaut = IndSaut + 1
                LigneFic2(IndSaut) = "\Champs�"
                IndSaut = IndSaut + 1
                LigneFic2(IndSaut) = "\End�BlockPAGE1"
                IndSaut = IndSaut + 1
                
                LigneFic = LigneFic & "VRAI,FAUX,VRAI,VRAI" & ";"
                
            End If
        Else
            LigneFic = LigneFic & "VRAI,VRAI,VRAI,VRAI" & ";"
        End If
        
        LigneFic = LigneFic & Flex.CellFontName & ";"
        
        LigneFic = LigneFic & Flex.CellFontSize & ";"
        
        '-> Cellule en gras
        If Flex.CellFontBold = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> cellule en italic
        If Flex.CellFontItalic = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> Cellule en souligne
        If Flex.CellFontUnderline = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        
        '-> Couleur de police de la cellule
        LigneFic = LigneFic & Flex.CellForeColor & ";"
        '-> Couleur de fond   de la cellule
        If (Flex.FixedRows >= 1 And i < Flex.FixedRows) Or (Flex.FixedCols >= 1 And j < Flex.FixedCols) Then
            LigneFic = LigneFic & Flex.BackColorFixed & ";"
        Else
            If Flex.CellBackColor <> 0 Then
                LigneFic = LigneFic & Flex.CellBackColor & ";"
            Else
                LigneFic = LigneFic & Flex.BackColor & ";"
            End If
        End If

        If InStr(1, Flex.Text, Chr(13)) = 0 Then
            LigneFic = LigneFic & "FAUX;0@0@1|"
        Else
            LigneFic = LigneFic & "VRAI;0@0@1|"
        End If
    Next j
    
    Print #MaqTurbo, LigneFic
    
    '-> Fin de block
    Print #MaqTurbo, "\Champs�"
    Print #MaqTurbo, "\End�Block" & Trim(Str(i))
    
    If LigneFic2(0) <> "" Then
        For k = 0 To IndSaut
            Print #MaqTurbo, LigneFic2(k)
            LigneFic2(k) = ""
        Next
    End If
    
Next i
'-> Fin tableau
Print #MaqTurbo, "\Tableau�End"

'-> Edition conditonnelle d'un pied de page
If TxtPied <> "" Then
    PrintPied MaqTurbo, MiseEnPage, TxtPied, HauteurPied, LargeurRef
    '-> On envoie l'entete dans le fichier ascii
    For i = 0 To IndPied
        Print #MaqTurbo, LignePied(i)
    Next
End If

'-> determine la fin de la description de la page a imprimer
Print #MaqTurbo, "[/MAQ]"

'-> On envoie les instructions turbo pour imprimer l'entete si elle a ete demandee
If TxtEntete <> "" Then
    Print #MaqTurbo, "[ST-SectionE()]"
End If

'-> Lignes d'ordres d'impression envoy�s dans fichier ascii
If NoPage > 1 Then
    ListePages = Mid$(ListePages, 1, Len(ListePages) - 1)
End If
For i = 0 To Flex.Rows - 1
    For k = 1 To NoPage - 1
        If i = Int(Entry(k, ListePages, ",")) Then
            Print #MaqTurbo, "[TB-Tableau(BLK-blockPAGE1)][\1]"
            If TxtPied <> "" Then Print #MaqTurbo, "[ST-SectionP()]"
            Print #MaqTurbo, "[PAGE]"
            '-> reedition entete et pied de page si saut de page et si precis� par programmeur
            If ReEdit = True Then
                If TxtEntete <> "" Then Print #MaqTurbo, "[ST-SectionE()]"
            End If
        End If
    Next k
    Print #MaqTurbo, "[TB-Tableau(BLK-block" & Trim(Str(i)) & ")][\1]"
Next i

'-> Si plusieurs pages on edite une ligne de fin de tableau comme pour un saut de page
If NoPage > 1 Then
    Print #MaqTurbo, "[TB-Tableau(BLK-blockPAGE1)][\1]"
    If TxtPied <> "" Then
        Print #MaqTurbo, "[ST-SectionP()]"
    End If
End If

'-> pied de page pour la derniere page
If TxtPied <> "" Then Print #MaqTurbo, "[ST-SectionP()]"

'-> On ferme le fichier
Close MaqTurbo

'-> retour du pointeur a la normale
Flex.Parent.MousePointer = flexDefault

'->on lance l'edition par turbograph
Shell PrinterPath & " " & Fichier, vbMaximizedFocus

GestError:
    '-> Lib�rer le pointeur de souris
    Flex.Parent.Enabled = True
    Flex.Parent.MousePointer = 0
    LockWindowUpdate 0

End Sub

Private Sub PrintPied(ByVal MaqTurbo As Integer, ByVal MiseEnPage As String, TxtPied As String, HauteurRef As Long, LargeurRef As Long)
'-> Fonction qui edite une entete d'edition au format TURBO
Dim i As Integer

'-> Saisie des param�trages entete

i = 0
IndPied = 0
    
'-> SECTION 2
LignePied(IndPied) = "[NV-SectionP-STD]"
IndPied = IndPied + 1
LignePied(IndPied) = "\Hauteur�1"
IndPied = IndPied + 1
LignePied(IndPied) = "\Largeur�0"
IndPied = IndPied + 1
LignePied(IndPied) = "\RgChar�"
IndPied = IndPied + 1
LignePied(IndPied) = "\Contour�0"
IndPied = IndPied + 1
LignePied(IndPied) = "\AlignementTop�" & HauteurRef
IndPied = IndPied + 1
LignePied(IndPied) = "\Top�0"
IndPied = IndPied + 1
LignePied(IndPied) = "\AlignementLeft�0"
IndPied = IndPied + 1
LignePied(IndPied) = "\Left�0"
IndPied = IndPied + 1
LignePied(IndPied) = "\BackColor�16777215"
IndPied = IndPied + 1
LignePied(IndPied) = "\Champs�"
IndPied = IndPied + 1
LignePied(IndPied) = "\Texte�End"
IndPied = IndPied + 1

'-> Carde 2
LignePied(IndPied) = "\Cadre�Nouveau Cadre3"
IndPied = IndPied + 1
LignePied(IndPied) = "\Left�1"
IndPied = IndPied + 1
LignePied(IndPied) = "\Top�" & HauteurRef
IndPied = IndPied + 1
LignePied(IndPied) = "\Distance�0.125"
IndPied = IndPied + 1
LignePied(IndPied) = "\Hauteur�0.75"
IndPied = IndPied + 1
LignePied(IndPied) = "\Largeur�" & LargeurRef
IndPied = IndPied + 1
LignePied(IndPied) = "\LargeurBordure�2"
IndPied = IndPied + 1
LignePied(IndPied) = "\RoundRect�0"
IndPied = IndPied + 1
LignePied(IndPied) = "\Couleur�8421504"
IndPied = IndPied + 1
LignePied(IndPied) = "\Contour�1,1,1,1"
IndPied = IndPied + 1
LignePied(IndPied) = "\Locked�0"
IndPied = IndPied + 1
LignePied(IndPied) = "\Masteralign�"
IndPied = IndPied + 1
LignePied(IndPied) = "\MasterIndexAlign�PAGE-8-AlignPage8"
IndPied = IndPied + 1
LignePied(IndPied) = "\SlaveAlign�"
IndPied = IndPied + 1
LignePied(IndPied) = "\AlignX�0"
IndPied = IndPied + 1
LignePied(IndPied) = "\AlignY�0"
IndPied = IndPied + 1
LignePied(IndPied) = "\Champs�"
IndPied = IndPied + 1
LignePied(IndPied) = "\Cadre�End"
IndPied = IndPied + 1
'-> Fin Cadre 2

'-> Titre saisit par l'utilisateur
LignePied(IndPied) = "\CadreRtf�Begin"
IndPied = IndPied + 1
LignePied(IndPied) = "{\rtf1\ansi\ansicpg1252\deff0\deflang1036{\fonttbl{\f0\fnil\fcharset0 Times New Roman;}}"
IndPied = IndPied + 1
LignePied(IndPied) = "\viewkind4\uc1\pard\qc\ul\b\f0\fs20 " & TxtPied & " \ulnone\b0\fs20"
IndPied = IndPied + 1
LignePied(IndPied) = "\par }"
IndPied = IndPied + 1
LignePied(IndPied) = "\CadreRtf�End"
IndPied = IndPied + 1

End Sub
