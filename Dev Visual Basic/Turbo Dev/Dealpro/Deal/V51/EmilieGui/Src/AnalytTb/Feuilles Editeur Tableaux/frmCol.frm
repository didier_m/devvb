VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmCol 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   5205
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5430
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5205
   ScaleWidth      =   5430
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Height          =   375
      Left            =   4920
      Picture         =   "frmCol.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   4800
      Width           =   495
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   4695
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   8281
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "G�n�ral"
      TabPicture(0)   =   "frmCol.frx":0442
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Calcul"
      TabPicture(1)   =   "frmCol.frx":045E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame3"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame3 
         Caption         =   "Elements du calcul : "
         Height          =   4215
         Left            =   -74880
         TabIndex        =   14
         Top             =   360
         Width           =   5175
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   330
            Left            =   120
            TabIndex        =   16
            Top             =   360
            Width           =   735
            _ExtentX        =   1296
            _ExtentY        =   582
            ButtonWidth     =   609
            ButtonHeight    =   582
            Wrappable       =   0   'False
            Style           =   1
            ImageList       =   "imlToolbarIcons"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ADD"
                  Object.ToolTipText     =   "Ins�rer une colonne"
                  ImageKey        =   "Macro"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "CHECK"
                  Object.ToolTipText     =   "V�rifier la formule"
                  ImageKey        =   "Spell Check"
               EndProperty
            EndProperty
         End
         Begin RichTextLib.RichTextBox Rtf 
            Height          =   3255
            Left            =   120
            TabIndex        =   15
            Top             =   840
            Width           =   4935
            _ExtentX        =   8705
            _ExtentY        =   5741
            _Version        =   393217
            Enabled         =   -1  'True
            TextRTF         =   $"frmCol.frx":047A
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Propri�t�s : "
         Height          =   4215
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   5175
         Begin VB.ComboBox Combo2 
            Height          =   315
            Left            =   1560
            TabIndex        =   19
            Top             =   2040
            Width           =   3015
         End
         Begin VB.ComboBox Combo1 
            Height          =   315
            Left            =   1560
            TabIndex        =   17
            Top             =   1680
            Width           =   3015
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1560
            ScrollBars      =   3  'Both
            TabIndex        =   0
            Top             =   600
            Width           =   1215
         End
         Begin VB.TextBox Text2 
            Height          =   285
            Left            =   1560
            TabIndex        =   1
            Top             =   960
            Width           =   3015
         End
         Begin VB.TextBox Text3 
            Height          =   285
            Left            =   1560
            TabIndex        =   2
            Top             =   1320
            Width           =   1215
         End
         Begin VB.Frame Frame2 
            Caption         =   "Type de colonne : "
            Height          =   1455
            Left            =   600
            TabIndex        =   4
            Top             =   2520
            Width           =   3975
            Begin VB.OptionButton TypeLig 
               Caption         =   "Colonne de s�paration"
               Height          =   195
               Index           =   2
               Left            =   240
               TabIndex        =   12
               Top             =   1080
               Width           =   2535
            End
            Begin VB.OptionButton TypeLig 
               Caption         =   "Colonne li�e � une entr�e du fichier"
               Height          =   195
               Index           =   0
               Left            =   240
               TabIndex        =   8
               Top             =   360
               Value           =   -1  'True
               Width           =   3135
            End
            Begin VB.OptionButton TypeLig 
               Caption         =   "Colonne de calcul"
               Height          =   195
               Index           =   1
               Left            =   240
               TabIndex        =   7
               Top             =   720
               Width           =   2535
            End
         End
         Begin VB.CheckBox Check1 
            Alignment       =   1  'Right Justify
            Caption         =   "Visible"
            Height          =   195
            Left            =   3240
            TabIndex        =   3
            Top             =   1350
            Width           =   1335
         End
         Begin VB.Image Image1 
            Height          =   240
            Left            =   4680
            Picture         =   "frmCol.frx":04FC
            ToolTipText     =   "Acc�s au descriptif"
            Top             =   1920
            Width           =   240
         End
         Begin VB.Label Label4 
            Caption         =   "Champ : "
            Height          =   255
            Left            =   600
            TabIndex        =   20
            Top             =   2040
            Width           =   855
         End
         Begin VB.Label Label5 
            Caption         =   "Progiciel : "
            Height          =   255
            Left            =   600
            TabIndex        =   18
            Top             =   1680
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Code : "
            Height          =   255
            Left            =   600
            TabIndex        =   11
            Top             =   600
            Width           =   735
         End
         Begin VB.Label Label2 
            Caption         =   "Libell� : "
            Height          =   255
            Left            =   600
            TabIndex        =   10
            Top             =   960
            Width           =   1215
         End
         Begin VB.Label Label3 
            Caption         =   "Largeur : "
            Height          =   255
            Left            =   600
            TabIndex        =   9
            Top             =   1320
            Width           =   855
         End
      End
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   0
      Top             =   4680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCol.frx":0886
            Key             =   "Macro"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCol.frx":0998
            Key             =   "Spell Check"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public TbCode As String 'code du tableau en cours
Public aCol As clsCol 'Pointeur vers la colonne en cours

'-> Indique le type de la colonne
Private IdTypeCol As Integer

'-> Indique pour l'�diteur de formule que l'on est en cours de chargement
Dim IsInserting As Boolean

Private Sub Combo1_Click()

    '---> Charger le descro attach� au fichier
    
    Dim aTb As clsTableau
    
    '-> quitter si progiciel = ""
    If Me.Combo1.List(Me.Combo1.ListIndex) = "" Then Exit Sub
    
    '-> Get d'un pointeurs vers le tableau
    Set aTb = Tableaux("TB_" & Me.TbCode)
    
    '-> Charger le descriptif
    LoadDescro UCase$(Trim(Entry(1, Me.Combo1.List(Me.Combo1.ListIndex), "-"))), aCol.Field

End Sub

Private Sub Command1_Click()

    '-> D�charger la feuille
    Unload Me

End Sub

Public Sub Initialisation()

    '---> Cette proc�dure initialise avec les propri�t�s de la colonne
    Dim aLb As Libelle
    
    '-> on charge les libelles
    Set aLb = Libelles("FRMCOL")
    
    '-> Propri�t�s
    Me.Text1.Text = aCol.Code
    Me.Text2.Text = aCol.Libelle
    Me.Text3.Text = aCol.Largeur
    Me.Check1.Value = Abs(CInt(aCol.Visible))
    
    '-> Cliqu� sur le type de colonne
    TypeLig_Click (aCol.TypeCol)
    TypeLig(aCol.TypeCol).Value = True
    
    '-> Caption de la feuille MESSPROG
    Me.Caption = aLb.GetCaption(1) & aCol.Code & " - " & aCol.Libelle
    
    '-> Charger les progiciels
    Loadprogi
    
    '-> Cr�er la liste des calculs
    If aCol.TypeCol = 1 Then
        '-> Ligne de type calcul
        IsInserting = True
        Me.Rtf.TextRTF = aCol.CalculRtf
    Else
        Me.Rtf.Text = ""
    End If
    
    IsInserting = False
    
    Set aLb = Nothing
    
End Sub

Private Sub Loadprogi(Optional Modif As Boolean)

    '---> Cette proc�dure r�initialise les 2 combo Progiciel et field
    
    Dim i As Integer
    Dim aTb As clsTableau
    Dim aProgiciel As clsProgiciel
    Dim Val1 As String
    Dim Val2 As String
    
    '-> Recup des valeurs si necessaire
    If Modif Then
        If Me.Combo1.ListIndex <> -1 Then Val1 = UCase$(Trim(Entry(1, Me.Combo1.Text, "-")))
        If Me.Combo2.ListIndex <> -1 Then Val2 = UCase$(Trim(Entry(1, Me.Combo2.Text, "-")))
    End If
    
    '-> Get d'un pointeur vers le tableau
    Set aTb = Tableaux("TB_" & Me.TbCode)
    
    '-> Init des combos
    Me.Combo1.Clear
    Me.Combo1.AddItem ""
    Me.Combo2.Clear
    Me.Combo2.AddItem ""
    
    '-> Charger la liste des progiciels affect�s au tableau
    For Each aProgiciel In aTb.Progiciels
        '-> Ajouter le progiciel dans la liste
        Me.Combo1.AddItem aProgiciel.Code & "-" & Progiciels("PRO_" & aProgiciel.Code).Libelle
        If Modif Then
                If aProgiciel.Code = Val1 Then
                    '-> S�lectionner le progiciel dans la liste
                    Me.Combo1.ListIndex = Me.Combo1.ListCount - 1
                    '-> Charger son descriptif
                    LoadDescro Val1, Val2
                End If
            Else
                '-> Si param de la colonne sont saisis
                If aProgiciel.Code = aCol.Progiciel Then
                    Me.Combo1.ListIndex = Me.Combo1.ListCount - 1
                    '-> Charger la combo2 avec la liste des champs de ce progiciel
                    LoadDescro aCol.Progiciel, aCol.Field
                End If
            End If
        Next

                
End Sub

Private Sub LoadDescro(IdProgi As String, Optional CodeCol As String)

    '---> Cette proc�dure load la liste des champs affect� � un descro
    
    Dim aTb As clsTableau
    Dim aProgiciel As clsProgiciel
    Dim aDes As clsDescriptif
    
    '-> Get d'un pointeur vers le tableau
    Set aTb = Tableaux("TB_" & Me.TbCode)
    
    '-> Vider la combo
    Me.Combo2.Clear
    
    '-> Ajouter le premier champ � blanc
    Me.Combo2.AddItem ""
    
    '-> Pointer sur le progiciel sp�cifi�
    Set aProgiciel = aTb.Progiciels("PRO_" & IdProgi)
    
    '-> Ajouter tous les champs
    For Each aDes In aProgiciel.Descriptifs
        '-> Ajouter dans la liste
        Me.Combo2.AddItem aDes.Field & " - " & aDes.Commentaire
        '-> S�lectionner une champ si sp�cifi�
        If CodeCol <> "" Then
            If aDes.Field = CodeCol Then Me.Combo2.ListIndex = Me.Combo2.ListCount - 1
        End If
    Next

End Sub

Private Sub Form_Unload(Cancel As Integer)

    Dim IsError  As Boolean
    Dim aTb As clsTableau
    Dim aLb As Libelle
    
    '-> charge les libelles
    Set aLb = Libelles("FRMCOL")
    
    '-> V�rifier la largeur de la colonne
    If Trim(Me.Text3.Text) = "" Then IsError = True
    If Not IsNumeric(Me.Text3.Text) Then
        IsError = True
    Else
        If CLng(Me.Text3.Text) = 0 Then IsError = True
    End If
    
    If IsError Then 'MESSPROG
        MsgBox aLb.GetCaption(2), vbCritical + vbOKOnly, aLb.GetCaption(3)
        Me.Text3.SetFocus
        Cancel = 1
        Exit Sub
    End If
    
    '-> Ne v�rifier l'entr�e du fichier que si on est sur une colonne de d�tail
    If IdTypeCol = 0 Then
        '-> V�rifier qu'un progiciel soit s�lectionn�
        If Me.Combo1.ListIndex = 0 Then 'MESSPROG
            MsgBox aLb.GetCaption(4), vbExclamation + vbOKOnly, aLb.GetCaption(3)
            Me.Combo1.SetFocus
            Cancel = 1
            Exit Sub
        End If
        
        '-> V�rifier qu'un champ soit s�lectionn�
        If Me.Combo2.ListIndex = -1 Then 'MESSPROG
            MsgBox aLb.GetCaption(5), vbExclamation + vbOKOnly, aLb.GetCaption(3)
            Me.Combo2.SetFocus
            Cancel = 1
            Exit Sub
        End If
    End If 'Si colonne de type d�tail
    
    If IsError Then 'MESSPROG
        MsgBox aLb.GetCaption(2), vbCritical + vbOKOnly, aLb.GetCaption(3)
        Me.Text3.SetFocus
        Cancel = 1
        Exit Sub
    End If
    
    '-> Mise � jour des prori�t�s de l'objet Colonne
    aCol.Libelle = Trim(Me.Text2.Text)
    aCol.Largeur = CLng(Me.Text3.Text)
    aCol.LargeurTwips = Me.ScaleX(aCol.Largeur, 6, 1)
    aCol.Visible = CBool(Me.Check1.Value)
    aCol.TypeCol = IdTypeCol
    
    '-> vider les zones de calcul
    aCol.Calcul = ""
    aCol.CalculRtf = ""
    
    Select Case IdTypeCol
        Case 0 'Li� � un fichier
            '-> Get d'un pointeur vers le tableau
            Set aTb = Tableaux("TB_" & Me.TbCode)
            aCol.Progiciel = Entry(1, UCase$(Trim(Me.Combo1.List(Me.Combo1.ListIndex))), "-")
            aCol.Field = UCase$(Trim(Entry(1, Me.Combo2.List(Me.Combo2.ListIndex), "-")))
            '-> Virer les forlules de calcul
            aCol.Calcul = ""
            aCol.CalculRtf = ""
        Case 1 'Type Calcul
            aCol.Calcul = Me.Rtf.Text
            aCol.CalculRtf = Me.Rtf.TextRTF
            aCol.Progiciel = ""
            aCol.Field = ""
        Case 2 'Type s�paration
            aCol.Calcul = ""
            aCol.CalculRtf = ""
            aCol.Progiciel = ""
            aCol.Field = ""
    End Select
    
    Set aLb = Nothing
    
End Sub

Private Sub Image1_Click()

    '---> Afficher le gestionnaire de descriptif
    
    Dim aTb As clsTableau
    
    '-> Get d'un pointeur vers le tableau
    Set aTb = Tableaux("TB_" & Me.TbCode)
    
    '-> Initialiser le gestionnaire de descriptif
    Set frmDescriptif.aTb = aTb
    frmDescriptif.Show vbModal
    
    '-> Recharger la liste des progiciels
    Loadprogi True

End Sub

Private Sub Rtf_KeyPress(KeyAscii As Integer)

    '---> Gestion de l'�diteur de fonction
    
    If IsInserting Then Exit Sub
    
    Select Case KeyAscii
        Case 40, 41 'Parenth�ses
            Me.Rtf.SelColor = RGB(0, 255, 0)
        Case 8, 42, 43, 45, 47 'Signes math
            Me.Rtf.SelColor = RGB(0, 0, 255)
        Case 48 To 57 'Chiffres
            Me.Rtf.SelColor = 0
        Case Else 'Annulation
            KeyAscii = 0
    End Select

End Sub


Private Sub Text2_GotFocus()
    SelectTxtBox Me.Text2
End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)

    Select Case KeyAscii
        Case 45, 61, 124, 167
            KeyAscii = 0
    End Select

End Sub

Private Sub Text3_GotFocus()
    SelectTxtBox Me.Text3
End Sub

Private Sub Text3_KeyPress(KeyAscii As Integer)
    KeyAscii = GetIntAscii(KeyAscii)
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

    Dim Msg As String
    Dim aLb As Libelle
    
    '-> Charge les libelles
    Set aLb = Libelles("FRMCOL")
    
    '-> Selon le button
    Select Case Button.Key
        Case "ADD" 'Insertion du code d'une colonne dans la formule
            AddColInFormule
            
        Case "CHECK" 'V�rification de la syntaxe MESSPROG
            If Not CheckFormule(Me.Rtf.Text, Tableaux("TB_" & UCase$(Trim(Me.TbCode)))) Then
                MsgBox aLb.GetCaption(6) & Chr(13) & strError, vbCritical + vbOKOnly, aLb.GetCaption(3)
                Exit Sub
            End If
            
    End Select
    
    Set aLb = Nothing
    
End Sub

Private Sub AddColInFormule()

    Dim MyCol As clsCol
    Dim ToInsert As String
    
    '-> vider la variable d'�change
    strRetour = ""
    
    '-> Afficher la liste des colonnes excistantes
    Set frmListeCol.aTb = Tableaux("TB_" & Me.TbCode)
    frmListeCol.TypeDisplay = 0
    frmListeCol.ObjectInit = aCol.Code
    frmListeCol.Init
    frmListeCol.Show vbModal
    If strRetour = "" Then Exit Sub
    
    '-> Traiter le retour
    If Entry(1, strRetour, Chr(0)) = "OBJECT" Then
        '-> On a renvoy� le nom d'une colonne
        ToInsert = "$COL�" & Entry(2, strRetour, Chr(0)) & "$"
    Else
        '-> On a renvoy� un champ et son progiciel
        ToInsert = "$PRO�" & Entry(2, strRetour, Chr(0)) & "�" & Entry(3, strRetour, Chr(0)) & "$"
    End If
    
    '-> Ins�rer dans la colonne
    IsInserting = True
    Me.Rtf.SelColor = RGB(255, 0, 0)
    Me.Rtf.SelText = ToInsert
    IsInserting = False
    Me.Rtf.SetFocus
    
EndProc:
    '-> Vider la variable de retour
    strRetour = ""

End Sub

Private Sub TypeLig_Click(Index As Integer)

    '-> Selon le type de colonne
    If Index = 1 Then
        Me.SSTab1.TabEnabled(1) = True
    Else
        Me.SSTab1.TabEnabled(1) = False
    End If
    
    '-> Indiquer sur quel type de colonne on est
    IdTypeCol = Index

End Sub

