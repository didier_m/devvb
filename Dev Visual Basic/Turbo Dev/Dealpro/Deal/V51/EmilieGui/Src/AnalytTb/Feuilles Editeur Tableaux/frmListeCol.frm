VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmListeCol 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   4830
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   6150
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   322
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   410
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin TabDlg.SSTab SSTab1 
      Height          =   4815
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   8493
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   529
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmListeCol.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label3"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label4"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Command2"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Command1"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Combo2"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "List2"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmListeCol.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Command4"
      Tab(1).Control(1)=   "Command3"
      Tab(1).Control(2)=   "List1"
      Tab(1).Control(3)=   "Combo1"
      Tab(1).Control(4)=   "Label2"
      Tab(1).Control(5)=   "Image1"
      Tab(1).Control(6)=   "Label1"
      Tab(1).ControlCount=   7
      Begin VB.ListBox List2 
         Height          =   3180
         Left            =   2040
         TabIndex        =   12
         Top             =   960
         Width           =   3615
      End
      Begin VB.ComboBox Combo2 
         Height          =   315
         Left            =   2040
         TabIndex        =   10
         Top             =   480
         Width           =   3615
      End
      Begin VB.CommandButton Command4 
         Height          =   375
         Left            =   -69840
         Picture         =   "frmListeCol.frx":0038
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   4320
         Width           =   495
      End
      Begin VB.CommandButton Command3 
         Cancel          =   -1  'True
         Height          =   375
         Left            =   -70440
         Picture         =   "frmListeCol.frx":047A
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   4320
         Width           =   495
      End
      Begin VB.CommandButton Command1 
         Height          =   375
         Left            =   5160
         Picture         =   "frmListeCol.frx":0804
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   4320
         Width           =   495
      End
      Begin VB.CommandButton Command2 
         Height          =   375
         Left            =   4560
         Picture         =   "frmListeCol.frx":0C46
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   4320
         Width           =   495
      End
      Begin VB.ListBox List1 
         Height          =   3180
         Left            =   -72960
         TabIndex        =   4
         Top             =   960
         Width           =   3615
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   -72960
         TabIndex        =   2
         Top             =   480
         Width           =   3615
      End
      Begin VB.Label Label4 
         Caption         =   "S�lection d'un champ : "
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   960
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "Type de colonne : "
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   530
         Width           =   1935
      End
      Begin VB.Label Label2 
         Caption         =   "S�lection d'un champ : "
         Height          =   255
         Left            =   -74880
         TabIndex        =   3
         Top             =   960
         Width           =   1815
      End
      Begin VB.Image Image1 
         Height          =   240
         Left            =   -69300
         Picture         =   "frmListeCol.frx":0FD0
         Top             =   500
         Width           =   240
      End
      Begin VB.Label Label1 
         Caption         =   "S�lection d'un progiciel : "
         Height          =   255
         Left            =   -74880
         TabIndex        =   1
         Top             =   530
         Width           =   1935
      End
   End
End
Attribute VB_Name = "frmListeCol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Public aTb As clsTableau 'Pointeur vers un tableau
Public TypeDisplay As Integer  'Indique si on affiche la liste des lignes ou des colonnes
Public ObjectInit As String 'Code de la ligne ou de la colonne en cours
Public TypeProgi As String 'N'afficher les champs que pour ce progiciel

Public Sub Init()

    '---> Cette proc�dure initialise l'interface en fonction de ce que l'on doit afficher
    
    'MESSPROG pour toute la feuille
    Dim aLb As Libelle
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMLISTECOL")
    
    '-> Libell�s
    Me.Caption = aLb.GetCaption(1)
    
    '-> Selon ligne ou colonne
    If TypeDisplay = 0 Then 'Colonne
        Me.SSTab1.TabCaption(0) = aLb.GetCaption(2)
        Me.Label3.Caption = aLb.GetCaption(3)
        Me.Label4.Caption = aLb.GetCaption(4)
        '-> Ajouter les types de colonne
        Me.Combo2.AddItem aLb.GetCaption(5)
        Me.Combo2.AddItem aLb.GetCaption(6)
    ElseIf TypeDisplay = 1 Then  'Ligne
        Me.SSTab1.TabCaption(0) = aLb.GetCaption(7)
        Me.Label3.Caption = aLb.GetCaption(8)
        Me.Label4.Caption = aLb.GetCaption(9)
        '-> Ajouter les type de ligne
        Me.Combo2.AddItem aLb.GetCaption(10)
        Me.Combo2.AddItem aLb.GetCaption(11)
    ElseIf TypeDisplay = 2 Then 'Champ pour condition
        Me.SSTab1.TabCaption(0) = ""
        Me.SSTab1.TabEnabled(0) = False
        Me.SSTab1.Tab = 1
        Me.Image1.Visible = False
    End If 'Si on est en mode ligne ou colonne
    
    '-> Dans les 2 cas , ajouter la liste des progiciels
    Me.SSTab1.TabCaption(1) = aLb.GetCaption(12)
    Set aLb = Nothing
    Loadprogi

End Sub

Private Sub Loadprogi()

    '---> Cette proc�dure vide la combo et la liste des champs et la charge avec les progiciels affect�s
    
    Dim i As Integer
    Dim ToAdd As Boolean
    Dim aProgiciel As clsProgiciel
    
    Me.Combo1.Clear
    Me.List1.Clear
    
    
    For Each aProgiciel In aTb.Progiciels '-> Attention si on ne doit afficher que les champs d'un progiciel
        If Me.TypeDisplay = 2 Then
            If aProgiciel.Code = Me.TypeProgi Then
                ToAdd = True
            Else
                ToAdd = False
            End If
        Else
            ToAdd = True
        End If
        '-> Ajouter le progiciel dans la liste
        If ToAdd Then
            '-> AJouter dans la liste
            Me.Combo1.AddItem aProgiciel.Code & "-" & Progiciels("PRO_" & aProgiciel.Code).Libelle
            If Me.TypeDisplay = 2 Then
                Me.Combo1.ListIndex = Me.Combo1.ListCount - 1
                Combo1_Click
            End If
        End If 'Si on doit l'ajouter
    Next


End Sub

Private Sub LoadObject()

    '---> Cette proc�dure affiche les lignes/colonnes selon le type s�lectionn�
    
    Dim aCol As clsCol
    Dim aRow As clsRow
    
    '-> Ne rien faire si le type d'objet n'est pas sp�cifi�
    If Me.Combo2.ListIndex = -1 Then Exit Sub
    
    '-> Vider la liste de rec�ption
    Me.List2.Clear
    
    If Me.TypeDisplay = 0 Then  'Colonnes
        '-> Afficher toutes les colonnes du type sp�cifi�
        For Each aCol In aTb.Colonnes
            If aCol.TypeCol = Me.Combo2.ListIndex Then
                If aCol.Code <> Me.ObjectInit Then
                    Me.List2.AddItem aCol.Code & " - " & aCol.Libelle
                End If 'Si code <> de l'objet d'origine
            End If 'Si on est dans le m�me type
        Next 'Pour toutes les colonnes
    Else 'Lignes
        For Each aRow In aTb.Lignes
            If aRow.TypeLig = Me.Combo2.ListIndex Then
                If aRow.Code <> Me.ObjectInit Then
                    Me.List2.AddItem aRow.Code & " - " & aRow.Code
                End If 'Si code <> de l'objet d'origine
            End If 'Si on est sur le m�me type de ligne
        Next
    End If 'Selon le type d'objet


End Sub

Private Sub Combo1_Click()

    '->Ne rien faire si pas de progciel
    If Me.Combo1.ListIndex = -1 Then Exit Sub
    
    '-> Charger ele descriptif du progiciel sp�cifi�
    LoadDescro UCase$(Trim(Entry(1, Me.Combo1.List(Me.Combo1.ListIndex), "-")))

End Sub

Private Sub Combo2_Click()

    '-> Charger la liste des objets sp�cifi�e
    LoadObject

End Sub

Private Sub Command1_Click()

'---> Retourner l'objet s�lectionn�
    
    Dim aLb As Libelle
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMLISTECOL")
    
    '-> V�rifier que l'objet soit s�lectionn�
    If Me.List2.ListIndex = -1 Then 'MESSPROG
        MsgBox aLb.GetCaption(13), vbQuestion + vbOKOnly, aLb.GetCaption(14)
        Me.List2.SetFocus
        Exit Sub
    End If
        
    '-> Renvoyer l'objet s�lectionner
    strRetour = "OBJECT" & Chr(0) & UCase$(Trim(Entry(1, Me.List2.List(Me.List2.ListIndex), "-")))
    
    Set aLb = Nothing
    
    '-> D�charger la feuille
    Unload Me

End Sub

Private Sub Command2_Click()
    strRetour = ""
    Unload Me
End Sub

Private Sub Command3_Click()
    strRetour = ""
    Unload Me
End Sub

Private Sub LoadDescro(IdProgi As String)

    '---> Cette proc�dure load la liste des champs affect� � un descro
    
    Dim aDes As clsDescriptif
    Dim aProgiciel As clsProgiciel
    
    '-> Vider la combo
    Me.List1.Clear
    
    '-> Pointer sur le progiciel sp�cifi�
    Set aProgiciel = aTb.Progiciels("PRO_" & IdProgi)
    
    '-> Ajouter tous les champs
    For Each aDes In aProgiciel.Descriptifs
        '-> Ajouter dans la liste
        Me.List1.AddItem aDes.Field & " - " & aDes.Commentaire
    Next

End Sub

Private Sub Command4_Click()
    
    Dim aLb As Libelle
    
    '-> On charge les libelles
    Set aLb = Libelles("FRMLISTECOL")
    
    '-> Tester qu'un champ soit s�lectionn�
    If Me.List1.ListIndex = -1 Then  'MESSPROG
        MsgBox aLb.GetCaption(15), vbExclamation + vbOKOnly, aLb.GetCaption(14)
        Me.List1.SetFocus
        Exit Sub
    End If
    
    '-> Renvoyer le champ s�lectionn�
    strRetour = "FIELD" & Chr(0) & UCase$(Trim(Entry(1, Me.Combo1.List(Me.Combo1.ListIndex), "-"))) & Chr(0) & UCase$(Trim(Entry(1, Me.List1.List(Me.List1.ListIndex), "-")))
    
    Set aLb = Nothing
    
    '-> D�charger la feuille
    Unload Me

End Sub



Private Sub Image1_Click()
    '---> Afficher le gestionnaire de descriptif
    
    '-> Initialiser le gestionnaire de descriptif
    Set frmDescriptif.aTb = aTb
    'frmDescriptif.Init
    frmDescriptif.Show vbModal
    
    '-> Recharger la liste des progiciels
    Loadprogi

End Sub

Private Sub List1_DblClick()
    Command4_Click
End Sub

Private Sub List2_DblClick()
    Command1_Click
End Sub
