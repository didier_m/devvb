VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmOpenTab 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Choix du tableau et des fichiers � int�grer"
   ClientHeight    =   5820
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7200
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5820
   ScaleWidth      =   7200
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   6360
      Picture         =   "frmOpenTab.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   5160
      Width           =   735
   End
   Begin VB.CommandButton Command2 
      Height          =   615
      Left            =   5520
      Picture         =   "frmOpenTab.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   5160
      Width           =   735
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   2040
      Top             =   5160
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Ins�rer un fichier"
      Filter          =   "*.*"
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4320
      Top             =   4680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOpenTab.frx":0E14
            Key             =   "TAB"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOpenTab.frx":1AEE
            Key             =   "FILE"
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Command4 
      Height          =   615
      Left            =   960
      Picture         =   "frmOpenTab.frx":27C8
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   5160
      Width           =   735
   End
   Begin VB.CommandButton Command3 
      Height          =   615
      Left            =   120
      Picture         =   "frmOpenTab.frx":3492
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   5160
      Width           =   735
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   2175
      Left            =   60
      TabIndex        =   2
      Top             =   360
      Width           =   7095
      _ExtentX        =   12515
      _ExtentY        =   3836
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView ListView2 
      Height          =   2175
      Left            =   60
      TabIndex        =   3
      Top             =   2880
      Width           =   7095
      _ExtentX        =   12515
      _ExtentY        =   3836
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Fichier"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Progiciel"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Emplacement"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label3 
      Caption         =   "Liste des fichiers � int�grer :"
      Height          =   255
      Left            =   60
      TabIndex        =   1
      Top             =   2640
      Width           =   2655
   End
   Begin VB.Label Label1 
      Caption         =   "S�lection du tableau : "
      Height          =   255
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   1935
   End
End
Attribute VB_Name = "frmOpenTab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim FirstLoad As Boolean '-> Indique si on � d�ja ouvert un fichier
Dim ToUnload As Boolean '-> Indique que les seclections sont OK pour d�chargement normal

Private Sub Command1_Click()

'---> Lancer l'analyse

'-> V�rifier qu'un tableau soit s�lectionn�
If Me.ListView1.SelectedItem Is Nothing Then 'MESSPROG
    MsgBox "Veuillez s�lectionner un tableau", vbExclamation + vbOKOnly, "Erreur"
    Me.ListView1.SetFocus
    Exit Sub
End If

'-> V�rifier qu'il y ait au moins un fichier de s�lectionner
If Me.ListView2.ListItems.Count = 0 Then 'MESSPROG
    MsgBox "Veuillez ins�rer un fichier de donn�es", vbExclamation + vbOKOnly, "Erreur"
    Me.ListView2.SetFocus
    Exit Sub
End If

'-> V�rifier que le progiciel sp�cifi� appartienne au tableau THIERRY

'-> Indiquer que l'on d�charge la feuille
ToUnload = True

'-> Setting de la variable de tableau
TabFileName = Me.ListView1.SelectedItem.Tag

'-> D�charger la feuille
Unload Me


End Sub

Private Sub Command2_Click()

Unload Me

End Sub

Private Sub Command3_Click()

'---> Int�gration d'un fichier

Dim Progiciel As String
Dim FileName As String
Dim i As Integer
Dim x As ListItem


On Error GoTo GestError

'-> Afficher la feuille de choix des fichiers
Me.CommonDialog1.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly
If FirstLoad Then Me.CommonDialog1.InitDir = App.Path & "\Fichiers\"
Me.CommonDialog1.ShowOpen

'-> V�rifier si le fichier n'est pas d�ja int�gr�
For i = 1 To Files.Count
    If UCase$(Trim((Entry(1, Files(i), "|")))) = UCase$(Trim(Me.CommonDialog1.FileName)) Then   'MESSPROG
        MsgBox "Impossible d'int�grer deux fois le m�me fichier", vbExclamation + vbOKOnly, "Erreur"
        Exit Sub
    End If
Next

'-> Chercher la cl� progiciel
Progiciel = GetIniFileValue("PARAM", "PRO", Me.CommonDialog1.FileName)

'-> La demander s'il elle n'existe pas THIERRY

'-> Ajouter dans la collection des fichiers int�gr�s
FileName = Me.CommonDialog1.FileName
Files.Add FileName & "|" & Progiciel

'-> Ajouter dans le listeview
Set x = Me.ListView2.ListItems.Add(, Me.CommonDialog1.FileName, Entry(NumEntries(FileName, "\"), FileName, "\"), , "FILE")
x.SubItems(1) = Progiciel
x.SubItems(2) = FileName

'-> Ne plus modifier le path
FirstLoad = False

'-> Formatter le listview
FormatListView Me.ListView2


Exit Sub

GestError:
    

End Sub

Private Sub Command4_Click()

Dim Rep As VbMsgBoxResult
Dim i As Integer

'-> Ne rien faire si pas de fichier
If Me.ListView2.SelectedItem Is Nothing Then Exit Sub

'-> Demander confirm 'MESSPROG
Rep = MsgBox("Supprimer le fichier : " & Chr(13) & Chr(10) & Me.ListView2.SelectedItem.SubItems(2), vbQuestion + vbYesNo, "Confirmation")
If Rep = vbNo Then Exit Sub

'-> Supprimer de la collection
For i = 1 To Files.Count
    If Files(i) = Me.ListView2.SelectedItem.SubItems(2) Then
        Files.Remove (i)
    End If
Next

'-> Supprimer du listview
Me.ListView2.ListItems.Remove (Me.ListView2.SelectedItem.Index)

End Sub

Private Sub Form_Load()

'---> Charger la liste des tableaux de bords en cours

Dim FileName As String
Dim x As ListItem
Dim CodeTb As String
Dim Lib As String

'-> Anlayse de la liste des tableaux
FileName = Dir$(App.Path & "\Tableaux\*.data")
Do While FileName <> ""
    '-> R�cup�rer le code du tableau
    CodeTb = GetIniFileValue("PARAM_ENTETE", "NAME", App.Path & "\Tableaux\" & FileName)
    Lib = GetIniFileValue("PARAM_ENTETE", "LIBEL", App.Path & "\Tableaux\" & FileName)
    '-> Ajouter une icone
    Set x = Me.ListView1.ListItems.Add(, , Lib, "TAB")
    x.Tag = App.Path & "\Tableaux\" & FileName
    '-> Lire le fichier suivant
    FileName = Dir
Loop

'-> Initialiser la collection des fichiers charg�s
Set Files = New Collection
FirstLoad = True


End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

Dim Rep As VbMsgBoxResult

'-> Demander confirmation
If Not ToUnload Then
    Rep = MsgBox("Quitter maintenant ?", vbQuestion + vbYesNo, "Confirmation")
    If Rep = vbNo Then
        Cancel = 1
        Exit Sub
    End If
    '-> Fin du programme
    End
End If


End Sub
