VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmTableau 
   Caption         =   "Form1"
   ClientHeight    =   4035
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5685
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   269
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   379
   Begin VB.CommandButton Command1 
      Caption         =   "Enregistrer"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   720
      ScaleHeight     =   315
      ScaleWidth      =   915
      TabIndex        =   1
      Top             =   2400
      Visible         =   0   'False
      Width           =   975
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   1575
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   2778
      _Version        =   393216
      Rows            =   3
      WordWrap        =   -1  'True
      FocusRect       =   2
      AllowUserResizing=   3
      MouseIcon       =   "frmTableau.frx":0000
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuPrint 
         Caption         =   "Imprimer"
      End
      Begin VB.Menu mnuDetail 
         Caption         =   "Justifier"
      End
   End
End
Attribute VB_Name = "frmTableau"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> Cumul en cours d'affichage
Public aCumul As clsCumul

'-> Pour d�tection de la ligne et de la colonne sur laquelle on click
Dim ClickX As Integer
Dim ClickY As Integer

'-> Indique si on est sur un tableau issu de la consultation de l'axe ou du d�tail
Private TypeAxe As Integer

Private Sub Command1_Click()

Dim Rep As String

Rep = InputBox("Libell� de la simulation", "Enregistrer", "")

If Trim(Rep) = "" Then Exit Sub

Name App.Path & "\Fichiers\Budget.old" As App.Path & "\Fichiers\Budget.DATA"


End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

'-> Supprimer le handle de la feuille affect� au tableau
aCumul.HdlForm = 0

End Sub

Public Sub Init(cTypeAxe As Integer)

Dim Largeur As Long
Dim Hauteur As Long
Dim aLig As clsRow
Dim aCol As clsCol

'-> Indiquer le type de tableau
TypeAxe = cTypeAxe

'-> Donner son titre � la fen�tre
If TypeAxe = 1 Then
    '-> Issu de l'ae hierarchique
    Me.Caption = aCumul.aTb.LibAxe & " - " & aCumul.Key
Else
    '-> Issu de la consult d�tail
    Me.Caption = aCumul.aTb.LibDet & " - " & aCumul.Key
End If

'-> Retailler le tableau
For Each aLig In aCumul.aTb.Lignes
    If aLig.Visible Then Hauteur = Hauteur + aLig.HauteurTwips
Next
For Each aCol In aCumul.aTb.Colonnes
    If aCol.Visible Then Largeur = Largeur + aCol.LargeurTwips
Next

'-> THIERRY Faire un algo de  redimension de l'�cran
Me.Width = Largeur * 1.45
Me.Height = Hauteur * 1.14

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next

GetClientRect Me.hwnd, aRect

Me.MSFlexGrid1.Left = 0
Me.MSFlexGrid1.Top = 0
Me.MSFlexGrid1.Height = aRect.Bottom
Me.MSFlexGrid1.Width = aRect.Right

End Sub


Private Sub mnuDetail_Click()
strRetour = "DETAIL"
End Sub

Private Sub mnuPrint_Click()
strRetour = "PRINT"
End Sub

Private Sub MSFlexGrid1_DblClick()

Dim Rep As String

Rep = InputBox("Saisie d'une valeur", "Modification", Me.MSFlexGrid1.Text)
If Rep <> "" Then Me.MSFlexGrid1.Text = Rep

Me.Command1.Visible = True

End Sub

Private Sub MSFlexGrid1_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

'---> Afficher le menu

'-> R�cup�rer la ligne et la colonne
ClickX = GetCol(Me.MSFlexGrid1, X)
ClickY = GetRow(Me.MSFlexGrid1, y)

'-> Tester le buton
If Button <> vbRightButton Then Exit Sub

'-> Ne rien afficher sur les entetes
If ClickX = 0 Or ClickY = 0 Then
    Me.mnuDetail.Visible = False
Else
    Me.mnuDetail.Visible = True
End If

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher le menu
Me.PopupMenu Me.mnuPopup

'-> Selon le retour
Select Case strRetour
    Case "PRINT"
        '-> Envoyer l'impression
        PrintFlexGrid Me.MSFlexGrid1, GetIniFileValue("PRINTER", "PATH", App.Path & "\TurboAnalyt.ini"), "", Me.Picture1, Me.Caption, Now, False
    Case "DETAIL"
        DisplayDet
End Select

End Sub

Private Sub DisplayDet()

Dim aCol As clsCol
Dim aRow As clsRow

'-> Pointer sur la colonne
Set aCol = Me.aCumul.aTb.Colonnes("COL_" & Entry(ClickX, aCumul.aTb.MatriceCol, "|"))

'-> Pointer sur la ligne
Set aRow = Me.aCumul.aTb.Lignes("LIG_" & Entry(ClickY, aCumul.aTb.MatriceLig, "|"))

'-> Charger la feuille
Load frmDetail

'-> Initialiser
frmDetail.Init aCol, aRow, aCumul

'-> Afficher
frmDetail.Show vbModal

End Sub
