VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmResult 
   Caption         =   "R�sultat de la recherche"
   ClientHeight    =   6555
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9075
   LinkTopic       =   "Form1"
   ScaleHeight     =   437
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   605
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ListView ListView1 
      Height          =   1815
      Left            =   0
      TabIndex        =   1
      Top             =   360
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   3201
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7455
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "POPUP"
      Visible         =   0   'False
      Begin VB.Menu mnuPrint 
         Caption         =   "Imprimer"
      End
      Begin VB.Menu mnuGraph 
         Caption         =   "Graphique"
      End
   End
End
Attribute VB_Name = "frmResult"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ColX As Integer

Private Sub Combo1_Click()
SearchData

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next

GetClientRect Me.hwnd, aRect

Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom - Me.ListView1.Top
Me.Combo1.Width = aRect.Right

End Sub
Public Sub SearchData()

Dim hdlfile As Integer
Dim Ligne As String
Dim X As ListItem
Dim aCumul As clsCumul
Dim aCell As clsCell
Dim aRow As clsRow
Dim i As Integer

'-> Mettre le sablier
Screen.MousePointer = 11
Load frmTempo
TailleLue = 1
TailleTotale = 100

'-> Vider la listView
Me.ListView1.ListItems.Clear

'-> Ouvrir le fichier
hdlfile = FreeFile
Open App.Path & "\Search.txt" For Input As #hdlfile

'-> Lecture du fichier
Do While Not EOF(hdlfile)
    '-> Lecture de la ligne
    Line Input #hdlfile, Ligne
    '-> Tester le niveau
    If NumEntries(Ligne, "|") > frmSearch.Combo1.ListIndex + 1 Then
        '-> Ajouter dans le tableau au bon endroit
        Set X = frmResult.ListView1.ListItems.Add(, , "")
        '-> Ajouter dans le tableau
        If Entry(1, Ligne, "|") = "CUMUL" Then
            '-> Pointer sur le cumul
            For i = 2 To NumEntries(Ligne, "|")
                If i = 2 Then
                    Set aCumul = Axes("CUM_" & Trim(UCase$(Entry(i, Ligne, "|"))))
                Else
                    Set aCumul = aCumul.Cumuls("CUM_" & Trim(UCase$(Entry(i, Ligne, "|"))))
                End If
            Next
            
            '-> Ajouter au bon endroit
            Select Case NumEntries(Ligne, "|")
                Case 2
                    X.Text = Entry(NumEntries(Ligne, "|"), Ligne, "|")
                Case 3
                    X.SubItems(1) = Entry(NumEntries(Ligne, "|"), Ligne, "|")
                Case 4
                    X.SubItems(2) = Entry(NumEntries(Ligne, "|"), Ligne, "|")
                Case 5
                    X.SubItems(3) = Entry(NumEntries(Ligne, "|"), Ligne, "|")
            End Select
        Else
            '-> C'est une affaire, pointer sur cette affaire
            Set aCumul = Details("CUM_" & Trim(UCase$(Entry(2, Ligne, "|"))))
            '-> Ajouter dans le browse
            X.SubItems(4) = aCumul.Key
        End If ' si cmumul ou d�tail
        
        If aCumul.aTb Is Nothing Then
            '-> charger le tableau
            Set aCumul.aTb = LoadTb(TabFileName)
            '-> Cr�er le cumul
            CreateCumul aCumul
            '-> Ex�cuter les calculs
            CreateCalcul aCumul
        End If
        
        '-> Pointer sur la ligne
        Set aRow = aCumul.aTb.Lignes("LIG_" & Entry(Me.Combo1.ListIndex + 1, aCumul.aTb.MatriceLig, "|"))
            
        '-> Mise � jour des donn�s
        i = 5
        For j = 1 To aCumul.aTb.Colonnes.Count
            Set aCell = aRow.Cellules("CELL_" & UCase$(Trim(Entry(j, aCumul.aTb.MatriceCol, "|"))))
            If aCell.ValueCell <> "" Then
                If CDbl(aCell.ValueCell) <> 0 Then
                    X.SubItems(i) = aCell.ValueCell
                End If
            End If
            i = i + 1
        Next
    End If 'si on est sur le bon niveua de l'axe
Loop

'-> Formatter le browse
FormatListView frmResult.ListView1

Screen.MousePointer = 0

End Sub


Private Function GetColList(X As Integer) As Integer


Dim i As Long

For i = 1 To Me.ListView1.ColumnHeaders.Count
    If Me.ScaleX(X, 1, 3) >= Me.ListView1.ColumnHeaders(i).Left And Me.ScaleX(X, 1, 3) <= Me.ListView1.ColumnHeaders(i).Left + Me.ListView1.ColumnHeaders(i).Width Then
        GetColList = i
        Exit Function
    End If
Next

End Function

Public Sub Init(IdLigne As Integer, UseAxe As Integer)

Dim aRow As clsRow
Dim X As ListItem
Dim aCol As clsCol
Dim i As Integer

'-> Charger la liste des lignes
For Each aRow In aTb.Lignes
    Me.Combo1.AddItem aRow.Libelle
Next

'-> Si on utilise l'axe hierarchique cr�er les colonnes
If UseAxe <> 0 Then
    For i = UseAxe To aTb.NvAxeAnalyse
        Me.ListView1.ColumnHeaders.Add , , Entry(i, aTb.LibNvAxe, "|")
    Next
End If

'-> Ajouter les colonnes de l'axe de d�tail
For i = 1 To aTb.NvAxeDetail
    Me.ListView1.ColumnHeaders.Add , , Entry(i, aTb.LibNvDet, "|")
Next

'-> Formatter le browse
For Each aCol In aTb.Colonnes
    Me.ListView1.ColumnHeaders.Add , , aCol.Libelle
Next
FormatListView Me.ListView1

'-> Positionner la ligne dans la combo
Me.Combo1.ListIndex = IdLigne

End Sub



Private Sub ListView1_DblClick()




If ColX = 5 Then
    If Trim(Me.ListView1.SelectedItem.SubItems(4)) <> "" Then
        DisplayTB "ROOT|" & Me.ListView1.SelectedItem.SubItems(4), False
        Unload Me
    End If
End If



End Sub

Private Sub ListView1_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

ColX = GetColList(CLng(X))

If Button <> vbRightButton Then Exit Sub
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub



strRetour = ""
Me.PopupMenu Me.mnuPopup

Select Case strRetour

    Case "GRAPH"
        CreateGraph
    Case "PRINT"
    
End Select

End Sub

Private Sub CreateGraph()

'---> Cette proc�dure c�er un graphique

Dim X As ListItem
Dim Valeur(1 To 6) As Double
Dim OkToGo As Boolean
Dim i As Integer, j As Integer
Dim ListSeries As String


'-> R�cup�rer les valeurs
For Each X In Me.ListView1.ListItems
    If ColX = 1 Then
        If X.Text <> "" Then
            OkToGo = True
        Else
            OkToGo = False
        End If
    Else
        If X.SubItems(ColX - 1) <> "" Then
            OkToGo = True
        Else
            OkToGo = False
        End If
    End If
    If OkToGo Then
        If ListSeries = "" Then
            ListSeries = X.Index
        Else
            ListSeries = ListSeries & "|" & X.Index
        End If
    End If
Next

'-> Charger un graphique
Load frmChart
frmChart.MSChart1.ColumnCount = NumEntries(ListSeries, "|")
frmChart.MSChart1.RowCount = 6

'-> Titre des axes
For i = 1 To NumEntries(ListSeries, "|")
    '-> Pointer sur la valeur associ�e
    Set X = Me.ListView1.ListItems(CInt(Entry(i, ListSeries, "|")))
    frmChart.MSChart1.Column = i
    If ColX = 1 Then
        frmChart.MSChart1.ColumnLabel = X.Text
    Else
        frmChart.MSChart1.ColumnLabel = X.SubItems(ColX - 1)
    End If
    '-> Postionner les valeurs
    For j = 1 To 6
        frmChart.MSChart1.Row = j
        If X.SubItems(j + 4) <> "" Then
            frmChart.MSChart1.Data = X.SubItems(j + 4)
            
        End If
    Next
Next

'-> Titre des lignes
For i = 1 To 6
    frmChart.MSChart1.Row = i
    frmChart.MSChart1.RowLabel = Me.ListView1.ColumnHeaders(i + 5).Text
Next

frmChart.MSChart1.TitleText = frmChart.MSChart1.TitleText & " - " & Me.Combo1.Text
frmChart.Show vbModal

'-> Titre des colonnes



End Sub

Private Sub mnuGraph_Click()
strRetour = "GRAPH"
End Sub

Private Sub mnuPrint_Click()
strRetour = "PRINT"
End Sub
