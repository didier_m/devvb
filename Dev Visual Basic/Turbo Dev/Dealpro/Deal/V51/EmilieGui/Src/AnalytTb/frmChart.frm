VERSION 5.00
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmChart 
   Caption         =   "Form1"
   ClientHeight    =   8460
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9180
   LinkTopic       =   "Form1"
   ScaleHeight     =   564
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   612
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.CommandButton Command1 
      Height          =   375
      Index           =   5
      Left            =   600
      Picture         =   "frmChart.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   840
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Index           =   4
      Left            =   120
      Picture         =   "frmChart.frx":018A
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   840
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Index           =   3
      Left            =   600
      Picture         =   "frmChart.frx":0314
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   480
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Index           =   2
      Left            =   120
      Picture         =   "frmChart.frx":049E
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   480
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Index           =   1
      Left            =   600
      Picture         =   "frmChart.frx":0628
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   120
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Height          =   375
      Index           =   0
      Left            =   120
      Picture         =   "frmChart.frx":07B2
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   120
      Width           =   495
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   7455
      Left            =   0
      OleObjectBlob   =   "frmChart.frx":093C
      TabIndex        =   0
      Top             =   0
      Width           =   9015
   End
End
Attribute VB_Name = "frmChart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ListeGraph(0 To 5) As Integer

Private Sub Command1_Click(Index As Integer)

Me.MSChart1.chartType = ListeGraph(Index)
Me.MSChart1.Refresh

End Sub

Private Sub Form_Load()

ListeGraph(0) = VtChChartType2dBar
ListeGraph(1) = VtChChartType2dPie
ListeGraph(2) = VtChChartType3dArea
ListeGraph(3) = VtChChartType3dBar
ListeGraph(4) = VtChChartType3dCombination
ListeGraph(5) = VtChChartType3dStep

End Sub

Private Sub Form_Resize()

On Error Resume Next

Dim aRect As RECT

GetClientRect Me.hwnd, aRect

Me.MSChart1.Left = 0
Me.MSChart1.Top = 0
Me.MSChart1.Width = aRect.Right
Me.MSChart1.Height = aRect.Bottom

End Sub
