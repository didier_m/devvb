VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmDetail 
   Caption         =   "D�tail  : "
   ClientHeight    =   7320
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9975
   LinkTopic       =   "Form1"
   ScaleHeight     =   488
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   665
   StartUpPosition =   3  'Windows Default
   Begin RichTextLib.RichTextBox RichTextBox1 
      Height          =   2295
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   4048
      _Version        =   393217
      Enabled         =   -1  'True
      TextRTF         =   $"frmDetail.frx":0000
   End
   Begin MSComctlLib.ListView ListView2 
      Height          =   2295
      Left            =   0
      TabIndex        =   0
      Top             =   2280
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   4048
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
End
Attribute VB_Name = "frmDetail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next

'-> R�cup�rer les dimensions de la zone cliente de la feuille
GetClientRect Me.hwnd, aRect

'-> Positionner chaque objet
Me.ListView2.Left = 0
If Me.RichTextBox1.Visible = True Then
    Me.ListView2.Top = Me.RichTextBox1.Height
    Me.RichTextBox1.Left = 0
    Me.RichTextBox1.Width = aRect.Right
Else
    Me.ListView2.Top = 0
End If

'-> Positionner le listview
Me.ListView2.Height = aRect.Bottom - Me.ListView2.Top
Me.ListView2.Width = aRect.Right


End Sub

Public Sub Init(aCol As clsCol, aRow As clsRow, aCumul As clsCumul)

'---> Cette proc�dure initialise la justification du d�tail

Dim MyCol As clsCol
Dim aCell As clsCell
Dim x As ListItem
Dim Signe As String
Dim Ligne As String
Dim lpBuffer As String

Dim i As Integer
Dim j As Integer

On Error Resume Next

Me.Caption = "Colonne : " & aCol.Libelle & " - Ligne :  " & aRow.Libelle & " - " & aCumul.Key

'-> Selon le type de colonne
If aCol.TypeCol = 0 Then '-> Si on affiche une colonne de d�tail
    Me.RichTextBox1.Visible = False
    Me.WindowState = vbMaximized
    '-> Ouvrir le fichier sp�cifi�
    lpBuffer = GetIniFileValue("DATA", "", App.Path & "\Fichiers\detail.txt", True)
    Me.ListView2.ColumnHeaders.Add , , "Soci�t�"
    Me.ListView2.ColumnHeaders.Add , , "Etablissement"
    Me.ListView2.ColumnHeaders.Add , , "Exercice"
    Me.ListView2.ColumnHeaders.Add , , "P�riode"
    Me.ListView2.ColumnHeaders.Add , , "Journal"
    Me.ListView2.ColumnHeaders.Add , , "N� Pi�ce"
    Me.ListView2.ColumnHeaders.Add , , "Date de pi�ce"
    Me.ListView2.ColumnHeaders.Add , , "Compte"
    Me.ListView2.ColumnHeaders.Add , , "D�bit "
    Me.ListView2.ColumnHeaders.Add , , "Cr�dit"
    Me.ListView2.ColumnHeaders.Add , , "Affaire"
    Me.ListView2.ColumnHeaders.Add , , "Nature"
    '-> Ne lire que les 50 premi�re lignes
    For i = 1 To 50
        Ligne = Entry(i, lpBuffer, Chr(0))
        Set x = Me.ListView2.ListItems.Add(, , Trim(Entry(1, Ligne, "|")))
        For j = 2 To 14
            Select Case j
                Case 11, 12
                Case 13, 14
                    x.SubItems(j - 3) = Trim(Entry(j, Ligne, "|"))
                Case Else
                    x.SubItems(j - 1) = Trim(Entry(j, Ligne, "|"))
            End Select
        Next
        
    Next
    Me.ListView2.Font.Size = 10
    FormatListView Me.ListView2
Else '-> Affichage d'un colonne de calcul

    '-> Ajoutr 2 colonnes
    Me.ListView2.ColumnHeaders.Add , , "Colonne"
    Me.ListView2.ColumnHeaders.Add , , "Signe"
    Me.ListView2.ColumnHeaders.Add , , "Montant"

    '-> Afficher le RTF
    Me.RichTextBox1.Visible = True
    '-> Positionner la formule de calcul
    If aCol.Code = "REELTOT" Then
        Me.RichTextBox1.LoadFile App.Path & "\REELTOT.RTF"
                    
        '-> Colonne R�alis�
        Set aCell = aRow.Cellules("CELL_REAL")
        If aCell.ValueCell <> "" Then
            If CDbl(aCell.ValueCell) > 0 Then
                Signe = "+"
            Else
                Signe = "-"
            End If
            Set x = Me.ListView2.ListItems.Add(, , "R�alis� : ")
            x.SubItems(1) = Signe
            x.SubItems(2) = Format(Abs(aCell.ValueCell), "0.00")
        End If
        '-> colonne Livraisons
        Set aCell = aRow.Cellules("CELL_LIV")
        If aCell.ValueCell <> "" Then
            If CDbl(aCell.ValueCell) > 0 Then
                Signe = "+"
            Else
                Signe = "-"
            End If
            Set x = Me.ListView2.ListItems.Add(, , "Livraisons : ")
            x.SubItems(1) = Signe
            x.SubItems(2) = Format(Abs(aCell.ValueCell), "0.00")
        End If
        '-> Colonne Commandes
        Set aCell = aRow.Cellules("CELL_CDE")
        If aCell.ValueCell <> "" Then
            If CDbl(aCell.ValueCell) > 0 Then
                Signe = "+"
            Else
                Signe = "-"
            End If
            Set x = Me.ListView2.ListItems.Add(, , "Commandes : ")
            x.SubItems(1) = Signe
            x.SubItems(2) = Format(Abs(aCell.ValueCell), "0.00")
        End If
        
        Me.ListView2.ListItems.Add
        Set x = Me.ListView2.ListItems.Add(, , "R�alis� total : ")
        If CDbl(aRow.Cellules("CELL_REELTOT").ValueCell) < 0 Then
            x.SubItems(1) = "-"
        Else
            x.SubItems(1) = "+"
        End If
        x.SubItems(2) = Format(Abs(aRow.Cellules("CELL_REELTOT").ValueCell), "0.00")
        Me.ListView2.ColumnHeaders(2).Alignment = lvwColumnRight
        Me.ListView2.ColumnHeaders(3).Alignment = lvwColumnRight

        FormatListView Me.ListView2
    End If
End If


'-> Lancer un resize
Form_Resize

End Sub

Private Sub ListView2_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

'-> Trier sur la colonne
Me.ListView2.SortKey = ColumnHeader.Index - 1
If Me.ListView2.SortOrder = lvwAscending Then
    Me.ListView2.SortOrder = lvwDescending
Else
    Me.ListView2.SortOrder = lvwAscending
End If
Me.ListView2.Sorted = True

End Sub

