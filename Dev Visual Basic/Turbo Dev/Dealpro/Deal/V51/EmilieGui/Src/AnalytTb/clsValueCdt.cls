VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsValueCdt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Operation As Integer
Public Operande As Integer '0 -> ET, 1 -> OU
Public ValMini As String
Public ValMaxi As String

Private Sub Class_Initialize()

    '-> Initialisation des propriétés de l'objet
    Operation = 1

End Sub
