VERSION 5.00
Begin VB.Form frmMenu 
   Caption         =   "Form1"
   ClientHeight    =   300
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   4935
   LinkTopic       =   "Form1"
   ScaleHeight     =   300
   ScaleWidth      =   4935
   Begin VB.Menu mnuCell 
      Caption         =   "CELLULE"
      Begin VB.Menu mnuEditCell 
         Caption         =   "Format de la cellule"
      End
   End
   Begin VB.Menu mnuTB 
      Caption         =   "TB"
      Begin VB.Menu mnuEdit 
         Caption         =   "Editer"
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDel 
         Caption         =   "Supprimer"
      End
   End
   Begin VB.Menu mnuDescroProgi 
      Caption         =   "DESCRO"
      Begin VB.Menu mnuAddEntry 
         Caption         =   "Ajouter"
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelEntru 
         Caption         =   "Supprimer"
      End
   End
   Begin VB.Menu mnuProgiciel 
      Caption         =   "PROGICIEL"
      Begin VB.Menu mnuDescro 
         Caption         =   "Sélectionner dans la liste des progiciels"
      End
      Begin VB.Menu mnusep4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOutDescro 
         Caption         =   "Supprimer de la liste des progiciels"
      End
   End
End
Attribute VB_Name = "frmMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub mnuAddEntry_Click()
strRetour = "ADD"
End Sub

Private Sub mnuDel_Click()
strRetour = "DEL"
End Sub

Private Sub mnuDup_Click()
strRetour = "DUP"
End Sub

Private Sub mnuDelEntru_Click()
strRetour = "DEL"
End Sub

Private Sub mnuDescro_Click()
strRetour = "ADD"
End Sub

Private Sub mnuEdit_Click()
strRetour = "EDIT"
End Sub

Private Sub mnuEditCell_Click()
strRetour = "FORMAT"
End Sub

Private Sub mnuMove_Click()
strRetour = "MOVE"
End Sub

Private Sub mnuOutDescro_Click()
strRetour = "OUT"
End Sub
