VERSION 5.00
Begin VB.Form frmAccueil 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4245
   ClientLeft      =   225
   ClientTop       =   1380
   ClientWidth     =   5670
   ControlBox      =   0   'False
   Icon            =   "TM-frmAccueil.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4245
   ScaleWidth      =   5670
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   4050
      Left            =   150
      TabIndex        =   0
      Top             =   60
      Width           =   5385
      Begin VB.Frame Frame2 
         Caption         =   "Choix de la langue"
         Height          =   1335
         Left            =   240
         TabIndex        =   4
         Top             =   2520
         Width           =   3255
         Begin VB.Image Image3 
            Height          =   480
            Index           =   3
            Left            =   840
            Picture         =   "TM-frmAccueil.frx":000C
            ToolTipText     =   "Portugais"
            Top             =   720
            Width           =   480
         End
         Begin VB.Image Image3 
            Height          =   480
            Index           =   2
            Left            =   840
            Picture         =   "TM-frmAccueil.frx":044E
            ToolTipText     =   "Allemand"
            Top             =   240
            Width           =   480
         End
         Begin VB.Image Image3 
            Height          =   480
            Index           =   1
            Left            =   120
            Picture         =   "TM-frmAccueil.frx":0890
            ToolTipText     =   "Anglais"
            Top             =   720
            Width           =   480
         End
         Begin VB.Image Image3 
            Height          =   480
            Index           =   0
            Left            =   120
            Picture         =   "TM-frmAccueil.frx":0CD2
            ToolTipText     =   "Fran�ais"
            Top             =   240
            Width           =   480
         End
      End
      Begin VB.Image Image4 
         Height          =   240
         Left            =   5040
         Picture         =   "TM-frmAccueil.frx":1114
         ToolTipText     =   "Quitter"
         Top             =   120
         Width           =   240
      End
      Begin VB.Image Image2 
         Height          =   1920
         Left            =   3720
         Picture         =   "TM-frmAccueil.frx":149E
         Top             =   1800
         Width           =   1440
      End
      Begin VB.Label Label1 
         Caption         =   "Cr�ation, Edition et visualisation de maquette d'impression au format Deal Informatique"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   855
         Left            =   240
         TabIndex        =   3
         Top             =   1680
         Width           =   3015
      End
      Begin VB.Label Label4 
         Caption         =   "Deal Informatique"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1680
         TabIndex        =   2
         Top             =   240
         Width           =   3255
      End
      Begin VB.Label Label5 
         Caption         =   "Editeur de maquette"
         Height          =   255
         Left            =   1680
         TabIndex        =   1
         Top             =   840
         Width           =   2655
      End
      Begin VB.Image Image1 
         BorderStyle     =   1  'Fixed Single
         Height          =   1350
         Left            =   240
         Picture         =   "TM-frmAccueil.frx":A4E0
         Top             =   240
         Width           =   1350
      End
   End
End
Attribute VB_Name = "frmAccueil"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then Image4_Click
    If KeyAscii = 70 Or KeyAscii = 102 Then Image3_Click 0
End Sub

Private Sub Form_Load()
    ClignOk = False
End Sub

Private Sub Image3_Click(Index As Integer)

On Error GoTo GestError

Dim Pos As Integer
Dim Res As Long
Dim lpbuffer As String
Dim Result As Long
Dim aLb As Libelle



'-> Construire les paths selon le mode en cours
RessourcePath = GetPath(App.Path)
Me.MousePointer = 11

'-> Gestion du fichier des langues de turbo-graph

'-> Messprog de trubo graph
If Not CreateMessProg("TurboMaq-0" & Index + 1 & ".lng") Then GoTo GestError

'-> Messprog de feuilles communes
If Not CreateMessProg("Common-0" & Index + 1 & ".lng") Then GoTo GestError

'-> Ajout de la font Video Terminal pour lecture des maquettes
Result = AddFontResource(RessourcePath & "vtsr____.ttf")

'-> Setting du code langue
CodeLangue = "0" & Index + 1

'-> Chargement des s�parateurs de chiffres
lpbuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, lpbuffer, Len(lpbuffer))
SepDec = Mid$(lpbuffer, 1, Res - 1)
lpbuffer = Space$(10)
Res = GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, lpbuffer, Len(lpbuffer))
SepMil = Mid$(lpbuffer, 1, Res - 1)

''---> V�rification du format du s�parateur d�cimal
'lpBuffer = Space$(10)
'Result = GetLocaleInfo(LOCALE_USER_DEFAULT, 14, lpBuffer, Len(lpBuffer))
'lpBuffer = Mid$(lpBuffer, 1, Result - 1)
'If lpBuffer <> "." Then
'    Set aLb = Libelles("MESSAGE")
'    MsgBox aLb.GetCaption(8), vbCritical + vbOKOnly, aLb.GetToolTip(9)
'    End
'End If

'-> Redonner son aspect au pointeur de souris
Me.MousePointer = 0

'-> Indiquer que le chargement est termin�
IsLoading = False

'-> D�charger la feuille
Unload Me

Set aLb = Nothing

Exit Sub

GestError:

    MsgBox "Erreur de chargement dans la page d'accueil", vbCritical, "Erreur"
    End
    
End Sub

Private Sub Image4_Click()

Dim Res

Res = MsgBox("Quitter ?", vbQuestion + vbYesNo, "Quitter")
If Res = vbYes Then
    Unload Me
    End
End If


End Sub
