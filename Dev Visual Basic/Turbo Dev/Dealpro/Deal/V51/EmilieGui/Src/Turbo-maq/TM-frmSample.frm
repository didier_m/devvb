VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmSample 
   BackColor       =   &H00E0E0E0&
   ClientHeight    =   5580
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8370
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   372
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   558
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox List1 
      Height          =   1425
      Left            =   6480
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   720
      Visible         =   0   'False
      Width           =   975
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   5640
      Top             =   600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   14
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":0000
            Key             =   "Fonction"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":031A
            Key             =   "DocVide"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":0FF4
            Key             =   "Erp"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":1446
            Key             =   "Doc"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":1898
            Key             =   "App"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":2572
            Key             =   "New"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":2E4C
            Key             =   "Perso"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":3726
            Key             =   "DocNull"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":4400
            Key             =   "Existant"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":4852
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":4B6C
            Key             =   "Modele"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":5446
            Key             =   "Mod"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":5760
            Key             =   "Exit"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmSample.frx":643A
            Key             =   "NewClient"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeSamples 
      Height          =   5175
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   9128
      _Version        =   393217
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      BorderStyle     =   1
      Appearance      =   1
   End
End
Attribute VB_Name = "frmSample"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim LastFind As String

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)


Select Case KeyCode
       
    Case 13
        TreeSamples_DblClick
    Case 114
        
        


End Select



End Sub



Private Sub Form_KeyPress(KeyAscii As Integer)

Dim aNode As Node
Dim aLb As Libelle
Dim StrToFind

If KeyAscii <> 6 Then Exit Sub

Set aLb = Libelles("FRMFIELDS")
StrToFind = InputBox(aLb.GetCaption(8), aLb.GetToolTip(8), LastFind)

'-> Affecteur une recherche dans les nodes
For Each aNode In Me.TreeSamples.Nodes
    If aNode.Index > Me.TreeSamples.SelectedItem.Index Then
        If InStr(1, UCase$(aNode.Text), UCase$(StrToFind)) Then
            aNode.Selected = True
            aNode.EnsureVisible
            Me.TreeSamples.SetFocus
            LastFind = StrToFind
            Exit Sub
        End If
    End If
Next


End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

Me.TreeSamples.ToolTipText = ""

End Sub

Private Sub Form_Load()

Dim aLb As Libelle

Set aLb = Libelles("FRMSAMPLE")

Me.Caption = aLb.GetCaption(7)

Set aLb = Nothing


End Sub

Private Sub Form_Resize()


On Error Resume Next

Dim Rect1 As Rect
Dim Res As Long

Res = GetClientRect(Me.hwnd, Rect1)

Me.TreeSamples.Left = 0
Me.TreeSamples.Top = 0
Me.TreeSamples.Width = Rect1.Right - Rect1.Left
Me.TreeSamples.Height = Rect1.Bottom - Rect1.Top


End Sub

Private Sub TreeSamples_DblClick()

Dim aNode As Node
Dim aMaq As New Maquette
Dim aLb As Libelle
Dim NomFichier As String
Dim Rep
Dim NomMaq As String

If Me.TreeSamples.Nodes.Count = 0 Then Exit Sub

NomMaq = UCase$(Me.TreeSamples.SelectedItem.Key)

Select Case Entry(1, NomMaq, "|")

    Case "MOD"
        '-> Vider les variables pour test retour
        Progiciel = ""
        Rubrique = ""
        '-> Afficher la feuille de choix du progiciel
        frmChoixApp.Show vbModal
        '-> Tester si le retour a �t� appliquer
        If Rubrique <> "" And Progiciel <> "" Then
            '-> Ouvrir le mod�le
            If Mid$(Entry(2, NomMaq, "|"), 1, 5) = "BLANK" Then
                '-> Fen�tre d'ouverture
                frmNewMaq.Show vbModal
            Else
                Unload Me
                OpenMaq Entry(2, NomMaq, "|")
                Set aLb = Libelles("FRMNEWMAQ")
                Maquettes("MAQUETTE").Nom = aLb.GetCaption(3)
                frmNaviga.TreeNaviga.Nodes("MAQUETTE").Text = aLb.GetCaption(3)
            End If
        End If
        
    Case "MAQ"
        '-> R�cup�ration du nom du progiciel ainsi que celui de la rubrique associ�e
        Progiciel = Entry(2, Me.TreeSamples.SelectedItem.Parent.Key, "|")
        Rubrique = Entry(3, Me.TreeSamples.SelectedItem.Parent.Key, "|")
        '-> Charger la maquette
        Unload Me
        OpenMaq Entry(2, NomMaq, "|")
                
    Case "RETOUR"
    
        Unload Me
        frmListeClient.Show vbModal
                
    Case "END"
    
        '-> Demander la confirmation
            Set aLb = Libelles("FRMSAMPLE")
             Rep = MsgBox(aLb.GetCaption(9), vbQuestion + vbYesNo, aLb.GetToolTip(9))
            If Rep = vbYes Then
                End
            Else
                Exit Sub
            End If

End Select
  
'-> Liberer les pointeurs
Set aLb = Nothing
Set aMaq = Nothing
Set aNode = Nothing



End Sub

Private Sub TreeSamples_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)

If Shift Then MsgBox Me.TreeSamples.SelectedItem.Tag
    
    

End Sub
