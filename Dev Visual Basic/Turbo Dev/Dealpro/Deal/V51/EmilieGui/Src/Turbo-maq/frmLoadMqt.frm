VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmLoadMqt 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   3600
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4230
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3600
   ScaleWidth      =   4230
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   2040
      Top             =   720
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.FileListBox File1 
      Height          =   3600
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4215
   End
End
Attribute VB_Name = "frmLoadMqt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub File1_DblClick()
    
If Me.File1.ListCount > 0 Then RetourMqt = Me.File1.Path & "|" & Me.File1.FileName
Unload Me
End Sub

Private Sub File1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    
On Error GoTo EndError
    
Dim i As Integer

Dim PathName As String
Dim FileName As String
    
If Shift Then MsgBox Me.File1.Path
    
If Button = vbRightButton And Shift = 1 Then

    '-> Afficher la boite de s�lection d'un fichier
    Me.CommonDialog1.CancelError = True
    Me.CommonDialog1.ShowOpen
    
    i = InStrRev(Me.CommonDialog1.FileName, "\")
    PathName = Mid$(Me.CommonDialog1.FileName, 1, i - 1)
    FileName = Entry(NumEntries(Me.CommonDialog1.FileName, "\"), Me.CommonDialog1.FileName, "\")
    
    '-> Retourner la valeur saisie
    RetourMqt = PathName & "|" & FileName
    Unload Me
    
End If
    
    
Exit Sub

EndError:
    
    
End Sub

Private Sub Form_Load()

On Error GoTo GestError

Dim aLb As Libelle
Dim aApp As Application

'-> Pointer sur la bone classe
Set aLb = Libelles("FRMLOADMQT")
Me.Caption = aLb.GetCaption(1)
Me.File1.ToolTipText = aLb.GetCaption(2)
Set aLb = Nothing

'-> Faire le setting du propath
Set aApp = Apps(UCase$(CurrentUser))
Me.File1.Path = Mid$(aApp.PathMqt, 1, Len(aApp.PathMqt) - 1)
Me.File1.Pattern = "*.maq"
Set aApp = Nothing

If Me.File1.ListCount > 0 Then Me.File1.Selected(0) = True
    
Exit Sub

GestError:

    Me.File1.Enabled = False
    
End Sub
