VERSION 5.00
Begin VB.Form frmSave 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   3030
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5835
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3030
   ScaleWidth      =   5835
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   2640
      Width           =   3495
   End
   Begin VB.PictureBox SaveBt 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   5160
      Picture         =   "TM-fmrSave.frx":0000
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   4
      Top             =   2520
      Width           =   630
   End
   Begin VB.ListBox List1 
      Height          =   1620
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   5655
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   2640
      TabIndex        =   1
      Text            =   "Text1"
      Top             =   120
      Width           =   3135
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   3720
      Top             =   2520
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   4440
      Top             =   2520
      Width           =   630
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Label2"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   5055
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2175
   End
End
Attribute VB_Name = "frmSave"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim aApp As Application

Set aLb = Libelles("BOUTONS")

Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)
Me.SaveBt.ToolTipText = aLb.GetCaption(4)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.SaveBt.Picture = Turbo_Maq.Ok.Picture


Set aLb = Nothing

Set aLb = Libelles("FRMSAVE")
Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Label2.Caption = aLb.GetCaption(3)
Me.Check1.Caption = aLb.GetCaption(4)

Set aLb = Nothing

Me.Text1.Text = Maquettes("MAQUETTE").Nom

Set aApp = Apps(UCase$(CurrentUser))
If aApp.IsApp Then
    '-> Ajouter le path li� au progiciel
    Set aApp = Apps(Progiciel)
    Me.List1.AddItem aApp.PathApp
    '-> S�lectionner le node
    Me.List1.Selected(Me.List1.ListCount - 1) = True
End If
'-> N'ajouter que le path du client
If UCase$(CurrentUser) <> Progiciel Then
    '-> Ajouter le path
    Set aApp = Apps(UCase$(CurrentUser))
    Me.List1.AddItem aApp.PathApp
    Me.List1.Selected(Me.List1.ListCount - 1) = True
End If

'-> Lib�rer le pointeur
Set aApp = Nothing


End Sub

Private Sub SaveBt_Click()

'---> Effectuer l'enregistrement

Dim aLb As Libelle
Dim NomFile As String
Dim Rep

'-> Pointer sur les libelles
Set aLb = Libelles("FRMSAVE")

'-> V�rifier que le nom du fichier est renseign�
If Trim(Me.Text1.Text) = "" Then
    MsgBox aLb.GetCaption(5), vbCritical + vbOKOnly, aLb.GetToolTip(5)
    Me.Text1.SetFocus
    Exit Sub
End If

'-> V�rifier la r�gularit� du nom du fichier
If Not IsLegalName(Me.Text1.Text, False) Then
    MsgBox aLb.GetCaption(7), vbCritical + vbOKOnly, aLb.GetToolTip(7)
    Me.Text1.SetFocus
    Exit Sub
End If

'-> Construire le fichier
NomFile = Me.List1.Text & Me.Text1.Text

If Me.Check1.Value = 1 Then
    If UCase$(Mid$(NomFile, Len(NomFile) - 3, 4)) <> ".DRM" Then
        NomFile = NomFile & ".Drm"
    End If
Else
    '-> V�rifier si on trouve maqgui � la fin
    If UCase$(Mid$(NomFile, Len(NomFile) - 6, 7)) <> ".MAQGUI" Then
        NomFile = NomFile & ".Maqgui"
    End If
End If

'-> V�rifier que le fichier n'existe pas
If Dir$(NomFile) <> "" Then
    Rep = MsgBox(aLb.GetCaption(6), vbQuestion + vbYesNo, aLb.GetCaption(6))
    If Rep = vbNo Then Exit Sub
End If

'-> Lancer l'enregistrement
Save LCase$(NomFile)

'-> Changer le nom de la maquette
frmNaviga.TreeNaviga.Nodes("MAQUETTE").Text = Me.Text1.Text

'-> D�charger la feuille
Unload Me


End Sub

Private Sub Text1_GotFocus()
    Me.Text1.SelStart = 0
    Me.Text1.SelLength = 500
End Sub
