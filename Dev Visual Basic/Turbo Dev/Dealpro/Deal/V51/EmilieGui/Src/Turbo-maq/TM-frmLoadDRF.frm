VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmLoadDRF 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   6525
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   10815
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6525
   ScaleWidth      =   10815
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   1920
      Top             =   2640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmLoadDRF.frx":0000
            Key             =   "NonSelect"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmLoadDRF.frx":0CDA
            Key             =   "Select"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmLoadDRF.frx":0FF4
            Key             =   "DelLien"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmLoadDRF.frx":1CCE
            Key             =   "App"
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox RichTextBox1 
      Height          =   6495
      Left            =   4680
      TabIndex        =   1
      Top             =   0
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   11456
      _Version        =   393217
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"TM-frmLoadDRF.frx":29A8
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   6495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   11456
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
End
Attribute VB_Name = "frmLoadDRF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()

'On Error Resume Next

Dim aLb As Libelle
Dim aNode As Node
Dim i As Integer
Dim NodeParent As String
Dim aApp As Application
Dim ListePro As String, NomFichier As String

'-> Pointer sur le progiciel
Set aApp = Apps(UCase$(Progiciel))

'-> Ajouter l'icone du progiciel
Me.TreeView1.Nodes.Add , 4, "APP|" & Progiciel, Progiciel & " - " & aApp.NameApp, "App", "App"

'-> Faire la liste des fichiers DRF
NomFichier = Dir(aApp.PathApp & "*.drf")    ' Extrait la premi�re entr�e.
Do While NomFichier <> ""
    Set aNode = Me.TreeView1.Nodes.Add("APP|" & Progiciel, 4, "DRF|" & aApp.PathApp & NomFichier, NomFichier, "NonSelect", "Select")
    NomFichier = Dir
Loop

'-> Gestion du multilangue
Set aLb = Libelles("FRMLOADDRF")
Me.Caption = aLb.GetCaption(1)
Me.TreeView1.ToolTipText = aLb.GetCaption(2)
Me.RichTextBox1.ToolTipText = aLb.GetCaption(4)

'-> Ajouter le node de suppression
Set aNode = Me.TreeView1.Nodes.Add(, , "DRF|Suppr", aLb.GetCaption(3), "DelLien", "DelLien")

Set aLb = Nothing

End Sub

Private Sub TreeView1_Click()

On Error Resume Next

If Entry(1, Me.TreeView1.SelectedItem.Key, "|") <> "DRF" Then Exit Sub

If Me.TreeView1.SelectedItem.Key = "DRF-Suppr" Then
    Me.RichTextBox1.TextRTF = ""
    Exit Sub
End If

'-> afficher dans le Controle Rtf le fichier reessource
Me.RichTextBox1.LoadFile Entry(2, Me.TreeView1.SelectedItem.Key, "|")


End Sub

Private Sub TreeView1_DblClick()

On Error Resume Next

Dim FileKey

'-> Tester si le fichier charg� n'est pas d�ja celui qui est ouvert
If Me.TreeView1.SelectedItem.Key = Maquettes("MAQUETTE").FieldListe Then Exit Sub

'-> Tester si le click se fait sur une node fichier
If Entry(1, Me.TreeView1.SelectedItem.Key, "|") <> "DRF" Then Exit Sub

'-> R�cup�rer le fichier et son path
FileKey = Entry(2, Me.TreeView1.SelectedItem.Key, "|")

'-> Vider le treeview
frmFields.TreeFields.Nodes.Clear

If FileKey = "Suppr" Then
    Maquettes("MAQUETTE").FieldListe = ""
    Unload Me
    Exit Sub
End If
    
'-> Charger le treeview
ChargeDRF FileKey

'-> Supprimer le fichier tempo s'il existe encore
If Dir(TempFileName) <> "" Then Kill TempFileName

'-> Appliquer � la maquette
Maquettes("MAQUETTE").FieldListe = Entry(NumEntries(FileKey, "\"), FileKey, "\")

'-> Quitter
Unload Me

End Sub
