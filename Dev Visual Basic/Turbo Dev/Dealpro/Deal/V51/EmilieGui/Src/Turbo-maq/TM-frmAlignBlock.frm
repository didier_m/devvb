VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAlignBlock 
   Caption         =   "Form1"
   ClientHeight    =   3075
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6435
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3075
   ScaleWidth      =   6435
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "Frame1"
      Height          =   2295
      Left            =   3240
      TabIndex        =   1
      Top             =   120
      Width           =   3135
      Begin VB.OptionButton Option2 
         Caption         =   "Option2"
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   11
         Top             =   1800
         Width           =   1935
      End
      Begin VB.TextBox SpeTop 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2400
         TabIndex        =   10
         Top             =   1800
         Width           =   495
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Option2"
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   9
         Top             =   1440
         Width           =   1935
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Option2"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   8
         Top             =   1080
         Width           =   1935
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Option2"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   7
         Top             =   720
         Width           =   1935
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Option2"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Width           =   1935
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   2295
      Left            =   50
      TabIndex        =   0
      Top             =   120
      Width           =   3135
      Begin VB.OptionButton Option1 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   195
         Index           =   4
         Left            =   240
         TabIndex        =   13
         Top             =   360
         Width           =   2295
      End
      Begin VB.TextBox SpeLeft 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2400
         TabIndex        =   12
         Top             =   1800
         Width           =   495
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Option1"
         Height          =   195
         Index           =   3
         Left            =   240
         TabIndex        =   5
         Top             =   1800
         Width           =   2295
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Option1"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   4
         Top             =   1440
         Width           =   2295
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Option1"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   3
         Top             =   1080
         Width           =   2295
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Option1"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   2
         Top             =   720
         Width           =   2295
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   2640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlignBlock.frx":0000
            Key             =   "Block"
         EndProperty
      EndProperty
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   5040
      Top             =   2520
      Width           =   630
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   4320
      Top             =   2520
      Width           =   630
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   5760
      Top             =   2520
      Width           =   630
   End
End
Attribute VB_Name = "frmAlignBlock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public aBlock As Block

Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Form_Load()

Dim aLb As Libelle

Set aLb = Libelles("FRMALIGNBLOCK")

Me.Caption = aLb.GetCaption(1)
Me.Frame1.Caption = aLb.GetCaption(3)
Me.Frame2.Caption = aLb.GetCaption(2)

Set aLb = Libelles("FRMPROP")


Me.Option1(0).Caption = aLb.GetCaption(17)
Me.Option2(0).Caption = aLb.GetCaption(14)
Me.Option1(1).Caption = aLb.GetCaption(19)
Me.Option2(1).Caption = aLb.GetCaption(15)
Me.Option1(2).Caption = aLb.GetCaption(18)
Me.Option2(2).Caption = aLb.GetCaption(19)
Me.Option1(3).Caption = aLb.GetCaption(20)
Me.Option2(3).Caption = aLb.GetCaption(16)
Me.Option2(4).Caption = aLb.GetCaption(20)
Me.Option1(4).Caption = aLb.GetCaption(14) 'N 'est pas utilis� juste pour le design

Set aLb = Libelles("BOUTONS")
Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)
Me.Ok.ToolTipText = aLb.GetCaption(3)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ok.Picture = Turbo_Maq.Ok.Picture


Set aLb = Nothing

'---> charger les propri�t�s d'alignement d'un block
Me.Option1(aBlock.AlignementLeft - 2).Value = True
Me.SpeLeft.Text = aBlock.Left

Me.Option2(aBlock.AlignementTop - 1).Value = True
Me.SpeTop.Text = aBlock.Top

End Sub

Private Sub Ok_Click()

Dim aLb As Libelle

'-> Alignement Left
If Me.Option1(3).Value = True Then
    '-> V�rifier la valeur saisie
    If Not IsNumeric(SpeLeft.Text) Then
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(3), vbOKOnly + vbCritical, aLb.GetCaption(3)
        Me.SpeLeft.SetFocus
        Set aLb = Nothing
        Exit Sub
    End If
    aBlock.Left = CSng(Me.SpeLeft)
Else
    aBlock.Left = 0
End If

If Me.Option1(0).Value = True Then aBlock.AlignementLeft = 2
If Me.Option1(1).Value = True Then aBlock.AlignementLeft = 3
If Me.Option1(2).Value = True Then aBlock.AlignementLeft = 4
If Me.Option1(3).Value = True Then aBlock.AlignementLeft = 5

'-> Alignement Top
If Me.Option2(4).Value = True Then
    '-> V�rifier la valeur saisie
    If Not IsNumeric(SpeTop.Text) Then
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(3), vbOKOnly + vbCritical, aLb.GetCaption(3)
        Me.SpeTop.SetFocus
        Set aLb = Nothing
        Exit Sub
    End If
    aBlock.Top = CSng(Me.SpeTop)
Else
    aBlock.Top = 0
End If

If Me.Option2(0).Value = True Then aBlock.AlignementTop = 1
If Me.Option2(1).Value = True Then aBlock.AlignementTop = 2
If Me.Option2(2).Value = True Then aBlock.AlignementTop = 3
If Me.Option2(3).Value = True Then aBlock.AlignementTop = 4
If Me.Option2(4).Value = True Then aBlock.AlignementTop = 5

'-> Quitter
Unload Me

Set aLb = Nothing

End Sub

Private Sub Option1_Click(Index As Integer)

On Error Resume Next

If Index = 3 Then 'Sp�cifi�
    Me.SpeLeft.Enabled = True
    Me.SpeLeft.SetFocus
Else
    Me.SpeLeft.Enabled = False
End If

End Sub

Private Sub Option2_Click(Index As Integer)

On Error Resume Next

If Index = 4 Then 'Sp�cifi�
    Me.SpeTop.Enabled = True
    Me.SpeTop.SetFocus
Else
    Me.SpeTop.Enabled = False
End If


End Sub

Private Sub SpeLeft_GotFocus()
    Me.SpeLeft.SelStart = 0
    Me.SpeLeft.SelLength = 65535
End Sub

Private Sub SpeTop_GotFocus()
    Me.SpeTop.SelStart = 0
    Me.SpeTop.SelLength = 65535
End Sub
