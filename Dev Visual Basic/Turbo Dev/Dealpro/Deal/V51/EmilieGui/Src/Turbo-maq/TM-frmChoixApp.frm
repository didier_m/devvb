VERSION 5.00
Begin VB.Form frmChoixApp 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   4275
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4440
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4275
   ScaleWidth      =   4440
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox List2 
      Height          =   1815
      Left            =   120
      TabIndex        =   2
      Top             =   1800
      Width           =   4215
   End
   Begin VB.ListBox List1 
      Height          =   840
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   4215
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   2280
      Top             =   3720
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   3000
      Top             =   3720
      Width           =   630
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   3720
      Top             =   3720
      Width           =   630
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Label2"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   1440
      Width           =   3975
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3855
   End
End
Attribute VB_Name = "frmChoixApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Form_Load()

Dim aLb As Libelle
Dim aApp As Application
Dim i As Integer

Set aLb = Libelles("FRMCHOIXAPP")

Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Label2.Caption = aLb.GetCaption(3)

Set aLb = Libelles("BOUTONS")

Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)
Me.Ok.ToolTipText = aLb.GetCaption(3)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ok.Picture = Turbo_Maq.Ok.Picture


Set aLb = Nothing

'-> Pointer sur le client pour afficher la liste des progiciels autorisés
Set aApp = Apps(UCase$(CurrentUser))

'-> Ajouter les losgiciels
For i = 1 To NumEntries(aApp.Progiciel, ",")
    Me.List1.AddItem Entry(i, aApp.Progiciel, ",")
Next

Set aApp = Nothing

'-> Sélectionner le progiciel si renseigné
If Progiciel <> "" Then
    Me.List1.Text = Progiciel
    '-> Rechercher la rubrique
    Set aApp = Apps(UCase$(Progiciel))
    For i = 1 To aApp.nRubriques
        If aApp.GetRubriquesCode(i) = Rubrique Then
            Me.List2.Text = aApp.GetRubriquesName(i)
            Exit For
        End If
    Next
Else
    '-> Sélectionner le premier
    Me.List1.Selected(0) = True
End If

End Sub

Private Sub List1_Click()

'---> Afficher la liste des rubriques propres au logiciel en cours

Dim aApp As Application

'-> Pointer sur le logiciel
Set aApp = Apps(UCase$(Me.List1.Text))

'-> Vider la liste
Me.List2.Clear

For i = 1 To aApp.nRubriques
    '-> Ajout du  libelle de la rubrique
    Me.List2.AddItem aApp.GetRubriquesName(i)
Next

'-> Sélectionner la première rubrique
Me.List2.Selected(0) = True

'-> Liberer le pointeur
Set aApp = Nothing


End Sub

Private Sub List1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then List2_DblClick
End Sub

Private Sub List2_DblClick()
    Ok_Click
End Sub

Private Sub List2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then Ok_Click
End Sub

Private Sub Ok_Click()

'---> Faire le setting des variables progiciel et rubrique
Dim aApp As Application
Progiciel = UCase$(Me.List1.Text)

Set aApp = Apps(Progiciel)
Rubrique = aApp.GetRubriquesCode(Me.List2.ListIndex + 1)

Set aApp = Nothing

'-> Quitter
Unload Me

End Sub
