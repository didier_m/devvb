VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmProp 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   3465
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6795
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3465
   ScaleWidth      =   6795
   ShowInTaskbar   =   0   'False
   Begin VB.ListBox ListProp 
      Height          =   1035
      Left            =   3480
      TabIndex        =   1
      Top             =   960
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSFlexGridLib.MSFlexGrid Prop 
      Height          =   3495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6825
      _ExtentX        =   12039
      _ExtentY        =   6165
      _Version        =   393216
      Rows            =   10
      BackColor       =   16777215
      BackColorFixed  =   12632256
      BackColorSel    =   16777215
      BackColorBkg    =   14737632
      AllowBigSelection=   0   'False
      HighLight       =   0
      GridLines       =   3
      ScrollBars      =   2
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   12
      ImageHeight     =   12
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   40
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":0000
            Key             =   "C1"
            Object.Tag             =   "1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":0202
            Key             =   "C53"
            Object.Tag             =   "53"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":0404
            Key             =   "C52"
            Object.Tag             =   "52"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":0606
            Key             =   "C51"
            Object.Tag             =   "51"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":0808
            Key             =   "C49"
            Object.Tag             =   "49"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":0A0A
            Key             =   "C11"
            Object.Tag             =   "11"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":0C0C
            Key             =   "C55"
            Object.Tag             =   "55"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":0E0E
            Key             =   "C56"
            Object.Tag             =   "56"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":1010
            Key             =   "C9"
            Object.Tag             =   "9"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":1212
            Key             =   "C46"
            Object.Tag             =   "46"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":1414
            Key             =   "C12"
            Object.Tag             =   "12"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":1616
            Key             =   "C10"
            Object.Tag             =   "10"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":1818
            Key             =   "C14"
            Object.Tag             =   "14"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":1A1A
            Key             =   "C5"
            Object.Tag             =   "5"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":1C1C
            Key             =   "C47"
            Object.Tag             =   "47"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":1E1E
            Key             =   "C16"
            Object.Tag             =   "16"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":2020
            Key             =   "C3"
            Object.Tag             =   "3"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":2222
            Key             =   "C45"
            Object.Tag             =   "45"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":2424
            Key             =   "C43"
            Object.Tag             =   "43"
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":2626
            Key             =   "C50"
            Object.Tag             =   "50"
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":2828
            Key             =   "C42"
            Object.Tag             =   "42"
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":2A2A
            Key             =   "C41"
            Object.Tag             =   "41"
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":2C2C
            Key             =   "C13"
            Object.Tag             =   "13"
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":2E2E
            Key             =   "C48"
            Object.Tag             =   "48"
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":3030
            Key             =   "C7"
            Object.Tag             =   "7"
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":3232
            Key             =   "C44"
            Object.Tag             =   "44"
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":3434
            Key             =   "C6"
            Object.Tag             =   "6"
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":3636
            Key             =   "C4"
            Object.Tag             =   "4"
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":3838
            Key             =   "C8"
            Object.Tag             =   "8"
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":3A3A
            Key             =   "C33"
            Object.Tag             =   "33"
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":3C3C
            Key             =   "C54"
            Object.Tag             =   "54"
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":3E3E
            Key             =   "C15"
            Object.Tag             =   "15"
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":4040
            Key             =   "C38"
            Object.Tag             =   "38"
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":4242
            Key             =   "C40"
            Object.Tag             =   "40"
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":4444
            Key             =   "C36"
            Object.Tag             =   "36"
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":4646
            Key             =   "C35"
            Object.Tag             =   "35"
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":4848
            Key             =   "C34"
            Object.Tag             =   "34"
         EndProperty
         BeginProperty ListImage38 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":4A4A
            Key             =   "C37"
            Object.Tag             =   "37"
         EndProperty
         BeginProperty ListImage39 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":4C4C
            Key             =   "C39"
            Object.Tag             =   "39"
         EndProperty
         BeginProperty ListImage40 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmProp.frx":4E4E
            Key             =   "C2"
            Object.Tag             =   "2"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmProp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Public Sub Form_Load()

Dim aLb As Libelle
Dim i As Integer
Dim aRect As Rect
Dim Res As Long
Dim TypeObj As Integer

'-> Indiquer que la feuille est charg�e
frmPropVisible = True

'-> Initilaisation de la largeur des colonnes
Me.Prop.ColWidth(0) = CInt(Me.Prop.Width * (2 / 5))
Me.Prop.ColWidth(1) = Fix(Me.Prop.Width * (3 / 5))

'-> Cr�er l'objet de r�f�rence
Set aLb = Libelles("FRMPROP")

'-> Charger les libell�s de la feuille
Me.Caption = aLb.GetCaption(1)
Me.Prop.Row = 0
Me.Prop.Col = 0
Me.Prop.Text = aLb.GetCaption(2)
Me.Prop.Col = 1
Me.Prop.Text = aLb.GetCaption(3)


'-> Cr�er le nombre de ligne de propri�t�
If TypeOf ObjCours Is Maquette Then
        Set aLb = Libelles("PROPMAQ")
        TypeObj = 1
ElseIf TypeOf ObjCours Is Section Then
        Set aLb = Libelles("PROPSECTION")
        TypeObj = 2
ElseIf TypeOf ObjCours Is ImageObj Then
        Set aLb = Libelles("PROPBMP")
        TypeObj = 3
ElseIf TypeOf ObjCours Is Cadre Then
        Set aLb = Libelles("PROPCADRE")
        TypeObj = 4
ElseIf TypeOf ObjCours Is Tableau Then
        Set aLb = Libelles("PROPTABLEAU")
        TypeObj = 6
End If
Me.Prop.Rows = aLb.NbKey + 1
Me.Prop.FixedCols = 1
Me.Prop.FixedRows = 1
'-> Charger les libell�s des propri�t�s
For i = 1 To aLb.NbKey
    Me.Prop.Col = 1
    Me.Prop.Row = i
    Me.Prop.CellBackColor = QBColor(15)
    Me.Prop.Text = ""
    Me.Prop.CellAlignment = 1
    Me.Prop.Col = 0
    Me.Prop.Text = aLb.GetCaption(i)
    Me.Prop.RowData(i) = TypeObj * 10 + i
Next 'Pour toutes les propri�t�s

'-> Redimensionner la hauteur de la feuille en fonction du nombre de colonne
Me.Prop.Height = Me.Prop.RowPos(aLb.NbKey) + Me.Prop.RowHeight(aLb.NbKey) + Me.Prop.RowHeight(aLb.NbKey) * 0.5
Res = GetClientRect(Me.Prop.hwnd, aRect)
Me.Height = Me.ScaleY(aRect.Bottom + GetSystemMetrics(4) + (GetSystemMetrics(46) * 4), 3, 1)

'-> Liberer le pointeur
Set aLb = Nothing

'-> Afficher les propri�t�s
DisplayProperties

End Sub


Private Sub Form_Unload(Cancel As Integer)

If EraseFrm Then
Else
    frmPropVisible = False
End If

End Sub

Private Sub ListProp_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        ListProp_LostFocus
        Exit Sub
    End If
    If KeyAscii = 27 Then
        Me.ListProp.Text = Me.Prop.Text
        Me.ListProp.Visible = False
    End If
    
End Sub

Private Sub ListProp_LostFocus()

Select Case Me.Prop.RowData(Me.Prop.Row)
    
    Case 25 'Alignement Top
    
        ObjCours.AlignementTop = Me.ListProp.ListIndex + 1
        DisplayProperties
    
    Case 27 'Alignement Left
        
        ObjCours.AlignementLeft = Me.ListProp.ListIndex + 2
        DisplayProperties
                        
End Select 'selon le RowData

Me.ListProp.Visible = False

End Sub

Private Sub Prop_DblClick()

Dim aLb As Libelle
Dim aMaq As Maquette
Dim aFrmSection As frmSection
Dim Rep
Dim Tempo As String
     
On Error GoTo InvalidProp

If Me.Prop.Col = 0 Then Me.Prop.Col = 1

'-> Dans tous les cas positionner la liste d�roulante
Me.ListProp.Width = Me.Prop.ColWidth(Me.Prop.Col) - Me.ScaleY(GetSystemMetrics(46) * 3, 3, 1)
Me.ListProp.Top = Me.Prop.RowPos(Me.Prop.Row) + Me.ScaleY(GetSystemMetrics(46) * 2, 3, 1)
Me.ListProp.Left = Me.Prop.ColPos(Me.Prop.Col) + Me.ScaleX(GetSystemMetrics(46) * 2, 3, 1)

Me.Prop.Tag = Me.Prop.Row

Set aLb = Libelles("FRMPROP")

Select Case Me.Prop.RowData(Me.Prop.Row)

    '****************************
    '* Propri�t�s des maquettes *
    '****************************

    Case 11, 17 'Nom de la maquette
    
        MsgBox aLb.GetCaption(24), vbInformation + vbOKOnly, aLb.GetCaption(2)
        
    Case 19 'Page de garde
    
'        ObjCours.PageGarde = False
'        Me.Prop.Text = aLb.GetCaption(11)
    
        If ObjCours.PageGarde = True Then
            '-> Passer � non
            ObjCours.PageGarde = False
            Me.Prop.Text = aLb.GetCaption(11)
        Else
            ObjCours.PageGarde = True
            Me.Prop.Text = aLb.GetCaption(10)
        End If
        
    Case 20 'Page de s�lection
    
'        ObjCours.PageSelection = False
'        Me.Prop.Text = aLb.GetCaption(11)
'
        If ObjCours.PageSelection = True Then
            '-> Passer � non
            ObjCours.PageSelection = False
            Me.Prop.Text = aLb.GetCaption(11)
        Else
            ObjCours.PageSelection = True
            Me.Prop.Text = aLb.GetCaption(10)
        End If
        
    Case 12, 62 'Orientation
            
        If ObjCours.Orientation = aLb.GetCaption(12) Then
            ObjCours.Orientation = aLb.GetCaption(13)
        Else
            ObjCours.Orientation = aLb.GetCaption(12)
        End If
        Me.Prop.Text = ObjCours.Orientation
        '-> Afficher les nouvelles dimensions pour la maquette
        If TypeOf ObjCours Is Maquette Then
            Me.Prop.Col = 1
            Me.Prop.Row = 3
            Me.Prop.Text = ObjCours.Hauteur
            Me.Prop.Row = 4
            Me.Prop.Text = ObjCours.Largeur
        End If
        
        '-> PIERROT 06/07/2005
        '-> mise a jour de la propriete orientation dans la maquette
        '-> Pointer sur les objets
        Set aMaq = Maquettes("MAQUETTE")
        aMaq.Orientation = ObjCours.Orientation
        
    Case 13, 23, 36, 45  'Hauteur
        Rep = InputBox(aLb.GetCaption(5), aLb.GetCaption(2), ObjCours.Hauteur)
        If Rep <> "" Then
            If Not IsNumeric(Rep) Then GoTo InvalidProp
            If Rep = 0 Then GoTo InvalidProp
            '-> Appliquer la modif
            ObjCours.Hauteur = Abs(Rep)
            Me.Prop.Text = ObjCours.Hauteur
            '-> Pointer sur les objets
            Set aMaq = Maquettes("MAQUETTE")
            If TypeOf ObjCours Is Section Then
                Set aFrmSection = aMaq.frmSections(UCase$(ObjCours.Nom))
            Else
                Set aFrmSection = aMaq.frmSections(UCase$(ObjCours.SectionName))
            End If
            '-> Dans le cas de la hauteur de la section modifier la repr�sentation physique
            Select Case Me.Prop.RowData(Me.Prop.Row)
                Case 23 ' hauteur d'une section
                    aFrmSection.ContainerRtf.Height = aFrmSection.ScaleY(ObjCours.Hauteur, 7, 1)
                    '-> Pour redimension des barres
                    aFrmSection.Form_Resize
                    '-> R�ajuster les alignements
                    ObjCours.RefreshAlignSection
                Case 36
                    aFrmSection.BitMap(ObjCours.IdAffichage).Height = Me.ScaleY(ObjCours.Hauteur, 7, 1)
                    aFrmSection.BitMap(ObjCours.IdAffichage).AutoSize = False
                    '-> Modifier les alignements
                    RefreshAlign ObjCours
                Case 45
                    aFrmSection.Cadre(ObjCours.IdAffichage).Height = Me.ScaleY(ObjCours.Hauteur, 7, 1)
                    aFrmSection.CadreTexte(ObjCours.IdAffichage).Height = Me.ScaleY(ObjCours.Hauteur - (ObjCours.MargeInterne * 2), 7, 3)
                    ObjCours.RefreshBordures
                    '-> Modifier les alignements
                    RefreshAlign ObjCours
            End Select
            '-> Liberer les pointeurs
            Set aMaq = Nothing
            Set aFrmSection = Nothing
            
        End If
        
    Case 14, 24, 35, 44 'Largeur
    
        Rep = InputBox(aLb.GetCaption(6), aLb.GetCaption(2), ObjCours.Largeur)
        If Rep <> "" Then
            If Not IsNumeric(Rep) Then GoTo InvalidProp
            If Rep = 0 Then GoTo InvalidProp
            '-> Appliquer la modif
            ObjCours.Largeur = Abs(Rep)
            Me.Prop.Text = ObjCours.Largeur
            '-> Pointer sur les objets
            Set aMaq = Maquettes("MAQUETTE")
            If TypeOf ObjCours Is Section Then
                Set aFrmSection = aMaq.frmSections(UCase$(ObjCours.Nom))
            Else
                Set aFrmSection = aMaq.frmSections(UCase$(ObjCours.SectionName))
            End If
            Select Case Me.Prop.RowData(Me.Prop.Row)
                Case 24 ' Largeur d'une section
                    aFrmSection.ContainerRtf.Width = aFrmSection.ScaleX(ObjCours.Largeur, 7, 1)
                    '-> Pour redimension des barres
                    aFrmSection.Form_Resize
                    '-> R�ajuster les alignements
                    ObjCours.RefreshAlignSection
                Case 35 'Largeur d'un Bmp
                    aFrmSection.BitMap(ObjCours.IdAffichage).Width = Me.ScaleX(ObjCours.Largeur, 7, 1)
                    aFrmSection.BitMap(ObjCours.IdAffichage).AutoSize = False
                    '-> Modifier les alignements
                    RefreshAlign ObjCours
                Case 44
                    aFrmSection.Cadre(ObjCours.IdAffichage).Width = Me.ScaleX(ObjCours.Largeur, 7, 1)
                    aFrmSection.CadreTexte(ObjCours.IdAffichage).Width = Me.ScaleX(ObjCours.Largeur - (ObjCours.MargeInterne * 2), 7, 3)
                    ObjCours.RefreshBordures
                    '-> Modifier les alignements
                    RefreshAlign ObjCours
            End Select
            '-> Liberer les pointeurs
            Set aMaq = Nothing
            Set aFrmSection = Nothing
        End If
    
    Case 15 'Marge haut
    
        Rep = InputBox(aLb.GetCaption(15), aLb.GetCaption(2), ObjCours.MargeTop)
        If Rep <> "" Then
            If Not IsNumeric(Rep) Then GoTo InvalidProp
            '-> Appliquer la modif
            ObjCours.MargeTop = Abs(Rep)
            Me.Prop.Text = ObjCours.MargeTop
        End If
    
    Case 16 'Marge bas
    
        Rep = InputBox(aLb.GetCaption(17), aLb.GetCaption(2), ObjCours.MargeLeft)
        If Trim(Rep) <> "" Then
            If Not IsNumeric(Rep) Then GoTo InvalidProp
            '-> Appliquer la modif
            ObjCours.MargeLeft = Abs(Rep)
            Me.Prop.Text = ObjCours.MargeLeft
        End If
    
    Case 18 'Description
        
        Rep = InputBox(aLb.GetCaption(21), aLb.GetCaption(2), ObjCours.Description)
        If Trim(Rep) <> "" Then
            If Rep = "?" Then Rep = ""
            ObjCours.Description = Rep
            Me.Prop.Text = Rep
        End If
                            
    Case 22 'Contour
     
        If ObjCours.Contour = True Then
            '-> Passer � non
            ObjCours.Contour = False
            Me.Prop.Text = aLb.GetCaption(11)
        Else
            ObjCours.Contour = True
            Me.Prop.Text = aLb.GetCaption(10)
        End If
        '-> Modifier l'apparence du Rtf associ�
        Set aMaq = Maquettes("MAQUETTE")
        Set aFrmSection = aMaq.frmSections(UCase$(ObjCours.Nom))
        
        If ObjCours.Contour = True Then
            aFrmSection.RangText.BorderStyle = rtfFixedSingle
            aFrmSection.Toolbar1.Buttons(11).Image = "Plein"
        Else
            aFrmSection.RangText.BorderStyle = rtfNoBorder
            aFrmSection.Toolbar1.Buttons(11).Image = "Sans"
        End If
        
        Set aMaq = Nothing
        Set aFrmSection = Nothing
                
    Case 26, 34, 43  'Top
    
       Rep = InputBox(aLb.GetCaption(8), aLb.GetCaption(2), ObjCours.Top)
        If Rep <> "" Then
            If Not IsNumeric(Rep) Then GoTo InvalidProp
            '-> Appliquer la modif
            ObjCours.Top = Abs(Rep)
            Me.Prop.Text = ObjCours.Top
            Set aMaq = Maquettes("MAQUETTE")
            If TypeOf ObjCours Is Section Then
                Set aFrmSection = aMaq.frmSections(UCase$(ObjCours.Nom))
            Else
                Set aFrmSection = aMaq.frmSections(UCase$(ObjCours.SectionName))
            End If
            Select Case Me.Prop.RowData(Me.Prop.Row)
                Case 34
                    aFrmSection.BitMap(ObjCours.IdAffichage).Top = Me.ScaleY(ObjCours.Top, 7, 1)
                    '-> Modifier les alignements
                    RefreshAlign ObjCours
                Case 43
                    aFrmSection.Cadre(ObjCours.IdAffichage).Top = Me.ScaleY(ObjCours.Top, 7, 1)
            End Select
            '-> Liberrer les ressources
            Set aMaq = Nothing
            Set aFrmSection = Nothing
        End If
        
    Case 28, 33, 42  'Left
        Rep = InputBox(aLb.GetCaption(7), aLb.GetCaption(2), ObjCours.Left)
        If Rep <> "" Then
            If Not IsNumeric(Rep) Then GoTo InvalidProp
            '-> Appliquer la modif
            ObjCours.Left = Abs(Rep)
            Me.Prop.Text = ObjCours.Left
            '-> Pointer sur les objets
            Set aMaq = Maquettes("MAQUETTE")
            If TypeOf ObjCours Is Section Then
                Set aFrmSection = aMaq.frmSections(UCase$(ObjCours.Nom))
            Else
                Set aFrmSection = aMaq.frmSections(UCase$(ObjCours.SectionName))
            End If
            Select Case Me.Prop.RowData(Me.Prop.Row)
                Case 33 'Left BMP
                    aFrmSection.BitMap(ObjCours.IdAffichage).Left = Me.ScaleX(ObjCours.Left, 7, 1)
                    '-> Modifier les alignements
                    RefreshAlign ObjCours
                Case 42 'Left cadre
                    aFrmSection.Cadre(ObjCours.IdAffichage).Left = Me.ScaleX(ObjCours.Left, 7, 1)
                    '-> Modifier les alignements
                    RefreshAlign ObjCours
            End Select
            '-> Liberrer les ressources
            Set aMaq = Nothing
            Set aFrmSection = Nothing
                        
        End If
     
    Case 25 'Alignement top
     
        AddItemToList aLb.GetCaption(14) & "|" & aLb.GetCaption(15) _
                                        & "|" & aLb.GetCaption(16) _
                                        & "|" & aLb.GetCaption(19) _
                                        & "|" & aLb.GetCaption(20), Me.ListProp
        Me.ListProp.Text = Me.Prop.Text
        Me.ListProp.Visible = True
        Me.ListProp.SetFocus
     
    Case 27 'Alignement Left
        
        AddItemToList aLb.GetCaption(17) _
                      & "|" & aLb.GetCaption(18) _
                      & "|" & aLb.GetCaption(19) _
                      & "|" & aLb.GetCaption(20), Me.ListProp
                      
        Me.ListProp.Text = Me.Prop.Text
        Me.ListProp.Visible = True
        Me.ListProp.SetFocus
          
     Case 29 'backcolor
        
        '-> Afficher la feuille
        DisplayfrmPalette
        '-> Appliquer la nouvelle couleur
        If RetourColor <> -1 Then
            If RetourColor = 999 Then RetourColor = 16777215
            '-> Modifier la propri�t�
            ObjCours.BackColor = RetourColor
            Me.Prop.Col = 1
            Me.Prop.CellBackColor = RetourColor
            '-> Modifier l'affichage
            Set aMaq = Maquettes("MAQUETTE")
            Set aFrmSection = aMaq.frmSections(UCase$(ObjCours.Nom))
            aFrmSection.RangText.BackColor = RetourColor
            Set aMaq = Nothing
            Set aFrmSection = Nothing
        End If
            
    
     Case 30, 38, 52, 63 'Ordre d'affichage
        
        LoadOrdreAffichage ObjCours
        frmDisplayOrder.Show vbModal
     
     Case 32 'Fichier source BMP
     
        '-> Tester si le bmp est en mode variable
        If ObjCours.isVariable Then
           '-> Demander de saisir le code du # associ�
            If ObjCours.Fichier = "" Then
                Tempo = ""
            Else
                Tempo = Mid$(ObjCours.Fichier, 2, 4)
            End If
            Rep = InputBox(aLb.GetCaption(26), aLb.GetCaption(2), Tempo)
            If Rep = "" Then Exit Sub
            If Len(Rep) <> 4 Then GoTo InvalidProp
            '-> Afficher le #
            ObjCours.Fichier = "^" & Rep
            Me.DisplayProperties
        Else
            '-> R�cup�rer la feuille associ�e
            Set aMaq = Maquettes("MAQUETTE")
            Set aFrmSection = aMaq.frmSections(UCase$(ObjCours.SectionName))
            '-> Afficher la page de s�lection du bmp
            frmLoadBmp.Show vbModal
            '-> Mise � jour des propri�t�s
            If RetourBmp <> "" Then
                '-> Recherhcer le fichier
                Tempo = SearchPath(Entry(1, RetourBmp, "|"), Entry(2, RetourBmp, "|"))
                If Tempo <> "" Then
                    '-> Charger le fichier sp�cifi�
                    aFrmSection.BitMap(ObjCours.IdAffichage).Picture = LoadPicture(Tempo)
                    '-> Modification des dimensions
                    ObjCours.Largeur = aFrmSection.ScaleX(aFrmSection.BitMap(ObjCours.IdAffichage).Width, 1, 7)
                    ObjCours.Hauteur = aFrmSection.ScaleY(aFrmSection.BitMap(ObjCours.IdAffichage).Height, 1, 7)
                    ObjCours.Fichier = Entry(2, RetourBmp, "|")
                    ObjCours.Path = Entry(1, RetourBmp, "|")
                    '-> Mettre ajour l'affichage
                    Me.DisplayProperties
                End If
            End If
            '-> Liberer les ressources
            Set aMaq = Nothing
            Set aFrmSection = Nothing
        End If
        'Me.Prop.Text = ObjCours.Fichier
     
     Case 37
     '-> Pointer sur la feuille
        Set aFrmSection = Maquettes("MAQUETTE").frmSections(UCase$(ObjCours.SectionName))
        
        If ObjCours.Contour = True Then
            '-> Passer � non
            ObjCours.Contour = False
            Me.Prop.Text = aLb.GetCaption(11)
            aFrmSection.BitMap(ObjCours.IdAffichage).BorderStyle = 0
        Else
            ObjCours.Contour = True
            Me.Prop.Text = aLb.GetCaption(10)
            aFrmSection.BitMap(ObjCours.IdAffichage).BorderStyle = 1
        End If
        
        Set aFrmSection = Nothing

    Case 39  'Bmp Variable
     
        '-> Modifier la valeur de la propri�t�
        If ObjCours.isVariable Then
            ObjCours.isVariable = False
            ObjCours.UseAssociation = False
            Me.Prop.Text = aLb.GetCaption(11)
            Me.Prop.Row = Me.Prop.Row + 1
            Me.Prop.Text = aLb.GetCaption(11)
        Else
            ObjCours.isVariable = True
            Me.Prop.Text = aLb.GetCaption(10)
        End If
        Me.Prop.Row = 2
        ObjCours.Fichier = ""
        Me.Prop.Text = ObjCours.Fichier
        
    Case 40 'Utiliser l'association
    
        If ObjCours.UseAssociation Then
            ObjCours.UseAssociation = False
            Me.Prop.Text = aLb.GetCaption(11)
        Else
            ObjCours.UseAssociation = True
            Me.Prop.Text = aLb.GetCaption(10)
            If Not ObjCours.isVariable Then
                Me.Prop.Row = Me.Prop.Row - 1
                Me.Prop.Text = aLb.GetCaption(10)
                Me.Prop.Row = 2
                ObjCours.Fichier = ""
                Me.Prop.Text = ObjCours.Fichier
            End If
        End If
        
     
    Case 46 'Distance interne
     
        Rep = InputBox(aLb.GetCaption(9), aLb.GetCaption(2), ObjCours.MargeInterne)
        If Rep <> "" Then
            If Not IsNumeric(Rep) Then GoTo InvalidProp
            '-> Affectation de la valeur
            ObjCours.MargeInterne = Abs(CSng(Rep))
            Me.Prop.Text = ObjCours.MargeInterne
            '-> Modification de l'affichage
            Set aMaq = Maquettes("MAQUETTE")
            Set aFrmSection = aMaq.frmSections(UCase$(ObjCours.SectionName))
            aFrmSection.CadreTexte(ObjCours.IdAffichage).Height = Me.ScaleY(ObjCours.Hauteur - (ObjCours.MargeInterne * 2), 7, 3)
            aFrmSection.CadreTexte(ObjCours.IdAffichage).Width = Me.ScaleX(ObjCours.Largeur - (ObjCours.MargeInterne * 2), 7, 3)
            aFrmSection.CadreTexte(ObjCours.IdAffichage).Left = Me.ScaleX(ObjCours.MargeInterne, 7, 3)
            aFrmSection.CadreTexte(ObjCours.IdAffichage).Top = Me.ScaleY(ObjCours.MargeInterne, 7, 3)
            ObjCours.RefreshBordures
            '-> Liberrer les ressources
            Set aMaq = Nothing
            Set aFrmSection = Nothing
        End If
     
            
     Case 47 'Backcolor
     
        DisplayfrmPalette
        If RetourColor <> -1 Then
            '-> Modification de la propri�t�
            ObjCours.BackColor = RetourColor
            Me.Prop.CellBackColor = ObjCours.BackColor
            '-> Modification de la repr�sentation
            Set aMaq = Maquettes("MAQUETTE")
            Set aFrmSection = aMaq.frmSections(UCase$(ObjCours.SectionName))
            aFrmSection.Cadre(ObjCours.IdAffichage).BackColor = RetourColor
            aFrmSection.CadreTexte(ObjCours.IdAffichage).BackColor = RetourColor
            ObjCours.RefreshBordures
            '-> Liberrer les ressources
            Set aFrmSection = Nothing
            Set aMaq = Nothing
        End If
        
     
     Case 48, 49, 50, 51 'Bordures G , D , H , B
       
        If Me.Prop.Text = aLb.GetCaption(10) Then
            Select Case Me.Prop.RowData(Me.Prop.Row)
                Case 48
                    ObjCours.BordureGauche = False
                Case 49
                    ObjCours.BordureDroite = False
                Case 50
                    ObjCours.BordureHaut = False
                Case 51
                    ObjCours.BordureBas = False
            End Select
            Me.Prop.Text = aLb.GetCaption(11)
        Else
            Select Case Me.Prop.RowData(Me.Prop.Row)
                Case 48
                    ObjCours.BordureGauche = True
                Case 49
                    ObjCours.BordureDroite = True
                Case 50
                    ObjCours.BordureHaut = True
                Case 51
                    ObjCours.BordureBas = True
            End Select
            Me.Prop.Text = aLb.GetCaption(10)
        End If
        
End Select

Set aLb = Nothing
Set aMaq = Nothing
Set aFrmSection = Nothing


Exit Sub

InvalidProp:

    MsgBox aLb.GetCaption(25), vbCritical + vbOKOnly, aLb.GetToolTip(25)
    



End Sub

Private Sub Prop_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then Unload Me
    If KeyAscii = 13 Then Prop_DblClick
End Sub

Private Sub DisplayfrmPalette()

Dim Res As Long
Dim aPoint As POINTAPI

'-> Nature indique : 1 backcolor, 2 forecolor

Res = GetCursorPos(aPoint)
frmPalette.Top = Screen.TwipsPerPixelY * aPoint.y
frmPalette.Left = Screen.TwipsPerPixelX * aPoint.x
frmPalette.Show vbModal

End Sub

Public Sub DisplayProperties()

'-> Affichage des propri�t�s de l'objet en cours
Dim aLb As Libelle
Dim TypeObj As Integer

If TypeOf ObjCours Is Maquette Then
        TypeObj = 1
ElseIf TypeOf ObjCours Is Section Then
        TypeObj = 2
ElseIf TypeOf ObjCours Is ImageObj Then
        TypeObj = 3
ElseIf TypeOf ObjCours Is Cadre Then
        TypeObj = 4
ElseIf TypeOf ObjCours Is Tableau Then
        TypeObj = 6
End If

Set aLb = Libelles("FRMPROP")

Select Case TypeObj

    Case 1 'Maquette

        Me.Prop.Col = 1
        Me.Prop.Row = 1
        Me.Prop.Text = ObjCours.Nom
        Me.Prop.Row = 2
        Me.Prop.Text = ObjCours.Orientation
        Me.Prop.Row = 3
        Me.Prop.Text = ObjCours.Hauteur
        Me.Prop.Row = 4
        Me.Prop.Text = ObjCours.Largeur
        Me.Prop.Row = 5
        Me.Prop.Text = ObjCours.MargeTop
        Me.Prop.Row = 6
        Me.Prop.Text = ObjCours.MargeLeft
        Me.Prop.Row = 7
        Me.Prop.Text = ObjCours.Fichier
        Me.Prop.Row = 8
        Me.Prop.Text = ObjCours.Description
        Me.Prop.Row = 9
        If ObjCours.PageGarde = True Then
            Me.Prop.Text = aLb.GetCaption(10)
        Else
            Me.Prop.Text = aLb.GetCaption(11)
        End If
        Me.Prop.Row = 10
        If ObjCours.PageSelection = True Then
            Me.Prop.Text = aLb.GetCaption(10)
        Else
            Me.Prop.Text = aLb.GetCaption(11)
        End If
        
    Case 2 'Section
    
        Me.Prop.Col = 1
        Me.Prop.Row = 1
        Me.Prop.Text = ObjCours.Nom
        Me.Prop.Row = 2
        If ObjCours.Contour = True Then
            Me.Prop.Text = aLb.GetCaption(10)
        Else
            Me.Prop.Text = aLb.GetCaption(11)
        End If
        Me.Prop.Row = 3
        Me.Prop.Text = ObjCours.Hauteur
        Me.Prop.Row = 4
        Me.Prop.Text = ObjCours.Largeur
        Me.Prop.Row = 5
        Select Case ObjCours.AlignementTop
            Case 1 'Libre
                Me.Prop.Text = aLb.GetCaption(14)
            Case 2 'Marge haut
                Me.Prop.Text = aLb.GetCaption(15)
            Case 3 'Marge bas
                Me.Prop.Text = aLb.GetCaption(16)
            Case 4 'Centr�
                Me.Prop.Text = aLb.GetCaption(19)
            Case 5 'Sp�cifi�
                Me.Prop.Text = aLb.GetCaption(20)
        End Select
        Me.Prop.Row = 6
        Me.Prop.Text = ObjCours.Top
        Me.Prop.Row = 7
        Select Case ObjCours.AlignementLeft
            Case 2 'Marge gauche
                Me.Prop.Text = aLb.GetCaption(17)
            Case 3 'Marge Droite
                Me.Prop.Text = aLb.GetCaption(18)
            Case 4 'Centr�
                Me.Prop.Text = aLb.GetCaption(19)
            Case 5 'Sp�cifi�
                Me.Prop.Text = aLb.GetCaption(20)
        End Select
        Me.Prop.Row = 8
        Me.Prop.Text = ObjCours.Left
        Me.Prop.Row = 9
        Me.Prop.CellBackColor = ObjCours.BackColor
        Me.Prop.Row = 10
        Me.Prop.Text = ObjCours.IdAffichage
        
    Case 3 'Bmp

        Me.Prop.Col = 1
        Me.Prop.Row = 1
        Me.Prop.Text = ObjCours.Nom
        Me.Prop.Row = 2
        Me.Prop.Text = ObjCours.Fichier
        Me.Prop.Row = 3
        Me.Prop.Text = ObjCours.Left
        Me.Prop.Row = 4
        Me.Prop.Text = ObjCours.Top
        Me.Prop.Row = 5
        Me.Prop.Text = ObjCours.Largeur
        Me.Prop.Row = 6
        Me.Prop.Text = ObjCours.Hauteur
        Me.Prop.Row = 7
        If ObjCours.Contour = True Then
            Me.Prop.Text = aLb.GetCaption(10)
        Else
            Me.Prop.Text = aLb.GetCaption(11)
        End If
        Me.Prop.Row = 8
        Me.Prop.Text = ObjCours.IdOrdreAffichage
        Me.Prop.Row = 9
        If ObjCours.isVariable = True Then
            Me.Prop.Text = aLb.GetCaption(10)
        Else
            Me.Prop.Text = aLb.GetCaption(11)
        End If
        '-> Association d'image
        Me.Prop.Row = 10
        If ObjCours.UseAssociation = True Then
            Me.Prop.Text = aLb.GetCaption(10)
        Else
            Me.Prop.Text = aLb.GetCaption(11)
        End If
        
        
    Case 4 'Cadre
    
        Me.Prop.Col = 1
        Me.Prop.Row = 1
        Me.Prop.Text = ObjCours.Nom
        Me.Prop.Row = 2
        Me.Prop.Text = ObjCours.Left
        Me.Prop.Row = 3
        Me.Prop.Text = ObjCours.Top
        Me.Prop.Row = 4
        Me.Prop.Text = ObjCours.Largeur
        Me.Prop.Row = 5
        Me.Prop.Text = ObjCours.Hauteur
        Me.Prop.Row = 6
        Me.Prop.Text = ObjCours.MargeInterne
        Me.Prop.Row = 7
        Me.Prop.CellBackColor = ObjCours.BackColor
        Me.Prop.Row = 8
        If ObjCours.BordureGauche = True Then
            Me.Prop.Text = aLb.GetCaption(10)
        Else
            Me.Prop.Text = aLb.GetCaption(11)
        End If
        Me.Prop.Row = 9
        If ObjCours.BordureDroite = True Then
            Me.Prop.Text = aLb.GetCaption(10)
        Else
            Me.Prop.Text = aLb.GetCaption(11)
        End If
        Me.Prop.Row = 10
        If ObjCours.BordureHaut = True Then
            Me.Prop.Text = aLb.GetCaption(10)
        Else
            Me.Prop.Text = aLb.GetCaption(11)
        End If
        Me.Prop.Row = 11
        If ObjCours.BordureBas = True Then
            Me.Prop.Text = aLb.GetCaption(10)
        Else
            Me.Prop.Text = aLb.GetCaption(11)
        End If
        Me.Prop.Row = 12
        Me.Prop.Text = ObjCours.IdOrdreAffichage
        
    Case 6 'Tableaux
    
        Me.Prop.Col = 1
        Me.Prop.Row = 1
        Me.Prop.Text = ObjCours.Nom
        Me.Prop.Row = 2
        Me.Prop.Text = ObjCours.Orientation
        Me.Prop.Row = 3
        Me.Prop.Text = ObjCours.IdAffichage
    
    
End Select

Me.Prop.Col = 1
Me.Prop.Row = 1
Set aLb = Nothing

End Sub

