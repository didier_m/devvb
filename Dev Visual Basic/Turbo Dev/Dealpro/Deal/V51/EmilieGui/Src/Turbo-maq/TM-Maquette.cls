VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Maquette"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* Objet Maquette                             *
'*                                            *
'**********************************************


'---> Liste des propri�t�s
Public Nom As String
Public Orientation As String
Public Fichier As String
Public Description As String
Public DateCreation As Date
Public PageGarde As Boolean
Public PageSelection As Boolean
Public Lig As Integer '-> Nouvelle Version indique le nombre de lignes carat�res de la maquette

'---> Variables Internes de stockage des donn�es pour les propri�t�s
Private pHauteur As Single
Private pLargeur As Single
Private pMargeTop As Single
Private pMargeLeft As Single

'---> Liste des fichiers champs associ�s � la maquette
Public FieldListe As String

'---> Collections du model objet
Public Sections As Collection
Public frmSections As Collection
Public frmTableurs As Collection
Public Bmps As Collection
Public Cadres As Collection
Public Tableaux As Collection

'---> Stocker les ordres d'affichage dans un tableau dynamic
Private pOrdreAffichage() As String
Public nEntries As Integer 'var utilis�e pour le redimensionnement de la matrice des ordres d'affichage

'---> Gestion du Mutlilangue
Private aLb As Libelle

'---> Gestion du publipostage
Public PubliPostage As String 'TYPE�KEY|DIESE�...

Public Property Get Hauteur() As Single

    Set aLb = Libelles("FRMPROP")
    If Orientation = aLb.GetCaption(12) Then
        Hauteur = pHauteur
    Else
        Hauteur = pLargeur
    End If
    Set aLb = Nothing
    
End Property

Public Property Let Hauteur(ByVal vNewValue As Single)
    pHauteur = vNewValue
End Property

Public Property Get Largeur() As Single
    
    Set aLb = Libelles("FRMPROP")
    If Orientation = aLb.GetCaption(12) Then
        Largeur = pLargeur
    Else
        Largeur = pHauteur
    End If
    Set aLb = Nothing
    
End Property

Public Property Let Largeur(ByVal vNewValue As Single)
    pLargeur = vNewValue
End Property


Public Property Get MargeTop() As Single
    MargeTop = pMargeTop
End Property

Public Property Let MargeTop(ByVal vNewValue As Single)
    pMargeTop = vNewValue
End Property

Public Property Get MargeLeft() As Single
    MargeLeft = pMargeLeft
End Property

Public Property Let MargeLeft(ByVal vNewValue As Single)
    pMargeLeft = vNewValue
End Property

Private Sub Class_Initialize()

'-> Initialisation des classes propri�t�s
    
Set Sections = New Collection
Set frmSections = New Collection
Set frmTableurs = New Collection
Set Cadres = New Collection
Set Bmps = New Collection
Set Tableaux = New Collection

'-> index pour les ordres d'affichage
nEntries = 1
PageGarde = False
PageSelection = False
    
End Sub

Public Sub AddTreeView()

'-> Ajout dans le treeview de l'icone de la maquette

Dim aNode As Node

Set aNode = frmNaviga.TreeNaviga.Nodes.Add(, , "MAQUETTE", Nom, "Maquette")
aNode.Tag = "MAQUETTE"
aNode.Selected = True

End Sub

Public Sub DelTreeView()

frmNaviga.TreeNaviga.Nodes.Remove "MAQUETTE"

End Sub

Public Function GetSectionExist(ByVal MatchName As String) As Boolean

'---> V�rifie si une section n'existe pas d�ja avec ce nom

Dim aSection As Section

For Each aSection In Sections
    If UCase$(aSection.Nom) = UCase$(MatchName) Then
        GetSectionExist = True
        Exit Function
    End If
Next
GetSectionExist = False
    

End Function

Public Function GetTableauExist(ByVal MatchName As String) As Boolean

'---> V�rifie si une section n'existe pas d�ja avec ce nom

Dim aTb As Tableau

For Each aTb In Tableaux
    If UCase$(aTb.Nom) = UCase$(MatchName) Then
        GetTableauExist = True
        Exit Function
    End If
Next
GetTableauExist = False

End Function
Public Function AddOrdreAffichage(ByVal Value As String) As Integer

ReDim Preserve pOrdreAffichage(1 To nEntries)
pOrdreAffichage(nEntries) = Value
AddOrdreAffichage = nEntries
nEntries = nEntries + 1


End Function

Public Function GetOrdreAffichage(ByVal nIndex As Integer) As String

GetOrdreAffichage = pOrdreAffichage(nIndex)


End Function

Public Sub ClearOrdreAffichage()

    Erase pOrdreAffichage()
    nEntries = 1
    
End Sub

Public Sub DelOrdreAffichage(ByVal nIndex As Integer)

'---> Cette proc�dure supprime une entr�e dans la matrice des ordres d'affichage

Dim DupOrdre() As String
Dim i As Integer
Dim j As Integer
Dim aSection As Section
Dim aTb As Tableau
Dim TypeObj As String
Dim NomObj As String

'-> Dans un premier temps, faire une copie du tableau des ordres d'affichage
ReDim DupOrdre(1 To nEntries)
For i = 1 To nEntries - 1
    DupOrdre(i) = pOrdreAffichage(i)
Next

'-> Ecraser la matrice
Erase pOrdreAffichage()

'-> Ne rien faire s'il n'y a plus d'entr�es
If nEntries - 1 = 1 Then
    nEntries = 1
    Exit Sub
End If

'-> La reconstituer en supprimant l'entr�e
ReDim pOrdreAffichage(1 To nEntries - 2)
j = 1
For i = 1 To nEntries - 1
    If i = nIndex Then
    Else
        '-> Ajouter l'entr�e dans la matrice
        pOrdreAffichage(j) = DupOrdre(i)
        '-> Modifier l'ordre de l'objet concern�
        TypeObj = Entry(1, pOrdreAffichage(j), "-")
        NomObj = Entry(2, pOrdreAffichage(j), "-")
        If UCase$(TypeObj) = "SCT" Then
            Set aSection = Maquettes("MAQUETTE").Sections(UCase$(NomObj))
            aSection.IdAffichage = j
            Set aSection = Nothing
        Else
            Set aTb = Maquettes("MAQUETTE").Tableaux(UCase$(NomObj))
            aTb.IdAffichage = j
            Set aTb = Nothing
        End If
        '-> Incr�menter le compteur d'ordre
        j = j + 1
    End If
Next

'-> Indiquer qu'il y a une entr�e en mois
nEntries = nEntries - 1

End Sub



