VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmAlign 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   6210
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4890
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6210
   ScaleWidth      =   4890
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4200
      Top             =   2040
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   48
      ImageHeight     =   48
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   25
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":0000
            Key             =   "Align1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":1CDA
            Key             =   "Align2"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":39B4
            Key             =   "Align3"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":568E
            Key             =   "Align4"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":7368
            Key             =   "Align5"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":9042
            Key             =   "Align6"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":AD1C
            Key             =   "Align7"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":C9F6
            Key             =   "Align8"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":E6D0
            Key             =   "Align9"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":103AA
            Key             =   "Align10"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":12084
            Key             =   "Align11"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":13D5E
            Key             =   "Align16"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":15A38
            Key             =   "Align13"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":17712
            Key             =   "Align14"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":193EC
            Key             =   "Align15"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":1B0C6
            Key             =   "Align12"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":1CDA0
            Key             =   "AlignPage9"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":1EA7A
            Key             =   "AlignPage2"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":20754
            Key             =   "AlignPage3"
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":2242E
            Key             =   "AlignPage4"
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":24108
            Key             =   "AlignPage5"
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":25DE2
            Key             =   "AlignPage6"
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":27ABC
            Key             =   "AlignPage7"
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":29796
            Key             =   "AlignPage8"
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-frmAlign.frx":2B470
            Key             =   "AlignPage1"
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   5880
      Width           =   3855
   End
   Begin VB.Frame Frame1 
      Height          =   5655
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3975
      Begin VB.OptionButton Option2 
         Caption         =   "Option2"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   5160
         Width           =   3615
      End
      Begin VB.ListBox List1 
         Height          =   1035
         Left            =   240
         TabIndex        =   4
         Top             =   1920
         Width           =   3495
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Option1"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   2
         Top             =   1200
         Width           =   2175
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Option1"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Top             =   480
         Width           =   2175
      End
      Begin VB.Label Align 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   1
         Left            =   2760
         TabIndex        =   10
         Top             =   4560
         Width           =   735
      End
      Begin VB.Label Align 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   0
         Left            =   2760
         TabIndex        =   9
         Top             =   4080
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "Label4"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   4560
         Width           =   2295
      End
      Begin VB.Label Label3 
         Caption         =   "Label3"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   4080
         Width           =   2415
      End
      Begin VB.Label Label2 
         Caption         =   "Label2"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   1560
         Width           =   3375
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   3120
         Width           =   2415
      End
      Begin VB.Image Image1 
         Height          =   720
         Index           =   1
         Left            =   2760
         Picture         =   "TM-frmAlign.frx":2D14A
         Top             =   3120
         Width           =   720
      End
      Begin VB.Image Image1 
         Height          =   720
         Index           =   0
         Left            =   2760
         OLEDropMode     =   1  'Manual
         Picture         =   "TM-frmAlign.frx":2EE14
         Top             =   240
         Width           =   720
      End
   End
   Begin VB.Image Annuler 
      Height          =   480
      Left            =   4200
      Top             =   1440
      Width           =   630
   End
   Begin VB.Image Aide 
      Height          =   480
      Left            =   4200
      Top             =   840
      Width           =   630
   End
   Begin VB.Image Ok 
      Height          =   480
      Left            =   4200
      Top             =   240
      Width           =   630
   End
End
Attribute VB_Name = "frmAlign"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Cette variable indique l'alignement qu'il faut appliquer
Public RetourAlign As Integer
Dim MyObjectRefObj

Private Sub Align_DblClick(Index As Integer)

Dim Rep As String
Dim aLb As Libelle

'-> Gestion des messprog
Set aLb = Libelles("MESSAGE")

Rep = InputBox(aLb.GetCaption(30 + Index), aLb.GetToolTip(30 + Index), Me.Align(Index).Caption)
If Trim(Rep) = "" Then Exit Sub
If Not IsNumeric(Rep) Then GoTo ErreurSaisie
If CLng(Rep) > 32000 Then GoTo ErreurSaisie

Align(Index).Caption = Rep
        
Set aLb = Nothing
Exit Sub
    
ErreurSaisie:

    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Set aLb = Nothing
    Exit Sub
    
End Sub

Private Sub Annuler_Click()
    Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then Unload Me
End Sub

Private Sub Form_Load()

'---> Gestion des messprog
Dim aLb As Libelle

Set aLb = Libelles("FRMALIGN")

Me.Caption = aLb.GetCaption(1)
Me.Option1(0).Caption = aLb.GetCaption(2)
Me.Image1(0).ToolTipText = aLb.GetCaption(3)
Me.Image1(1).ToolTipText = aLb.GetCaption(3)
Me.Align(0).ToolTipText = aLb.GetCaption(3)
Me.Align(0).ToolTipText = aLb.GetCaption(3)
Me.Option1(1).Caption = aLb.GetCaption(4)
Me.Option2.Caption = aLb.GetCaption(10)
Me.Label1.Caption = aLb.GetCaption(6)
Me.Label2.Caption = aLb.GetCaption(5)
Me.Label3.Caption = aLb.GetCaption(8)
Me.Label4.Caption = aLb.GetCaption(9)
Me.Check1.Caption = aLb.GetCaption(7)
Me.Check1.Value = 1

Set aLb = Libelles("BOUTONS")

Me.Annuler.ToolTipText = aLb.GetCaption(1)
Me.Aide.ToolTipText = aLb.GetCaption(2)
Me.Ok.ToolTipText = aLb.GetCaption(3)

Me.Annuler.Picture = Turbo_Maq.Annuler.Picture
Me.Aide.Picture = Turbo_Maq.Aide.Picture
Me.Ok.Picture = Turbo_Maq.Ok.Picture

Set aLb = Nothing


'---> Gestion des alignements
GestLien

'-> Cle des images par d�faut
Me.Image1(0).Tag = "AlignPage1"
Me.Image1(1).Tag = "Align1"
Me.Option1(0).Tag = 1
Me.Option1(1).Tag = 1
Me.RetourAlign = 1

'-> V�rifier si l'objet en cours a d�ja un alignement
If ObjCours.MasterAlign <> "" Then
    '-> r�cup�ration de l'alignement
    If UCase$(Entry(1, ObjCours.MasterAlign, "-")) = "PAGE" Then
        '-> Align Page OK
        EnableOption1 True, 0
        '-> R�cup�ration de l'icone qui va bien
        Me.Image1(0).Picture = Me.ImageList1.ListImages(Entry(3, ObjCours.MasterAlign, "-")).Picture
        Me.Image1(0).Tag = Entry(3, ObjCours.MasterAlign, "-")
        '-> Sauvegarder l'alignement d�sir�
        Me.Option1(0).Tag = CInt(Entry(2, ObjCours.MasterAlign, "-"))
    Else
        '-> Alignement sur un autre Objet
        EnableOption1 True, 1
        '-> R�cup�ration de l'icone qui va bien
        Me.Image1(1).Picture = Me.ImageList1.ListImages(Entry(3, ObjCours.MasterAlign, "-")).Picture
        Me.Image1(1).Tag = Entry(3, ObjCours.MasterAlign, "-")
        '-> S�lectionner l'entr� dans la liste
        Me.List1.Text = Entry(2, Entry(1, ObjCours.MasterAlign, "-"), "|")
        '-> Mise � jour des positions
        Me.Align(0).Caption = Format(ObjCours.AlignX, "0.00")
        Me.Align(1).Caption = Format(ObjCours.AlignY, "0.00")
        '-> Sauvagarder l'alignement d�sir�
        Me.Option1(1).Tag = CInt(Entry(2, ObjCours.MasterAlign, "-"))
        '-> Bouton option Ok
        Me.Option1(1).Value = True
    End If
    '-> R�cup�ration du Lock
    If ObjCours.Locked Then
        Me.Check1.Value = 1
    Else
        Me.Check1.Value = 0
    End If
    '-> R�cup�ration de l'alignement
    RetourAlign = CInt(Entry(2, ObjCours.MasterAlign, "-"))
Else
    Me.Option1(0).Value = True
End If

'-> Rajouter le nom de l'objet dans la barre de titre
Me.Caption = Me.Caption & " [" & ObjCours.Nom & "]"

End Sub

Private Sub Image1_DblClick(Index As Integer)
    frmTypeAlign.TypeAlign = Index
    frmTypeAlign.Show vbModal
End Sub

Private Sub Ok_Click()

'---> R�aliser l'alignement

Dim KeyMaster As String
Dim NomObject As String
Dim TypeObject As String
Dim aSection As Section
Dim aObject
Dim Key As String
Dim i As Integer
Dim aLb As Libelle
Dim MyObjectRefObj
 
 
'-> Pointer sur la section de r�f�rence
Set aSection = Maquettes("MAQUETTE").Sections(UCase$(ObjCours.SectionName))

'-> Dans un premier temp, v�rifier s'il n'y avait pas de lien objet
If Trim(ObjCours.MasterAlign) = "" Then
Else
    If UCase$(Entry(1, ObjCours.MasterAlign, "-")) = "PAGE" Then
    Else
        '-> Rechercher notre r�f�rence dans la propri�t� SlaveAlign du maitre
        If TypeOf ObjCours Is Cadre Then
            Key = "CDR|" & ObjCours.Nom
        Else
            Key = "BMP|" & ObjCours.Nom
        End If
        '-> Pointer sur l'objet parent
        TypeObject = Entry(1, Entry(1, ObjCours.MasterAlign, "-"), "|")
        NomObject = Entry(2, Entry(1, ObjCours.MasterAlign, "-"), "|")
        If UCase$(TypeObject) = "CDR" Then
            Set aObject = aSection.Cadres(UCase$(NomObject))
        Else
            Set aObject = aSection.Bmps(UCase$(NomObject))
        End If
        '-> Supprimer dans l'objet parent la r�f�nce. car on pourrait changer d'alignement (Page)
        i = GetEntryIndex(aObject.SlaveAlign, Key, "-")
        aObject.SlaveAlign = DeleteEntry(aObject.SlaveAlign, i, "-")
        Set aObject = Nothing
    End If
End If

'-> Suppression de l'alignement
If Me.Option2.Value Then
    ObjCours.Locked = False
    ObjCours.MasterAlign = ""
    ObjCours.AlignX = 0
    ObjCours.AlignY = 0
    ObjCours.MasterIndexAlign = 0
    Unload Me
    Exit Sub
End If

'-> Lock de l'alignement
If Me.Check1.Value = 1 Then
    ObjCours.Locked = True
Else
    ObjCours.Locked = False
    ObjCours.MasterAlign = ""
    ObjCours.AlignX = 0
    ObjCours.AlignY = 0
    ObjCours.MasterIndexAlign = 0
End If

If Me.Option1(0) Then
    '-> Aligner l'objet physique
    AlignOnPage Me.RetourAlign, ObjCours
    '-> Mettre � jour l'objet objcours
    ObjCours.MasterAlign = "PAGE-" & Me.RetourAlign & "-" & Me.Image1(0).Tag
ElseIf Me.Option1(1) Then
    '-> ALigner l'objet
    KeyMaster = AlignOnObject(ObjCours, Me.List1.Text, Me.RetourAlign, CSng(Convert(Me.Align(0).Caption)), CSng(Convert(Me.Align(1).Caption)))
    '-> Mettre � jour l'objet en cours
    ObjCours.MasterAlign = KeyMaster & "-" & Me.RetourAlign & "-" & Me.Image1(1).Tag
    ObjCours.AlignX = CSng(Convert(Me.Align(0).Caption))
    ObjCours.AlignY = CSng(Convert(Me.Align(1).Caption))
    '-> Pointer sur l'objet de r�f�rence
    If aSection.GetBmpExist(Me.List1.Text) Then
        '-> L'objet est un Bmp
        Set MyObjectRefObj = aSection.Bmps(Me.List1.Text)
    ElseIf aSection.GetCadreExist(Me.List1.Text) Then
        '-> L'objet est un cadre
        Set MyObjectRefObj = aSection.Cadres(UCase$(Me.List1.Text))
    Else
        Set aLb = Libelles("MESSAGE")
        MsgBox aLb.GetCaption(32), vbCritical + vbOKOnly, aLb.GetToolTip(32)
        Set aSection = Nothing
        Unload Me
        Exit Sub
    End If
    '-> Mettre � jour l'objet Parent
    If TypeOf ObjCours Is Cadre Then
        MyObjectRefObj.SlaveAlign = AddEntryToMatrice(MyObjectRefObj.SlaveAlign, "-", "CDR|" & ObjCours.Nom)
    ElseIf TypeOf ObjCours Is ImageObj Then
        MyObjectRefObj.SlaveAlign = AddEntryToMatrice(MyObjectRefObj.SlaveAlign, "-", "BMP|" & ObjCours.Nom)
    End If
    ObjCours.MasterIndexAlign = Me.RetourAlign
End If

'-> D�placer les esclaves de l'objet que l'on vient d'aligner
ObjCours.MoveSlave

'-> D�charger la feuille
Unload Me

Set aLb = Nothing
Set aSection = Nothing
Set MyObjectRefObj = Nothing

End Sub



Private Sub Option1_Click(Index As Integer)

EnableOption1 True, Index
Me.RetourAlign = CInt(Me.Option1(Index).Tag)


End Sub

Private Sub EnableOption1(ByVal Value As Boolean, ByVal Index As Integer)

If Index = 0 Then
Else
    Value = Not Value
End If

Me.Image1(0).Enabled = Value
Me.Label1.Enabled = Not Value
Me.Label2.Enabled = Not Value
Me.List1.Enabled = Not Value
Me.Image1(1).Enabled = Not Value
Me.Label3.Enabled = Not Value
Me.Label4.Enabled = Not Value
Me.Align(0).Enabled = Not Value
Me.Align(1).Enabled = Not Value

End Sub

Private Sub GestLien()

'---> Cette proc�dure � pour but d'alimenter la liste d�roulante des objets _
r�f�rences en v�rifiant qu'il n'y ait pas de lien crois�

Dim aCadre As Cadre
Dim aBmp As ImageObj
Dim aSection As Section
Dim ListObj() As String
Dim OkObj() As Boolean
Dim nEntries As Integer
Dim Continue As Boolean
Dim aObject
Dim TypeObject As String
Dim NomObject As String

'-> Variables utilis�es pour l'analyse r�cursive des Esclaves
Dim ListeSlaves() As String
Dim AnalyseSlaves() As Boolean
Dim nSlaves As Integer
Dim Slaves As String
Dim i As Integer, j As Integer

'-> Index de la prochaine dimension
nEntries = 1
nSlaves = 1
'-> Continuer la recherche
Continue = True

'-> Pointer sur la section parente
Set aSection = Maquettes("MAQUETTE").Sections(UCase$(ObjCours.SectionName))

'---> Analyse de l'ensemble des objets pouvant servir potentiellement de _
r�f�rence � l'alignement de l'objet en cours

'-> Cr�ation de la liste des cadres
For Each aCadre In aSection.Cadres
    '-> Ne pas ajouter si c'est l'objet en cours
    If UCase$(aCadre.Nom) = UCase$(ObjCours.Nom) Then
    Else
        ReDim Preserve ListObj(1 To nEntries)
        ListObj(nEntries) = "CDR|" & aCadre.Nom
        nEntries = nEntries + 1
    End If
Next 'pour tous les cadres
'-> Cr�ation de la liste des Bmps
For Each aBmp In aSection.Bmps
    '-> Ne pas ajouter si c'est l'objet en cours
    If UCase$(aBmp.Nom) = UCase$(ObjCours.Nom) Then
    Else
        ReDim Preserve ListObj(1 To nEntries)
        ListObj(nEntries) = "BMP|" & aBmp.Nom
        nEntries = nEntries + 1
    End If
Next 'pour tous les bmps

'---> Cr�ation de la liste des escalves de l'objet en cours

Set aObject = ObjCours
Do While Continue
    Slaves = aObject.SlaveAlign
    If Trim(Slaves) <> "" Then
        For i = 1 To NumEntries(Slaves, "-")
            '-> Ajouter dans la liste des esclaves
            ReDim Preserve ListeSlaves(1 To nSlaves)
            ReDim Preserve AnalyseSlaves(1 To nSlaves)
            ListeSlaves(nSlaves) = Entry(i, Slaves, "-")
            AnalyseSlaves(nSlaves) = False
            nSlaves = nSlaves + 1
        Next
    End If
    '-> indiquer que l'on ne continue pas la recherche
    Continue = False
    '-> Analyser les enfants un par un
    For i = 1 To nSlaves - 1
        If Not AnalyseSlaves(i) Then
            '-> Cet objet n'a pas encore �t� analys� en faire l'objet en cours d'analyse
            TypeObject = Entry(1, ListeSlaves(i), "|")
            NomObject = Entry(2, ListeSlaves(i), "|")
            If UCase$(TypeObject) = "CDR" Then
                Set aObject = aSection.Cadres(UCase$(NomObject))
            ElseIf UCase$(TypeObject) = "BMP" Then
                Set aObject = aSection.Bmps(UCase$(NomObject))
            End If
            '-> Indiquer que l'objet en cours est analys�
            AnalyseSlaves(i) = True
            '-> Indiquer que l'on continue l'analyse
            Continue = True
            '-> Sortir de la boucle
            Exit For
        End If
    Next 'Analyse de tous les esclaves
Loop
    
'-> Redimensionner la liste des Etats (False/True). Par d�faut ils sont tous Ok
ReDim OkObj(1 To nEntries)
For i = 1 To nEntries - 1
    OkObj(i) = True
Next

'-> Maintenant ListeSlaves contient la liste de tous les esclaves. Ne pas afficher tous ces _
objets l� comme base possible de r�f�rence
For i = 1 To nSlaves - 1
    For j = 1 To nEntries - 1
        '-> Si il y a concordance des noms : -> Rejet de l'objet
        If UCase$(ListObj(j)) = UCase$(ListeSlaves(i)) Then OkObj(j) = False
    Next
Next
    
'-> Cr�er la liste avec tous les objets non rejett�s
For i = 1 To nEntries - 1
    If OkObj(i) Then
        Me.List1.AddItem Entry(2, ListObj(i), "|")
    End If
Next

'-> Tester s'il y a des entr�es
If Me.List1.ListCount = 0 Then
    EnableOption1 True, 0
    Me.Option1(1).Enabled = False
Else
    Me.List1.Selected(0) = True
End If


Set aCadre = Nothing
Set aBmp = Nothing
Set aSection = Nothing

End Sub

