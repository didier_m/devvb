VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Application"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'-> Nom de l'applicatif : peut �tre une app ou un client
Public NameApp As String
'-> Lecture de la cl� Path
Public PathApp As String
'-> Lecture du propath maquettes Char
Public PathMqt As String
'-> Lecture des logiciels associ�s
Public Progiciel As String
'-> Doit-il �tre affich�
Public Display As Boolean
'-> Liste des rubriques associ�es
Public Rubriques As String
Public nRubriques As Integer
Private pRubriques() As String
'-> Indique si le client est un applicatif
Public IsApp As Boolean
'->Indique le code du client
Public CodeCLient As String

Public Sub CreateRubriques()

Dim i As Integer
Dim Def As String

'-> Fonction qui �clate les diff�rentes rubriques
nRubriques = NumEntries(Rubriques, "|")
ReDim pRubriques(1 To 2, 1 To nRubriques)

For i = 1 To nRubriques
    Def = Entry(i, Rubriques, "|")
    pRubriques(1, i) = Entry(1, Def, "@")
    pRubriques(2, i) = Entry(2, Def, "@")
Next

End Sub

Public Function GetRubriquesCode(ByVal Index As Integer) As String

GetRubriquesCode = pRubriques(1, Index)

End Function

Public Function GetRubriquesName(ByVal Index As Integer) As String

GetRubriquesName = pRubriques(2, Index)

End Function

