VERSION 5.00
Begin VB.Form frmMenu 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   5565
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   5565
   Visible         =   0   'False
   Begin VB.Menu mnucolonne 
      Caption         =   "Colonnes"
      Begin VB.Menu mnuModifLargeurCol 
         Caption         =   ""
      End
      Begin VB.Menu Sep23 
         Caption         =   "-"
      End
      Begin VB.Menu mnuInsertCol 
         Caption         =   ""
      End
      Begin VB.Menu mnuDupCol 
         Caption         =   ""
         Visible         =   0   'False
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelCol 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelContenuCol 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep12 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFormatCol 
         Caption         =   ""
      End
   End
   Begin VB.Menu mnuLigne 
      Caption         =   "Lignes"
      Begin VB.Menu mnuModifHauteurLig 
         Caption         =   ""
      End
      Begin VB.Menu sep90 
         Caption         =   "-"
      End
      Begin VB.Menu mnuinsertLigne 
         Caption         =   ""
      End
      Begin VB.Menu mnuDupLigne 
         Caption         =   ""
         Visible         =   0   'False
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelLigne 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDelContenuLig 
         Caption         =   ""
      End
      Begin VB.Menu mnuSep56 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFormatLig 
         Caption         =   ""
      End
      Begin VB.Menu sep1245 
         Caption         =   "-"
      End
      Begin VB.Menu mnuVarLig 
         Caption         =   ""
      End
      Begin VB.Menu sep14587 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExportExcel 
         Caption         =   "Traiter la ligne dans Excel"
      End
      Begin VB.Menu mnuSep1243 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAccesDet 
         Caption         =   "Param�trage de l'acc�s au d�tail"
      End
   End
   Begin VB.Menu mnuCarret 
      Caption         =   "Carret"
      Begin VB.Menu mnuDelContenuBlock 
         Caption         =   ""
      End
      Begin VB.Menu mnuFormatCarret 
         Caption         =   ""
      End
   End
   Begin VB.Menu mnuTableau 
      Caption         =   "Tableau"
      Begin VB.Menu mnuEditTableau 
         Caption         =   ""
      End
   End
   Begin VB.Menu mnuCellule 
      Caption         =   "Cellules"
      Begin VB.Menu mnuformatCell 
         Caption         =   ""
      End
      Begin VB.Menu mnuCopyCell 
         Caption         =   ""
      End
      Begin VB.Menu mnuPasteCell 
         Caption         =   ""
      End
   End
End
Attribute VB_Name = "frmMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

Dim aLb As Libelle

Set aLb = Libelles("FRMMENU")

Me.mnuModifLargeurCol.Caption = aLb.GetCaption(21)
Me.mnuModifHauteurLig.Caption = aLb.GetCaption(22)
Me.mnuInsertCol.Caption = aLb.GetCaption(1)
Me.mnuDupCol.Caption = aLb.GetCaption(3)
Me.mnuDelCol.Caption = aLb.GetCaption(2)

Me.mnuinsertLigne.Caption = aLb.GetCaption(4)
Me.mnuDupLigne.Caption = aLb.GetCaption(6)
Me.mnuDelLigne.Caption = aLb.GetCaption(5)

Me.mnuDelContenuCol.Caption = aLb.GetCaption(9)
Me.mnuDelContenuLig.Caption = aLb.GetCaption(10)
Me.mnuDelContenuBlock.Caption = aLb.GetCaption(13)

Me.mnuEditTableau.Caption = aLb.GetCaption(14)

Me.mnuformatCell.Caption = aLb.GetCaption(15)
Me.mnuFormatCarret.Caption = aLb.GetCaption(16)
Me.mnuFormatLig.Caption = aLb.GetCaption(17)
Me.mnuFormatCol.Caption = aLb.GetCaption(18)

Me.mnuCopyCell.Caption = aLb.GetCaption(19)
Me.mnuPasteCell.Caption = aLb.GetCaption(20)

Me.mnuVarLig.Caption = aLb.GetCaption(25)

Set aLb = Nothing

End Sub

Private Sub mnuAccesDet_Click()
RetourMenu = 998
End Sub

Private Sub mnuCopyCell_Click()

RetourMenu = 12

End Sub

Private Sub mnuDelCol_Click()

'---> Proc�dure qui supprime une colonne
RetourMenu = 2

End Sub

Private Sub mnuDelContenuBlock_Click()

'---> Effacer le contenu du block
RetourMenu = 9

End Sub

Private Sub mnuDelContenuCol_Click()

'---> Proc�dure qui supprime le contenu d'une colonne
RetourMenu = 3

End Sub

Private Sub mnuDelContenuLig_Click()

'---> Procedure qui supprime le contenu d'une ligne
RetourMenu = 7

End Sub

Private Sub mnuDelLigne_Click()

'---> Proc�dure qui supprime une ligne
RetourMenu = 6

End Sub

Private Sub mnuEditTableau_Click()

'---> Procedure qui edite un block de ligne
RetourMenu = 11

End Sub

Private Sub mnuExportExcel_Click()
RetourMenu = 999
End Sub

Private Sub mnuFormatCarret_Click()
RetourMenu = 10
End Sub

Private Sub mnuFormatCell_Click()

'-> Faire le setting de la cellule
'Set frmFormatCell.aCell = aForm.BlockCours.Cellules("L" & Entry(1, CellAdd, "|") & "C" & Entry(2, CellAdd, "|"))
'frmFormatCell.Action = 0
'frmFormatCell.Show vbModal
RetourMenu = 11

End Sub


Private Sub mnuFormatCol_Click()

RetourMenu = 4

End Sub

Public Sub mnuFormatLig_Click()

RetourMenu = 8

End Sub

Private Sub mnuInsertCol_Click()

'---> Proc�dure qui insert une colonne
RetourMenu = 1


End Sub

Private Sub mnuinsertLigne_Click()

'-> Proc�dure qui insert une ligne
RetourMenu = 5

End Sub

Private Sub mnuModifHauteurLig_Click()

RetourMenu = 15

End Sub

Private Sub mnuModifLargeurCol_Click()

RetourMenu = 14

End Sub

Private Sub mnuPasteCell_Click()
    
RetourMenu = 13
    
End Sub

Private Sub mnuVarLig_Click()

RetourMenu = 99

End Sub
