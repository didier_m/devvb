VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MsComCtl.ocx"
Begin VB.Form frmListeClient 
   BorderStyle     =   5  'Sizable ToolWindow
   ClientHeight    =   6105
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   8520
   ControlBox      =   0   'False
   Icon            =   "TM-ListeClient.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   407
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   568
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView ListView1 
      Height          =   3015
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5055
      _ExtentX        =   8916
      _ExtentY        =   5318
      View            =   2
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ColHdrIcons     =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4560
      Top             =   2640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-ListeClient.frx":08CA
            Key             =   "App"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-ListeClient.frx":15A4
            Key             =   "User"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "TM-ListeClient.frx":18BE
            Key             =   "Exit"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmListeClient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Form_Load()

Dim aLb As Libelle
Dim IniPath As String
Dim i As Integer, j As Integer
Dim lpbuffer As String
Dim Res As Long
Dim TempFileName As String
Dim NomSection As String
Dim lpKey As String
Dim KeyName As String
Dim KeyValue As String
Dim Progiciel As Boolean
Dim GeneralPath As String
Dim Lecteur As String
Dim Version As String
Dim ListeProgiciels() As String
Dim FindApp As Boolean
Dim aApp As Application

Set aLb = Libelles("FRMLISTECLIENT")
Me.Caption = aLb.GetCaption(1)

If Not IsLoadedPath Then
    
    '-> Cr�ation des propaths
    If InStr(1, UCase$(App.Path), "\EMILIEGUI\SRC") <> 0 Then
        IniPath = InitStringPath(1)
    Else
        CreatePath IniPath
    End If
    
    '-> R�cup�ration de l'index de la racine
    i = GetEntryIndex(App.Path, "DEALPRO", "\")
    '-> R�cup�ration du nom de la version
    Version = Entry(i + 2, App.Path, "\")
    '-> R�cup�ration du nom de lecteur logique
    i = InStr(1, UCase$(App.Path), "DEALPRO")
    Lecteur = Mid$(App.Path, 1, i - 2)
    
    '-> Reconstruire le chemin
    If Dir$(App.Path & "\TurboMaq.ini") <> "" Then
        IniPath = App.Path & "\TurboMaq.ini"
    Else
        IniPath = IniPath & "\TurboMaq.ini"
    End If
            
    '-> Quitter si on ne trouve pas ce fichier
    If Dir$(IniPath) = "" Then
        MsgBox aLb.GetCaption(2) & Chr(13) & IniPath, vbCritical + vbOKOnly, aLb.GetToolTip(2)
        '-> Quitter
        End
    End If
        
    '-> PB WIN95 : L'utilisation des API des fichiers ini pose des pb pour _
    les r�pertoires qui sont en lecture seule; En effet , lorsqu'il lit _
    un fichier INI, WIN95 lock el fichier en �crivant dedans : donc -> Violation _
    des privil�ges
    
    '-> Effectuer une copie du fichier ini dans le r�pertoire tempo
    TempFileName = GetTempFileNameVB("INI")
    Res = CopyFile(IniPath, TempFileName, 0)
    
    '-> R�cup�ration des param�trages g�n�raux : Lecture de la section Param
    
    '-> Doit on afficher ou pas les logiciels ????
    lpKey = Space$(100)
    Res = GetPrivateProfileString("Param", "Progiciels", "", lpKey, Len(lpKey), TempFileName)
    If lpKey = "" Then
        '-> Impossible de trouver la cl� ou la section : Quitter
        MsgBox aLb.GetCaption(4) & Chr(13) & IniPath, vbCritical + vbOKOnly, aLb.GetToolTip(4)
        Set aLb = Nothing
        Unload Me
    End If
    '-> R�cup�ration de la valeur
    If UCase$(Mid$(lpKey, 1, 3)) = "OUI" Then
        Progiciel = True
    Else
        Progiciel = False
    End If
        
    '-> R�cup�ration de la liste des logiciels
    lpKey = Space(500)
    Res = GetPrivateProfileString("Progiciels", "App", "", lpKey, Len(lpKey), TempFileName)
    If lpKey = "" Then
        '-> Impossible de trouver la cl� ou la section : Quitter
        MsgBox aLb.GetCaption(6), vbCritical + vbOKOnly, aLb.GetToolTip(6)
        Set aLb = Nothing
        End
    End If
    
    '-> Cr�ation de la liste des logiciels
    lpKey = Mid$(lpKey, 1, Res)
    ReDim Preserve ListeProgiciels(1 To NumEntries(lpKey, "|"))
    For i = 1 To UBound(ListeProgiciels)
        ListeProgiciels(i) = Entry(i, lpKey, "|")
    Next
    
    '-> Lecture du path g�n�ral
    lpKey = Space$(100)
    Res = GetPrivateProfileString("Param", "Path", "", lpKey, Len(lpKey), TempFileName)
    If lpKey = "" Then
        '-> Impossible de trouver la cl� ou la section : Quitter
        MsgBox aLb.GetCaption(5) & Chr(13) & IniPath, vbCritical + vbOKOnly, aLb.GetToolTip(5)
        End
    End If
    '-> R�cup�ration de la valeur
    GeneralPath = Mid$(lpKey, 1, Res)
        
    '-> Remplacer les noms logiques $Lecteur$ et $version$ par leur valeur
    GeneralPath = Replace(GeneralPath, "$LECTEUR$", Lecteur)
    GeneralPath = Replace(GeneralPath, "$VERSION$", Version)
    
    '-> Sauvegarder le propath g�n�ral
    ProPath = GeneralPath
                
    '-> Cr�ation de l'applicatif param
    Set aApp = New Application
    aApp.CodeCLient = "PARAM"
    aApp.IsApp = True
    aApp.NameApp = "Param�trage g�n�ral"
    aApp.PathApp = ProPath
    Apps.Add aApp, "PARAM"
    Set aApp = Nothing
    
    '-> R�cup�ration de toutes les sections
    lpbuffer = Space$(32765)
    Res = GetPrivateProfileString(vbNullString, "", "Erreur", lpbuffer, Len(lpbuffer), TempFileName)
    lpbuffer = Mid$(lpbuffer, 1, Res - 1)
    If lpbuffer = "Erreur" Then GoTo LoadSectionErr
    '-> Charger le tableau des sections
    nEntries = NumEntries(lpbuffer, Chr(0))
    For i = 1 To nEntries
        '-> R�cup�rer le nom de la section
        NomSection = Entry(i, lpbuffer, Chr(0))
        '-> Pb WIN 95 : Gestion des blancs
        If Trim(NomSection) = "" Then Exit For
        '-> Selon la cl� que l'on est en train de lire
        If UCase$(NomSection) = "PARAM" Or UCase$(NomSection) = "PROGICIELS" Then
        Else
            For j = 1 To UBound(ListeProgiciels)
                If UCase$(NomSection) = UCase$(ListeProgiciels(j)) Then
                    GetPathApp NomSection, TempFileName, Version, Lecteur, Progiciel, True
                    FindApp = True
                    Exit For
                Else
                    FindApp = False
                End If
            Next 'Pour tous les progiciels
            If Not FindApp Then
                '-> On est dans le cas d'un client
                GetPathApp NomSection, TempFileName, Version, Lecteur, True, False
            End If
        End If
    
    Next 'Pour toutes les sections du fichier ini
    
    '-> supprimer le fichier temporaire
    If Dir$(TempFileName) <> "" Then Kill TempFileName
    
    '-> S�lectionner le premier
    If Me.ListView1.ListItems.Count <> 0 Then Me.ListView1.ListItems(1).Selected = True
    
    '-> Indiquer que le load est fait
    IsLoadedPath = True
    
Else

    '-> Analyser les applicatifs
    For Each aApp In Apps
        If aApp.Display Then
            If aApp.IsApp Then
                Me.ListView1.ListItems.Add , "CL|" & UCase$(aApp.CodeCLient), aApp.CodeCLient & " - " & aApp.NameApp, "App", "App"
            Else
                Me.ListView1.ListItems.Add , "CL|" & UCase$(aApp.CodeCLient), aApp.CodeCLient & " - " & aApp.NameApp, "User", "User"
            End If
        End If
    Next
    '-> S�lectionner le premier
    If Me.ListView1.ListItems.Count <> 0 Then Me.ListView1.ListItems(1).Selected = True
End If

'-> Ajouter dans tous les cas l'option de FIN
Me.ListView1.ListItems.Add , "EXIT", aLb.GetCaption(7), , "Exit"

'-> Liberer le pointeur
Set aLb = Nothing

Exit Sub

LoadSectionErr:
    MsgBox aLb.GetCaption(3), vbCritical + vbOKOnly, aLb.GetToolTip(3)
    Set aLb = Nothing

End Sub

Private Sub GetPathApp(ByVal NomPath As String, TempFileName As String, Version As String, Lecteur As String, Display As Boolean, IsApp As Boolean)

'-> Cette proc�dure cr�er une nouvelle instance App et _
effectue le setting des propri�t�s

Dim lpKey As String
Dim aApp As Application
Dim aTempFile As String
Dim Res As Long

'-> Cr�ation d'une nouvelle instance
Set aApp = New Application

aApp.CodeCLient = NomPath

'-> Recherche de la cl� Libell�
lpKey = Space$(100)
Res = GetPrivateProfileString(NomPath, "Libel", "", lpKey, Len(lpKey), TempFileName)
If Res <> 0 Then aApp.NameApp = Mid$(lpKey, 1, Res)

'-> Recherche du path associ�
lpKey = Space$(100)
Res = GetPrivateProfileString(NomPath, "Path", "", lpKey, Len(lpKey), TempFileName)
If Res <> 0 Then
    '-> Faire ici les remplacements
    lpKey = Mid$(lpKey, 1, Res)
    lpKey = ReplaceLogicalName(lpKey, NomPath, Lecteur, Version)
    aApp.PathApp = lpKey
End If


'-> Recherche du path associ� aux maquettes
lpKey = Space$(100)
Res = GetPrivateProfileString(NomPath, "Mqt", "", lpKey, Len(lpKey), TempFileName)
If Res <> 0 Then
    '-> Faire ici les remplacements
    lpKey = Mid$(lpKey, 1, Res)
    lpKey = ReplaceLogicalName(lpKey, NomPath, Lecteur, Version)
    aApp.PathMqt = lpKey
End If

'-> Recherche des progiciels associ�s
lpKey = Space$(1000)
Res = GetPrivateProfileString(NomPath, "Progiciels", "", lpKey, Len(lpKey), TempFileName)
If Res <> 0 Then
    '-> Test si on doit chercher le fichier Param.ini
    lpKey = Mid$(lpKey, 1, Res)
    If Entry(2, UCase$(Entry(NumEntries(lpKey, "\"), lpKey, "\")), ".") = "INI" Then
        '-> Remplacer les noms logiques par leur valeur
        lpKey = ReplaceLogicalName(lpKey, NomPath, Lecteur, Version)
        '-> Obtenir un nom de fichier temporaire
        aTempFile = GetTempFileNameVB("TMP")
        '-> Faire une copie
        CopyFile lpKey, aTempFile, 0
        '-> Lire la cl� $progiciels$ du fichier sp�cifi�
        lpKey = Space$(100)
        Res = GetPrivateProfileString("LOGICAL", "$Progiciel$", "", lpKey, Len(lpKey), aTempFile)
        If Res <> 0 Then
            lpKey = Mid$(lpKey, 1, Res)
            aApp.Progiciel = lpKey
        End If
        '-> Supprimer le fichier temporaire
        If Dir$(aTempFile) <> "" Then Kill aTempFile
    Else
        aApp.Progiciel = lpKey
    End If
End If

'-> Recherche des rubriques associ�es
lpKey = Space$(2000)
Res = GetPrivateProfileString(NomPath, "Rubriques-" & CodeLangue, "", lpKey, Len(lpKey), TempFileName)
If Res <> 0 Then
    lpKey = Mid$(lpKey, 1, Res)
    aApp.Rubriques = lpKey
    aApp.CreateRubriques
End If

'-> Ajouter le nouvel objet dans la collection des clients existants
Apps.Add aApp, UCase$(NomPath)
aApp.Display = Display
aApp.IsApp = IsApp

'-> Ajouter le client � la liste si necessaire
If Display Then
    If aApp.IsApp Then
        Me.ListView1.ListItems.Add , "CL|" & UCase$(aApp.CodeCLient), UCase$(aApp.CodeCLient) & " - " & aApp.NameApp, "App", "App"
    Else
        '-> ajouter le code du client
        Me.ListView1.ListItems.Add , "CL|" & UCase$(aApp.CodeCLient), UCase$(aApp.CodeCLient) & " - " & aApp.NameApp, "User", "User"
        '-> Ajouter la liste des applicatifs autoris�s si necessaire
        
    End If
End If
'-> Lib�rer le pointeur
Set aApp = Nothing

End Sub

Private Function ReplaceLogicalName(lpKey As String, Ident As String, Lecteur As String, Version As String) As String

lpKey = Replace(lpKey, "$LECTEUR$", Lecteur)
lpKey = Replace(lpKey, "$IDENT$", Ident)
lpKey = Replace(lpKey, "$VERSION$", Version)

ReplaceLogicalName = lpKey

End Function

Private Sub Form_Resize()

On Error Resume Next

Dim Rect1 As Rect
Dim Res As Long

Res = GetClientRect(Me.hwnd, Rect1)

Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = Rect1.Right - Rect1.Left
Me.ListView1.Height = Rect1.Bottom - Rect1.Top

End Sub


Private Function GetRubrique(aNode As Node, aApp As Application, ByVal KeyValue As String)

'---> Cette fonction cr�er toutes les rubriques propres � un applicatif , analyse son r�pertoire _
et effectue l'affectation

Dim i As Integer

'-> Ajouter  toutes rubriques
For i = 1 To aApp.nRubriques
    frmSample.TreeSamples.Nodes.Add aNode.Key, 4, KeyValue & UCase$(aApp.GetRubriquesCode(i)), aApp.GetRubriquesName(i), "Fonction"
Next 'Pour toutes les rubriques


End Function

Private Sub DispatchFile(aApp As Application, ByVal IsCLient As Boolean)

'---> Cette fonction est charg�e de trier les �ditions par rubrique

Dim aFileName As String
Dim lpKey1 As String
Dim lpKey2 As String
Dim Res As Long
Dim aNode As Node

On Error Resume Next

aFileName = Dir$(aApp.PathApp & "*.maqgui")
Do While aFileName <> ""
    '-> Lecture de la cle [Progiciel] dans chaque maquette
    lpKey1 = Space(100)
    Res = GetPrivateProfileString("PROGICIEL", "\Prog", "", lpKey1, Len(lpKey1), aApp.PathApp & aFileName)
    If Res = 0 Then
    Else
        lpKey1 = Mid$(lpKey1, 1, Res)
        '-> R�cup�ration du nom de la rubrique
        lpKey2 = Space(100)
        Res = GetPrivateProfileString("PROGICIEL", "\RUB", "", lpKey2, Len(lpKey2), aApp.PathApp & aFileName)
        If Res = 0 Then Exit Sub
        lpKey2 = Mid$(lpKey2, 1, Res)
        If IsCLient Then
            Set aNode = frmSample.TreeSamples.Nodes.Add("CLI|" & UCase$(lpKey1) & "|" & UCase$(lpKey2), 4, "MAQ|" & aApp.PathApp & aFileName, Entry(1, aFileName, "."), "Doc")
            aNode.Tag = aApp.PathApp & aFileName & "   " & aNode.Parent.Key
        Else
            Set aNode = frmSample.TreeSamples.Nodes.Add("ERP|" & UCase$(lpKey1) & "|" & UCase$(lpKey2), 4, "MAQ|" & aApp.PathApp & aFileName, Entry(1, aFileName, "."), "Doc")
            aNode.Tag = aApp.PathApp & aFileName & "   " & aNode.Parent.Key
        End If
    End If
    '-> Ajouter le node dans le treeview
    
    '-> Lire le fichier suivant
    aFileName = Dir
Loop


End Sub

Private Sub ListView1_DblClick()

Dim aNode As Node
Dim aLb As Libelle
Dim FileName As String, aFile As String
Dim i As Integer
Dim NomApp As String
Dim aApp As Application
Dim aApp2 As Application
Dim Temp As String
Dim Rep

'---> Il faut cr�er la feuille frmSample

On Error Resume Next

'-> Pointer sur le libell�
Set aLb = Libelles("FRMSAMPLE")

'-> Tester si on quitte
If Me.ListView1.SelectedItem.Key = "EXIT" Then
    Set aLb = Libelles("FRMLISTECLIENT")
    Rep = MsgBox(aLb.GetCaption(7), vbQuestion + vbYesNo, aLb.GetToolTip(7))
    If Rep = vbYes Then
        End
    Else
        Exit Sub
    End If
End If

'-> Ajouter les applicatifs autoris�s : pointer sur le client
NomApp = Trim(Entry(2, Me.ListView1.SelectedItem.Key, "|"))
Set aApp = Apps(UCase$(NomApp))

If aApp.IsApp Then

    '************************************************
    '* Ajout des nodes propres aux applicatifs DEAL *
    '************************************************
    
    '-> Ajouter dans tous les cas le node BUREAU
    Set aNode = frmSample.TreeSamples.Nodes.Add(, , "BUREAU", aLb.GetCaption(1), "Erp")
    aNode.Expanded = True
    aNode.Tag = aApp.PathApp

    '-> Ajouter le sous node Appliquer un modele
    Set aNode = frmSample.TreeSamples.Nodes.Add("BUREAU", 4, "MODELE", aLb.GetCaption(2), "Modele")

    '-> Ajouter le sous node Document vide
    Set aNode = frmSample.TreeSamples.Nodes.Add("MODELE", 4, "MOD|BLANK|", aLb.GetCaption(5), "DocVide")

    '-> Faire la liste des documents Mod�les de DEAL qui se trouve dans propath Emilie
    FileName = Dir$(ProPath & "*.drm", vbNormal)
    Do While FileName <> ""
        frmSample.List1.AddItem FileName
        FileName = Dir
    Loop
            
    '-> Afficher les documents modeles
    If frmSample.List1.ListCount <> 0 Then
        For i = 0 To frmSample.List1.ListCount - 1
            Set aNode = frmSample.TreeSamples.Nodes.Add("MODELE", 4, "MOD|" & ProPath & frmSample.List1.List(i), Entry(1, Entry(NumEntries(frmSample.List1.List(i), "\"), frmSample.List1.List(i), "\"), "."), "Mod")
            aNode.Tag = Entry(2, aNode.Key, "|")
        Next
    End If
    
    '-> vider la feuille
    frmSample.List1.Clear
    
    For i = 1 To NumEntries(aApp.Progiciel, ",")
        
        '******************************************************
        '* Ajout de la liste des progiciels pour le node DEAL *
        '******************************************************
        
        '-> R�cup�rer le nom du progiciel
        FileName = Entry(i, aApp.Progiciel, ",")
        '-> pointer sur l'application
        Set aApp2 = Apps(UCase$(FileName))
        '-> Ajouter le node progiciel
        Set aNode = frmSample.TreeSamples.Nodes.Add("BUREAU", 4, "ERP|" & UCase$(FileName), aApp2.CodeCLient & " - " & aApp2.NameApp, "App")
        aNode.Tag = aApp2.PathApp
        '-> Ajouter toutes les rubriques
        GetRubrique aNode, aApp2, aNode.Key & "|"
        '-> Lister tous les fichiers par rubriques
        DispatchFile aApp2, False
        '-> Ajouter le node MODELE par applicatif
        frmSample.TreeSamples.Nodes.Add aNode.Key, 4, aNode.Key & "|MODELE", aLb.GetCaption(2), "Modele"
        Temp = aNode.Key & "|MODELE"
        '-> Faire la liste des documents Mod�les de chaque applicatif
        frmSample.List1.Clear
        aFile = Dir$(aApp2.PathApp & "*.drm", vbNormal)
        Do While aFile <> ""
            frmSample.List1.AddItem aFile
            aFile = Dir$
        Loop
        If frmSample.List1.ListCount <> 0 Then
            For j = 0 To frmSample.List1.ListCount - 1
                Set aNode = frmSample.TreeSamples.Nodes.Add(Temp, 4, "MOD|" & aApp2.PathApp & frmSample.List1.List(j), Entry(1, Entry(NumEntries(frmSample.List1.List(j), "\"), frmSample.List1.List(j), "\"), "."), "Mod")
                aNode.Tag = Entry(2, aNode.Key, "|")
            Next
        End If
                
    Next 'Pour tous les progiciels
Else
    '********************************************************
    '* Ajout de la liste des progiciels pour le node Client *
    '********************************************************
                
    Set aNode = frmSample.TreeSamples.Nodes.Add(, , "CLIENT", aLb.GetCaption(6) & " " & aApp.NameApp, "Erp")
    '-> Remplacer la cle $PROGICIEL par le reste
    
    aNode.Tag = aApp.PathApp
    
    '-> Ajouter le sous node Appliquer un modele
    Set aNode = frmSample.TreeSamples.Nodes.Add("CLIENT", 4, "MODELE2", aLb.GetCaption(2), "Modele")
        
    '-> V�rifier si en premier il y a un fichier nomm� client.ico dans le r�pertoire du client
    If Dir$(aApp.PathApp & "Client.ico") <> "" Then
        frmSample.ImageList1.ListImages.Add , "Client", LoadPicture(aApp.PathApp & "Client.ico")
        aNode.Image = "Client"
    End If
    
    '-> Si le client a des applicatifs autoris�s :
    If aApp.Progiciel <> "" Then
        
        For i = 1 To NumEntries(aApp.Progiciel, ",")
        
            '-> R�cup�ration du nom de l'applicatif
            FileName = Entry(i, aApp.Progiciel, ",")
            
            '-> pointer sur l'application
            Set aApp2 = Apps(UCase$(FileName))
        
            '-> Ajouter le node de chaque applicatif sous le node client
            Set aNode = frmSample.TreeSamples.Nodes.Add("CLIENT", 4, "CLI|" & UCase$(FileName), aApp2.CodeCLient & "-" & aApp2.NameApp, "App")
            aNode.Tag = aApp.PathApp
        
            '-> Ajouter les sous rubriques
            GetRubrique aNode, aApp2, aNode.Key & "|"
            
        Next 'Pour tous les applicatifs
        
        '-> Ajouter le modele Vide pour le client
        frmSample.TreeSamples.Nodes.Add "MODELE2", 4, "MOD|BLANK2|", aLb.GetCaption(5), "DocVide"
        
        '-> Faire la liste des documents Mod�les de DEAL qui se trouve dans propath
        FileName = Dir$(aApp.PathApp & "*.drm", vbNormal)
        Do While FileName <> ""
            frmSample.List1.AddItem FileName
            FileName = Dir
        Loop
        If frmSample.List1.ListCount <> 0 Then
            For i = 0 To frmSample.List1.ListCount - 1
                Set aNode = frmSample.TreeSamples.Nodes.Add("MODELE2", 4, "MOD|" & aApp.PathApp & frmSample.List1.List(i), Entry(1, Entry(NumEntries(frmSample.List1.List(i), "\"), frmSample.List1.List(i), "\"), "."), "Mod")
                aNode.Tag = Entry(2, aNode.Key, "|")
            Next
        End If
        
        '-> Lister tous les fichiers par rubriques pour le r�pertoire client
        DispatchFile aApp, True

        
    End If 'Si il y a des applicatifs
End If 'Si c'est un progiciel ou non

'-> Setting du nom du client connect�
CurrentUser = NomApp

'-> Ajouter le node de retour de choix du client
Set aNode = frmSample.TreeSamples.Nodes.Add(, , "RETOUR", aLb.GetCaption(10), "NewClient")

'-> Ajouter dans tous les cas le node de fin
Set aNode = frmSample.TreeSamples.Nodes.Add(, , "END", aLb.GetCaption(8), "Exit")


'-> Decharger la feuille en cours
Unload Me

'-> S�lectionner la premi�re icone
If frmSample.TreeSamples.Nodes.Count <> 0 Then frmSample.TreeSamples.Nodes(1).Selected = True

'-> Afficher la feuille
frmSample.Show vbModal

'-> Liberer le pointeur
Set aLb = Nothing

End Sub


Private Sub ListView1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then ListView1_DblClick
End Sub
