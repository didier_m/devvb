VERSION 5.00
Begin VB.Form frmCde 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Choix d'une commande"
   ClientHeight    =   5865
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   2670
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5865
   ScaleWidth      =   2670
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command3 
      Caption         =   "Afficher l'aide en ligne"
      Height          =   855
      Left            =   240
      Picture         =   "frmCde.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   3960
      Width           =   2175
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Ecran de paramétrage"
      Height          =   855
      Left            =   240
      Picture         =   "frmCde.frx":070E
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   120
      Width           =   2175
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Liste des commandes"
      Height          =   855
      Left            =   240
      Picture         =   "frmCde.frx":0A18
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   4920
      Width           =   2175
   End
   Begin VB.CommandButton Bt_Stop 
      Caption         =   "Arreter TurboBatch"
      Height          =   855
      Left            =   240
      Picture         =   "frmCde.frx":12E2
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2040
      Width           =   2175
   End
   Begin VB.CommandButton Bt_Go 
      Caption         =   "Lancer TurboBatch"
      Height          =   855
      Left            =   240
      Picture         =   "frmCde.frx":1BAC
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1080
      Width           =   2175
   End
   Begin VB.CommandButton Bt_Log 
      Caption         =   "Editer le fichier Log"
      Height          =   855
      Left            =   240
      Picture         =   "frmCde.frx":2476
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   3000
      Width           =   2175
   End
End
Attribute VB_Name = "frmCde"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Bt_Go_Click()

LigneCommande = "/RUN"
Unload Me

End Sub

Private Sub Bt_Log_Click()

LigneCommande = "/LOG"
Unload Me

End Sub

Private Sub Bt_Stop_Click()

LigneCommande = "/STOP"
Unload Me

End Sub

Private Sub Command1_Click()

frmHelp.Show vbModal

End Sub

Private Sub Command2_Click()

LigneCommande = "/PARAM"
Unload Me

End Sub

Private Sub Command3_Click()

LigneCommande = "/HELP"
Unload Me

End Sub
