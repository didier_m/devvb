VERSION 5.00
Begin VB.Form frmTurbo_batch 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Turbobatch"
   ClientHeight    =   7605
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5400
   Icon            =   "Turbo-batch.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7605
   ScaleWidth      =   5400
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "TurboBatch : "
      Height          =   1455
      Left            =   120
      TabIndex        =   14
      Top             =   6000
      Width           =   5175
      Begin VB.CommandButton Bt_Log 
         Caption         =   "Fichier Log"
         Height          =   855
         Left            =   3480
         Picture         =   "Turbo-batch.frx":08CA
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   360
         Width           =   1575
      End
      Begin VB.CommandButton Bt_Go 
         Caption         =   "Lancer"
         Height          =   855
         Left            =   120
         Picture         =   "Turbo-batch.frx":1194
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   360
         Width           =   1575
      End
      Begin VB.CommandButton Bt_Stop 
         Caption         =   "Arreter"
         Enabled         =   0   'False
         Height          =   855
         Left            =   1800
         Picture         =   "Turbo-batch.frx":1A5E
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   360
         Width           =   1575
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Param�trage :"
      Height          =   5775
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5175
      Begin VB.TextBox Delai 
         Height          =   285
         Left            =   3840
         TabIndex        =   7
         Top             =   5400
         Width           =   1215
      End
      Begin VB.TextBox DirMaqGui 
         Height          =   285
         Left            =   120
         TabIndex        =   2
         Top             =   1200
         Width           =   4935
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Left            =   3480
         Top             =   2160
      End
      Begin VB.TextBox DirSpool 
         Height          =   285
         Left            =   120
         TabIndex        =   1
         Top             =   600
         Width           =   4935
      End
      Begin VB.TextBox LogFile 
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Top             =   1800
         Width           =   4935
      End
      Begin VB.ListBox List1 
         Height          =   840
         Left            =   120
         TabIndex        =   4
         Top             =   2520
         Width           =   4935
      End
      Begin VB.TextBox Objet_Imp 
         Enabled         =   0   'False
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Top             =   4200
         Width           =   4935
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Ajouter"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   3480
         Width           =   2415
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Supprimer"
         Height          =   255
         Left            =   2760
         TabIndex        =   9
         Top             =   3480
         Width           =   2295
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Modifier l'imprimante associ�e"
         Height          =   495
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   4680
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Dur�e d'analyse (en seconde) :"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   5400
         Width           =   3375
      End
      Begin VB.Image Aide 
         Height          =   480
         Left            =   4440
         Picture         =   "Turbo-batch.frx":2328
         Top             =   4680
         Width           =   630
      End
      Begin VB.Label Label2 
         Caption         =   "R�pertoire des maquettes :"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   960
         Width           =   2055
      End
      Begin VB.Label Label1 
         Caption         =   "R�pertoire des spools :"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   2055
      End
      Begin VB.Label Label3 
         Caption         =   "Journal des log :"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   1560
         Width           =   2055
      End
      Begin VB.Label Label4 
         Caption         =   "Queues batch :"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   2280
         Width           =   3375
      End
      Begin VB.Image Image1 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   510
         Left            =   3840
         Picture         =   "Turbo-batch.frx":336A
         ToolTipText     =   "Enregistrer la configuration"
         Top             =   4680
         Width           =   510
      End
      Begin VB.Label Label8 
         Caption         =   "Imprimante associ�e :"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   3960
         Width           =   3735
      End
   End
End
Attribute VB_Name = "frmTurbo_batch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim TurboPath As String
Public Imp_Objs As New Collection
Public Imprimante As String
Dim Max As Integer

'-> Lecture du fichier
Dim SpoolFic As String
Dim NbCopies As Variant
Dim strImprimante As String
Dim Origine As String
Dim NomFile As String



Private Sub Aide_Click()

Shell "HelpDeal.exe DEAL|1|DEAL|TURBOBATCH", vbNormalFocus

End Sub

Public Sub Bt_Go_Click()

'-> Faire la verification du r�pertiore d'analyse

Dim hdlFile As Integer

If Trim(Me.DirSpool.Text) = "" Then
    MsgBox "Veuillez saisir le r�pertoire des spools", vbCritical + vbOKOnly, "Erreur"
    Me.DirSpool.SetFocus
    Exit Sub
End If

'-> Analyse si le chemin se termine par un \
If Right$(Me.DirSpool.Text, 1) <> "\" Then Me.DirSpool.Text = Me.DirSpool.Text & "\"

'-> V�rifier si le r�pertoire existe
If Dir$(Me.DirSpool.Text, vbDirectory) = "" Then
    MsgBox "R�pertoire d'analyse des spools incorrect", vbCritical + vbOKOnly, "Erreur"
    Me.DirSpool.SetFocus
    Exit Sub
End If

'-> V�rification du r�pertoire des maquettes
If Trim(Me.DirMaqGui.Text) = "" Then
    MsgBox "Veuillez saisir le r�pertoire des maquettes.", vbCritical + vbOKOnly, "Erreur"
    Me.DirMaqGui.SetFocus
    Exit Sub
End If

'-> Analyse si le chemin se termine par un \
If Right$(Me.DirMaqGui.Text, 1) <> "\" Then Me.DirMaqGui.Text = Me.DirMaqGui.Text & "\"

'-> V�rifier si le r�pertoire existe
If Dir$(Me.DirMaqGui.Text, vbDirectory) = "" Then
    MsgBox "R�pertoire des maquettes incorrect", vbCritical + vbOKOnly, "Erreur"
    Me.DirMaqGui.SetFocus
    Exit Sub
End If


'-> V�rification du r�pertoire du fichier log
If Trim(Me.LogFile.Text) = "" Then
    MsgBox "Le r�pertoire du journal Batch n'est pas param�tr�. Veuillez le saisir", vbCritical + vbOKOnly, "Erreur"
    Me.LogFile.SetFocus
    Exit Sub
End If

'-> Ajouter un "\" � la fin si necessaire
If Right$(Me.LogFile.Text, 1) <> "\" Then Me.LogFile.Text = Me.LogFile.Text & "\"

'-> Tester que le r�pertoire existe
If Dir$(Me.LogFile.Text, vbDirectory) = "" Then
    MsgBox "R�pertoire du journal des Log incorrect. Veuillez le modifier", vbCritical + vbOKOnly, "Erreur"
    Me.LogFile.SetFocus
    Exit Sub
End If

'-> V�rifier s'il y a une imprimante de renseign�e
If Imp_Objs.Count = 0 Then
    MsgBox "Impossible de la lancer l'analyseur de batch. Il n'y a pas d'imprimante.", vbCritical + vbOKOnly, "Erreur"
    Me.Command1.SetFocus
    Exit Sub
End If

'-> V�rifier la dur�e d'analyse
If Not IsNumeric(Me.Delai.Text) Then
    MsgBox "D�lai de temporisation incorrect", vbCritical + vbOKOnly, "Erreur"
    Me.Delai.SetFocus
    Exit Sub
End If

'-> Supprimer le fichier s'il existe d�ja
If Dir$(Me.LogFile.Text & "TurboBatch.log") <> "" Then Kill Me.LogFile.Text & "TurboBatch.log"

'-> Indiquer dans le fichier des Log que la session est en cours d'ouverture
hdlFile = FreeFile
Open Me.LogFile.Text & "TurboBatch.log" For Append As #hdlFile
Print #hdlFile, "-----------------------------------------------------------------------------"
Print #hdlFile, "Ouverture de session TurboBatch : " & Now
Print #hdlFile, "R�pertoire des spools : " & Me.DirSpool.Text
Print #hdlFile, "R�pertoire des maquettes : " & Me.DirMaqGui.Text
Print #hdlFile, "R�pertoire du Turbobatch.log : " & Me.LogFile
Print #hdlFile, "TurboBatch en attente ..."
Print #hdlFile, "-----------------------------------------------------------------------------"
Close #hdlFile

'-> Lancer l'analyse
Me.Timer1.Interval = CInt(Me.Delai.Text) * 1000
Me.Timer1.Enabled = True
Bt_Stop.Enabled = True
Bt_Go.Enabled = False

Me.Visible = False
    
End Sub

Private Sub Bt_Log_Click()

On Error Resume Next

frmJournalLog.Caption = "Edition du journal des Log : " & Me.LogFile.Text & "TurboBatch.log"
frmJournalLog.RichTextBox1.LoadFile Me.LogFile.Text & "Turbobatch.log"
frmJournalLog.Show 'vbModal

End Sub

Private Sub Bt_Stop_Click()


Me.Timer1.Enabled = False
Me.Bt_Stop.Enabled = False
Me.Bt_Go.Enabled = True



End Sub

Private Sub Command1_Click()

Dim Rep As String
Dim aImp_obj As Imp_Obj

Rep = InputBox("Cr�ation d'une queue batch : ", "Nom de la queue batch : ", "Nouveau")
If Trim(Rep) = "" Then Exit Sub

'-> V�rifier si la queue batch n'existe pas d�ja
For Each aImp_obj In Imp_Objs
    If UCase$(Trim(Rep)) = UCase$(Trim(aImp_obj.Name)) Then
        MsgBox "Cette queue batch est d�ja r�f�renc�e.", vbCritical + vbOKOnly, "Erreur"
        Exit Sub
    End If
Next

'-> Cr�ation d'une nouvelle queue batch
Set aImp_obj = New Imp_Obj
aImp_obj.Name = Trim(Rep)
Imp_Objs.Add aImp_obj, "IMP|" & aImp_obj.Name

'-> Ajout dans la liste
Me.List1.AddItem aImp_obj.Name

End Sub

Private Sub Command2_Click()

Dim Rep

If Me.List1.ListCount = 0 Then Exit Sub

If Me.List1.ListIndex = -1 Then
    MsgBox "Veuillez s�lectionner une queue batch", vbInformation + vbOKOnly, "Erreur"
    Exit Sub
End If

Rep = MsgBox("Supprimer la queue batch : " & Me.List1.Text, vbYesNo + vbQuestion)
If Rep = vbNo Then Exit Sub

'-> Supprimer l'objet de la collection
Imp_Objs.Remove ("IMP|" & Me.List1.Text)

'-> Supprimer l'entr�e de la liste
Me.List1.RemoveItem Me.List1.ListIndex

'-> S�lectionner la premi�re entr�e
If Me.List1.ListCount <> 0 Then
    Me.List1.Selected(0) = True
Else
    Me.Objet_Imp.Text = ""
End If

End Sub

Private Sub Command3_Click()


If Me.List1.ListCount = 0 Then Exit Sub

If Me.List1.ListIndex = -1 Then
    MsgBox "Veuillez s�lectionner une queue batch", vbCritical + vbOKOnly, "Erreur"
    Exit Sub
End If
    
Me.Imprimante = "NULL"
frmPrinters.Show vbModal
    
If Me.Imprimante = "NULL" Then Exit Sub

Set aImp_obj = Imp_Objs("IMP|" & Me.List1.Text)
aImp_obj.Imprimante = Trim(Me.Imprimante)
Me.Objet_Imp.Text = Trim(Me.Imprimante)

End Sub




Private Sub Delai_GotFocus()
    Selection Delai
End Sub

Private Sub DirMaqGui_GotFocus()

Selection Me.DirMaqGui

End Sub

Private Sub DirSpool_GotFocus()

Selection DirSpool

End Sub

Private Sub Form_Load()

Dim res As Long
Dim KeyValue As String
Dim lpBuffer As String
Dim aImp_obj As Imp_Obj
Dim i As Integer

'-> Variable pour temporisateur
Max = 10

'-> charger le propath des ressources TurboGraph
If InStr(1, UCase$(App.Path), "EMILIEGUI\SRC") <> 0 Then
    TurboPath = "E:\Erp\Turbo\"
Else
    CreatePath TurboPath
    TurboPath = TurboPath & "\"
End If
TurboPath = TurboPath & "TurboBatch.ini"

'-> V�rifier que le fichier Turbo-batch.ini existe bien
If Dir$(TurboPath, vbNormal) = "" Then Exit Sub

'-> Charger le fichier TurboBatch.ini

'-> Section General
DirSpool.Text = GetIniString("GENERAL", "Spool_Directory", TurboPath, False)
Me.DirMaqGui.Text = GetIniString("GENERAL", "Maqgui_Directory", TurboPath, False)
Me.LogFile.Text = GetIniString("GENERAL", "Log_Directory", TurboPath, False)
Me.Delai.Text = GetIniString("GENERAL", "DELAI", TurboPath, False)

'-> Initialiser la collection des imprimantes
Set Imp_Objs = New Collection
lpBuffer = Space$(20000)
res = GetPrivateProfileSection("Imprimantes", lpBuffer, Len(lpBuffer), TurboPath)
If res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Len(lpBuffer))
    For i = 1 To NumEntries(lpBuffer, Chr(0))
        Set aImp_obj = New Imp_Obj
        KeyValue = Entry(i, lpBuffer, Chr(0))
        If Trim(KeyValue) <> "" Then
            aImp_obj.Name = Entry(1, KeyValue, "=")
            aImp_obj.Imprimante = Entry(2, KeyValue, "=")
            Imp_Objs.Add aImp_obj, "IMP|" & aImp_obj.Name
            Me.List1.AddItem aImp_obj.Name
        End If
    Next
    '-> S�lectionner le premier �l�ment
    Me.List1.Selected(0) = True
        
End If


End Sub



Private Sub Form_Paint()

Dim Action As String

'---> Pour gestion des messages

'-> Lecture du press papier pour r�cup�ration de l'action � effectuer
Action = Clipboard.GetText

Select Case UCase$(Action)

    Case "TBB_STOP"
    
        '-> Arreter l'analyseur et rendre la fen�tre visible
        Bt_Stop_Click
        Me.Visible = True
        
    Case "TBB_END"
        End
        
    Case "TBB_LOG"
        '-> Lancer la fen�tre des log
        Bt_Log_Click
        
    Case "TBB_KILL"
        '-> Terminer
        End
        
        
               
End Select

End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub Image1_DblClick()

'---> Cette fonction enregistre les modifications dans le fichier
Dim res As Long
Dim lpBuffer As String
Dim aImp_obj As Imp_Obj

'-> Enregistrer la section g�n�rale
lpBuffer = "Spool_Directory=" & Me.DirSpool.Text & Chr(0)
lpBuffer = lpBuffer & "MaqGui_Directory=" & Me.DirMaqGui.Text & Chr(0)
lpBuffer = lpBuffer & "Log_Directory=" & Me.LogFile.Text & Chr(0)
lpBuffer = lpBuffer & "Delai=" & Me.Delai.Text
res = WritePrivateProfileSection("General", lpBuffer, TurboPath)

'-> Enrgistrer la liste des imprimantes
lpBuffer = ""
For Each aImp_obj In Imp_Objs
    If lpBuffer = "" Then
        lpBuffer = aImp_obj.Name & "=" & aImp_obj.Imprimante & Chr(0)
    Else
        lpBuffer = lpBuffer & aImp_obj.Name & "=" & aImp_obj.Imprimante & Chr(0)
    End If
Next
res = WritePrivateProfileSection("Imprimantes", lpBuffer, TurboPath)

'-> Message de fin de traitement
MsgBox "Enregistrement termin�", vbInformation + vbOKOnly, "Enregistrement"

End Sub


Private Sub List1_Click()

Dim aImp_obj As Imp_Obj

If Me.List1.ListCount = 0 Then Exit Sub

Set aImp_obj = Imp_Objs("IMP|" & Me.List1.Text)
Objet_Imp.Text = aImp_obj.Imprimante

End Sub

Sub Selection(aText As TextBox)

aText.SelStart = 0
aText.SelLength = 65535

End Sub

Private Sub LogFile_GotFocus()

Selection LogFile

End Sub


Private Sub Objet_Imp_GotFocus()

Selection Objet_Imp

End Sub

Private Sub Timer1_Timer()

Dim TempFileName As String
Dim FindImp As Boolean
Dim aImp_obj As Imp_Obj
Dim LigneCommande As String
Dim Ligne As String
Dim hdlFile As Integer
Dim hdlFichier As Integer
Dim hdlLog As Integer

On Error GoTo GestError

'-> Rendre la main � la CPU
DoEvents

'-> Analyse des fichiers scripts
NomFile = Dir$(Me.DirSpool.Text & "*.tb", vbNormal)
If NomFile <> "" Then

    '-> Ouverture du journal des log
    hdlLog = FreeFile
    Open Me.LogFile.Text & "TurboBatch.log" For Append As #hdlLog

    '-> Lecture des donn�es du fichier Script
    SpoolFic = GetIniString("DATA", "SPOOL", Me.DirSpool.Text & NomFile, False)
    NbCopies = GetIniString("DATA", "COPIES", Me.DirSpool.Text & NomFile, False)
    strImprimante = GetIniString("DATA", "IMP", Me.DirSpool.Text & NomFile, False)
    Origine = GetIniString("DATA", "ORIGINE", Me.DirSpool.Text & NomFile, False)
    
    '-> Si origine NT, ne garder que le nom du fichier
    If UCase$(Origine) = "NT" Then
        SpoolFic = Entry(NumEntries(SpoolFic, "\"), SpoolFic, "\")
    End If
    
    '-> V�rifier les donn�es
    If Dir$(Me.DirSpool.Text & SpoolFic, vbNormal) = "" Then SetError 1, hdlLog
    
    '-> Tester le nombre de copies
    If Not IsNumeric(NbCopies) Then
        NbCopies = 1
    Else
        NbCopies = CInt(NbCopies)
    End If
    
    '-> V�rifier si l'imprimante sp�cifi�e est bien param�tr�e
    For Each aImp_obj In Imp_Objs
        If Trim(UCase$(strImprimante)) = UCase$(aImp_obj.Name) Then
            FindImp = True
            Exit For
        End If
    Next
    If Not FindImp Then
        SetError 2, hdlLog
        Exit Sub
    Else
        LigneCommande = aImp_obj.Imprimante & "$BATCH$" & NbCopies & "|"
    End If
    
    '-> Analyse d'origine
    If Trim(Origine) = "" Then
        SetError 3, hdlLog
        Exit Sub
    Else
        '-> Analyse de l'origine
        Select Case Trim(UCase$(Origine))
            
            Case "UNIX"
            Case "NT"
            
            Case Else
                SetError 3, hdlLog
                Exit Sub
        End Select
    End If
    
    '-> Obtenir un nom de fichier temporaire
    TempFileName = GetTempFileNameVB("BAT")
    LigneCommande = LigneCommande & TempFileName
        
    '-> Ouvrir le fichier dans le RTF pour substitution des chaines de cract�re si vient d'UNIX
    If Origine = "UNIX" Then
        '-> Ouverture du fichier pour r�cup�rer la ligne de d�finition de la maquette
        hdlFile = FreeFile
        Open Me.DirSpool.Text & SpoolFic For Input As #hdlFile
        '-> Ouvrir le fichier de donn�es
        hdlFichier = FreeFile
        Open TempFileName For Output As #hdlFichier
        Do While Not EOF(hdlFile)
            Line Input #hdlFile, Ligne
            If InStr(1, UCase$(Ligne), "%%GUI%%") <> 0 Then
                '-> R�cup�ration du nom de la maquette
                NomMaquette = Entry(NumEntries(Ligne, "/"), Ligne, "/")
                Ligne = "%%GUI%%" & Me.DirMaqGui.Text & NomMaquette
                '->Inscrire la premi�re ligne
                Print #hdlFichier, Ligne
            Else
                '-> Enregistrer les lignes
                Print #hdlFichier, Ligne
            End If
        Loop
        '-> Fermer les 2 fichiers
        Close #hdlFile
        Close #hdlFichier
    Else
        '-> Faire une copie du fichier de donn�es
        CopyFile Me.DirSpool.Text & SpoolFic, TempFileName, 0
            
    End If
    '-> Supprimer le fichier de donn�es
    'Kill Me.DirSpool.Text & SpoolFic
    
    '-> Supprimer le fichier script
    'Kill Me.DirSpool.Text & NomFile
    
    '-> Mettre � jour le journal des log
    Print #hdlLog, "Date de traitement : " & Now
    Print #hdlLog, "     Fichier Script : " & NomFile
    Print #hdlLog, "     Fichier donn�es : " & SpoolFic
    Print #hdlLog, "     Nombre de copies : " & NbCopies
    Print #hdlLog, "     Queue batch : " & aImp_obj.Name
    Print #hdlLog, "     Imprimante associ�e : " & aImp_obj.Imprimante
    Print #hdlLog, "     Origine : " & Origine
    Print #hdlLog, "     Fichier temporaire associ� : " & TempFileName
    Print #hdlLog, "     Ligne de commande g�n�r�e : " & LigneCommande
    Print #hdlLog, "    "
    Print #hdlLog, "*****************************************"
    Print #hdlLog, "     "
    '-> Fermer le fichier tempo des logs
    Close #hdlLog
    
    '-> Lancer le TurboPrint
    Shell App.Path & "\Turbograph.exe " & LigneCommande
    
    '-> Mettre � jour le journal des Log
    If frmJournalLog.Visible Then
        
        frmJournalLog.RichTextBox1.Text = ""
        frmJournalLog.RichTextBox1.LoadFile Me.LogFile.Text & "TurboBatch.log"
    End If
    
End If 'S'il y a un batch

Exit Sub

GestError:

    SetError 999, hdlLog
    Exit Sub

End Sub




Private Sub SetError(ErrorCode As Integer, hdlFile As Integer)

'---> Cette fonction enregistre une erreur dans le fichier des log

On Error GoTo GestError


Print #hdlFile, "Date de traitement : " & Now
Print #hdlFile, "     Fichier Script : " & NomFile

Select Case ErrorCode

    Case 1
        '-> Fichier de donn�es introuvable
        Print #hdlFile, "Impossible de trouver le fichier donn�es " & SpoolFic & " dans le r�pertoire " & Me.DirSpool.Text
                   
    Case 2
        '-> Imprimante non trouv�e
        Print #hdlFile, "Impossible de trouver l'imprimante " & strImprimante & " dans le fichier Turbo-batch.ini"
    Case 3
        '-> Origine non connue
        Print #hdlFile, "Impossible de trouver l'origine " & Origine & " dans le fichier Turbo-batch.ini"

    Case 999
        '-> Erreur Incontrol�e
        Print #hdlFile, "Erreur innatendue"

End Select

Print #hdlFile, "Traitement annul�"
Print #hdlFile, "*****************************************"
Print #hdlFile, "     "
'-> Fermer le fichier tempo des logs
Close #hdlFile

'-> Supprimer le fichier script
'Kill Me.DirSpool.Text & NomFile

'-> Supprimer le fichier de donn�es
'Kill Me.DirSpool.Text & SpoolFic
    
'-> Mettre � jour le journal des Log
If frmJournalLog.Visible Then
    frmJournalLog.RichTextBox1.Text = ""
    frmJournalLog.RichTextBox1.LoadFile Me.LogFile.Text & "TurboBatch.log"
    frmJournalLog.RichTextBox1.SelStart = Len(frmJournalLog.RichTextBox1.Text)
    frmJournalLog.Refresh
End If


Exit Sub


GestError:

    '-> fermer les fichiers
    Reset


End Sub
