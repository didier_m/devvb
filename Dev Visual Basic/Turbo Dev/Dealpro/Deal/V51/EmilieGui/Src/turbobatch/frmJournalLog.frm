VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmJournalLog 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   213
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   312
   StartUpPosition =   2  'CenterScreen
   Begin RichTextLib.RichTextBox RichTextBox1 
      Height          =   2415
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   4260
      _Version        =   393217
      Enabled         =   -1  'True
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      AutoVerbMenu    =   -1  'True
      TextRTF         =   $"frmJournalLog.frx":0000
   End
End
Attribute VB_Name = "frmJournalLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

Me.Width = Screen.Width / 2
Me.Height = Screen.Height / 2

End Sub

Private Sub Form_Resize()

Dim aRect As Rect
Dim res As Long

res = GetClientRect(Me.hWnd, aRect)
Me.RichTextBox1.Left = 0
Me.RichTextBox1.Top = 0
Me.RichTextBox1.Height = aRect.Bottom
Me.RichTextBox1.Width = aRect.Right


End Sub
