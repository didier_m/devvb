Attribute VB_Name = "fctTurboBatch"
Public TargetName As String
Public TargetHwnd As Long
Public LigneCommande As String

Sub Main()


Dim Exist As Boolean
Dim Rep

'---> Fonction d'intialisation du trubo batch

'-> R�cup�ration de la ligne de commande
LigneCommande = Command$

If Trim(LigneCommande) = "" Then frmCde.Show vbModal
    
Exist = WindowExist()

Select Case UCase$(Trim(LigneCommande))

    Case "/PARAM"
    
        If Exist Then
            Rep = MsgBox("Attention : Une session est en cours de travail. Cette fonction arretra le batch. Continuer", vbExclamation + vbYesNo + vbDefaultButton2, "TurboBatch")
            If Rep = vbNo Then End
            Clipboard.SetText "TBB_STOP"
            SendMessage TargetHwnd, WM_PAINT, 0, 0
            '-> Quitter cette application
            End
                        
        End If
        Clipboard.Clear
        frmTurbo_batch.Show
            
    
    Case "/RUN"
        If Exist Then
            Rep = MsgBox("Attention : Une session est en cours de travail. Cette fonction arretra la pr�c�dente session et activera celle-ci. Continuer", vbExclamation + vbYesNo + vbDefaultButton2, "TurboBatch")
            If Rep = vbNo Then End
        End If
        
        Clipboard.SetText "TBB_END"
        SendMessage TargetHwnd, WM_PAINT, 0, 0
        
        Clipboard.Clear
        Load frmTurbo_batch
        frmTurbo_batch.Bt_Go_Click
        
    Case "/LOG"
    
        If Exist Then
            Clipboard.SetText "TBB_LOG"
            SendMessage TargetHwnd, WM_PAINT, 0, 0
        Else
            MsgBox "Il n'y pas de session TurboBatch en cours.", vbCritical + vbOKOnly, "TurboBatch"
            Clipboard.Clear
            End
        End If
        
        Clipboard.Clear
        
    Case "/STOP"
    
        If Exist Then
            '-> LAncer la fin du programme
            Clipboard.SetText "TBB_KILL"
            SendMessage TargetHwnd, WM_PAINT, 0, 0
            End
        End If
        
    Case "/?"
        frmHelp.Show vbModal
        
    Case "/HELP"
    
        Shell App.Path & "\HELPDEAL.EXE DEAL|1|DEAL|TURBOBATCH", vbNormalFocus
        End
        
            
End Select



End Sub

Public Function WindowExist() As Boolean

'---> D�termine s'il y a d�ja une session overte du TB

TargetName = "Turbobatch"
TargetHwnd = 0
    
EnumWindows AddressOf WindowEnumerator, 0

If TargetHwnd = 0 Then
    WindowExist = False
Else
    WindowExist = True
End If

End Function

Public Function WindowEnumerator(ByVal app_hwnd As Long, ByVal lParam As Long) As Long
Dim buf As String * 256
Dim title As String
Dim length As Long

    length = GetWindowText(app_hwnd, buf, Len(buf))
    title = Left$(buf, length)

    If InStr(title, TargetName) > 0 Then
        TargetHwnd = app_hwnd
        WindowEnumerator = False
    Else
        WindowEnumerator = True
    End If
    
End Function

