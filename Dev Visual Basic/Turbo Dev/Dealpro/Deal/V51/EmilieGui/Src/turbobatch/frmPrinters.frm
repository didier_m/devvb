VERSION 5.00
Begin VB.Form frmPrinters 
   Caption         =   "Liste des imprimantes disponibles sur le syst�me "
   ClientHeight    =   3165
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6810
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3165
   ScaleWidth      =   6810
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Ok 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   6120
      Picture         =   "frmPrinters.frx":0000
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   3
      ToolTipText     =   "Affecter l'imprimante"
      Top             =   2640
      Width           =   630
   End
   Begin VB.PictureBox Annuler 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   5400
      Picture         =   "frmPrinters.frx":1042
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   42
      TabIndex        =   2
      ToolTipText     =   "Annuler"
      Top             =   2640
      Width           =   630
   End
   Begin VB.ListBox List1 
      Height          =   2010
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   6615
   End
   Begin VB.Label Label1 
      Caption         =   "Veuillez s�lectionner l'imprimante � associer :"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3615
   End
End
Attribute VB_Name = "frmPrinters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Annuler_Click()

Unload Me

End Sub

Private Sub Form_Load()

Dim x As Printer

For Each x In Printers
    Me.List1.AddItem x.DeviceName
Next

If Me.List1.ListCount <> 0 Then
    Me.List1.Selected(0) = True
Else
    Me.Ok.Visible = False
End If

End Sub

Private Sub Ok_Click()

frmTurbo_batch.Imprimante = Me.List1.Text
Unload Me

End Sub
