VERSION 5.00
Begin VB.Form frmRecrut 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Fichier recrutement : "
   ClientHeight    =   8115
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6150
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8115
   ScaleWidth      =   6150
   Begin VB.TextBox Text1 
      Height          =   4095
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   19
      Top             =   3840
      Width           =   5895
   End
   Begin VB.Label Label1 
      Caption         =   "Nom :"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   18
      Top             =   480
      Width           =   1575
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   0
      Left            =   1800
      TabIndex        =   17
      Top             =   480
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   1
      Left            =   1800
      TabIndex        =   16
      Top             =   840
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   4
      Left            =   1800
      TabIndex        =   15
      Top             =   1200
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   5
      Left            =   1800
      TabIndex        =   14
      Top             =   1560
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   6
      Left            =   1800
      TabIndex        =   13
      Top             =   1920
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   7
      Left            =   1800
      TabIndex        =   12
      Top             =   2280
      Width           =   1095
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   8
      Left            =   3000
      TabIndex        =   11
      Top             =   2280
      Width           =   3015
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   9
      Left            =   1800
      TabIndex        =   10
      Top             =   2640
      Width           =   2055
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   10
      Left            =   3960
      TabIndex        =   9
      Top             =   2640
      Width           =   2055
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   11
      Left            =   1800
      TabIndex        =   8
      Top             =   3000
      Width           =   4215
   End
   Begin VB.Label Label1 
      Caption         =   "Pr�nom : "
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   7
      Top             =   840
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Adresse"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   6
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Cp / Ville :"
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   5
      Top             =   2280
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "T�l / Fax :"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   4
      Top             =   2640
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "e-mail : "
      Height          =   255
      Index           =   9
      Left            =   120
      TabIndex        =   3
      Top             =   3000
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Cursus / Formation : "
      Height          =   255
      Index           =   10
      Left            =   120
      TabIndex        =   2
      Top             =   3480
      Width           =   1575
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   14
      Left            =   1800
      TabIndex        =   1
      Top             =   120
      Width           =   4215
   End
   Begin VB.Label Label1 
      Caption         =   "Date et heure : "
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1575
   End
End
Attribute VB_Name = "frmRecrut"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Fichier As String '-> nom du fichier recrutement
Public aRecrut As Recrutement  '-> Pointeur vers un objet recrutement

Private Sub Text1_KeyPress(KeyAscii As Integer)

KeyAscii = 0

End Sub

Public Sub Init()

'---> Cette proc�dure ouvre le fichier ASCII sp�cifi� et remplis les zones

Dim hdlFile As Integer
Dim Ligne As String
Dim DefValue As String
Dim Cv As String
Dim FirstLigne As Boolean

'-> Ouvrir le fichier ASCII
hdlFile = FreeFile
Open Fichier For Input As #hdlFile
FirstLigne = True
Do While Not EOF(hdlFile)
    Line Input #hdlFile, Ligne
    If FirstLigne Then
        DefValue = Ligne
        FirstLigne = False
    Else
        If Trim(Cv) = "" Then
            Cv = Ligne
        Else
            Cv = Cv & Chr(13) & Chr(10) & Ligne
        End If
    End If
Loop

'-> Fermer le fichier ASCII
Close #hdlFile

'-> Afficher les information
Me.lblValue(0) = Entry(1, DefValue, "|")
Me.lblValue(1) = Entry(2, DefValue, "|")
Me.lblValue(4) = Entry(3, DefValue, "|")
Me.lblValue(5) = Entry(4, DefValue, "|")
Me.lblValue(6) = Entry(5, DefValue, "|")
Me.lblValue(7) = Entry(6, DefValue, "|")
Me.lblValue(8) = Entry(7, DefValue, "|")
Me.lblValue(9) = Entry(8, DefValue, "|")
Me.lblValue(10) = Entry(9, DefValue, "|")
Me.lblValue(11) = Entry(10, DefValue, "|")
Me.Text1.Text = Cv
Me.lblValue(14) = Entry(11, DefValue, "|")

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set aRecrut.aFrm = Nothing
End Sub

