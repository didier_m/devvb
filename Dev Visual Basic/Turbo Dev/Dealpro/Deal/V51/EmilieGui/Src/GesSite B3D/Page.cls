VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Page"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Cette classe d�finit une page d'un site
Public Path As String '-> Path du site
Public PathToPublish As String '-> Indique le path de l'endroit o� sera publi� l'�l�ment
Public Onglets As Collection '-> Liste des onglets affect�s � la page
Public Key As String '-> Cl� d'acces de la page
Public Libel As String '-> Libell� de la page


Private Sub Class_Initialize()

'-> initialiser la collection des �l�ments associ�s � cette page
Set Onglets = New Collection

End Sub
