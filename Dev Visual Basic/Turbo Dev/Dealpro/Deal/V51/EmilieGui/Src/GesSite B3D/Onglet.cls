VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Onglet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> D�finition d'un onglet
Public Libel As String '-> Libelle de l'onglet
Public Path As String '-> Path de l'onglet
Public PathToPublish As String '-> Indique le path de l'endroit o� sera publi� l'�l�ment
Public Elements As Collection '-> Elements modifiables de l'onglet
Public NbElements As Integer '-> Compteur d'�l�ments dans l'onglet
Public Key As String '-> Cl� d'acces de l'onglet


Private Sub Class_Initialize()

'-> Init de la collection des �l�ments
Set Elements = New Collection

'-> Init du compteur d'�lements
NbElements = 1

End Sub

Public Sub AddElement(ByVal DefElement As String)

'---> Cette proc�dure ajoute un �l�ment dans la collection des �l�ments de la page

Dim aElement As New Element

'-> Init des prorpri�t�s
aElement.ElementType = Entry(1, DefElement, "�")
aElement.Fichier = Entry(2, DefElement, "�")
aElement.Key = "ELEMENT|" & NbElements
aElement.PathToPublish = Me.PathToPublish
aElement.PathTempo = Me.Path

'-> Ajout dans la collection des �l�ments
Me.Elements.Add aElement, aElement.Key

'-> Incr�menter le compteur d'�l�ments
NbElements = NbElements + 1

End Sub
