VERSION 5.00
Begin VB.Form frmGestPwd 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gestion du mot de passe"
   ClientHeight    =   2040
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4785
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2040
   ScaleWidth      =   4785
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Modification du mot de passe : "
      Height          =   1935
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   4695
      Begin VB.CommandButton Command1 
         Caption         =   "&Appliquer"
         Height          =   375
         Left            =   3240
         TabIndex        =   5
         Top             =   1440
         Width           =   1335
      End
      Begin VB.TextBox Text2 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2160
         MaxLength       =   50
         PasswordChar    =   "*"
         TabIndex        =   4
         Top             =   840
         Width           =   2415
      End
      Begin VB.TextBox Text1 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2160
         MaxLength       =   50
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   480
         Width           =   2415
      End
      Begin VB.Image Image1 
         Height          =   480
         Left            =   240
         Picture         =   "frmGestPwd.frx":0000
         Top             =   1200
         Width           =   480
      End
      Begin VB.Label Label2 
         Caption         =   "Confirmation : "
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   840
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Nouveau mot de passe : "
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   480
         Width           =   1815
      End
   End
End
Attribute VB_Name = "frmGestPwd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

Dim Res As Long


'-> V�rifier dans un premier temps que le mot de passe est saisi
If Trim(Me.Text1.Text) = "" Then
    MsgBox "Veuillez saisir un mot de passe", vbExclamation + vbOKOnly, "Erreur"
    Me.Text1.SetFocus
    Exit Sub
End If

'-> V�rifier qu'il y a confirmation
If Me.Text1.Text <> Me.Text2.Text Then
    MsgBox "Erreur sur la confirmation du mot de passe", vbExclamation + vbOKOnly, "Erreur"
    Me.Text2.SetFocus
    Exit Sub
End If

'-> Mettre � jour le fichier Ini
Res = WritePrivateProfileString("SETUP", "PWD", Crypt(Me.Text1.Text), App.Path & "\Datas\Param.ini")

'-> Mettre � jour la variable globale du mot de passe
Pwd = Me.Text1.Text

'-> Quitter la feille
Unload Me


End Sub
