VERSION 5.00
Begin VB.Form frmDemarrage 
   BackColor       =   &H00FF8080&
   ClientHeight    =   6195
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   10920
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   Picture         =   "frmDemarrage.frx":0000
   ScaleHeight     =   6195
   ScaleWidth      =   10920
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Tempo 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   200
      Left            =   240
      ScaleHeight     =   195
      ScaleWidth      =   8175
      TabIndex        =   0
      Top             =   5880
      Width           =   8175
   End
   Begin VB.Timer Timer1 
      Interval        =   1
      Left            =   1200
      Top             =   480
   End
   Begin VB.Image Image2 
      Height          =   660
      Left            =   6840
      Picture         =   "frmDemarrage.frx":DD2AA
      Top             =   2640
      Width           =   3420
   End
   Begin VB.Image Image1 
      Height          =   900
      Left            =   5760
      Picture         =   "frmDemarrage.frx":E487C
      Top             =   2520
      Width           =   900
   End
End
Attribute VB_Name = "frmDemarrage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim TailleLue As Long
Dim TailleTotale As Long

Private Sub Form_Load()

TailleLue = 0
TailleTotale = 100

End Sub

Private Sub Timer1_Timer()

'-> Incr�ment du compteur
TailleLue = TailleLue + 1

'-> Dessin de la temporisation
Me.Tempo.Line (0, 0)-((TailleLue / TailleTotale) * Me.Tempo.ScaleWidth, Me.Tempo.ScaleHeight), &HFF8080, BF

DoEvents
'-> Test de fin de chargement
If TailleLue = TailleTotale Then Unload Me

End Sub
