VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "SHDOCVW.DLL"
Begin VB.Form frmPage 
   Caption         =   "Aper�u du fichier : "
   ClientHeight    =   6060
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5940
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   404
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   396
   Begin VB.PictureBox Picture1 
      Height          =   4215
      Left            =   0
      ScaleHeight     =   277
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   381
      TabIndex        =   0
      Top             =   120
      Width           =   5775
      Begin SHDocVwCtl.WebBrowser WebBrowser1 
         Height          =   2415
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   2895
         ExtentX         =   5106
         ExtentY         =   4260
         ViewMode        =   0
         Offline         =   0
         Silent          =   0
         RegisterAsBrowser=   0
         RegisterAsDropTarget=   1
         AutoArrange     =   0   'False
         NoClientEdge    =   0   'False
         AlignLeft       =   0   'False
         ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
         Location        =   ""
      End
   End
End
Attribute VB_Name = "frmPage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public NomFichier As String '-> Indique le nom du fichier en cours de validation
Public aElement As Element '-> Element � qui appartient
Private Sub Form_Resize()

Dim Res As Long
Dim aRect As RECT

'-> Get de la zone cliente
Res = GetClientRect(Me.hwnd, aRect)

Me.Picture1.Left = 0
Me.Picture1.Height = aRect.Bottom
Me.Picture1.Width = aRect.Right
Me.Picture1.Top = 0

'-> Get de la zone cliente
Res = GetClientRect(Picture1.hwnd, aRect)

Me.WebBrowser1.Left = 0
Me.WebBrowser1.Height = aRect.Bottom
Me.WebBrowser1.Width = aRect.Right
Me.WebBrowser1.Top = 0


End Sub

Private Sub Form_Unload(Cancel As Integer)

'-> Lib�rer le pointeur sur la feuille
Set aElement.aFrm = Nothing

End Sub
