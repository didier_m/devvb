Attribute VB_Name = "fctGestSite"
Option Explicit

Public aSite As Site '-> Pointeur vers l'objet Site en cours
Public Contacts As Collection '-> Collection des contacts
Public Recrutements As Collection '-> Collection des recrutements

'-> Variable d'�change entre deux feuilles
Public StrRetour As String

Public Function GetTempFileNameVB(ByVal Id As String, Optional Rep As Boolean) As String

Dim TempPath As String
Dim lpBuffer As String
Dim Result As Long
Dim TempFileName As String

'---> Fonction qui d�termine un nom de fichier tempo sous windows

'-> Recherche du r�pertoire temporaire
lpBuffer = Space$(500)
Result = GetTempPath(Len(lpBuffer), lpBuffer)
TempPath = Mid$(lpBuffer, 1, Result)

'-> Si on ne demande que le r�pertoire de windows
If Rep Then
    GetTempFileNameVB = TempPath
    Exit Function
End If

'-> Cr�ation d'un nom de fichier
TempFileName = Space$(1000)
Result = GetTempFileName(TempPath, Id, 0, TempFileName)
TempFileName = Entry(1, TempFileName, Chr(0))

GetTempFileNameVB = TempFileName

End Function


Public Sub OpenSite(ByVal NomSite As String)

'---> Cette proc�dure charge un site dans l'interface

Dim Res As Long
Dim ErrorCode As Integer
Dim aPage As Page
Dim aOnglet As Onglet
Dim aElement As Element
Dim aContact As Contact
Dim i As Integer, j As Integer, k As Integer
Dim strDef As String
Dim lpBuffer As String
Dim lpBuffer2 As String
Dim lpbuffer3 As String
Dim lpBuffer4 As String
Dim LibCode As String
Dim aNode As Node
Dim aNode2 As Node
Dim aNode3 As Node
Dim aNode4 As Node
Dim ArchivePath As String

'-> Initialiser le pointeur vers l'objet Site
Set aSite = New Site

'-> Analyse du PATH
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("SITE", "TEMPO", "", lpBuffer, Len(lpBuffer), NomSite)
ErrorCode = 1

'-> Annuler la proc�dure si on ne trouve pas les param�tres
If Res = 0 Then GoTo GestError
    
'-> Setting de l'adresse du site
aSite.Tempo = Mid$(lpBuffer, 1, Res)

'-> Analyse du PATH tempo
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("SITE", "MAPPING", "", lpBuffer, Len(lpBuffer), NomSite)
ErrorCode = 1

'-> Annuler la proc�dure si on ne trouve pas les param�tres
If Res = 0 Then GoTo GestError
    
'-> Setting de l'adresse du site
aSite.Path = Mid$(lpBuffer, 1, Res)

'-> R�cup�ration du path du fichier d'archivage
ArchivePath = Replace(NomSite, ".Dgs", ".Das")
aSite.ArchivageFile = ArchivePath

'-> Analyse du PATH des contacts
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("SITE", "CONTACTS", "", lpBuffer, Len(lpBuffer), NomSite)

If Res = 0 Then
    aSite.IsContact = False
Else
    '-> Setting du path des contacts
    aSite.Contact = Mid$(lpBuffer, 1, Res)
    '-> V�rifier que le path existe
    If Dir$(aSite.Contact, vbDirectory) = "" Then
        MsgBox "Impossible de trouver le r�pertoire : " & aSite.Contact & Chr(13) & "Gestion des contacts d�sactiv�e.", vbExclamation + vbOKOnly, "Avertissement"
        aSite.IsContact = False
    Else
        aSite.IsContact = True
    End If
End If
    
'-> Analyse du PATH des recrutement
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("SITE", "RECRUTEMENT", "", lpBuffer, Len(lpBuffer), NomSite)

If Res = 0 Then
    aSite.IsRecrutement = False
Else
    '-> Setting de l'adresse du site
    aSite.Recrutement = Mid$(lpBuffer, 1, Res)
        If Dir$(aSite.Recrutement, vbDirectory) = "" Then
        MsgBox "Impossible de trouver le r�pertoire : " & aSite.Recrutement & Chr(13) & "Gestion des recrutements d�sactiv�e.", vbExclamation + vbOKOnly, "Avertissement"
        aSite.IsRecrutement = False
    Else
        aSite.IsRecrutement = True
    End If
End If

'-> Analyse de l'URL
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("SITE", "URL", "", lpBuffer, Len(lpBuffer), NomSite)
ErrorCode = 2

'-> Annuler la proc�dure si on ne trouve pas les param�tres
If Res = 0 Then GoTo GestError

'-> Setting de l'url
aSite.Url = Mid$(lpBuffer, 1, Res)

'-> Setting du fichier historique
lpBuffer = Replace(UCase$(NomSite), ".DGS", ".dps")
If Dir$(lpBuffer, vbNormal) <> "" Then aSite.Histo = lpBuffer

'-> R�cup�ration de la liste des pages du site
lpBuffer = Space$(2000)
Res = GetPrivateProfileString("PAGES", "PAGE", "", lpBuffer, Len(lpBuffer), NomSite)
ErrorCode = 3

'-> Annuler la proc�dure si on ne trouve pas les param�tres
If Res = 0 Then GoTo GestError

lpBuffer = Mid$(lpBuffer, 1, Res)

'-> Analyse de la liste des pages pr�sentes sur le site
For i = 1 To NumEntries(lpBuffer, "�")
Reprise:
    '-> Cr�ation d'un nouvel objet Page
    Set aPage = New Page
    '-> Get de la page
    strDef = Entry(i, lpBuffer, "�")
    '-> Ajout de la page
    aSite.Pages.Add aPage, "PAGE|" & UCase$(strDef)
    aPage.Key = "PAGE|" & UCase$(strDef)
    aPage.Libel = strDef
    '-> Get de la cl� Path
    lpBuffer2 = Space$(2000)
    Res = GetPrivateProfileString(strDef, "PATH", "", lpBuffer2, Len(lpBuffer2), NomSite)
    ErrorCode = 4
    LibCode = "Impossible de trouver le Path de la page : " & strDef
    If Res = 0 Then GoTo GestError
    '-> Setting du path
    aPage.Path = Replace(Mid$(lpBuffer2, 1, Res), "$MAPPING$", aSite.Tempo)
    '-> Setting du path de publication
    aPage.PathToPublish = Replace(Mid$(lpBuffer2, 1, Res), "$MAPPING$", aSite.Path)
    '-> r�cup�ration de la liste des diff�rents onglets de la page sp�cifi�e
    lpbuffer3 = Space$(2000)
    Res = GetPrivateProfileString(strDef, "Onglets", "", lpbuffer3, Len(lpbuffer3), NomSite)
    If Res = 0 Then GoTo GestError
    lpbuffer3 = Mid$(lpbuffer3, 1, Res)
    For j = 1 To NumEntries(lpbuffer3, "�")
        '-> Cr�er un nouvel �lement
        Set aOnglet = New Onglet
        '-> Ajouter dans la collection
        aPage.Onglets.Add aOnglet, "ONGLET|" & UCase$(Entry(j, lpbuffer3, "�"))
        aOnglet.Key = "ONGLET|" & UCase$(Entry(j, lpbuffer3, "�"))
        '-> Faire un acces � la cle Libel
        lpBuffer4 = Space$(2000)
        Res = GetPrivateProfileString(Entry(j, lpbuffer3, "�"), "Libel", "", lpBuffer4, Len(lpBuffer4), NomSite)
        If Res = 0 Then GoTo GestError
        '-> Setting de la valeur
        aOnglet.Libel = Mid$(lpBuffer4, 1, Res)
        '-> Faire un acces � la cle Path
        lpBuffer4 = Space$(2000)
        Res = GetPrivateProfileString(Entry(j, lpbuffer3, "�"), "Path", "", lpBuffer4, Len(lpBuffer4), NomSite)
        If Res = 0 Then GoTo GestError
        '-> Remplacer le mot cle $PATH$ par sa valeur
        aOnglet.Path = Replace(Mid$(lpBuffer4, 1, Res), "$PATH$", aPage.Path)
        aOnglet.PathToPublish = Replace(Mid$(lpBuffer4, 1, Res), "$PATH$", aPage.PathToPublish)
        '-> Cr�er la liste des �l�ments de cet onglet
        lpBuffer4 = Space$(2000)
        Res = GetPrivateProfileString(Entry(j, lpbuffer3, "�"), "Elements", "", lpBuffer4, Len(lpBuffer4), NomSite)
        If Res <> 0 Then
            lpBuffer4 = Mid$(lpBuffer4, 1, Res)
            For k = 1 To NumEntries(lpBuffer4, "�")
                '-> Ajout dans la collection des �l�ments
                aOnglet.AddElement Entry(k, lpBuffer4, "�")
            Next 'Pour tous les �l�ments d'une page
        End If
    Next 'Pour tous les onglets d'une page
Next 'Pour toutes les pages r�f�renc�es

'-> Cr�er l'icone du site
Set aNode = MdiMain.TreeNaviga.Nodes.Add(, , "SITE", NomSite, "Close")
aNode.Expanded = True
aNode.ExpandedImage = "Open"
aNode.Tag = "SITE"

'-> Cr�er l'icone des contacts
If aSite.IsContact Then
    MdiMain.TreeNaviga.Nodes.Add , , "CONTACT", "Contacts", "Close"
    MdiMain.TreeNaviga.Nodes("CONTACT").ExpandedImage = "Open"
    MdiMain.TreeNaviga.Nodes("CONTACT").Tag = "CONTACTS"
End If

'-> Cr�er l'icone des recrutements
If aSite.IsRecrutement Then
    MdiMain.TreeNaviga.Nodes.Add , , "RECRUTEMENTS", "Recrutement", "Close"
    MdiMain.TreeNaviga.Nodes("RECRUTEMENTS").ExpandedImage = "Open"
    MdiMain.TreeNaviga.Nodes("RECRUTEMENTS").Tag = "RECRUTEMENTS"
End If

'-> Cr�er les icones du site
MdiMain.TreeNaviga.Nodes.Add aNode.Key, 4, "MAPPING", "Site publi� : " & aSite.Path, "Site"
MdiMain.TreeNaviga.Nodes.Add aNode.Key, 4, "MAPPINGTEMPO", "Site temporaire : " & aSite.Tempo, "Site"
MdiMain.TreeNaviga.Nodes.Add aNode.Key, 4, "URL", "Site Internet : " & aSite.Url, "Web"
MdiMain.TreeNaviga.Nodes("URL").Tag = "URL"

'-> Cr�er l'icone de l'historique
MdiMain.TreeNaviga.Nodes.Add aNode.Key, 4, "HISTO", "Historique de la publication", "Histo"
MdiMain.TreeNaviga.Nodes("HISTO").Tag = "HISTO"

'-> Cr�er l'icone du gestionnaire de contact
If aSite.IsContact Or aSite.IsRecrutement Then
    MdiMain.TreeNaviga.Nodes.Add aNode.Key, 4, "BIBLI", "Archivage", "Histo"
    MdiMain.TreeNaviga.Nodes("BIBLI").Tag = "BIBLI"
End If

'-> Faire l'affichage des icones dans le treeview
For Each aPage In aSite.Pages
    '-> Ajouter le node de la page
    Set aNode2 = MdiMain.TreeNaviga.Nodes.Add(aNode.Key, 4, aPage.Key, aPage.Libel, "Page")
    aNode2.Tag = "PAGE"
    '-> Ajouter le path de la page
    MdiMain.TreeNaviga.Nodes.Add aNode2.Key, 4, aNode2.Key & "�MAPPING", "Chemin : " & aPage.Path, "Site"
    For Each aOnglet In aPage.Onglets
        '-> Ajouter le node de l'onglet
        Set aNode3 = MdiMain.TreeNaviga.Nodes.Add(aNode2.Key, 4, aPage.Key & "�" & aOnglet.Key, aOnglet.Libel, "Onglet")
        aNode3.Tag = "ONGLET"
        '-> Ajouter le path de 'onglet
        MdiMain.TreeNaviga.Nodes.Add aNode3.Key, 4, aNode3.Key & "|MAPPING", "Chemin : " & aOnglet.Path, "Site"
        For Each aElement In aOnglet.Elements
            '-> Ajouter le node de l'�l�ment
            Set aNode4 = MdiMain.TreeNaviga.Nodes.Add(aNode3, 4, aNode3.Key & "�" & aElement.Key, aElement.Fichier, "Element")
            aNode4.Tag = "HTML"
        Next 'Pour tous les �l�ments de l'onglet
    Next 'Pour tous les onglets de la page
Next 'Pour toutes les pages du site

'-> Ajouter la liste des contacts
If aSite.IsContact Then RefreshContact

'-> Ajouter la liste des recrutements
If aSite.IsRecrutement Then RefreshRecrutement

Exit Sub

GestError:

    Select Case ErrorCode
    
        Case 1 '-> MAPPING
            MsgBox "Impossible de trouver la cle MAPPING dans le section [SITE] du fichier " & NomSite, vbCritical + vbOKOnly, "Erreur"
        Case 2 '-> URL
            MsgBox "Impossible de trouver la cle URL dans le section [SITE] du fichier " & NomSite, vbCritical + vbOKOnly, "Erreur"
        Case 3
            MsgBox "Impossible de trouver la cle PAGE dans la cle [PAGES] du fichier " & NomSite, vbCritical + vbOKOnly, "Erreur"
        Case 4
            MsgBox LibCode, vbCritical + vbOKOnly, "Erreur"
            GoTo Reprise
    End Select
    
    '-> Lib�rer le pointeur vers le site
    Set aSite = Nothing

End Sub

Public Sub RefreshContact()

'---> Cette proc�dure affiche la liste des contacts

Dim aNode As Node
Dim FileName As String
Dim aContact As Contact

'-> Supprimer dans un premier temps les contacts existants
Do While MdiMain.TreeNaviga.Nodes("CONTACT").Children <> 0
    '-> Pointer sur le contact
    Set aContact = Contacts(MdiMain.TreeNaviga.Nodes("CONTACT").Child.Key)
    '-> Fermer la feuille si elle est ouverte
    If Not aContact.aFrm Is Nothing Then Unload aContact.aFrm
    '-> Supprimer le node
    MdiMain.TreeNaviga.Nodes.Remove (aContact.Fichier)
    '-> Supprimer l'objet
    Contacts.Remove (aContact.Fichier)
Loop

'-> Analyse des contacts
FileName = Dir$(aSite.Contact & "\*.dct", vbNormal)
Do While FileName <> ""
    '-> Cr�er un pointeur vers un object contact
    Set aContact = New Contact
    '-> Setting des propri�t�s
    aContact.Fichier = UCase$(FileName)
    '-> Ajout dans la collection
    Contacts.Add aContact, UCase$(FileName)
    '-> Cr�er le node
    Set aNode = MdiMain.TreeNaviga.Nodes.Add(MdiMain.TreeNaviga.Nodes("CONTACT"), 4, UCase$(FileName), FileName, "Contact")
    aNode.Tag = "CONTACT"
    '-> Fichier suivant
    FileName = Dir
Loop 'Pour tous les contacts

'-> Ouvrir le r�pertoire des contacts
If MdiMain.TreeNaviga.Nodes("CONTACT").Children <> 0 Then MdiMain.TreeNaviga.Nodes("CONTACT").Expanded = True

End Sub

Public Sub RefreshRecrutement()

'---> Cette proc�dure affiche la liste des recrutement

Dim aNode As Node
Dim FileName As String
Dim aRecrut As Recrutement

'-> Supprimer dans un premier temps les contacts existants
Do While MdiMain.TreeNaviga.Nodes("RECRUTEMENTS").Children <> 0
    '-> Pointer sur le recrutement
    Set aRecrut = Recrutements(MdiMain.TreeNaviga.Nodes("RECRUTEMENTS").Child.Key)
    '-> Fermer la feuille si elle est ouverte
    If Not aRecrut.aFrm Is Nothing Then Unload aRecrut.aFrm
    '-> Supprimer le node
    MdiMain.TreeNaviga.Nodes.Remove (aRecrut.Fichier)
    '-> Supprimer l'objet
    Recrutements.Remove (aRecrut.Fichier)
Loop

'-> Analyse des contacts
FileName = Dir$(aSite.Recrutement & "\*.dct", vbNormal)
Do While FileName <> ""
    '-> Cr�er un pointeur vers un object contact
    Set aRecrut = New Recrutement
    '-> Setting des propri�t�s
    aRecrut.Fichier = UCase$(FileName)
    '-> Ajout dans la collection
    Recrutements.Add aRecrut, UCase$(FileName)
    '-> Cr�er le node
    Set aNode = MdiMain.TreeNaviga.Nodes.Add(MdiMain.TreeNaviga.Nodes("RECRUTEMENTS"), 4, UCase$(FileName), FileName, "Contact")
    aNode.Tag = "RECRUTEMENT"
    '-> Fichier suivant
    FileName = Dir
Loop 'Pour tous les contacts

'-> Ouvrir le r�pertoire des contacts
If MdiMain.TreeNaviga.Nodes("RECRUTEMENTS").Children <> 0 Then MdiMain.TreeNaviga.Nodes("RECRUTEMENTS").Expanded = True

End Sub



Public Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Integer
Dim PosEnCour As Integer
Dim i As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
    

End Function

Public Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Integer

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Integer
Dim i As Integer
Dim PosAnalyse As Integer

On Error GoTo GestError

PosAnalyse = 1

Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1

Exit Function

GestError:

    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
    

End Function
Public Sub Main()

MdiMain.Show
frmDemarrage.Show vbModal
frmPassWord.IsLoadingApp = True
frmPassWord.Show vbModal

End Sub

Public Sub Publication(ByRef aElement As Element)

'---> Cette proc�dure publie un element du site Temporaire vers le site d�finitif

Dim DefNom As String '-> R�cup�rer le nom du fichier HTM
Dim FileName As String '-> Nom d'un fichier pour DIR
Dim hdlFile As Integer '-> handle d'ouverture de fichier

On Error GoTo GestError

'-> Setting de l'�l�ment
DefNom = Entry(1, aElement.Fichier, ".")

'-> V�rifier si le fichier destination existe
If Dir$(aElement.PathToPublish & "\" & aElement.Fichier, vbNormal) <> "" Then
    '-> Supprimer la destination
    Kill aElement.PathToPublish & "\" & aElement.Fichier
End If

'-> Faire une copie du fichier
FileCopy aElement.PathTempo & "\" & aElement.Fichier, aElement.PathToPublish & "\" & aElement.Fichier

'-> V�rifier si l'�l�ment que l'on copie est au format WORD ( avec un sous r�pertoire )
If Dir$(aElement.PathTempo & "\" & DefNom & "_fichiers", vbDirectory) <> "" Then
    '-> On a trouv� un sous r�pertoire : le copier + cr�er les sous fichiers
    
    '-> Cr�ation du r�pertoire de destination
    If Dir$(aElement.PathToPublish & "\" & DefNom & "_fichiers", vbDirectory) <> "" Then
        '-> Pas besoin de cr�er le r�pertoire, il existe : il faut supprimer les fichiers
        FileName = Dir$(aElement.PathToPublish & "\" & DefNom & "_fichiers\*.*", vbNormal)
        Do While FileName <> ""
            Kill aElement.PathToPublish & "\" & DefNom & "_fichiers\" & FileName
            FileName = Dir
        Loop
    End If

    '-> Copier du contenu du r�pertoire
    FileName = Dir$(aElement.PathTempo & "\" & DefNom & "_fichiers\*.*", vbNormal)
    Do While FileName <> ""
        FileCopy aElement.PathTempo & "\" & DefNom & "_fichiers\" & FileName, aElement.PathToPublish & "\" & DefNom & "_fichiers\" & FileName
        FileName = Dir
    Loop
End If 'Si element de type Word

'-> Mettre � jour l'historique de publication
If aSite.Histo <> "" Then
    '-> Ouvrir le fichier Historique
    hdlFile = FreeFile
    Open aSite.Histo For Append As #hdlFile
    Print #hdlFile, Now & " -> Publication de l'�l�ment : " & aElement.Fichier & " du site : " & aElement.PathTempo & " vers le site " & aElement.PathToPublish
    '-> Fermer le fichier
    Close #hdlFile
End If

Exit Sub

GestError:

    MsgBox "Erreur dans la publication de l'�l�ment : " & aElement.Fichier, vbCritical + vbOKOnly, "Erreur"
    MsgBox Err.Number & Err.Description

End Sub

