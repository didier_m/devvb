VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.MDIForm MdiMain 
   BackColor       =   &H8000000C&
   Caption         =   "Gestionnaire de site Deal Informatique"
   ClientHeight    =   8265
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   10005
   Icon            =   "MdiMain.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.PictureBox picSplit 
      Align           =   3  'Align Left
      BorderStyle     =   0  'None
      Height          =   8265
      Left            =   6000
      MousePointer    =   9  'Size W E
      ScaleHeight     =   8265
      ScaleWidth      =   105
      TabIndex        =   3
      Top             =   0
      Width           =   105
   End
   Begin VB.PictureBox picNaviga 
      Align           =   3  'Align Left
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   8265
      Left            =   0
      ScaleHeight     =   551
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   400
      TabIndex        =   0
      Top             =   0
      Width           =   6000
      Begin MSComDlg.CommonDialog cmDlg 
         Left            =   1200
         Top             =   3480
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         CancelError     =   -1  'True
      End
      Begin VB.PictureBox picTitre 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         Height          =   90
         Left            =   240
         ScaleHeight     =   6
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   128
         TabIndex        =   2
         Top             =   840
         Width           =   1920
      End
      Begin MSComctlLib.TreeView TreeNaviga 
         Height          =   1455
         Left            =   240
         TabIndex        =   1
         Top             =   1440
         Width           =   2295
         _ExtentX        =   4048
         _ExtentY        =   2566
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   960
         Top             =   4560
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   9
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MdiMain.frx":014A
               Key             =   "Open"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MdiMain.frx":02A4
               Key             =   "Contact"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MdiMain.frx":03FE
               Key             =   "Close"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MdiMain.frx":0558
               Key             =   "Element"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MdiMain.frx":06B2
               Key             =   "Page"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MdiMain.frx":080C
               Key             =   "Onglet"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MdiMain.frx":0966
               Key             =   "Site"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MdiMain.frx":0AC0
               Key             =   "Web"
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "MdiMain.frx":0C1A
               Key             =   "Histo"
            EndProperty
         EndProperty
      End
      Begin VB.Image imgClose 
         Height          =   165
         Left            =   2400
         Picture         =   "MdiMain.frx":0D74
         Top             =   240
         Width           =   165
      End
   End
   Begin VB.Menu muFichier 
      Caption         =   "&Fichier"
      Begin VB.Menu mnuPwd 
         Caption         =   "Mot de passe"
      End
      Begin VB.Menu mnuq 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOpen 
         Caption         =   "Ouvrir un site"
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Quitter"
      End
   End
   Begin VB.Menu mnuFenetre 
      Caption         =   "Fe&n�tre"
      Begin VB.Menu mnuSpool 
         Caption         =   "Volet d'exploration"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuMosaiqueH 
         Caption         =   "Mosa�que horizontale"
      End
      Begin VB.Menu mnuMosaiqueV 
         Caption         =   "Mosa�que verticale"
      End
      Begin VB.Menu mnuCascade 
         Caption         =   "Cascade"
      End
      Begin VB.Menu mnuReorIcone 
         Caption         =   "R�organiser les icones"
      End
   End
   Begin VB.Menu mnuDeal 
      Caption         =   "?"
      Begin VB.Menu mnuApropos 
         Caption         =   "A propos de ..."
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu mnuPublier 
         Caption         =   "Publier"
      End
      Begin VB.Menu mnuOpensite 
         Caption         =   "Ouvrir le site"
      End
      Begin VB.Menu mnuApercu 
         Caption         =   "Aper�u"
      End
      Begin VB.Menu mnuModif 
         Caption         =   "Modifier avec Word"
      End
      Begin VB.Menu mnuHisto 
         Caption         =   "Ouvrir l'historique"
      End
      Begin VB.Menu mnuOpenContact 
         Caption         =   "Ouvrir la fiche contact"
      End
      Begin VB.Menu mnuActualiser 
         Caption         =   "&Actualiser"
      End
      Begin VB.Menu mnuKillContact 
         Caption         =   "Supprimer le contact"
      End
      Begin VB.Menu mnuOpenRecrut 
         Caption         =   "Ouvrir la fiche recrutement"
      End
      Begin VB.Menu mnuRefresh 
         Caption         =   "&Actualiser"
      End
      Begin VB.Menu mnuKillRecrut 
         Caption         =   "Supprimer la fiche recrutement"
      End
      Begin VB.Menu mnuPrint 
         Caption         =   "Imprimer"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuGestContact 
         Caption         =   "Ouvrir le gestionnaire de contacts"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuArchivage 
         Caption         =   "Archiver"
      End
   End
End
Attribute VB_Name = "MdiMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim IsNaviga As Boolean '-> Indique si l'explorateur de projet est actif





Private Sub imgClose_Click()
mnuSpool_Click

End Sub

Private Sub MDIForm_Load()

'-> Explorateur affich�
IsNaviga = True

'-> Intialiser les collections
Set Contacts = New Collection
Set Recrutements = New Collection

DoEvents

End Sub

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)

'---> Demander la confirmation

Dim Rep As VbMsgBoxResult

Rep = MsgBox("Quitter maintenant", vbQuestion + vbYesNo, "Quitter")
If Rep = vbNo Then
    Cancel = 1
    Exit Sub
End If

'-> quitter
End

End Sub

Private Sub mnuActualiser_Click()

    RefreshContact

End Sub

Private Sub mnuApercu_Click()

Dim aPage As Page
Dim aOnglet As Onglet
Dim aElement As Element
Dim aFrm As frmPage

'-> Pointer sur la page sp�cifi�e
Set aPage = aSite.Pages(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))

'-> Pointer sur l'onglet
Set aOnglet = aPage.Onglets(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))

'-> Pointer sur l'�l�ment sp�cifi�
Set aElement = aOnglet.Elements(Entry(3, Me.TreeNaviga.SelectedItem.Key, "�"))

'-> V�rifier si le fichier sp�cifi� existe
If Dir$(aOnglet.Path & aElement.Fichier, vbNormal) = "" Then
    MsgBox "Impossible de trouver le fichier : " & aOnglet.Path & aElement.Fichier, vbCritical + vbOKOnly, "Erreur"
    Exit Sub
End If

'-> Afficher la page si necessaire
If aElement.aFrm Is Nothing Then
    '-> Charger la page en m�moire
    Set aFrm = New frmPage
    '-> l'affecter � l'objet �l�ment
    Set aElement.aFrm = aFrm
    '-> Affecter l'�l�ment � la page
    Set aFrm.aElement = aElement
End If

'-> Mettre � jour la propri�t� Fichier de la page de visu
aElement.aFrm.NomFichier = aOnglet.Path & aElement.Fichier

'-> Mettre � jour le caption de la feuille
aElement.aFrm.Caption = aElement.aFrm.Caption & aOnglet.Path & aElement.Fichier

'-> Afficher dans le browser internet
aElement.aFrm.WebBrowser1.Navigate aElement.aFrm.NomFichier

'-> Mettre la feuille au premier plan
aElement.aFrm.Show
aElement.aFrm.ZOrder


End Sub

Private Sub mnuApropos_Click()

'---> Afficher la page Apropos de
AproposDe.Show vbModal

End Sub

Private Sub mnuArchivage_Click()

Dim Rep As String
Dim hdlFile As Integer
Dim aContact As Contact
Dim aRecrut As Recrutement

On Error GoTo GestError

'-> Demander la confirmation
Rep = MsgBox("Archiver maintenant ? ", vbQuestion + vbYesNo, "Confirmation")
If Rep = vbNo Then Exit Sub

'-> Ouvrir le fichier d'archivage
hdlFile = FreeFile
Open aSite.ArchivageFile For Append As #hdlFile

'-> Lancer l'archivage selon la nature du node
Select Case Me.TreeNaviga.SelectedItem.Tag
    Case "CONTACTS"
        '-> Archiver la liste des contacts en cours
        For Each aContact In Contacts
            '-> Archiver le fichier
            If ArchiveContact(aContact.Fichier, hdlFile) Then
                '-> Supprimer le node dans le tree
                Me.TreeNaviga.Nodes.Remove (aContact.Fichier)
                '-> Fermer la feuille de contact si elle est ouverte
                If Not aContact.aFrm Is Nothing Then Unload aContact.aFrm
                '-> Supprimer l'objet de la collection des contacts
                Contacts.Remove aContact.Fichier
            End If
        Next ' Pour tous les fichiers contcts
    Case "CONTACT"
        '-> Pointer sur l'objet contact
        Set aContact = Contacts(Me.TreeNaviga.SelectedItem.Key)
        If ArchiveContact(aContact.Fichier, hdlFile) Then
            '-> Supprimer le node dans le tree
            Me.TreeNaviga.Nodes.Remove (aContact.Fichier)
            '-> Fermer la feuille de contact si elle est ouverte
            If Not aContact.aFrm Is Nothing Then Unload aContact.aFrm
            '-> Supprimer l'objet de la collection des contacts
            Contacts.Remove aContact.Fichier
        End If
    Case "RECRUTEMENTS"
    
    Case "RECRUTEMENT"
'        '-> Pointer sur l'objet de recrutement
'        Set aRecrut = Recrutements(Me.TreeNaviga.SelectedItem.Key)
'        '-> Archiver l' objet recrutement
'        If ArchiveRecrut(aRecrut, hdlFile) Then
'            '->
        
        
End Select

'-> Fermer le fichier d'archivage
Close #hdlFile

'-> Modifier l'icone
If Me.TreeNaviga.Nodes("CONTACT").Children = 0 Then
    Me.TreeNaviga.Nodes("CONTACT").Expanded = False
End If

Exit Sub

GestError:
    Close #hdlFile
    MsgBox "Erreur lors de l'arcgivage", vbCritical + vbOKOnly, "Erreur"

End Sub
Private Function ArchiveContact(ByVal FileToArchive As String, ByVal hdlFile As Integer) As Boolean

'---> Archiver Le fichier contact

Dim Ligne As String
Dim CtFile As Integer
Dim ErrorCode As Integer

On Error GoTo GestError

'-> V�rifier que le fichier ASCII existe bien toujours
If Dir$(aSite.Contact & "\" & FileToArchive, vbNormal) = "" Then
    MsgBox "Impossible de trouver le fichier " & FileToArchive, vbCritical + vbOKOnly, "Impossible d'archiver"
    Exit Function
End If
    
'-> Ouvrir le fichier ASCII
ErrorCode = 1
CtFile = FreeFile
Open aSite.Contact & "\" & FileToArchive For Input As #CtFile

'-> Lire la ligne
Line Input #CtFile, Ligne

'-> Fermer le fichier tempo
ErrorCode = 2
Close #CtFile

'-> Mettre � jour le fichieur d'archivage
ErrorCode = 3
Print #hdlFile, "CONTACT~" & Ligne

'-> Supprimer le fichier Contact
ErrorCode = 4
Kill aSite.Contact & "\" & FileToArchive

ArchiveContact = True

Exit Function

GestError:
    
    Select Case ErrorCode
        Case 1 'Ouverture du fichier
            MsgBox "Impossible d'ouvrir le fichier : " & FileToArchive & ". Ce fichier ne sera pas archiv�.", vbCritical + vbOKOnly, "Erreur d'archivage"
            ArchiveContact = False
        Case 2 'Fermeture du fichier
            '-> Forcer la fermeture du fichier
            Reset
            Resume Next
        Case 3 'Mise � jour du fichier d'archives
            MsgBox "Impossible d'archiver le fichier : " & FileToArchive, vbCritical + vbOKOnly, "Erreur d'archivage"
            ArchiveContact = False
        Case 4 'Suppression du fichier Contact
            MsgBox "Archivage du fichier : " & FileToArchive & " effectu�, mais impossible de supprimer ce fichier. Veuillez le supprimer manuellement.", vbExclamation + vbOKOnly, "Erreur lors de la suppression du fichier"
            ArchiveContact = True
    End Select
    

End Function

Private Function ArchiveRecrut(ByVal FileToArchive As String, ByVal hdlFile As Integer) As Boolean

'---> Cette proc�dure archive un fichier de recrutement

'Dim Ligne As String
'Dim CtFile As Integer
'Dim ErrorCode As Integer
'
''On Error GoTo GestError
'
''-> V�rifier que le fichier ASCII existe bien toujours
'If Dir$(aSite.Recrutement & "\" & FileToArchive, vbNormal) = "" Then
'    MsgBox "Impossible de trouver le fichier " & FileToArchive, vbCritical + vbOKOnly, "Impossible d'archiver"
'    Exit Function
'End If
'
''-> Ouvrir le fichier ASCII
'ErrorCode = 1
'CtFile = FreeFile
'Open aSite.Contact & "\" & FileToArchive For Input As #CtFile
'
'
'


End Function



Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuGestContact_Click()

'-> V�rifier si la feuill est charg�e
If frmBibli.Visible = True Then
    If Not frmBibli.Init Then Exit Sub
Else
    Load frmBibli
    If Not frmBibli.Init Then Exit Sub
End If

frmBibli.Visible = True
frmBibli.ZOrder

End Sub

Private Sub mnuHisto_Click()

'---> Affichage de l'historique

Dim hdlFile As Integer
Dim Ligne As String

'-> Charger la feuille de l'historique
If frmHisto.Visible = False Then
    Load frmHisto
Else
    '-> vider le treeview
    frmHisto.TreeView1.Nodes.Clear
End If

'-> Ouverture du fichier Historique
hdlFile = FreeFile
Open aSite.Histo For Input As #hdlFile
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    If Trim(Ligne) <> "" And UCase$(Ligne) <> "[PUBLICATIONS]" Then
        '-> Ajouter le node
        frmHisto.TreeView1.Nodes.Add , , "NODE|" & frmHisto.TreeView1.Nodes.Count, Ligne, "Web"
    End If
Loop

'-> Fermer le fichier
Close #hdlFile

'-> Afficher la feuille
frmHisto.Show
frmHisto.ZOrder

End Sub

Private Sub mnuKillContact_Click()

On Error GoTo GestError

Dim Rep As VbMsgBoxResult
Dim aContact As Contact

'-> Demander la confirmation
Rep = MsgBox("Supprimer le fichier contact : " & Me.TreeNaviga.SelectedItem.Text, vbYesNo, "Confirmation")
If Rep = vbNo Then Exit Sub

'-> Supprimer le fichier sp�cifi�
Kill aSite.Contact & "\" & Me.TreeNaviga.SelectedItem.Text

'-> Fermer la feuille si elle est ouverte
Set aContact = Contacts(Me.TreeNaviga.SelectedItem.Key)
If Not aContact.aFrm Is Nothing Then Unload aContact.aFrm

'-> Supprimer l'objet contact
Contacts.Remove (Me.TreeNaviga.SelectedItem.Key)

'-> Supprimer le node
Me.TreeNaviga.Nodes.Remove (Me.TreeNaviga.SelectedItem.Key)

'-> Si on a supprimer le dernier Node : modifier l'image
If Me.TreeNaviga.Nodes("CONTACT").Children = 0 Then Me.TreeNaviga.Nodes("CONTACT").Expanded = False

Exit Sub

GestError:

    MsgBox "Erreur dans la suppression du fichier contact : " & Me.TreeNaviga.SelectedItem.Text, vbCritical + vbOKOnly, "Erreur"


End Sub

Private Sub mnuKillRecrut_Click()

On Error GoTo GestError

Dim Rep As VbMsgBoxResult
Dim aRecrut As Recrutement

'-> Demander la confirmation
Rep = MsgBox("Supprimer le fichier recrutement : " & Me.TreeNaviga.SelectedItem.Text, vbYesNo, "Confirmation")
If Rep = vbNo Then Exit Sub

'-> Supprimer le fichier sp�cifi�
Kill aSite.Recrutement & "\" & Me.TreeNaviga.SelectedItem.Text

'-> Fermer la feuille si elle est ouverte
Set aRecrut = Recrutements(Me.TreeNaviga.SelectedItem.Key)
If Not aRecrut.aFrm Is Nothing Then Unload aRecrut.aFrm

'-> Supprimer l'objet contact
Recrutements.Remove (Me.TreeNaviga.SelectedItem.Key)

'-> Supprimer le node
Me.TreeNaviga.Nodes.Remove (Me.TreeNaviga.SelectedItem.Key)

'-> Si on a supprimer le dernier Node : modifier l'image
If Me.TreeNaviga.Nodes("RECRUTEMENTS").Children = 0 Then Me.TreeNaviga.Nodes("RECRUTEMENTS").Expanded = False

Exit Sub

GestError:

    MsgBox "Erreur dans la suppression du fichier recrutement : " & Me.TreeNaviga.SelectedItem.Text, vbCritical + vbOKOnly, "Erreur"

End Sub

Private Sub mnuModif_Click()

Dim aWord As Object
Dim aPage As Page
Dim aOnglet As Onglet
Dim aElement As Element
Dim aFrm As frmPage

On Error GoTo GestError

'-> Pointer sur la page sp�cifi�e
Set aPage = aSite.Pages(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))

'-> Pointer sur l'onglet
Set aOnglet = aPage.Onglets(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))

'-> Pointer sur l'�l�ment sp�cifi�
Set aElement = aOnglet.Elements(Entry(3, Me.TreeNaviga.SelectedItem.Key, "�"))

'-> V�rifier si le fichier sp�cifi� existe
If Dir$(aOnglet.Path & aElement.Fichier, vbNormal) = "" Then
    MsgBox "Impossible de trouver le fichier : " & aOnglet.Path & aElement.Fichier, vbCritical + vbOKOnly, "Erreur"
    Exit Sub
End If

'-> Cr�ation d'un lien OLE
Set aWord = CreateObject("Word.Application")

aWord.Documents.Open aOnglet.Path & aElement.Fichier

aWord.Visible = True

GestError:

Set aWord = Nothing


End Sub

Private Sub mnuOpen_Click()

On Error GoTo GestError

Dim Rep As String

'-> bloquer l'�cran
Me.Enabled = False
Me.MousePointer = 11

'-> Tester si un site est en cours
If Not aSite Is Nothing Then
    Rep = MsgBox("Fermer le site en cours ?", vbQuestion + vbYesNo, "Question")
    If Rep = vbNo Then GoTo GestError
    '-> Vider le site
    ClearSite
End If

'-> Flags d'ouverture
Me.cmDlg.Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly Or _
                   cdlOFNPathMustExist
Me.cmDlg.FileName = "*.dgs"
Me.cmDlg.Filter = "Fichiers de gestion de site ( *.dgs)|*.dgs"
Me.cmDlg.InitDir = App.Path & "\Datas"
'-> Afficher la feuille
Me.cmDlg.ShowOpen

'-> Ouvrir le fichier Site
OpenSite Me.cmDlg.FileName

GestError:

    Me.Enabled = True
    Me.MousePointer = 0

End Sub

Private Sub ClearSite()

'---> Cette proc�dure ferme le site en cours

Dim aOnglet As Onglet
Dim aPage As Page
Dim aElement As Element
Dim aContact As Contact
Dim aRecrut As Recrutement

'-> Fermer toutes les fen�tres actives
For Each aPage In aSite.Pages
    For Each aOnglet In aPage.Onglets
        For Each aElement In aOnglet.Elements
            '-> Fermer la feuille en cours
            If Not aElement.aFrm Is Nothing Then Unload aElement.aFrm
        Next 'Pour tous les �l�ments
    Next '-> Pour tous les onglets
Next '-> Pour toutes les pages

'-> Vider les contacts
For Each aContact In Contacts
    If Not aContact.aFrm Is Nothing Then Unload aContact.aFrm
Next

'-> Vider eles recurements
For Each aRecrut In Recrutements
    If Not aRecrut.aFrm Is Nothing Then Unload aRecrut.aFrm
Next

'-> Fermer la feuille de l'hista en cours
If frmHisto.Visible Then Unload frmHisto

'-> Vider les collections du site
Set aSite = Nothing

'-> Supprimer les contacts
Do While Contacts.Count <> 0
    Contacts.Remove (1)
Loop

'-> Supprimer les recrutements
Do While Recrutements.Count <> 0
    Recrutements.Remove (1)
Loop

'-> Vider le treeveiw
Me.TreeNaviga.Nodes.Clear

End Sub

Private Sub mnuOpenContact_Click()

Dim aFrm As New frmContact
Dim aContact As Contact

'-> V�rifier que le fichier existe
If Dir$(aSite.Contact & "\" & Me.TreeNaviga.SelectedItem.Key, vbNormal) = "" Then
    MsgBox "Impossible de trouver le fichier : " & aSite.Contact & "\" & Me.TreeNaviga.SelectedItem.Key, vbCritical + vbOKOnly, "Erreur"
    Exit Sub
End If

'-> Pointer sur l'objet Contatc
Set aContact = Contacts(Me.TreeNaviga.SelectedItem.Key)

'-> V�rifier que la feuille contact n'est pas affich�e
If aContact.aFrm Is Nothing Then

    '-> Afficher le titre
    aFrm.Caption = aFrm.Caption & Me.TreeNaviga.SelectedItem.Key
    aFrm.Fichier = aSite.Contact & "\" & Me.TreeNaviga.SelectedItem.Key
    aFrm.Init
    aFrm.Show
    aFrm.ZOrder
    
    '-> Affecter l'objet � la feuille
    Set aFrm.aContact = aContact
    
    '-> Affecter la feuille � l'objet
    Set aContact.aFrm = aFrm
    
    Exit Sub
End If

aContact.aFrm.ZOrder

End Sub

Private Sub mnuOpenRecrut_Click()


Dim aFrm As New frmRecrut
Dim aRecrut As Recrutement

'-> V�rifier que le fichier existe
If Dir$(aSite.Recrutement & "\" & Me.TreeNaviga.SelectedItem.Key, vbNormal) = "" Then
    MsgBox "Impossible de trouver le fichier : " & aSite.Recrutement & "\" & Me.TreeNaviga.SelectedItem.Key, vbCritical + vbOKOnly, "Erreur"
    Exit Sub
End If

'-> Pointer sur l'objet recrutement
Set aRecrut = Recrutements(Me.TreeNaviga.SelectedItem.Key)

'-> V�rifier que la feuille contact n'est pas affich�e
If aRecrut.aFrm Is Nothing Then

    '-> Afficher le titre
    aFrm.Caption = aFrm.Caption & Me.TreeNaviga.SelectedItem.Key
    aFrm.Fichier = aSite.Recrutement & "\" & Me.TreeNaviga.SelectedItem.Key
    aFrm.Init
    aFrm.Show
    aFrm.ZOrder
    
    '-> Affecter l'objet � la feuille
    Set aFrm.aRecrut = aRecrut
    
    '-> Affecter la feuille � l'objet
    Set aRecrut.aFrm = aFrm
    
    Exit Sub
End If

aRecrut.aFrm.ZOrder

End Sub

Private Sub mnuOpensite_Click()

Dim http As Long
    
http = ShellExecute(hwnd, "Open", aSite.Url, vbNullString, App.Path, 1)

End Sub
Private Sub PrintRecrut(ByVal Key As String, ByVal hdlFile As Integer)

'---> Cette proc�dure imprime un contact dans un fichier

Dim aRecrut As Recrutement
Dim CtFile As Integer
Dim Ligne As String
Dim i As Integer
Dim Libelle As String


'-> Pointer sur le contact
Set aRecrut = Recrutements(Key)

'-> Ouvrir le fichier contact
CtFile = FreeFile
Open aSite.Recrutement & "\" & aRecrut.Fichier For Input As #CtFile

'-> Lire la ligne
Line Input #CtFile, Ligne

'-> Impression de l'ent�te dans le fichier sp�cifi�
Print #hdlFile, "[ST-ENTETE(TXT-SECTION)[]{^0001" & Key & " ^0002 " & Entry(NumEntries(Ligne, "|"), Ligne, "|") & " }"

'-> Impression des fonds de page
Print #hdlFile, "[TB-NOUVEAUTABLEAU4(BLK-NouveauBlock1)][\1]{}"
Print #hdlFile, "[TB-NOUVEAUTABLEAU5(BLK-NouveauBlock1)][\1]{}"
Print #hdlFile, "[TB-NOUVEAUTABLEAU6(BLK-NouveauBlock1)][\1]{}"

'-> Impression des datas
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\1]{^0001" & Entry(NumEntries(Ligne, "|"), Ligne, "|") & " }" 'Date et heure
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\2]{^0002" & Entry(1, Ligne, "|") & " }" 'Nom
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\3]{^0003" & Entry(2, Ligne, "|") & " }" 'Prenom
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\4]{}"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\5]{^0005" & Entry(3, Ligne, "|") & " }" 'Adresse 1
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\6]{^0006" & Entry(4, Ligne, "|") & " }" 'Adresse 2
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\7]{^0007" & Entry(5, Ligne, "|") & " }" 'Adresse 3
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\8]{^0008" & Entry(6, Ligne, "|") & " " & Entry(7, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\9]{^0009" & Entry(8, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\10]{^0010" & Entry(9, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\11]{^0011" & Entry(10, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\12]{}"

'-> Gestion du cv et du cursus
Libelle = ""
Do While Not EOF(CtFile)
    '-> Lecture de la ligne
    Line Input #CtFile, Ligne
    If Trim(Ligne) <> "" Then
        If Libelle = "" Then
            Libelle = Ligne
        Else
            Libelle = Libelle & " " & Ligne
        End If
    End If
Loop 'Boucle d'analyse du fichier

'-> Fermer le fichier
Close #CtFile

'-> Imprimer dans la maquette
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\13]{^0012" & Libelle & " }"


End Sub



Private Sub PrintContact(ByVal Key As String, ByVal hdlFile As Integer)

'---> Cette proc�dure imprime un contact dans un fichier

Dim aContact As Contact
Dim CtFile As Integer
Dim Ligne As String
Dim i As Integer
Dim Libelle As String

'-> Pointer sur le contact
Set aContact = Contacts(Key)

'-> Ouvrir le fichier contact
CtFile = FreeFile
Open aSite.Contact & "\" & aContact.Fichier For Input As #CtFile

'-> Lire la ligne
Line Input #CtFile, Ligne

'-> Fermer le fichier
Close #CtFile

'-> Impression de l'ent�te dans le fichier sp�cifi�
Print #hdlFile, "[ST-ENTETE(TXT-SECTION)[]{^0001" & Key & " ^0002 " & Entry(NumEntries(Ligne, "|"), Ligne, "|") & " }"

'-> Impression des fonds de page
Print #hdlFile, "[TB-NOUVEAUTABLEAU4(BLK-NouveauBlock1)][\1]{}"
Print #hdlFile, "[TB-NOUVEAUTABLEAU5(BLK-NouveauBlock1)][\1]{}"
Print #hdlFile, "[TB-NOUVEAUTABLEAU6(BLK-NouveauBlock1)][\1]{}"
Print #hdlFile, "[TB-NOUVEAUTABLEAU7(BLK-NouveauBlock1)][\1]{}"

'-> Impression des datas
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\1]{^0001" & Entry(1, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\2]{^0002" & Entry(2, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\3]{^0003" & Entry(3, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\4]{^0004" & Entry(4, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\5]{}"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\6]{^0005" & Entry(5, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\7]{^0006" & Entry(6, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\8]{^0007" & Entry(7, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\9]{^0008" & Entry(8, Ligne, "|") & " " & Entry(9, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\10]{^0009" & Entry(10, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\11]{^0010" & Entry(11, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\12]{^0011" & Entry(12, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\13]{}"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\14]{^0012" & Entry(13, Ligne, "|") & " }"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\15]{}"
Print #hdlFile, "[TB-FICHE(BLK-Entete)][\16]{^0013" & Entry(20, Ligne, "|") & " }"

'-> Block de s�paration
Print #hdlFile, "[TB-NOUVEAUTABLEAU8(BLK-NouveauBlock1)][\1]{}"

'-> Choix du Contact
For i = 14 To 19
    Select Case i
    
        Case 14
            Libelle = "^0001 ERP Grands Comptes"
        Case 15
            Libelle = "^0001 DEALPACK PGI Pme-Pmi"
        Case 16
            Libelle = "^0001 NATHALIE Agro-Agri"
        Case 17
            Libelle = "^0001 BACCHUS Vins && spiritueux"
        Case 18
            Libelle = "^0001 Une documentation"
        Case 19
            Libelle = "^0001 Un Contact T�l�phonique"
    End Select
            
    If UCase$(Entry(i, Ligne, "|")) = "ON" Then
        Print #hdlFile, "[ST-ERP_OK(TXT-SECTION)[]{ " & Libelle & " }"
    Else
        Print #hdlFile, "[ST-ERP_FALSE(TXT-SECTION)[]{ " & Libelle & " }"
    End If
Next



End Sub

Private Sub mnuPrint_Click()

'---> Cette fonction Imprime soit un contact, soit une fiche de recrutement

Dim hdlFile As Integer
Dim TempFileName As String
Dim aContact As Contact

'-> Ouverture du fichier temporaire d'impression
TempFileName = GetTempFileNameVB("IMP")
hdlFile = FreeFile
Open TempFileName For Output As #hdlFile

'-> Selon le node sur lequel on click
Select Case Me.TreeNaviga.SelectedItem.Tag

    Case "CONTACT"
        For Each aContact In Contacts
            '-> Imprimer l'emplacement de la maquette
            Print #hdlFile, "%%GUI%%" & App.Path & "\Datas\tws-contact.maqgui"

            PrintContact aContact.Fichier, hdlFile
            Print #hdlFile, "[PAGE]"
        Next
    
    Case "RECRUTEMENT"
        '-> Imprimer l'emplacement de la maquette
        Print #hdlFile, "%%GUI%%" & App.Path & "\Datas\tws-recrut.maqgui"

        PrintRecrut Me.TreeNaviga.SelectedItem.Key, hdlFile
    
End Select

'-> Fermer le fichier
Close #hdlFile

'-> Lancer l'impression
Shell App.Path & "\TurboGraph.exe " & TempFileName, vbMaximizedFocus

End Sub

Private Sub mnuPublier_Click()

Dim aOnglet As Onglet
Dim aPage As Page
Dim aElement As Element

Select Case NumEntries(Me.TreeNaviga.SelectedItem.Key, "�")

    Case 1 '-> Publication d'une page ou du site
        If Me.TreeNaviga.SelectedItem.Key = "SITE" Then
            '-> Publication du site
            For Each aPage In aSite.Pages
                For Each aOnglet In aPage.Onglets
                    For Each aElement In aOnglet.Elements
                        '-> V�rifier que le r�pertoire de destination est ouvert
                        If Dir$(aElement.PathToPublish, vbDirectory) = "" Then
                            MsgBox "Impossible de publier l'�l�ment : " & aElement.Fichier & " de l'onglet : " & aOnglet.Libel & " de la page : " & aPage.Libel, vbCritical + vbOKOnly, "Erreur"
                        Else
                            Publication aElement
                        End If 'Si destination existe
                    Next 'Pour tous les �lements
                Next 'Pour tous les onglets
            Next 'Pour toutes les pages
        Else
            '-> Publication d'une page
            Set aPage = aSite.Pages(Me.TreeNaviga.SelectedItem.Key)
            For Each aOnglet In aPage.Onglets
                For Each aElement In aOnglet.Elements
                    '-> V�rifier que le r�pertoire de destination est ouvert
                    If Dir$(aElement.PathToPublish, vbDirectory) = "" Then
                        MsgBox "Impossible de publier l'�l�ment : " & aElement.Fichier & " de l'onglet : " & aOnglet.Libel & " de la page : " & aPage.Libel, vbCritical + vbOKOnly, "Erreur"
                    Else
                        Publication aElement
                    End If 'Si destination existe
                Next 'Pour tous les �lements
            Next 'Pour tous les onglets
        End If
    
    Case 2 '-> Publication d'un onglet
    
        '-> Pointer sur la page
        Set aPage = aSite.Pages(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))
        '-> Pointer sur l'onglet
        Set aOnglet = aPage.Onglets(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))
        '-> Publier l'onglet
        For Each aElement In aOnglet.Elements
            '-> V�rifier que le r�pertoire de destination est ouvert
            If Dir$(aElement.PathToPublish, vbDirectory) = "" Then
                MsgBox "Impossible de publier l'�l�ment : " & aElement.Fichier & " de l'onglet : " & aOnglet.Libel & " de la page : " & aPage.Libel, vbCritical + vbOKOnly, "Erreur"
            Else
                Publication aElement
            End If 'Si destination existe
        Next
    Case 3 '-> Publication d'un �l�ment
        '-> Pointer sur la page
        Set aPage = aSite.Pages(Entry(1, Me.TreeNaviga.SelectedItem.Key, "�"))
        '-> Pointer sur l'onglet
        Set aOnglet = aPage.Onglets(Entry(2, Me.TreeNaviga.SelectedItem.Key, "�"))
        '-> Pointer sur l'�l�ment
        Set aElement = aOnglet.Elements(Entry(3, Me.TreeNaviga.SelectedItem.Key, "�"))
        '-> V�rifier que le r�pertoire de destination est ouvert
        If Dir$(aElement.PathToPublish, vbDirectory) = "" Then
            MsgBox "Impossible de publier l'�l�ment : " & aElement.Fichier & " de l'onglet : " & aOnglet.Libel & " de la page : " & aPage.Libel, vbCritical + vbOKOnly, "Erreur"
        Else
            Publication aElement
        End If 'Si destination existe
End Select

MsgBox "Elements Publi�s.", vbInformation + vbOKOnly, "Confirmation"


End Sub

Private Sub mnuPwd_Click()

'-> vider la variable de retour
StrRetour = ""

'-> Confirmer par ancien mot de pass
frmPassWord.IsLoadingApp = False
frmPassWord.Show vbModal

'-> Quitter si pas de confirmation de mot de passe
If StrRetour = "" Then Exit Sub

'-> Afficher la feuille de gestion du mot de passe
frmGestPwd.Show vbModal

End Sub

Private Sub mnuRefresh_Click()

'---> Cette proc�dure analyse le r�pertoire des fichiers de recrutement
RefreshRecrutement

End Sub

Private Sub picNaviga_Resize()

Dim aRect As RECT
Dim Res As Long

On Error Resume Next

'-> R�cup�rer la taille de la zone cliente
Res = GetClientRect(Me.picNaviga.hwnd, aRect)

'-> Positionner l'entete de la fen�tre
Me.picTitre.Left = 3
Me.picTitre.Top = 7
Me.picTitre.Width = aRect.Right - 6 - imgClose.Width
Me.imgClose.Left = aRect.Right - imgClose.Width
Me.imgClose.Top = Me.picTitre.Top - 2

'-> Positionner le treeview
Me.TreeNaviga.Left = Me.picTitre.Left
Me.TreeNaviga.Top = Me.picTitre.Top * 2 + Me.picTitre.Height
Me.TreeNaviga.Width = Me.picTitre.Width + Me.imgClose.Width + 6
Me.TreeNaviga.Height = Me.picNaviga.ScaleY(Me.picNaviga.Height, 1, 3) - 21

End Sub

Private Sub picSplit_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim apt As POINTAPI
Dim Res As Long

If Button = vbLeftButton Then
    '-> R�cup�rer la position du curseur
    Res = GetCursorPos(apt)
    '-> Convertir en coordonn�es clientes
    Res = ScreenToClient(Me.hwnd, apt)
    '-> Tester les positions mini et maxi
    If apt.x > Me.picNaviga.ScaleX(2, 7, 3) And apt.x < Me.picNaviga.ScaleX(Me.Width, 1, 3) - Me.picNaviga.ScaleX(2, 7, 3) Then Me.picNaviga.Width = Me.picNaviga.ScaleX(apt.x, 3, 1)
End If

End Sub



Private Sub picTitre_Resize()

'---> Dessiner la barre de titre
Me.picTitre.Line (0, 0)-(Me.picTitre.Width - 1, 0), RGB(255, 255, 255)
Me.picTitre.Line (0, 1)-(Me.picTitre.Width - 1, 1), RGB(255, 255, 255)
Me.picTitre.Line (1, 1)-(Me.picTitre.Width - 1, 1), &HE0E0E0
Me.picTitre.Line (0, 3)-(Me.picTitre.Width - 1, 3), RGB(255, 255, 255)
Me.picTitre.Line (0, 4)-(Me.picTitre.Width - 1, 4), RGB(255, 255, 255)
Me.picTitre.Line (1, 4)-(Me.picTitre.Width - 1, 4), &HE0E0E0

End Sub

Private Sub mnuMosaiqueH_Click()

Me.Arrange vbTileHorizontal

End Sub

Private Sub mnuMosaiqueV_Click()

Me.Arrange vbTileVertical

End Sub

Private Sub mnuReorIcone_Click()

Me.Arrange vbArrangeIcons

End Sub


Private Sub mnuSpool_Click()

'-> Afficher ou ne pas afficher le volet expmloration de spool
IsNaviga = Not IsNaviga
Me.mnuSpool.Checked = IsNaviga
Me.picSplit.Visible = IsNaviga
Me.picNaviga.Visible = IsNaviga

End Sub

Private Sub mnuCascade_Click()

Me.Arrange vbCascade

End Sub




Private Sub TreeNaviga_DblClick()

If Me.TreeNaviga.SelectedItem Is Nothing Then Exit Sub

If Me.TreeNaviga.SelectedItem.Tag = "HTML" Then mnuApercu_Click


End Sub

Private Sub TreeNaviga_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

'---> Afficher le menu Popup

Dim aNode As Node

'-> Tester le node sur lequel on click
Set aNode = Me.TreeNaviga.HitTest(x, y)
If aNode Is Nothing Then Exit Sub

'-> Le s�lectionner
Set Me.TreeNaviga.SelectedItem = aNode

'-> Quitter si pas bouton droit
If Button <> vbRightButton Then Exit Sub

'-> Masquer les menus
Me.mnuPublier.Visible = True
Me.mnuApercu.Visible = False
Me.mnuOpensite.Visible = False
Me.mnuModif.Visible = False
Me.mnuHisto.Visible = False
Me.mnuOpenContact.Visible = False
Me.mnuActualiser.Visible = False
Me.mnuKillContact.Visible = False
Me.mnuOpenRecrut.Visible = False
Me.mnuKillRecrut.Visible = False
Me.mnuRefresh.Visible = False
Me.mnuPrint.Visible = False
Me.mnuGestContact.Visible = False
Me.mnuArchivage.Visible = False

'-> Afficher les menus en visible ou non
Select Case aNode.Tag
    Case "SITE"
        Me.mnuPublier.Visible = True
    Case "PAGE"
        Me.mnuPublier.Visible = True
    Case "URL"
        Me.mnuOpensite.Visible = True
        Me.mnuPublier.Visible = False
    Case "ONGLET"
        Me.mnuPublier.Visible = True
    Case "HTML"
        Me.mnuApercu.Visible = True
        Me.mnuModif.Visible = True
        Me.mnuPublier.Visible = True
    Case "HISTO"
        Me.mnuHisto.Visible = True
        Me.mnuPublier.Visible = False
    Case "BIBLI"
        Me.mnuGestContact.Visible = True
        Me.mnuPublier.Visible = False
    Case "CONTACT"
        Me.mnuOpenContact.Visible = True
        Me.mnuActualiser.Visible = True
        Me.mnuPublier.Visible = False
        Me.mnuKillContact.Visible = True
        Me.mnuPrint.Visible = True
        Me.mnuArchivage.Visible = True
    Case "CONTACTS"
        Me.mnuActualiser.Visible = True
        Me.mnuPrint.Visible = True
        Me.mnuPublier.Visible = False
        Me.mnuArchivage.Visible = True
    Case "RECRUTEMENT"
        Me.mnuOpenRecrut.Visible = True
        Me.mnuRefresh.Visible = True
        Me.mnuKillRecrut.Visible = True
        Me.mnuPublier.Visible = False
        Me.mnuPrint.Visible = True
        Me.mnuArchivage.Visible = True
    Case "RECRUTEMENTS"
        Me.mnuRefresh.Visible = True
        Me.mnuPrint.Visible = True
        Me.mnuPublier.Visible = False
        Me.mnuArchivage.Visible = True
    Case Else
        Exit Sub
End Select 'Selon le tag du node s�lectionn�

'-> Afficher les menus
Me.PopupMenu Me.mnuPopup

End Sub
