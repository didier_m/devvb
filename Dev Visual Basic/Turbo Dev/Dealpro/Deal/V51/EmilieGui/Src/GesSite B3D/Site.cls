VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Site"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Cette classe gere les différentes pages d'un site
Public Url As String '-> URL du site
Public Path As String '-> Path du site
Public Tempo As String  '-> Path du site tempo
Public Pages As Collection '-> Liste des pages dans le site
Public Contact As String '-> Path du répertoire des contacts
Public Histo As String '-> Nom du fichier historique
Public Recrutement As String '-> Path du répertoire des recrutements
Public ArchivageFile As String '-> Path du fichier d'archivage

Public IsContact As Boolean '-> Indique si gestion des contacts
Public IsRecrutement As Boolean  '-> Indique si gestion des recrutements


Private Sub Class_Initialize()

'---> Initialiser la collection des pages du site
Set Pages = New Collection

End Sub
