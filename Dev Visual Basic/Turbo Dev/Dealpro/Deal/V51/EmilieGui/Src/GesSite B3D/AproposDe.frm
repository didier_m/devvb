VERSION 5.00
Begin VB.Form AproposDe 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "A propos de Deal Informatique ..."
   ClientHeight    =   4440
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5040
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4440
   ScaleWidth      =   5040
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture3 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   120
      Picture         =   "AproposDe.frx":0000
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   32
      TabIndex        =   3
      Top             =   120
      Width           =   480
   End
   Begin VB.PictureBox Picture2 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   120
      Picture         =   "AproposDe.frx":0CCA
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   32
      TabIndex        =   2
      ToolTipText     =   "Liste des e-mail"
      Top             =   2760
      Width           =   480
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   120
      Picture         =   "AproposDe.frx":346C
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   32
      TabIndex        =   1
      ToolTipText     =   "Liste des num�ros de fax"
      Top             =   1920
      Width           =   480
   End
   Begin VB.PictureBox Picture4 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   600
      Left            =   120
      Picture         =   "AproposDe.frx":38AE
      ScaleHeight     =   40
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   40
      TabIndex        =   0
      ToolTipText     =   "Liste des num�ros de t�l�phone"
      Top             =   1320
      Width           =   600
   End
   Begin VB.Label Label12 
      BackStyle       =   0  'Transparent
      Caption         =   "Administratif : 05.56.75.05.59"
      Height          =   255
      Left            =   840
      TabIndex        =   15
      Top             =   2400
      Width           =   3375
   End
   Begin VB.Label Label11 
      BackStyle       =   0  'Transparent
      Caption         =   "Maintenance : 05.56.75.15.45"
      Height          =   255
      Left            =   840
      TabIndex        =   14
      Top             =   2160
      Width           =   3375
   End
   Begin VB.Label Label10 
      BackStyle       =   0  'Transparent
      Caption         =   "Commercial : 05.56.75.12.28"
      Height          =   255
      Left            =   840
      TabIndex        =   13
      Top             =   1920
      Width           =   3135
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "Maintenance : 05.57.35.03.63"
      Height          =   255
      Left            =   840
      TabIndex        =   12
      Top             =   1560
      Width           =   3375
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Commercial : 05.56.75.56.65"
      Height          =   255
      Left            =   840
      TabIndex        =   11
      Top             =   1320
      Width           =   3135
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "maintenance@deal-informatique.com"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   840
      MouseIcon       =   "AproposDe.frx":3BB8
      MousePointer    =   99  'Custom
      TabIndex        =   10
      Top             =   3480
      Width           =   2775
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "commercial@deal-informatique.com"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   840
      MouseIcon       =   "AproposDe.frx":3D0A
      MousePointer    =   99  'Custom
      TabIndex        =   9
      Top             =   3120
      Width           =   2535
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "infos@deal-informatique.com"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   840
      MouseIcon       =   "AproposDe.frx":3E5C
      MousePointer    =   99  'Custom
      TabIndex        =   8
      Top             =   2760
      Width           =   2535
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "http://www.deal.fr"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   840
      MouseIcon       =   "AproposDe.frx":3FAE
      MousePointer    =   99  'Custom
      TabIndex        =   7
      Top             =   4080
      Width           =   2535
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Visitez notre site : "
      Height          =   255
      Left            =   840
      TabIndex        =   6
      Top             =   3840
      Width           =   1335
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Label2"
      Height          =   615
      Left            =   840
      TabIndex        =   5
      Top             =   600
      Width           =   3975
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "DEAL INFORMATIQUE"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   840
      TabIndex        =   4
      Top             =   120
      Width           =   3975
   End
End
Attribute VB_Name = "AproposDe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

Me.Label2.Caption = "3,Avenue de la Madeleine" & Chr(13) & _
                    "B.P. 100" & Chr(13) & _
                    "33173 GRADIGNAN CEDEX"


End Sub


Private Sub Label4_Click()

Call ShellExecute(0&, vbNullString, "mailto: " & Label4.Caption, vbNullString, vbNullString, vbNormalFocus)

End Sub

Private Sub Label6_Click()

Dim http As Long
    
http = ShellExecute(hwnd, "Open", Me.Label6.Caption, vbNullString, App.Path, 1)

End Sub

Private Sub Label7_Click()

Call ShellExecute(0&, vbNullString, "mailto: " & Label7.Caption, vbNullString, vbNullString, vbNormalFocus)

End Sub

Private Sub Label8_Click()

Call ShellExecute(0&, vbNullString, "mailto: " & Label8.Caption, vbNullString, vbNullString, vbNormalFocus)

End Sub
