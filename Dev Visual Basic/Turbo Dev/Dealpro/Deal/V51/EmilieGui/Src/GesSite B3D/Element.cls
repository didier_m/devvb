VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Element"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> D�finition d'un �l�ment dans la page
Public ElementType As String '-> Type de l'�l�ment
Public Fichier As String '-> Fichier associ� � cet �l�ment
Public Key As String '-> Cl� d'acc�s dans la collection
Public aFrm As frmPage '-> Page de visu d'un fichier HTML
Public PathToPublish As String '-> Indique le path de l'endroit o� sera publi� l'�l�ment
Public PathTempo As String '-> Indique le path du r�pertoire temporaire

