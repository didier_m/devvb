VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmBibli 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Archivage"
   ClientHeight    =   10125
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10050
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   675
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   670
   Begin TabDlg.SSTab SSTab1 
      Height          =   10095
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10005
      _ExtentX        =   17648
      _ExtentY        =   17806
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Commercial"
      TabPicture(0)   =   "frmBibli.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(1)=   "TreeView1"
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Recrutement"
      TabPicture(1)   =   "frmBibli.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "TreeView2"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Frame3"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      Begin VB.Frame Frame3 
         Caption         =   "Frame3"
         Height          =   9495
         Left            =   3720
         TabIndex        =   37
         Top             =   480
         Width           =   6135
         Begin VB.TextBox Text1 
            Height          =   5295
            Left            =   120
            MultiLine       =   -1  'True
            TabIndex        =   38
            Top             =   4080
            Width           =   5895
         End
         Begin VB.Label Label1 
            Caption         =   "Date et heure : "
            Height          =   255
            Index           =   18
            Left            =   120
            TabIndex        =   57
            Top             =   360
            Width           =   1575
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   25
            Left            =   1800
            TabIndex        =   56
            Top             =   360
            Width           =   4215
         End
         Begin VB.Label Label1 
            Caption         =   "Cursus / Formation : "
            Height          =   255
            Index           =   17
            Left            =   120
            TabIndex        =   55
            Top             =   3720
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "e-mail : "
            Height          =   255
            Index           =   16
            Left            =   120
            TabIndex        =   54
            Top             =   3240
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "T�l / Fax :"
            Height          =   255
            Index           =   15
            Left            =   120
            TabIndex        =   53
            Top             =   2880
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Cp / Ville :"
            Height          =   255
            Index           =   14
            Left            =   120
            TabIndex        =   52
            Top             =   2520
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Adresse"
            Height          =   255
            Index           =   13
            Left            =   120
            TabIndex        =   51
            Top             =   1440
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Pr�nom : "
            Height          =   255
            Index           =   12
            Left            =   120
            TabIndex        =   50
            Top             =   1080
            Width           =   1575
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   24
            Left            =   1800
            TabIndex        =   49
            Top             =   3240
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   23
            Left            =   3960
            TabIndex        =   48
            Top             =   2880
            Width           =   2055
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   22
            Left            =   1800
            TabIndex        =   47
            Top             =   2880
            Width           =   2055
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   21
            Left            =   3000
            TabIndex        =   46
            Top             =   2520
            Width           =   3015
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   20
            Left            =   1800
            TabIndex        =   45
            Top             =   2520
            Width           =   1095
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   19
            Left            =   1800
            TabIndex        =   44
            Top             =   2160
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   18
            Left            =   1800
            TabIndex        =   43
            Top             =   1800
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   17
            Left            =   1800
            TabIndex        =   42
            Top             =   1440
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   16
            Left            =   1800
            TabIndex        =   41
            Top             =   1080
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   15
            Left            =   1800
            TabIndex        =   40
            Top             =   720
            Width           =   4215
         End
         Begin VB.Label Label1 
            Caption         =   "Nom :"
            Height          =   255
            Index           =   6
            Left            =   120
            TabIndex        =   39
            Top             =   720
            Width           =   1575
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Propri�t�s : "
         Height          =   9495
         Left            =   -71280
         TabIndex        =   2
         Top             =   480
         Width           =   6135
         Begin VB.Frame Frame2 
            Enabled         =   0   'False
            Height          =   2295
            Left            =   1800
            TabIndex        =   3
            Top             =   6960
            Width           =   4215
            Begin VB.CheckBox Check1 
               Caption         =   "Un Contatct T�l�phonique"
               Height          =   255
               Index           =   5
               Left            =   120
               TabIndex        =   9
               Top             =   1920
               Width           =   2415
            End
            Begin VB.CheckBox Check1 
               Caption         =   "Une documentation"
               Height          =   255
               Index           =   4
               Left            =   120
               TabIndex        =   8
               Top             =   1560
               Width           =   2415
            End
            Begin VB.CheckBox Check1 
               Caption         =   "BACCHUS Vins && spiritueux"
               Height          =   255
               Index           =   3
               Left            =   120
               TabIndex        =   7
               Top             =   1200
               Width           =   2415
            End
            Begin VB.CheckBox Check1 
               Caption         =   "NATHALIE Agro-Agri"
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   6
               Top             =   840
               Width           =   2415
            End
            Begin VB.CheckBox Check1 
               Caption         =   "DEALPACK PGI Pme-Pmi"
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   5
               Top             =   480
               Width           =   2415
            End
            Begin VB.CheckBox Check1 
               Caption         =   "ERP Grands Comptes"
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   4
               Top             =   120
               Width           =   2415
            End
         End
         Begin MSComctlLib.ImageList ImageList1 
            Left            =   480
            Top             =   480
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   16
            ImageHeight     =   16
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   1
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmBibli.frx":0038
                  Key             =   "Contact"
               EndProperty
            EndProperty
         End
         Begin VB.Label Label1 
            Caption         =   "Date et heure : "
            Height          =   255
            Index           =   5
            Left            =   120
            TabIndex        =   35
            Top             =   360
            Width           =   1575
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   14
            Left            =   1800
            TabIndex        =   34
            Top             =   360
            Width           =   4215
         End
         Begin VB.Label Label1 
            Caption         =   "Mes disponibilit�s : "
            Height          =   255
            Index           =   11
            Left            =   120
            TabIndex        =   33
            Top             =   5880
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Observations : "
            Height          =   255
            Index           =   10
            Left            =   120
            TabIndex        =   32
            Top             =   4320
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "e-mail : "
            Height          =   255
            Index           =   9
            Left            =   120
            TabIndex        =   31
            Top             =   3960
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "T�l / Fax :"
            Height          =   255
            Index           =   8
            Left            =   120
            TabIndex        =   30
            Top             =   3600
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Cp / Ville :"
            Height          =   255
            Index           =   7
            Left            =   120
            TabIndex        =   29
            Top             =   3240
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Adresse"
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   28
            Top             =   2160
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Fonction : "
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   27
            Top             =   1800
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Interlocuteur :"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   26
            Top             =   1440
            Width           =   1575
         End
         Begin VB.Label Label1 
            Caption         =   "Activit� / A.P.E. :"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   25
            Top             =   1080
            Width           =   1575
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   975
            Index           =   13
            Left            =   1800
            TabIndex        =   24
            Top             =   5880
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   1455
            Index           =   12
            Left            =   1800
            TabIndex        =   23
            Top             =   4320
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   11
            Left            =   1800
            TabIndex        =   22
            Top             =   3960
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   10
            Left            =   3960
            TabIndex        =   21
            Top             =   3600
            Width           =   2055
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   9
            Left            =   1800
            TabIndex        =   20
            Top             =   3600
            Width           =   2055
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   8
            Left            =   3000
            TabIndex        =   19
            Top             =   3240
            Width           =   3015
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   7
            Left            =   1800
            TabIndex        =   18
            Top             =   3240
            Width           =   1095
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   6
            Left            =   1800
            TabIndex        =   17
            Top             =   2880
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   5
            Left            =   1800
            TabIndex        =   16
            Top             =   2520
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   4
            Left            =   1800
            TabIndex        =   15
            Top             =   2160
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   3
            Left            =   1800
            TabIndex        =   14
            Top             =   1800
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   2
            Left            =   1800
            TabIndex        =   13
            Top             =   1440
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   1
            Left            =   1800
            TabIndex        =   12
            Top             =   1080
            Width           =   4215
         End
         Begin VB.Label lblValue 
            BorderStyle     =   1  'Fixed Single
            Height          =   255
            Index           =   0
            Left            =   1800
            TabIndex        =   11
            Top             =   720
            Width           =   4215
         End
         Begin VB.Label Label1 
            Caption         =   "Raison sociale : "
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   10
            Top             =   720
            Width           =   1575
         End
      End
      Begin MSComctlLib.TreeView TreeView1 
         Height          =   9375
         Left            =   -74880
         TabIndex        =   1
         Top             =   550
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   16536
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin MSComctlLib.TreeView TreeView2 
         Height          =   9375
         Left            =   120
         TabIndex        =   36
         Top             =   550
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   16536
         _Version        =   393217
         LabelEdit       =   1
         Style           =   7
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
   End
End
Attribute VB_Name = "frmBibli"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Contacts As Collection
Private recrutments As Collection
Private NbContact As Integer
Private NbRecrut As Integer
Public Function Init() As Boolean

'---> Charger le fichier d'archivage

Dim Ligne As String
Dim hdlFile As Integer
Dim aContact As Contact
Dim DefLigne As String
'-> v�rifier que le fichier Archivage existe
If Dir$(aSite.ArchivageFile, vbNormal) = "" Then
    MsgBox "Impossible de trouver le fichier archive.", vbCritical + vbOKOnly, "Erreur"
    Unload Me
    Exit Function
End If

'-> Vider les treeviews
Me.TreeView1.Nodes.Clear
Me.TreeView2.Nodes.Clear

'-> Gestion des erreurs
On Error GoTo GestError

'-> Initialiser les 2 collections
Set Contacts = New Collection
Set Recrutement = New Collection

'-> Obtenir un handle de fichier
hdlFile = FreeFile
Open aSite.ArchivageFile For Input As #hdlFile

Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    If Trim(Ligne) = "" Then GoTo NextLigne
    '-> Analyse de la ligne
    If UCase$(Trim(Entry(1, Ligne, "~"))) = "CONTACT" Then '-> On traite un contact
        '-> Recup�rer le contenu de la ligne
        DefLigne = Entry(2, Ligne, "~")
        '-> Cr�er un nouvel objet contact
        Set aContact = New Contact
        '-> Init de ses propri�t�s
        aContact.RaisonSociale = Entry(1, DefLigne, "|")
        aContact.Activite = Entry(2, DefLigne, "|")
        aContact.Interlocuteur = Entry(3, DefLigne, "|")
        aContact.Fonction = Entry(4, DefLigne, "|")
        aContact.Adresse1 = Entry(5, DefLigne, "|")
        aContact.Adresse2 = Entry(6, DefLigne, "|")
        aContact.Adresse3 = Entry(7, DefLigne, "|")
        aContact.CodePostal = Entry(8, DefLigne, "|")
        aContact.Ville = Entry(9, DefLigne, "|")
        aContact.Tel = Entry(10, DefLigne, "|")
        aContact.Fax = Entry(11, DefLigne, "|")
        aContact.Mail = Entry(12, DefLigne, "|")
        aContact.Observations = Entry(13, DefLigne, "|")
        aContact.Disponibilit�s = Entry(20, DefLigne, "|")
        aContact.aDate = Entry(NumEntries(DefLigne, "|"), DefLigne, "|")
        If Trim(Entry(14, DefLigne, "|")) <> "" Then aContact.Erp = True
        If Trim(Entry(15, DefLigne, "|")) <> "" Then aContact.DealPack = True
        If Trim(Entry(16, DefLigne, "|")) <> "" Then aContact.Nathalie = True
        If Trim(Entry(17, DefLigne, "|")) <> "" Then aContact.Bacchus = True
        If Trim(Entry(18, DefLigne, "|")) <> "" Then aContact.Documentation = True
        If Trim(Entry(19, DefLigne, "|")) <> "" Then aContact.Contact = True
        '-> Ajouter dans la collection
        Contacts.Add aContact, "CT" & NbContact
        aContact.Key = "CT" & NbContact
        NbContact = NbContact + 1
        '-> Ajouter un nodes dans la collection
        Me.TreeView1.Nodes.Add , , aContact.Key, aContact.RaisonSociale, "Contact"
    Else
        '-> On traite une fiche de recrutement

    End If
NextLigne:
Loop 'Boucle d'analyse du fichier

'-> Fermer le fichier
Close #hdlFile

'-> Renvoyer une valeur de succes
Init = True

Exit Function

GestError:

    MsgBox "Erreur lors du chargement du fichier d'archivage", vbCritical + vbOKOnly, "Erreur fatale"
    Init = False
    



End Function

Private Sub Form_Load()

'---> Masquer ou non les onglets
Me.SSTab1.TabVisible(0) = aSite.IsContact
Me.SSTab1.TabVisible(1) = aSite.IsRecrutement


End Sub

Private Sub Form_Resize()

Dim Res As Long
Dim aRect As RECT

'-> Setting du tabstrip
Res = GetClientRect(Me.hwnd, aRect)
Me.SSTab1.Left = 0
Me.SSTab1.Top = 0
Me.SSTab1.Width = aRect.Right
Me.SSTab1.Height = aRect.Bottom

End Sub
Private Sub TreeView1_Click()

Dim aContact As Contact

'-> quitter si pas de contacts
If Me.TreeView1.Nodes.Count = 0 Then Exit Sub

'-> Pointer sur l'objet contact
Set aContact = Contacts(Me.TreeView1.SelectedItem.Key)

'-> Afficher les informations
Me.lblValue(14).Caption = aContact.aDate
Me.lblValue(0).Caption = aContact.RaisonSociale
Me.lblValue(1).Caption = aContact.Activite
Me.lblValue(2).Caption = aContact.Interlocuteur
Me.lblValue(3).Caption = aContact.Fonction
Me.lblValue(4).Caption = aContact.Adresse1
Me.lblValue(5).Caption = aContact.Adresse2
Me.lblValue(6).Caption = aContact.Adresse3
Me.lblValue(7).Caption = aContact.CodePostal
Me.lblValue(8).Caption = aContact.Ville
Me.lblValue(9).Caption = aContact.Tel
Me.lblValue(10).Caption = aContact.Fax
Me.lblValue(11).Caption = aContact.Mail
Me.lblValue(12).Caption = aContact.Observations
Me.lblValue(13).Caption = aContact.Disponibilit�s

If aContact.Erp Then
    Me.Check1(0).Value = 1
Else
    Me.Check1(0).Value = 0
End If

If aContact.DealPack Then
    Me.Check1(1).Value = 1
Else
    Me.Check1(1).Value = 0
End If
If aContact.Nathalie Then
    Me.Check1(2).Value = 1
Else
    Me.Check1(2).Value = 0
End If
If aContact.Bacchus Then
    Me.Check1(3).Value = 1
Else
    Me.Check1(3).Value = 0
End If
If aContact.Documentation Then
    Me.Check1(4).Value = 1
Else
    Me.Check1(4).Value = 0
End If
If aContact.Contact Then
    Me.Check1(5).Value = 1
Else
    Me.Check1(5).Value = 0
End If


End Sub
