VERSION 5.00
Begin VB.Form frmContact 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Fichier contact : "
   ClientHeight    =   9285
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6180
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9285
   ScaleWidth      =   6180
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      Height          =   2295
      Left            =   1800
      TabIndex        =   24
      Top             =   6840
      Width           =   4215
      Begin VB.CheckBox Check1 
         Caption         =   "Un Contact T�l�phonique"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   30
         Top             =   1920
         Width           =   2415
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Une documentation"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   29
         Top             =   1560
         Width           =   2415
      End
      Begin VB.CheckBox Check1 
         Caption         =   "BACCHUS Vins && spiritueux"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   28
         Top             =   1200
         Width           =   2415
      End
      Begin VB.CheckBox Check1 
         Caption         =   "NATHALIE Agro-Agri"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   27
         Top             =   840
         Width           =   2415
      End
      Begin VB.CheckBox Check1 
         Caption         =   "DEALPACK PGI Pme-Pmi"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   26
         Top             =   480
         Width           =   2415
      End
      Begin VB.CheckBox Check1 
         Caption         =   "ERP Grands Comptes"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   25
         Top             =   120
         Width           =   2415
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Date et heure : "
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   32
      Top             =   240
      Width           =   1575
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   14
      Left            =   1800
      TabIndex        =   31
      Top             =   240
      Width           =   4215
   End
   Begin VB.Label Label1 
      Caption         =   "Mes disponibilit�s : "
      Height          =   255
      Index           =   11
      Left            =   120
      TabIndex        =   23
      Top             =   5760
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Observations : "
      Height          =   255
      Index           =   10
      Left            =   120
      TabIndex        =   22
      Top             =   4200
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "e-mail : "
      Height          =   255
      Index           =   9
      Left            =   120
      TabIndex        =   21
      Top             =   3840
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "T�l / Fax :"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   20
      Top             =   3480
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Cp / Ville :"
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   19
      Top             =   3120
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Adresse"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   18
      Top             =   2040
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Fonction : "
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   17
      Top             =   1680
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Interlocuteur :"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   16
      Top             =   1320
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Activit� / A.P.E. :"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   15
      Top             =   960
      Width           =   1575
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   975
      Index           =   13
      Left            =   1800
      TabIndex        =   14
      Top             =   5760
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   1455
      Index           =   12
      Left            =   1800
      TabIndex        =   13
      Top             =   4200
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   11
      Left            =   1800
      TabIndex        =   12
      Top             =   3840
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   10
      Left            =   3960
      TabIndex        =   11
      Top             =   3480
      Width           =   2055
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   9
      Left            =   1800
      TabIndex        =   10
      Top             =   3480
      Width           =   2055
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   8
      Left            =   3000
      TabIndex        =   9
      Top             =   3120
      Width           =   3015
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   7
      Left            =   1800
      TabIndex        =   8
      Top             =   3120
      Width           =   1095
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   6
      Left            =   1800
      TabIndex        =   7
      Top             =   2760
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   5
      Left            =   1800
      TabIndex        =   6
      Top             =   2400
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   4
      Left            =   1800
      TabIndex        =   5
      Top             =   2040
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   3
      Left            =   1800
      TabIndex        =   4
      Top             =   1680
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   2
      Left            =   1800
      TabIndex        =   3
      Top             =   1320
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   1
      Left            =   1800
      TabIndex        =   2
      Top             =   960
      Width           =   4215
   End
   Begin VB.Label lblValue 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Index           =   0
      Left            =   1800
      TabIndex        =   1
      Top             =   600
      Width           =   4215
   End
   Begin VB.Label Label1 
      Caption         =   "Raison sociale : "
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   1575
   End
End
Attribute VB_Name = "frmContact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Fichier As String '-> nom du fichier contact
Public aContact As Contact '-> Pointeur vers un objet contact

Public Sub Init()

'---> Cette proc�dure ouvre le fichier ASCII sp�cifi� et remplis les zones

Dim hdlFile As Integer
Dim Ligne As String

'-> Ouvrir le fichier ASCII
hdlFile = FreeFile
Open Fichier For Input As #hdlFile
Do While Not EOF(hdlFile)
    Line Input #hdlFile, Ligne
    If Trim(Ligne) <> "" Then Exit Do
Loop

'-> Fermer le fichier ASCII
Close #hdlFile

'-> Afficher les information
For i = 0 To 12
    Me.lblValue(i).Caption = Entry(i + 1, Ligne, "|")
Next
Me.lblValue(13) = Entry(20, Ligne, "|")

For i = 14 To 19
    If UCase$(Trim(Entry(i, Ligne, "|"))) = "ON" Then Me.Check1(i - 14).Value = 1
Next

Me.lblValue(14) = Entry(NumEntries(Ligne, "|"), Ligne, "|")


End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set aContact.aFrm = Nothing
End Sub
