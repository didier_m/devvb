VERSION 5.00
Begin VB.Form frmPassWord 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Saisie du mot de passe"
   ClientHeight    =   1545
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4110
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1545
   ScaleWidth      =   4110
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Caption         =   "&Ok"
      Height          =   375
      Left            =   3000
      TabIndex        =   2
      Top             =   1080
      Width           =   975
   End
   Begin VB.TextBox Text1 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   840
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   480
      Width           =   3135
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   120
      Picture         =   "frmPassWord.frx":0000
      Top             =   240
      Width           =   480
   End
   Begin VB.Label Label1 
      Caption         =   "Veuillez saisir le mot de passe :"
      Height          =   255
      Left            =   840
      TabIndex        =   0
      Top             =   120
      Width           =   2535
   End
End
Attribute VB_Name = "frmPassWord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private IsPass As Boolean
Private Pwd As String
Private NbEssai As Integer
'-> Comme cette fen�tre est utilis�e � plusieurs endroits, elle indique
' si on est en cours de chargement de l'applicatif
Public IsLoadingApp As Boolean

Private Sub Command1_Click()

'-> Incr�menter le compteur d'essai
NbEssai = NbEssai + 1

'-> V�rifier le mot de passe
If Me.Text1.Text = Pwd Then
    IsPass = True
    If Not IsLoadingApp Then StrRetour = "OK"
    Unload Me
    Exit Sub
End If

'-> message d'erreur
MsgBox "Erreur dans le mot de passe. Essai : " & NbEssai & "/3", vbCritical + vbOKOnly, "Erreur"
Me.Text1.Text = ""
Me.Text1.SetFocus

'-> Quitter au bout du Troisi�me
If NbEssai = 3 Then
    If IsLoadingApp Then
        End
    Else
        Unload Me
    End If
End If

    
End Sub

Private Sub Form_Load()

Dim Res As Long
Dim lpBuffer As String

'-> V�rifier que l'on trouve le fichier de param�trage
If Dir$(App.Path & "\Datas\Param.ini", vbNormal) = "" Then
    MsgBox "Impossible de trouver le fichier de param�trage Param.ini", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> R�cup�rer le mot de passe
lpBuffer = Space$(200)
Res = GetPrivateProfileString("SETUP", "PWD", "", lpBuffer, Len(lpBuffer), App.Path & "\Datas\Param.ini")
If Res = 0 Then
    MsgBox "Erreur dans la r�cup�ration des param�trages  du setup", vbCritical + vbOKOnly, "Erreur fatale"
    End
End If

'-> Setting du mot de passe
Pwd = Trim(DeCrypt(Mid$(lpBuffer, 1, Res)))

'-> Nombre d'enregistrement
NbEssai = 0

End Sub


Private Sub Form_Unload(Cancel As Integer)

If Not IsPass Then End

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then Command1_Click

End Sub
