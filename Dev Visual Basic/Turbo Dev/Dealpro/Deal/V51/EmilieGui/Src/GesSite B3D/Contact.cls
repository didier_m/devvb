VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Contact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Définit un objet contact
Public Fichier As String
Public aFrm As frmContact
Public Key As String

'-> Zones spécifiques au contact DEAL
Public RaisonSociale As String
Public Activite As String
Public Interlocuteur As String
Public Fonction As String
Public Adresse1 As String
Public Adresse2 As String
Public Adresse3 As String
Public CodePostal As String
Public Ville As String
Public Tel As String
Public Fax As String
Public Mail As String
Public Observations As String
Public Disponibilités As String
Public aDate As String
Public Erp As Boolean
Public DealPack As Boolean
Public Nathalie As Boolean
Public Bacchus As Boolean
Public Documentation As Boolean
Public Contact As Boolean

