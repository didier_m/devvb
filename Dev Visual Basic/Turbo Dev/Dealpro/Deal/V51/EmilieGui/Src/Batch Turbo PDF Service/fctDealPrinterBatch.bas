Attribute VB_Name = "fctDealPrinterPath"
Option Explicit

'-> Entete [PARAM]
Public IsSyncho As Boolean 'Indique si on acc�s � la synchro
Public IsLog As Boolean 'Indique si on a acc�s � la gestion des log
Public FilenameStop As String 'Indique le nom du fichier Par d�faut pour arret du batch

'-> Synchronisation [SYNCHRO]
Public SynchroDirectory As String
Public SynchroFilename As String
Public SynchroDelai As Long

'-> Analyse des demandes d'impression [PRINT]
Public ScanDirectory As String
Public ScanExtension As String
Public ScanDelai As Long

'-> Pour gestion des log [LOG]
Public LogDirectory As String
Public LogFilename As String

'-> Pour gestion des paths
Public PathTurboFile As String
Public PathPdfFile As String

'-> Handle du fchier Log
Private hdlFile As Integer

Public Const Service_Name = "DealPrinterBatch"
Public Const INFINITE = -1&      '  Infinite timeout
Private Const WAIT_TIMEOUT = 258&

Public Type OSVERSIONINFO
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion(1 To 128) As Byte      '  Maintenance string for PSS usage
End Type

Public Const VER_PLATFORM_WIN32_NT = 2&

Private Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFO) As Long
Private Declare Function MessageBox Lib "user32" Alias "MessageBoxA" (ByVal hWnd As Long, ByVal lpText As String, ByVal lpCaption As String, ByVal wType As Long) As Long

Public hStopEvent As Long, hStartEvent As Long, hStopPendingEvent
Public IsNT As Boolean, IsNTService As Boolean
Public ServiceName() As Byte, ServiceNamePtr As Long



Sub SetInitialisation()

'---> Point d'entr�e du programme

Dim Temp
Dim IniFileName As String
Dim lpBuffer As String
Dim Res As Long

On Error Resume Next

'-> R�cup�rer le nom de la machine
lpBuffer = Space$(255)
Res = GetComputerName(lpBuffer, Len(lpBuffer))
lpBuffer = Entry(1, lpBuffer, Chr(0))

'-> Composer le nom du fichier Ini � analyser
IniFileName = LCase$(App.Path & "\dpb" & lpBuffer & ".ini")

'-> Tester si on trouve le fichier
If Dir$(IniFileName) = "" Then
    App.LogEvent "Impossible de trouver le fichier ini de param�trage : " & vbCrLf & IniFileName
    End
End If

'-> Tester si on trouve le programme d'impression
If Dir$(App.Path & "\TurboGraph.exe") = "" Then
    App.LogEvent "Impossible de trouver le moteur d'impression : " & App.Path & "\TurboGraph.exe"
    End
End If

'-> Positionnement des valeurs par d�faut
IsSyncho = False
IsLog = False
FilenameStop = "DealPrinterBatch.Stop"
SynchroDelai = 60000  '1 mn
ScanExtension = "*.dpb"
ScanDelai = 10000 '10 secondes
LogDirectory = App.Path
LogFilename = "DealPrinterBatch.Log"

'**********
'* Entete *
'**********

'-> Synchro
Temp = GetIniString("PARAM", "SYNCHRO", IniFileName, False)
If Temp = "1" Then IsSyncho = True

'-> Log
Temp = GetIniString("PARAM", "LOG", IniFileName, False)
If Temp = "1" Then IsLog = True

'-> FilenameStop
Temp = GetIniString("PARAM", "FILENAMESTOP", IniFileName, False)
If Temp <> "" And IsLegalName(Temp) Then FilenameStop = Temp

'-> Path des fichiers Turbo
Temp = GetIniString("PARAM", "PATHTURBOFILE", IniFileName, False)
If Trim(Temp) <> "" Then
    If (GetAttr(Temp) And vbDirectory) = vbDirectory Then PathTurboFile = Temp
End If 'Si on a sp�cifi� un path pour les spools Turbo

'-> Path des fichiers Pdf
Temp = GetIniString("PARAM", "PATHPDFFILE", IniFileName, False)
If Trim(Temp) <> "" Then
    If (GetAttr(Temp) And vbDirectory) = vbDirectory Then PathPdfFile = Temp
End If 'Si on a sp�cifi� un path pour les spools Turbo

'*******************
'* Synchronisation *
'*******************

'-> R�pertoire de synchronisation
Temp = GetIniString("SYNCHRO", "DIRECTORY", IniFileName, False)
If Trim(Temp) = "" Then
    IsSyncho = False
Else
    '-> V�rifier si on trouve le r�pertoire
    If (GetAttr(Temp) And vbDirectory) <> vbDirectory Then
        IsSyncho = False
    Else
        SynchroDirectory = Temp
        If Right$(SynchroDirectory, 1) <> "\" Then SynchroDirectory = SynchroDirectory & "\"
    End If
End If
        
'-> Fichier de synchronisation
Temp = GetIniString("SYNCHRO", "FILENAME", IniFileName, False)
If Trim(Temp) = "" Then
    IsSyncho = False
Else
    If Not IsLegalName(Temp) Then
        IsSyncho = False
    Else
        SynchroFilename = Temp
    End If
End If

'-> Delai de synchronisation
Temp = GetIniString("SYNCHRO", "DELAI", IniFileName, False)
If IsNumeric(Temp) Then SynchroDelai = CLng(Temp)


'********
'* Scan *
'********

'-> R�pertoire d'impression
Temp = GetIniString("SCAN", "DIRECTORY", IniFileName, False)
If Trim(Temp) = "" Then
    App.LogEvent "R�pertoire d'analyse des fichiers non renseign�"
    End
End If

'-> V�rifier si on trouve le r�pertoire
If (GetAttr(Temp) And vbDirectory) <> vbDirectory Then
    App.LogEvent "R�pertoire d'analyse des fichiers non valide : " & Chr(13) & Temp
    End
End If

'-> Affecter la variable
ScanDirectory = Temp
If Right$(ScanDirectory, 1) <> "\" Then ScanDirectory = ScanDirectory & "\"


'-> Extension
Temp = GetIniString("SCAN", "EXTENSION", IniFileName, False)
If Trim(Temp) <> "" Then ScanExtension = Trim(Temp)

'-> Delai analyse
Temp = GetIniString("SCAN", "DELAI", IniFileName, False)
If IsNumeric(Temp) Then ScanDelai = CInt(Temp)
        
'*******
'* Log *
'*******

'-> R�pertoire d'�criture des log
Temp = GetIniString("LOG", "DIRECTORY", IniFileName, False)
If Trim(Temp) = "" Then
    IsLog = False
Else
    '-> V�rifier si on trouve le r�pertoire
    If (GetAttr(Temp) And vbDirectory) <> vbDirectory Then
        IsLog = False
    Else
        LogDirectory = Temp
        If Right$(LogDirectory, 1) <> "\" Then LogDirectory = LogDirectory & "\"
    End If
End If
        
'-> Fichier des log
Temp = GetIniString("LOG", "FILENAME", IniFileName, False)
If Trim(Temp) <> "" Then
    If Not IsLegalName(Temp) Then
        IsLog = False
    Else
        LogFilename = Temp
    End If
End If

'-> Ouvrir un fichier LOG si necessaire
If IsLog Then
    '-> Ouverture
    hdlFile = FreeFile
    Open LogDirectory & LogFilename For Append As #hdlFile
    '-> Imprimer l'entete
    Print #hdlFile, "****** Debut de session du programme DealPrintBatch ******"
    Print #hdlFile, "Date : " & Now
    Print #hdlFile, "****** Etat des variables ****** "
    Print #hdlFile, "Synchronisation : " & CStr(IsSyncho)
    Print #hdlFile, "Gestion des log : " & CStr(IsLog)
    Print #hdlFile, "FileNameStop : " & FilenameStop
    Print #hdlFile, "PathTurboFile : " & PathTurboFile
    Print #hdlFile, "PathPdfFile : " & PathPdfFile
    Print #hdlFile, "Synchro Directory : " & SynchroDirectory
    Print #hdlFile, "Synchro FileName : " & SynchroFilename
    Print #hdlFile, "Synchro Delai : " & SynchroDelai
    Print #hdlFile, "Scan Directory : " & ScanDirectory
    Print #hdlFile, "Scan Extension : " & ScanExtension
    Print #hdlFile, "Scan Delai : " & ScanDelai
    Print #hdlFile, "Log Directory : " & LogDirectory
    Print #hdlFile, "Log Filename : " & LogFilename
    Print #hdlFile, "****** Traitements ******"
End If

''-> Charger la feuille de synchronisation
'If IsSyncho Then
'    '-> Cr�er le fichier de  synchronistation
'    SetIniString "DEAL", "TIME", SynchroDirectory & SynchroFilename, CStr(Now)
'    '-> Lancer la feuille de temporisation
'    Load frmSynchronisation
'    '->Poser le D�lai de Temporisation
'    frmSynchronisation.Timer1.Interval = SynchroDelai
'End If
'
''-> charger la feuille d'analyse
'Load frmPrint
'frmPrint.Timer1.Interval = ScanDelai

End Sub

Public Sub PrintLog(strToPrint As String)

'---> Mise � jour du fichier Log
If Not IsLog Then Exit Sub

Print #hdlFile, strToPrint

End Sub

Private Sub SetAnalyseToPrint()

'-> Variables locales
Dim TurboFile As String
Dim PdfFile As String
Dim KillFile As String
Dim PrinterName As String
Dim NbCopies As Integer
Dim FileName As String
Dim Temp As String
Dim LogLigne As String
Dim CommandLigne As String

On Error Resume Next

'-> V�rifier si on trouve un fichier de fin de travail
If Dir$(ScanDirectory & FilenameStop) <> "" Then
    '-> Butter le fichier de demande de fin
    Kill ScanDirectory & FilenameStop
    '-> Supprimer le fichier de synchronisation
    Kill SynchroDirectory & SynchroFilename
    '-> Insrire la fin du log
    Call PrintLog("****** Arret du Batch " & Now & " *****")
    End
End If

'-> Faire une analyse du r�pertoire
FileName = Dir$(ScanDirectory & ScanExtension)
Do While FileName <> ""

    '-> Lecture des informations
    TurboFile = GetIniString("BATCH", "TURBOFILE", ScanDirectory & FileName, False)
    '-> Get du printername
    PrinterName = GetIniString("BATCH", "PRINTERNAME", ScanDirectory & FileName, False)
    '-> Get du nom Pdf
    PdfFile = GetIniString("BATCH", "PDFFILE", ScanDirectory & FileName, False)
    '-> Get du kill
    KillFile = GetIniString("BATCH", "KILLFILE", ScanDirectory & FileName, False)
    '-> R�cup�rer le nombre de copies
    Temp = GetIniString("BATCH", "NBCOPIES", ScanDirectory & FileName, False)
        
    '-> Suppression du fichier
    Kill ScanDirectory & FileName
        
    '-> Raz de la avriable de Log
    Call PrintLog("---> Taitement du fichier : " & ScanDirectory & FileName)
    Call PrintLog("Date : " & Now)
    '-> Test sur le fichier Turbo
    Call PrintLog("Fichier Turbo : " & PathTurboFile & TurboFile)
    '-> Tester que l'on trouve le fichier turbo de r�f�rence
    If Dir$(PathTurboFile & TurboFile) = "" Then
        Call PrintLog("Impossible de trouver le fichier Turbo. Fin du traitement")
        GoTo NextFile
    Else
        Call PrintLog("Fichier trouv�")
    End If
    '-> Test du printername
    Call PrintLog("Printername : " & PrinterName)
    '-> Tester que cela soit un printervalide
    If Not GetPrinter(PrinterName) Then
        Call PrintLog("Imprimante non valide. Fin du traitement")
        GoTo NextFile
    Else
        Call PrintLog("Printer valide")
    End If
    '-> Tester si on est dans le cas d'une imprimante PDF
    If UCase$(Trim(PrinterName)) = "ACROBAT PDFWRITER" Or UCase$(Trim(PrinterName)) = "WIN2PDF" Then
        Call PrintLog("Impression PDF : Oui")
        '-> Tester si le nom est sp�cifi�
        If Trim(PdfFile) = "" Then
            PdfFile = PathPdfFile & TurboFile & ".pdf"
        Else
            PdfFile = PathPdfFile & PdfFile
        End If
        '-> Imprimer le Log
        Call PrintLog("PdfFile : " & PdfFile)
        '-> Faire un Tests si = 1
        If KillFile = "1" Then
            Call PrintLog("Suppression du fichier Turbo : Oui")
        Else
            KillFile = "0"
            Call PrintLog("Suppression du fichier Turbo : Non")
        End If
        '-> Cr�ation de la chaine de commande
        CommandLigne = UCase$(Trim(PrinterName)) & "~DIRECT~1|" & PathTurboFile & TurboFile & "*" & PdfFile & "*" & KillFile
    Else
        '-> Indiquer que l'on n'est pas sur une impression PDF
        Call PrintLog("Impression PDF : Non")
        If IsNumeric(Temp) Then
            NbCopies = CInt(Temp)
        Else
            NbCopies = 1
        End If
        '-> Cr�ation de la chaine de commande
        CommandLigne = PrinterName & "~DIRECT~" & NbCopies & "|" & PathTurboFile & TurboFile
    End If
    
    '-> Impression
    Call PrintLog("Ligne de commande Turbo : " & CommandLigne)
    '-> Run de l'impression
    Shell App.Path & "\TurboGraph.exe  " & CommandLigne, vbNormalFocus
    
NextFile:
    '-> Analyse de l'entr�e suivante
    FileName = Dir
Loop


End Sub

Private Function GetPrinter(strPrinter As String) As Boolean

Dim x As Printer

On Error GoTo GestError

'-> Essayer de pointer sur le printer
For Each x In Printers
    If UCase$(x.DeviceName) = UCase$(Trim(strPrinter)) Then
        GetPrinter = True
        Exit Function
    End If
Next

GestError:

End Function



Private Sub SetSynchronisation()

On Error Resume Next

'---> Mettre � jour le fichier de synchronisation
SetIniString "DEAL", "TIME", SynchroDirectory & SynchroFilename, CStr(Now)


End Sub

Private Sub Main()
    Dim hnd As Long
    Dim h(0 To 1) As Long
    ' Only one instance
    If App.PrevInstance Then Exit Sub
    ' Check OS type
    IsNT = CheckIsNT()
    ' Creating events
    hStopEvent = CreateEvent(0, 1, 0, vbNullString)
    hStopPendingEvent = CreateEvent(0, 1, 0, vbNullString)
    hStartEvent = CreateEvent(0, 1, 0, vbNullString)
    ServiceName = StrConv(Service_Name, vbFromUnicode)
    ServiceNamePtr = VarPtr(ServiceName(LBound(ServiceName)))
    If IsNT Then
        ' Trying to start service
        hnd = StartAsService
        h(0) = hnd
        h(1) = hStartEvent
        ' Waiting for one of two events: sucsessful service start (1) or
        ' terminaton of service thread (0)
        IsNTService = WaitForMultipleObjects(2&, h(0), 0&, INFINITE) = 1&
        If Not IsNTService Then
            CloseHandle hnd
            'MsgBox "This program must be started as service."
            MessageBox 0&, "Ce programme dpoit �tre lanc� comme un servcie.", App.Title, vbInformation Or vbOKOnly Or vbMsgBoxSetForeground
        End If
    Else
        MessageBox 0&, "Ce programme est destin� aux OS Windows NT/2000/XP.", App.Title, vbInformation Or vbOKOnly Or vbMsgBoxSetForeground
    End If
    
    If IsNTService Then
        '-> Lancer le processus d'initialisation
        Call SetInitialisation
        ' ******************
        ' Here you may initialize and start service's objects
        ' These objects must be event-driven and must return control
        ' immediately after starting.
        ' ******************
        SetServiceState SERVICE_RUNNING
        App.LogEvent "DealPrinterBatch service -> OK"
        Do
            ' ******************
            ' It is main service loop. Here you may place statements
            ' which perform useful functionality of this service.
            Sleep ScanDelai
            '-> Lancer la mise � jour du fichier de synchronisation
            Call SetSynchronisation
            DoEvents
            Call SetAnalyseToPrint
            DoEvents
            ' ******************
            ' Loop repeats every second. You may change this interval.
        Loop While WaitForSingleObject(hStopPendingEvent, 1000&) = WAIT_TIMEOUT
        ' ******************
        ' Here you may stop and destroy service's objects
        ' ******************
        SetServiceState SERVICE_STOPPED
        App.LogEvent "Arret du service DealPrinterBacth"
        SetEvent hStopEvent
        ' Waiting for service thread termination
        WaitForSingleObject hnd, INFINITE
        CloseHandle hnd
    End If
    CloseHandle hStopEvent
    CloseHandle hStartEvent
    CloseHandle hStopPendingEvent
End Sub

' CheckIsNT() returns True, if the program runs
' under Windows NT or Windows 2000, and False
' otherwise.
Public Function CheckIsNT() As Boolean
    Dim OSVer As OSVERSIONINFO
    OSVer.dwOSVersionInfoSize = LenB(OSVer)
    GetVersionEx OSVer
    CheckIsNT = OSVer.dwPlatformId = VER_PLATFORM_WIN32_NT
End Function




