VERSION 5.00
Begin VB.Form frmNetSend 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   3450
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8880
   Icon            =   "frmNetSend.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3450
   ScaleWidth      =   8880
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox Check1 
      Caption         =   "Check1"
      Height          =   255
      Left            =   1560
      TabIndex        =   9
      Top             =   3000
      Width           =   4455
   End
   Begin VB.CommandButton Command3 
      Height          =   855
      Left            =   7920
      Picture         =   "frmNetSend.frx":08CA
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   240
      Width           =   855
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   375
      Left            =   7560
      TabIndex        =   7
      Top             =   3000
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   375
      Left            =   6240
      TabIndex        =   6
      Top             =   3000
      Width           =   1215
   End
   Begin VB.TextBox strMessage 
      Height          =   1695
      Left            =   1560
      MultiLine       =   -1  'True
      TabIndex        =   5
      Top             =   1200
      Width           =   7215
   End
   Begin VB.TextBox strObjet 
      Height          =   285
      Left            =   1560
      TabIndex        =   3
      Top             =   720
      Width           =   6255
   End
   Begin VB.TextBox strURL 
      Height          =   285
      Left            =   1560
      TabIndex        =   1
      Top             =   240
      Width           =   6255
   End
   Begin VB.Image Image1 
      Height          =   720
      Left            =   120
      Picture         =   "frmNetSend.frx":0BD4
      Top             =   1680
      Width           =   720
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   1200
      Width           =   1335
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1335
   End
End
Attribute VB_Name = "frmNetSend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public aFichier As String
Public SpoolName As String
Public PageToPrint As String

Private Sub Command1_Click()

'---> Cette proc�dure envoie un e-mail via MAPI

Dim aLb As Libelle

'-> Pointer sur la classe libell�
Set aLb = Libelles("MESSAGE")

'-> Il faut dans u permier temps v�rifier si l'URL est saisi
If Trim(Me.strURL) = "" Then
    MsgBox aLb.GetCaption(8), vbCritical + vbOKOnly, aLb.GetToolTip(8)
    Me.strURL.SetFocus
    Exit Sub
End If

'-> Mettre � jour la variable net param
NetParam = Me.strURL.Text & Chr(0) & Me.strObjet.Text & Chr(0) & Me.strMessage.Text

'-> Cryptage
If Me.Check1.Value = 1 Then
    IsCryptedFile = True
Else
    IsCryptedFile = False
End If

Unload Me

End Sub

Private Sub Command2_Click()
    Unload Me
End Sub

Private Sub Command3_Click()

On Error GoTo GestError

'---> Initialiser la session MAPI
Call InitMAPISession

'-> Afficher le carnet d'adresse
MDIMain.MAPIMessages1.Compose
MDIMain.MAPIMessages1.Show False

'-> Afficher le destinataire si saisi
If MDIMain.MAPIMessages1.RecipAddress <> "" Then Me.strURL = MDIMain.MAPIMessages1.RecipAddress

GestError:



End Sub

Private Sub Form_Load()

Dim aLb As Libelle

'-> Chargement des libelles
Set aLb = Libelles("FRMNETSEND")

Me.Caption = aLb.GetCaption(1)
Me.Label1.Caption = aLb.GetCaption(2)
Me.Label2.Caption = aLb.GetCaption(4)
Me.Label3.Caption = aLb.GetCaption(3)
Me.Command3.ToolTipText = aLb.GetCaption(5)
Me.Check1.Caption = aLb.GetCaption(6)

'-> Chargement des libelles des boutons
Set aLb = Libelles("BOUTONS")
Me.Command1.Caption = aLb.GetCaption(3)
Me.Command2.Caption = aLb.GetCaption(2)


End Sub
