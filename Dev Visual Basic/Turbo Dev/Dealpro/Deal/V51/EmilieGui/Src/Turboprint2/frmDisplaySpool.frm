VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDisplaySpool 
   BackColor       =   &H00808080&
   Caption         =   "Form1"
   ClientHeight    =   7590
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9060
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   506
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   604
   Begin VB.PictureBox picAngle 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   4680
      ScaleHeight     =   25
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   25
      TabIndex        =   7
      Top             =   1440
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.PictureBox Page 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      DrawWidth       =   6
      ForeColor       =   &H80000008&
      Height          =   1695
      Left            =   4200
      ScaleHeight     =   113
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   238
      TabIndex        =   6
      Top             =   3240
      Visible         =   0   'False
      Width           =   3570
      Begin VB.Label lblAccesDet 
         BackColor       =   &H00C0C0FF&
         BackStyle       =   0  'Transparent
         Height          =   375
         Index           =   0
         Left            =   120
         MouseIcon       =   "frmDisplaySpool.frx":0000
         MousePointer    =   99  'Custom
         TabIndex        =   8
         Top             =   240
         Visible         =   0   'False
         Width           =   1575
      End
   End
   Begin VB.VScrollBar VScroll1 
      Height          =   975
      Left            =   0
      TabIndex        =   5
      Top             =   720
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.HScrollBar HScroll1 
      Height          =   255
      Left            =   3600
      TabIndex        =   4
      Top             =   5520
      Visible         =   0   'False
      Width           =   2655
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   5520
      Top             =   2400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":0CCA
            Key             =   "Warning"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   2415
      Left            =   240
      TabIndex        =   3
      Top             =   1080
      Visible         =   0   'False
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   4260
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList2"
      Appearance      =   1
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9060
      _ExtentX        =   15981
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   17
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SELECTION"
            ImageKey        =   "SELECTION"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FIRST"
            ImageKey        =   "FIRST"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PREV"
            ImageKey        =   "PREV"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "GOTO"
            ImageKey        =   "GOTO"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "NEXT"
            ImageKey        =   "NEXT"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "LAST"
            ImageKey        =   "LAST"
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PRINT"
            ImageKey        =   "PRINT"
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   3
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Object.Visible         =   0   'False
                  Key             =   "PRINTFILE"
                  Text            =   "Imprimer le fichier"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PRINTSPOOL"
                  Text            =   "Imprimer le spool"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PRINTPAGE"
                  Text            =   "Imprimer la page"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "MESSAGERIE"
            ImageKey        =   "IMG_MAIL"
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EXPORTHTML"
            ImageKey        =   "MAIL"
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EXCEL"
            Object.ToolTipText     =   "Exporter vers Excel"
            ImageIndex      =   15
         EndProperty
         BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "HELP"
            ImageKey        =   "HELP"
         EndProperty
         BeginProperty Button16 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button17 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SAVEASSPOOL"
            ImageIndex      =   16
         EndProperty
      EndProperty
      Begin VB.PictureBox Picture1 
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         Height          =   600
         Left            =   7920
         Picture         =   "frmDisplaySpool.frx":0E24
         ScaleHeight     =   40
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   50
         TabIndex        =   1
         Top             =   0
         Width           =   750
         Begin VB.Label lblCountPage 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   0
            TabIndex        =   2
            Top             =   180
            Width           =   735
         End
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   8280
      Top             =   1560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   16
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":144E
            Key             =   "SELECTION"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":2128
            Key             =   "FIRST"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":2442
            Key             =   "PREV"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":311C
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":4426
            Key             =   "NEXT"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":5100
            Key             =   "LAST"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":541A
            Key             =   "PRINT"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":5CF4
            Key             =   "HELP"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":600E
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":6460
            Key             =   "MAIL"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":6D3A
            Key             =   "Warning"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":6E94
            Key             =   "MESSAGERIE"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":71AE
            Key             =   "IMG_MAIL"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":74C8
            Key             =   "GOTO"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":77E2
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDisplaySpool.frx":7AFC
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmDisplaySpool"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-> Indique le Spool associ�
Public aSpool As Spool

'-> Pour positionnement de la page
Public LargeurX As Long
Public HauteurY As Long
Public MgX As Long
Public MgY As Long
Private Sub Form_Load()


Dim aLb As Libelle
Dim i As Integer

On Error Resume Next

'-> Pointer sur la classe Libelle
Set aLb = Libelles("FRMVISUSPOOL")

'-> ToolTip des boutons
Me.Toolbar1.Buttons("SELECTION").ToolTipText = aLb.GetCaption(1)
Me.Toolbar1.Buttons("FIRST").ToolTipText = aLb.GetCaption(2)
Me.Toolbar1.Buttons("PREV").ToolTipText = aLb.GetCaption(3)
Me.Toolbar1.Buttons("GOTO").ToolTipText = aLb.GetCaption(4)
Me.Toolbar1.Buttons("NEXT").ToolTipText = aLb.GetCaption(5)
Me.Toolbar1.Buttons("LAST").ToolTipText = aLb.GetCaption(6)
Me.Toolbar1.Buttons("PRINT").ToolTipText = aLb.GetCaption(7)
Me.Toolbar1.Buttons("EXPORTHTML").ToolTipText = aLb.GetCaption(9)
Me.Toolbar1.Buttons("MESSAGERIE").ToolTipText = aLb.GetCaption(8)
Me.Toolbar1.Buttons("HELP").ToolTipText = aLb.GetCaption(10)

'-> Titre de la fen�tre
If Fichiers(aSpool.FileName).NbSpool <> "1" Then
    Me.Caption = aLb.GetCaption(11) & aSpool.SpoolText & "/" & Fichiers(aSpool.FileName).NbSpool & aLb.GetToolTip(11) & aSpool.FileName
Else
    Me.Caption = aLb.GetCaption(11) & aSpool.SpoolText & aLb.GetToolTip(11) & aSpool.FileName
End If

'-> Afficher les boutons des sous menus
Me.Toolbar1.Buttons("PRINT").ButtonMenus("PRINTFILE").Text = aLb.GetCaption(12)
Me.Toolbar1.Buttons("PRINT").ButtonMenus("PRINTSPOOL").Text = aLb.GetCaption(13)
Me.Toolbar1.Buttons("PRINT").ButtonMenus("PRINTPAGE").Text = aLb.GetCaption(14)

'-> Bloquer le bouton de la page de s�lection s'il n'y en a pas
If Not aSpool.IsSelectionPage Then Me.Toolbar1.Buttons("SELECTION").Enabled = False

'-> Masquer le bouton d'export vers Excel
Me.Toolbar1.Buttons("EXCEL").Visible = ExcelOk

End Sub

Public Sub Form_Resize()

Dim aRect As RECT
Dim aRect2 As RECT
Dim Res As Long

On Error Resume Next

'-> Recup�rer la taille de la zone client
Res = GetClientRect(Me.hwnd, aRect)

'-> Initialiser les valeurs des barres de d�filement
Me.HScroll1.Min = MgX
Me.HScroll1.Max = -(MgX * 2 + LargeurX - aRect.Right)
Me.VScroll1.Min = MgY + Me.Toolbar1.Height
Me.VScroll1.Max = -(MgY * 2 + HauteurY - aRect.Bottom)

'-> Cas du redim en cas d'erreur
Me.TreeView1.Left = 0
Me.TreeView1.Top = 0
Me.TreeView1.Width = aRect.Right
Me.TreeView1.Height = aRect.Bottom
'-> Se tirer si le tree est visible
If Me.TreeView1.Visible Then Exit Sub

'-> Tester si on doit faire apparaitre les barres de d�filement
If aRect.Right < MgX * 2 + LargeurX Then
    '-> Positionner le ScrollBar
    Me.HScroll1.Left = 0
    Me.HScroll1.Top = aRect.Bottom - Me.HScroll1.Height
    Me.HScroll1.Width = aRect.Right
    Me.HScroll1.Visible = True
    Me.HScroll1.ZOrder
Else
    Me.HScroll1.Visible = False
End If

If aRect.Bottom < MgY * 2 + HauteurY Then
    '-> Positionner le ScrollBar
    Me.VScroll1.Left = aRect.Right - Me.VScroll1.Width
    Me.VScroll1.Top = Me.Toolbar1.Height
    Me.VScroll1.Height = aRect.Bottom - Me.VScroll1.Top
    Me.VScroll1.Visible = True
    Me.VScroll1.ZOrder
Else
    Me.VScroll1.Visible = False
End If

'-> Modifier la taille des Scrolls si les 2 sont pr�sents
If Me.HScroll1.Visible And Me.VScroll1.Visible Then
    Me.HScroll1.Width = Me.HScroll1.Width - Me.VScroll1.Width
    Me.VScroll1.Height = Me.VScroll1.Height - Me.HScroll1.Height
    Me.picAngle.Width = Me.VScroll1.Width
    Me.picAngle.Height = Me.HScroll1.Height
    Me.picAngle.Top = Me.VScroll1.Top + Me.VScroll1.Height
    Me.picAngle.Left = Me.HScroll1.Left + Me.HScroll1.Width
    Me.picAngle.Visible = True
Else
    Me.picAngle.Visible = False
End If

'-> Mettre la barre d'outils au premier plan
Me.Toolbar1.ZOrder

End Sub

Private Sub Form_Unload(Cancel As Integer)

Dim i As Integer

'-> Supprimer l'affectation de la feuille dans le spool
Set aSpool.frmDisplay = Nothing

If aSpool.NbError = 0 Then
    '-> Suppirmer l'icone de s�lection
    For i = 1 To aSpool.NbPage
        '-> Test attention aux pages avec des erreurs rechercher la mise � jour des icnoes
        MDIMain.TreeNaviga.Nodes(UCase(Trim(aSpool.FileName)) & "�" & UCase$(Trim(aSpool.Key)) & "�PAGE|" & i).Image = "Page"
    Next
End If

aSpool.CurrentPage = 0

End Sub

Public Sub DisplayCurrentPage()

'---> Cette proc�dure mat � jour l'affichage

Me.lblCountPage = aSpool.CurrentPage & "/" & aSpool.NbPage

End Sub

Private Sub HScroll1_Change()
    Me.Page.Left = Me.HScroll1.Value
End Sub

Private Sub DisplayDetail(Param As String)

'---> Cette proc�dure affiche un d�tail sp�cifi�

Dim i As Long, j As Long
Dim aLb As Libelle
Dim Condition As String
Dim AccesKey As String
Dim ToCompare As String
Dim Ligne As String
Dim ToAdd As Boolean
Dim LenAcces As Integer
Dim X As ListItem

On Error GoTo GestError

'-> Pointer sur la classe libell� affect�e
Set aLb = Libelles("ACCESDET")

'-> Ne rien faire si pas d'acc�s au d�tail
If Me.aSpool.NbLigDetail = 0 Then
    MsgBox aLb.GetCaption(1), vbExclamation + vbOKOnly, aLb.GetToolTip(1)
    Exit Sub
End If

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11
MDIMain.StatusBar1.Panels(1).Text = aLb.GetCaption(2)

'-> Eclater le param�trage
AccesKey = Entry(1, Param, Chr(0))
Condition = Entry(2, Param, Chr(0))

'-> Quitter si pas de cl� d'acc�s
If Trim(AccesKey) = "" Then GoTo GestError

'-> Charger la feuille d'acc�s au d�tail
Load frmDetail

'-> Cr�ation du browse
For i = 2 To NumEntries(aSpool.TitreAccesDetail, "|")
    '-> Cr�ation des colonnes
    frmDetail.ListView1.ColumnHeaders.Add , , Entry(i, aSpool.TitreAccesDetail, "|")
Next

'-> Analyse de toutes les lignes de l'acc�s au d�tail
For i = 1 To Me.aSpool.NbLigDetail
    ToAdd = False
    '-> R�cup�rer la ligne en cours
    Ligne = Me.aSpool.GetLigDetail(i)
    '-> R�cup�rer la cl� d'acc�s au d�tail
    ToCompare = Entry(1, Ligne, "|")
    '-> Pas de test sur cl� de comparaison � blanc
    If Trim(ToCompare) = "" Then GoTo NextLig
    '-> Selon la condition � appliquer
    Select Case Condition
        Case 1 'Egal
            '-> Tester en �galit�
            If UCase$(Trim(ToCompare)) = UCase$(Trim(AccesKey)) Then ToAdd = True
        Case 2 'Inf�rieur
            '-> Tester que l'op�rateur soit num�ric
            If IsNumeric(ToCompare) Then
                If CDbl(ToCompare) < CDbl(AccesKey) Then ToAdd = True
            End If 'Si crtit�re de comparaison num
        Case 3 'Inf�rieur ou �gal
            '-> Tester que l'op�rateur soit num�ric
            If IsNumeric(ToCompare) Then
                If CDbl(ToCompare) <= CDbl(AccesKey) Then ToAdd = True
            End If 'Si crtit�re de comparaison num
        Case 4 'Supr�rieur
            '-> Tester que l'op�rateur soit num�ric
            If IsNumeric(ToCompare) Then
                If CDbl(ToCompare) > CDbl(AccesKey) Then ToAdd = True
            End If 'Si crtit�re de comparaison num
        Case 5 'Supr�rieur ou �gal
            '-> Tester que l'op�rateur soit num�ric
            If IsNumeric(ToCompare) Then
                If CDbl(ToCompare) >= CDbl(AccesKey) Then ToAdd = True
            End If 'Si crtit�re de comparaison num
        Case 6 'Commence par
            '-> Get de la longueur de la chaine de comparaison
            LenAcces = Len(AccesKey)
            '-> Comparer
            If UCase$(Mid$(Trim(ToCompare), 1, LenAcces)) = UCase$(Trim(AccesKey)) Then ToAdd = True
        Case 7 'Contient
            '-> Tester la pr�sence
            If InStr(1, UCase$(Trim(ToCompare)), UCase$(Trim(AccesKey))) <> 0 Then ToAdd = True
    End Select
    
    '-> Si on doit cr�er ou non
    If Not ToAdd Then GoTo NextLig
    
    '-> Cr�er un n ouvel Item
    Set X = frmDetail.ListView1.ListItems.Add
    '-> Charger la ligne dans le browse
    For j = 2 To NumEntries(aSpool.TitreAccesDetail, "|")
        If j = 2 Then
            X.Text = Entry(j, Ligne, "|")
        Else
            X.SubItems(j - 2) = Entry(j, Ligne, "|")
        End If
    Next
NextLig:
Next

'-> Formatter le listview
FormatListView frmDetail.ListView1

'-> D�bloquer
Screen.MousePointer = 0
MDIMain.StatusBar1.Panels(1).Text = ""

'-> Afficher la feuille
frmDetail.Show vbModal

GestError:
    '-> D�bloquer l'�ran
    Me.Enabled = True
    Screen.MousePointer = 0
    MDIMain.StatusBar1.Panels(1).Text = ""

End Sub

Private Sub lblAccesDet_Click(Index As Integer)

DisplayDetail lblAccesDet(Index).Tag
End Sub

Private Sub picAngle_Resize()

'---> Dessin des lignes de la 3D
Me.picAngle.Line (0, 0)-(Me.picAngle.Width - 1, 0), RGB(255, 255, 255)
Me.picAngle.Line (0, 0)-(0, Me.picAngle.Height - 1), RGB(255, 255, 255)

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim Rep As VbMsgBoxResult
Dim aLb As Libelle
Dim MinPage As Integer
Dim DeviceName As String
Dim ChoixPage As String
Dim PageMin As Integer
Dim PageMax As Integer
Dim NbCopies As Integer
Dim FileToPrint As String

On Error Resume Next

'-> Setting del page mini d'impression
If aSpool.IsSelectionPage Then
    MinPage = 0
Else
    MinPage = 1
End If

Select Case Button.Key

    Case "SELECTION"
        '-> Afficher la page de s�lection de cet utilisateur
        PrintPageSpool aSpool, 0
        
        '-> Afficher la page
        aSpool.DisplayInterfaceByPage 0
        
    Case "FIRST"
        '-> on gere le ca ou il y a plusieurs spool sur le fichier
        If Fichiers(UCase(aSpool.FileName)).NbSpool = 1 Then
            '-> Afficher la premi�re page si elle n'est pas en cours
            If aSpool.CurrentPage <> MinPage Then
                '-> Imprimer la page
                PrintPageSpool aSpool, MinPage
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage MinPage
            End If
        Else
            Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.FileName) & "�SPOOL|1" & "�PAGE|1")
            MDIMain.TreeNaviga_DblClick
        End If
    
    Case "PREV"
        '-> on gere le ca ou il y a plusieurs spool sur le fichier
        If Fichiers(UCase(aSpool.FileName)).NbSpool = 1 Then
            '-> Afficher la page pr�c�dente si on n'est pas sur la page 1
            If aSpool.CurrentPage <> MinPage Then
                '-> Imprimer la page
                PrintPageSpool aSpool, aSpool.CurrentPage - 1
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.CurrentPage
            End If
        Else
            '-> si on est sur la premiere page essayer d'aller sur la derniere page du spool precedent
            If aSpool.CurrentPage > 1 Then
                '-> Imprimer la page
                PrintPageSpool aSpool, aSpool.CurrentPage - 1
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.CurrentPage
            Else
                If Fichiers(UCase(aSpool.FileName)).Spools(aSpool.Key).Num_Spool > 1 Then
                    Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.FileName) & "�SPOOL|" & (Fichiers(UCase(aSpool.FileName)).Spools(aSpool.Key).Num_Spool - 1) & "�PAGE|" & Fichiers(UCase(aSpool.FileName)).Spools((Fichiers(UCase(aSpool.FileName)).Spools(aSpool.Key).Num_Spool - 1)).NbPage)
                    MDIMain.TreeNaviga_DblClick
                End If
            End If
        End If
    
    Case "PRINT"
        '-> Vider la variable de retour
        strRetour = ""
        
        '-> Setting des param�trages sur le fichier et le spool en cours
        frmPrint.FichierName = aSpool.FileName
        frmPrint.SpoolKey = aSpool.Key
        
        '-> afficher ou pas la zone d'impression de la globalit� des spools
        If Fichiers(UCase$(aSpool.FileName)).Spools.Count = 1 Then
            frmPrint.Option4.Visible = False
        End If
        
        '-> Afficher le choix de l'imprimante
        frmPrint.Show vbModal
    
        '-> Quitter si annuler
        If strRetour = "" Then Exit Sub
        
        '-> Traiter les choix d'impression
        DeviceName = Entry(1, strRetour, "|")
        ChoixPage = Entry(2, strRetour, "|")
        NbCopies = Entry(3, strRetour, "|")
        RectoVerso = Entry(4, strRetour, "|")
        '-> Analyse du choix d'impression
        Select Case Entry(1, ChoixPage, "�")
            Case 1 '-> Fichier en entier
                '-> Construire le nom du fichier
                'FileToPrint = CreateDetail(Me.aSpool, -1)
                FileToPrint = aSpool.FileName
                
            Case 2 '-> Page mini � page maxi
                '-> R�cup�rer les pages � imprimer
                PageMin = CInt(Entry(2, ChoixPage, "�"))
                PageMax = CInt(Entry(3, ChoixPage, "�"))
                
                '-> V�rifier la page min
                If PageMin < 1 Then
                    '-> V�rifier si le fichier poss�de une page de s�lection
                    If aSpool.IsSelectionPage Then
                        PageMin = 0
                    Else
                        PageMin = 1
                    End If
                End If
                
                '-> V�rifier la page maxi
                If PageMax > aSpool.NbPage Then PageMax = aSpool.NbPage
                    
                '-> V�rifier que la page mini est < � la page maxi
                If PageMin > PageMax Then PageMin = PageMax
                    
                '-> Construire le nom du fichier
                FileToPrint = CreateDetail(Me.aSpool, -2, PageMin, PageMax)
                            
            Case 3  '-> Page en cours
                '-> Construire le nom du fichier
                FileToPrint = CreateDetail(Me.aSpool, aSpool.CurrentPage)
            Case 4 '-> Spool en cours
                FileToPrint = CreateDetail(Me.aSpool, -1)
        End Select
    
        'Debug.Print DeviceName & "~DIRECT~" & NbCopies & "|" & FileToPrint
    
        '-> Tester le cas de l'impression PDF
        
        ' *** PIERROT : tjrs pour gerer le soft WIN2PDF
        If UCase$(Trim(DeviceName)) = "WIN2PDF" Then
            '-> Lancer l'�dition
            Shell App.Path & "\TurboGraph.exe " & "WIN2PDF" & "~DIRECT~1|" & FileToPrint & "**0", vbNormalFocus
            Exit Sub
        End If
        
        If UCase$(Trim(DeviceName)) = "ACROBAT PDFWRITER" Then
            '-> Lancer l'�dition
            Shell App.Path & "\TurboGraph.exe " & "ACROBAT PDFWRITER" & "~DIRECT~1|" & FileToPrint & "**0", vbNormalFocus
        ElseIf UCase$(Trim(DeviceName)) = "ACROBAT DISTILLER" Then
            Shell App.Path & "\TurboGraph.exe " & "ACROBAT DISTILLER" & "~DIRECT~1|" & FileToPrint & "**0" & "|" & RectoVerso, vbNormalFocus
        Else
            '-> Lancer l'impression
            Shell App.Path & "\TurboGraph.exe " & DeviceName & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso, vbNormalFocus
        End If
    
    Case "GOTO"
        '-> Afficher la fen�tre de recherche des pages
        strRetour = ""
        If aSpool.IsSelectionPage Then
            frmSearchPage.NbMin = 0
        Else
            frmSearchPage.NbMin = 1
        End If
        frmSearchPage.NbMax = aSpool.NbPage
        frmSearchPage.Show vbModal
        If strRetour <> "" Then
            '-> Imprimer la page
            PrintPageSpool aSpool, CLng(strRetour)
            '-> Afficher la page
            aSpool.DisplayInterfaceByPage CLng(strRetour)
        End If
            
    Case "NEXT"
        '-> on gere le ca ou il y a plusieurs spool sur le fichier
        If Fichiers(UCase(aSpool.FileName)).NbSpool = 1 Then
            '-> Afficher la page suivante si on n'est pas sur la derniere page
            If aSpool.CurrentPage <> aSpool.NbPage Then
                '-> Imprimer la page
                PrintPageSpool aSpool, aSpool.CurrentPage + 1
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.CurrentPage
            End If
        Else
            '-> si on est sur la derniere page essayer d'aller sur la premiere page du spool suivant
            If aSpool.CurrentPage < aSpool.NbPage Then
                '-> Imprimer la page
                PrintPageSpool aSpool, aSpool.CurrentPage + 1
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.CurrentPage
            Else
                If Fichiers(UCase(aSpool.FileName)).Spools(aSpool.Key).Num_Spool < Fichiers(UCase(aSpool.FileName)).Spools(aSpool.Key).NbSpool Then
                    Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.FileName) & "�SPOOL|" & (Fichiers(UCase(aSpool.FileName)).Spools(aSpool.Key).Num_Spool + 1) & "�PAGE|1")
                    MDIMain.TreeNaviga_DblClick
                End If
            End If
        End If
    
    Case "LAST"
        '-> on gere le ca ou il y a plusieurs spool sur le fichier
        If Fichiers(aSpool.FileName).NbSpool = 1 Then
            '-> Afficher la page si ce n'est pas la derni�re
            If aSpool.CurrentPage <> aSpool.NbPage Then
                '-> Imprimer la page
                PrintPageSpool aSpool, aSpool.NbPage
                '-> Afficher la page
                aSpool.DisplayInterfaceByPage aSpool.NbPage
            End If
      Else
            Set MDIMain.TreeNaviga.SelectedItem = MDIMain.TreeNaviga.Nodes(UCase(aSpool.FileName) & "�SPOOL|" & (Fichiers(UCase(aSpool.FileName)).NbSpool & "�PAGE|" & Fichiers(UCase(aSpool.FileName)).Spools(Fichiers(UCase(aSpool.FileName)).NbSpool).NbPage))
            MDIMain.TreeNaviga_DblClick
      End If
    
    Case "MESSAGERIE"
        Set frmMail.aSpool = Me.aSpool
        frmMail.FromVisu = True
        frmMail.Show vbModal
            
        
    Case "EXPORTHTML"
        Set frmInternet.aSpool = Me.aSpool
        frmInternet.Show vbModal
        
    Case "HELP"
        
    Case "EXCEL"
        strRetour = ""
        frmExcel.FichierName = UCase$(Trim(aSpool.FileName))
        frmExcel.SpoolKey = UCase$(Trim(aSpool.Key))
        frmExcel.Init
        frmExcel.Show vbModal
        
    Case "SAVEASSPOOL"
        SaveSpool
End Select


End Sub

Private Sub SaveSpool()

'---> Cette proc�dure enregistre le spool en cours dans un fichier


Dim Rep As VbMsgBoxResult
Dim aLb As Libelle
Dim TempFile As String

On Error GoTo GestError

'-> Bloquer la feuille
Me.Enabled = False
Screen.MousePointer = 11

'-> G�n�rer une erreur sur ANNULER
MDIMain.OpenDlg.CancelError = True

MDIMain.OpenDlg.FileName = ""
MDIMain.OpenDlg.DefaultExt = "Turbo"
MDIMain.OpenDlg.Filter = "*.Turbo"

'-> Flags d'ouverture
MDIMain.OpenDlg.Flags = cdlOFNPathMustExist

'-> Afficher la feuille
MDIMain.OpenDlg.ShowSave

'-> V�rifier que le fichier n'existe pas d�ja
If Dir$(MDIMain.OpenDlg.FileName) <> "" Then
    '-> Pointer sur la classe libell�
    Set aLb = Libelles("MDIMAIN")
    Rep = MsgBox(aLb.GetCaption(25), vbQuestion + vbYesNo, aLb.GetToolTip(25))
    If Rep = vbNo Then GoTo GestError
    '-> Supprimer le fichier
    Kill MDIMain.OpenDlg.FileName
    '-> Lib�rer le pointeur
    Set aLb = Nothing
End If

'-> Cr�er un fichier de d�tail
TempFile = CreateDetail(Me.aSpool, -1)

'-> Le renommer
Name TempFile As MDIMain.OpenDlg.FileName

GestError:

'-> D�bloquer la feuille
Me.Enabled = True
Screen.MousePointer = 0


End Sub


Private Sub VScroll1_Change()
    Me.Page.Top = Me.VScroll1.Value
End Sub

