VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDetail 
   Caption         =   "Form1"
   ClientHeight    =   4605
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9630
   Icon            =   "frmDetail.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   307
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   642
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7920
      Top             =   1800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   48
      ImageHeight     =   48
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDetail.frx":08CA
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   900
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   1588
      ButtonWidth     =   1455
      ButtonHeight    =   1429
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   1
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            ImageIndex      =   1
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   2895
      Left            =   0
      TabIndex        =   0
      Top             =   960
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   5106
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmDetail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub ExportExcelList()

'---> Cette proc�dure exporte le contenu du tableau vers Excel

Dim Ligne As String
Dim x As ListItem
Dim aFeuille As Object
Dim aClasseur As Object
Dim MyApp As Object
Dim aRange As Object
Dim i As Integer
Dim aRangeToFormat As Object

On Error GoTo GestError

'-> Tester qu'excel existe
If Not IsExcel Then Exit Sub

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> Cr�er une nouvelle instance d'excel
Set MyApp = CreateObject("Excel.application")

'-> Ajouter une classeur
Set aClasseur = MyApp.Workbooks.Add()

'-> Supprimer les questions
MyApp.DisplayAlerts = False

'-> Supprimer les 2 feuilles en trop
aClasseur.Sheets(3).Delete
aClasseur.Sheets(2).Delete

'-> Get d'un pointeur vers la feuille active
Set aFeuille = aClasseur.ActiveSheet
Set aRange = aFeuille.Range("$A$1")


'-> Cr�er la ligne des entetes
For i = 1 To Me.ListView1.ColumnHeaders.Count
    If Ligne = "" Then
        Ligne = Me.ListView1.ColumnHeaders(i).Text
    Else
        Ligne = Ligne & "|" & Me.ListView1.ColumnHeaders(i).Text
    End If
Next

aRange.Value = Ligne
'-> Eclater sur les colonnes suivantes
aRange.TextToColumns Destination:=aFeuille.Range("" & aRange.AddressLocal & ""), DataType:=xlDelimited, _
    TextQualifier:=xlNone, ConsecutiveDelimiter:=False, Tab:=False, _
    Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar:="|"
'-> S�lectionner la ligne en entier
Set aRangeToFormat = aFeuille.Range(aRange, aRange.Offset(, Me.ListView1.ColumnHeaders.Count - 1))
'-> Appliquer un format
ApplicExcelFormat aRangeToFormat, 1

'-> S�lectionner la ligne suivante
Set aRange = aFeuille.Range("$A$2")


'-> Transf�rer le contenu du browse
For Each x In Me.ListView1.ListItems
    '-> Constituer la ligne d'export
    For i = 1 To Me.ListView1.ColumnHeaders.Count
        If i = 1 Then
            Ligne = x.Text
        Else
            Ligne = Ligne & "|" & x.SubItems(i - 1)
        End If
    Next
    '-> Transf�rer la ligne dans la cellule active
    aRange.Value = Ligne
    '-> Eclater sur les colonnes suivantes
    aRange.TextToColumns Destination:=aFeuille.Range("" & aRange.AddressLocal & ""), DataType:=xlDelimited, _
        TextQualifier:=xlNone, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :="|"
    '-> D�caler de 1 cellule vers le bas
    Set aRange = aRange.Offset(1, 0)
Next 'Pour toutes les lignes

'-> Appliquer le format d'un coup
Set aRangeToFormat = aFeuille.Range("$A$2", aFeuille.Range("$A$2").Offset(Me.ListView1.ListItems.Count - 1, Me.ListView1.ColumnHeaders.Count - 1))
ApplicExcelFormat aRangeToFormat, 12

'-> Formatter les colonnes
For i = 1 To Me.ListView1.ColumnHeaders.Count
    aFeuille.Columns(i).AutoFit
Next

'-> Rendre Excel visible
MyApp.Visible = True

'-> Rendre la main sur les questions
MyApp.DisplayAlerts = True

GestError:
    
    '-> Ecran
    Me.Enabled = True
    Screen.MousePointer = 0
    
    '-> Lib�rer les pointeurs
    Set aRange = Nothing
    Set aRangeToFormat = Nothing
    Set aFeuille = Nothing
    Set aClasseur = Nothing
    Set MyApp = Nothing


End Sub

Private Sub ApplicExcelFormat(aRange As Object, Ligne As Long)


'---> Cette proc�dure applique un format � une cellule Excel

aRange.Borders(xlDiagonalDown).LineStyle = xlNone
aRange.Borders(xlDiagonalUp).LineStyle = xlNone
With aRange.Borders(xlEdgeLeft)
    .LineStyle = xlContinuous
    .Weight = xlThin
    .ColorIndex = xlAutomatic
End With
With aRange.Borders(xlEdgeTop)
    .LineStyle = xlContinuous
    .Weight = xlThin
    .ColorIndex = xlAutomatic
End With
With aRange.Borders(xlEdgeBottom)
    .LineStyle = xlContinuous
    .Weight = xlThin
    .ColorIndex = xlAutomatic
End With
With aRange.Borders(xlEdgeRight)
    .LineStyle = xlContinuous
    .Weight = xlThin
    .ColorIndex = xlAutomatic
End With
With aRange.Borders(xlInsideVertical)
    .LineStyle = xlContinuous
    .Weight = xlThin
    .ColorIndex = xlAutomatic
End With

'-> Couleur de fond est gras si c'est le titre
If Ligne = 1 Then
    With aRange.Interior
        .ColorIndex = 15
        .Pattern = xlSolid
    End With
    aRange.Font.Bold = True
    With aRange
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .ShrinkToFit = False
        .MergeCells = False
    End With
End If


End Sub

Private Sub Form_Load()

Dim aLb As Libelle

'-> Pointer sur la classe libell�
Set aLb = Libelles("ACCESDET")

'-> Titre de la feuille
Me.Caption = aLb.GetCaption(3)

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next

GetClientRect Me.hwnd, aRect

Me.ListView1.Left = 0
Me.ListView1.Top = Me.Toolbar1.Height
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom - Me.ListView1.Top

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
ExportExcelList
End Sub
