VERSION 5.00
Begin VB.Form frmTempo 
   ClientHeight    =   1380
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7560
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   1380
   ScaleWidth      =   7560
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Tempo 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      ScaleHeight     =   225
      ScaleWidth      =   7065
      TabIndex        =   0
      Top             =   1080
      Width           =   7095
   End
   Begin VB.Image Image1 
      BorderStyle     =   1  'Fixed Single
      Height          =   705
      Left            =   6600
      Picture         =   "frmTempo.frx":0000
      Top             =   240
      Width           =   705
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Label2"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   5655
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   600
      Width           =   5775
   End
End
Attribute VB_Name = "frmTempo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Public ToUnload As Boolean

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

If Not ToUnload Then Cancel = 1

End Sub
