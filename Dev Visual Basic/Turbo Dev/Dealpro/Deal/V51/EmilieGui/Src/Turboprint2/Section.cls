VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Section"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************
'*                                            *
'* Cette classe contient la d�finition d'un   *
'* Objet Section                              *
'*                                            *
'**********************************************

'---> Liste des propri�t�s
Public Nom As String
Public Contour As Boolean
Public Hauteur As Single
Public Largeur As Single
Public AlignementTop As Integer
Public Top As Single
Public AlignementLeft As Integer
Public Left As Single
Public BackColor As Long
Public IdAffichage As Integer
Public nEntries As Integer
Private pOrdreAffichage() As String

'-> Valeurs pour la propri�t� AlignementTop
    '-> Libre (1)
    '-> Marge haut (2)
    '-> Centr� (3)
    '-> Marge Bas (4)
    '-> Sp�cifi� (5)
    
'-> Valeurs pour la propri�t� AlignementLeft
    '-> Libre (1) N'EST PLUS VALABLE
    '-> Marge gauche (2)
    '-> Centr� (3)
    '-> Marge droite (4)
    '-> Sp�cifi� (5)
    
'-> Pour AlignementTop et AlignementLeft , les valeurs Top et Left ne sont _
accessibles que pour la valeur Sp�cifi� (5)

Public Cadres As Collection
Public Bmps As Collection

'---> Maquette de transition
Public RangChar As String

'-> Pour chargement du contenu RTF
Public TempoRTF As Integer 'stocker le handle du fichier ouvert
Public TempoRtfString As String ' stocker le nom du fichier ouvert

Public IdRTf As Integer
    
Private Sub Class_Initialize()
    
    '-> Index du cadre
    nCadre = 1
    '-> Index du prochain objet dans le tableau des ordres d'affichage
    nEntries = 1
    '-> Index du prochain Bmp
    nBmp = 1
    
    '-> Initaliser les collections
    Set Cadres = New Collection
    Set Bmps = New Collection
    
End Sub
Public Function AddOrdreAffichage(ByVal Value As String) As Integer

ReDim Preserve pOrdreAffichage(1 To nEntries)
pOrdreAffichage(nEntries) = Value
AddOrdreAffichage = nEntries
nEntries = nEntries + 1


End Function

Public Function GetOrdreAffichage(ByVal nIndex As Integer) As String
    GetOrdreAffichage = pOrdreAffichage(nIndex)
End Function

