Attribute VB_Name = "Module1"
'---> Ceci est un commentaire de thierry
Public Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

On Error Resume Next

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Integer
Dim PosEnCour As Integer
Dim i As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
    

End Function

Public Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Integer

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Integer
Dim i As Integer
Dim PosAnalyse As Integer

On Error GoTo GestError

PosAnalyse = 1

Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1

Exit Function

GestError:

    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
    

End Function

Public Sub CreatePalette()

Dim aRange As Range
Dim aNode As Node
Dim aNode2 As Node
Dim i As Integer

'---> Cr�ation de la palette de navigation dans le classeur

On Error Resume Next

'-> Supprimer la mise � jour de l'�cran
Application.ScreenUpdating = False

'-> Activer la feuille Sommaire qui est masqu�e
Sheets("Sommaire").Visible = True

'-> Cellule de d�part
i = 1

'-> Initialiser la liste d'image
Set frmSommaire.TreeView1.ImageList = frmSommaire.ImageList1

'-> Vider le node
frmSommaire.TreeView1.Nodes.Clear

'-> Se d�placer vers la fin jusqu'� ce que l'on trouve un nouvel onglet
Do
    '-> Se postionner sur la cellule de r�f�rence
    Set aRange = Range("Sommaire!$A$" & CStr(i))
    '-> Quitter si le contenu de la cellule active est vide
    If aRange.Value = "" Then Exit Do
    '-> Selon le contenu de la cellule
    If UCase$(aRange.Value) = "NEW" Then
        '-> Ajouter un node de premier Niveau
        Set aNode = frmSommaire.TreeView1.Nodes.Add(, , , Range("Sommaire!$B$" & i).Value, "WorkSheet")
    Else
        '-> Ajouter un sous node au node en cours
        Set aNode2 = frmSommaire.TreeView1.Nodes.Add(, , , Range("Sommaire!$B$" & i).Value, "Table")
        Set aNode2.Parent = aNode
        aNode2.Tag = Range("Sommaire!$B$" & i).FormulaLocal
    End If
    '-> Incr�menter le compteur de cellule
    i = i + 1
Loop 'Analyse du contenu de la cellule active
 
'-> Masquer la feuille
Sheets("Sommaire").Visible = False

'-> Restituer la mise � jour de l'�cran
Application.ScreenUpdating = True



End Sub


Public Sub DisplayPalette()

'-> Cr�er la palette
CreatePalette

'-> Afficher le sommaire
frmSommaire.Show 0


End Sub
