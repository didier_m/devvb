/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 998/nathalie/inc/ns-cdeent-spe.pp                                                           !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52   668 04-01-02!Bug!Commarieu      !Specifs DEAL pour gestion Contrats et OPX                             !
!V51     1 14-06-01!New!Commarieu      !deal                                                                  !
!V51     1 12-04-01!New!Commarieu      !deal                                                                  !
!V51   230 06-01-01!New!Commarieu      !fourni                                                                !
!V51   229 05-01-01!New!Commarieu      !habili                                                                !
!V51   206 08-12-00!New!Commarieu      !xxxxxx                                                                !
!V51   161 09-11-00!New!Commarieu      !xxxxxx                                                                !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/

PROCEDURE AFTER-DISPLAY-SPE :

    def var i-par       as char         no-undo .
    def var o-par       as char         no-undo .
    def var i-i         as int          no-undo .
    def var x-x         as char         no-undo .
    def var x-y         as char         no-undo .
    def var h-h          as handle          no-undo .

    CASE CDE-MOTCLE :

        WHEN "VTA"
        THEN DO :

            /*    Chargement Th�me 'SPZ-DEAL' de BONENS   */    

/* �����    Remplace par le deal-scan      
            i-par = "R|BONENT|" + string( rowid( BONENT ) ) + "|SPZ-DEAL|0" .
            run n-ficsuit ( input i-par , input-output o-par ) .

            do i-i = 3 to num-entries( o-par , "|" ) :
                assign  broker-trav = entry ( i-i , o-par   , "|"  )   
                        x-x         = entry ( 1 , broker-trav , "="  )
                        x-y         = entry ( 2 , broker-trav , "="  ) .
                case x-x :
                    when "A01"  then  deal-put-screen( "xxxxxx" , x-y ) .
                end .  /*  case x-x  */
            end .  /*  i-i  */
����� */

            FIND BONENS where bonens.codsoc = bonent.codsoc
                        and   bonens.motcle = bonent.motcle
                        and   bonens.typcom = bonent.typcom
                        and   bonens.numbon = bonent.numbon
                        and   bonens.theme  = "SPZ-DEAL"
                        and   bonens.chrono = 0
                       no-lock  no-error .
            if available bonens  then
            do :
                x-x = deal-scan ( "fr-specif" , "_file=bonens _style=4" ) .
                h-h = buffer bonens:handle .
                deal-fieldget ( h-h , x-x ) .
            end .

            /*  Ajout dans Maintenance a preciser  */
            if gfic-create = yes  then  deal-put-screen( "dev-main" , "2" ) .

        END .  /*  CDE-MOTCLE = "VTA"  */

        WHEN "VTC"
        THEN DO :

            /*  Recherche Date de Cloture de CNT ou d'OPX  */
            i-par = "Read|BONENT|CLT|0" .
            o-par = string( rowid( bonent ) ) .
            run n-majtrte( i-par , input-output o-par ) .
            repeat :
                i-i = int( entry( 1 , o-par , "|" ) ) .
                if i-i <> 0  then  leave .
                x-x = entry( 3 , o-par , "|" ) no-error .
                if x-x = ?  or x-x = ""  then  leave .
                dat-test = date( x-x )  no-error .
                if error-status:error  or  dat-test = ?  then  leave .
                deal-put-screen ( "date-cloture" , x-x ) . 
                /*  deal-attribut ( "date-cloture" , "_sensitive=no" ) .  */
                leave .
            end .

        END .  /*  CDE-MOTCLE = "VTC"  */

    END .  /*  CASE CDE-MOTCLE  */

END . /*  PROCEDURE AFTER-DISPLAY-SPE  */

PROCEDURE AUTHORIZE-SPE :

    define input  parameter i-event as char no-undo .
    define output parameter x-err   as char no-undo .

    def var x-x         as char         no-undo .

    x-err = "" .

    if i-event = "GO"  and  cde-motcle = "VTA"  then
    do :
        x-x = deal-get-screen( "dev-main" ) .
        if x-x <> "0"  and  x-x <> "1"  then
        do :
            message  "Statut inclus dans la maintenance non pr�cis� ... "  view-as alert-box .
            x-err = "ERROR" .
        end .
    end .

END . /*  PROCEDURE AUTHORIZE-SPE  */

PROCEDURE BEFORE-TRANSAC-SPE :

    define input  parameter i-event as char no-undo .
    define output parameter x-err   as char no-undo .

    x-err = "" .

END . /*  PROCEDURE BEFORE-TRANSAC-SPE  */

PROCEDURE AFTER-TRANSAC-SPE :

    define input  parameter i-event as char no-undo .

    def var i-par       as char         no-undo .
    def var o-par       as char         no-undo .

    def var x-x         as char         no-undo .
    def var x-y         as char         no-undo .
    def var h-h          as handle          no-undo .

    CASE CDE-MOTCLE :

        WHEN "VTA"
        THEN DO :

            IF I-EVENT = "GO"  THEN
            DO :
                /*    Chargement Th�me 'SPZ-DEAL' de BONENS   */    

/* �����  Remplace par le deal-scan                
                assign  i-par = "U|BONENT|" + string( rowid( BONENT ) ) + "|SPZ-DEAL|0"
                        o-par = "A01=" + deal-get-screen( "xxxxxx" ) .
                run n-ficsuit( input i-par , input-output o-par ) . 
������ */        
                FIND BONENS where bonens.codsoc = bonent.codsoc
                            and   bonens.motcle = bonent.motcle
                            and   bonens.typcom = bonent.typcom
                            and   bonens.numbon = bonent.numbon
                            and   bonens.theme  = "SPZ-DEAL"
                            and   bonens.chrono = 0
                            exclusive-lock  no-error .
                if not available bonens  then
                do :
                    CREATE BONENS .
                    assign  bonens.codsoc = bonent.codsoc
                            bonens.motcle = bonent.motcle
                            bonens.typcom = bonent.typcom
                            bonens.numbon = bonent.numbon
                            bonens.theme  = "SPZ-DEAL"
                            bonens.chrono = 0 .
                end .
                x-x = deal-scan ( "fr-specif" , "_file=bonens _style=4" ) .
                h-h = buffer bonens:handle .
                deal-fieldput ( h-h , x-x , "" ) .

            END .  /*  I-EVENT = "GO"  */

            /*  DELETE Non necessaire car une boucle efface tous les BONENS dans ns-cdeent.p  */

        END .  /*  CDE-MOTCLE = "VTA"  */

        WHEN "VTC"
        THEN DO :

            IF I-EVENT = "GO"  THEN
            DO :
                o-par = string( rowid( bonent ) ) .
                /*  MaJ date de Cloture d'OPX ou CNT  */
                repeat :
                    x-x = deal-get-screen ( "date-cloture" ) .
                    if x-x = ?  or x-x = ""  then  leave .
                    dat-test = date( x-x )  no-error .
                    if error-status:error  or  dat-test = ?  then  leave .
                    i-par = "Update|BONENT|CLT|0|" + x-x .
                    run n-majtrte( i-par , input-output o-par ) .
                    leave .
                end .

            END .

        END .  /*  CDE-MOTCLE = "VTC"  */

    END .  /*  CASE MOTCLE  */

END . /*  PROCEDURE AFTER-TRANSAC-SPE  */

/*  Trigger des compositions des zones specifiques  */
PROCEDURE TRIGGERS-SPE :

    define input  parameter i-trigg  as char no-undo .
    define input  parameter i-event  as char no-undo .
    define input  parameter i-motcle as char no-undo .
    define input  parameter i-param  as char no-undo .
    define output parameter x-err    as char no-undo .

    def var i-i         as int              no-undo .
    def var x-x         as char             no-undo .
    def var x-y         as char             no-undo .

    x-err = "" .
    CASE I-TRIGG :

        WHEN "OPERAT"  THEN DO :

            /*  Contr�le que le chef de d�partement(4) appartient au m�me d�partement que le Demandeur(1) */
            if i-event <> "GET"  then  return .
            assign  x-y = substring( i-motcle , 7 , 1 )
                    i-i = int( x-y )
                    x-x = hdl-widget:screen-value .
            if i-i <> 4  then  return .
            /* ��� */

        END .

    END CASE . /*  I-MOTCLE  */

END . /*  PROCEDURE TRIGGERS-SPE  */

