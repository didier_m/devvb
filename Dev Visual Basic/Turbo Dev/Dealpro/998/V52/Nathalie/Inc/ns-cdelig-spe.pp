/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 998/nathalie/inc/ns-cdelig-spe.pp                                                           !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52   668 04-01-02!Bug!Commarieu      !Specifs DEAL pour gestion Contrats et OPX                             !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/

PROCEDURE AFTER-DISPLAY-SPE :

    def buffer X-BONLIG  for  BONLIG .

    def var h-h          as handle          no-undo .
    def var x-x        as char  no-undo .

    CASE CDE-MOTCLE :

        WHEN "VTC"
        THEN DO :

            /*    Chargement Th�me 'SPZ-DEAL' de BONLGS   */    
            FIND BONLGS where bonlgs.codsoc = bonlig.codsoc
                        and   bonlgs.motcle = bonlig.motcle
                        and   bonlgs.typcom = bonlig.typcom
                        and   bonlgs.numbon = bonlig.numbon
                        and   bonlgs.chrono = bonlig.chrono
                        and   bonlgs.theme  = "SPZ-DEAL"
                        and   bonlgs.ss-chrono = 0
                        no-lock  no-error .
            if available bonlgs
            then  do :
                x-x = deal-scan ( "fr-specif" , "_file=bonlgs _style=4" ) .
                h-h = buffer bonlgs:handle .
                deal-fieldget ( h-h , x-x ) .
            end .

        END .  /*  CDE-MOTCLE = "VTC"  */

    END .  /*  CASE CDE-MOTCLE  */

END . /*  PROCEDURE AFTER-DISPLAY-SPE  */

PROCEDURE AUTHORIZE-SPE :

    define input  parameter i-event as char no-undo .
    define output parameter x-err   as char no-undo .

END . /*  PROCEDURE AUTHORIZE-SPE  */

PROCEDURE BEFORE-TRANSAC-SPE :

    define input  parameter i-event as char no-undo .
    define output parameter x-err   as char no-undo .

END . /*  PROCEDURE BEFORE-TRANSAC-SPE  */


PROCEDURE AFTER-TRANSAC-SPE :

    define input  parameter i-event as char no-undo .

    def var x-x         as char         no-undo .
    def var h-h         as handle       no-undo .

    CASE CDE-MOTCLE :

        WHEN "VTC"
        THEN DO :

            IF I-EVENT = "GO"  THEN
            DO :

                /*    Chargement Th�me 'SPZ-DEAL' de BONLGS   */    
                FIND BONLGS where bonlgs.codsoc = bonlig.codsoc
                            and   bonlgs.motcle = bonlig.motcle
                            and   bonlgs.typcom = bonlig.typcom
                            and   bonlgs.numbon = bonlig.numbon
                            and   bonlgs.theme  = "SPZ-DEAL"
                            and   bonlgs.chrono = bonlig.chrono
                            and   bonlgs.ss-chrono = 0
                            exclusive-lock  no-error .
                if not available bonlgs  then
                do :
                    CREATE BONLGS .
                    assign  bonlgs.codsoc = bonlig.codsoc
                            bonlgs.motcle = bonlig.motcle
                            bonlgs.typcom = bonlig.typcom
                            bonlgs.numbon = bonlig.numbon
                            bonlgs.theme  = "SPZ-DEAL"
                            bonlgs.chrono = bonlig.chrono
                            bonlgs.ss-chrono = 0 .
                end .
                x-x = deal-scan ( "fr-specif" , "_file=bonlgs _style=4" ) .
                h-h = buffer bonlgs:handle .
                deal-fieldput ( h-h , x-x , "" ) .

            END .  /*  I-EVENT = "GO"  */

            /*  DELETE Non necessaire car une boucle efface tous les BONLGS dans ns-cdelig.p  */

        END .  /*  CDE-MOTCLE = "VTC"  */

    END .

END . /*  PROCEDURE AFTER-TRANSAC-SPE  */

/*  Trigger des compositions des zones specifiques  */
PROCEDURE TRIGGERS-SPE :

    define input  parameter i-trigg  as char no-undo .
    define input  parameter i-event  as char no-undo .
    define input  parameter i-motcle as char no-undo .
    define input  parameter i-param  as char no-undo .
    define output parameter x-err    as char no-undo .

    def var x-x         as char             no-undo .

    x-err = "" .
    CASE I-TRIGG :

        WHEN "������"  THEN DO :

        END .

    END CASE . /*  I-TRIGG  */

END . /*  PROCEDURE TRIGGERS-SPE  */