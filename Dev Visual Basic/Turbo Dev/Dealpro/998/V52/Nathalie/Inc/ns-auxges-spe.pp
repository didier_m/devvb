/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! 998/nathalie/inc/ns-auxges-spe.pp                                                           !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!------------------!---!---------------!----------------------------------------------------------------------!
!V52   744 22-01-02!Bug!Commarieu      !Modifs pour Julie DEAL                                                !
!-------------------------------------------------------------------------------------------------------------!
!                                                        M A I N                                              !
!-------------------------------------------------------------------------------------------------------------!
&End
**************************************************************************************************************/

PROCEDURE AFTER-DISPLAY-SPE :

    def var i-i        as int   no-undo .
    def var x-x        as char  no-undo .
    def var x-y        as char  no-undo .

    def var i-par      as char  no-undo .
    def var o-par      as char  no-undo .

    /*    Chargement Theme 'INFO-EDIT' de AUXGSS   */
    deal-put-screen(  "adr-comm" , "0" ) .
    i-par = "R|AUXGES|" + string( rowid( AUXGES ) ) + "|INFO-EDIT|0" .
    run n-ficsuit( input i-par , input-output o-par ) .
    if int( entry( 1 , o-par , "|" ) ) = 0  then
    do :
        mat-suit-sp[ 1 ] = 1 .
        do i-i = 3 to num-entries( o-par , "|" ) :

            assign  broker-trav = entry ( i-i , o-par   , "|"  )   
                    x-x         = entry ( 1 , broker-trav , "="  )
                    x-y         = entry ( 2 , broker-trav , "="  ) .
            if x-x = "A01"  then  deal-put-screen(  "adr-comm" , x-y ) .
        end .
    end.

END . /*  PROCEDURE AFTER-DISPLAY-SPE  */


PROCEDURE AUTHORIZE-SPE :

    define input  parameter i-event as char no-undo .
    define output parameter x-err   as char no-undo .

END . /*  PROCEDURE AUTHORIZE-SPE  */


PROCEDURE BEFORE-TRANSAC-SPE :

    define input  parameter i-event as char no-undo .
    define output parameter x-err   as char no-undo .

END . /*  PROCEDURE BEFORE-TRANSAC-SPE  */


PROCEDURE AFTER-TRANSAC-SPE :

    define input  parameter i-event as char no-undo .

    def var i-par       as char    no-undo .
    def var o-par       as char    no-undo .

    if i-event <> "GO"  then  return .

    /*    Chargement Theme 'INFO-EDIT' de AUXGSS   */
    assign  i-par = "U|AUXGES|" + string( rowid( AUXGES ) ) + "|INFO-EDIT|0"
            o-par = "A01=" + deal-get-screen( "adr-comm" ) .
    if mat-suit-sp[ 1 ] = 1  or  o-par <> "A01="  then
    do :   
         run n-ficsuit( input i-par , input-output o-par ) . 
    end .

END . /*  PROCEDURE AFTER-TRANSAC-SPE  */


/*  Trigger des compositions des zones specifiques  */
PROCEDURE TRIGGERS-SPE :

    define input  parameter i-trigg  as char no-undo .
    define input  parameter i-event  as char no-undo .
    define input  parameter i-motcle as char no-undo .
    define input  parameter i-param  as char no-undo .
    define output parameter x-err    as char no-undo .

END . /*  PROCEDURE TRIGGERS-SPE  */

