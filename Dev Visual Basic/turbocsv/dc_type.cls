VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "dc_type"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'***********************************************************
'* Description de la Enreg de l'affichage des donn�es  *
'***********************************************************

'-> D�finition des propri�t�s
Public Libel As String
Public Niveau As Integer
Public Image As Integer
Public NbStructure As Integer
Public Keyword As String
Public Structures As Collection

Public Function IsType(strKey As String) As Boolean
'-> cette fonction permet de savoir si un �l�ment appartient � la Enreg
Dim aType As dc_type

On Error GoTo gesterror
Set aType = cTypes.Item(strKey)
IsType = True
Set aType = Nothing
Exit Function

gesterror:
IsType = False
End Function

