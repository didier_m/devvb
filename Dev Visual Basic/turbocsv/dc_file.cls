VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "dc_file"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'***********************************************************
'* Description de la File de l'affichage des donn�es  *
'***********************************************************

'-> D�finition des propri�t�s
Public FileName As String
Public NbLignes As Long 'nombre total des lignes du fichier
Public NbCols As Long 'nombre maximal de colonnes du fichier
Public Lignes As Collection
Public Separator As String
Public FileOutPut As String
Public NbEntete As Integer 'nombre de lignes de l'entete
Public NbColonneHeader As Integer 'nombre de ligne pour les entes de colonne
Public NbColonneGauche As Integer
Public NbSection As Integer 'nombre de section
Public strEntete As String
