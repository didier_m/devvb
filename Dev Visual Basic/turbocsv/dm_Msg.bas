Attribute VB_Name = "dm_Msg"
Option Explicit

'-> Pour les messages saisis, valeur de retour
Public InputValue As String

'-> Type D'icone pour la fonction DisplayMessage
Public Enum DisplayMessageIcone
    dsmCritical = 1
    dsmWarning = 2
    dsmInterro = 3
    dsmQuestion = 4
End Enum

'-> Type de bouton par d�faut
Public Enum DisplayMessageTypeButton
    dsmOk = 1
    dsmCancel = 2
    dsmYes = 3
    dsmNo = 4
End Enum

'-> Type de combinaison de bouton
Public Enum DisplayMessageButton
    dsmOkOnly = 1
    dsmOkCancel = 2
    dsmYesNo = 3
    dsmYesNoCancel = 4
End Enum

'-> Fichier d'erreur supplementaire
Public DogFatalErrorFileHelp As String

Public Function DisplayMessage(Message As String, Icone As DisplayMessageIcone, Boutons As DisplayMessageButton, Titre As String, Optional IsInputBox As Boolean, Optional DefaultValue As String, Optional IsPasWord As Boolean, Optional DefaultButton As DisplayMessageTypeButton) As String

'---> Param�trage

Dim SavMousePointer As Integer

'-> R�cup�rer l'�tat du pointeur en cours
SavMousePointer = Screen.MousePointer
Screen.MousePointer = 0

On Error Resume Next

'Icone : 1 = Critical, 2 = Warning , 3 = Interro , 4 = Question
'Boutons : 1 = Ok , 2 = Ok + Annuler , 3 = Oui + Non , 4 = Oui + non + annuler

'-> Cahger la feuille ne m�moire
Load df_MsgBox

'-> Titre de la feuille
df_MsgBox.DogSkinObject1.Caption = Titre

'-> Corp du message
df_MsgBox.Label1.Caption = Replace(Message, Chr(13), Chr(13) & Chr(10))

'-> Si on est en inputbox , forcer l'icone et les boutons
If IsInputBox Then
    '-> Tester si on est en PassWord
    If IsPasWord Then df_MsgBox.Text1.PasswordChar = "*"
    '-> Vider la variable
    InputValue = ""
    '-> Ok, Annuler
    Boutons = 2
    '-> Images du clavier
    Icone = 5
    '-> Afficher la zone de saisie
    df_MsgBox.Text1.Visible = True
    df_MsgBox.shapeInput.Visible = True
    df_MsgBox.Text1.Text = DefaultValue
Else
    '-> Masquer la zone de saisie
    df_MsgBox.Text1.Visible = False
    df_MsgBox.shapeInput.Visible = False
End If

'-> Choix de l'icone
df_MsgBox.imgIco.Picture = df_MsgBox.ImageList1.ListImages(Icone).Picture

'-> Choix des boutons MESSPROG
Select Case Boutons
    Case 1 'Ok Only
        '-> Masquer le bouton 2
        df_MsgBox.NewButton2.Visible = False
        '-> Masquer le bouton 3
        df_MsgBox.NewButton3.Visible = False
        '-> Bouton Ok
        df_MsgBox.Label4.Caption = "Ok"
        df_MsgBox.NewButton1.Tag = "OK"
        '-> Positionner les valeurs par d�faut
        df_MsgBox.DefaultCancelValue = "OK"
        df_MsgBox.DefaultReturnValue = "OK"
        df_MsgBox.NewButton1.TabIndex = 0
    Case 2 'Ok Cancel
        '-> Masquer le bouton 3
        df_MsgBox.NewButton3.Visible = False
        '-> Libell� des boutons
        df_MsgBox.Label3.Caption = "OK"
        df_MsgBox.NewButton2.Tag = "OK"
        df_MsgBox.Label4.Caption = "Annuler"
        df_MsgBox.NewButton1.Tag = "ANNULER"
        '-> Positionner les valeurs par d�faut
        df_MsgBox.DefaultCancelValue = "ANNULER"
        df_MsgBox.DefaultReturnValue = "OK"
        df_MsgBox.NewButton2.TabIndex = 0
    Case 3 'Yes No
        '-> Masquer le bouton 3
        df_MsgBox.NewButton3.Visible = False
        '-> Libell� des boutons
        df_MsgBox.Label4.Caption = "Non"
        df_MsgBox.NewButton1.Tag = "NON"
        df_MsgBox.Label3.Caption = "Oui"
        df_MsgBox.NewButton2.Tag = "OUI"
        '-> Positionner les valeurs par d�faut
        df_MsgBox.DefaultCancelValue = "NON"
        df_MsgBox.DefaultReturnValue = "OUI"
        df_MsgBox.NewButton2.TabIndex = 0
    Case 4 'Yes Non Cancel
        '-> Libell� des boutons
        df_MsgBox.Label4.Caption = "Non"
        df_MsgBox.NewButton1.Tag = "NON"
        df_MsgBox.Label3.Caption = "Oui"
        df_MsgBox.NewButton2.Tag = "OUI"
        df_MsgBox.Label2.Caption = "Annuler"
        df_MsgBox.NewButton3.Tag = "ANNULER"
        '-> Positionner les valeurs par d�faut
        df_MsgBox.DefaultCancelValue = "ANNULER"
        df_MsgBox.DefaultReturnValue = "OUI"
        df_MsgBox.NewButton2.TabIndex = 0
End Select

'-> Repositionner le focus sur la zone de saisie
If IsInputBox Then df_MsgBox.Text1.TabIndex = 0

'-> Afficher la feuille de message
df_MsgBox.Show vbModal

'-> Afficher le retour
DisplayMessage = strRetour

'-> Vider la variabzle d'�change
strRetour = ""

'-> Restituer le pointeur d'origine
Screen.MousePointer = SavMousePointer

End Function






