VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "dc_structure"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'**************************************************************************
'* Description de la Enreg des donn�es qui sont lues dans le fichier  *
'**************************************************************************
Public Enum pType
    pNumeric
    pString
    pstringUcase
    pDate
End Enum

Public Enum pStatut
    pObligatoire
    pFacultatif
    pDep
    pNonUtilis�
    pstt
End Enum

Public enreg As String 'Valeur de l'enregistrement correspondant au champ
Public Position As Integer  'Position du champ
Public Keyword As String 'la cl� de l'enregistrement
Public Libel As String 'libell� du champ
Public sType As pType 'type de donn�e du champ
Public Longueur As Integer 'longueur du champ
Public StatutUR As pStatut
Public URSSAF As String
Public StatutAS As pStatut
Public ASSEDIC As String
Public StatutAG As pStatut
Public AGIRC As String
Public StatutCC As pStatut
Public CCPBTP As String
Public NomCoplat As String
Public Help As String

Public Function IsField(strKey As String) As Boolean
'-> cette fonction permet de savoir si un �l�ment appartient � la Enreg
Dim aField As dc_field

On Error GoTo GestError
Set aField = Fields.Item(strKey)
IsField = True
Set aField = Nothing
Exit Function

GestError:
IsField = False
End Function

Public Function NextField(strKey As String) As dc_field

Dim aField As dc_field
Dim TopNext As Boolean

For Each aField In Fields
    If aField.Keyword = strKey Then
        TopNext = True
    End If
    If TopNext = True Then
        Set NextField = aField
        Exit For
    End If
Next

End Function

