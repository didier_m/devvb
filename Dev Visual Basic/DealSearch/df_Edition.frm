VERSION 5.00
Object = "{DAFD85F1-343B-47EE-8170-1C26723F7A5D}#2.0#0"; "dogskin.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form df_Edition 
   BackColor       =   &H00CBB49A&
   BorderStyle     =   0  'None
   Caption         =   "Saisie des selections"
   ClientHeight    =   2250
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5925
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   2250
   ScaleWidth      =   5925
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture2 
      Height          =   255
      Left            =   2400
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   5
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin DogSkin.DealCheckBox DealCheckBox1 
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   1800
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   450
      BackColor       =   16579059
      Caption         =   "Format turbo"
      ForeColor       =   -2147483630
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   1
      Checked         =   0   'False
      SendToNextControl=   -1  'True
   End
   Begin DogSkin.DealCmdButton Picture1 
      Height          =   615
      Left            =   4560
      TabIndex        =   1
      Tag             =   "DICTAB�BUTTON-OK"
      Top             =   1500
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   1
      ButtonForm      =   1
      BackColor       =   16579059
   End
   Begin DogSkin.DogSkinObject DogSkinObject1 
      Left            =   0
      Top             =   0
      _ExtentX        =   979
      _ExtentY        =   953
      BorderColor     =   13077835
      BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderForeColor =   10053171
      HeaderBackColor =   16637889
      HeaderLineColor =   15847345
      HeaderUseLine   =   -1  'True
      BeginProperty BodyFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BodyForeColor   =   10053171
      BodyBackColor   =   16579059
      HeaderIco       =   "df_Edition.frx":0000
      HeaderIcoNa     =   "df_Edition.frx":0682
      ShadowColor     =   15847345
   End
   Begin DogSkin.DealCmdButton Picture3 
      Height          =   615
      Left            =   5160
      TabIndex        =   2
      Tag             =   "DICTAB�BUTTON-CANCEL"
      Top             =   1500
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   2
      ButtonForm      =   1
      BackColor       =   16579059
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   5715
      Left            =   120
      TabIndex        =   3
      Top             =   2280
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   10081
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      SmallIcons      =   "ImageList2"
      ForeColor       =   -2147483640
      BackColor       =   16777215
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin MSComctlLib.ImageCombo Combo1 
      Height          =   330
      Left            =   360
      TabIndex        =   0
      Tag             =   "DICT�VALIDE"
      Top             =   840
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   582
      _Version        =   393216
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
   End
   Begin DogSkin.DealCheckBox DealCheckBox2 
      Height          =   255
      Left            =   2400
      TabIndex        =   6
      Top             =   1800
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   450
      BackColor       =   16579059
      Caption         =   "Format auto"
      ForeColor       =   -2147483630
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   1
      Checked         =   0   'False
      SendToNextControl=   -1  'True
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00C78D4B&
      Height          =   855
      Left            =   120
      Top             =   600
      Width           =   5655
   End
End
Attribute VB_Name = "df_Edition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim LigneEntete(5000) As String
Dim TypeCombo As TypCombo
Enum TypCombo
    pExcel
    pAjout
End Enum

Private Sub Combo1_KeyPress(KeyAscii As Integer)
Dim aItem As ComboItem


If KeyAscii = 13 Then
    '-> on lance l'edition selon la selection effectu�e
Else
    KeyAscii = 0
End If

End Sub

Private Sub Combo1_KeyUp(KeyCode As Integer, Shift As Integer)
ComboSaisieAuto Combo1, False
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
    Case 113 'F2 validation
    
    Case 115, 27 'F4 ou esc annulation bon
        Unload Me
End Select

End Sub

Private Function isCol(strKey As String) As Boolean
'--> cette fonction permet de savoir si la colonne existe
Dim i As Integer
On Error GoTo GestError
i = Me.ListView1.ColumnHeaders(strKey).Index
'-> valeur de succes
isCol = True

Exit Function
GestError:
End Function

Private Function GetTrueValue(ByRef strKeyword As String, ByRef strValue As String) As String
'--> cette fonction va permettre de ramener une valeur de correspondance
GetTrueValue = GetIniFileValue(strKeyword, strValue, appTurbo)

'-> si on a rien ramen� charg� avec la valeur initiale
If GetTrueValue = "" Then GetTrueValue = strValue

End Function


Private Sub Picture3_Click()

'-> on quitte sans rien faire
Unload Me

End Sub

Public Sub PrintListView(ByRef aList As ListView, ByRef Spool As String, Optional Entete As String, Optional retourParam As String, Optional apend As Boolean)
    '-> Fonction qui imprime un Listview centr� sur une feuille A4 en l'envoyant sous turbo
    '-> Spool = chemin & nom de fichier turbo
    Dim MaqTurbo As Integer
    Dim LigneFic As String
    Dim LigneFic2(500) As String
    Dim IndSaut As Integer
    Dim i As Integer
    Dim j As Integer
    Dim Fichier As String
    Dim Portrait As Boolean
    Dim HauteurGrid As Long
    Dim LargeurGrid As Long
    Dim MargeTopEntete As Integer
    Dim MiseEnPage As String
    Dim Hauteur As Variant
    Dim HauteurRef As Long
    Dim k As Integer
    Dim l As Integer
    Dim TopPage As Boolean
    Dim NbPages As String
    Dim NoPage As Integer
    Dim IndEnt As Integer
    Dim aItem As ListItem
    Dim ColRupt As Integer
    Dim StrRupt As String
    Dim CurRupt As String
    Dim z As Integer
    
    NoPage = 1
    IndSaut = 0

    df_Drloading.Label1.Caption = "Cr�ation des pages..."
    DoEvents

    '-> si pas de ligne ne rien faire
    If aList.ListItems.Count = 0 Then Exit Sub
    
    On Error Resume Next
    
    '-> dans le cas ou on est pas deja sur un spool
    If Not apend Then
        '-> si le spool existe on le supprime
        If Dir(Spool) <> "" Then Kill Spool
    End If
    '-> On ouvre un fichier pour creer la maquette
    MaqTurbo = FreeFile
    Fichier = Spool
    Open Fichier For Append As #MaqTurbo
    '-> Calcul de la hauteur du grid
    For i = 0 To aList.ListItems.Count - 1
        HauteurGrid = HauteurGrid + aList.ListItems(1).Height
    Next
    '-> Calcul de la largeur grid
    For i = 1 To aList.ColumnHeaders.Count
        LargeurGrid = LargeurGrid + aList.ColumnHeaders.Item(i).Width
    Next

    '-> Definition du format de mise en page (portrait ou paysage )
    Portrait = False

    '-> ***** On envoie les renseignements de base pour la maquette (entete fichier)
    LigneFic = "[SPOOL]"
    Print #MaqTurbo, LigneFic
    LigneFic = "[MAQ]"
    Print #MaqTurbo, LigneFic
    LigneFic = "\DefEntete�Begin"
    Print #MaqTurbo, LigneFic
    LigneFic = "\Version�2"
    Print #MaqTurbo, LigneFic
    LigneFic = "\FieldListe�"
    Print #MaqTurbo, LigneFic
    LigneFic = "\Nom�" & Spool
    Print #MaqTurbo, LigneFic
    LigneFic = "\Date�" & Now
    Print #MaqTurbo, LigneFic
    Print #MaqTurbo, "\Description�Pas de Description"
    Print #MaqTurbo, "\Largeur�21"
    Print #MaqTurbo, "\Hauteur�29.7"

    '-> On decide si on place le grid en portrait ou paysage selon sa largeur
    If Portrait = True Then
        Print #MaqTurbo, "\Orientation�1" '-> Portrait
        MiseEnPage = 1
    Else
        Print #MaqTurbo, "\Orientation�0" '-> Paysage
        MiseEnPage = 0
    End If
    Entete = "O"  'essai
    retourParam = ",,,O"
    '-> Imprime une entete (page 0) si un titre a ete passe en parametre
    If Entete <> "" Then IndEnt = Printentete(MaqTurbo, MiseEnPage, Entete, Me.Picture2.ScaleX(LargeurGrid, 3, 7), aList)
    Entete = strRetour
    
    If Entete <> "" Then
        MargeTopEntete = 7
    Else
        MargeTopEntete = 0
    End If

    If Portrait = True Then
        Print #MaqTurbo, "\MargeTop�1" '& Str(MargeTopEntete + 1)
        Print #MaqTurbo, "\MargeLeft�" & Str((21 - Me.Picture2.ScaleX(LargeurGrid, 3, 7)) / 2) '-> On centre horizontalement
    Else
        Print #MaqTurbo, "\MargeTop�1" '& Str(MargeTopEntete + 1)
        If Me.Picture2.ScaleX(LargeurGrid, 3, 7) <= 29.7 - 0.5 Then
            Print #MaqTurbo, "\MargeLeft�" & Str((29.7 - Me.Picture2.ScaleX(LargeurGrid, 3, 7)) / 2 + 0.5)  '-> On centre horizontalement
        Else 'le tableau est plus large on cale sur la droite
            Print #MaqTurbo, "\MargeLeft�" & Str(1.2) '-> On centre horizontalement
        End If
    End If

    Print #MaqTurbo, "\Suite�"
    Print #MaqTurbo, "\Report�"
    Print #MaqTurbo, "\Entete�"
    Print #MaqTurbo, "\Pied�"
    Print #MaqTurbo, "\PageGarde�FAUX"
    Print #MaqTurbo, "\PageSelection�FAUX"
    Print #MaqTurbo, "\RightPage�VRAI"
    Print #MaqTurbo, "\Publipostage�"
    Print #MaqTurbo, "\DefEntete�End"
    Print #MaqTurbo, "[PROGICIEL]"
    Print #MaqTurbo, "\Prog=DEAL"
    Print #MaqTurbo, "\Rub=AUTRES"
    Print #MaqTurbo, "\Cli=DEAL"
    Print #MaqTurbo, "[HTML]"
    Print #MaqTurbo, "\Fichier�"
    Print #MaqTurbo, "\Rupture�"
    Print #MaqTurbo, "\HTML�End"
    '-> ******      Fin de la description entete maquette

    '-> ******      On declare le tableau
    Print #MaqTurbo, "[TB-Tableau]"
    '-> On definit le tableau dans la maquette graphique
    Print #MaqTurbo, "\LargeurTb�0"
    '-> On decide de l'orientation du tableau en fonction de sa largeur
    If Portrait = True Then
        Print #MaqTurbo, "\OrientationTB�1"
    Else
        Print #MaqTurbo, "\OrientationTB�0"
    End If
    '-> ******      Fin de la declaration du tableau
    
    '-> ******      On envoie l'entete (la premiere ligne du tableau) dans le fichier ascii
    If Entete <> "" Then
        For i = 0 To IndEnt
            Print #MaqTurbo, LigneEntete(i)
        Next
    End If
    '-> ******      fin de l'ecriture de l'entete
    
    '-> ******      on cree la ligne correspondant au libell�s de colonnes
    Print #MaqTurbo, "\Begin�Colonnes"
    Print #MaqTurbo, "\Largeur�" & Trim(Str(Me.Picture2.ScaleX(LargeurGrid, 3, 7)))
    Print #MaqTurbo, "\Hauteur�" & Trim(Str(Me.Picture2.ScaleX(aList.ListItems(1).Height, 3, 7) * 3 + 0.3))
    Print #MaqTurbo, "\AlignTop�1"
    Print #MaqTurbo, "\Top�0"
    Print #MaqTurbo, "\AlignLeft�0"
    Print #MaqTurbo, "\Left�3"
    Print #MaqTurbo, "\MasterLink�"
    Print #MaqTurbo, "\SlaveLink�"
    Print #MaqTurbo, "\RgChar�"
    Print #MaqTurbo, "\Ligne�1"
    Print #MaqTurbo, "\Colonne�" & aList.ColumnHeaders.Count
    Print #MaqTurbo, "\Varlig�"
    '-> Creation de la ligne de maquette en fonction des colonnes aListgrid
    LigneFic = "\Col�" & Trim(Str(Me.Picture2.ScaleX(aList.ListItems(1).Height, 3, 7) * 3)) & "�"
    For j = 0 To aList.ColumnHeaders.Count - 1
        z = GetColumn(aList, j)
        '-> Alignement de la cellule
        If z <> 0 Then
            Select Case aList.ColumnHeaders(z + 1).Alignment
                Case "0"
                    LigneFic = LigneFic & "1" & ";"
                Case "1"
                    LigneFic = LigneFic & "3" & ";"
                Case "2"
                    LigneFic = LigneFic & "2" & ";"
            End Select
        Else
            LigneFic = LigneFic & 1 & ";"
        End If
        '-> texte de la cellule avec gestion des sauts de ligne
        LigneFic = LigneFic & aList.ColumnHeaders(z + 1).Text & ";"
        '-> Largeur de la cellule
        LigneFic = LigneFic & Trim(Str(Me.Picture2.ScaleX(aList.ColumnHeaders(z + 1).Width, 3, 7))) & ";"
        '-> Bordure de la cellule ( x 4 )
        LigneFic = LigneFic & "VRAI,VRAI,VRAI,VRAI" & ";"
        '-> nom de la font
        LigneFic = LigneFic & aList.Font.Name & ";"
        '-> taille de la font
        LigneFic = LigneFic & aList.Font.Size & ";"
        '-> Cellule en gras
        If aList.Font.Bold = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> cellule en italic
        If aList.Font.Italic = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> Cellule en souligne
        If aList.Font.Underline = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> Couleur de police de la cellule
        LigneFic = LigneFic & aList.ForeColor & ";"
        '-> Couleur de fond   de la cellule
        LigneFic = LigneFic & &HFDDFC1 & ";"
        LigneFic = LigneFic & "VRAI;0@0@1|"
    Next j
    '-> on ecrit les infos de la colonne avec les entete des colonnes
    Print #MaqTurbo, LigneFic
    Print #MaqTurbo, "\Champs�"
    Print #MaqTurbo, "\End�Colonnes"
    '->*****        fin des libell�s des colonne
    
    '->*****        on definit la ligne standard du listview avec un champ par colonne
    Print #MaqTurbo, "\Begin�ColonnesStd"
    Print #MaqTurbo, "\Largeur�" & Trim(Str(Me.Picture2.ScaleX(LargeurGrid, 3, 7)))
    Print #MaqTurbo, "\Hauteur�" & Trim(Str(Me.Picture2.ScaleX(aList.ListItems(1).Height, 3, 7)))
    Print #MaqTurbo, "\AlignTop�1"
    Print #MaqTurbo, "\Top�0"
    Print #MaqTurbo, "\AlignLeft�0"
    Print #MaqTurbo, "\Left�2"
    Print #MaqTurbo, "\MasterLink�"
    Print #MaqTurbo, "\SlaveLink�"
    Print #MaqTurbo, "\RgChar�"
    Print #MaqTurbo, "\Ligne�1"
    Print #MaqTurbo, "\Colonne�" & aList.ColumnHeaders.Count
    Print #MaqTurbo, "\Varlig�"
    '-> Creation de la ligne de maquette en fonction des colonnes aListgrid
    '-> on pointe sur la premiere ligne (elles sont toutes identiques!)
    Set aItem = aList.ListItems(1)
    LigneFic = "\Col�" & Trim(Str(Me.Picture2.ScaleX(aItem.Height, 3, 7))) & "�"
    For j = 0 To aList.ColumnHeaders.Count - 1
        z = GetColumn(aList, j)
        '-> Alignement de la cellule
        If z <> 0 Then
            Select Case aList.ColumnHeaders(z + 1).Alignment
                Case "0"
                    LigneFic = LigneFic & "1" & ";"
                Case "1"
                    LigneFic = LigneFic & "3" & ";"
                Case "2"
                    LigneFic = LigneFic & "2" & ";"
            End Select
        Else
            LigneFic = LigneFic & "1" & ";"
        End If
        '-> texte de la cellule en fait le numero de champ correspondant a la colonne
        If z + 1 < 10 Then
            LigneFic = LigneFic & "^000" & (z + 1) & ";"
        Else
            LigneFic = LigneFic & "^00" & (z + 1) & ";"
        End If
        '-> Largeur de la cellule
        LigneFic = LigneFic & Trim(Str(Me.Picture2.ScaleX(aList.ColumnHeaders(z + 1).Width, 3, 7))) & ";"
        '-> Bordure de la cellule ( x 4 )
        LigneFic = LigneFic & "FAUX,VRAI,VRAI,VRAI" & ";"
        '-> nom de la font
        LigneFic = LigneFic & aList.Font.Name & ";"
        '-> taille de la font
        LigneFic = LigneFic & aList.Font.Size & ";"
        '-> Cellule en gras
        If aList.Font.Bold = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> cellule en italic
        If aList.Font.Italic = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> Cellule en souligne
        If aList.Font.Underline = True Then
            LigneFic = LigneFic & "VRAI;"
        Else
            LigneFic = LigneFic & "FAUX;"
        End If
        '-> Couleur de police de la cellule
        LigneFic = LigneFic & aList.ForeColor & ";"
        '-> Couleur de fond   de la cellule
        LigneFic = LigneFic & &HFFFFFF & ";"
        LigneFic = LigneFic & "FAUX;0@0@1|"
    Next j
    '-> on ecrit les infos de la colonne avec les entete des colonnes
    Print #MaqTurbo, LigneFic
    '-> on cree les champs
    LigneFic = "Champs�"
    For j = 0 To aList.ColumnHeaders.Count - 1
        z = GetColumn(aList, j)
        If z + 1 > 10 Then
            LigneFic = LigneFic & "00" & z + 1 & ";40 |"
        Else
            LigneFic = LigneFic & "000" & z + 1 & ";40 |"
        End If
    Next j
    LigneFic = Mid(LigneFic, 1, Len(LigneFic) - 1)
    Print #MaqTurbo, "\Champs�" & LigneFic
    Print #MaqTurbo, "\End�ColonnesSTD"
    '->*****          Fin de la construction d'une ligne standard ****************
    '-> on termine la maquette
    Print #MaqTurbo, "\Tableau�End"
    Print #MaqTurbo, "[/MAQ]"
    
    '->*****          On charge le tableau avec les donnees des lignes
    '-> hauteur en cours
    If Entete <> "" Then
        Hauteur = 2   '0
    Else
        Hauteur = 2
    End If
    '-> hauteur de reference pour determiner les sauts de pages
    If Portrait = True Then
        HauteurRef = 26.7
    Else
        HauteurRef = 18
    End If
    NbPages = Fix(Me.Picture2.ScaleX(HauteurGrid, 3, 7) / (HauteurRef - 1.9)) + 1
    '-> on charge de toute facon l'entete page et colonne
    Print #MaqTurbo, "[TB-Tableau(BLK-Entete)][\1]{^0001" & NoPage & "/" & NbPages & Space(5); "}"
    Print #MaqTurbo, "[TB-Tableau(BLK-Colonnes)][\1]"
    '-> on parcour les differentes lignes
    For i = 1 To aList.ListItems.Count
        '-> on pointe sur la ligne
        Set aItem = aList.ListItems(i)
        LigneFic = "{"
        '->on teste si on est sur une ligne de rupture ou pas
        If Trim(UCase(Entry(1, aItem.Key, "|"))) = "RUPTURE" Then
        Else 'cas des lignes normales
            '->creation de la ligne
            For j = 1 To aList.ColumnHeaders.Count
                z = GetColumn(aList, j)
                If z < 10 Then
                    If z = 1 Then
                        LigneFic = LigneFic & "^000" & z & Mid(aItem.Text, 1, 40) & Space(40 - Len(Mid(aItem.Text, 1, 40)))
                    Else
                        LigneFic = LigneFic & "^000" & z & Mid(aItem.ListSubItems(z - 1).Text, 1, 40) & Space(40 - Len(Mid(aItem.ListSubItems(z - 1).Text, 1, 40)))
                    End If
                Else
                    LigneFic = LigneFic & "^00" & z & Mid(aItem.ListSubItems(z - 1).Text, 1, 40) & Space(40 - Len(Mid(aItem.ListSubItems(z - 1).Text, 1, 40)))
                End If
            Next
        End If
        '-> on ecrit la ligne
        Print #MaqTurbo, "[TB-Tableau(BLK-ColonnesStd)][\1]" & LigneFic & "}"
        Hauteur = Hauteur + CVar(Me.Picture2.ScaleX(aItem.Height, 3, 7))
        '-> on regarde si on doit faire un saut de page
        If Hauteur > HauteurRef Then
            Hauteur = 2
            NoPage = NoPage + 1
            Print #MaqTurbo, "[PAGE]"
            'Print #MaqTurbo, "[TB-Tableau(BLK-Entete)][\1]{^0001" & NoPage & "/" & NbPages & Space(5) & "}"
            Print #MaqTurbo, "[TB-Tableau(BLK-Colonnes)][\1]"
        End If
    Next

    '-> On ferme le fichier
    Close MaqTurbo

    '->on lance l'edition par turbograph
    '-> ouvrir le spool avec l'editeur associ� au .turbo soit turbograph
    'ShellExecute Me.hwnd, "Open", Fichier, vbNullString, App.Path, 1
End Sub

Public Function Printentete(ByVal MaqTurbo As Integer, ByVal MiseEnPage As String, Optional retourParam As String, Optional LargeurGrid As Integer, Optional aList As ListView) As Integer
'-> Fonction qui edite une entete d'edition au format TURBO
Dim i As Integer
Dim j As Integer
Dim Largeur As Integer
Dim IndEnt As Integer
Dim NbRupt As String
Dim ColRupt As String

i = 0
IndEnt = 0
If MiseEnPage = 0 Then
    Largeur = 29.7
Else
    Largeur = 21
End If

'-> on charge le texte de l'entete

'-> On construit la ligne d'ent�te
LigneEntete(IndEnt) = "\Begin�Entete"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Largeur�" & Trim(Str(LargeurGrid))
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Hauteur�2"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\AlignTop�1"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Top�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\AlignLeft�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Left�0"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\MasterLink�"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\SlaveLink�"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\RgChar�"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Ligne�1"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Colonne�1"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Varlig�"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\ExportLig�"
IndEnt = IndEnt + 1
'-> hauteur
LigneEntete(IndEnt) = "\Col�2"
'-> alignement
LigneEntete(IndEnt) = LigneEntete(IndEnt) & "�4;"
'-> texte
LigneEntete(IndEnt) = LigneEntete(IndEnt) & strRetour & "        Page : ^0001;"
'-> largeur
If LargeurGrid > 27 Then
    LigneEntete(IndEnt) = LigneEntete(IndEnt) & Trim(Str(27)) & ";"
Else
    LigneEntete(IndEnt) = LigneEntete(IndEnt) & Trim(Str(LargeurGrid)) & ";"
End If
LigneEntete(IndEnt) = LigneEntete(IndEnt) & "FAUX,FAUX,FAUX,FAUX;Comic Sans MS;10;VRAI;FAUX;FAUX;0;16777215;VRAI;0@0@1;0@1"
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\Champs�0001;5 "
IndEnt = IndEnt + 1
LigneEntete(IndEnt) = "\End�Entete"
IndEnt = IndEnt + 1
'-> Fin de l'entete

Printentete = IndEnt

End Function

Private Function GetColumn(aList As ListView, Col As Integer) As Integer
'--> cette fonction recherche une colonne par rapport a sa position d'affichage et non pas par son indes
Dim i As Integer

'-> on parcourt les colonnes
For i = 1 To aList.ColumnHeaders.Count
    '-> on regarde la position
    If aList.ColumnHeaders(i).Position - 1 = Col Then
        GetColumn = aList.ColumnHeaders(i).SubItemIndex
        Exit For
    End If
    If i = aList.ColumnHeaders.Count Then GetColumn = i
Next

End Function

Private Sub ComboChargeTag(Combo As ImageCombo, Separateur As String, Optional NoShowTag As Boolean)
'--> cette proc�dure permet de charger les tags pours une combo en fonction
'    de la valeur du texte du type "tag - libelle tag" et si demand� on sort le tag du texte de la combo
Dim aItem As ComboItem

'-> on parcourt tous les element de la combo
For Each aItem In Combo.ComboItems
    '-> on affecte le tag
    aItem.Tag = Mid(Trim(Entry(1, aItem.Text, Separateur)), 1, 5)
    '-> si demand� on n'affiche pas le tag dans la combo
    If NoShowTag Then aItem.Text = Trim(Entry(2, aItem.Text, Separateur))
Next

End Sub

Public Sub ComboSaisieAuto(aCombo As ImageCombo, SaisieLibre As Boolean)
'--> cette proc�dure permet la saisie automatique dans une combo
Dim i As Long
Dim sel As Long
Dim aItem As ComboItem

'-> on parcours les elements de la combo
For Each aItem In aCombo.ComboItems
    sel = Len(aCombo.Text)
    '-> si on trouve le texte on l'affiche
    If (StrComp(Left$(aItem.Text, sel), aCombo.Text, vbTextCompare) = 0) And aCombo.Text <> "" Then
        aItem.Selected = True
        aCombo.SelStart = sel
        If Len(aCombo.Text) <> sel Then
            aCombo.SelLength = Len(aCombo.Text) - sel
        Else
            aCombo.SelStart = 0
            aCombo.SelLength = Len(aCombo.Text)
        End If
        GoTo Suite
    End If
Next
'-> on a rien trouv�
If Not SaisieLibre Then
    aCombo.Text = ""
End If
Suite:

End Sub

Public Sub ExportToExcel(aListView As ListView)

Dim Ligne As String
Dim x As ListItem
Dim aFeuille As Object
Dim aClasseur As Object
Dim MyApp As Object
Dim aRange As Object
Dim i As Integer
Dim aRangeToFormat As Object
Dim aCol As ColumnHeader
Dim ValueField As String
Dim NbCol As Integer
Dim ListViewFormat() As Variant

Screen.MousePointer = 11

'On Error GoTo GestError

If Not IsExcel Then
    Screen.MousePointer = 0
    DisplayMessage "Excel non install�", dsmCritical, dsmOkOnly, ""
    Exit Sub
End If

'-> Cr�er une nouvelle instance d'excel
'ShowWait "Export vers Excel en cours"
Set MyApp = CreateObject("Excel.application")

'-> Ajouter une classeur
Set aClasseur = MyApp.Workbooks.Add()

'-> Supprimer les questions
MyApp.displayalerts = False

'-> Supprimer les 2 feuilles en trop
aClasseur.Sheets(3).Delete
aClasseur.Sheets(2).Delete

'-> Get d'un pointeur vers la feuille active
Set aFeuille = aClasseur.ActiveSheet
Set aRange = aFeuille.Range("$A$1")

'-> Raz de la variable
NbCol = 0

'-> Cr�ation de la ligne d'entete
For Each aCol In aListView.ColumnHeaders
    '-> Cr�ation de la ligne
    Ligne = AddEntryInMatrice(Ligne, aCol.Text, "|")
    NbCol = NbCol + 1
Next 'Pour toutes les colonnes

'-> Cr�er la ligne des entetes
If Ligne <> "" Then
    aRange.Value = Ligne
    '-> Eclater sur les colonnes suivantes
    aRange.TextToColumns Destination:=aFeuille.Range("" & aRange.AddressLocal & ""), DataType:=1, _
            TextQualifier:=-4142, ConsecutiveDelimiter:=False, Tab:=False, _
            Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar:="|"
    '-> S�lectionner la ligne en entier
    Set aRangeToFormat = aFeuille.Range(aRange, aRange.Offset(, NbCol - 1))
    '-> Appliquer un format
    ApplicExcelFormat aRangeToFormat, 1
    '-> Vider la matrice
    Ligne = ""
End If 'Si on a trouv� les entetes

'-> valeurs par defaut
ListViewFormat = Array(Array(1, 2), Array(2, 2), Array(3, 2), Array(4, 2), Array(5, 2), Array(6, 2), Array(7, 2), Array(8, 2), Array(9, 2), Array(10, 2), Array(11, 2), Array(12, 2), Array(13, 2), Array(14, 2), Array(15, 2), Array(16, 2), Array(17, 2), Array(18, 2), Array(19, 2), Array(20, 2), Array(21, 2), Array(22, 2), Array(23, 2), Array(24, 2), Array(25, 2), Array(26, 2), Array(27, 2), Array(28, 2), Array(29, 2), Array(30, 2), Array(31, 2), Array(32, 2), Array(33, 2), Array(34, 2), Array(35, 2), Array(36, 2), Array(37, 2), Array(38, 2), Array(39, 2), Array(40, 2), Array(41, 2), Array(42, 2), Array(43, 2), Array(44, 2), Array(45, 2), Array(46, 2), Array(47, 2), Array(48, 2), Array(49, 2), Array(50, 2), Array(51, 2), Array(52, 2), Array(53, 2), Array(54, 2), Array(55, 2), Array(56, 2), Array(57, 2), Array(58, 2), Array(59, 2), Array(60, 2), Array(61, 2), Array(62, 2), Array(63, 2), Array(64, 2), Array(65, 2), Array(66, 2), Array(67, 2), Array(68, 2), Array(69, 2), Array(70, 2) _
 , Array(71, 2), Array(72, 2), Array(73, 2), Array(74, 2), Array(75, 2), Array(76, 2), Array(77, 2), Array(78, 2), Array(79, 2), Array(80, 2), Array(81, 2), Array(82, 2), Array(83, 2), Array(84, 2), Array(85, 2), Array(86, 2), Array(87, 2), Array(88, 2), Array(89, 2), Array(90, 2), Array(91, 2), Array(92, 2), Array(93, 2), Array(94, 2), Array(95, 2), Array(96, 2), Array(97, 2), Array(98, 2), Array(99, 2), Array(100, 2), Array(101, 2), Array(102, 2), Array(103, 2), Array(104, 2), Array(105, 2), Array(106, 2), Array(107, 2), Array(108, 2), Array(109, 2), Array(110, 2), Array(111, 2), Array(112, 2), Array(113, 2), Array(114, 2), Array(115, 2), Array(116, 2), Array(117, 2), Array(118, 2), Array(119, 2), Array(120, 2))
'-> on redimensionne le tableau
ReDim Preserve ListViewFormat(aListView.ColumnHeaders.Count)

'-> on reccupere le format du listview
For Each aCol In aListView.ColumnHeaders
    '-> on redimensionne le tableau en conservant l'aquis
    If aCol.Alignment = 1 Then
        '-> alignement � droite c'est un chiffre
        ListViewFormat(aCol.Index - 1)(1) = 1
    Else
        '-> les autres colonnes c'est du string
        ListViewFormat(aCol.Index - 1)(1) = 2
    End If
Next

'-> S�lectionner la ligne suivante
Set aRange = aFeuille.Range("$A$2")

'-> Exporter tous les enregsitrements
For Each x In aListView.ListItems
    'ShowWait "Export vers Excel en cours " & x.Index & "/" & aListView.ListItems.Count
    '-> Cr�er la ligne � exporter
    For i = 1 To aListView.ColumnHeaders.Count
        '-> Tester la valeur a ajouter
        If i = 1 Then
            ValueField = x.Text
        Else
            ValueField = x.SubItems(i - 1)
        End If
        '-> Ajouter dans la matrice des lignes
        Ligne = Ligne & ValueField & "|"
        'Ligne = AddEntryInMatrice(Ligne, ValueField, "|", CLng(i))
    Next 'Pour tous les champs
    
    '-> Exporter cet enregistrement si <> ""
    If Trim(Ligne) <> "" Then
        '-> Transf�rer la ligne dans la cellule active
        aRange.Value = Ligne
        '-> Eclater sur les colonnes suivantes
        If Me.DealCheckBox2.Checked Then
            aRange.TextToColumns Destination:=aFeuille.Range("" & aRange.AddressLocal & ""), DataType:=1, _
                TextQualifier:=-4142, ConsecutiveDelimiter:=False, Tab:=False, _
                Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar:="|"
        Else
            aRange.TextToColumns Destination:=aFeuille.Range("" & aRange.AddressLocal & ""), DataType:=1, _
                TextQualifier:=-4142, ConsecutiveDelimiter:=False, Tab:=False, _
                Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar:="|", FieldInfo:=ListViewFormat
        End If
    End If
    '-> D�caler de 1 cellule vers le bas
    Set aRange = aRange.Offset(1, 0)
    '-> Raz de la variable
    Ligne = ""
Next 'Pour tous les enregistrements dans la page

'-> Appliquer le format d'un coup
Set aRangeToFormat = aFeuille.Range("$A$2", aFeuille.Range("$A$2").Offset(aListView.ListItems.Count - 1, NbCol - 1))
ApplicExcelFormat aRangeToFormat, 12

'-> Formatter les colonnes en largeur automatique
For i = 1 To NbCol
    '-> Largeur automatique de la colonne
    aFeuille.Columns(i).AutoFit
Next

'-> Rendre Excel visible
MyApp.Visible = True

'-> Rendre la main sur les questions
MyApp.displayalerts = True

GestError:
           
    '-> Lib�rer les pointeurs
    Set aRange = Nothing
    Set aRangeToFormat = Nothing
    Set aFeuille = Nothing
    Set aClasseur = Nothing
    Set MyApp = Nothing

    '-> D�bloquer la mise � jour
    Screen.MousePointer = 0
    'ShowWait "Export vers Excel en cours", True
End Sub

Public Function IsExcel() As Boolean

'---> Cette procedure indique si Excel est install� sur le poste ou non

Dim ExcelApp As Object

On Error GoTo GestError

'-> Essayer de pointer sur l'objet
Set ExcelApp = CreateObject("Excel.application")

'-> Renvoyer une valeur de succ�s
IsExcel = True

GestError:
    Set ExcelApp = Nothing
    
End Function

Public Sub ApplicExcelFormat(aRange As Object, Ligne As Long)


'---> Cette proc�dure applique un format � une cellule Excel

On Error Resume Next

aRange.Borders(5).LineStyle = -4142
aRange.Borders(6).LineStyle = -4142
With aRange.Borders(7)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(8)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(9)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(10)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(11)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With
With aRange.Borders(12)
    .LineStyle = 1
    .Weight = 2
    .ColorIndex = -4105
End With


'-> Couleur de fond est gras si c'est le titre
If Ligne = 1 Then
    With aRange.Interior
        .ColorIndex = 15
        .Pattern = 1
    End With
    aRange.Font.Bold = True
    With aRange
        .HorizontalAlignment = -4108
        .VerticalAlignment = -4107
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .ShrinkToFit = False
        .MergeCells = False
    End With
End If

'-> Suppreimer les erreurs de formats
Err.Number = 0

End Sub

Public Sub FormatListView(List As Object)

'---> Cette proc�dure formatte les entetes d'un listView

Dim i As Long
Dim x As Object

'-> Ne rien faire si pas de colonnes
If List.ColumnHeaders.Count = 0 Then Exit Sub

'-> De base toujours cr�er un enregistrement avec les entetes de colonnes
Set x = List.ListItems.Add(, "DEALENREGENTETE")

For i = 0 To List.ColumnHeaders.Count - 1
    '-> Ajouter le libelle de l'entete de la colonne
    If i = 0 Then
        x.Text = List.ColumnHeaders(1).Text
    Else
        x.SubItems(i) = List.ColumnHeaders(i + 1).Text
    End If
    SendMessage List.hwnd, LVM_SETCOLUMNWIDTH, i, 0
Next

'-> Supprimer le premier enregistrement
List.ListItems.Remove ("DEALENREGENTETE")

End Sub

