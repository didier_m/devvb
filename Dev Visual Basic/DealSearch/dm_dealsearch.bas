Attribute VB_Name = "dm_dealSearch"
Option Explicit

Public Declare Function PostMessage Lib "user32.dll" Alias "PostMessageA" (ByVal Hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long

Private TempFileName As String
Private Ligne As String

Private Const vbBackslash = "\"
Private Const ALL_FILES = "*.*"

Private Type FILETIME
   dwLowDateTime As Long
   dwHighDateTime As Long
End Type

Private Type WIN32_FIND_DATA
   dwFileAttributes As Long
   ftCreationTime As FILETIME
   ftLastAccessTime As FILETIME
   ftLastWriteTime As FILETIME
   nFileSizeHigh As Long
   nFileSizeLow As Long
   dwReserved0 As Long
   dwReserved1 As Long
   cFileName As String * 260
   cAlternate As String * 14
End Type

Private Type FILE_PARAMS
   bRecurse As Boolean
   bFindOrExclude As Long
   nCount As Long
   nSearched As Long
   sFileNameExt As String
   sFileRoot As String
End Type

Private Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
   
Private Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
   
Private Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long

Private Declare Function lstrlen Lib "kernel32" Alias "lstrlenW" (ByVal lpString As Long) As Long

Private Declare Function PathMatchSpec Lib "shlwapi" Alias "PathMatchSpecW" (ByVal pszFileParam As Long, ByVal pszSpec As Long) As Long

Private fp As FILE_PARAMS
Private stopSearch As Boolean

Public Enum pTypeLigne
    pEntete
    pColumnHeader
    pData
    pSeparateur
End Enum

Public DSO As Object
Public hdlFile As Integer

Sub Main()

'-> on ouvre la feuille de chargement
df_DealSearch.init

End Sub

Private Function Uncote(strValue As String) As String
'--> cette fonction va permettre de sortir les ''
Uncote = Mid(strValue, 2, Len(strValue) - 2)
End Function

Public Function FileExist(Fichier As String) As Boolean

On Error GoTo GestError

If Dir$(Fichier) <> "" Then FileExist = True
Exit Function

GestError:
    FileExist = False

End Function

Public Function createFile(sRoot As String, sFile As String, Optional sFiltreDirectory As String) As String
    '--> cette fonction charge le listview a partir d'un chemin root
 Dim DSOP As Object
 Dim WFD As WIN32_FIND_DATA
 Dim hFile As Long
 Dim sP As String
 
 With fp
    .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
    .sFileNameExt = sFile                'fichier (* ? autoris�
    .bRecurse = 1                             'True = recherche recursive
    .bFindOrExclude = 1                       '0=inclure, 1=exclure
 End With
 
 Set DSO = CreateObject("DSOFile.OleDocumentProperties")
  
 hFile = FindFirstFile(sRoot & "*.*", WFD)
 If hFile <> -1 Then
    Do
      'DoEvents
      'si c'est un repertoire on boucle
       If (WFD.dwFileAttributes And vbDirectory) Then
          If Asc(WFD.cFileName) <> CLng(46) Then
              If fp.bRecurse Then
                  createFile sRoot & TrimNull(WFD.cFileName) & vbBackslash, sFile
              End If
          End If
       Else
         'doit etre un fichier..
          If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
              If sFiltreDirectory <> "" Then
                  If InStr(1, sRoot & TrimNull(WFD.cFileName), sFiltreDirectory) <> 0 Then
                           Put #hdlFile, , getFileInfo(sRoot & TrimNull(WFD.cFileName))
                      'Exit Do
                  End If
              Else
                  Put #hdlFile, , getFileInfo(sRoot & TrimNull(WFD.cFileName))
              End If
          End If
       End If
    Loop While FindNextFile(hFile, WFD)
 End If
Call FindClose(hFile)

Close #hdlFile

'-> Formatter le ListView
LockWindowUpdate 0
'FormatListView ListView1
DoEvents
End Function

Private Function QualifyPath(sPath As String) As String
   If Right$(sPath, 1) <> vbBackslash Then
      QualifyPath = sPath & vbBackslash
   Else
      QualifyPath = sPath
   End If
End Function

Private Function TrimNull(startstr As String) As String
   TrimNull = Left$(startstr, lstrlen(StrPtr(startstr)))
End Function

Private Function MatchSpec(sFile As String, sSpec As String) As Boolean
   MatchSpec = PathMatchSpec(StrPtr(sFile), StrPtr(sSpec)) = fp.bFindOrExclude
End Function

Public Function GetVariableEnv(strVariable As String) As String

Dim Res As Long
Dim lpbuffer As String

lpbuffer = Space$(500)
Res = GetEnvironmentVariable(strVariable, lpbuffer, Len(lpbuffer))
If Res <> 0 Then
    '-> Faire un trim sur le buffer
    lpbuffer = Mid$(lpbuffer, 1, Res)
Else
    lpbuffer = ""
End If

'-> Retouner la valeur
GetVariableEnv = lpbuffer

End Function

Public Function getFileInfo(sFile As String) As String
Dim DSOprop As Object

'-> on s'occupe maintenant des propri�t�s
DSO.Open sFile
'-> on pointe sur les propri�t�s
Set DSOprop = DSO.SummaryProperties
getFileInfo = sFile + ";"
getFileInfo = getFileInfo + DSOprop.Author + ";"
getFileInfo = getFileInfo + DSOprop.Comments + ";"
getFileInfo = getFileInfo + DSOprop.Keywords + ";"
getFileInfo = getFileInfo + DSOprop.Subject + ";"
getFileInfo = getFileInfo + DSOprop.Title + ";"
getFileInfo = getFileInfo + DSOprop.company + ";"
getFileInfo = getFileInfo + DSOprop.manager

End Function

Public Function GetFileName(MyFile As String) As String

Dim i As Integer

i = InStrRev(MyFile, "\")
If i = 0 Then
    GetFileName = MyFile
Else
    GetFileName = Mid$(MyFile, i + 1, Len(MyFile) - i)
End If


End Function

