Attribute VB_Name = "dm_ApiWin32"
Option Explicit

'******************************
'* D�finitions des constantes *
'******************************

Public Const WM_CLOSE& = &H10 '-> Fermeture d'une fen�tre
Public Const WM_ACTIVATE = &H6 '-> Activation d'une fen�tre
Public Const WA_INACTIVE = 0 '-> Constante de fen�tre inactive
Public Const LVM_SETCOLUMNWIDTH = 4126 '-> Setting auto de la largeur d'une colonne d'un listview
Public Const LVM_GETCOLUMNWIDTH = 4125 '-> Get de la largeur d'une colonne d'un listview
Public Const WM_CHILDACTIVATE = &H22 '-> Activer une fen�tre
Public Const WM_PAINT& = &HF '-> Forcer le dessin de la fen�tre
Public Const EM_LINEINDEX = &HBB '->R�cup�rer l'index du premier char d'une ligne dans un RTF
Public Const EM_LINELENGTH = &HC1 '-> R�cup�rer le nombre de char d'une ligne dans un RTF
Public Const EM_GETLINECOUNT As Long = &HBA '-> R�cup�rer le nombre de ligne dans RTF
Public Const WM_CREATE& = &H1 '-> Cr�ation d'une fen�tre
Public Const WM_KILLFOCUS = &H8 '-> perte du focus
Public Const WM_SETFOCUS = &H7 'Get du focus
Public Const WM_SIZE = &H5 'Event Resize
Public Const WM_USER = &H400 'Event de base
Public Const WS_THICKFRAME = &H40000 'Style de fen�tre
Public Const HTBOTTOMRIGHT = 17 'Constante de bouton droit
Public Const WM_NCLBUTTONDOWN = &HA1 'Pour redimension de la feuille
Public Const WM_NCLBUTTONUP = &HA2 'Pour simuler un bouton Up
Public Const WM_LBUTTONDOWN = &H201 'Pour d�placement de la fen�tre
Public Const WM_LBUTTONUP = &H202 'Pour d�placement de la fen�tre
Public Const WM_LBUTTONDBLCLK = &H203 'Pour DblClick
Public Const WS_EX_APPWINDOW = &H40000 'Pour affichage de la barre des titres
Public Const GWL_STYLE = (-16) 'Pour get du style d'une fen�tre
Public Const GWL_EXSTYLE = (-20) 'Pour get du style etendu d'une fen�tre
Public Const HTCAPTION = 2 'Simuler le d�placement dune feuille
Public Const HTCLOSE = 7 'Simuler une fermeture de fen�tre
Public Const WS_CAPTION = &HC00000 'Style de fen�tre
Public Const WM_MOUSELEAVE As Long = &H2A3& 'Souris qui quitte la feuille
Public Const WM_MOUSEMOVE = &H200 'Event Mouse Move
Public Const ACTIVEOBJECT_STRONG = 0 'Pour serveur ActiveX
Public Const ACTIVEOBJECT_WEAK = 1 'Pour serveur ActiveX
'-> Inscription dans le registre
Public Const REG_SZ = 1
Public Const HKEY_CURRENT_USER = &H80000001
'-> Pour r�duire ou agrandire les fen�tres
Public Const SW_MAXIMIZE = 3
Public Const SW_MINIMIZE = 6
Public Const SW_NORMAL = 1
'-> Pour emplacement des fen�tres
Public Const SWP_NOSIZE = &H1
Public Const SWP_SHOWWINDOW = &H40
Public Const SWP_NOACTIVATE = &H10
'-> Pour d�tection des messages
Public Const GWL_WNDPROC As Long = (-4&)
'-> Pour gestion du Tracking souris
Public Const TME_CANCEL = &H80000000
Public Const TME_HOVER = &H1&
Public Const TME_LEAVE = &H2&
Public Const TME_NONCLIENT = &H10&
Public Const TME_QUERY = &H40000000
'-> Pour gestion des process m�moire
Public Const PROCESS_QUERY_INFORMATION = &H400
Public Const STILL_ACTIVE = &H103
'-> Pour gestion des   adresses IP
Public Const MAX_HOSTNAME_LEN = 132
Public Const MAX_DOMAIN_NAME_LEN = 132
Public Const MAX_SCOPE_ID_LEN = 260
Public Const MAX_ADAPTER_NAME_LENGTH = 260
Public Const MAX_ADAPTER_ADDRESS_LENGTH = 8
Public Const MAX_ADAPTER_DESCRIPTION_LENGTH = 132
Public Const ERROR_BUFFER_OVERFLOW = 111
Public Const MIB_IF_TYPE_ETHERNET = 1
Public Const MIB_IF_TYPE_TOKENRING = 2
Public Const MIB_IF_TYPE_FDDI = 3
Public Const MIB_IF_TYPE_PPP = 4
Public Const MIB_IF_TYPE_LOOPBACK = 5
Public Const MIB_IF_TYPE_SLIP = 6

'*****************************
'* D�finition des Enregs *
'*****************************

'-> Pour gestion des fen�tres
Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type
'-> Pour get de la souris
Public Type POINTAPI
    x As Long
    y As Long
End Type
'-> Pour gestion de l'affichage des fen�tres
Public Type WINDOWPLACEMENT
        Length As Long
        flags As Long
        showCmd As Long
        ptMinPosition As POINTAPI
        ptMaxPosition As POINTAPI
        rcNormalPosition As RECT
End Type
'-> Pour interception de la souris
Public Type TRACKMOUSEEVENTTYPE
    cbSize As Long
    dwFlags As Long
    hwndTrack As Long
    dwHoverTime As Long
End Type
'-> Pour inscription dans la R.O.T.
Public Type GUIDs
    Data1 As Long
    Data2 As Integer
    Data3 As Integer
    Data4(0 To 7) As Byte
End Type

'-> Enreg des Adresses IP
Type IP_ADDR_STRING
    Next As Long
    IpAddress As String * 16
    IpMask As String * 16
    Context As Long
End Type
Type IP_ADAPTER_INFO
    Next As Long
    ComboIndex As Long
    AdapterName As String * MAX_ADAPTER_NAME_LENGTH
    Description As String * MAX_ADAPTER_DESCRIPTION_LENGTH
    AddressLength As Long
    Address(MAX_ADAPTER_ADDRESS_LENGTH - 1) As Byte
    Index As Long
    Type As Long
    DhcpEnabled As Long
    CurrentIpAddress As Long
    IpAddressList As IP_ADDR_STRING
    GatewayList As IP_ADDR_STRING
    DhcpServer As IP_ADDR_STRING
    HaveWins As Boolean
    PrimaryWinsServer As IP_ADDR_STRING
    SecondaryWinsServer As IP_ADDR_STRING
    LeaseObtained As Long
    LeaseExpires As Long
End Type
Type FIXED_INFO
    HostName As String * MAX_HOSTNAME_LEN
    DomainName As String * MAX_DOMAIN_NAME_LEN
    CurrentDnsServer As Long
    DnsServerList As IP_ADDR_STRING
    NodeType As Long
    ScopeId  As String * MAX_SCOPE_ID_LEN
    EnableRouting As Long
    EnableProxy As Long
    EnableDns As Long
End Type


'**********************
'* D�finition des API *
'**********************

'-> API de gestion pour TrackMouseEvent
Public Declare Function TrackMouseEvent Lib "user32" (lpEventTrack As TRACKMOUSEEVENTTYPE) As Long
Public Declare Function IsWindow Lib "user32" (ByVal hwnd&) As Long
Public Declare Function SetProp Lib "user32" Alias "SetPropA" (ByVal hwnd As Long, ByVal lpString As String, ByVal hData As Long) As Long
Public Declare Function GetProp Lib "user32" Alias "GetPropA" (ByVal hwnd As Long, ByVal lpString As String) As Long
Public Declare Function RemoveProp Lib "user32" Alias "RemovePropA" (ByVal hwnd&, ByVal lpString$) As Long

'-> Pour Lib�rer le pointeur de la souris
Public Declare Function ReleaseCapture Lib "user32" () As Long

'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'-> API Pour gestion du temps
Public Declare Function GetTickCount& Lib "kernel32" ()

'-> Pour Maj d'une fen�tre
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long

'-> Pour get de la fen�tre active
Public Declare Function GetActiveWindow Lib "user32" () As Long

'-> Style des fen�tres
Public Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long) As Long
Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Public Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc&, ByVal hwnd&, ByVal msg&, ByVal wParam&, ByVal lParam&) As Long

'-> Pour get des dimensions de la zone cliente d'une fen�tre et convertion de coordonn�es
Public Declare Function GetClientRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function ScreenToClient Lib "user32" (ByVal hwnd As Long, lpPoint As POINTAPI) As Long
Public Declare Function PtInRect Lib "user32" (lpRect As RECT, ByVal x As Long, ByVal y As Long) As Long
Public Declare Function GetWindowRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long
Public Declare Function SetWindowPlacement Lib "user32" (ByVal hwnd As Long, lpwndpl As WINDOWPLACEMENT) As Long
Public Declare Function GetWindowPlacement Lib "user32" (ByVal hwnd As Long, lpwndpl As WINDOWPLACEMENT) As Long
Public Declare Sub SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long)
Public Declare Function ShowWindow Lib "user32" (ByVal hwnd As Long, ByVal nCmdShow As Long) As Long

'-> API de gestion des r�gions
Public Declare Function CreateRectRgn Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Public Declare Function SetRectRgn Lib "gdi32" (ByVal hRgn As Long, ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Public Declare Function PtInRegion Lib "gdi32" (ByVal hRgn As Long, ByVal x As Long, ByVal y As Long) As Long

'-> API de GDI
Public Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long

'-> Api gestion des fichiers temporaires
Public Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Public Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long

'-> Nom de la machine
Public Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long

'-> API pour gestion des messages
Public Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)

'-> API de lecture de fichier INI
Public Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function GetPrivateProfileSection& Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long
Public Declare Function WritePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpString As String, ByVal lpFileName As String) As Long
Public Declare Function GetPrivateProfileSectionNames Lib "kernel32.dll" Alias "GetPrivateProfileSectionNamesA" (ByVal lpszReturnBuffer As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

'-> Pour ex�cution d'un programme ou proc�dure
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

'-> Pour r�cup�ration du param�trage des infos en local
Public Declare Function GetLocaleInfoVB& Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long)

'-> Pour gestion des fen�tres
Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
Public Declare Function EnableWindow Lib "user32" (ByVal hwnd As Long, ByVal fEnable As Long) As Long

'-> Pour inscription des serveurs ActiveX dans la ROT
Public Declare Function CLSIDFromProgID Lib "ole32.dll" (ByVal ProgID As Long, rclsid As GUIDs) As Long
Public Declare Function CoDisconnectObject Lib "ole32.dll" (ByVal pUnk As IUnknown, pvReserved As Long) As Long
Public Declare Function RegisterActiveObject Lib "oleaut32.dll" (ByVal pUnk As IUnknown, rclsid As GUIDs, ByVal dwFlags As Long, pdwRegister As Long) As Long
Public Declare Function RevokeActiveObject Lib "oleaut32.dll" (ByVal dwRegister As Long, ByVal pvReserved As Long) As Long

'-> Api pour gestion du Registre
Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal HKey As Long) As Long
Public Declare Function RegOpenKey Lib "advapi32.dll" Alias "RegOpenKeyA" (ByVal HKey As Long, ByVal lpSubKey As String, phkResult As Long) As Long
Public Declare Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal HKey As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, lpData As Any, ByVal cbData As Long) As Long
Public Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal HKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long

'-> API pour gestion des process
Public Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Public Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long

'-> API de d�termination des adresses IP
Public Declare Function GetNetworkParams Lib "IPHlpApi" (FixedInfo As Any, pOutBufLen As Long) As Long
Public Declare Function GetAdaptersInfo Lib "IPHlpApi" (IpAdapterInfo As Any, pOutBufLen As Long) As Long
Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

'-> API de r�cup�ration du user NT
Public Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long

'-> API pour pointer sur un objet
Public Declare Function VBObjPtr Lib "msvbvm50.dll" Alias "VarPtr" (ByVal pObj As IUnknown) As Long
Public Declare Sub CoTaskMemFree Lib "ole32.dll" (ByVal hMem As Long)

'-> API pour r�cup�ration des variables d'environnement
Public Declare Function GetEnvironmentVariable Lib "kernel32" Alias "GetEnvironmentVariableA" (ByVal lpName As String, ByVal lpBuffer As String, ByVal nSize As Long) As Long

