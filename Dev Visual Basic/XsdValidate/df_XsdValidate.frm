VERSION 5.00
Object = "{DAFD85F1-343B-47EE-8170-1C26723F7A5D}#2.0#0"; "dogskin.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form df_XsdValidate 
   BackColor       =   &H00FF8080&
   BorderStyle     =   0  'None
   Caption         =   "Deal Informatique"
   ClientHeight    =   5985
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10380
   Icon            =   "df_XsdValidate.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   399
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   692
   Begin VB.TextBox Text3 
      Height          =   2535
      Left            =   360
      MultiLine       =   -1  'True
      TabIndex        =   4
      Top             =   3000
      Width           =   9615
   End
   Begin VB.TextBox Text2 
      Height          =   285
      Left            =   360
      TabIndex        =   3
      Top             =   960
      Width           =   8895
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   360
      TabIndex        =   2
      Top             =   1560
      Width           =   8895
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   11400
      Picture         =   "df_XsdValidate.frx":57E2
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   0
      Top             =   240
      Visible         =   0   'False
      Width           =   480
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   11760
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   21
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":64AC
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":7186
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":8E60
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":9B3A
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":A814
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":B4EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":C1C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":CEA2
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":D77C
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":F456
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":FD30
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":10A0A
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":112E4
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":11BBE
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":12898
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":13572
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":13E4C
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":15B26
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":17800
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":1CFF2
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_XsdValidate.frx":1ECCC
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin DogSkin.DogSkinObject DogSkinObject1 
      Left            =   0
      Top             =   0
      _ExtentX        =   979
      _ExtentY        =   953
      BorderColor     =   13077835
      BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderForeColor =   10053171
      HeaderBackColor =   16637889
      HeaderLineColor =   15847345
      HeaderUseLine   =   -1  'True
      BeginProperty BodyFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BodyForeColor   =   10053171
      BodyBackColor   =   16579059
      HeaderIco       =   "df_XsdValidate.frx":209A6
      HeaderIcoNa     =   "df_XsdValidate.frx":21028
      ShadowColor     =   13078868
   End
   Begin DogSkin.DealPopupMenu DealPopupMenu1 
      Left            =   2400
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
   End
   Begin DogSkin.DealCmdButton Picture1 
      Height          =   615
      Left            =   8880
      TabIndex        =   7
      Tag             =   "DICTAB�BUTTON-OK"
      Top             =   2100
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   1
      ButtonForm      =   1
      BackColor       =   16777215
   End
   Begin DogSkin.DealCmdButton Picture3 
      Height          =   615
      Left            =   9480
      TabIndex        =   8
      Tag             =   "DICTAB�BUTTON-CANCEL"
      Top             =   2100
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   2
      ButtonForm      =   1
      BackColor       =   16777215
   End
   Begin DogSkin.DogSkinObject DogSkinObject2 
      Left            =   0
      Top             =   0
      _ExtentX        =   979
      _ExtentY        =   953
      BorderColor     =   13077835
      BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderForeColor =   10053171
      HeaderBackColor =   16637889
      HeaderLineColor =   15847345
      HeaderUseLine   =   -1  'True
      BeginProperty BodyFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BodyForeColor   =   10053171
      BodyBackColor   =   16579059
      HeaderIco       =   "df_XsdValidate.frx":216AA
      HeaderIcoNa     =   "df_XsdValidate.frx":21D2C
      ShadowColor     =   13078868
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Fichier Xsd de r�f�rence"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   360
      TabIndex        =   6
      Tag             =   "DICTAB�TVA"
      Top             =   1320
      Width           =   3615
   End
   Begin VB.Label DICTAB�BONTVA 
      BackStyle       =   0  'Transparent
      Caption         =   "Fichier � analyser de type Xml"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   360
      TabIndex        =   5
      Tag             =   "DICTAB�TVA"
      Top             =   720
      Width           =   3615
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FEF3E9&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  'Solid
      Height          =   3015
      Left            =   120
      Top             =   2760
      Width           =   10095
   End
   Begin VB.Image Image2 
      Height          =   480
      Left            =   9360
      MousePointer    =   99  'Custom
      Picture         =   "df_XsdValidate.frx":223AE
      ToolTipText     =   "Ouvre un nouveau fichier"
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   9360
      MousePointer    =   99  'Custom
      Picture         =   "df_XsdValidate.frx":22C78
      ToolTipText     =   "Ouvre un nouveau fichier"
      Top             =   840
      Width           =   480
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   360
      TabIndex        =   1
      Top             =   1050
      Width           =   8895
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H00FEF3E9&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  'Solid
      Height          =   1455
      Left            =   120
      Top             =   600
      Width           =   10095
   End
End
Attribute VB_Name = "df_XsdValidate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Public haveFocus As String

Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare Function TerminateProcess Lib "kernel32" (ByVal hProcess As Long, ByVal uExitCode As Long) As Long
Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, ByRef lpdwProcessId As Long) As Long
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Private Const SYNCHRONIZE = &H100000
Private Const PROCESS_TERMINATE As Long = &H1

Public Sub TerminateApp(strProg As String)
Dim target_hwnd As Long
Dim target_process_id As Long
Dim target_process_handle As Long

    '-> on pointe sur la fenetre.
    target_hwnd = FindWindow(vbNullString, strProg)
    If target_hwnd = 0 Then
        Exit Sub
    End If

    '-> on reccupere l'ID.
    GetWindowThreadProcessId target_hwnd, target_process_id
    If target_process_id = 0 Then
        Exit Sub
    End If

    ' ouvrir le process.
    target_process_handle = OpenProcess(SYNCHRONIZE Or PROCESS_TERMINATE, ByVal 0&, target_process_id)
    If target_process_handle = 0 Then
        Exit Sub
    End If

    '-> terminer process.
    TerminateProcess target_process_handle, 0&

    '-> fermer le process.
    CloseHandle target_process_handle
End Sub

Private Sub KillApp(strProg As String)
'-> cette proc�dure ferme les programes en cours
Dim Ret As String
Dim hwnd As Long

'-> Pour r�cup�rer le hWnd d'une application dont on connait le nom
'nom EXACT de la fen�tre (gestionnaire des t�ches pour l'obtenir)
hwnd = FindWindow(vbNullString, strProg)
If hwnd <> 0 Then PostMessage hwnd, 16, 0&, 0&    'fin de t�che sur l'application

End Sub

Public Sub init()
'-> on ouvre la fen�tre de selection de fichier le fichier en retour est stock� dans strretour
Dim appDir As String

Me.Show
'-> on charge les fichiers eventuels de l'ini
Me.Text1.Text = GetIniFileValue("PARAM", "XsdFile", searchExe("xsdvalidate.ini"))
Me.Text2.Text = GetIniFileValue("PARAM", "XmlFile", searchExe("xsdvalidate.ini"))

'-> on charge la couleur de fond desbouttons
Me.Picture1.BackColor = Me.DogSkinObject1.BodyBackColor
Me.Picture3.BackColor = Me.DogSkinObject1.BodyBackColor

End Sub

Private Sub Form_Load()

'-> on regardre si on doit mettre un skin
SkinColorModify Me

Me.DogSkinObject1.Initialisation False, "Analyseur de fichier XML", pMinMaxClose, True
Me.DogSkinObject1.SetMatriceEffect "Shape1|Shape2"

'-> Cr�er le premier niveau de menu
Me.DealPopupMenu1.AddMenu "", "F10", "", "F10"

'-> Initialisation des recherches
'-> Rechercher
Me.DealPopupMenu1.AddMenu "F10", "RECHERCHE", "Rechercher (CTRL+F)", "RECHERCHE"
Me.DealPopupMenu1.AddMenu "F10", "SUIVANT", "Suivant (F3)", "SUIVANT"
'-> Separation
Me.DealPopupMenu1.AddMenu "F10", "-1", "-", "-"
'-> Gestion des exports Excel
Me.DealPopupMenu1.AddMenu "F10", "EXCEL", "Exporter vers Excel", "EXCEL"
'-> Menu d'impression
Me.DealPopupMenu1.AddMenu "F10", "PRINT", "Imprimer", "PRINT"

'-> Initialiser le menu
Me.DealPopupMenu1.InitDealMenu

Image2.MouseIcon = Picture2

End Sub


Private Sub Form_Unload(Cancel As Integer)
    
TerminateApp "XsdValidate"

End Sub

Private Sub Image1_Click()
'--> va charger un fichier
Dim appDir As String

df_Drloading.Show
df_Drloading.Label1.Caption = "Chargement..."
appDir = GetIniFileValue("PARAM", "OpenDir", searchExe("XsdValidate.ini"))
If appDir <> "" Then
    If InStr(1, appDir, "%", vbTextCompare) <> 0 Then
        appDir = Entry(1, appDir, "%") & GetVariableEnv(Entry(2, appDir, "%")) & Entry(3, appDir, "%")
    End If
    df_OpenFile.init 0, appDir, "Choix du fichier", "*.*"
Else
    df_OpenFile.init 0, Replace(Me.Text2, GetFileName(Me.Text2.Text), ""), "Choix du fichier", "*.xml"
End If

Unload df_Drloading
df_OpenFile.Show vbModal
'-> on v�rifie l'existance d'un fichier � charger
If Trim(strRetour) = "" Then Exit Sub
Me.Text2.Text = strRetour

End Sub

Private Sub Image2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
Image2.BorderStyle = 1
End Sub

Private Sub Image2_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
Image2.BorderStyle = 0
End Sub

Private Sub Image2_Click()
'--> va charger un fichier
Dim appDir As String

df_Drloading.Show
df_Drloading.Label1.Caption = "Chargement..."
appDir = GetIniFileValue("PARAM", "OpenDir", searchExe("XsdValidate.ini"))
If appDir <> "" Then
    If InStr(1, appDir, "%", vbTextCompare) <> 0 Then
        appDir = Entry(1, appDir, "%") & GetVariableEnv(Entry(2, appDir, "%")) & Entry(3, appDir, "%")
    End If
    df_OpenFile.init 0, appDir, "Choix du fichier", "*.*"
Else
    df_OpenFile.init 0, Replace(Me.Text1, GetFileName(Me.Text1.Text), ""), "Choix du fichier", "*.xsd"
End If

Unload df_Drloading
df_OpenFile.Show vbModal
'-> on v�rifie l'existance d'un fichier � charger
If Trim(strRetour) = "" Then Exit Sub
Me.Text1.Text = strRetour

End Sub

Private Sub Picture1_Click()
'-> on lance l'analyse et affiche le resultat
Me.Text3.Text = ValidateXML(Me.Text2.Text, Me.Text1.Text)
End Sub

Private Sub Form_Resize()
'--> on redessine et repositionne les controles au retaillage de l'ecran
Call ScreenResize
End Sub

Private Sub ScreenResize()
'--> cette procedure g�re la pr�sentation de l'�cran

On Error GoTo GestError

'-> les listview


Me.Refresh

GestError:

End Sub

Private Sub Picture3_Click()
End
End Sub
