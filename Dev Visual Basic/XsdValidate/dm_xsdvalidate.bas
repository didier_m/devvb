Attribute VB_Name = "dm_xsdvalidate"
Option Explicit

Public Declare Function PostMessage Lib "user32.dll" Alias "PostMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long

Private hdlFile As Integer
Private TempFileName As String
Private Ligne As String

Private Const vbBackslash = "\"
Private Const ALL_FILES = "*.*"

Private Type FILETIME
   dwLowDateTime As Long
   dwHighDateTime As Long
End Type

Private Type WIN32_FIND_DATA
   dwFileAttributes As Long
   ftCreationTime As FILETIME
   ftLastAccessTime As FILETIME
   ftLastWriteTime As FILETIME
   nFileSizeHigh As Long
   nFileSizeLow As Long
   dwReserved0 As Long
   dwReserved1 As Long
   cFileName As String * 260
   cAlternate As String * 14
End Type

Private Type FILE_PARAMS
   bRecurse As Boolean
   bFindOrExclude As Long
   nCount As Long
   nSearched As Long
   sFileNameExt As String
   sFileRoot As String
End Type

Private Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
   
Private Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
   
Private Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long

Private Declare Function lstrlen Lib "kernel32" Alias "lstrlenW" (ByVal lpString As Long) As Long

Private Declare Function PathMatchSpec Lib "shlwapi" Alias "PathMatchSpecW" (ByVal pszFileParam As Long, ByVal pszSpec As Long) As Long

Private fp As FILE_PARAMS
Private stopSearch As Boolean

Public Enum pTypeLigne
    pEntete
    pColumnHeader
    pData
    pSeparateur
End Enum

Sub Main()
Dim StrError As String

'-> on initialise le message d'erreur
StrError = "Une erreur est survenue lors du chargement"

'-> selon que l'on est en mode console ou pas
If ZonChargGet("CONSOLE") <> "" Then
    '->TODO!!!!!
    
    End
End If

'-> on ouvre la feuille de chargement du Ducs
df_XsdValidate.init

End Sub

Public Function ZonChargGet(StrZonCharg As String) As String

'--> cette fonction retourne la valeur d'un zoncharge
Dim zcharge As String
Dim ztrav As String
Dim i As Integer

'-> on regarde si on a changer le command$
zcharge = Entry(2, Command$, "|")
zcharge = Replace(zcharge, "'", "")
zcharge = Replace(zcharge, Chr(34), "")

For i = 2 To NumEntries(zcharge, "_")
  ztrav = UCase$(Trim(Entry(1, Entry(i, zcharge, "_"), "=")))
  Select Case ztrav
    Case UCase(StrZonCharg)
        ZonChargGet = Trim(Entry(2, Entry(i, zcharge, "_"), "="))
        Exit For
  End Select
Next



End Function

Private Function Uncote(strValue As String) As String
'--> cette fonction va permettre de sortir les ''
Uncote = Mid(strValue, 2, Len(strValue) - 2)
End Function

Public Function FileExist(Fichier As String) As Boolean

On Error GoTo GestError

If Dir$(Fichier) <> "" Then FileExist = True
Exit Function

GestError:
    FileExist = False

End Function

Public Function searchExe(strFile As String) As String
'-> cette fonction va chercher les fichiers en fonction du param�trage
'   diff�rentes possibilit�s
'   rien
'   chemin spe|chemin std
'   gloident
Dim sPath As String
Dim sParam As String

stopSearch = False

If Command$ = "" Then
    sParam = "deal"
Else
    sParam = Command$
End If

If InStr(1, sParam, "|") <> 0 Then
    '-> on lit d'abord dans le spe puis le standard
    If Dir$(Entry(1, sParam, "|") & "\" & strFile) <> "" Then
        searchExe = Entry(1, sParam, "|") & "\" & strFile
    Else
        If Dir$(Entry(1, sParam, "|") & "\" & strFile) <> "" Then
            searchExe = Entry(1, sParam, "|") & "\" & strFile
        Else
            searchExe = App.Path & "\" & strFile
        End If
    End If
Else
    '-> � partir du gloident on va essayer de s'y retrouver!
    '-> on pointe sur le chemin de base
    sPath = App.Path
    '-> pour les test
    'sPath = "R:\V6\tools\exe"
    sPath = Replace(sPath, "/", "\")
    '-> on regarde si on est en v51 ou v52
    If InStr(1, sPath, "\V51\") <> 0 Or InStr(1, sPath, "\V52\") <> 0 Then
        If InStr(1, sPath, "\V51\") <> 0 Then
            sPath = Mid(sPath, 1, InStr(1, sPath, "\V51\", vbTextCompare) + 4)
        Else
            sPath = Mid(sPath, 1, InStr(1, sPath, "\V52\", vbTextCompare) + 4)
        End If
        '-> repertoire v52/sophie de l'ident
        If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\deal\", "\" & Trim(sParam) & "\", , , vbTextCompare) & "\Sophie\", strFile)
        '-> repertoire v52/sophie de deal
        If searchExe = "" Then searchExe = SearchForFiles(sPath & "\Sophie\", strFile)
    Else '-> on regarde si on est en V6
        If InStr(1, sPath, "\dog\", vbTextCompare) <> 0 Or InStr(1, sPath, "\tools\", vbTextCompare) <> 0 Then
            sPath = Replace(sPath, "\tools\", "\dog\", , , vbTextCompare)
            sPath = Mid(sPath, 1, InStr(1, sPath, "\dog\", vbTextCompare) + 4)
            '-> on cherche dans le repertoire maquette du client
            If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\dog\", "\" & Trim(sParam) & "\sophie\", , , vbTextCompare), strFile)
            '-> on cherche dans le repertoire spe du client
            If searchExe = "" Then searchExe = SearchForFiles(sPath & "\" & sParam & "\", strFile)
            '-> on cherche dans le repertoire des maquettes
            If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\dog\", "\maq\gui\sophie\", , , vbTextCompare), strFile)
        End If
    End If
    '-> on regarde dans le repertoire courant
    If searchExe = "" Then searchExe = SearchForFiles(App.Path & "\" & Trim(sParam) & "\", strFile)
    If searchExe = "" Then searchExe = SearchForFiles(App.Path & "\", strFile)
    '-> on ramene le repertoire courant
    If searchExe = "" Then searchExe = App.Path & "\" & strFile
End If

End Function

Private Function SearchForFiles(sRoot As String, sfile As String, Optional sFiltreDirectory As String) As String
    '--> cette fonction cherche des fichiers � partir d'une directorie
   Dim WFD As WIN32_FIND_DATA
   Dim hFile As Long
  
   With fp
      .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
      .sFileNameExt = sfile                'fichier (* ? autoris�
      .bRecurse = 1                             'True = recherche recursive
      .bFindOrExclude = 1                       '0=inclure, 1=exclure
   End With
   
   
   hFile = FindFirstFile(sRoot & "*.*", WFD)
   If hFile <> -1 Then
      Do
        If stopSearch = True Then Exit Function
        DoEvents
        'si c'est un repertoire on boucle
         If (WFD.dwFileAttributes And vbDirectory) Then
            If Asc(WFD.cFileName) <> CLng(46) Then
                If fp.bRecurse Then
                    SearchForFiles = SearchForFiles(sRoot & TrimNull(WFD.cFileName) & vbBackslash, sfile)
                End If
            End If
         Else
           'doit etre un fichier..
            If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
                If sFiltreDirectory <> "" Then
                    If InStr(1, sRoot & TrimNull(WFD.cFileName), sFiltreDirectory) <> 0 Then
                        SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                        stopSearch = True
                        Exit Do
                    End If
                Else
                    SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                    stopSearch = True
                    Exit Do
                End If
            End If
         End If
      Loop While FindNextFile(hFile, WFD)
   End If
   Call FindClose(hFile)
End Function

Private Function QualifyPath(sPath As String) As String
   If Right$(sPath, 1) <> vbBackslash Then
      QualifyPath = sPath & vbBackslash
   Else
      QualifyPath = sPath
   End If
End Function

Private Function TrimNull(startstr As String) As String
   TrimNull = Left$(startstr, lstrlen(StrPtr(startstr)))
End Function

Private Function MatchSpec(sfile As String, sSpec As String) As Boolean
   MatchSpec = PathMatchSpec(StrPtr(sfile), StrPtr(sSpec)) = fp.bFindOrExclude
End Function

Public Sub SkinColorModify(aForm As Form)
'--> ceci est un essai de changement de couleur
Dim Lcolor As Long
Dim aControl As Control
Dim aPicture As PictureBox
Dim bKeepBorder As Boolean
Dim sKinName As String

On Error Resume Next

'-> V�rifier que l'on trouve le fichier d'application du skin
sKinName = searchExe("Skin.ini")
If Dir$(sKinName) = "" Then Exit Sub

'-> Appliquer un skin s'il y en a un
If GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BodyBackColor = getColor(GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "BODYFORECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BodyForeColor = getColor(GetIniFileValue("SKIN", "BODYFORECOLOR", sKinName))
If GetIniFileValue("SKIN", "BORDERCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BorderColor = getColor(GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERBACKCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderBackColor = getColor(GetIniFileValue("SKIN", "HEADERBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERFORECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderForeColor = getColor(GetIniFileValue("SKIN", "HEADERFORECOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERLINECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderLineColor = getColor(GetIniFileValue("SKIN", "HEADERLINECOLOR", sKinName))
If GetIniFileValue("SKIN", "SHADOWCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.ShadowColor = getColor(GetIniFileValue("SKIN", "SHADOWCOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERUSELINE", sKinName) <> "" Then aForm.DogSkinObject1.HeaderUseLine = Val(GetIniFileValue("SKIN", "HEADERUSELINE", sKinName))
If GetIniFileValue("SKIN", "BODYFONTNAME", sKinName) <> "" Then aForm.DogSkinObject1.BodyFont.Name = GetIniFileValue("SKIN", "BODYFONTNAME", sKinName)
If GetIniFileValue("SKIN", "BODYFONTSIZE", sKinName) <> "" Then aForm.DogSkinObject1.BodyFont.Size = CDbl(GetIniFileValue("SKIN", "BODYFONTSIZE", sKinName))
If GetIniFileValue("SKIN", "BODYFONTBOLD", sKinName) <> "" Then aForm.DogSkinObject1.BodyFont.Bold = Val(GetIniFileValue("SKIN", "BODYFONTBOLD", sKinName))
If GetIniFileValue("SKIN", "BODYFONTITALIC", sKinName) <> "" Then aForm.DogSkinObject1.BodyFont.Italic = Val(GetIniFileValue("SKIN", "BODYFONTITALIC", sKinName))
If GetIniFileValue("SKIN", "HEADERICO", sKinName) <> "" Then
    '-> on regarde si on trouve bien l'image
    If Dir(GetIniFileValue("SKIN", "HEADERICO", sKinName)) <> "" Then
        aForm.DogSkinObject1.HeaderIco = LoadPicture(GetIniFileValue("SKIN", "HEADERICO", sKinName))
        aForm.DogSkinObject1.HeaderIcoNa = LoadPicture(GetIniFileValue("SKIN", "HEADERICO", sKinName))
    Else
        '-> on regarde si l'icone n'est pas dans le repertoire courant
        aForm.DogSkinObject1.HeaderIco = LoadPicture(App.Path & "\" & GetIniFileValue("SKIN", "HEADERICO", sKinName))
        aForm.DogSkinObject1.HeaderIcoNa = LoadPicture(App.Path & "\" & GetIniFileValue("SKIN", "HEADERICO", sKinName))
    End If
End If
If GetIniFileValue("SKIN", "HEADERICONA", sKinName) <> "" Then aForm.DogSkinObject1.HeaderUseLine = Val(GetIniFileValue("SKIN", "HEADERICONA", sKinName))
If GetIniFileValue("SKIN", "HEADERFONTNAME", sKinName) <> "" Then aForm.DogSkinObject1.HeaderFont.Name = GetIniFileValue("SKIN", "HEADERFONTNAME", sKinName)
If GetIniFileValue("SKIN", "HEADERFONTSIZE", sKinName) <> "" Then aForm.DogSkinObject1.HeaderFont.Size = CDbl(GetIniFileValue("SKIN", "HEADERFONTSIZE", sKinName))
If GetIniFileValue("SKIN", "HEADERFONTBOLD", sKinName) <> "" Then aForm.DogSkinObject1.HeaderFont.Bold = Val(GetIniFileValue("SKIN", "HEADERFONTBOLD", sKinName))
If GetIniFileValue("SKIN", "HEADERFONTITALIC", sKinName) <> "" Then aForm.DogSkinObject1.HeaderFont.Italic = Val(GetIniFileValue("SKIN", "HEADERFONTITALIC", sKinName))
If GetIniFileValue("SKIN", "KEEPBORDER", sKinName) = "1" Then bKeepBorder = True
'-> on parcours les differents elements pour faire une variation de couleur
For Each aControl In aForm
    If TypeOf aControl Is Shape Then
        If aControl.BackColor = &HF1CFB1 Or aControl.BackColor = 16708585 Then aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
        If aControl.BorderColor = &HC78D4B Then aControl.BorderColor = aForm.DogSkinObject1.BorderColor
    End If
    If TypeOf aControl Is DealCmdButton Or TypeOf aControl Is PictureBox Or TypeOf aControl Is DealCheckBox Or TypeOf aControl Is TextBox Then
        If aControl.BackColor = &HF1CFB1 Or aControl.BackColor = 16579059 Then aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
    End If
    If TypeOf aControl Is DealGrid Then
        aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
        aControl.BackColorFixed = aForm.DogSkinObject1.ShadowColor
        aControl.BackColorSel = aForm.DogSkinObject1.ShadowColor
        aControl.GridLineColor = aForm.DogSkinObject1.HeaderForeColor
    End If
    If TypeOf aControl Is DealTextBox Or TypeOf aControl Is Label Then
        aControl.ForeColor = aForm.DogSkinObject1.HeaderForeColor
    End If
    If TypeOf aControl Is Label And bKeepBorder Then
        aControl.BorderStyle = 0
    End If

Next

End Sub

Public Function GetVariableEnv(strVariable As String) As String

Dim Res As Long
Dim lpbuffer As String

lpbuffer = Space$(500)
Res = GetEnvironmentVariable(strVariable, lpbuffer, Len(lpbuffer))
If Res <> 0 Then
    '-> Faire un trim sur le buffer
    lpbuffer = Mid$(lpbuffer, 1, Res)
Else
    lpbuffer = ""
End If

'-> Retouner la valeur
GetVariableEnv = lpbuffer

End Function

Public Function ValidateXML(ByVal sXMLPath As String, ByVal sXSDPath As String) As String


Dim sNamespace As String
Dim objSchemas As Object
Dim oXML As Object
Dim oXSD As Object
Dim oErr As Object
    
    Set oXML = CreateObject("MSXML2.DOMDocument") 'Nouvelle instance d'un doc XML
    oXML.validateOnParse = True 'Validaiton du parseur sur le fichier
    
    If oXML.Load(sXMLPath) Then
        ValidateXML = ""
    Else
        ValidateXML = "Le fichier n'est pas conforme a son DTD" & vbCrLf
        With oXML.parseError
            ValidateXML = ValidateXML & vbCrLf _
             & "Erreur " & .errorCode & ":" & .reason & vbCrLf _
             & "Fichier : " & .url & vbCrLf _
             & "Ligne : " & .Line & vbCrLf _
             & "Charactere : " & .linepos & vbCrLf & vbCrLf _
             & "Extrait : " & .srcText & vbCrLf
        End With
    End If
    
    If ValidateXML <> "" Then Exit Function
    On Error Resume Next
    'ValidateXML = ""
    
    'Load XSD as DOM to populate in Schema Cache
    Set oXSD = CreateObject("MSXML2.DOMDocument.4.0")
    If Err.Number <> 0 Then
        ValidateXML = "1 " + Err.Description
        Exit Function
    End If
    oXSD.async = False
    If Not oXSD.Load(sXSDPath) Then
        ValidateXML = "Fail - XSD Load failure"
        Exit Function
    Else
        'Get namespace name from XSD TargetNamespace attribute
        sNamespace = oXSD.documentElement.getAttribute("targetNamespace")
    End If
   
    'Populate schema cache
    Set objSchemas = CreateObject("MSXML2.XMLSchemaCache.4.0")
    If Err.Number <> 0 Then
       ValidateXML = "2 " + Err.Description
      Exit Function
    End If
    objSchemas.Add sNamespace, oXSD
   
    'Load XML file (without validation - that comes later)
    Set oXML = CreateObject("MSXML2.DOMDocument.4.0")
    If Err.Number <> 0 Then
        ValidateXML = "3 " + Err.Description
        Exit Function
    End If
    oXML.async = False
    oXML.validateOnParse = False
    oXML.resolveExternals = False
   
    'Load XML, without any validation
    If Not oXML.Load(sXMLPath) Then
        ValidateXML = "Fail - XML Load failure"
        Exit Function
    End If
   
    'Bind Schema Cache to DOM
    Set oXML.schemas = objSchemas
   
    'Does this XML measure up?
    Set oErr = oXML.Validate()
 
    If oErr.errorCode <> 0 Then
        ValidateXML = "Fail - " & oErr.reason
    Else
        ValidateXML = "Le fichier semble coh�rent..."
    End If
End Function

Public Function getColor(sColor As String) As Long
'--> cette fonction reccupere la couleur
Dim r, V, b As Integer
On Error Resume Next
'-> on decompose la couleur
r = Val("&H" & Mid(sColor, 5, 2))
V = Val("&H" & Mid(sColor, 7, 2))
b = Val("&H" & Mid(sColor, 9, 2))

getColor = RGB(r, V, b)

End Function

Public Function GetFileName(MyFile As String) As String

Dim i As Integer

i = InStrRev(MyFile, "\")
If i = 0 Then
    GetFileName = MyFile
Else
    GetFileName = Mid$(MyFile, i + 1, Len(MyFile) - i)
End If


End Function

