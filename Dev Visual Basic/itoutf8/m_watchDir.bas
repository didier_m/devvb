Attribute VB_Name = "m_watchDir"

Private Type FILE_NOTIFY_INFORMATION
  NextEntryOffset As Long
  Action As Long
  FileNameLength As Long
  FileName(1 To 255) As Byte
End Type

Private Const FILE_NOTIFY_CHANGE_FILE_NAME = &H1&
Private Const FILE_NOTIFY_CHANGE_DIR_NAME = &H2&
Private Const FILE_NOTIFY_CHANGE_ATTRIBUTES = &H4&
Private Const FILE_NOTIFY_CHANGE_SIZE = &H8&
Private Const FILE_NOTIFY_CHANGE_LAST_WRITE = &H10&
Private Const FILE_NOTIFY_CHANGE_LAST_ACCESS = &H20&
Private Const FILE_NOTIFY_CHANGE_CREATION = &H40&
Private Const FILE_NOTIFY_CHANGE_SECURITY = &H100&

Private Const FILE_ACTION_ADDED = &H1&
Private Const FILE_ACTION_REMOVED = &H2&
Private Const FILE_ACTION_MODIFIED = &H3&
Private Const FILE_ACTION_RENAMED_OLD_NAME = &H4&
Private Const FILE_ACTION_RENAMED_NEW_NAME = &H5&

Private Const MAX_DIRS = 10 '//Only handle maximum upto 10 dir watch
Private Const MAX_BUFFER = 4096
Private Const MAX_PATH = 256 '//Your code will not work if Less than 256 (Yeppppp)
Private Type OVERLAPPED
  Internal As Long
  InternalHigh As Long
  offset As Long
  OffsetHigh As Long
  hEvent As Long
End Type

'// this is the all purpose structure that contains
'// the interesting directory information and provides
'// the input buffer that is filled with file change data

Private Type DIRECTORY_INFO
  hDir As Long
  lpszDirName As String * MAX_PATH
  lpBuffer(MAX_BUFFER) As Byte
  dwBufLength As Long
  oOverLapped As OVERLAPPED
  lComplKey As Long
End Type

Private Type SECURITY_ATTRIBUTES
  nLength As Long
  lpSecurityDescriptor As Long
  bInheritHandle As Long
End Type

Private Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" ( _
    ByVal lpFileName As String, _
    ByVal dwDesiredAccess As Long, _
    ByVal dwShareMode As Long, _
    lpSecurityAttributes As SECURITY_ATTRIBUTES, _
    ByVal dwCreationDisposition As Long, _
    ByVal dwFlagsAndAttributes As Long, _
    ByVal hTemplateFile As Long) As Long

Private Declare Function ReadDirectoryChangesW Lib "kernel32" ( _
    ByVal hDirectory As Long, _
    lpBuffer As Any, _
    ByVal nBufferLength As Long, _
    ByVal bWatchSubtree As Long, _
    ByVal dwNotifyFilter As Long, _
    lpBytesReturned As Long, _
    lpOverlapped As Any, _
    lpCompletionRoutine As Any) As Long

Private Declare Function CreateIoCompletionPort Lib "kernel32" ( _
    ByVal FileHandle As Long, _
    ByVal ExistingCompletionPort As Long, _
    ByVal CompletionKey As Long, _
    ByVal NumberOfConcurrentThreads As Long) As Long

Private Declare Function CloseHandle Lib "kernel32" ( _
    ByVal hObject As Long) As Long

Private Declare Function PostQueuedCompletionStatus Lib "kernel32" ( _
    ByVal CompletionPort As Long, _
    lpNumberOfBytesTransferred As Long, _
    lpCompletionKey As Long, _
    lpOverlapped As Long) As Long

Private Declare Function GetQueuedCompletionStatus Lib "kernel32" ( _
    ByVal CompletionPort As Long, _
    lpNumberOfBytesTransferred As Long, _
    lpCompletionKey As Long, _
    lpOverlapped As OVERLAPPED, _
    ByVal dwMilliseconds As Long) As Long

Private Const FILE_LIST_DIRECTORY = &H1
Private Const FILE_SHARE_READ = &H1
Private Const FILE_SHARE_DELETE = &H4
Private Const FILE_SHARE_WRITE = &H2

Private Const FILE_FLAG_BACKUP_SEMANTICS = &H2000000
Private Const FILE_FLAG_OVERLAPPED = &H40000000
Private Const OPEN_EXISTING = 3

Private Const INVALID_HANDLE_VALUE = -1
Private Const INFINITE = &HFFFF

Private hDir As Long
Private lpSecurityAttributes As SECURITY_ATTRIBUTES

'Private hEvent As Long
Private hIOCompPort As Long
Private oOverLapped As OVERLAPPED
Private InfoBuffer(MAX_BUFFER) As Byte
Private lpBytesReturned As Long

Private Type FileChange
  NextPos As Long
  Name As String
  Type As Long
End Type

Private hThread As Long, lngTid As Long
Private DirInfo(MAX_DIRS) As DIRECTORY_INFO  '// Buffer for all of the directories
Private numDirs As Integer
Private ret As Long, lngFilter As Long, bWatchSubDir As Boolean

Public Function WatchDirectoryOrFilename(dirPath As String) As Boolean

  bWatchSubDir = recursive
  '//Create Notification filter
  lngFilter = FILE_NOTIFY_CHANGE_FILE_NAME _
      Or FILE_NOTIFY_CHANGE_SIZE _
      Or FILE_NOTIFY_CHANGE_ATTRIBUTES _
      Or FILE_NOTIFY_CHANGE_DIR_NAME _
      Or FILE_NOTIFY_CHANGE_FILE_NAME _
      Or FILE_NOTIFY_CHANGE_LAST_ACCESS _
      Or FILE_NOTIFY_CHANGE_LAST_WRITE _
      Or FILE_NOTIFY_CHANGE_SECURITY
  lngFilter = FILE_NOTIFY_CHANGE_LAST_WRITE
  lpSecurityAttributes.nLength = Len(lpSecurityAttributes)

    '//[Step 1] Obtain each dir handle which you want to monitor
    DirInfo(1).hDir = CreateFile(dirPath, _
        FILE_LIST_DIRECTORY, _
        FILE_SHARE_READ Or FILE_SHARE_WRITE Or FILE_SHARE_DELETE, _
        lpSecurityAttributes, _
        OPEN_EXISTING, _
        FILE_FLAG_BACKUP_SEMANTICS Or FILE_FLAG_OVERLAPPED, _
        0)

    DirInfo(1).lpszDirName = dirPath

    '//[Step 2] Set up a key(directory info) for directory

    '//Note: Use hIOCompPort as an exisiting port so CreateIoCompletionPort dont create new port for each dir

    DirInfo(1).lComplKey = VarPtr(DirInfo(1))  '//Get a completion Key for each directory
    '//Here we have used mem loc for each dirinfo record
    hIOCompPort = CreateIoCompletionPort(DirInfo(1).hDir, hIOCompPort, DirInfo(1).lComplKey, 0)

    '// Start watching each of the directories of interest

    ret = ReadDirectoryChangesW(DirInfo(1).hDir, _
        DirInfo(1).lpBuffer(0), _
        MAX_BUFFER, _
        bWatchSubDir, _
        lngFilter, _
        lpBytesReturned, _
        DirInfo(1).oOverLapped, _
        ByVal 0&)

    If ret <> 0 Then WatchDirectoryOrFilename = True
End Function

Public Function HandleDirectoryChanges() As String
  On Error Resume Next

  Dim fContinue As Boolean
  Dim lPointer As Long, iPos As Integer
  Dim lngCompKey As Long
  Dim lpOverlapped As OVERLAPPED
  Dim fni As FILE_NOTIFY_INFORMATION
  Dim DI As DIRECTORY_INFO
  Dim FileInfo As FileChange

  '// Retrieve the directory info for this directory
  '// through the completion key
  ret = GetQueuedCompletionStatus(hIOCompPort, _
      lpBytesReturned, _
      lngCompKey, _
      oOverLapped, _
      100)       '//Put INFINITE if synchronous

  If (ret <> 0) Then
    lPointer = 0

    '//Findout which directory is this based on completionkey
    
    iPos = 1
    
    Do
      FileInfo = GetFileName(DirInfo(iPos).lpBuffer, lPointer)

      strPath = Trim(DirInfo(iPos).lpszDirName) & "\" & FileInfo.Name

      Select Case FileInfo.Type
        Case FILE_ACTION_ADDED
          strMsg = "[" & strPath & "] was added to the directory."
        Case FILE_ACTION_REMOVED
          strMsg = "[" & strPath & "] was removed from the directory."
        Case FILE_ACTION_MODIFIED
          strMsg = "[" & strPath & "] was modified. (i.e. timestamp/attributes chage)."
        Case FILE_ACTION_RENAMED_OLD_NAME
          strMsg = "[" & strPath & "] was renamed and this is the old name."
        Case FILE_ACTION_RENAMED_NEW_NAME
          strMsg = "[" & strPath & "] was renamed and this is the new name."
      End Select
      
      'List2.AddItem strMsg
      
      lPointer = FileInfo.NextPos
    Loop While (lPointer > 0)
        
    ret = ReadDirectoryChangesW(DirInfo(iPos).hDir, _
        DirInfo(iPos).lpBuffer(0), _
        MAX_BUFFER, _
        bWatchSubDir, _
        lngFilter, _
        lpBytesReturned, _
        DirInfo(iPos).oOverLapped, _
        ByVal 0&)
  End If
  HandleDirectoryChanges = strPath
End Function

Private Function GetLongFromBuf(Buf() As Byte, lPos As Long) As Long
  On Error Resume Next

  Dim lTemp As Long
  Dim lResult As Long
  lResult = Buf(lPos)
  lTemp = Buf(lPos + 1): lResult = lResult + lTemp * 256
  lTemp = Buf(lPos + 2): lResult = lResult + lTemp * 65536
  lTemp = Buf(lPos + 3): lResult = lResult + lTemp * 65536 * 256
  GetLongFromBuf = lResult
End Function

Private Function GetFileName(Buf() As Byte, lPointer As Long) As FileChange
  On Error Resume Next

  Dim lNextOffset As Long
  Dim lFileSize As Long
  Dim sTemp As String
  Dim bTemp() As Byte
  Dim i As Long

  lNextOffset = GetLongFromBuf(Buf, lPointer)
  GetFileName.Type = GetLongFromBuf(Buf, lPointer + 4)
  lFileSize = GetLongFromBuf(Buf, lPointer + 8)

  If lFileSize = 0 Then
    GetFileName.NextPos = 0
    Exit Function
  End If
  ReDim bTemp(1 To lFileSize)

  For i = 1 To lFileSize
    bTemp(i) = Buf(lPointer + 11 + i)
  Next i

  GetFileName.Name = bTemp

  If lNextOffset = 0 Then
    GetFileName.NextPos = 0
  Else
    GetFileName.NextPos = lNextOffset + lPointer
  End If
End Function

