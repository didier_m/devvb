VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ChangedFileInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Enum fcnFILE_ATTRIBUTES
    FILE_ATTRIBUTE_READONLY = &H1
    FILE_ATTRIBUTE_HIDDEN = &H2
    FILE_ATTRIBUTE_SYSTEM = &H4
    FILE_ATTRIBUTE_DIRECTORY = &H10
    FILE_ATTRIBUTE_ARCHIVE = &H20
    FILE_ATTRIBUTE_NORMAL = &H80
    FILE_ATTRIBUTE_TEMPORARY = &H100
    FILE_ATTRIBUTE_COMPRESSED = &H800
End Enum

Private mstrName As String
Private mstrPath As String
Private mlngAttributes As Long
Private mvarTimeCreated As Variant
Private mvarTimeLastAccessed As Variant
Private mvarTimeLastWritten As Variant
Private mlngSize As Long

Public Property Get AttributesString() As String
Attribute AttributesString.VB_Description = "Returns the file's system attributes as a concatenated string"
Dim strOut As String
    If mlngAttributes And FILE_ATTRIBUTE_READONLY Then
        strOut = strOut & "R"
    End If
    If mlngAttributes And FILE_ATTRIBUTE_HIDDEN Then
        strOut = strOut & "H"
    End If
    If mlngAttributes And FILE_ATTRIBUTE_SYSTEM Then
        strOut = strOut & "S"
    End If
    If mlngAttributes And FILE_ATTRIBUTE_DIRECTORY Then
        strOut = strOut & "D"
    End If
    If mlngAttributes And FILE_ATTRIBUTE_ARCHIVE Then
        strOut = strOut & "A"
    End If
    If mlngAttributes And FILE_ATTRIBUTE_NORMAL Then
        strOut = strOut & "N"
    End If
    If mlngAttributes And FILE_ATTRIBUTE_TEMPORARY Then
        strOut = strOut & "T"
    End If
    If mlngAttributes And FILE_ATTRIBUTE_COMPRESSED Then
        strOut = strOut & "C"
    End If
    AttributesString = strOut
End Property

Public Property Get Name() As String
Attribute Name.VB_Description = "Returns the name of the specified file"
    Name = mstrName
End Property

Friend Property Let Name(Value As String)
    mstrName = Value
End Property

Public Property Get Path() As String
Attribute Path.VB_Description = "Returns the path (including a trailing backslash) of the specified file"
    Path = mstrPath
End Property

Friend Property Let Path(Value As String)
    mstrPath = Value
End Property

Public Property Get Attributes() As Long
Attribute Attributes.VB_Description = "Returns the file's system attributes as a long integer"
    Attributes = mlngAttributes
End Property

Friend Property Let Attributes(Flags As Long)
    mlngAttributes = Flags
End Property

Public Property Get TimeCreated() As Variant
Attribute TimeCreated.VB_Description = "Returns the time when the file was created"
    TimeCreated = mvarTimeCreated
End Property

Friend Property Let TimeCreated(Value As Variant)
    mvarTimeCreated = Value
End Property

Public Property Get TimeLastAccessed() As Variant
Attribute TimeLastAccessed.VB_Description = "Returns the time when the file was last accessed"
    TimeLastAccessed = mvarTimeLastAccessed
End Property

Friend Property Let TimeLastAccessed(Value As Variant)
    mvarTimeLastAccessed = Value
End Property

Public Property Get TimeLastWritten() As Variant
Attribute TimeLastWritten.VB_Description = "Returns the time when the file was last written to"
    TimeLastWritten = mvarTimeLastWritten
End Property

Friend Property Let TimeLastWritten(Value As Variant)
    mvarTimeLastWritten = Value
End Property

Public Property Get Size() As Long
Attribute Size.VB_Description = "Returns the size of the file in bytes"
    Size = mlngSize
End Property

Friend Property Let Size(Value As Long)
    mlngSize = Value
End Property


