VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FileChangeNotification"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' The various notification flags
Public Enum fcnNOTIFY_FLAGS
    FILE_NOTIFY_CHANGE_FILE_NAME = &H1
    FILE_NOTIFY_CHANGE_DIR_NAME = &H2
    FILE_NOTIFY_CHANGE_ATTRIBUTES = &H4
    FILE_NOTIFY_CHANGE_SIZE = &H8
    FILE_NOTIFY_CHANGE_LAST_WRITE = &H10
    FILE_NOTIFY_CHANGE_SECURITY = &H100
End Enum

'   Max allowable length of a folder/filename
Private Const MAX_PATH = 260

'   The FILETIME data structure is a 64-bit value representing the number
'   of 100-nanosecond intervals since January 1, 1601. It is the
'   means by which Win32 determines the date and time.
Private Type FILETIME
   dwLowDateTime As Long
   dwHighDateTime As Long
End Type

'  represents a date and time using individual members for the month,
'  day, year, weekday, hour, minute, second, and millisecond.
Private Type SYSTEMTIME
  wYear As Integer
  wMonth As Integer
  wDayOfWeek As Integer
  wDay As Integer
  wHour As Integer
  wMinute As Integer
  wSecond As Integer
  wMilliseconds As Integer
End Type

'   structure describes a file found by the FindFirstFile, FindFirstFileEx,
'   or FindNextFile function
Private Type WIN32_FIND_DATA
    dwFileAttributes As Long
    ftCreationTime As FILETIME
    ftLastAccessTime As FILETIME
    ftLastWriteTime As FILETIME
    nFileSizeHigh As Long
    nFileSizeLow As Long
    dwReserved0 As Long
    dwReserved1 As Long
    cFileName As String * MAX_PATH
    cAlternate As String * 14
End Type

Private Type BY_HANDLE_FILE_INFORMATION
    dwFileAttributes As Long
    ftCreationTime As FILETIME
    ftLastAccessTime As FILETIME
    ftLastWriteTime As FILETIME
    dwVolumeSerialNumber As Long
    nFileSizeHigh As Long
    nFileSizeLow As Long
    nNumberOfLinks As Long
    nFileIndexHigh As Long
    nFileIndexLow As Long
End Type

Private Type TIME_ZONE_INFORMATION
  Bias As Long
  StandardName(0 To 63) As Byte
  StandardDate As SYSTEMTIME
  StandardBias As Long
  DaylightName(0 To 63) As Byte
  DaylightDate As SYSTEMTIME
  DaylightBias As Long
End Type

Private Declare Function apiFindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
Private Declare Function apiFindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long
Private Declare Function apiFindClose Lib "kernel32" Alias "FindClose" (ByVal hFindFile As Long) As Long
Private Declare Function apiGetDateFormat Lib "kernel32" Alias "GetDateFormatA" (ByVal Locale As Long, ByVal dwFlags As Long, lpDate As SYSTEMTIME, ByVal lpFormat As Long, ByVal lpDateStr As String, ByVal cchDate As Long) As Long
Private Declare Function apiCreateFile Lib "kernel32" Alias "CreateFileA" (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, ByVal lpSecurityAttributes As Long, ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) As Long
Private Declare Sub sapiCopyMem Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
Private Declare Function apiGetTimeZoneInformation Lib "kernel32" Alias "GetTimeZoneInformation" (lpTimeZoneInformation As TIME_ZONE_INFORMATION) As Long
Private Declare Function apiFileTimeToSystemTime Lib "kernel32" Alias "FileTimeToSystemTime" (lpFileTime As FILETIME, lpSystemTime As SYSTEMTIME) As Long
Private Declare Function apiFindFirstChangeNotification Lib "kernel32" Alias "FindFirstChangeNotificationA" (ByVal lpPathName As String, ByVal bWatchSubtree As Long, ByVal dwNotifyFilter As Long) As Long
Private Declare Function apiFindNextChangeNotification Lib "kernel32" Alias "FindNextChangeNotification" (ByVal hChangeHandle As Long) As Long
Private Declare Function apiFindCloseChangeNotification Lib "kernel32" Alias "FindCloseChangeNotification" (ByVal hChangeHandle As Long) As Long
Private Declare Function apiWaitForSingleObject Lib "kernel32" Alias "WaitForSingleObject" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Private Declare Function apiWaitForMultipleObjects Lib "kernel32" Alias "WaitForMultipleObjects" (ByVal nCount As Long, lpHandles As Long, ByVal bWaitAll As Long, ByVal dwMilliseconds As Long) As Long
Private Declare Function apiGetTimeFormat Lib "kernel32" Alias "GetTimeFormatA" (ByVal Locale As Long, ByVal dwFlags As Long, lpTime As SYSTEMTIME, ByVal lpFormat As Long, ByVal lpTimeStr As String, ByVal cchTime As Long) As Long
Private Declare Function apiGetFileInformationByHandle Lib "kernel32" Alias "GetFileInformationByHandle" (ByVal hFile As Long, lpFileInformation As BY_HANDLE_FILE_INFORMATION) As Long
Private Declare Function apiCloseHandle Lib "kernel32" Alias "CloseHandle" (ByVal hObject As Long) As Long
Private Declare Function apiGetFileAttributes Lib "kernel32" Alias "GetFileAttributesA" (ByVal lpFileName As String) As Long
Private Declare Function apiFormatMsg Lib "kernel32" Alias "FormatMessageA" (ByVal dwFlags As Long, ByVal lpSource As Long, ByVal dwMessageId As Long, ByVal dwLanguageId As Long, ByVal lpBuffer As String, ByVal nSize As Long, Arguments As Long) As Long

Private Const FORMAT_MESSAGE_FROM_SYSTEM As Long = &H1000

'  do not use user overrides
Private Const LOCALE_NOUSEROVERRIDE = &H80000000
'  use the system ACP
Private Const LOCALE_USE_CP_ACP = &H40000000

' Time Flags for GetTimeFormatW.
'  do not use minutes or seconds
Private Const TIME_NOMINUTESORSECONDS = &H1
'  do not use seconds
Private Const TIME_NOSECONDS = &H2
'  do not use time marker
Private Const TIME_NOTIMEMARKER = &H4
'  always use 24 hour format
Private Const TIME_FORCE24HOURFORMAT = &H8

'  Date Flags for GetDateFormat.
'  use short date picture
Private Const DATE_SHORTDATE = &H1
'  use long date picture
Private Const DATE_LONGDATE = &H2
'  use alternate calendar (if any)
Private Const DATE_USE_ALT_CALENDAR = &H4

'#if(WINVER >= 0x0500)
'  use year month picture
Private Const DATE_YEARMONTH = &H8
'  add marks for left to right reading order layout
Private Const DATE_LTRREADING = &H10
'  add marks for right to left reading order layout
Private Const DATE_RTLREADING = &H20
'#endif /* WINVER >= 0x0500 */

Private Const LOCALE_SYSTEM_DEFAULT = &H800
Private Const LOCALE_USER_DEFAULT = &H400

Private Const TIME_ZONE_ID_INVALID = &HFFFFFFFF
Private Const TIME_ZONE_ID_UNKNOWN = 0
Private Const TIME_ZONE_ID_STANDARD = 1
Private Const TIME_ZONE_ID_DAYLIGHT = 2

Private Const GENERIC_READ = &H80000000
Private Const GENERIC_WRITE = &H40000000
Private Const OPEN_EXISTING = 3

Private Const FILE_SHARE_READ = &H1
Private Const FILE_SHARE_WRITE = &H2
Private Const MAXDWORD = &HFFFFFFFF

Private Const FILE_ATTRIBUTE_DIRECTORY = &H10
Private Const INVALID_HANDLE_VALUE = -1&
Private Const WAIT_OBJECT_0 = &H0
Private Const WAIT_ABANDONED = &H80
Private Const WAIT_TIMEOUT = &H102
Private Const WAIT_FAILED = &HFFFFFFFF

Private Const INFINITE = -1&      '  Infinite timeout

Private Const ERROR_NO_MORE_FILES = 18&
Private Const ERR_BASE = vbObjectError + 9999

Private Type FILE_INFO
    strFileName As String
    lngSize As Long
    lngAttributes As Long
    varCreationTime As Variant
    varModifiedTime As Variant
    varAccessedTime As Variant
End Type

Private Type SUBFOLDER_INFO
    strName As String
    intFileCount As Integer
    atFileInfo() As FILE_INFO
End Type

Private Type FOLDER_INFO
    lngChangeHandle As Long
    strPath As String
    lngFlags As Long
    blnIncludeSubFolders As Boolean
    atSubFolders() As SUBFOLDER_INFO
    intFileCount As Integer
    atFileInfo() As FILE_INFO
End Type

Private matFolderInfo() As FOLDER_INFO
Private maFiles() As FILE_INFO
Private malngChangeHandles() As Long

Private hSearchFile As Long
Private lpDataFolder As WIN32_FIND_DATA
Private lpDataFile As WIN32_FIND_DATA
Private Const vbKeyDot = 46
Private Const vbKeySlash = 92

Private mintCount As Integer
Private mlngFileCount As Long
Private mintWatchCount As Integer
Private mblnIsWatchOn As Boolean

Public Sub StartWatch()
Attribute StartWatch.VB_Description = "Gathers a list of all files in specified folders and starts the watch"
' Initializes and enters watch state
'
Dim i As Integer
Dim j As Integer
Dim lngRet As Long
    ' Get a count of all the folders we have to watch
    mintWatchCount = UBound(matFolderInfo)
    For i = 0 To mintWatchCount
        With matFolderInfo(i)
            ' Try to initialize a watch on each folder
            lngRet = apiFindFirstChangeNotification(.strPath, CLng(.blnIncludeSubFolders), .lngFlags)
            If lngRet = INVALID_HANDLE_VALUE Then
                ' something failed, cleanup and get out of here
                For j = 0 To i
                    If matFolderInfo(j).lngChangeHandle Then
                        Call apiFindCloseChangeNotification(matFolderInfo(j).lngChangeHandle)
                    End If
                Next
                Err.Raise ERR_BASE + 2, "FileChangeNotify.Start", fAPIErr(Err.LastDllError)
            Else
                ' Notification is set, store the handle in an array
                ' and move on to the next folder
                .lngChangeHandle = lngRet
                malngChangeHandles(i) = lngRet
            End If
        End With
    Next
    ' Set the internal flag so that we know the code is in a wait state
    mblnIsWatchOn = True
    ' All notifications are set, start waiting for notification updates
    Call sWaitLoop
End Sub

Private Sub sWaitLoop()
' Waits for notifications from all folders
' until the internal flag is set to false
'
Dim lngWaitStatus As Long
Dim i As Integer
Dim lngRet As Long
Dim clsCFIOld As ChangedFileInfo
Dim clsCFICurrent As ChangedFileInfo
Dim blnNotified As Boolean
On Error GoTo ErrHandler

Const ERR_WAITFAIL = ERR_BASE + 5

    ' Repeat the loop until IsRunning is set to false
    Do While mblnIsWatchOn
       blnNotified = False
        ' Wait for notification. mintWatchCount is zero based
        ' default wait is 100 miliseconds
        lngWaitStatus = apiWaitForMultipleObjects( _
                                    CLng(mintWatchCount + 1), _
                                    malngChangeHandles(0), _
                                    0, _
                                    100)
 
        Select Case lngWaitStatus
            Case WAIT_TIMEOUT
                ' re-enter Wait state, nothing to do
                DoEvents
            Case WAIT_OBJECT_0 To mintWatchCount + WAIT_OBJECT_0
                ' Find the files and raise event
                Set clsCFICurrent = fGetChangedFileInfo(matFolderInfo(lngWaitStatus))
                
                ' This step is necessary because FindNextChangeNotification/WaitForMultipleObjects
                ' right after a successful notification will immediately return successfully
                ' without "flushing" the last notification, so we will get two notifications per change
                If Not clsCFICurrent Is Nothing Then
                    ' Compare the data stored in the two class instances and only
                    ' raise the ChangeOccurred event is the data doesn't match
                    If Not fIsSameData(clsCFICurrent, clsCFIOld) Then
                        ChangeOccured (clsCFICurrent)
                        ' set an internal flag to true so that
                        ' if FindNextChangeNotification returns successfully without
                        ' really waiting for a change, we know not to refresh the
                        ' internal arrays with the list of files
                        blnNotified = True
                    End If
                End If
                ' setup notification again
                lngRet = apiFindNextChangeNotification(matFolderInfo( _
                                        lngWaitStatus).lngChangeHandle)
                If lngRet = 0 Then
                    ' If notification watch failed then error out
                    With Err
                        .Raise ERR_WAITFAIL, "FileChangeNotification.Wait", fAPIErr(.LastDllError)
                    End With
                Else
                    If blnNotified Then
                        ' If we've previously raised an event in response to the change
                        ' notification, that is the object data for the two instances is different
                        Set clsCFIOld = clsCFICurrent
                        'Set clsCFICurrent = Nothing
                        ' rebuild the files list from all the watched folders
                        With matFolderInfo(lngWaitStatus)
                            ReDim .atFileInfo(0)
                            ' Get all the files for this folder again
                            Call sGetFiles(.strPath, CInt(lngWaitStatus), 1, 0)
                            ' If there are any subfolders to watch
                            If .blnIncludeSubFolders Then
                                ReDim .atSubFolders(0)
                                mintCount = 0
                                ' Get the files for the subfolders as well
                                Call sGetFolders(.strPath, CInt(lngWaitStatus))
                            End If
                            ' There's going to be one extra element in the array
                            ' for all dynamic sub-arrays, so trim them now
                            Call sTrimArrays
                        End With
                    End If
                End If
            Case WAIT_FAILED
                ' The wait failed for some reason, so error out
                With Err
                    .Raise ERR_WAITFAIL, "FileChangeNotification.Wait", fAPIErr(.LastDllError)
                End With
            Case Else
                ' Unknown. Shouldn't happen, but if it does, then error out
                With Err
                    .Raise ERR_WAITFAIL, "FileChangeNotification.Wait", _
                        "Wait failed with a return status code of " & lngWaitStatus & "."
                End With
        End Select
    Loop
    
ExitHere:
    Set clsCFIOld = Nothing
    Set clsCFICurrent = Nothing
    Exit Sub
ErrHandler:
    mblnIsWatchOn = False
    'With Err
    '    .Raise .Number, .Source, .Description
    'End With
    Resume ExitHere
End Sub

Private Function fIsSameData(clsNew As ChangedFileInfo, clsOld As ChangedFileInfo) As Boolean
' Compare the two copies of the class instance
' KISS!!
    With clsNew
        If Not clsOld Is Nothing And Not clsNew Is Nothing Then
            fIsSameData = (StrComp(.Name, clsOld.Name, vbTextCompare) = 0) And _
                                (StrComp(.Path, clsOld.Path, vbTextCompare) = 0) And _
                                (StrComp(.TimeCreated, clsOld.TimeCreated, vbTextCompare) = 0) And _
                                (StrComp(.TimeLastAccessed, clsOld.TimeLastAccessed, vbTextCompare) = 0) And _
                                (StrComp(.TimeLastWritten, clsOld.TimeLastWritten, vbTextCompare) = 0) And _
                                (.Size = clsOld.Size)
        End If
    End With
End Function

Private Function fGetChangedFileInfo(fi As FOLDER_INFO) As ChangedFileInfo
' Locates the file whose attributes have changed since the last notification or
' from when the notification was set up
'
Dim strPath As String
Dim blnFound As Boolean
Dim blnChangeDetect As Boolean
Dim intUBound As Integer
Dim cfi As ChangedFileInfo
Dim atFilesSrc() As FILE_INFO
On Error GoTo ErrHandler

    strPath = fi.strPath
    ' If there are any files in this folder
    If fi.intFileCount > 0 Then
        ' get the names into a local array
        atFilesSrc = fi.atFileInfo
    End If
    If fi.blnIncludeSubFolders Then
        ' if any subfolders are included, then
        ' get their count (vbDirectory)
        intUBound = UBound(fi.atSubFolders)
    End If
    ' Keep iterating until there are no more folders left
    Do While Len(strPath) > 0
        ' Get the files for this folder
        ' the 3 in the proc call just tells the proc which
        ' array to redim Preserve and fill
        Call sGetFiles(strPath, 0, 3, 0)
        
        ' Compare the before and after arrays
        With fCompareArrays(atFilesSrc, maFiles)
            ' If the function suceeded, that is there's a
            ' valid filename in the struct
            If Len(.strFileName) > 0 Then
                ' We've found our file for which
                ' the API issued a notification
                ' Instantiate the class, fill it and exit
                Set cfi = New ChangedFileInfo
                cfi.Path = strPath
                cfi.Name = .strFileName
                cfi.Size = .lngSize
                cfi.TimeCreated = .varCreationTime
                cfi.TimeLastAccessed = .varAccessedTime
                cfi.TimeLastWritten = .varModifiedTime
                cfi.Attributes = .lngAttributes
                Exit Do
            End If
        End With
        
        ' If there are any subfolders included then
        If fi.blnIncludeSubFolders Then
            With fi.atSubFolders(intUBound)
                ' get the folderpath into the local var
                strPath = .strName & "\"
                If .intFileCount > 0 Then
                    atFilesSrc = .atFileInfo
                End If
            End With
            ' This code will loop backwards over the array
            intUBound = intUBound - 1
        Else
            strPath = vbNullString
        End If
    Loop
    
    Set fGetChangedFileInfo = cfi
    
ExitHere:
    Set cfi = Nothing
    Exit Function
ErrHandler:
    Resume ExitHere
End Function

Private Function fCompareArrays(atSrc() As FILE_INFO, atTarget() As FILE_INFO) As FILE_INFO
' Compares two arrays and finds the new/changed element
On Error Resume Next    'Arrays may not be initialized
Dim i As Integer
Dim j As Integer
Dim strFile As String
Dim fiNew As FILE_INFO
Dim blnFound As Boolean
Dim blnChanged As Boolean

    ' This logic won't work for deletes (Yet) since
    ' we are starting off with the target array
    For i = 0 To UBound(atTarget)
        strFile = atTarget(i).strFileName
        blnFound = False
        blnChanged = False
        ' For each element in the target array
        ' go over the entire source array to find a match
        For j = 0 To UBound(atSrc)
            If StrComp(atSrc(j).strFileName, strFile, vbTextCompare) = 0 Then
            ' if the file names match, then compare the attributes to see if they match
                blnFound = True
                If (StrComp(atTarget(j).varAccessedTime, atSrc(i).varAccessedTime, vbTextCompare) <> 0) Or _
                    (StrComp(atTarget(j).varCreationTime, atSrc(i).varCreationTime, vbTextCompare) <> 0) Or _
                    (StrComp(atTarget(j).varModifiedTime, atSrc(i).varModifiedTime, vbTextCompare) <> 0) Or _
                    (atTarget(j).lngAttributes <> atSrc(i).lngAttributes) Or _
                    (atTarget(j).lngSize <> atSrc(i).lngSize) Then
                        ' change detected
                        blnChanged = True
                        LSet fiNew = atTarget(i)
                        Exit For
                End If
            End If
            If blnFound Then Exit For
        Next
        If blnChanged Then Exit For
        If Not blnFound Then
            ' new file
            LSet fiNew = atTarget(i)
            blnFound = True
            Exit For
        End If
    Next
    LSet fCompareArrays = fiNew
End Function

Public Property Get IsRunning() As Boolean
Attribute IsRunning.VB_Description = "Indicates whether the object is currently watching any folders"
' True if a watch is currently active
    IsRunning = mblnIsWatchOn
End Property

Public Property Let IsRunning(Value As Boolean)
' Terminates the watch
    mblnIsWatchOn = Value
    Call Me.EndWatch
End Property

Public Sub EndWatch()
Attribute EndWatch.VB_Description = "Terminates all current watches"
' Terminates the watch
Dim i As Integer
    mblnIsWatchOn = False
    For i = 0 To mintWatchCount
        Call apiFindCloseChangeNotification(matFolderInfo(i).lngChangeHandle)
    Next
End Sub


Public Sub AddWatchFolder(ByVal FolderName As String, _
                                        WatchFlags As fcnNOTIFY_FLAGS, _
                                        Optional IncludeSubFolders As Boolean)
Attribute AddWatchFolder.VB_Description = "Adds a folder to list of folders to be watched"
' Adds a folder to the list of watched folders
'
    If Len(Dir(FolderName, vbDirectory)) = 0 Then
        ' if the foldername is invalid, error out
        Err.Raise ERR_BASE + 1, _
                    "FileChangeNotify.WatchFolder", _
                    "The system cannot find the path specified."
    Else
        ' if there's no trailing backslash, then append one
        If Not Asc(Right$(FolderName, 1)) = vbKeySlash Then
            FolderName = FolderName & Chr$(vbKeySlash)
        End If
        ' INitialize the internal arrays
        ReDim Preserve matFolderInfo(mintWatchCount)
        ReDim Preserve malngChangeHandles(mintWatchCount)
        With matFolderInfo(mintWatchCount)
            ' store the folder information
            .strPath = FolderName
            ' get the files
            ReDim .atFileInfo(0)
            Call sGetFiles(.strPath, mintWatchCount, 1, 0)
            
            .lngFlags = WatchFlags
            .blnIncludeSubFolders = IncludeSubFolders
            ' if there are subfolders to be included, get their names
            ' as well (sGetFolders is a recursive proc)
            If .blnIncludeSubFolders Then
                ReDim .atSubFolders(0)
                mintCount = 0
                Call sGetFolders(.strPath, mintWatchCount)
            End If
        End With
        mintWatchCount = mintWatchCount + 1
        ' Trim off the trailing elements from the array
        Call sTrimArrays
    End If
End Sub

Private Sub sTrimArrays()
' Trism the trailing elements
' and erases empty arrays
'
Dim i As Integer, j As Integer
Dim intTmp1 As Integer
Dim intTmp2 As Integer
    
    intTmp1 = UBound(matFolderInfo)
    For i = 0 To intTmp1
        If matFolderInfo(i).intFileCount = 0 Then
            Erase matFolderInfo(i).atFileInfo
        End If
        If matFolderInfo(i).blnIncludeSubFolders Then
            intTmp2 = UBound(matFolderInfo(i).atSubFolders)
            For j = 0 To intTmp2
                If matFolderInfo(i).atSubFolders(j).intFileCount = 0 Then
                    Erase matFolderInfo(i).atSubFolders(j).atFileInfo
                End If
            Next
        End If
    Next
End Sub

Private Sub sGetFiles(strPath As String, intArrayIndex As Integer, intTargetArray As Integer, intSubArrayIndex As Integer)
' Fills an appropriate array with all the filenames from the specified folder
'
Static sblnFilesPresent As Boolean
Static sstrTemp As String
Static slngRet As Long

    '   Initialize the static variables
    sblnFilesPresent = False
    sstrTemp = vbNullString
    slngRet = 0
    mlngFileCount = 0

    '   obtain a search handle
    hSearchFile = apiFindFirstFile(strPath & "*.*", lpDataFile)
    If Not hSearchFile = INVALID_HANDLE_VALUE Then
        '   if a valid handle was returned, go through all the files
        '   till there are no more (indicated by blnFilespresent)
        Do While Not sblnFilesPresent
            '   only look at files
            sstrTemp = fStripNull(lpDataFile.cFileName)
            If Not (Asc(sstrTemp) = vbKeyDot) Then
                slngRet = apiGetFileAttributes(strPath & sstrTemp)
                If (slngRet And FILE_ATTRIBUTE_DIRECTORY) = 0 Or _
                    (Err.LastDllError = 32 And slngRet = -1) Then   ' Check for error code = 32 (in use by another process)
                    Select Case intTargetArray
                        ' We are looking at a file from a subfolder
                        Case 1
                            ReDim Preserve matFolderInfo(intArrayIndex).atFileInfo(mlngFileCount)
                            With matFolderInfo(intArrayIndex).atFileInfo(mlngFileCount)
                                .strFileName = sstrTemp
                                .varCreationTime = fGetFileTime(VarPtr(lpDataFile), 1, 1)
                                .varAccessedTime = fGetFileTime(VarPtr(lpDataFile), 2, 1)
                                .varModifiedTime = fGetFileTime(VarPtr(lpDataFile), 3, 1)
                                .lngSize = ((lpDataFile.nFileSizeHigh * (MAXDWORD + 1)) + lpDataFile.nFileSizeLow)
                                .lngAttributes = lpDataFile.dwFileAttributes
                            End With
                            With matFolderInfo(intArrayIndex)
                                .intFileCount = .intFileCount + 1
                            End With
                            mlngFileCount = mlngFileCount + 1
                        Case 2
                            ' We are looking at a file from an included subfolder
                            ReDim Preserve matFolderInfo(intArrayIndex).atSubFolders(intSubArrayIndex).atFileInfo(mlngFileCount)
                            With matFolderInfo(intArrayIndex).atSubFolders(intSubArrayIndex).atFileInfo(mlngFileCount)
                                .strFileName = sstrTemp
                                .varCreationTime = fGetFileTime(VarPtr(lpDataFile), 1, 1)
                                .varAccessedTime = fGetFileTime(VarPtr(lpDataFile), 2, 1)
                                .varModifiedTime = fGetFileTime(VarPtr(lpDataFile), 3, 1)
                                .lngSize = ((lpDataFile.nFileSizeHigh * (MAXDWORD + 1)) + lpDataFile.nFileSizeLow)
                                .lngAttributes = lpDataFile.dwFileAttributes
                            End With
                            With matFolderInfo(intArrayIndex).atSubFolders(intSubArrayIndex)
                                .intFileCount = .intFileCount + 1
                            End With
                            mlngFileCount = mlngFileCount + 1
                        Case 3
                            ' We are filling an array to compare with old data in response to a notification
                            ReDim Preserve maFiles(mlngFileCount)
                            With maFiles(mlngFileCount)
                                .strFileName = sstrTemp
                                .varCreationTime = fGetFileTime(VarPtr(lpDataFile), 1, 1)
                                .varAccessedTime = fGetFileTime(VarPtr(lpDataFile), 2, 1)
                                .varModifiedTime = fGetFileTime(VarPtr(lpDataFile), 3, 1)
                                .lngSize = ((lpDataFile.nFileSizeHigh * (MAXDWORD + 1)) + lpDataFile.nFileSizeLow)
                                .lngAttributes = lpDataFile.dwFileAttributes
                            End With
                            mlngFileCount = mlngFileCount + 1
                    End Select
                        'End If
                End If
            End If
            If apiFindNextFile(hSearchFile, lpDataFile) = 0 _
                    And Err.LastDllError = ERROR_NO_MORE_FILES Then
                sblnFilesPresent = True
            End If
        Loop
        Call apiFindClose(hSearchFile)
        hSearchFile = 0
    End If
End Sub

Private Sub sGetFolders(strPath As String, intArrayIndex As Integer)
'   Recursive proc!!!
'   retrieves a list of all folders under the path strPath
'   and then call sEnumFiles to enumerate all finds and try to locate strFindFile
'
'   These dims cannot be static as they must
'   be reallocated when the proc calls itself
Dim hSearch As Long
Dim hFile As Long
Dim blnFoldersPresent As Boolean
Dim strFile As String
Dim strFullPath As String
Dim i As Integer, intFolderCount As Integer
Dim lpFileInfo As BY_HANDLE_FILE_INFORMATION
Dim lngRet As Long
Dim astrFolders() As String

    intFolderCount = 0
    ReDim astrFolders(intFolderCount)
    
    '   Notify clients of our progress
    'RaiseEvent ScanStatus(strPath)
    '  DoEvents is necessary to let the client do
    '  some processing
    DoEvents
    '   get a seach handle
    hSearch = apiFindFirstFile(strPath & "*", lpDataFolder)
    If Not hSearch = INVALID_HANDLE_VALUE Then
        '   if a valid handle was returned, go through all the folders
        '   till there are no more (indicated by blnFoldersPresent)
        Do While Not blnFoldersPresent
            strFile = fStripNull(lpDataFolder.cFileName)
            '   build the complete path to the file
            strFullPath = strPath & strFile
            '   we're only interested in Directories
            If apiGetFileAttributes(strFullPath) And _
                                FILE_ATTRIBUTE_DIRECTORY Then
                '   ignore the "." (current) and ".." (parent)" entries
                '   in a folder.  Using Asc gives slightly better performance
                If Not (Asc(strFile) = vbKeyDot) Then
                    ReDim Preserve astrFolders(intFolderCount)
                    ReDim Preserve matFolderInfo(intArrayIndex).atSubFolders(mintCount)
                    astrFolders(intFolderCount) = strFile & "\"
                    matFolderInfo(intArrayIndex).atSubFolders(mintCount).strName = strFullPath
                    ' Enum the files
                    ReDim matFolderInfo(intArrayIndex).atSubFolders(mintCount).atFileInfo(0)
                    Call sGetFiles(strFullPath & "\", intArrayIndex, 2, mintCount)
                    intFolderCount = intFolderCount + 1
                    mintCount = mintCount + 1
                End If
            End If
                
            If apiFindNextFile(hSearch, lpDataFolder) = 0 _
                        And Err.LastDllError = ERROR_NO_MORE_FILES Then
                '   If no more folders are present, prepare to quit the loop
                blnFoldersPresent = True
            End If
        Loop
        Call apiFindClose(hSearch)
        
        If intFolderCount > 0 Then
            For i = 0 To intFolderCount - 1
                Call sGetFolders(strPath & astrFolders(i), intArrayIndex)
            Next
        End If
    End If
End Sub

Private Function fGetFileTime(lpFileData As Long, intTimeType As Integer, intCallType As Integer) As Variant
'   Returns a particular FileTime associated with a file.
'strFileName:    Full path to the file
'intTimeType:    = 1 to retrieve Creation Time
'                      =  2 To retrieve Last Access Time
'                      = 3 to retrieve LastWrite Time
'  intCallType=1 => Use Win32_Find_Data
'  intCallType=2 => Use BY_HANDLE_FILE_INFORMATION
'
On Error GoTo ErrHandler
Dim lngRet As Long
Dim lpSystem As SYSTEMTIME
Dim lpFileWFD As WIN32_FIND_DATA
Dim lpFileBHFI As BY_HANDLE_FILE_INFORMATION
Dim strTime As String, strDate As String
Dim lngLen As Long
Const conERR_GENERIC = vbObjectError + 10000

    '   fill the correct UDT based on which proc is calling this function
    Select Case intCallType
        Case 1:
            Call sapiCopyMem(lpFileWFD, ByVal lpFileData, Len(lpFileWFD))
        Case 2:
            Call sapiCopyMem(lpFileBHFI, ByVal lpFileData, Len(lpFileBHFI))
        Case Else:
            Err.Raise conERR_GENERIC
    End Select
 
    '   Retrieve the various time values
    Select Case intTimeType
        Case 1:    'Creation Time
            If intCallType = 1 Then
                lngRet = apiFileTimeToSystemTime(lpFileWFD.ftCreationTime, lpSystem)
            Else
                lngRet = apiFileTimeToSystemTime(lpFileBHFI.ftCreationTime, lpSystem)
            End If
        Case 2:    'LastAccess Time
            If intCallType = 1 Then
                lngRet = apiFileTimeToSystemTime(lpFileWFD.ftLastAccessTime, lpSystem)
            Else
                lngRet = apiFileTimeToSystemTime(lpFileBHFI.ftLastAccessTime, lpSystem)
            End If
        Case 3:    'LastWrite time
            If intCallType = 1 Then
                lngRet = apiFileTimeToSystemTime(lpFileWFD.ftLastWriteTime, lpSystem)
            Else
                lngRet = apiFileTimeToSystemTime(lpFileBHFI.ftLastWriteTime, lpSystem)
            End If
        Case Else:  'invalid value
            
    End Select
 
    '  format the time according the system settings
    strTime = Space$(255)
    lngLen = apiGetTimeFormat(LOCALE_SYSTEM_DEFAULT, _
                                    LOCALE_NOUSEROVERRIDE, _
                                    lpSystem, _
                                    0, _
                                    strTime, _
                                    Len(strTime) - 1)
    strTime = Left$(strTime, lngLen - 1)
 
    '  format the date according to the system settings
    strDate = Space$(255)
    lngLen = apiGetDateFormat(LOCALE_SYSTEM_DEFAULT, _
                                    LOCALE_NOUSEROVERRIDE, _
                                    lpSystem, _
                                    0, _
                                    strDate, _
                                    Len(strDate) - 1)
    strDate = Left$(strDate, lngLen - 1)
 
    fGetFileTime = strDate & Chr$(vbKeySpace) & strTime
 
    '  UTC = local time + bias
    fGetFileTime = CStr(DateAdd("n", -fGetTimeBias(), CDate(fGetFileTime)))
ExitHere:
  Exit Function
ErrHandler:
  fGetFileTime = Null
  Resume ExitHere
End Function

Private Function fGetTimeBias() As Long
'  Returns Local-UTC Bias in minutes
Dim lpTZI As TIME_ZONE_INFORMATION
Dim lngRet As Long, lngOut As Long
   lngRet = apiGetTimeZoneInformation(lpTZI)
    Select Case lngRet
        Case TIME_ZONE_ID_INVALID:
            'Error
            lngOut = 0
        Case TIME_ZONE_ID_UNKNOWN:
            'cannot determine
            lngOut = 0
        Case TIME_ZONE_ID_STANDARD:
            lngOut = lpTZI.Bias + lpTZI.StandardBias
        Case TIME_ZONE_ID_DAYLIGHT:
            lngOut = lpTZI.Bias + lpTZI.DaylightBias
    End Select
    fGetTimeBias = lngOut
End Function

Private Function fStripNull(str As String) As String
'   Returns the string after truncating it at first vbNullChar
Static intPos As Integer
    intPos = 0
    intPos = InStr(str, vbNullChar)
    If intPos > 0 Then
        fStripNull = Left$(str, intPos - 1)
    End If
End Function

Private Function fAPIErr(ByVal lngErr As Long) As String
'   Returns the description associated with Err.LastDLLError
'
Dim strMsg As String
Dim lngRet As Long
    strMsg = String$(1024, 0)
    lngRet = apiFormatMsg(FORMAT_MESSAGE_FROM_SYSTEM, 0, _
                           lngErr, 0, strMsg, Len(strMsg), ByVal 0)
    If lngRet Then
        fAPIErr = Left$(strMsg, lngRet)
    End If
End Function

Private Sub Class_Terminate()
    Erase matFolderInfo
     Erase maFiles
    Erase malngChangeHandles

End Sub

Private Sub ChangeOccured(ByVal clsCFICurrent As ChangedFileInfo)
    MsgBox clsCFICurrent.Name
    
End Sub
