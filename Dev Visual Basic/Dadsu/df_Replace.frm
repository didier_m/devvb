VERSION 5.00
Object = "{DAFD85F1-343B-47EE-8170-1C26723F7A5D}#2.0#0"; "dogskin.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form df_Replace 
   BackColor       =   &H00CBB49A&
   BorderStyle     =   0  'None
   Caption         =   "Saisie des selections"
   ClientHeight    =   2880
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5955
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   2880
   ScaleWidth      =   5955
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   1920
      TabIndex        =   3
      Top             =   1380
      Width           =   3615
   End
   Begin DogSkin.DealCmdButton Picture1 
      Height          =   615
      Left            =   4560
      TabIndex        =   0
      Tag             =   "DICTAB�BUTTON-OK"
      Top             =   2100
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   1
      ButtonForm      =   1
      BackColor       =   16579059
   End
   Begin DogSkin.DogSkinObject DogSkinObject1 
      Left            =   0
      Top             =   0
      _ExtentX        =   979
      _ExtentY        =   953
      BorderColor     =   13077835
      BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderForeColor =   10053171
      HeaderBackColor =   16637889
      HeaderLineColor =   15847345
      HeaderUseLine   =   -1  'True
      BeginProperty BodyFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BodyForeColor   =   10053171
      BodyBackColor   =   16579059
      HeaderIco       =   "df_Replace.frx":0000
      HeaderIcoNa     =   "df_Replace.frx":0682
      ShadowColor     =   15847345
   End
   Begin DogSkin.DealCmdButton Picture3 
      Height          =   615
      Left            =   5160
      TabIndex        =   1
      Tag             =   "DICTAB�BUTTON-CANCEL"
      Top             =   2100
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   2
      ButtonForm      =   1
      BackColor       =   16579059
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4080
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   21
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":0D04
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":19DE
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":36B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":4392
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":506C
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":5D46
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":6A20
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":76FA
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":7FD4
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":9CAE
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":A588
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":B262
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":BB3C
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":C416
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":D0F0
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":DDCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":E6A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":1037E
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":12058
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":1784A
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Replace.frx":19524
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageCombo Combo1 
      Height          =   330
      Left            =   360
      TabIndex        =   2
      Tag             =   "DICT�VALIDE"
      Top             =   840
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   582
      _Version        =   393216
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nouvelle valeur"
      Height          =   255
      Left            =   360
      TabIndex        =   4
      Top             =   1440
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00C78D4B&
      Height          =   1335
      Left            =   120
      Top             =   600
      Width           =   5655
   End
End
Attribute VB_Name = "df_Replace"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Combo1_KeyUp(KeyCode As Integer, Shift As Integer)
ComboSaisieAuto Combo1, False
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
    Case 113 'F2 validation
    
    Case 115, 27 'F4 ou esc annulation bon
        Unload Me
End Select

End Sub

Private Sub Form_Load()

'---> Init du skin
Me.DogSkinObject1.Initialisation False, "Supprimer des enregistrements", pCloseOnly, False
Me.DogSkinObject1.SetMatriceEffect "SHAPE1"
'-> on charge la couleur de fond desbouttons
Me.Picture1.BackColor = Me.DogSkinObject1.BodyBackColor
Me.Picture3.BackColor = Me.DogSkinObject1.BodyBackColor

End Sub

Public Sub init(strKeyWord As String)
'--> cette procedure va charger la combo avec la liste des champs dispo
' a partir du noeud ou on est situ�

'--> cette proc�dure permet de supprimer si on a le droit tout un noeud d'enregistrement
Dim Niveau As Variant
Dim strTemp As Variant
Dim afield As dc_field
Dim tempKeyWord As String
Dim i As Integer

If strKeyWord = "" Then Exit Sub

On Error GoTo GestError
'-> on effectue les controles de suppression

'-> on se met la cle dans un vecteur pour pouvoir travailler dessus
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split(strKeyWord, "|")
For i = 1 To 6
    Niveau(i) = CSng(strTemp(i))
Next

'-> on cree la chaine de base du noeud
For i = 1 To Fields(strKeyWord).Niveau
    tempKeyWord = tempKeyWord & "|" & CSng(strTemp(i))
Next

tempKeyWord = "�" & tempKeyWord & "|"

'-> on va mettre le statut a delete de tous les enreg de ce noeud et des noeud inferieurs
For Each afield In Fields
    '-> on verifie que l'enreg appartient bien � notre noeud
    If InStr(1, "�" & afield.KeyWord, tempKeyWord) <> 0 Then
        '-> si c'est pas deja fait on rajoute la ligne de rubrique dans la combo
        On Error Resume Next
        If Me.Combo1.ComboItems(afield.Struct) Is Nothing Then
            Me.Combo1.ComboItems.Add , afield.Struct, afield.Struct & " - " & Structures(afield.Struct).FieldDescription
        End If
        On Error GoTo GestError
    End If
Next

df_Replace.Show vbModal

GestError:

End Sub

Private Sub Picture1_Click()
'--> cette proc�dure permet de supprimer toutes les rubriques a partir d'un noeud
Dim Niveau As Variant
Dim strTemp As Variant
Dim afield As dc_field
Dim tempKeyWord As String
Dim strKeyWord As String
Dim i As Integer
Dim j As Integer

strKeyWord = df_Dadsu.TreeView1.SelectedItem.Key
If strKeyWord = "" Then Exit Sub

'-> Demander confirmation sur la suppression des enregistrement
strRetour = DisplayMessage("Remplacer d�finitivement tous les valeurs d'une rubrique � partir de la selection (" & df_Dadsu.TreeView1.SelectedItem.Text & ")?", dsmWarning, dsmYesNo, "Confirmation de suppression")
If strRetour = "NON" Then Exit Sub

On Error GoTo GestError
'-> on effectue les controles de suppression

'-> on se met la cle dans un vecteur pour pouvoir travailler dessus
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split(strKeyWord, "|")
For i = 1 To 6
    Niveau(i) = CSng(strTemp(i))
Next

'-> on cree la chaine de base du noeud
For i = 1 To Fields(strKeyWord).Niveau
    tempKeyWord = tempKeyWord & "|" & CSng(strTemp(i))
Next

tempKeyWord = "�" & tempKeyWord & "|"

'-> on va mettre le statut a delete de tous les enreg de ce noeud et des noeud inferieurs
For Each afield In Fields
    '-> on verifie que l'enreg appartient bien � notre noeud
    If InStr(1, "�" & afield.KeyWord, tempKeyWord) <> 0 And afield.Struct = Me.Combo1.SelectedItem.Key Then
        '-> on verifie les donn�es
        If Not CtrlFormatZone(Me.Text1.Text, afield.Struct) Then
            Exit Sub
        End If
        '-> on enregistre la saisie en effectuant les controles de premier niveau
        If EnregSave(afield.KeyWord, Me.Text1.Text) Then
        Else
            Exit Sub
        End If
        j = j + 1
    End If
Next

'-> on supprime l'enregistrement de l'ecran au niveau du treeview et du dealgrid
LoadDealGrid df_Dadsu.TreeView1.SelectedItem.Key

'-> on remet a jour les compteurs
CtrlFieldsCompteur


DisplayMessage "Remplacement de " & j & " enregistrement(s).", dsmQuestion, dsmOkOnly, "Remplacement de valeurs"

Exit Sub
GestError:
DisplayMessage "Remplacement non autoris�e", dsmQuestion, dsmOkOnly, "Remplacement d'enregistrements"

End Sub

Private Sub Picture3_Click()

'-> on quitte sans rien faire
Unload Me

End Sub

Public Sub ComboSaisieAuto(aCombo As ImageCombo, SaisieLibre As Boolean)
'--> cette proc�dure permet la saisie automatique dans une combo
Dim i As Long
Dim sel As Long
Dim aItem As ComboItem

'-> on parcours les elements de la combo
For Each aItem In aCombo.ComboItems
    sel = Len(aCombo.Text)
    '-> si on trouve le texte on l'affiche
    If (StrComp(Left$(aItem.Text, sel), aCombo.Text, vbTextCompare) = 0) And aCombo.Text <> "" Then
        aItem.Selected = True
        aCombo.SelStart = sel
        If Len(aCombo.Text) <> sel Then
            aCombo.SelLength = Len(aCombo.Text) - sel
        Else
            aCombo.SelStart = 0
            aCombo.SelLength = Len(aCombo.Text)
        End If
        GoTo Suite
    End If
Next
'-> on a rien trouv�
If Not SaisieLibre Then
    aCombo.Text = ""
End If
Suite:

End Sub

