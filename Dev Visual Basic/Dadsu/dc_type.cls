VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "dc_type"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'**************************************************************************
'* Description de la structure des donn�es qui sont lues dans le fichier  *
'**************************************************************************

'---> Classe de description des differents type d'enregistrement

Public KeyWord As String 'Code du champ
Public Value As String 'Valeur du champ
Public curChrono As Single  'Valeur du chrono en cours
Public Niveau As Integer 'D�finit la profondeur dans le treeview

Public Function IsType(strKey As String) As Boolean
'-> cette fonction permet de savoir si un �l�ment appartient � la structure
Dim aType As dc_type

On Error GoTo GestError
Set aType = Types.Item(strKey)
IsType = True
Set aType = Nothing
Exit Function

GestError:
IsType = False
End Function

