VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "dc_enreg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'***********************************************************
'* Description de la Enreg de l'affichage des donn�es  *
'***********************************************************

'-> D�finition des propri�t�s
Public Fields As Collection
Public Keyword As String
Public Libel As String

Public Function IsEnreg(strKey As String) As Boolean
'-> cette fonction permet de savoir si un �l�ment appartient � la Enreg
Dim aEnreg As dc_enreg

On Error GoTo GestError
Set aEnreg = Enregs.Item(strKey)
IsEnreg = True
Set aEnreg = Nothing
Exit Function

GestError:
IsEnreg = False
End Function
