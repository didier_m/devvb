VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "dc_file"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'***********************************************************
'* Description de la File de l'affichage des donn�es  *
'***********************************************************

'-> D�finition des propri�t�s
Public nbFile As Integer '-> D�finit le nombre de structures completes contenues
Public Keyword As String
Public Enregs As Collection

Public Function IsFile(strKey As String) As Boolean
'-> cette fonction permet de savoir si un �l�ment appartient � la File
Dim aFile As dc_file

On Error GoTo GestError
Set aFile = Files.Item(strKey)
IsFile = True
Set aFile = Nothing
Exit Function

GestError:
IsFile = False
End Function

