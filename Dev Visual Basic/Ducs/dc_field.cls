VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "dc_field"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'**************************************************************************
'* Description de la Enreg des donn�es qui sont lues dans le fichier  *
'**************************************************************************

Public Keyword As String
Public Position As Integer  'Position sur la ligne
Public Longueur As Integer
Public OldValue As String
Public Libel As String
Public Value As String
Public Modify As Boolean
Public sType As pType
Public sStatut As pStatut
Public NomCoplat As String
Public Help As String

Public Function IsField(strKey As String) As Boolean
'-> cette fonction permet de savoir si un �l�ment appartient � la Enreg
Dim aField As dc_field

On Error GoTo GestError
Set aField = Fields.Item(strKey)
IsField = True
Set aField = Nothing
Exit Function

GestError:
IsField = False
End Function

Public Function NextField(strKey As String) As dc_field

Dim aField As dc_field
Dim TopNext As Boolean

For Each aField In Fields
    If aField.Keyword = strKey Then
        TopNext = True
    End If
    If TopNext = True Then
        Set NextField = aField
        Exit For
    End If
Next

End Function

Public Function GetStatut(sStatut As pStatut) As String
Select Case sStatut
    Case pObligatoire
        GetStatut = "Obl."
    Case pFacultatif
        GetStatut = "Fac."
    Case pDep
        GetStatut = "Dep."
    Case pNonUtilis�
        GetStatut = "Non.uti."
    Case pstt
        GetStatut = "Stt."
End Select

End Function


