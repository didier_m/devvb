Attribute VB_Name = "dm_ducs"
Option Explicit

Public Enum pStatutEnreg
    pModify
    pVisu
    pCreate
    pSaisie
    pSave
End Enum

Public FileName As String
Public StrError As String
Public Files As Collection
Public Enregs As Collection
Public Types As Collection
Public cTypes As Collection
Public Structures As Collection
Public Fields As Collection
Public aEnreg As dc_enreg
Public aType As dc_type
Dim aField As dc_field
Dim aFile As Collection
Public curStatut As pStatut
Public FileDucs As String
Public StrCopyKey As String
Public appTurbo As String
Public lastMaquette As String
Public DucsExportini

Public Declare Function PostMessage Lib "user32.dll" Alias "PostMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long

Private hdlFile As Integer
Private TempFileName As String
Private Ligne As String

Private Const vbBackslash = "\"
Private Const ALL_FILES = "*.*"

Private Type FILETIME
   dwLowDateTime As Long
   dwHighDateTime As Long
End Type

Private Type WIN32_FIND_DATA
   dwFileAttributes As Long
   ftCreationTime As FILETIME
   ftLastAccessTime As FILETIME
   ftLastWriteTime As FILETIME
   nFileSizeHigh As Long
   nFileSizeLow As Long
   dwReserved0 As Long
   dwReserved1 As Long
   cFileName As String * 260
   cAlternate As String * 14
End Type

Private Type FILE_PARAMS
   bRecurse As Boolean
   bFindOrExclude As Long
   nCount As Long
   nSearched As Long
   sFileNameExt As String
   sFileRoot As String
End Type

Private Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
   
Private Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
   
Private Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long

Private Declare Function lstrlen Lib "kernel32" Alias "lstrlenW" (ByVal lpString As Long) As Long

Private Declare Function PathMatchSpec Lib "shlwapi" Alias "PathMatchSpecW" (ByVal pszFileParam As Long, ByVal pszSpec As Long) As Long

Private fp As FILE_PARAMS
Private stopSearch As Boolean

Sub Main()

'-> on initialise le message d'erreur
StrError = "Une erreur est survenue lors du chargement"
'-> on ouvre la feuille de chargement du Ducs
df_Ducs.init

End Sub

Public Function LoadDucs(aFilePath As String) As Boolean
Dim hdlFic As Integer
Dim Ligne As String
Dim aField As dc_field
Dim aEnreg As dc_enreg
Dim aStructure As dc_structure
Dim aFile As New dc_file
Dim Structures As Collection
Dim fileLength As Single 'pour le timer
Dim linelength As Single 'pour le timer
Dim tempTimer As Integer 'pour le timer
Dim i As Integer
Dim topFin As Boolean

'On Error GoTo GestError:

fileLength = FileLen(aFilePath)

'-> on affiche le nom du fichier pour info
df_Ducs.Label1.Caption = aFilePath

'-> Ouvrir le fichier binaire source
hdlFic = FreeFile
Open aFilePath For Input As #hdlFic
Line Input #hdlFic, Ligne

'-> on teste la premiere chaine pour v�rifier que l'on est bien sur un fichier Ducs
If Mid(Ligne, 1, 3) <> "DEB" Then GoTo GestError
    
'-> on recharge le fichier
Close #hdlFic
Open aFilePath For Input As #hdlFic
    
'-> on charge maintenant le fichier de description des enregistrements
If Not LoadDucsStructure Then GoTo GestError

'-> on affiche le timer
df_Drloading.Show

'on initialise les champs
Set Fields = New Collection
Set Enregs = New Collection
Set Files = New Collection

'-> on se charge les enregistrements du fichier � l'aide de la Structure que l'on vient de creer
'-> chaque ligne correspondant � un enregistrement
Do
    '-> Lecture de la ligne
    Line Input #hdlFic, Ligne
    If Ligne = "" Then GoTo Suite
    '-> ce doevents sert a rafraichir le timer
    tempTimer = tempTimer + 1
    linelength = linelength + Len(Ligne) + 2
    If tempTimer = 1000 Then
        tempTimer = 0
        DoEvents
        df_Drloading.Label1.Caption = "Chargement en cours..." & CInt(linelength / fileLength * 100) & "%"
    End If
    '-> on regarde si on est sur un nouveau fichier
    If topFin And Mid(Ligne, 1, 3) = "DEB" Then
        Files.Add Enregs, "FILE|" & Files.Count + 1
        '-> on redefini la collection
        Set Enregs = New Collection
    End If
    If Trim(Ligne) = "" Then GoTo Suite
    Set aEnreg = New dc_enreg
    Set aEnreg.Fields = New Collection
    '-> on pointe sur la structure de l'enregistrement
    Set Structures = Types(Mid(Ligne, 1, 3))
    '-> on boucle sur les diff�rents champs de l'enregistrement
    For Each aStructure In Structures
        '-> on declare un nouveau champ
        Set aField = New dc_field
        '-> on charge les propri�t�s du champ
        aField.Value = Mid(Ligne, aStructure.Position, aStructure.Longueur)
        aField.OldValue = aField.Value
        aField.Longueur = aStructure.Longueur
        aField.Position = aStructure.Position
        aField.Libel = aStructure.Libel
        aField.Keyword = "FIELD|" & aEnreg.Fields.Count + 1      'aStructure.Keyword
        aField.sType = aStructure.sType
        aField.sStatut = aStructure.StatutUR
        aField.NomCoplat = aStructure.NomCoplat
        aField.Help = aStructure.URSSAF
        aField.Modify = False
        aEnreg.Fields.Add aField, "FIELD|" & aEnreg.Fields.Count + 1
        aEnreg.Keyword = "ENREG|" & Enregs.Count + 1
        aEnreg.Libel = aStructure.enreg
        If aEnreg.Libel <> "DEB" Then topFin = True
    Next
    Enregs.Add aEnreg, "ENREG|" & Enregs.Count + 1
Suite:
Loop While Not EOF(hdlFic)

'-> on ajoute la derniere collection d enreregistrement a la collection des fichiers
Files.Add Enregs, "FILE|" & Files.Count + 1

'-> on affiche le treeview en pointant sur la base
df_Ducs.TreeView1.Nodes.Add , , "FILE", "Fichier Ducs", 2

df_Drloading.Label1.Caption = "Chargement de l'ecran..."
DoEvents
Call LoadTreeview("")

'-> on charge les entetes du dealgrid
df_Ducs.DealGrid1.SetCellText 0, 0, "Description"
df_Ducs.DealGrid1.SetCellText 1, 0, "Position"
df_Ducs.DealGrid1.SetCellText 2, 0, "Taille"
df_Ducs.DealGrid1.SetCellText 3, 0, "Type"
df_Ducs.DealGrid1.SetCellText 4, 0, "Statut"
df_Ducs.DealGrid1.SetCellText 5, 0, "Edifact"
df_Ducs.DealGrid1.SetCellText 6, 0, "Valeur"

'-> par defaut on est en visu
curStatut = pVisu

'-> V�rifier que l'on trouve le fichier de Enreg
DucsExportini = searchExe("DucsExport.ini")

'-> on masque le timer
Unload df_Drloading

'-> on ferme le fichier
Close #hdlFic

'-> on affiche la feuille
df_Ducs.Show

Exit Function
GestError:

DisplayMessage StrError, dsmCritical, dsmOkOnly, "Erreur sur l'application"
'-> on ferme le fichier
Close #hdlFic
End
End Function

Public Sub LoadTreeview(strKeyword As String)
 '--> cette proc�dure permet d'afficher le treeview
Dim aNode As Node
Dim curKeyWord As String
Dim tempKeyword As String
Dim curNiveau As Integer
Dim TopLibel As Integer 'sert de compteur pour les libell�s dans le treeview
Dim iNiveau As Integer
Dim strTemp As Variant
Dim strLibel As String
Dim i As Integer
Dim j As Integer
Dim intImage As Integer

'On Error Resume Next

'-> on parcours la collection files
For i = 1 To Files.Count
    Set aFile = Files.Item(i)
    '-> on pointe sur le premier noeud
    Set aEnreg = Nothing
    If aFile.Count > 0 Then Set aNode = df_Ducs.TreeView1.Nodes.Add("FILE", 4, "FILE|" & i, "D�claration : " & i, GetImage("FILE"))
    '-> par defaut on est au niveau 0
    curNiveau = 0
    '-> on parcourt la collection des enregistrements
    For j = 1 To aFile.Count
        Set aEnreg = aFile(j)
        intImage = GetImage(aEnreg.Keyword)
        '-> on ajoute selon le niveau
        Select Case cTypes(aEnreg.Libel).Niveau
            Case Is > curNiveau
                Set aNode = df_Ducs.TreeView1.Nodes.Add(aNode.Index, 4, "ENREG|" & i & "|" & j, aEnreg.Libel & GetLabel(aEnreg.Libel), GetImage("ENREG"))
            Case Is = curNiveau
                Set aNode = df_Ducs.TreeView1.Nodes.Add(aNode.Index, 2, "ENREG|" & i & "|" & j, aEnreg.Libel & GetLabel(aEnreg.Libel), GetImage("ENREG"))
            Case Is < curNiveau
                Select Case curNiveau - cTypes(aEnreg.Libel).Niveau
                    Case 1
                        Set aNode = df_Ducs.TreeView1.Nodes.Add(aNode.Parent.Index, 2, "ENREG|" & i & "|" & j, aEnreg.Libel & GetLabel(aEnreg.Libel), GetImage("ENREG"))
                    Case 2
                        Set aNode = df_Ducs.TreeView1.Nodes.Add(aNode.Parent.Parent.Index, 2, "ENREG|" & i & "|" & j, aEnreg.Libel & GetLabel(aEnreg.Libel), GetImage("ENREG"))
                    Case 3
                        Set aNode = df_Ducs.TreeView1.Nodes.Add(aNode.Parent.Parent.Parent.Index, 2, "ENREG|" & i & "|" & j, aEnreg.Libel & GetLabel(aEnreg.Libel), GetImage("ENREG"))
                End Select
        End Select
        '-> on sauvegarde le niveau
        curNiveau = cTypes(aEnreg.Libel).Niveau
    Next
Next

End Sub

Private Function GetLabel(strKey As String) As String
'-> cette fonction permet de ramener le libell� pour le treeview selon le niveau d'ou l'on vient
Dim strValue As String

If cTypes(1).IsType(strKey) Then
    GetLabel = cTypes(strKey).Libel
End If

End Function

Private Function isNode(strKey As String) As Boolean
'--> cette fonction indique si le node existe dans le treeview
Dim aNode As Node

On Error GoTo GestError
Set aNode = df_Ducs.TreeView1.Nodes(strKey)
Set aNode = Nothing
isNode = True
Exit Function

GestError:
isNode = False
End Function

Private Function Uncote(strValue As String) As String
'--> cette fonction va permettre de sortir les ''
Uncote = Mid(strValue, 2, Len(strValue) - 2)
End Function

Private Function LoadDucsStructure() As Boolean
'--> cette fonction permet de charger le fichier de Enreg en lisant un fichier de
'    param�trage contenant les informations : DucsStruct.ini
Dim curStructure As String
Dim curType As String
Dim aStructure As dc_structure
Dim aType As dc_type
Dim iValue As Integer

'On Error GoTo GestError

'-> on initialise les collections
Set Types = New Collection
Set Structures = New Collection
Set cTypes = New Collection

'-> V�rifier que l'on trouve le fichier de Enreg
If Dir$(searchExe("DucsStruct.ini")) = "" Then
    StrError = "Fichier DucsStruct.ini introuvable."
    Exit Function
End If

'-> Ouvrir le fichier ASCII de r�ponse et charger la matrice des lignes
hdlFile = FreeFile
Open searchExe("DucsStruct.ini") For Input As #hdlFile

'-> On parcours les lignes du fichier de param�trage
Do While Not EOF(hdlFile)
    '-> on initialise les objet
    Set aType = New dc_type
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> On ne garde que les lignes qui commencent par ';'
    If NumEntries(Ligne, ";") > 6 Then
        '-> On cr�� le type si il n'existe pas encore
        Ligne = Ligne & ";;;;;;;;;;;;"
        curType = Entry(1, Ligne, ";")
        If Not aType.IsType(curType) Then
            '-> Si on a termin� la structure complete d'un enreg on l'ajoute
            If Structures.Count <> 0 Then
                If Entry(1, aStructure.Keyword, "|") <> curType Then
                    Types.Add Structures, Structures(1).enreg
                    '-> on reinitialise la collection
                    iValue = 0
                    Set Structures = New Collection
                End If
            End If
            '-> on est sur un nouveau type
            Set aType = New dc_type
            aType.Keyword = Entry(1, Ligne, ";")
            If Not aType.IsType(aType.Keyword) Then cTypes.Add aType, aType.Keyword
        End If
        Set aStructure = New dc_structure
        '-> on est sur la definition de la structure d'un champ
        '-> on se charge toutes les donn�es en fonction de la descro
        aStructure.enreg = curType
        '-> on fait un controle de coh�rence
        If iValue <> 0 And Val(Entry(2, Ligne, ";")) <> iValue Then
            DisplayMessage "Incoh�rence de la structure pour " & aStructure.enreg & " � la position " & iValue, dsmWarning, dsmOkOnly, ""
        End If
        aStructure.Position = Entry(2, Ligne, ";")
        aStructure.Libel = Entry(3, Ligne, ";")
        Select Case Entry(4, Ligne, ";")
            Case "_N"
                aStructure.sType = pNumeric
            Case "AN"
                aStructure.sType = pString
            Case "N"
                aStructure.sType = pDate
            Case ""
                aStructure.sType = pstringUcase
        End Select
        aStructure.Longueur = Val(Entry(5, Ligne, ";"))
        iValue = aStructure.Position + aStructure.Longueur
        Select Case Entry(6, Ligne, ";")
            Case "Dep."
                aStructure.StatutUR = pDep
            Case "Fac."
                aStructure.StatutUR = pFacultatif
            Case "Obl."
                aStructure.StatutUR = pObligatoire
            Case "Stt."
                aStructure.StatutUR = pstt
            Case "N.ut."
                aStructure.StatutUR = pNonUtilis�
        End Select
        aStructure.URSSAF = Entry(7, Ligne, ";")
        Select Case Entry(8, Ligne, ";")
            Case "Dep."
                aStructure.StatutAS = pDep
            Case "Fac."
                aStructure.StatutAS = pFacultatif
            Case "Obl."
                aStructure.StatutAS = pObligatoire
            Case "Stt."
                aStructure.StatutAS = pstt
            Case "N.ut."
                aStructure.StatutAS = pNonUtilis�
        End Select
        aStructure.ASSEDIC = Entry(9, Ligne, ";")
        Select Case Entry(10, Ligne, ";")
            Case "Dep."
                aStructure.StatutAG = pDep
            Case "Fac."
                aStructure.StatutAG = pFacultatif
            Case "Obl."
                aStructure.StatutAG = pObligatoire
            Case "Stt."
                aStructure.StatutAG = pstt
            Case "N.ut."
                aStructure.StatutAG = pNonUtilis�
        End Select
        aStructure.AGIRC = Entry(11, Ligne, ";")
        Select Case Entry(12, Ligne, ";")
            Case "Dep."
                aStructure.StatutCC = pDep
            Case "Fac."
                aStructure.StatutCC = pFacultatif
            Case "Obl."
                aStructure.StatutCC = pObligatoire
            Case "Stt."
                aStructure.StatutCC = pstt
            Case "N.ut."
                aStructure.StatutCC = pNonUtilis�
        End Select
        aStructure.CCPBTP = Entry(13, Ligne, ";")
        aStructure.NomCoplat = Entry(14, Ligne, ";")
        aStructure.Keyword = aStructure.enreg & "|" & aStructure.Position
        Structures.Add aStructure, aStructure.Keyword
    Else
        If Mid(Ligne, 1, 1) = "[" Then
            cTypes(Mid(Ligne, 2, 3)).Libel = Entry(2, Ligne, ";")
            cTypes(Mid(Ligne, 2, 3)).Niveau = Val(Entry(3, Ligne, ";"))
            cTypes(Mid(Ligne, 2, 3)).Image = Val(Entry(4, Ligne, ";"))
        End If
    End If 'Ne pas traiter les lignes <> "" et de commentaire
Loop

'-> on ajoute la derniere structure
Types.Add Structures, Structures(1).enreg

Close #hdlFile
'-> valeur de succes
LoadDucsStructure = True
Exit Function
GestError:
StrError = "Erreur lors de la lecture du fichier de param�trage"
LoadDucsStructure = False

End Function

Public Sub LoadDealGrid(strKeyword As String)
'--> cette fonction va permettre de charger le dealgrid
Dim i As Integer
Dim aNode As Node
Dim aFile As Collection
Dim aEnreg As dc_enreg
Dim aField As dc_field
Dim posLigne As Integer
Dim curLigne As Integer
Dim tempKeyword As String

'-> ne pas remplir pour le premier noeud
If Entry(1, strKeyword, "|") <> "ENREG" Then Exit Sub

For Each aNode In df_Ducs.TreeView1.Nodes
    aNode.BackColor = vbWhite
Next
tempKeyword = strKeyword
strKeyword = "ENREG|" & Entry(2, strKeyword, "|") & "|" & Entry(3, strKeyword, "|")
'-> pour pointer eventuellement sur la ligne
tempKeyword = "FIELD|" & Entry(5, tempKeyword, "|")

Set aNode = df_Ducs.TreeView1.Nodes(strKeyword)
aNode.BackColor = vbYellow

'-> on vide le dealgrid
df_Ducs.DealGrid1.ClearRow (False)

'-> on pointe maintenant sur le bon enreg qui contient la collection des champ
Set aFile = Files("FILE|" & Entry(2, strKeyword, "|"))
Set aEnreg = aFile("ENREG|" & Entry(3, strKeyword, "|"))

posLigne = 1

'-> on charge le dealgrid en commencant par les informations de plus bas niveau
For Each aField In aEnreg.Fields
            '-> on rajoute la ligne
            df_Ducs.DealGrid1.AddRow True, posLigne
            '-> on ecrit les infos sur la ligne
            df_Ducs.DealGrid1.SetCellText 0, posLigne, aField.Libel
            df_Ducs.DealGrid1.SetColAlignement 1, 2
            df_Ducs.DealGrid1.SetCellText 1, posLigne, aField.Position
            df_Ducs.DealGrid1.SetColAlignement 2, 2
            df_Ducs.DealGrid1.SetCellText 2, posLigne, aField.Longueur
            df_Ducs.DealGrid1.SetColAlignement 3, 2
            Select Case aField.sType
                Case pNumeric
                    df_Ducs.DealGrid1.SetCellText 3, posLigne, "Num."
                Case pString
                    df_Ducs.DealGrid1.SetCellText 3, posLigne, "Alpha."
                Case pstringUcase
                    df_Ducs.DealGrid1.SetCellText 3, posLigne, "ALPHA."
                Case pDate
                    df_Ducs.DealGrid1.SetCellText 3, posLigne, "Date."
            End Select
            df_Ducs.DealGrid1.SetColAlignement 4, 2
            Select Case aField.sStatut
                Case pObligatoire
                    df_Ducs.DealGrid1.SetCellText 4, posLigne, "Obl."
                Case pFacultatif
                    df_Ducs.DealGrid1.SetCellText 4, posLigne, "Fac."
                Case pDep
                    df_Ducs.DealGrid1.SetCellText 4, posLigne, "Dep."
                Case pNonUtilis�
                    df_Ducs.DealGrid1.SetCellText 4, posLigne, "No.ut."
                Case pstt
                    df_Ducs.DealGrid1.SetCellText 4, posLigne, "Stt."
            End Select
            df_Ducs.DealGrid1.SetColAlignement 5, 2
            df_Ducs.DealGrid1.SetCellText 5, posLigne, aField.NomCoplat
            df_Ducs.DealGrid1.SetColAlignement 6, 2
            df_Ducs.DealGrid1.SetCellText 6, posLigne, aField.Value
            If aField.Keyword = tempKeyword Then curLigne = posLigne
            posLigne = posLigne + 1
Next

'-> on se positionne sur la ligne (l'astuce c'est de simuler une saisie pour la rendre visible)
df_Ducs.DealGrid1.SelectRow (curLigne)
df_Ducs.DealGrid1.InitSaisieCell True, FlexSaisieString, ""
df_Ducs.DealGrid1.SetSaisieCell 6, curLigne
SendKeys ("{ESC}")
DoEvents
End Sub

Public Function TextSearchGet() As String

If Files Is Nothing Then Exit Function

If Files.Count = 0 Then Exit Function

'-> on recupere la chaine a rechercher
DisplayMessage "Saisissez le texte � rechercher (code, libell�, ou valeur", dsmQuestion, dsmOkCancel, "Recherche de texte", True, InputValue & ""
TextSearchGet = InputValue

End Function

Public Function TextSearch(strText As String, Optional strKeyword As String) As Boolean
'--> cette procedure permet de rechercher du texte dans les donn�es
'    � partir d'une position (strKeyWord) et de se positionner dessus
'    pour cela on se positionne sur l'entr�e du noeud correspondante
Dim aField As dc_field

Dim aNode As Node
Dim topSearch As Boolean 'sert de marqueur pour savoir quand commencer a chercher
Dim i As Integer

On Error GoTo GestError

'-> si rien de specifier on recherche depuis le debut
If strKeyword = "" Then topSearch = True

'-> on parcourt les fichiers
For i = 1 To Files.Count
    Set aFile = Files("FILE|" & i)
    For Each aEnreg In aFile
        For Each aField In aEnreg.Fields
            '-> si on doit faire la recherche
            If topSearch Then
                If InStr(1, aField.Value, strText, vbTextCompare) Or InStr(1, aField.Libel, strText, vbTextCompare) Or InStr(1, aField.NomCoplat, strText, vbTextCompare) Then
                    '-> on a trouv� qque chose se positionner dessus a partir du dernier noeud
                    '-> on trouve le dernier noeud
                    Set aNode = df_Ducs.TreeView1.Nodes("ENREG|" & i & "|" & Entry(2, aEnreg.Keyword, "|"))
                    '-> on se positionne sur le treeview
                    df_Ducs.TreeView1.SelectedItem = aNode
                    aNode.EnsureVisible
                    LoadDealGrid ("ENREG|" & i & "|" & Entry(2, aEnreg.Keyword, "|") & "|" & aField.Keyword)
                    '-> quitter la boucle
                    GoTo Suite
                End If
           End If
           '-> on initialise le marqueur
           If aField.Keyword = strKeyword Then topSearch = True
        Next
    Next
Next
GestError:
'-> si on est ici c'est que l'on a rien trouv�
DisplayMessage "Fin du document atteinte", dsmInterro, dsmOkOnly, "Recherche de donn�es"

Suite:
End Function

Public Function CtrlFormatZone(ValueSaisie As String, aField As dc_field) As Boolean
'--> cette fonction teste la validit� de la saisie en fonction de la d�finition

'-> si la zone est facultative est vide c'est ok quitter
If (aField.sStatut = pFacultatif Or aField.sStatut = pDep) And ValueSaisie = "" Then
    CtrlFormatZone = True
    Exit Function
End If

ValueSaisie = Mid(ValueSaisie & Space(25), 1, aField.Longueur)

'-> selon le type de la donn�e
Select Case aField.sType
    Case pString, pstringUcase
        '-> on teste si la zone est obligatoire
        If aField.sStatut = pObligatoire And ValueSaisie = "" Then
            DisplayMessage "Cette zone doit �tre renseign�e.", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        '-> mettre en majuscule si le type le demande
        If aField.sType = pstringUcase Then ValueSaisie = UCase(ValueSaisie)
        '-> on test la longueur
        If Len(ValueSaisie) > aField.Longueur Then
            DisplayMessage "La longueur de saisie est limit�e � " & aField.Longueur & " caract�re(s).", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        '-> la longueur doit elle etre fixe
        If Not aField.Longueur And Len(ValueSaisie) <> aField.Longueur Then
            DisplayMessage "La longueur de saisie doit �tre de " & aField.Longueur & " caract�re(s).", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
    Case pDate
        '-> on teste si la zone est obligatoire
        If aField.sStatut = pObligatoire And ValueSaisie = "" Then
            DisplayMessage "Cette zone doit �tre renseign�e.", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        '-> la longueur doit elle etre fixe
        If Not aField.Longueur And Len(ValueSaisie) <> aField.Longueur Then
            DisplayMessage "La date doit �tre sous la forme 'jjmmaaaa'.", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        '-> on regarde si la zone saisie est bien une date
        If InStr(1, "|01|02|03|04|05|06|07|08|09|10|11|12|99", Mid(ValueSaisie, 3, 2)) = 0 Then
            DisplayMessage "Cette zone doit �tre une date valide.", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        If InStr(1, "|01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|99", Mid(ValueSaisie, 1, 2)) = 0 Then
            DisplayMessage "Cette zone doit �tre une date valide.", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        If Not (Val(Mid(ValueSaisie, 5, 4)) > 1900 And Val(Mid(ValueSaisie, 5, 4)) < 2100 And Val(Mid(ValueSaisie, 5, 4)) <> 9999) Then
            DisplayMessage "Cette zone doit �tre une date valide.", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        '-> un dernier test pour les fins de mois et ann�e bix..
        If InStr(1, ValueSaisie, "99") = 0 Then
            If Not IsDate(Mid(ValueSaisie, 1, 2) & "/" & Mid(ValueSaisie, 3, 2) & "/" & Mid(ValueSaisie, 5, 4)) Then
                DisplayMessage "Cette zone doit �tre une date valide.", dsmWarning, dsmOkOnly, "Erreur de saisie"
                GoTo GestError
            End If
        End If
    Case pNumeric
        '-> on teste si la zone est obligatoire
        If aField.sStatut = pObligatoire And ValueSaisie = "" Then
            DisplayMessage "Cette zone doit �tre renseign�e.", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        '-> mettre en majuscule si le type le demande
        If aField.sType = pstringUcase Then ValueSaisie = UCase(ValueSaisie)
        '-> on test la longueur
        If Len(ValueSaisie) > aField.Longueur Then
            DisplayMessage "La longueur de saisie est limit�e � " & aField.Longueur & " caract�re(s).", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
End Select

CtrlFormatZone = True

GestError:

End Function

Public Function EnregSave(aField As dc_field, ValueSaisie As String) As Boolean
'--> dans cette proc�dure on effectue des controles de second degr�
If aField.OldValue = "" Then aField.OldValue = aField.Value
aField.Value = ValueSaisie

'-> on modifie le statut
curStatut = pModify

EnregSave = True
Exit Function
GestError:

End Function

Public Function GetImage(curKeyWord As String) As Integer
'--> cette fonction permet de ramener le numero d'image du listimage
Dim intImage As Integer

On Error GoTo GestError

'-> on met une image selon le niveau
Select Case Entry(1, curKeyWord, "|")
    Case "FILE"
        intImage = 11
    Case "ENREG"
        '-> juste pour le timer
        DoEvents
        intImage = 20
    Case "FIELD"
        intImage = 10
End Select

If Not aEnreg Is Nothing Then
    intImage = cTypes(aEnreg.Libel).Image
End If

GetImage = intImage

Exit Function
GestError:
End Function

Public Function CtrlFieldsSame() As Boolean
'--> cette fonction � pour but de v�rifier qu'il y a eu une modification
Dim aField As dc_field
Dim i As Integer
Dim j As Integer

On Error GoTo GestError

For i = 1 To Files.Count
    '-> pointer sur lza serie d'enregistrement
    Set aFile = Files(i)
    For j = 1 To aFile.Count
        Set aEnreg = aFile(j)
        '-> on regarde si on a un statut different de celui de l'ouverture
        For Each aField In aEnreg.Fields
            If aField.Value <> aField.OldValue Then Exit Function
        Next
    Next
Next
GestError:

CtrlFieldsSame = True
End Function

Public Function CtrlFieldsSuppression(strKeyword As String) As Boolean
'--> cette fonction va permettre de verifier si on peut faire l'ajout

On Error GoTo GestError

'-> on teste si on peut faire une suppression
Select Case Mid(Fields(strKeyword).Struct, 1, 3)
    Case "S90", "S10", ""
        GoTo GestError
    Case Else
    
End Select

CtrlFieldsSuppression = True

GestError:

End Function

Public Function CtrlFieldsPaste(strKeyword As String) As Boolean
'--> cette fonction va permettre de verifier si on peut faire l'ajout

On Error GoTo GestError

'-> on teste si on peut faire un coller

'-> d�j� on doit avoir qque chose de copi�
If StrCopyKey = "" Then GoTo GestError

Select Case Mid(Fields(strKeyword).Struct, 1, 3)
    Case "S90", "S10", ""
        GoTo GestError
    Case Else
    
End Select

'-> on doit �tre sur le niveau identique ou au juste dessus
If Fields(StrCopyKey).Niveau = Fields(strKeyword).Niveau Then CtrlFieldsPaste = True
If Fields(StrCopyKey).Niveau = Fields(strKeyword).Niveau + 1 Then CtrlFieldsPaste = True

GestError:

End Function

Public Function CtrlFieldsCopy(strKeyword As String) As Boolean
'--> cette fonction va permettre de verifier si on peut faire l'ajout

On Error GoTo GestError

'-> on teste si on peut faire un copier
Select Case Mid(Fields(strKeyword).Struct, 1, 3)
    Case "S90", "S10", "", "S20"
        GoTo GestError
    Case Else
    
End Select

'-> valeur de succes
CtrlFieldsCopy = True

GestError:

End Function

Public Function FileExist(Fichier As String) As Boolean

On Error GoTo GestError

If Dir$(Fichier) <> "" Then FileExist = True
Exit Function

GestError:
    FileExist = False

End Function

Public Function EnregsSave() As Boolean
'--> cette fonction va permettre de reg�n�rer le fichier Ducs ou d'en creer un nouveau
'    on travaille avec un fichier tempo pour moins de risque
Dim aField As dc_field
Dim FileName As String
Dim TempFileName As String
Dim hdlFile As Integer
Dim i As Integer
Dim j As Integer
Dim strligne As String

If Files Is Nothing Then Exit Function

If Files.Count = 0 Then Exit Function

'-> on regarde si il y a eu des modifications sur le fichier en cours
If CtrlFieldsSame Then
    DisplayMessage "Aucunes modifications sur le fichier en cours", dsmWarning, dsmOkOnly, "Reg�n�ration du fichier"
    Exit Function
End If

'-> demander le nom du fichier on propose l'existant par defaut
If DisplayMessage("Fichier � cr�er", dsmQuestion, dsmOkCancel, "Reg�n�ration du fichier", True, FileDucs) = "ANNULER" Then
    Exit Function
End If

FileName = InputValue

'-> on �crit le nouveau fichier Ducs dans un fichier temporaire
TempFileName = GetTempFileNameVB("Ducs")
hdlFile = FreeFile
Open TempFileName For Output As #hdlFile

'-> on affiche le timer
df_Drloading.Show
df_Drloading.Label1.Caption = "Enregistrement en cours..."


'-> on parcours les enregistrements pour les ecrire dans le fichier
For i = 1 To Files.Count
    '-> on pointe sur la serie d'enregistrement
    Set aFile = Files(i)
    For j = 1 To aFile.Count
        '-> on pointe sur les enregistrements
        Set aEnreg = aFile(j)
        strligne = ""
        '-> on parcourt tous les champs
        For Each aField In aEnreg.Fields
            strligne = strligne & Mid(aField.Value & Space(255), 1, Val(aField.Longueur))
            i = i + 1
            If i = 2000 Then
                i = 0
                DoEvents
            End If
        Next
    '-> imprimer l'enregistrement
    Print #hdlFile, strligne
    Next
Next

'-> fermer le fichier
Close #hdlFile

'-> on masque le timer
Unload df_Drloading

'-> si le fichier existe demander si on l'�crase
If FileExist(FileName) Then
    If DisplayMessage("Le fichier" & Chr(13) & FileName & Chr(13) & "existe d�j�." & Chr(13) & "Le remplacer ?", dsmInterro, dsmYesNo, "Reg�n�ration du fichier") = "NON" Then
        Exit Function
    Else
        '-> supprimer le fichier
        Kill FileName
    End If
End If

'-> copier le fichier
FileCopy TempFileName, FileName

'-> on modifie le statut en cours
curStatut = pSave

'-> Supprimer le fichier temporaire
If Dir$(TempFileName) <> "" Then Kill TempFileName

GestError:
End Function

Public Function Initialise() As Boolean
'--> cette fonction a pour but de tout reinitialiser
Dim aField As dc_field
Dim i As Integer
Dim iTaille As Single

'On Error Resume Next

df_Drloading.Show
df_Drloading.Label1.Caption = "Vidage de la m�moire en cours..."
'-> on initialise la variable de copie
StrCopyKey = ""
'-> on vide le treeview
LockWindowUpdate df_Ducs.hwnd
df_Ducs.TreeView1.Nodes.Clear
'-> on vide le dealgrid
df_Ducs.DealGrid1.ClearRow
'-> on vide la zone non de fichier
df_Ducs.Label1.Caption = ""
'-> on vide les objets de travail
Set Enregs = Nothing
If Not Fields Is Nothing Then
    iTaille = Fields.Count
    Do While Fields.Count <> 0
        Fields.Remove (1)
        i = i + 1
        If i = 1000 Then
            i = 0
            df_Drloading.Label1.Caption = "Vidage de la m�moire en cours..." & 100 - CInt(Fields.Count / iTaille * 100) & "%"
            DoEvents
        End If
    Loop
End If
Set Types = Nothing
Unload df_Drloading
LockWindowUpdate 0
End Function

Private Function GetFieldAssociateValue(strKeyword As String, strStruct As String) As dc_field
'--> cette fonction permet de recup�rer la valeur d'un enregistrement � partir de sa valeur de Enreg
'    en s'appuyant sur les donn�es transversales d'un enreg de reference

Dim aField As dc_field
Dim curNiveau As Integer
Dim structNiveau As Integer
Dim curKeyWord As String
Dim Niveau As Variant
Dim strTemp As Variant
Dim i As Integer

'-> on pointe sur le niveau de la valeur de reference
curNiveau = Enregs(Fields(strKeyword).Struct).FieldNiveau
structNiveau = Enregs(strStruct).FieldNiveau

'-> on analyse ou rechercher la valeur
If structNiveau <= curNiveau Then
    '-> on va chercher ici la valeur sur la transversale
    '-> on se met la cle dans un vecteur pour pouvoir travailler dessus
    Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
    '-> on se charge le vecteur a partir de la cl�
    strTemp = Split(strKeyword, "|")
    For i = 1 To 7
        If i <= structNiveau Then
            Niveau(i) = CSng(strTemp(i))
        Else
            Niveau(i) = 0
        End If
    Next
    curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
    '-> on boucle pour trouver la bonne valeur
    Do While Fields(1).IsField(curKeyWord)
        If Fields(curKeyWord).Struct = strStruct Then
            Set GetFieldAssociateValue = Fields(curKeyWord)
            Exit Function
        End If
        '-> on passe a l'enreg suivant en testant egalement les eventuelles creations (niveau 6)
        If Fields(1).IsField("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & (Niveau(6) + 1) & "|" & Niveau(7)) Then
            Niveau(6) = Niveau(6) + 1
            curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
        Else
            Niveau(6) = 0
            Niveau(7) = Niveau(7) + 1
            curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
        End If
    Loop
Else
    DisplayMessage "Il peut y avoir plusieurs donn�es r�pondant � cette requ�te!" & Chr(13) & strKeyword & " sur " & strStruct, dsmCritical, dsmOkOnly, "GetFieldAssociateValue"
End If

GestError:

End Function

Public Function searchExe(strFile As String) As String
'-> cette fonction va chercher les fichiers en fonction du param�trage
'   diff�rentes possibilit�s
'   rien
'   chemin spe|chemin std
'   gloident
Dim sPath As String
Dim sParam As String

stopSearch = False

If Command$ = "" Then
    sParam = "deal"
Else
    sParam = Command$
End If

If InStr(1, sParam, "|") <> 0 Then
    '-> on lit d'abord dans le spe puis le standard
    If Dir$(Entry(1, sParam, "|") & "\" & strFile) <> "" Then
        searchExe = Entry(1, sParam, "|") & "\" & strFile
    Else
        If Dir$(Entry(1, sParam, "|") & "\" & strFile) <> "" Then
            searchExe = Entry(1, sParam, "|") & "\" & strFile
        Else
            searchExe = App.Path & "\" & strFile
        End If
    End If
Else
    '-> � partir du gloident on va essayer de s'y retrouver!
    '-> on pointe sur le chemin de base
    sPath = App.Path
    '-> pour les test
    'sPath = "R:\V6\tools\exe"
    sPath = Replace(sPath, "/", "\")
    '-> on regarde si on est en v51 ou v52
    If InStr(1, sPath, "\V51\") <> 0 Or InStr(1, sPath, "\V52\") <> 0 Then
        If InStr(1, sPath, "\V51\") <> 0 Then
            sPath = Mid(sPath, 1, InStr(1, sPath, "\V51\", vbTextCompare) + 4)
        Else
            sPath = Mid(sPath, 1, InStr(1, sPath, "\V52\", vbTextCompare) + 4)
        End If
        '-> repertoire v52/sophie de l'ident
        If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\deal\", "\" & Trim(sParam) & "\", , , vbTextCompare) & "\Sophie\", strFile)
        '-> repertoire v52/sophie de deal
        If searchExe = "" Then searchExe = SearchForFiles(sPath & "\Sophie\", strFile)
    Else '-> on regarde si on est en V6
        If InStr(1, sPath, "\dog\", vbTextCompare) <> 0 Or InStr(1, sPath, "\tools\", vbTextCompare) <> 0 Then
            sPath = Replace(sPath, "\tools\", "\dog\", , , vbTextCompare)
            sPath = Mid(sPath, 1, InStr(1, sPath, "\dog\", vbTextCompare) + 4)
            '-> on cherche dans le repertoire maquette du client
            If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\dog\", "\" & Trim(sParam) & "\sophie\", , , vbTextCompare), strFile)
            '-> on cherche dans le repertoire spe du client
            If searchExe = "" Then searchExe = SearchForFiles(sPath & "\" & sParam & "\", strFile)
            '-> on cherche dans le repertoire des maquettes
            If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\dog\", "\maq\gui\sophie\", , , vbTextCompare), strFile)
        End If
    End If
    '-> on regarde dans le repertoire courant
    If searchExe = "" Then searchExe = SearchForFiles(App.Path & "\" & Trim(sParam) & "\", strFile)
    If searchExe = "" Then searchExe = SearchForFiles(App.Path & "\", strFile)
    '-> on ramene le repertoire courant
    If searchExe = "" Then searchExe = App.Path & "\" & strFile
End If

End Function

Private Function SearchForFiles(sRoot As String, sfile As String, Optional sFiltreDirectory As String) As String
    '--> cette fonction cherche des fichiers � partir d'une directorie
   Dim WFD As WIN32_FIND_DATA
   Dim hFile As Long
  
   With fp
      .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
      .sFileNameExt = sfile                'fichier (* ? autoris�
      .bRecurse = 1                             'True = recherche recursive
      .bFindOrExclude = 1                       '0=inclure, 1=exclure
   End With
   
   
   hFile = FindFirstFile(sRoot & "*.*", WFD)
   If hFile <> -1 Then
      Do
        If stopSearch = True Then Exit Function
        DoEvents
        'si c'est un repertoire on boucle
         If (WFD.dwFileAttributes And vbDirectory) Then
            If Asc(WFD.cFileName) <> CLng(46) Then
                If fp.bRecurse Then
                    SearchForFiles = SearchForFiles(sRoot & TrimNull(WFD.cFileName) & vbBackslash, sfile)
                End If
            End If
         Else
           'doit etre un fichier..
            If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
                If sFiltreDirectory <> "" Then
                    If InStr(1, sRoot & TrimNull(WFD.cFileName), sFiltreDirectory) <> 0 Then
                        SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                        stopSearch = True
                        Exit Do
                    End If
                Else
                    SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                    stopSearch = True
                    Exit Do
                End If
            End If
         End If
      Loop While FindNextFile(hFile, WFD)
   End If
   Call FindClose(hFile)
End Function

Private Function QualifyPath(sPath As String) As String
   If Right$(sPath, 1) <> vbBackslash Then
      QualifyPath = sPath & vbBackslash
   Else
      QualifyPath = sPath
   End If
End Function

Private Function TrimNull(startstr As String) As String
   TrimNull = Left$(startstr, lstrlen(StrPtr(startstr)))
End Function

Private Function MatchSpec(sfile As String, sSpec As String) As Boolean
   MatchSpec = PathMatchSpec(StrPtr(sfile), StrPtr(sSpec)) = fp.bFindOrExclude
End Function

Public Sub SkinColorModify(aForm As Form)
'--> ceci est un essai de changement de couleur
Dim Lcolor As Long
Dim aControl As Control
Dim sKinName As String

'-> V�rifier que l'on trouve le fichier d'application du skin
sKinName = searchExe("Skin.ini")
If Dir$(sKinName) = "" Then Exit Sub

'-> Appliquer un skin s'il y en a un
If GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BodyBackColor = Val(GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "BODYFORECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BodyForeColor = Val(GetIniFileValue("SKIN", "BODYFORECOLOR", sKinName))
If GetIniFileValue("SKIN", "BORDERCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BorderColor = Val(GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERBACKCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderBackColor = Val(GetIniFileValue("SKIN", "HEADERBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERFORECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderForeColor = Val(GetIniFileValue("SKIN", "HEADERFORECOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERLINECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderLineColor = Val(GetIniFileValue("SKIN", "HEADERLINECOLOR", sKinName))
If GetIniFileValue("SKIN", "SHADOWCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.ShadowColor = Val(GetIniFileValue("SKIN", "SHADOWCOLOR", sKinName))

'-> on parcours les differents elements pour faire une variation de couleur
For Each aControl In aForm
    If TypeOf aControl Is Shape Then
        If aControl.BackColor = &HF1CFB1 Or aControl.BackColor = 16708585 Then aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
        If aControl.BorderColor = &HC78D4B Then aControl.BorderColor = aForm.DogSkinObject1.BorderColor
    End If
    If TypeOf aControl Is DealCmdButton Or TypeOf aControl Is PictureBox Or TypeOf aControl Is DealCheckBox Then
        If aControl.BackColor = &HF1CFB1 Or aControl.BackColor = 16579059 Then aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
    End If
    If TypeOf aControl Is DealGrid Then
        If aControl.BackColor = &HFDDFC4 Then aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
        If aControl.BackColorFixed = 16637889 Then aControl.BackColorFixed = aForm.DogSkinObject1.ShadowColor
        If aControl.BackColorSel = 10053171 Then aControl.BackColorSel = aForm.DogSkinObject1.ShadowColor
    End If
Next

End Sub

Public Function GetVariableEnv(strVariable As String) As String

Dim Res As Long
Dim lpBuffer As String

lpBuffer = Space$(500)
Res = GetEnvironmentVariable(strVariable, lpBuffer, Len(lpBuffer))
If Res <> 0 Then
    '-> Faire un trim sur le buffer
    lpBuffer = Mid$(lpBuffer, 1, Res)
Else
    lpBuffer = ""
End If

'-> Retouner la valeur
GetVariableEnv = lpBuffer

End Function



