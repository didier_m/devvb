VERSION 5.00
Object = "{DAFD85F1-343B-47EE-8170-1C26723F7A5D}#2.0#0"; "dogskin.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form df_AjoutLignes 
   BackColor       =   &H00CBB49A&
   BorderStyle     =   0  'None
   Caption         =   "Ajout multiple d'enregistrement"
   ClientHeight    =   5910
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7710
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   5910
   ScaleWidth      =   7710
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin DogSkin.DogSkinObject DogSkinObject1 
      Left            =   0
      Top             =   0
      _ExtentX        =   979
      _ExtentY        =   953
      BorderColor     =   10053171
      BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderForeColor =   10053171
      HeaderBackColor =   16637889
      HeaderLineColor =   15847345
      HeaderUseLine   =   -1  'True
      BeginProperty BodyFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BodyForeColor   =   10053171
      BodyBackColor   =   16579059
      HeaderIco       =   "df_AjoutLignes.frx":0000
      HeaderIcoNa     =   "df_AjoutLignes.frx":0682
      ShadowColor     =   16637889
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   4215
      Left            =   240
      TabIndex        =   0
      Top             =   720
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   7435
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin DogSkin.DealCmdButton Picture1 
      Height          =   615
      Left            =   6360
      TabIndex        =   1
      Tag             =   "DICTAB�BUTTON-OK"
      Top             =   5220
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   1
      ButtonForm      =   1
      BackColor       =   16579059
   End
   Begin DogSkin.DealCmdButton Picture3 
      Height          =   615
      Left            =   6960
      TabIndex        =   2
      Tag             =   "DICTAB�BUTTON-CANCEL"
      Top             =   5220
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   2
      ButtonForm      =   1
      BackColor       =   16579059
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4080
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   21
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":0D04
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":19DE
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":36B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":4392
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":506C
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":5D46
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":6A20
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":76FA
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":7FD4
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":9CAE
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":A588
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":B262
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":BB3C
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":C416
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":D0F0
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":DDCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":E6A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":1037E
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":12058
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":1784A
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_AjoutLignes.frx":19524
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "df_AjoutLignes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
    Case 113 'F2 validation
        Picture1_Click
    Case 115, 27 'F4 ou esc annulation bon
        Unload Me
End Select

End Sub

Private Sub Form_Load()

'---> Init du skin
Me.DogSkinObject1.Initialisation False, "Ajout multiple d'enregistrement", pCloseOnly, False
Me.DogSkinObject1.SetMatriceEffect "SHAPE1"
'-> on charge la couleur de fond desbouttons
Me.Picture1.BackColor = Me.DogSkinObject1.BodyBackColor
Me.Picture3.BackColor = Me.DogSkinObject1.BodyBackColor

End Sub

Private Sub Picture1_Click()
Dim aItem As ListItem
Dim topOk As Boolean
Dim curRow As Integer
Dim strKeyword As String

'-> on verifie que l'on a bien selectionne une ligne
For Each aItem In Me.ListView1.ListItems
    If aItem.Checked Then topOk = True
Next
If topOk = False Then Unload Me
curRow = df_Ducs.DealGrid1.Row
strKeyword = df_Ducs.DealGrid1.GetCellText(3, curRow)

For Each aItem In Me.ListView1.ListItems
    If aItem.Checked Then
        curRow = curRow + 1
        '-> ok on a bien une ligne on ajoute les fields
        '-> on ajoute une ligne
        df_Ducs.DealGrid1.AddRow True, curRow
        '-> on insere les valeurs de ligne
        df_Ducs.DealGrid1.SetCellText 0, curRow, aItem.Text
        df_Ducs.DealGrid1.SetCellText 1, curRow, aItem.SubItems(1)
        df_Ducs.DealGrid1.SetColAlignement 2, 2
        df_Ducs.DealGrid1.SetCellText 3, curRow, aItem.SubItems(2)
        '-> on ajoute le champ
        Set aField = New dc_field
        aField.KeyWord = aItem.SubItems(2)
        aField.Niveau = Enregs(aItem.Text).FieldNiveau
        aField.Struct = Enregs(aItem.Text).FieldValue
        aField.Statut = pCours
        '-> avant d'ajouter on regarde si le champ n'est pas d�ja existant
        If Fields(1).IsField(aField.KeyWord) Then
            Fields(aField.KeyWord).Statut = pCours
        Else
            Fields.Add aField, aField.KeyWord, , strKeyword
        End If
    Else
        '-> on cree juste le field a delete pour eviter les trous
        Set aField = New dc_field
        aField.KeyWord = aItem.SubItems(2)
        aField.Niveau = Enregs(aItem.Text).FieldNiveau
        aField.Struct = Enregs(aItem.Text).FieldValue
        aField.Statut = pDelete
        '-> avant d'ajouter on regarde si le champ n'est pas d�ja existant
        If Fields(1).IsField(aField.KeyWord) Then
            Fields(aField.KeyWord).Statut = pCours
        Else
            Fields.Add aField, aField.KeyWord, , strKeyword
        End If
    End If
    strKeyword = aItem.SubItems(2)
Next

Unload Me
End Sub

Private Sub Picture3_Click()

'-> on quitte sans rien faire
Unload Me

End Sub
