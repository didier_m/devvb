Attribute VB_Name = "fctPrintBrowse"
'---> Variables de d�finition des lignes d'impression
Const DefCelluleTitre = "5;Date d'impression : ^DATE Page Num�ro : ^PAGE;LARGEUR;Faux,Faux,Faux,Faux;Times New Roman;10;Vrai;Faux;Faux;0;16777215;FAUX;0@0"
Const DefCelluleEntete = "5;CONTENU;LARGEUR;Vrai,Vrai,Vrai,Vrai;Times New Roman;10;Vrai;Faux;Faux;0;13158600;FAUX;0@0"
Const DefCelluleDetail = "5;CONTENU;LARGEUR;Faux,Faux,Vrai,Vrai;Times New Roman;10;Faux;Faux;Faux;0;16777215;FAUX;0@0"
Const DefCelluleBasTableau = "5;;LARGEUR;Faux,Vrai,Vrai,Vrai;Times New Roman;10;Faux;Faux;Faux;0;16777215;FAUX;0@0"
Const DefCellulePage = "9;Edit� le ^DATE       - Page ^NUMP  - ;LARGEUR;Faux,Faux,Faux,Faux;Times New Roman;10;Faux;Faux;Faux;0;16777215;FAUX;0@0"

Private LigneDetail As String
Private NbPage As Integer

Private BrowseCols() As Double
Private Browse As MSFlexGrid
Private LargeurTableau As Single
Private HauteurTableau As Single
Private LargeurMaquette As Single
Private HauteurMaquette As Single
Private HauteurCours As Single
Private ModeImpression As String
Private BrowseCells() As String

Private Sub InitBrowseDimensions()

'---> Cette fonction est charg�e de cr�er de cr�re la matrice des colonnes de tableau

Dim i As Integer, j As Integer
Dim ContenuCell As String
Dim LargeurCell As Single


'-> Redimensionner la matrice en fonction du nombre de colonnes dans le tableau
Erase BrowseCols()
ReDim BrowseCols(Browse.Cols - 1)

'-> Redimensionner la matrice du contenu des cellules
Erase BrowseCells()
ReDim BrowseCells(Browse.Rows - 1, Browse.Cols - 1)

'-> Intialiser les variables
LargeurTableau = 0
HauteurTableau = 0
HauteurCours = 0
NbPage = 1

'-> Analyser le contenu de chaque cellule par colonne pour d�terminer la lergeur du contenu
For i = 0 To Browse.Cols - 1
    '-> Analyse du contenu de chaque ligne et de sa dimensions
    For j = 0 To Browse.Rows - 1
        '-> R�cup�rer le contenu de la cellule
        ContenuCell = Browse.TextArray(Get_Index_Cell(j, i, Browse))
        '-> Mettrre � jour la matrice des cellules
        BrowseCells(j, i) = ContenuCell
        '-> Largeur de la cellule
        LargeurCell = frmLaunch.TextWidth(ContenuCell) * 1.3
        '-> Setting si largeur sup�rieur
        If LargeurCell > BrowseCols(i) Then BrowseCols(i) = LargeurCell
    Next 'Pour toutes les lignes
Next '-> Pour toutes les colonnes du browse

'-> Setting de la largeur de l'�dition
For i = 0 To Browse.Cols - 1
    LargeurTableau = LargeurTableau + frmLaunch.ScaleX(BrowseCols(i), 3, 7)
Next

'-> Setting des dimensions de la maquette
If LargeurTableau > 20.5 Then
    '-> Passage en mode paysage
    LargeurMaquette = 21
    HauteurMaquette = 29.7
    ModeImpression = "0"
Else
    LargeurMaquette = 21
    HauteurMaquette = 29.7
    ModeImpression = "1"
End If


End Sub


Private Function CreateMaqGui() As String

'---> Cette focntion est charg�e de cr�er la maquette qui correspond au browse � imprimer

Dim TempoFileName As String
Dim hdlFile As Integer
Dim strTempo As String
Dim i As Integer
Dim strChamps As String

'-> Ouvrir un fichier temporaire
TempoFileName = GetTempFileNameVB("BRW")
hdlFile = FreeFile
Open TempoFileName For Output As #hdlFile


'-> Impression de l'entete de la maquette
Print #hdlFile, "\DefEntete�Begin"
Print #hdlFile, "\Hauteur�" & Replace(CStr(HauteurMaquette), ",", ".")
Print #hdlFile, "\Largeur�" & Replace(CStr(LargeurMaquette), ",", ".")
Print #hdlFile, "\MargeTop�1"
Print #hdlFile, "\MargeLeft�1"
Print #hdlFile, "\Orientation�" & ModeImpression
Print #hdlFile, "\Suite�"
Print #hdlFile, "\Report�"
Print #hdlFile, "\Entete�"
Print #hdlFile, "\Pied�"
Print #hdlFile, "\PageGarde�FAUX"
Print #hdlFile, "\PageSelection�FAUX"
Print #hdlFile, "\DefEntete�End"

'-> Impression de l'entete du tableau
Print #hdlFile, "[TB-Visualisation]"
Print #hdlFile, "\OrientationTB�" & ModeImpression

'-> Impression de la ligne de titre du tableau
strTempo = DefCelluleTitre
strTempo = Replace(strTempo, "LARGEUR", LargeurTableau)

'-> Enregistrer la ligne du titre du tableau
Print #hdlFile, "\Begin�Titre"
Print #hdlFile, "\AlignTop�1"
Print #hdlFile, "\Top�0"
Print #hdlFile, "\AlignLeft�3"
Print #hdlFile, "\Left�0"
Print #hdlFile, "\Largeur�" & LargeurTableau
Print #hdlFile, "\Hauteur�1"
Print #hdlFile, "\Distance�0"
Print #hdlFile, "\Ligne�1"
Print #hdlFile, "\Colonne�1"
Print #hdlFile, "\Col�1�" & strTempo
Print #hdlFile, "\Champs�"
Print #hdlFile, "\End�Titre"

'-> Enregistrer La ligne d'entete du tableau
Print #hdlFile, "\Begin�Entete"
Print #hdlFile, "\AlignTop�1"
Print #hdlFile, "\Top�0"
Print #hdlFile, "\AlignLeft�3"
Print #hdlFile, "\Left�0"
Print #hdlFile, "\Largeur�" & Replace(CStr(LargeurTableau), ",", ".")
Print #hdlFile, "\Hauteur�1"
Print #hdlFile, "\Distance�0"
Print #hdlFile, "\Ligne�1"
Print #hdlFile, "\Colonne�" & Browse.Cols
Print #hdlFile, "\Col�1�";

'-> Cr�er chaque colonne
For i = 0 To Browse.Cols - 1
    '-> Remplacer les valeurs
    strTempo = DefCelluleEntete
    strTempo = Replace(strTempo, "CONTENU", BrowseCells(0, i))
    strTempo = Replace(strTempo, "LARGEUR", Replace(CStr(frmLaunch.ScaleX(BrowseCols(i), 3, 7)), ",", "."))
    '-> Ecrire la cellule
    If i <> Browse.Cols - 1 Then
        Print #hdlFile, strTempo;
        Print #hdlFile, "|";
    Else
        Print #hdlFile, strTempo
    End If
Next 'pour toutes les colonnes
Print #hdlFile, "\Champs�"
Print #hdlFile, "\End�Entete"

'-> Formatter le contenu de la ligne de detail pour l'impression du spool
LigneDetail = "[TB-Visualisation(BLK-Detail)][\1]{"

'-> Enregistrer la ligne de d�tail du tableau
Print #hdlFile, "\Begin�Detail"
Print #hdlFile, "\AlignTop�1"
Print #hdlFile, "\Top�0"
Print #hdlFile, "\AlignLeft�3"
Print #hdlFile, "\Left�0"
Print #hdlFile, "\Largeur�" & Replace(CStr(LargeurTableau), ",", ".")
Print #hdlFile, "\Hauteur�0.50"
Print #hdlFile, "\Distance�0"
Print #hdlFile, "\Ligne�1"
Print #hdlFile, "\Colonne�" & Browse.Cols
Print #hdlFile, "\Col�0.50�";

'-> Cr�er chaque colonne et la ligne de d�tail
For i = 1 To Browse.Cols
    '-> Remplacer les valeurs
    strTempo = DefCelluleDetail
    strTempo = Replace(strTempo, "CONTENU", "^" & Format(i, "0000"))
    strTempo = Replace(strTempo, "LARGEUR", Replace(CStr(frmLaunch.ScaleX(BrowseCols(i - 1), 3, 7)), ",", "."))
    '-> Ecrire la cellule
    If i <> Browse.Cols Then
        Print #hdlFile, strTempo;
        Print #hdlFile, "|";
    Else
        Print #hdlFile, strTempo
    End If
    strChamps = strChamps & Format(i, "0000") & "; ;|"
    '-> Ligne de d�tail
    LigneDetail = LigneDetail & "^" & Format(i, "0000") & "#" & Format(i, "0000") & Space$(50)
Next 'pour toutes les colonnes
'-> Fermer le descriptif de la ligne de detail
LigneDetail = LigneDetail & "}"
'-> Imprimer la liste des champs
Print #hdlFile, "\Champs�" & Mid$(strChamps, 1, Len(strChamps) - 1)
Print #hdlFile, "\End�Detail"

'-> Enregistrement de la ligne de fin de tableau
Print #hdlFile, "\Begin�FinTableau"
Print #hdlFile, "\AlignTop�1"
Print #hdlFile, "\Top�0"
Print #hdlFile, "\AlignLeft�3"
Print #hdlFile, "\Left�0"
Print #hdlFile, "\Largeur�" & Replace(CStr(LargeurTableau), ",", ".")
Print #hdlFile, "\Hauteur�0.20"
Print #hdlFile, "\Distance�0"
Print #hdlFile, "\Ligne�1"
Print #hdlFile, "\Colonne�" & Browse.Cols
Print #hdlFile, "\Col�0.20�";
'-> Cr�er chaque colonne
For i = 1 To Browse.Cols
    '-> Remplacer les valeurs
    strTempo = DefCelluleBasTableau
    strTempo = Replace(strTempo, "LARGEUR", Replace(CStr(frmLaunch.ScaleX(BrowseCols(i - 1), 3, 7)), ",", "."))
    '-> Ecrire la cellule
    If i <> Browse.Cols Then
        Print #hdlFile, strTempo;
        Print #hdlFile, "|";
    Else
        Print #hdlFile, strTempo
    End If
Next 'pour toutes les colonnes
Print #hdlFile, "\Champs�"
Print #hdlFile, "\End�FinTableau"

'-> Enregistrement de la ligne de pagination de date et heure
Print #hdlFile, "\Begin�Pagination"
Print #hdlFile, "\AlignTop�4"
Print #hdlFile, "\Top�0"
Print #hdlFile, "\AlignLeft�3"
Print #hdlFile, "\Left�0"
Print #hdlFile, "\Largeur�" & Replace(CStr(LargeurTableau), ",", ".")
Print #hdlFile, "\Hauteur�0.50"
Print #hdlFile, "\Distance�0"
Print #hdlFile, "\Ligne�1"
Print #hdlFile, "\Colonne�1"

strTempo = DefCellulePage
strTempo = Replace(strTempo, "LARGEUR", Replace(CStr(LargeurTableau), ",", "."))
Print #hdlFile, "\Col�0.50�" & strTempo
Print #hdlFile, "\Champs�DATE;;|NUMP;;"
Print #hdlFile, "\End�Pagination"

'-> Enregistrer la fin du tableau
Print #hdlFile, "\Tableau�End"

'-> Fermer le fichier maquette
Close #hdlFile

'-> Renvoyer le nom du fichier maquette
CreateMaqGui = TempoFileName

End Function

Private Function CreateSpoolVisu(ByVal Maq As String) As String

'---> Cette fonction cr�er un spool de visualisation

Dim TempFileName As String
Dim hdlFile As Integer
Dim i As Integer, j As Integer


'-> Obtenir un nom de fichier tempo et l'ouvrir
TempFileName = GetTempFileNameVB("VIS")
hdlFile = FreeFile
Open TempFileName For Output As #hdlFile

'-> Chaine d'impression de la maquette
Print #hdlFile, "%%GUI%%" & Maq

'-> Impression de l'entete de la page
PrintEntetePage hdlFile

'-> Impression du corps du fichier
For i = 1 To Browse.Rows - 1
    '-> Tester si on a assez de palce pour imprimer la ligne
    If Not IsPlace(0.5) Then
        '-> Impression du report de page + Impression de l'entete de l'impression suivante
        PrintReportPage hdlFile, True
        PrintEntetePage hdlFile
    End If
    
    '-> Impression de la ligne
    PrintDetailLig hdlFile, i

Next 'Pour toutes les lignes

'-> Imprimer la ligne de fin de tableau sans saut de page
PrintReportPage hdlFile, False

'-> Fermer le fichier spool
Close #hdlFile

'-> Renvoyer le spool de visu
CreateSpoolVisu = TempFileName

End Function
Private Function PrintDetailLig(ByVal hdlFile, ByVal RowToPrint As Integer)

'---> Cette fonction imprime une ligne de detail du tableau

Dim i As Integer
Dim strTempo As String

'-> Recup du descro de la ligne
strTempo = LigneDetail

For i = 1 To Browse.Cols
    strTempo = Replace(strTempo, "#" & Format(i, "0000"), BrowseCells(RowToPrint, i - 1))
Next 'Pour toutes les colonnes du tableau

'-> Imprimer la ligne de detail dans le fichier
Print #hdlFile, strTempo

'-> Mettre � jour la hauteur du spool en cours d'impression
HauteurCours = HauteurCours + 0.5

End Function

Private Function PrintEntetePage(ByVal hdlFile As Integer)

'-> Impression du titre la premi�re fois
Print #hdlFile, "[TB-Visualisation(BLK-TITRE)][\1]{^DATE" & Now & "   ^PAGE" & NbPage & " }"

'-> Impression de l'entete du tableau
Print #hdlFile, "[TB-Visualisation(BLK-ENTETE)][\1]{ }"

'-> setting de la hauteur de la ligne
HauteurCours = 3 'MargeTop(1) + Titre(1) + Entete(1)

End Function


Private Function PrintReportPage(ByVal hdlFile As Integer, ByVal Saut As Boolean)

'---> Cette fonction imprime le block de fin de tableau + ligne de report

'-> Ligne de fin de tableau
Print #hdlFile, "[TB-Visualisation(BLK-FinTableau)][\1]{}"

'-> Ligne de pagination
'Print #hdlFile, "[TB-Visualisation(BLK-PAGINATION)][\1]{^DATE" & Now & "  ^NUMP" & NbPage & "   }"

'-> Tag de saut de page si necessaire
If Saut Then Print #hdlFile, "[PAGE]"

'-> Incr�menter le compteur de page
NbPage = NbPage + 1

End Function

Private Function IsPlace(ByVal HauteurLig As Single) As Boolean

'---> Cette fonction indique si on a assez de place pour imprimer la ligne sp�cifi�e

'-> La hauteur necessaire est de : MargeBas(1) + Ligne de report(0.5) + Ligne Fin tableau (0.2) + hauteur de la ligne � imprimer
If HauteurMaquette - HauteurCours - 1.7 - HauteurLig > 0 Then
    IsPlace = True
Else
    IsPlace = False
End If

End Function

Public Sub PrintBrowse(BrowseToPrint As MSFlexGrid)

'---> Cette fonction est charg�e d'imprimer le contenu d'un browse

Dim Maquette As String
Dim SpoolVisu As String

'-> Affecter le browse � imprimer � la variable du module d'impression
Set Browse = BrowseToPrint

'-> Lancer dans un premier temps le calcul des dimensions du tableau
Call InitBrowseDimensions

'-> Cr�er la maquette
Maquette = CreateMaqGui()

'-> Cr�er le spool de visu du tableau
SpoolVisu = CreateSpoolVisu(Maquette)

'-> Lancer la visu
Shell TurboPath & " ECRAN|" & SpoolVisu, vbMaximizedFocus


End Sub
