Attribute VB_Name = "CommunLib"

'***************************************************************
'*                                                             *
'* Contient les fonctions communes � tout les projets          *
'*                                                             *
'***************************************************************

'Date : 10/10/2000
'Modif : 06/12/2000 -> DOG


'-> Liste des API utilis�es dans ces diverses fonctions

'-> Api gestion des fichiers temporaires
Private Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Private Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long

'-> API pour r�cup�ration de la taille des fichiers ASCII
Private Declare Function CloseHandle& Lib "kernel32" (ByVal hObject As Long)
Private Declare Function OpenFile& Lib "kernel32" (ByVal lpFileName As String, lpReOpenBuff As OFSTRUCT, ByVal wStyle As Long)
Private Declare Function GetFileSize& Lib "kernel32" (ByVal hFile As Long, lpFileSizeHigh As Long)

Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

'-> Pour get des dimensions d'une fen�tre
Public Declare Function GetClientRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long

'-> Pour MAJ d'une fen�tre
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long

'-> API Pour gestion du temps
Public Declare Function GetTickCount& Lib "kernel32" ()

'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)


'-> Constantes de lecture des fichiers ASCII
Private Const OFS_MAXPATHNAME& = 128
Private Const OF_READ& = &H0

'-> Structure de lecture des fichiers ascci
Private Type OFSTRUCT
    cBytes As Byte
    fFixedDisk As Byte
    nErrCode As Integer
    Reserved1 As Integer
    Reserved2 As Integer
    szPathName(OFS_MAXPATHNAME) As Byte
End Type

'-> API pour gestion des messages
Public Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)

'-> Constantes de message
Public Const WM_CLOSE& = &H10
Public Const LVM_SETCOLUMNWIDTH = 4126 '( LVM_FIRST + 30 )
Public Const LVM_GETCOLUMNWIDTH = 4125 '( LVM_FIRST + 29 )
Public Const WM_CHILDACTIVATE = &H22

'-> API de lecture de fichier INI
Public Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function GetPrivateProfileSection& Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long
Public Declare Function WritePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpString As String, ByVal lpFileName As String) As Long
Public Declare Function GetPrivateProfileSectionNames Lib "kernel32.dll" Alias "GetPrivateProfileSectionNamesA" (ByVal lpszReturnBuffer As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

'-> Pour ouverture du site DEAL INFORMATIQUE
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

'-> Pour convertion des chiffres
'-> Gestion des s�parateurs d�cimaux
Public SepDec As String
Public SepMil As String
Private Declare Function GetLocaleInfoVB& Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long)

'-> Variable d'�change
Public strRetour As String

Public Function InitConvertion()

Dim lpBuffer As String
Dim Res As Long
Dim LOCALE_STHOUSAND As Long
Dim LOCALE_USER_DEFAULT As Long
Dim LOCALE_SDECIMAL As Long

'-> S�parateur de millier : gestion des variables d'environnement
LOCALE_STHOUSAND = &HF
LOCALE_USER_DEFAULT& = &H400
LOCALE_SDECIMAL = 14

lpBuffer = Space$(10)
Res = GetLocaleInfoVB(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, lpBuffer, Len(lpBuffer))
SepDec = Mid$(lpBuffer, 1, Res - 1)
lpBuffer = Space$(10)
Res = GetLocaleInfoVB(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, lpBuffer, Len(lpBuffer))
SepMil = Mid$(lpBuffer, 1, Res - 1)


End Function


Public Function Convert(ByVal StrToAnalyse As String) As String

Dim i As Integer
Dim Tempo As String
Dim FindSep As Boolean

For i = Len(StrToAnalyse) To 1 Step -1
    If Mid$(StrToAnalyse, i, 1) = "." Then
        If Not FindSep Then
            Tempo = SepDec & Tempo
            FindSep = True
        End If
    ElseIf Mid$(StrToAnalyse, i, 1) = "," Then
        If Not FindSep Then
            Tempo = SepDec & Tempo
            FindSep = True
        End If
    Else
        Tempo = Mid$(StrToAnalyse, i, 1) & Tempo
    End If
Next 'Pour tous les caract�res � analyser

Convert = Tempo

End Function

Public Function AddEntryInMatrice(Matrice As String, IdEntry As Long, NewCode As String, Sep As String) As String

'---> Cette proc�dure ajoute une entr�e dans la matrice

Dim i As Long
Dim MatriceTemp As String
Dim ToAdd As Boolean

If IdEntry = 1 Then '-> Si on doit ajouter en premier
    If Trim(Matrice) = "" Then
        Matrice = NewCode
    Else
        Matrice = NewCode & Sep & Matrice
    End If
ElseIf IdEntry > NumEntries(Matrice, Sep) Then 'Si on doit rajouter en dernier
    If Trim(Matrice) = "" Then
        Matrice = NewCode
    Else
        Matrice = Matrice & Sep & NewCode
    End If
Else
    '-> Il faut ins�rer dans la matrice
    For i = 1 To NumEntries(Matrice, Sep)
        If i = IdEntry Then
            '-> Doit on ins�rer dans la matrice
            If MatriceTemp = "" Then
                MatriceTemp = NewCode
            Else
                MatriceTemp = MatriceTemp & Sep & NewCode
            End If
        End If
        
        '-> Ajouter le code
        If MatriceTemp = "" Then
            MatriceTemp = Entry(i, Matrice, Sep)
        Else
            MatriceTemp = MatriceTemp & Sep & Entry(i, Matrice, Sep)
        End If
    Next 'Pour toutes les entr�es de la matrice
    
    '-> Affectation de la avariable temporaire
    Matrice = MatriceTemp
End If
    
'-> Renvoyer la matrice
AddEntryInMatrice = Matrice

End Function


Public Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Integer
Dim PosEnCour As Integer
Dim i As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
    

End Function

Public Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Integer

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Integer
Dim i As Integer
Dim PosAnalyse As Integer

On Error GoTo GestError

PosAnalyse = 1

Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1

Exit Function

GestError:

    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
    

End Function

Public Sub AddItemToList(ByVal Vecteur As String, ByVal aList As ListBox, Optional ByVal Selected As Integer)

'---> Cette fonction  alimente une liste avec toutes les entr�es de vecteur _
'S�parateur "|"

Dim i As Integer
Dim NbEntries As Integer

aList.Clear
NbEntries = NumEntries(Vecteur, "|")
For i = 1 To NbEntries
    aList.AddItem Entry(i, Vecteur, "|")
Next

'-> S�lectionner l'entr�e sp�cifi�e
If Selected <> 0 Then aList.Selected(Selected - 1) = True


End Sub

Public Function AddEntryToMatrice(ByVal Matrice As String, ByVal SepMatrice As String, ByVal EntryToAdd As String) As String

'---> cette fonction rajoute une entr�e dans une matrice. Elle renvoie la matrice mise � jour

'-> Ajouter l'entre�e dans la matrice
If Trim(Matrice) = "" Then
    Matrice = EntryToAdd
Else
    Matrice = Matrice & SepMatrice & EntryToAdd
End If

AddEntryToMatrice = Matrice

End Function

Public Function DeleteEntry(ByVal Matrice As String, ByVal EntryToDel As Integer, ByVal SepMatrice As String) As String

'---> Cette fonction supprime une entr�e dans la matrice. Elle renvoie la matrice mise � jour
Dim NbEntries As Integer
Dim i As Integer
Dim j As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> R�cup�rer le nombre d'entr�e
NbEntries = NumEntries(Matrice, SepMatrice)

'-> Renvoyer un chaine vide s'il n'y a qu'une seule entr�e
If NbEntries = 1 Then
    'Sil n'y a qu'une seule entr�e, vider la matrice
    DeleteEntry = ""
    Exit Function
ElseIf EntryToDel > NbEntries Then
    'Si on doit supprimer une entr�e qui n'existe pas -> renvoyer la matrice d'origine
    DeleteEntry = Matrice
    Exit Function
ElseIf EntryToDel = NbEntries Then
    'Si on doit supprimer la derni�re entr�e, chercher le s�parateur n-1
    i = InStrRev(Matrice, SepMatrice)
    DeleteEntry = Mid$(Matrice, 1, i - 1)
    Exit Function
ElseIf EntryToDel = 1 Then
    '-> Si on doit supprimer la premi�re entr�e, chercher le premier s�parateur
    i = InStr(1, Matrice, SepMatrice)
    DeleteEntry = Mid$(Matrice, i + 1, Len(Matrice) - i)
    Exit Function
Else
    '-> Suppression d'une entr�e
    j = 1
    i = 0
    NbEntries = 0
    Do
        i = InStr(j, Matrice, SepMatrice)
        NbEntries = NbEntries + 1
        If NbEntries = EntryToDel - 1 Then
            'Lecture de la position de d�but
            CHarDeb = i
            CharEnd = InStr(i + 1, Matrice, SepMatrice)
            DeleteEntry = Mid$(Matrice, 1, CHarDeb - 1) & Mid$(Matrice, CharEnd, Len(Matrice) - CharEnd + 1)
            Exit Function
        End If
        j = i + 1
    Loop
End If
    

End Function


Public Function GetEntryIndex(ByVal Matrice As String, ByVal EntryToSearch As String, ByVal SepMatrice As String) As Integer

'---> Cette fonction retourne l'index d'une valeur dans une matrice. _
Attention : Cette fonction retourne la premi�re it�ration trouv�e

On Error Resume Next

Dim i As Integer, j As Integer
Dim NbEntries As Integer

'-> Recherche de la pchaine de caract�re
i = InStr(1, UCase$(Matrice), UCase$(EntryToSearch))
j = 1
NbEntries = 1
'-> Analyse
Do While j < i
    j = InStr(j, Matrice, SepMatrice)
    NbEntries = NbEntries + 1
    j = j + 1
Loop

GetEntryIndex = NbEntries


End Function

Public Function IsLegalName(ByVal NewName As String, Optional Tiret As Boolean) As Boolean

'---> Fonction qui v�rifie le contenu d'un nom pour y supprimer tous les caract�res interdits
' Caract�res interdits : \ / : * " < > | -

Dim FindBad As Boolean


If InStr(1, NewName, "\") <> 0 Then FindBad = True
If InStr(1, NewName, "/") <> 0 Then FindBad = True
If InStr(1, NewName, ":") <> 0 Then FindBad = True
If InStr(1, NewName, "*") <> 0 Then FindBad = True
If InStr(1, NewName, """") <> 0 Then FindBad = True
If InStr(1, NewName, "<") <> 0 Then FindBad = True
If InStr(1, NewName, ">") <> 0 Then FindBad = True
If InStr(1, NewName, "|") <> 0 Then FindBad = True
If Tiret And InStr(1, NewName, "-") <> 0 Then FindBad = True

IsLegalName = Not FindBad


End Function

Public Function GetIniFileValue(ByVal Section As String, ByVal Keyname As String, IniFile As String, Optional GetSection As Boolean) As String

'---> Cette proc�dure r�cup�re le contenu d'une cl� dans un fichier ini

Dim Res As Long
Dim lpBuffer As String

'-> Init du buffer
lpBuffer = Space$(20000)

'-> Doit on r�cup�rer la section en entier
If GetSection Then
    Res = GetPrivateProfileSection&(Section, lpBuffer, Len(lpBuffer), IniFile)
Else
    Res = GetPrivateProfileString(Section, Keyname, "", lpBuffer, Len(lpBuffer), IniFile)
End If

If Res <> 0 Then
    lpBuffer = Mid$(lpBuffer, 1, Res)
Else
    lpBuffer = ""
End If

GetIniFileValue = lpBuffer


End Function

Public Function GetIntAscii(IntCode As Integer) As Integer

Select Case IntCode
    Case 8, 48 To 57
        GetIntAscii = IntCode
    Case Else
        IntCode = 0
End Select

End Function

Public Sub SelectTxtBox(ByRef aTxt As TextBox)

aTxt.SelStart = 0
aTxt.SelLength = Len(aTxt.Text)

End Sub

Public Function GetTempFileNameVB(ByVal Id As String, Optional Rep As Boolean) As String

Dim TempPath As String
Dim lpBuffer As String
Dim Result As Long
Dim TempFileName As String

'---> Fonction qui d�termine un nom de fichier tempo sous windows

'-> Recherche du r�pertoire temporaire
lpBuffer = Space$(500)
Result = GetTempPath(Len(lpBuffer), lpBuffer)
TempPath = Mid$(lpBuffer, 1, Result)

'-> Si on ne demande que le r�pertoire de windows
If Rep Then
    GetTempFileNameVB = TempPath
    Exit Function
End If

'-> Cr�ation d'un nom de fichier
TempFileName = Space$(1000)
Result = GetTempFileName(TempPath, Id, 0, TempFileName)
TempFileName = Entry(1, TempFileName, Chr(0))

GetTempFileNameVB = TempFileName

End Function

Public Function GetTempFileVB(ByVal Id As String, ByVal PathName As String) As String

'---> Cette fonction cr�er un fichier temporaire dans le �pertoire sp�cifi�

Dim Result As Long
Dim TempFileName As String

TempFileName = Space$(1000)
Result = GetTempFileName(PathName, Id, 0, TempFileName)
TempFileName = Entry(1, TempFileName, Chr(0))

GetTempFileVB = TempFileName

End Function


Public Function GetFileSizeVB(ByVal NomFichier As String) As Long

Dim hdlFile As Long
Dim Res As Long
Dim aOf As OFSTRUCT

'-> R�cup�ration des infos pour ProgressBar
hdlFile = OpenFile(NomFichier, aOf, OF_READ)
If hdlFile <> -1 Then
    Res = GetFileSize(hdlFile, 0)
    CloseHandle hdlFile
    GetFileSizeVB = Res
End If

End Function


Public Sub FormatListView(List As Object)

Dim i As Long
Dim x As Object

'-> Ne rien faire si pas de colonnes
If List.ColumnHeaders.Count = 0 Then Exit Sub

'-> De base toujours cr�er un enregistrement avec les entetes de colonnes
Set x = List.ListItems.Add(, "DEALENREGENTETE")

For i = 0 To List.ColumnHeaders.Count - 1
    '-> Ajouter le libelle de l'entete de la colonne
    If i = 0 Then
        x.Text = List.ColumnHeaders(1).Text
    Else
        x.SubItems(i) = List.ColumnHeaders(i + 1).Text
    End If
    SendMessage List.hwnd, LVM_SETCOLUMNWIDTH, i, 0
Next

'-> Supprimer le premier enregistrement
List.ListItems.Remove ("DEALENREGENTETE")

End Sub

