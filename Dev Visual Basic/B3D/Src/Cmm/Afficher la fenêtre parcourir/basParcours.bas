Attribute VB_Name = "fctParcourir"
Private Type BROWSEINFO
       hOwner As Long
       pidlRoot As Long
       pszDisplayName As String
       lpszTitle As String
       ulFlags As Long
       lpfn As Long
       lParam As Long
       iImage As Long
End Type

Private Declare Function SHGetPathFromIDList Lib "shell32.dll" Alias "SHGetPathFromIDListA" _
       (ByVal pidl As Long, ByVal pszPath As String) As Long

Private Declare Function SHBrowseForFolder Lib "shell32.dll" Alias "SHBrowseForFolderA" _
       (lpBrowseInfo As BROWSEINFO) As Long

'-> Afficher les répertoires
Private Const BIF_RETURNONLYFSDIRS = &H1
'-> Afficher les fichiers
Private Const BIF_BROWSEINCLUDEFILES = 16384

Public Function GetPathForm(ByVal Owner As Long, ByVal BrowseFile As Boolean) As String

'---> Cette fonction Affiche un répertoire

Dim x As BROWSEINFO
Dim Chemin As String
Dim pidl As Long
Dim RetVal As Long
Dim p As Integer

x.hOwner = Owner
x.pidlRoot = 0&
x.lpszTitle = "Selectionnez un répertoire"
x.ulFlags = BIF_RETURNONLYFSDIRS
If BrowseFile Then x.ulFlags = x.ulFlags Or BIF_BROWSEINCLUDEFILES

pidl& = SHBrowseForFolder(x)

Chemin = String(512, 0)
RetVal = SHGetPathFromIDList(pidl&, Chemin)
If RetVal Then
    p = InStr(Chemin, Chr$(0))
    GetPathForm = Left(Chemin, p - 1)
Else
    GetPathForm = ""
End If

End Function
