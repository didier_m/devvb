Attribute VB_Name = "fctBase"
Option Explicit

Public ADOCn As ADODB.Connection '-> Variable de connexion base
Public Top_Error As Integer '-> Gestion des erreurs
Public Nom_Base As String '-> Nom de la base � connecter : pass� en argument de fonction B3D init
Public Tempo_Lock As Long '-> D�lai de temporisation en Ms
Public Int_Error As String '-> Indique l'erreur VB pour les erreurs impr�vues
Public IsAccessBase As Boolean '-> Cette propri�t� indique que la base est de type ACCESS
Public AccessBase As Database '-> Pointeur vers la base au format ACCESS
Public Uc_Id As String '-> Chaine d'identification de la machine

Public Top_Debug As Boolean '-> Si on est en mode debug
Public DebugFile As String '-> Nom du fichier debug
Public hdlDebugFile As Integer '-> handle du fihier debug

'---> Pour gestion des conflits de lock
Public VecteurLock As String

'---> Pour gestion du mode debug
Public SQLValue As String

Public Function InitVecteurLock()

'---> Cett proc�dure r�cup�rer la variable de gestion des conflits de lock

Dim lpBuffer As String

'-> Get de la avriable dans le fichier Ini
lpBuffer = GetIniFileValue("LOCK", "VECTEUR", App.Path & "\Param\B3D.ini")

If lpBuffer = "" Then
    '-> Imposer une erreur si on est en mode debug
    If Top_Debug Then
        Call PrintDebug("Erreur fatale : Impossible de r�cup�rer le vecteur de gestion des conflits : " & lpBuffer & Chr(13) & Chr(10) & App.Path & "\Param\B3D.ini")
    End If
    End
End If

'-> Charger la variable
VecteurLock = lpBuffer

End Function

Public Function GetConflitVecteurLock(Erreur As Long) As Boolean

Dim i As Integer

For i = 1 To NumEntries(VecteurLock, ",")
    If CLng(Trim(Entry(i, VecteurLock, ","))) = Erreur Then
        GetConflitVecteurLock = True
        Exit Function
    End If
Next


End Function

Public Function OpenDebugFile()
'---> Ouverture du fichier de debug
hdlDebugFile = FreeFile
Open DebugFile For Output As #hdlDebugFile
End Function
Public Function PrintDebug(DebugToPrint As String)
'---> Enregistrer daqns le journal du debug
If Not Top_Debug Then Exit Function
Print #hdlDebugFile, DebugToPrint
End Function

Public Function CloseDebug()
If hdlDebugFile <> 0 Then Close #hdlDebugFile
hdlDebugFile = 0
End Function


Public Function OpenConnectB3D() As Boolean

'-> Lancer la connexion � la base de donn�es

Dim strCn As String '-> Pour construction de la chaine de connexion
Dim Driver As String 'Driver pour la connexion ADO
Dim Serveur As String 'Nom du serveur pour connexion ADO
Dim Uid As String 'Indentificateur de connexion
Dim Pwd As String 'Mot de passe de connexion
Dim Database_Name As String 'NOm de la base � connecter

'-> Pour lecture des fichiers ini
Dim res As Long
Dim lpBuffer As String
Dim DriverBase As String

On Error GoTo GestError

'-> Recup�ration du driver
lpBuffer = Space$(2000)
res = GetPrivateProfileString(Nom_Base, "Type", "NOT_FOUND", lpBuffer, Len(lpBuffer), IniPath & "\Bases.ini")
lpBuffer = Entry(1, lpBuffer, Chr(0))
lpBuffer = DeCrypt(lpBuffer)
If lpBuffer = "NOT_FOUND" Then
    Top_Error = 1
    GoTo Bad_End_Error
End If
'-> Affecter le type de base
DriverBase = lpBuffer

'-> R�cup�ration du d�lai de tempo
lpBuffer = Space$(2000)
res = GetPrivateProfileString(Nom_Base, "TEMPO_LOCK", "NOT_FOUND", lpBuffer, Len(lpBuffer), IniPath & "\Bases.ini")
lpBuffer = Entry(1, lpBuffer, Chr(0))
lpBuffer = DeCrypt(lpBuffer)
If lpBuffer = "NOT_FOUND" Then
    Top_Error = 6
    GoTo Bad_End_Error
End If
Tempo_Lock = CLng(lpBuffer)

'-> R�cup�ration du nom de la base
lpBuffer = Space$(2000)
res = GetPrivateProfileString(Nom_Base, "NOM", "NOT_FOUND", lpBuffer, Len(lpBuffer), IniPath & "\Bases.ini")
lpBuffer = Entry(1, lpBuffer, Chr(0))
lpBuffer = DeCrypt(lpBuffer)
If lpBuffer = "NOT_FOUND" Then
    Top_Error = 5
    GoTo Bad_End_Error
End If

'-> Setting de la variable
Database_Name = lpBuffer

'-> Traitement diff�rentiel vers une base access
If UCase$(Trim(DriverBase)) = "ACCESS" Then
    '-> R�cup�rer le fichier de base
    lpBuffer = DeCrypt(GetIniFileValue(Nom_Base, "FILE", IniPath & "\Bases.ini"))
    lpBuffer = Replace(lpBuffer, "$APP$", App.Path)
    '-> Lancer la connection Access
    If Not ConnectAccesBase(lpBuffer) Then
        '-> Envoyer le mode debug
        Call PrintDebug("Impossible de se connecter � la base. Fin du programme")
    Else
        '-> Renvoyer une valeur desucc�s
        OpenConnectB3D = True
    End If
    '-> Quitter la proc�dure
    Exit Function
End If
    

'-> Setting de la variable
Driver = lpBuffer
    
'-> Recup�ration du serveur
lpBuffer = Space$(2000)
res = GetPrivateProfileString(Nom_Base, "SERVEUR", "NOT_FOUND", lpBuffer, Len(lpBuffer), IniPath & "\Bases.ini")
lpBuffer = Entry(1, lpBuffer, Chr(0))
lpBuffer = DeCrypt(lpBuffer)
If lpBuffer = "NOT_FOUND" Then
    Top_Error = 2
    GoTo Bad_End_Error
End If

'-> Setting de la variable
Serveur = lpBuffer

'-> Recup�ration de l'identificateur de connexion
lpBuffer = Space$(2000)
res = GetPrivateProfileString(Nom_Base, "USER", "NOT_FOUND", lpBuffer, Len(lpBuffer), IniPath & "\Bases.ini")
lpBuffer = Entry(1, lpBuffer, Chr(0))
lpBuffer = DeCrypt(lpBuffer)
If lpBuffer = "NOT_FOUND" Then
    Top_Error = 3
    GoTo Bad_End_Error
End If

'-> Setting de la variable
Uid = lpBuffer

'-> Recup�ration du mot de passe
lpBuffer = Space$(2000)
res = GetPrivateProfileString(Nom_Base, "PWD", "NOT_FOUND", lpBuffer, Len(lpBuffer), IniPath & "\Bases.ini")
lpBuffer = Entry(1, lpBuffer, Chr(0))
lpBuffer = DeCrypt(lpBuffer)
If lpBuffer = "NOT_FOUND" Then
    Top_Error = 4
    GoTo Bad_End_Error
End If

'-> Setting de la variable
Pwd = lpBuffer

'-> Cr�ation de la chaine de connexion
strCn = "driver={" & Driver & "};Server=" & Serveur & ";uid=" & Uid & ";pwd=" & Pwd & ";Database=" & Database_Name

'-> Fermer la pr�c�dente connexion si elle ouverte
If Not ADOCn Is Nothing Then ADOCn.Close

'-> Initaliser la connection base
Set ADOCn = New ADODB.Connection

'-> Connecter la base
ADOCn.Open strCn
    
'-> DEBUGMODE
Call PrintDebug("OpenConnectB3D : Connexion base : " & strCn & " : -> OK")
    
'-> Indiquer que la connection n'est pas de type ACCESS
IsAccessBase = False
    
'-> Renvoyer une valeur de succ�s
OpenConnectB3D = True

Exit Function

GestError:

    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure de connexion OpenConnectB3D : " & Chr(13) & Chr(10) & "Erreur : " & Top_Error & Chr(13) & Chr(10) & Err.Number & " ->" & Err.Description)

    Int_Error = Err.Number & " - " & Err.Description
    Top_Error = 7

Bad_End_Error:

    OpenConnectB3D = False
    
End Function
Private Function ConnectAccesBase(strConnect As String) As Boolean

'---> Cette proc�dure est appel�e pour connecter une base

On Error GoTo GestError

'-> DEBUGMODE
Call PrintDebug("ConnectAccesBase : tentative de connexion � la base : " & strConnect)

'-> V�rifier que le fichier sp�cifi� existe
If Dir$(strConnect, vbNormal) = "" Then GoTo GestError

'-> Connecter la base access
Set AccessBase = OpenDatabase(strConnect)

'-> Positionner le marqueur de base
IsAccessBase = True

'-> Renvoyer une valeur de succ�s
ConnectAccesBase = True

'-> DEBUGMODE
Call PrintDebug("ConnectAccesBase : Connexion base OK")

'-> Quitter la fonction
Exit Function

GestError:

    '-> Renvoyer une valeur d'erreurt
    ConnectAccesBase = False
    
    '-> Lib�rer tout pointeur
    Set AccessBase = Nothing

End Function
Public Function CreateEnreg(ByVal strQuery As String) As Boolean

On Error GoTo GestError

If Not IsAccessBase Then
    ADOCn.Execute strQuery
Else
    AccessBase.Execute strQuery, dbFailOnError
End If

Exit Function

GestError:

    If GetConflitVecteurLock(Err.Number) Then
        Sleep (Tempo_Lock)
        Resume
    End If

    Call PrintDebug("Proc�dure Cr�ate Enreg Erreur dans la proc�dure : " & strQuery & Chr(13) & Chr(10) & Err.Number & "->" & Err.Description)

End Function



Public Function SetTable(ByVal strQuery As String) As Object

On Error GoTo Errorhandler

Dim recADO As Object
Dim recDAO As Object

'-> Cas d'attaque de base via ADO ( Chaine de connexion ODBC )
If Not IsAccessBase Then

    '-> Initialiser le recordset
    Set recADO = New ADODB.Recordset
    Set recADO.ActiveConnection = ADOCn
    
    '-> Propri�t�s de LOCK
    recADO.CursorType = adOpenDynamic
    recADO.LockType = adLockPessimistic
    
    '-> Ouvrir le recordset
    recADO.Open strQuery
    
    '-> Renvoyer le recordset de retour
    Set SetTable = recADO

Else 'Cas d'une base access
    
    '-> Pointer vers le recordset
    Set recDAO = AccessBase.OpenRecordset(strQuery)
    
    '-> Retourner le recordset
    Set SetTable = recDAO

End If

Exit Function

Errorhandler:

    If GetConflitVecteurLock(Err.Number) Then
        Sleep (Tempo_Lock)
        Resume
    End If
    
    Call PrintDebug("Erreur dans la proc�dure SET TABLE pour la requete : " & strQuery & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)
    Set SetTable = Nothing

End Function

Public Function GetDate() As String

Dim Jour As String
Dim Mois As String
Dim Annee As String

Jour = Format(Day(Now), "00")
Mois = Format(Month(Now), "00")
Annee = Format(Year(Now), "0000")

GetDate = Annee & Mois & Jour

End Function

Public Function GetTime() As String

Dim Heure As String
Dim Minutes As String
Dim Seconde As String

Heure = Format(Hour(Now), "00")
Minutes = Format(Minute(Time), "00")
Seconde = Format(Second(Now), "00")


GetTime = Heure & Minutes & Seconde

End Function
Public Function SetDate(ByVal strDate As String) As String

'---> Reformate une date dans le format europeen

Dim Annee As String
Dim Mois As String
Dim Jour As String

'-> Get de l'annee
Annee = Mid$(strDate, 1, 4)
Mois = Mid$(strDate, 5, 2)
Jour = Mid$(strDate, 7, 2)

SetDate = Jour & "/" & Mois & "/" & Annee

End Function

Public Function SetHeure(ByVal strHeure As String) As String

'---> Reformate une date dans le format europeen

Dim Heure As String
Dim Minute As String
Dim Seconde As String

'-> Get de l'annee
Heure = Mid$(strHeure, 1, 2)
Minute = Mid$(strHeure, 3, 2)
Seconde = Mid$(strHeure, 5, 2)

SetHeure = Heure & ":" & Minute & ":" & Seconde

End Function


Public Function Get_Total_Time() As String

Get_Total_Time = GetDate & GetTime

End Function

Public Function Get_NUM_ENREG(Id_Fname As String) As String


Dim aRec As Object
Dim NEnreg As String
Dim ErrorCode As String

On Error GoTo GestError

'-> Cr�er la requette
SQLValue = "SELECT * FROM [DEALB3D_ENREG] WHERE [ID_FNAME] = '" & Id_Fname & "�" & Agent_ID & "' AND [DAT_HISTO] = '99999999'"
'-> Mode DEBUG
Set aRec = SetTable(SQLValue)

If Not aRec Is Nothing Then
    '-> Tester si Il y a un enregistrement
    If aRec.BOF = True Then
        SQLValue = "INSERT INTO [DEALB3D_ENREG] ( ID_FNAME , NUM_ENREG , DAT_HISTO ) VALUES ( '" & _
                    UCase$(Id_Fname) & "�" & Agent_ID & "','" & CStr(Format(1, "0000000000000")) & "','99999999')"
        ErrorCode = "GET NUM_ENREG Mode cr�ation"
        CreateEnreg SQLValue
                    
        NEnreg = CStr(Format(1, "0000000000000"))
    Else
        '-> Recup la valeur
        NEnreg = Format(CStr(CInt(aRec.Fields("NUM_ENREG")) + 1), "0000000000000")
        '-> DEBUGMODE
        SQLValue = "UPDATE [DEALB3D_ENREG] SET [NUM_ENREG] =  '" & NEnreg & "' " & _
        "WHERE [ID_FNAME] = '" & Id_Fname & "�" & Agent_ID & "' AND [DAT_HISTO] = '99999999'"
        ErrorCode = "GET NUM_ENREG Mode update"
        CreateEnreg SQLValue
    End If
    
    '-> Retour de la valeur
    Get_NUM_ENREG = Uc_Id & Agent_ID & NEnreg
    'Get_NUM_ENREG = Agent_ID & NEnreg 'THIERRY
    
    '-> Fermer le query
    aRec.Close
End If

Exit Function

GestError:

    '-> Mode debug
    Call PrintDebug("Erreur dans la proc�dure Get_Num_ENREG " & Chr(13) & Chr(10) & Err.Number & Err.Description & Chr(13) & Chr(10) & SQLValue)
    '-> Gestion des conflits de lock
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (Tempo_Lock)
        Resume
    End If

End Function


Public Sub CloseConnectB3D()


'-> Debug
Call PrintDebug("Fermture de la connexion base � : " & Now)

'-> Selon que l'on soit en base access ou non
If IsAccessBase Then
    '-> Fermer la connexion DAO
    If Not AccessBase Is Nothing Then AccessBase.Close
Else
    '-> Fermer la connexion ADO
    If Not ADOCn Is Nothing Then ADOCn.Close
End If

End Sub




Public Function IsInameChampRef(Fname As String) As Boolean

'---> Analyse la table de correspondance associ�e � un champ pour chercher le champ de soci�t� de r�f�rence

Dim aIfield As Object
Dim aIname As Object
Dim aFname As Object
Dim SocRef As String

On Error GoTo GestError

'-> V�rifier qu'il y ait un param�trage de soci�t� de r�f�rence
SocRef = ServeurDOG.GetSystemTableValue("SOCREF")
If SocRef = "" Then Exit Function

'-> Pointer sur la table
Set aFname = ServeurDOG.Fnames(ServeurDOG.ServeurDOG_CurIdent & "|" & Fname)

'-> Se barrer si pas d'index de jointure
If aFname.IndexJointure = "" Then Exit Function

'-> Pointer sur le premier Index
Set aIname = aFname.Inames(aFname.IndexJointure)

'-> Analyser l'index pour voir si on trouve le champ d'acc�s sur groupe de r�f�rence
For Each aIfield In aIname.Ifields
    If aIfield.Num_Field = SocRef Then
        IsInameChampRef = True
        Exit Function
    End If
Next 'Pour tous les champs de l'index
    
'-> Pas d'acc�s sur soci�t� de r�f�rence dans cet index
IsInameChampRef = False
    
Exit Function

GestError:
    IsInameChampRef = False

End Function

Public Function FormatDate(strDate As String, Optional Anglo As Boolean) As String

'---> Formater une date Attention date et heure dans m�me zone si taille = 14

Dim aDate As String
Dim aTime As String
Dim tmpDate As String
Dim tmpTime As String

If Len(strDate) = 14 Then
    '-> Get de la date et de l'heure
    aDate = Mid$(strDate, 1, 8)
    aTime = Mid$(strDate, 9, 6)
ElseIf Len(strDate) = 6 Then
    '-> Get de l'heure
    aDate = ""
    aTime = strDate
Else
    '-> Get de la date
    aTime = ""
    aDate = strDate
End If

If Anglo Then '-> MM / JJ / AAAA
    '-> Si on doit formatter la date
    If aDate <> "" Then tmpDate = Mid$(aDate, 5, 2) & "/" & Mid$(aDate, 7, 2) & "/" & Mid$(aDate, 1, 4)
Else '-> JJ / MM / AAAA
    '-> Si on doit formatter la date
    If aDate <> "" Then tmpDate = Mid$(aDate, 7, 2) & "/" & Mid$(aDate, 5, 2) & "/" & Mid$(aDate, 1, 4)
End If

'-> Formatter l'heure
If aTime <> "" Then
    tmpTime = Mid$(aTime, 1, 2) & ":" & Mid$(aTime, 3, 2) & ":" & Mid$(aTime, 5, 2)
End If

'->  Retourner
FormatDate = Trim(tmpDate & " " & tmpTime)


End Function


Public Function Get_Error_Base() As String

'---> Cette fonction retourne le libell� d'une erreur d'acc&s � la base

Select Case Top_Error
    Case 1
        Get_Error_Base = "Erreur de connexion : Impossible de r�cup�rer le type de base associ�e � : " & Nom_Base
    Case 2
        Get_Error_Base = "Erreur de connexion : Impossible de r�cup�rer le serveur de base associ� � : " & Nom_Base
    Case 3
        Get_Error_Base = "Erreur de connexion : Impossible de r�cup�rer le user associ� � : " & Nom_Base
    Case 4
        Get_Error_Base = "Erreur de connexion : Impossible de r�cup�rer le mot de passe associ� � : " & Nom_Base
    Case 5
        Get_Error_Base = "Erreur de connexion : Impossible de r�cup�rer le nom de la base associ� � : " & Nom_Base
    Case 6
        Get_Error_Base = "Erreur de connexion : Impossible de r�cup�rer le d�lai de temporisation associ� � : " & Nom_Base
    Case 7
        Get_Error_Base = "Erreur impr�vue durant la connexion � la base : " & Nom_Base & " :  " & Int_Error
End Select

End Function
