VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IntField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Position As Integer '-> Indique soit l'entry � r�cup�rer si s�parateur ou la position de debut de lecture de la zone
Public Longueur As Integer '-> Longueur � lire
Public Num_Field As String '-> Code du champ aasoci� dans le catalogue
Public IsModif As Boolean '-> Si on accepte de modifier la valeur d'un champ s'il existe
Public Defaut As String
Public Table_Correspondance As String '-> Code de la table de correspondance � utiliser

