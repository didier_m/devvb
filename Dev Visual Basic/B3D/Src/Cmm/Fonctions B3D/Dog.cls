VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDOG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'*************************************
'* Classe d'acc�s au param�trage B3D *
'*************************************

'-> Collection DOG
Public Catalogue As Collection
Public fNames As Collection
Public SystemParams As Collection

'-> Compteurs d'objets
Public FnameCount As Long '-> Index du prochain num�ro de table
Public CatalogueCount As Long '-> Index du prochain objet du catalogue

'-> Fichiers associ�s
Public FichierCatalogue As String
Public FichierLink As String
Public FichierSchema As String

'-> Code de l'ident associ�
Public Ident As String
Public IdentIniFile As String


Public Function IsFname(ByVal Fname As String) As Boolean

'---> Cette Fonction v�rifi� si une table existe pour un ident

On Error GoTo GestError

Dim aFname As Fname

'-> Essayer de pointer usr la table
Set aFname = fNames(Ident & "|" & Fname)

IsFname = True

Exit Function

GestError:

    IsFname = False

End Function

Public Function IsInt(ByVal Fname As String, ByVal IntCode As String) As Boolean

'---> Cette Fonction v�rifi� si un interface existe pour un ident et une table donn�e

On Error GoTo GestError

Dim aFname As Fname
Dim aInt As Interface

'-> Essayer de pointer usr la table
Set aFname = fNames(Ident & "|" & Fname)

'-> Essayer de pointer sur l'interface
Set aInt = aFname.Interfaces("INT|" & IntCode)

IsInt = True

Exit Function

GestError:

    IsInt = False

End Function

Public Function AddFname(ByVal Num_Fname As String)

'---> Cette fonction cr�er une nouvelle table dans le descro de la base

Dim aFname As Fname
Dim lpBuffer As String

'-> Cr�er une nouvelle instance de la classe fname
Set aFname = New Fname

'-> Setting des propri�t�s
aFname.Ident = Ident
aFname.Iname_Count = "0"
aFname.iOrdre = 0
aFname.IsStat = True
aFname.Num_Fname = Num_Fname

'-> Ajout dans la collection
fNames.Add aFname, Ident & "|" & Num_Fname

'-> Incr�menter le compteur de champ dans le param�trage
Me.FnameCount = Me.FnameCount + 1

'-> Enregistrer la nouvelle valeur dans le fichier Ini
lpBuffer = CStr(Me.FnameCount)
WritePrivateProfileString Ident, "FNAME_COUNT", lpBuffer, IdentIniFile


End Function


Public Function AddField(ByVal FnameName As String, ByVal NumField As String)

'---> Cette fonction rajoute un champ dans une table
Dim aFname As Fname
Dim aField As Field
Dim aLinkFname As LinkFname
Dim aEmilieObj As EmilieObj

'-> Pointer sur la table associ�e
Set aFname = Me.fNames(Ident & "|" & FnameName)

'-> Cr�er une nouvelle instance du champ
Set aField = New Field

'-> Initialisation des prorpi�t�s
aField.Num_Field = NumField
aField.Ident = Ident
aField.Num_Fname = FnameName

'-> Ajouter le champ dans la table
aFname.Fields.Add aField, NumField

'-> Indiquer dans le catalogue que ce champ est li� � cette table
Set aLinkFname = New LinkFname
aLinkFname.IsIndexDirect = False
aLinkFname.IsChampRef = False
aLinkFname.Num_Fname = UCase$(FnameName)

'-> Pointer sur l'objet du catalogue
Set aEmilieObj = Me.Catalogue(Ident & "|" & NumField)

'-> Ajout dans la collection
aEmilieObj.fNames.Add aLinkFname, FnameName

End Function

Public Function DelField(ByVal NumFname As String, ByVal NumField As String)

'---> Cette proc�dure supprime un champ d'un fichier sp�cifi�


Dim aEmilieObj As EmilieObj
Dim aFname As Fname

'-> Pointer sur la table
Set aFname = fNames(Ident & "|" & NumFname)

'-> Supprimer le champ dans la table
aFname.Fields.Remove (NumField)

'-> Pointer sur l'objet du catalogue
Set aEmilieObj = Me.Catalogue(Ident & "|" & NumField)

'-> Supprimer la r�f�rence dans l'objet Emilie associ�
aEmilieObj.fNames.Remove (NumFname)


End Function
Public Function AddIname(ByVal Num_Fname As String) As Iname

'---> Cette fonction ajoute un index dans une table

Dim aFname As Fname
Dim aIname As Iname

'-> Pointer sur la table associ�e
Set aFname = Me.fNames(Ident & "|" & Num_Fname)

'-> Cr�er le nouvel index
Set aIname = New Iname
aIname.Ident = Ident
aIname.iOrdre = aFname.Iname_Count + 1
aFname.Iname_Count = aFname.Iname_Count + 1
aIname.Num_Iname = Format(aIname.iOrdre, "000")
aIname.Num_Fname = aFname.Num_Fname

'-> Ajouter dans la collection des index
aFname.Inames.Add aIname, aIname.Num_Iname

'-> Retourner le pointeur
Set AddIname = aIname

End Function


Public Function AddIfield(ByVal Num_Field As String, ByVal Num_Fname As String, ByVal Num_Iname)

'---> Cette fonction cr�er un nouveau champ d'index
Dim aIfield As iField
Dim aFname As Fname
Dim aIname As Iname

On Error GoTo GestError

'-> Pointer sur la tabel associ�e
Set aFname = Me.fNames(Ident & "|" & Num_Fname)

'-> Pointer sur l'index associ�
Set aIname = aFname.Inames(Num_Iname)

'-> Cr�er une nouvelle instance de la classe iField
Set aIfield = New iField

'-> Ajouter le champ dans la liste des champs de la table
aIfield.Num_Field = Num_Field
aIfield.Num_Fname = Num_Fname
aIfield.Num_Iname = Num_Iname
aIfield.Ident = Ident

'-> Ajouter l'objet dans la collection
aIname.iFields.Add aIfield, Num_Field

Exit Function

GestError:

    MsgBox "Erreur dans l'ajout d'un champ"

End Function

Public Function AddLinkIname(ByVal EmilieObj As String, ByVal Num_Fname As String, ByVal Num_Iname As String)

'---> Cette fonction rajoute un nouveau lien d'affectation vers un index

Dim aLinkFname As LinkFname
Dim aLinkIname As LinkIname
Dim aEmilieObj As EmilieObj

'-> Pointer sur l'objet Emilie
Set aEmilieObj = Me.Catalogue(Ident & "|" & EmilieObj)

'-> Pointer sur le line d'affectation table
Set aLinkFname = aEmilieObj.fNames(Num_Fname)

'-> Cr�er une nouvelle instance du lien d'affectation vers un index
Set aLinkIname = New LinkIname

'-> Setting des propri�t�s
aLinkIname.Num_Iname = Num_Iname

'-> Ajout de l'objet dans la collection des lines index de la table
aLinkFname.LinkInames.Add aLinkIname, Num_Iname

End Function


Public Function AddInterface(ByVal IntName As String, ByVal Fname As String)

'---> Proc�dure qui ajoute un interface dans le sch�ma

Dim aInt As Interface
Dim aFname As Fname

'-> Pointer sur la table associ�e
Set aFname = fNames(Ident & "|" & Fname)

'-> Init
Set aInt = New Interface

'-> Setting
aInt.Name = IntName

'-> AJout dans la collection
aFname.Interfaces.Add aInt, "INT|" & UCase$(IntName)

End Function

Public Function AddIntField(ByVal FnameName As String, ByVal IntName As String, ByVal Num_Field As String)

'---> Proc�dure qui ajoute un champ dans un interface

Dim aInt As Interface
Dim aIntField As IntField
Dim aFname As Fname

'-> Pointer sur la table associ�e
Set aFname = Me.fNames(Ident & "|" & FnameName)

'-> Pointer sur l'interface associ�
Set aInt = aFname.Interfaces("INT|" & IntName)

'-> Cr�ation d'une nouvelle instance d'un objet IntField
Set aIntField = New IntField

'-> Setting des prorpi�t�s
aIntField.Num_Field = Num_Field

'-> Ajout dans la collection
aInt.Fields.Add aIntField, Num_Field


End Function

Public Function AddLinkInterface(ByVal Fname As String, ByVal IntName As String, ByVal Num_Field As String)

'---> Cette fonction ajoute un lien dans l'objet du catalogue

Dim aEmilieObj As EmilieObj
Dim aLink As LinkInterface
Dim aLinkFname As LinkFname


'-> Pointer sur l'objet du catalogue
Set aEmilieObj = Me.Catalogue(Ident & "|" & Num_Field)

'-> Pointer sur le lien vers la table en cours
Set aLinkFname = aEmilieObj.fNames(Fname)

'-> Cr�er une nouvelle instance d'un objet Lien
Set aLink = New LinkInterface

'-> Setting de sa propri�t�
aLink.Name = IntName

'-> AJouter dans la collection des liens de la table
aLinkFname.LinkInterfaces.Add aLink, IntName


End Function

Public Function GetEmilieNumField() As String

'---> Cette fonction retourne l'index du prochain objet Emilie

GetEmilieNumField = Format(Me.CatalogueCount, "00000")

End Function

Public Function IsFieldRefFname(NumField As String, NumFname As String) As Boolean

'-> Cette fonction v�rifie qu'un champ n'est plus r�f�renc� dans une table

Dim aLinkFname As LinkFname
Dim aEmilieObj As EmilieObj

'-> Pointer sur l'objet du catalogue associ�
Set aEmilieObj = Me.Catalogue(Ident & "|" & NumField)

'-> Pointer vers l'objet lien attach�
Set aLinkFname = aEmilieObj.fNames(NumFname)

'-> V�rif des index
If aLinkFname.LinkInames.Count <> 0 Then Exit Function

'-> V�rif des interfaces
If aLinkFname.LinkInterfaces.Count <> 0 Then Exit Function

'-> Si pas r�f�renc� en index direct
If aLinkFname.IsIndexDirect Then Exit Function

'-> Pas de r�f�rence : on peut supprimer
IsFieldRefFname = True

End Function



Public Function AddEmilieObj(ByVal Num_Field As String)

'---> Cette fonction ajoute un objet dans le catalogue de l'ident en cours

Dim aEmilieObj As EmilieObj
Dim lpBuffer As String

'-> Cr�er une nouvelle variable
Set aEmilieObj = New EmilieObj

'-> Initialiser ses propri�t�s
aEmilieObj.Num_Field = Num_Field
aEmilieObj.Ident = Ident

'-> Ajouter l'objet dans la collection
Me.Catalogue.Add aEmilieObj, Ident & "|" & Num_Field

'-> Incr�menter le compteur de champ dans le param�trage
Me.CatalogueCount = Me.CatalogueCount + 1

'-> Enregistrer la nouvelle valeur dans le fichier Ini
lpBuffer = CStr(Me.CatalogueCount)
WritePrivateProfileString Ident, "EMILIE_COUNT", lpBuffer, IdentIniFile

End Function

Public Sub Save_Catalogue()

'---> Cette fonction lance l'enregistrement du fichier du catalogue
Call SaveCatalogueFile(Me, FichierCatalogue)

End Sub

Public Sub Save_Schema()

'---> Cette proc�dure lance la sauvegarde du schema B3D ainsi que des liens

'-> Enregistrer le schema b3D
Call SaveDictionnaireB3D(Me, FichierSchema)

'-> Enregistrer les liens entre le schema et le catalogue
Call SaveCatalogueLink(Me, FichierLink)

End Sub

Public Function GetSystemTable() As String

'---> Cette proc�dure retourne le contenu de la table system

Dim strRetour As String
Dim aSys As clsSystemParam

For Each aSys In Me.SystemParams
    If strRetour = "" Then
        strRetour = aSys.Key & "=" & aSys.Value
    Else
        strRetour = strRetour & Chr(0) & aSys.Key & "=" & aSys.Value
    End If
Next

'-> Retourner la table
GetSystemTable = strRetour


End Function

Public Function GetSystemTableValue(ValueToSearch As String) As String

'---> Cette proc�dure analyse la table de param�trage syst�me

Dim aSys As clsSystemParam

On Error GoTo GestError

'-> Essayer de pointer sur l'objet
Set aSys = Me.SystemParams("SYS|" & Trim(UCase$(ValueToSearch)))

'-> Retourner la valeur
GetSystemTableValue = aSys.Value

Exit Function

GestError:
    GetSystemTableValue = ""

End Function

Private Sub Class_Initialize()

'-> Initialiser les collections
Set Catalogue = New Collection
Set fNames = New Collection
Set SystemParams = New Collection

End Sub
