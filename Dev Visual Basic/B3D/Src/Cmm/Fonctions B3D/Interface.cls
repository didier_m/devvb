VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Interface"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Name As String 'Nom de l'interface
Public Intput_Output As Integer '0 -> Input , 1 -> Output
Public UseSeparateur As Boolean '-> Indique si on utilise un s�parateur ou si on est en position / Longueur
Public Separateur As String 'S�parateur utilis�
Public CodeLangue As Integer 'Code langue pour les libelles
Public UseIndex As Boolean '-> Indique si on utilise un index pour v�rifier si un enreg existe
Public Index As String '-> Code de l'index de v�rification

Public Tolerance As Single '-> % de tol�rance sur les rejets ( non actif )

Public Fields As Collection

Private pDesignation(1 To 10) As String

Public Property Get Designation() As String
    Designation = pDesignation(CodeLangue)
End Property

Public Property Let Designation(ByVal vNewValue As String)
    pDesignation(CodeLangue) = vNewValue
End Property

Private Sub Class_Initialize()

'-> Initialiser la collection des champs attach�s � l'interface
Set Fields = New Collection

End Sub
