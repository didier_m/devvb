VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsConnectConsole"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'-> Propri�t�s aliment�es par l'interface
Public Ident As String '-> Serveur B3D de connexion
Public Operat As String '-> Code de l'op�rateur connect�
Public TraceMode As Boolean '-> Nideau de trace demand� pour cet agent
Public CodeLangue As Integer '-> Code Langue de l'op�rateur de connexion
Public DebugMode As Boolean '-> Indique si l'agent est lanc� en mode debug ou non
Public CryptMode As Boolean '-> Indique si les �changes sont crypt�s
Public IsWorking As Boolean '-> Indique que l'agent est en train de travailler
Public IdSession As Long '-> Pour Identificateur de session si connexion r�ussie
Public IndexAgent As Integer '-> Code de l'agent b3D attach�
Public DateConnexion As String '-> Date de connexion
Public Num_Idx As String '-> Num�ro de l'agent Idx attach� � ce process
Public hdlProcess As Long  '-> handle du process NT
Public IsRunConsole As Boolean '-> Indique que cet agent est lanc� depuis la console
Public EndProg As Boolean '-> Indique que l'agent B3D est mort
Public DmdFile As String '-> Emplacement du fichier d'�change pour Dsiconnect
Public RspFile As String '-> Emplacement du fichier R�ponse

Public Function DisConnect() As Boolean

Dim aCon As B3DConnexion

'-> Pointer sur l'objet de connexion associ�
Set aCon = Connexions("CON|" & IndexAgent)

'-> Lancer la d�connexion
If Not aCon.DisConnect Then 'MESSPROG
    MsgBox "Erreur lors de la d�connexion de l'agent : " & Chr(13) & aCon.ErrorNumber & " - " & aCon.ErrorLibel, vbCritical + vbOKOnly, "Erreur"
    Exit Function
End If

'-> Renvoyer une valeur de succ�s
DisConnect = True

End Function
