VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "B3DConnexion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'*****************************************************
'* Classe de connexion et d'�change avec l'agent B3D *
'*****************************************************

Option Explicit

'-> Variable d'erreur
Public ErrorNumber As String
Public ErrorLibel As String
Public ErrorParam As String

'-> R�pertoires de travail
Public DmdPath As String ' Demandes
Public RspPath As String 'R�ponses

'-> Variable d'acc�s au fichier ASCII
Private hdlFile As Integer
Private TempFileName As String

'-> Pour gestion des propri�t�s en lecture seule
Private pOperateur As String 'Code op�rateur
Private pPassWord As String 'Mot de passe
Private pCodeLangue As Integer 'CodeLangue
Private pIdent As String 'code client
Private pIdAgent As String 'Agent B3D
Private pIdSession As String 'Identificateur de session
Private pUc_Id As String 'Num�ro d'UC
Private pDmdFile As String 'Fichier Demande
Private pRspFile As String 'Fichier R�ponse
Private pIsConnected As Boolean 'Indique si on est connect�
Private pIsWorking As Boolean 'ndique que l'agent affect� est en train de travailler
Public Property Get IsWorking() As Boolean
    IsWorking = pIsWorking
End Property

Public Property Get IsConnected() As Boolean
    IsConnected = pIsConnected
End Property
Public Property Get Operateur() As String
    Operateur = pOperateur
End Property
Public Property Get PassWord() As String
    PassWord = pPassWord
End Property
Public Property Get CodeLangue() As Integer
    CodeLangue = pCodeLangue
End Property
Public Property Get Ident() As String
    Ident = pIdent
End Property
Public Property Get IdAgent() As String
    IdAgent = pIdAgent
End Property
Public Property Get IdSession() As String
    IdSession = pIdSession
End Property
Public Property Get Uc_Id() As String
    Uc_Id = pUc_Id
End Property
Public Property Get DmdFile() As String
    DmdFile = pDmdFile
End Property
Public Property Get RspFile() As String
    RspFile = pRspFile
End Property


Private Function Wait_File(ByVal FileToWait As String, ByVal Tempo As Long, NbTempo As Integer) As Boolean

'---> Cette fonction de met en attente d'un fichier ASCII

'Tempo -> Wait en mili second entre 2 appels
'NbTempo -> Nombre d'appels

Dim i As Integer
Dim Wait_Infini As Boolean

If NbTempo = 0 Then Wait_Infini = True
Do
    '-> Rendre la main � la cpu
    DoEvents
    '-> Analyse du fichier
    If Dir$(FileToWait, vbNormal) <> "" Then
        Wait_File = True
        Exit Function
    Else
        i = i + 1
        If Not Wait_Infini Then
            If i = NbTempo Then
                Wait_File = False
                Exit Function
            End If
        End If
        Sleep (Tempo)
        DoEvents
    End If
Loop

End Function

Public Function InitDmd(Optional Connecting As Boolean) As Boolean

'---> Cette proc�dure initialisa une demande en cr�ant un fichier temporaire

On Error GoTo GestError

If Not Connecting Then
    '-> Ne rien faire si on n'est pas connect�
    If Not IsConnected Then
        '-> Positionner la variable d'erreur
        ErrorNumber = "100003"
        ErrorLibel = "Class not connected"
        Exit Function
    End If
    
    '-> Supprimer des traces d'�change
    If Dir$(RspFile) <> "" Then Kill RspFile
    If Dir$(DmdFile) <> "" Then Kill DmdFile
    
End If

'-> R�cup�rer un handle de fichier
hdlFile = FreeFile

'-> Nom de fichier temporaire
TempFileName = DmdFile & ".tmp"

'-> Ouverture du fichier
Open TempFileName For Output As #hdlFile

'-> Renvoyer une valeur de succ�s
InitDmd = True

'-> Quitter la fonction
Exit Function

GestError:
    
End Function

Public Function PutInstruction(Instruction As String, Optional Connecting As Boolean) As Boolean

'---> Cette  proc�dure rajoute une instruction dans le fichier de demande en attente

On Error GoTo GestError

'-> Ne pas faire le test si on est en cours de connexion
If Not Connecting Then
    '-> Ne rien faire si on n'est pas connect�
    If Not IsConnected Then
        '-> Positionner la variable d'erreur
        ErrorNumber = "100003"
        ErrorLibel = "Class not connected"
        Exit Function
    End If
End If

'-> Inscrire l'instruction
Print #hdlFile, Instruction

'-> Renvoyer une valeur de succ�s
PutInstruction = True

'-> Quitter la fonction
Exit Function

GestError:

End Function

Public Function SendDemande(aBuffer As B3DBuffer, Optional NoWait As Boolean) As Boolean

'---> Cette fonction envoie un fichier temporaire, se met en attente de la r�ponse, et lit le fichier retour

On Error GoTo GestError

Dim Ligne As String
Dim ErrLig As Integer

'-> Quitter si on n'est pas connect� � l'agent
If Not IsConnected Then Exit Function

'-> Quitter la fonction si le buffer n'est pas d�fini
If aBuffer Is Nothing Then Exit Function

'-> Quitter si l'agent est d�ja en train de travaille
If Me.IsWorking Then Exit Function

'-> Fermer le fichier de demande temporaire
Close #hdlFile

'-> Indiquer que l'agent est en train de travaille
pIsWorking = True

'-> V�rifier s'il n'y a pas une demande en cours
If Dir$(RspFile) <> "" Then Kill RspFile

'-> Le renommer sous forme de demande
ErrLig = 1
Name TempFileName As DmdFile
ErrLig = 0

'-> Se mettre en attente de la r�ponse si necessaire
If NoWait Then
    '-> Renvoyer une valeur de succ�s
    SendDemande = True
    '-> Indiquer qu'il ne travaille plus
    pIsWorking = False
    '-> Quitter la fonction
    Exit Function
End If

'-> Boucler en attente de la r�ponse
Wait_File RspFile, 10, 0
 
'-> Lire la r�ponse
If Not ReadReponse(RspFile, aBuffer) Then
    '-> Indiquer qu'il ne travaille plus
    pIsWorking = False
    Exit Function
End If

'-> Indiquer que l'agent n'est plus en train de travaille
pIsWorking = False

'-> Renvoyer une valeur de succ�s
SendDemande = True

'-> Quitter la fonction
Exit Function

GestError:
    
    '-> Indiquer qu'il ne travaille plus
    pIsWorking = False
        
End Function

Private Function ReadReponse(FileToRead As String, aBuffer As Object) As Boolean

Dim Ligne As String
Dim ErrLig As Integer

On Error GoTo GestError

If Not IsConnected Then Exit Function

'-> Tester que le buffer soit initialis�
If aBuffer Is Nothing Then Exit Function

'-> V�rifier que l'on trouve le fichier ASCII
If Dir$(FileToRead) = "" Then Exit Function
    
'-> Vider la matrice de r�ponse du buffer de desctination
ErrLig = 1
Do While aBuffer.Lignes.Count <> 0
    aBuffer.Lignes.Remove (1)
Loop
Set aBuffer.Lignes = New Collection

'-> Ouvrir le fichier ASCII de r�ponse et charger la matrice des lignes
ErrLig = 2
hdlFile = FreeFile
Open RspFile For Input As #hdlFile
Do While Not EOF(hdlFile)
    '-> Lecture du fichier ASCII
    Line Input #hdlFile, Ligne
    If Trim(Ligne) <> "" Then aBuffer.Lignes.Add DeCrypt(Ligne)
Loop

'-> Fermer le fichier ASCII
Close #hdlFile

'-> Supprimer le fichier ASCII
ErrLig = 3
Kill RspFile

'-> Renvoyer une valeur de succ�s
ReadReponse = True

'-> Quitter la proc�dure
Exit Function

GestError:
    

End Function

Public Function Connect(Operat As String, Pwd As String, IdentCli As String, DebugMode As String, TraceMode As String, CryptMode As String, Optional ConsoleMode As Boolean) As Boolean

'--> Cette proc�dure cr�er un fichier de demande de connexion

Dim CnRFile As String
Dim CnXFile As String
Dim Ligne As String
Dim Temp As String
Dim ErrLig As Integer

On Error GoTo GestError

'-> Ne rien faire si on est d�ja connect�
If Me.IsConnected Then Exit Function

'-> Initialiser la cr�ation du fichier ASCII
If Not InitDmd(True) Then Exit Function

'-> Initialiser les fichiers d'�change
Temp = CStr(GetTickCount())
CnRFile = RspPath & Temp & ".cnr"
CnXFile = DmdPath & Temp & ".cnx"

'-> Cr�ation de la cha�ne de connexion
If ConsoleMode Then
    If Not PutInstruction(Crypt(Operat & "�" & Pwd & "�" & IdentCli & "�" & DebugMode & "�1�" & TraceMode & "�" & CryptMode & "�" & Temp), True) Then Exit Function
Else
    If Not PutInstruction(Crypt(Operat & "�" & Pwd & "�" & IdentCli & "�" & DebugMode & "�0�" & TraceMode & "�" & CryptMode & "�" & Temp), True) Then Exit Function
End If

'-> Fermer le fichier temporaire
Close #hdlFile

'-> Renommer le fichier et se mettre en attente de la r�ponse
ErrLig = 1
Name TempFileName As CnXFile
DoEvents

'-> Attendre la r�ponse 1 minute
Wait_File CnRFile, 1000, 10

'-> si on n'pas trouv� le fichier renvoyer une valeur d'erreur
If Dir$(CnRFile, vbNormal) = "" Then
    '-> Positionner la variable d'erreur
    ErrorLibel = "Unable to connect Serveur"
    '-> V�rifier si le fichier CNR existe bien
    If Dir$(CnXFile, vbNormal) <> "" Then Kill CnXFile
    '-> Quitter la fonction
    Exit Function
End If

'-> Lire la premi�re ligne du fichier de connexion
ErrLig = 2
hdlFile = FreeFile
Open CnRFile For Input As #hdlFile
Line Input #hdlFile, Ligne

'-> Fermer le fichier et le supprimer
ErrLig = 3
Close #hdlFile
Kill CnRFile

'-> D�crypter la premi�re ligne
DeCryptLig:
Ligne = DeCrypt(Ligne)
If Trim(Ligne) = "" Then Exit Function

'-> Analyse de la ligne
If Entry(2, Ligne, "�") = "ERROR" Then
    '-> Positionner la variable d'erreur
    ErrorLibel = Entry(3, Ligne, "�")
    '-> Indiquer que la connexion a �chou�e
    Connect = False
    '-> Quitter la fonction
    Exit Function
End If

'-> Setting des propri�t�s
pOperateur = Operat
pPassWord = Pwd
pIdent = IdentCli

'-> La connexion a r�ussi� : positionner les variables internes d'�change
pIdAgent = Entry(3, Ligne, "�")
pUc_Id = Entry(4, Ligne, "�")
pIdSession = Entry(5, Ligne, "�")
pCodeLangue = CInt(Entry(6, Ligne, "�"))

'-> Setting de ses fichiers d'�change
pDmdFile = DmdPath & pIdSession & ".dmd"
pRspFile = RspPath & pIdSession & ".rsp"

'-> Indiquer que l'agent est connect�
pIsConnected = True

'-> Renvoyer une valeur de succ�s
Connect = True

'-> Quitter la fonction
Exit Function

GestError:
    Select Case ErrLig
        Case 1
            ErrorLibel = "Unable to send connexion"
        Case 2
            ErrorLibel = "Error reading file"
        Case 3
            GoTo DeCryptLig
    End Select
End Function

Public Function DisConnect() As Boolean

'---> Cette proc�dure d�connecte un agent B3D

Dim ErrLig As Integer

On Error GoTo GestError

If Not IsConnected Then Exit Function

'-> Quitter si l'agent est d�ja en train de travaille
If Me.IsWorking Then Exit Function

'-> Initialiser une demande
If Not InitDmd Then Exit Function

'-> Positionner l'instruction
PutInstruction "B3D_EXIT~"

'-> Fermer le fichier ASCII
Close #hdlFile

'-> Envoyer l 'ordre de fin
ErrLig = 1
Name TempFileName As DmdFile

'-> Indiquer que l'agent n'est plus connect�
pIsConnected = False

'-> Renvoyer une valeur de succ�s
DisConnect = True

'-> Quitter la fonction
Exit Function

GestError:
    If ErrLig = 1 Then
        ErrorLibel = "Unable to send exchange"
    Else
        ErrorLibel = Err.Description
    End If

End Function


Public Function ExportParamToFile() As String

'---> Cette proc�dure enregistre le pram�trage de la connexion en cours dans un fichier ASCII _
pour qu'il puisse �tre r�cup�r� depuis r�cup�r� par un autre programme

On Error GoTo GestError

'-> Ne rien faire si l'agent n'est pas connect�
If Not IsConnected Then Exit Function
    
'-> Ouvrir un fichier temporaire
hdlFile = FreeFile
TempFileName = GetTempFileNameVB("DMD")
Open TempFileName For Output As #hdlFile

'-> Entete de la section
Print #hdlFile, "[PARAM]"

'-> Ident
Print #hdlFile, "IDENT=" & Crypt(Me.Ident)
'-> Operateur
Print #hdlFile, "OPERAT=" & Crypt(Me.Operateur)
'-> Mot de passe
Print #hdlFile, "PWD=" & Crypt(Me.PassWord)
'-> Code langue
Print #hdlFile, "LANGUE=" & Crypt(CStr(Me.CodeLangue))
'-> Fichier demande
Print #hdlFile, "DMD=" & Crypt(Me.DmdFile)
'-> Fichier R�ponse
Print #hdlFile, "RSP=" & Crypt(Me.RspFile)
'-> Index de l'agent
Print #hdlFile, "IDAGENT=" & Crypt(Me.IdAgent)
'-> Uc_ID
Print #hdlFile, "UC_ID=" & Crypt(Me.Uc_Id)
'-> Index de session
Print #hdlFile, "IDSESSION=" & Crypt(Me.IdSession)

'-> Fermer le fichier temporaire
Close #hdlFile

'-> Renvoyer une valeur de succ�s
ExportParamToFile = TempFileName

'-> Quitter la fonction
Exit Function

GestError:
    '-> Positionner le code erreur
    ErrorNumber = Err.Number
    ErrorLibel = Err.Description

End Function

Public Function ReadParamFormFile(FileName As String) As Boolean

'---> Cette fonction sert � initialiser une connexion avec un agent B3D _
en partant d'une connexion existante qui a export� son param�trage dans un fichier temporaire

On Error GoTo GestError

'-> V�rifier que la classe ne soit pas d�ja connect�e
If Me.IsConnected Then Exit Function
    
'-> V�rifier si on trouve le fichier
If Dir$(FileName) = "" Then Exit Function

'-> Lecture des param�tres du fichier ASCII
pIdent = DeCrypt(GetIniFileValue("PARAM", "IDENT", FileName))
pOperateur = DeCrypt(GetIniFileValue("PARAM", "OPERAT", FileName))
pPassWord = DeCrypt(GetIniFileValue("PARAM", "PWD", FileName))
pCodeLangue = CInt(DeCrypt(GetIniFileValue("PARAM", "LANGUE", FileName)))
pIdAgent = DeCrypt(GetIniFileValue("PARAM", "IDAGENT", FileName))
pUc_Id = DeCrypt(GetIniFileValue("PARAM", "UC_ID", FileName))
pIdSession = DeCrypt(GetIniFileValue("PARAM", "IDSESSION", FileName))
pDmdFile = DeCrypt(GetIniFileValue("PARAM", "DMD", FileName))
pRspFile = DeCrypt(GetIniFileValue("PARAM", "RSP", FileName))

'-> Indiquer que la classe de connexion est connect�
pIsConnected = True

'-> Renvoyer une valeur de succ�s
ReadParamFormFile = True

'-> Quitter la fonction
Exit Function

GestError:
    '-> Positionner la variable d'erreur
    ErrorNumber = Err.Number
    ErrorLibel = Err.Description
    
End Function
