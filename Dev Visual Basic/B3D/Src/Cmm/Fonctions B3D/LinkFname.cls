VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LinkFname"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'-> Indique le Fname rattach�
Public Num_Fname As String

'-> Indique qu'il est rattach� � un index directe
Public IsIndexDirect As Boolean

'-> Indique qu'il est rattach� � un champ de r�f�rence
Public IsChampRef As Boolean

'-> Collection des index dans le quel ce champ est r�f�renc�
Public LinkInames As Collection

'-> Collection des interfaces dans lequel le champ est r�f�renc�
Public LinkInterfaces As Collection



Private Sub Class_Initialize()

'-> Initialiser la classe des inames associ�s
Set LinkInames = New Collection

'-> Initialiser la collection des interfaces associ�s
Set LinkInterfaces = New Collection

End Sub


