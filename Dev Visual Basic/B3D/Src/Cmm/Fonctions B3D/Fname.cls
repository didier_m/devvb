VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FNAME"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Ident As String

Public Name As String

Public IsReplica As Boolean
Public IsStat As Boolean

'-> Indique s'il y a un champ r�f�rence
Public IsChampRef As Boolean

'-> Indique s'il y a un index direct
Public IndexDirect As String

'-> Sert � indiquer le niveau de l'ordre en cours
Public iOrdre As Long

Private pDesignation(10) As String
Public CodeLangue As Integer

'-> Liste des champs associ�s � fname
Public Fields As Collection

'-> Liste des index associ�s � un Fname
Public Inames As Collection

'-> Liste des interfaces associ�s � une table
Public Interfaces As Collection

'-> Index jointure
Public IndexJointure As String

'-> Indique si on doit imprimer
Public ToPrint As Boolean

'-> Table de reference
Public ChampRef As String 'ATTENTION NE PLUS UTILISE : DEPORTE DANS CATALOGUE -> MasterField 16/01/2002

'-> Compteur d'index
Public Iname_Count As String

'-> Numero de la table
Public Num_Fname As String

'-> Pour mise � jour de la table DEALB3D_ENREG table des stats
Public Fonction_Create As Long
Public Fonction_Update As Long
Public Fonction_Delete As Long
Public Fonction_MAJ As Boolean

'-> Indique si la table est en cours de visualisation
Public hdlProp As Long


Public Property Get Designation() As String
    Designation = pDesignation(CodeLangue)
End Property

Public Property Let Designation(ByVal vNewValue As String)
    pDesignation(CodeLangue) = vNewValue
End Property

Private Sub Class_Initialize()

Set Fields = New Collection
Set Inames = New Collection
Set Interfaces = New Collection

CodeLangue = 1

End Sub
