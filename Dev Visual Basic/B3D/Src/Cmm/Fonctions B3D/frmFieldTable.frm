VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmFieldTable 
   BorderStyle     =   5  'Sizable ToolWindow
   ClientHeight    =   4485
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   4605
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   299
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   307
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Cancel          =   -1  'True
      Caption         =   "Fermer"
      Height          =   495
      Left            =   3480
      TabIndex        =   1
      Top             =   3960
      Width           =   975
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   6800
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmFieldTable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
Unload Me
End Sub

Private Sub Form_Load()

'-> Vider la variable d'�change
strRetour = ""
Me.ListView1.ZOrder

End Sub

Private Sub Form_Resize()

On Error Resume Next

Dim aRect As RECT

'-> Get de la zone cliente de la fen�tre
GetClientRect Me.hwnd, aRect

'-> Redimensionner
Me.ListView1.Left = 0
Me.ListView1.Top = 0
Me.ListView1.Width = aRect.Right
Me.ListView1.Height = aRect.Bottom


End Sub

Private Sub ListView1_DblClick()

'---> Cette proc�dure renvoie le num�ro d'enreg de l'�l�ment s�lectionn�

'-> Tester qu'un �l�ment soit s�lectionn�
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Renvoyer le num�ro d'enreg de l'�l�ment
strRetour = Entry(2, Me.ListView1.SelectedItem.Key, "|")

'-> D�charger la feuille
Unload Me

End Sub

Private Sub ListView1_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then ListView1_DblClick

End Sub
