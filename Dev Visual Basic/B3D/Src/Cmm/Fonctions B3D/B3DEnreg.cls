VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "B3DEnreg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Idfname As String '-> Ident + Table + [index]
Public NumEnreg As String '-> Num�ro d'enregistrement B3D
Public Ok As Boolean '-> Si la lecture de cet enregistrement c'est bien pass�e
Public ErrorCode As String '-> Code de l'erreur
Public Fields As Collection '-> Liste des champs de l'enregistrement
Public IsLocked As Boolean '-> Indique que l'enregistrement est lock�
Public OpeMaj As String '-> Si enreg en cours de lecture -> Operateur de lock
Public DateHeu As String '-> Date et heure de lock
Public Programme As String '-> Si lock : Programme qui lock

Private Sub Class_Initialize()

'-> Initialiser la collections des champs de l'enregistrement
Set Fields = New Collection

End Sub




