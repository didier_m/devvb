VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EmilieObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'---> Propri�t�s
Public Ident As String 'Identifiant  associ�
Public Progiciel  As String 'Progiciel de rattachement
Public WidgetFormat As String 'Format de la zone
Public Filled_Type As String 'Cadrage
Public Filled_Ascii As String 'Remplissage
Public MultiLangue As Integer 'Code langue si -> 0 pas multi
Public DataType As String 'Type de donn�e
Public Num_Field As String 'Num�ro du champ
Public Table_Correspondance As String 'Table associ�e
Public MasterField As String '-> Champ maitre pour l'acc�s au soci�t� de r�f�rence
 
'---> Champ � matrice
Public Sep_Vecteur As String '-> Indique qu'il faut traiter le champ en vecteur ( lookup )

'-> Variable d'affichage
Public hdlProp As Long

'-> Collection des fnames dans le quel le champ est rattach�
Public fNames As Collection
Public CodeLangue As Integer
Private pFill_Label(10) As String

Public Property Get Fill_Label() As String
'-> Renvoyer la valeur
Fill_Label = pFill_Label(CodeLangue)
End Property

Public Property Let Fill_Label(ByVal vNewValue As String)
'-> Setting de la valeur
pFill_Label(CodeLangue) = vNewValue
End Property

Private Sub Class_Initialize()
Set fNames = New Collection
End Sub
