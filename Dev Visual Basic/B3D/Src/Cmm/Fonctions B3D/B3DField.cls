VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "B3DField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Field As String '-> Code du champ que l'on cherche
Public Libel As String '-> Libell� du champ
Public ZonLock As String '-> Zone Lock
Public ZonData As String '-> Zone Data
Public EnregJoin As String '-> RowId de la jointure
Public FieldJoins As Collection '-> Collection des champs des jointues associ&s � ce champ
Public Histos As Collection '-> Collection des historiques associ�s � ce champ
Public FnameJoin As String '-> Table de jointure associ�e

Public Ordre As String '-> Ordre
Public IsReplica As Boolean '-> Si le champ est suivi en r�plication
Public IsCryptage As Boolean '-> Si le champ est r�piqu�
Public IsHisto As Boolean '-> Si le champ est suivi en historique
Public IsF10 As Boolean '-> Si le champ est suivi en Recherche
Public IsModifiable As Boolean '-> Indique si le champ est modifiable

Public Operat As String '-> Si apr�s Acc�s FindData
Public DatHeu As String '-> Si apr�s Acc�s FindData
Public Programme As String '-> Si apr�s Acc�s FindData

Private Sub Class_Initialize()

'-> Initialiser la collection des jointures
Set FieldJoins = New Collection

'-> Initialiser la collection des historiques
Set Histos = New Collection

End Sub
