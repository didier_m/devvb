VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "B3DBuffer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'*********************************************************
'* Classe B3DBuffer qui stocke la r�ponse d'un agent B3D *
'*********************************************************

'-> Apr�s lecture des lignes -> Cr�ation de la collection des enregistrements rattach�s
Public Enregs As Collection

'-> Pour lecture d'une r�ponse
Public Lignes As Collection

'-> Pour gestion des titres si necessaire
Public Title As String


Private Sub Class_Initialize()

'-> Initialiser la collection des lignes de r�ponse
Set Lignes = New Collection

'-> Intialiser la collection des champs
Set Enregs = New Collection

End Sub

Public Sub ResetLignes()

'---> Cette proc�dure supprime la collection des lignes issue de la lecture du RSP

Do While Me.Lignes.Count <> 0
    Me.Lignes.Remove (1)
Loop

End Sub



