Attribute VB_Name = "fctB3D"
Option Explicit

Dim Instruction As String

Public Catalogues As Collection

'----> Principe des �crans

' Les champs � charger depuis un enreg _
    FIELD~FNAME~BUFFERNAME~NUMFIELD
    
' Objet � charger depuis un FieldTable _
    FIELDTABLE~Buffer associ�~Field Index Direct

'Libell� des label _
    CATALOGUE~[Num field]
    
Public Function UnlockEnreg(Connexion As B3DConnexion, Fname As String, NumEnreg As String) As Boolean

'-> Cette proc�dure supprime un lock d'un enreg

Dim Ligne As String
Dim aBuffer As B3DBuffer

On Error GoTo GestError

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Instruction de load DOG
Instruction = "B3D_UNLOCK~" & Connexion.Ident & "�" & Fname & "�" & NumEnreg

If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Envoyer l'instruction et charger la matrice des champs
Set aBuffer = New B3DBuffer
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "LoadAgentDOG"
    Exit Function
End If

'-> Lire la premi�re ligne
Ligne = aBuffer.Lignes(1)

'-> Tester le retour
If Entry(2, Ligne, "~") <> "OK" Then
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
End If

'-> Lib�rer le buffer
Set aBuffer = Nothing

'-> Renvoyer une valeur de succ�s
UnlockEnreg = True

'-> Quitter la fonction
Exit Function

'-> Gestion des erreur
GestError:
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Err.Number
    Connexion.ErrorLibel = Err.Description
    Connexion.ErrorParam = "LoadAgentDOG"

End Function

    
Public Function LoadAgentDOG(Connexion As B3DConnexion) As Boolean


'-> Initialisation du mode trace

Dim Ligne As String
Dim aBuffer As B3DBuffer

On Error GoTo GestError

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Instruction de load DOG
Instruction = "B3D_LOAD_DOG"

If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Envoyer l'instruction et charger la matrice des champs
Set aBuffer = New B3DBuffer
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "LoadAgentDOG"
    Exit Function
End If

'-> Lire la premi�re ligne
Ligne = aBuffer.Lignes(1)

'-> Lib�rer le buffer
Set aBuffer = Nothing

'-> Rencoyer une valeur de succ�s
LoadAgentDOG = True

'-> Quitter la fonction
Exit Function

'-> Gestion des erreur
GestError:
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Err.Number
    Connexion.ErrorLibel = Err.Description
    Connexion.ErrorParam = "LoadAgentDOG"


End Function

Public Function SetModeTrace(Connexion As B3DConnexion, TopTrace As String) As Boolean


'-> Initialisation du mode trace

Dim Ligne As String
Dim aBuffer As B3DBuffer

On Error GoTo GestError

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Tester le mode cryptage
If TopTrace = "1" Then
    Instruction = "B3D_TRACE~1"
Else
    Instruction = "B3D_TRACE~0"
End If

If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Envoyer l'instruction et charger la matrice des champs
Set aBuffer = New B3DBuffer
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "SetModeTrace"
    Exit Function
End If

'-> Lib�rer le buffer
Set aBuffer = Nothing

'-> Rencoyer une valeur de succ�s
SetModeTrace = True

'-> Quitter la fonction
Exit Function

'-> Gestion des erreur
GestError:
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Err.Number
    Connexion.ErrorLibel = Err.Description
    Connexion.ErrorParam = "SetModeTrace"


End Function

Public Function SetModeDebug(Connexion As B3DConnexion, TopDebug As String) As Boolean


'-> Initialisation du mode debug

Dim Ligne As String
Dim aBuffer As B3DBuffer

On Error GoTo GestError

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Tester le mode cryptage
If TopDebug = "1" Then
    Instruction = "B3D_DEBUG~1"
Else
    Instruction = "B3D_DEBUG~0"
End If

If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Envoyer l'instruction et charger la matrice des champs
Set aBuffer = New B3DBuffer
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "SetModeDebug"
    Exit Function
End If

'-> Lib�rer le buffer
Set aBuffer = Nothing

'-> Rencoyer une valeur de succ�s
SetModeDebug = True

'-> Quitter la fonction
Exit Function

'-> Gestion des erreur
GestError:
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Err.Number
    Connexion.ErrorLibel = Err.Description
    Connexion.ErrorParam = "SetModeDebug"


End Function

Public Function CryptExchange(Connexion As B3DConnexion, TopCrypt As String) As Boolean


'-> Initialisation d'un programme

Dim Ligne As String
Dim aBuffer As B3DBuffer

On Error GoTo GestError

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Tester le mode cryptage
If TopCrypt = "1" Then
    Instruction = "B3D_CRYPT~1"
Else
    Instruction = "B3D_CRYPT~0"
End If

If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Envoyer l'instruction et charger la matrice des champs
Set aBuffer = New B3DBuffer
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "CryptExchange"
    Exit Function
End If

'-> Lib�rer le buffer
Set aBuffer = Nothing

'-> Rencoyer une valeur de succ�s
CryptExchange = True

'-> Quitter la fonction
Exit Function

'-> Gestion des erreur
GestError:
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Err.Number
    Connexion.ErrorLibel = Err.Description
    Connexion.ErrorParam = "CryptExchange"


End Function

Public Function InitialiseB3D(Connexion As B3DConnexion, NomProg As String, TopTrace As Boolean, TopCrypt As Boolean) As Boolean

'-> Initialisation d'un programme

Dim Ligne As String
Dim aBuffer As B3DBuffer

On Error GoTo GestError

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Tester si on a poser un nom de prog
If Trim(NomProg) <> "" Then
    '-> Poser l'instruction du nom du programme
    Instruction = "B3D_PROG~" & NomProg
    If Not Connexion.PutInstruction(Instruction) Then
        '-> Positionner le code Erreur
        Connexion.ErrorNumber = "100002"
        Connexion.ErrorLibel = "Error creating instruction"
        Connexion.ErrorParam = Instruction
        Exit Function
    End If
End If

'-> Gestion de la trace
If TopTrace Then
    '-> Poser l'instruction de trace
    Instruction = "B3D_TRACE~1"
Else
    '-> Pas de trace
    Instruction = "B3D_TRACE~0"
End If
If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Gestion du cryptage
If TopCrypt Then
    '-> Poser l'instruction de cryptage
    Instruction = "B3D_CRYPT~1"
Else
    '-> Pas de cryptage
    Instruction = "B3D_CRYPT~0"
End If
If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Envoyer l'instruction et charger la matrice des champs
Set aBuffer = New B3DBuffer
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "Initialise B3D"
    Exit Function
End If

'-> Lib�rer le buffer
Set aBuffer = Nothing

'-> Quitter la fonction
Exit Function

'-> Gestion des erreur
GestError:
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Err.Number
    Connexion.ErrorLibel = Err.Description
    Connexion.ErrorParam = "Initialise B3D"


End Function

Public Function SeekJoin(Connexion As B3DConnexion, Ident As String, Fname As String, NumEnreg As String) As String

'---> Cette fonction v�rifier si un enreg de jointure est utilis�

Dim Ligne As String
Dim aBuffer As B3DBuffer

On Error GoTo GestError

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Poser l'instruction de naviga select
Instruction = "B3D_SEEK_JOIN~" & Connexion.Ident & "�" & Fname & "�" & NumEnreg
If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Envoyer l'instruction et charger la matrice des champs
Set aBuffer = New B3DBuffer
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "SeekJoin"
    Exit Function
End If

'-> Lire la premi�re ligne de r�ponse
Ligne = aBuffer.Lignes(1)

'-> Tester le retour
If Entry(2, Ligne, "~") <> "OK" Then
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
End If

'-> Retourner la valeur
SeekJoin = Entry(3, Ligne, "~")

'-> Lib�rer le buffer
Set aBuffer = Nothing

'-> Quitter la fonction
Exit Function

'-> Gestion des erreur
GestError:
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Err.Number
    Connexion.ErrorLibel = Err.Description
    Connexion.ErrorParam = "Seekjoin"

End Function

Public Function InitNaviga(Connexion As B3DConnexion, Ident As String, Fname As String, _
                           BufferName As String, Sql As String, ConditionHorsIndex As String, _
                           ListeFieldToGet As String, ListeFieldToDisplay As String, EnregSize As Long, PageSize As Long, Descriptif As B3DFnameDescriptif) As B3DNavigation

'---> Cette fonction effectue un B3D_SELECT _NAVIGA
'-> En cas de succ�s, Cr�ation d'un objet de navigation et cr�ation de la premi�re page de navigation

Dim i As Long
Dim Ligne As String
Dim aNaviga As B3DNavigation
Dim aBuffer As B3DBuffer
Dim aEnreg As B3DEnreg
Dim aField As B3DField

On Error GoTo GestError

'-> Tester que la liste des champs � ramener soit sp�cifi�e
If Trim(ListeFieldToGet) = "" Then Exit Function

'-> Tester que la liste des champs � afficher soit sp�cifi�e
If Trim(ListeFieldToDisplay) = "" Then
    For i = 1 To NumEntries(ListeFieldToGet, "�")
        If ListeFieldToDisplay = "" Then
            ListeFieldToDisplay = Entry(1, Entry(i, ListeFieldToGet, "�"), "�")
        Else
            ListeFieldToDisplay = ListeFieldToDisplay & "�" & Entry(1, Entry(i, ListeFieldToGet, "�"), "�")
        End If
    Next
End If 'Si on a sp�cifi� la liste des champs � afficher

'-> Tester que la requete SQL soit s�lectionn�e
If Trim(Sql) = "" Then Exit Function

'-> Tester que le buffer soit saisi
If Trim(BufferName) = "" Then Exit Function

'-> Cr�er l'instruction
Instruction = "B3D_NAVIGA_SELECT~" & Ident & "�" & Fname & "�" & BufferName & "�" & EnregSize & "�" & PageSize & "~" & Sql & "~" & ConditionHorsIndex

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Poser l'instruction de naviga select
If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Faire un Naviga First ensuite
Instruction = "B3D_NAVIGA~" & BufferName & "�FIRST~" & ListeFieldToGet
If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Envoyer l'instruction et charger la matrice des champs
Set aBuffer = New B3DBuffer
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "InitNaviga"
    Exit Function
End If

'-> Lire la premi�re ligne de r�ponse
Ligne = aBuffer.Lignes(1)

'-> Analyse de l'instruction B3D_NAVIGA_SELECT
If Entry(2, Ligne, "~") <> "OK" Then
    '-> Tester s'il y a des enregistrements
    If Entry(3, Ligne, "~") = "90014" Then GoTo RepNaviga
    
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
End If

RepNaviga:

'-> Cr�er un objet de  navigation
Set aNaviga = New B3DNavigation

'-> Initialisation du buffer
aNaviga.BufferName = BufferName  '-> Nom du buffer
aNaviga.ConditionsHorsIndex = ConditionHorsIndex
aNaviga.Fname = Fname
aNaviga.Ident = Ident
aNaviga.ListeFieldToGet = ListeFieldToGet
aNaviga.ListeFieldToDisplay = ListeFieldToDisplay
aNaviga.EnregSize = EnregSize
aNaviga.PageSize = PageSize
Set aNaviga.aDescro = Descriptif


If Entry(3, Ligne, "~") <> "90014" Then

    '-> Positionner les variables de navigation
    aNaviga.EnregCount = CLng(Entry(2, Entry(3, Ligne, "~"), "�"))
    aNaviga.PageCount = CLng(Entry(1, Entry(3, Ligne, "~"), "�"))
    aNaviga.PageCours = 1
    
    '-> Analyse de l'intruction B3D_NAVIGA~FIRST
    Ligne = aBuffer.Lignes(2)
    
    '-> Selon le r�sultat de la requete
    If Entry(2, Ligne, "~") <> "OK" Then
        '-> Positionner la variable d'erreur
        Connexion.ErrorNumber = Entry(3, Ligne, "~")
        Connexion.ErrorLibel = Entry(4, Ligne, "~")
        Connexion.ErrorParam = Entry(5, Ligne, "~")
        '-> Quitter la fonction
        Exit Function
    End If
    
    '-> Cr�er le buffer de navigation
    For i = 3 To aBuffer.Lignes.Count
        '-> Lecture de la ligne
        Ligne = aBuffer.Lignes(i)
        '-> Cr�er un nouvel enregistrement
        Set aEnreg = Descriptif.GetEnreg
        '-> Le charger avec les valeurs
        exGetData Connexion, Ligne, aEnreg
        '-> Ajouter dans le buffer
        aBuffer.Enregs.Add aEnreg, UCase$(Trim(aEnreg.NumEnreg))
    Next

End If

'-> Ajouter ce buffer en tant que page dans l'objet de navigation en cours
aNaviga.Pages.Add aBuffer, "PAGE|1"

'-> Vider la collection des lignes buffer
aBuffer.ResetLignes

'-> Retourner l'objet de navigation
Set InitNaviga = aNaviga

Exit Function

GestError:
    '-> Positionner les variables d'erreur
    Connexion.ErrorNumber = Err.Number
    Connexion.ErrorLibel = Err.Description
    Connexion.ErrorParam = "InitNaviga"


End Function

Public Function InitScreen(aFrm As Form, aDescro As B3DFnameDescriptif, FieldTables As Collection)

'---> Charge tous les objets d'un �cran associ�s aux F10 et les captions des labels

Dim X As Control
Dim TypeTag As String
Dim ValueTag As String
Dim aObj As B3DObject

On Error Resume Next

'-> Analyse de tous les objest
For Each X In aFrm.Controls
    '-> Tester le tag
    If X.Tag = "" Then GoTo NextCtrl
    '-> Get de son param
    TypeTag = UCase$(Trim(Entry(1, X.Tag, "~")))
    ValueTag = UCase$(Trim(Entry(2, X.Tag, "~")))
    '-> Selon le type de tag
    Select Case TypeTag
        Case "CATALOGUE"
            '-> Pointer sur le champ du catalogue
            Set aObj = Catalogues(Format(ValueTag, "00000"))
            If Not TypeOf X Is CommandButton Then
                X.Caption = aObj.Fill_Label
            End If
            X.ToolTipText = aObj.Fill_Label
        Case "FIELDTABLE"
            '-> Tester le type de l'objet
            If TypeOf X Is ComboBox Then
                LoadFieldTableCombo X, FieldTables, aDescro
            ElseIf TypeOf X Is ListView Then
                LaodFieldTableListView X, FieldTables, aDescro
            End If
    
    End Select 'Selon le ype d'objet
NextCtrl:
Next 'Pour tous les objets de l'�cran


End Function
Public Sub LaodFieldTableListView(X As ListView, FnameJoins As Collection, MasterDescro As B3DFnameDescriptif)

'---> Cette proc�dure charge un browse � partir d'un buffer fieldTable

Dim aBuffer As B3DBuffer
Dim aEnreg As B3DEnreg
Dim aField As B3DField
Dim aObj As B3DObject
Dim aCol As ColumnHeader
Dim aItem As ListItem
Dim aDescro As B3DFnameDescriptif

'-> Vider la combo
X.ListItems.Clear

'-> Pointer sur l'objet
Set aObj = Catalogues(Format(Entry(3, X.Tag, "~"), "00000"))

'-> Pointer sur le buffer associ�
Set aBuffer = FnameJoins("B3D_" & aObj.Num_Field)

'-> Pointer sur le descriptif de la table associ�e � ce champ
Set aDescro = MasterDescro.Descros(aObj.Table_Correspondance)

'-> De base, ajouter le champ qui sert de jointure
Set aCol = X.ColumnHeaders.Add(, "COL|" & aObj.Num_Field, aObj.Fill_Label)

'-> Initialiser le listView
For Each aField In aDescro.Fields
    '-> Concatainer la valeur si le champ est en recherche
    If aField.IsF10 And aField.Field <> aDescro.IndexDirect Then
        '-> Cr�er une nouvelle colonne
        Set aCol = X.ColumnHeaders.Add(, "COL|" & aField.Field, aField.Libel)
    End If 'Si le champ est en recherche
Next 'Pour tous les champs

'-> Charger le listview
For Each aEnreg In aBuffer.Enregs
    '-> Cr�er un nouvel objet
    Set aItem = X.ListItems.Add(, "ENREG|" & aEnreg.NumEnreg)
    '-> Afficher la valeur de tous les champs en affichage
    For Each aCol In X.ColumnHeaders
        '-> Pointer sur le champ associ�
        Set aField = aEnreg.Fields(Entry(2, aCol.Key, "|"))
        If aCol.Position = 1 Then
            aItem.Text = aField.ZonData
        Else
            aItem.SubItems(aCol.Position - 1) = aField.ZonData
        End If
    Next 'Pour tous les champs
Next 'Pour tous les enregistrements

'-> Lib�rer le pointeur
Set aBuffer = Nothing
 
'-> Formatter le listview
FormatListView X

End Sub


Public Sub LoadFieldTableCombo(X As ComboBox, FnameJoins As Collection, MasterDescro As B3DFnameDescriptif)

'---> Cette proc�dure charge une combo avec un field table

Dim aBuffer As B3DBuffer
Dim aEnreg As B3DEnreg
Dim aField As B3DField
Dim ValueToAdd As String
Dim aDescro As B3DFnameDescriptif
Dim aObj As B3DObject

'-> Vider la combo
X.Clear

'-> Pointer sur l'objet
Set aObj = Catalogues(Format(Entry(3, X.Tag, "~"), "00000"))

'-> Pointer sur le buffer associ�
Set aBuffer = FnameJoins("B3D_" & aObj.Num_Field)

'-> Pointer sur le descriptif de la table associ�e � ce champ
Set aDescro = MasterDescro.Descros(aObj.Table_Correspondance)

'-> Rajouter le premier enreg � blanc pour annulation
X.AddItem ""

'-> Charger la combo
For Each aEnreg In aBuffer.Enregs
    '-> Afficher la valeur de tous les champs en affichage
    For Each aField In aEnreg.Fields
        If aDescro.Fields(aField.Field).IsF10 Then
            If ValueToAdd = "" Then
                ValueToAdd = aField.ZonData
            Else
                ValueToAdd = ValueToAdd & " - " & aField.ZonData
            End If
        End If
    Next 'Pour tous les champs
    '-> Ajouter dans la combo
    X.AddItem ValueToAdd
    '-> Vider la variable
    ValueToAdd = ""
Next 'Pour tous les enregistrements

'-> Lib�rer le pointeur
Set aBuffer = Nothing

End Sub

Public Function GetScreen(aEnreg As B3DEnreg, BufferRef As String, aFrm As Form, FnameRef As String, MasterDescro As B3DFnameDescriptif, FnameJoins As Collection) As String

'---> Mettre � jour l'objet Enreg en partant de l'�cran

Dim X As Control
Dim aObj As B3DObject
Dim aField As B3DField
Dim TypeTag As String
Dim BufferName As String
Dim Fname As String
Dim Field As String
Dim aBuffer As String
Dim ReturnValue As String
Dim temp As String

On Error GoTo GestError

'-> Analyse de tous les objest
For Each X In aFrm.Controls
    '-> Tester le tag
    If X.Tag = "" Then GoTo NextCtrl
    '-> Get de son param
    TypeTag = UCase$(Trim(Entry(1, X.Tag, "~")))
    '-> Selon le type de tag
    Select Case TypeTag
        Case "FIELD"
            '-> Get des param�tres
            Fname = UCase$(Trim(Entry(2, X.Tag, "~")))
            BufferName = UCase$(Trim(Entry(3, X.Tag, "~")))
            Field = Format(CLng(Entry(4, X.Tag, "~")), "00000")
            '-> V�rifier si on travaille sur la m�me table
            If Fname <> UCase$(Trim(FnameRef)) Then GoTo NextCtrl
            '-> V�rifier que l'on travaille sur le m�me buffer
            If BufferName <> BufferRef Then GoTo NextCtrl
            '-> Pointer sur le champ de l'enregistrement en cours
            Set aField = aEnreg.Fields(Field)
            '-> Mettre � jour sa ZonLock en fonction du type d'objet
            temp = ""
            If TypeOf X Is TextBox Then temp = SetZonLockFromTextbox(X, aField, MasterDescro)
        Case "FIELDTABLE"
            '-> Get des param�tres
            BufferName = UCase$(Trim(Entry(2, X.Tag, "~")))
            Field = Format(CLng(Entry(3, X.Tag, "~")), "00000")
            '-> V�rifier que l'on travaille sur le m�me buffer
            If BufferName <> BufferRef Then GoTo NextCtrl
            '-> Selon le type d'objet
            temp = ""
            If TypeOf X Is ComboBox Then
                temp = SetZonLockFromCombobox(X, aEnreg.Fields(Field), FnameJoins, MasterDescro)
            ElseIf TypeOf X Is ListView Then
            
            End If
    End Select
    
    '-> s'il y a une erreur : afficher un message
    If temp <> "" Then
        If Entry(1, temp, "~") = "ERROR" Then
            '-> Afficher un message d'erreur
            MsgBox Catalogues("00056").Fill_Label & " : " & Catalogues(Format(Entry(5, temp, "~"), "00000")).Fill_Label, vbExclamation + vbOKOnly, "Error"
        ElseIf Entry(1, temp, "~") = "OB" Then
            '-> Afficher un message d'erreur
            MsgBox Replace(Catalogues("00057").Fill_Label, "$###$", Catalogues(Format(Entry(5, temp, "~"), "00000")).Fill_Label), vbExclamation + vbOKOnly, "Error"
        End If
        '-> Renvoyer un message d'erreur
        GetScreen = "ERROR~" & X.Name
        '-> Quitter la fonction
        Exit Function
    End If 's'il ya une erreur sur le controle des zones
    
NextCtrl:
Next 'Pour tous les controles de la feuille

GestError:

End Function
Private Function SetZonLockFromCombobox(X As ComboBox, aField As B3DField, FnameJoins As Collection, MasterDescro As B3DFnameDescriptif) As String

Dim aObj As B3DObject
Dim aDescro As B3DFnameDescriptif
Dim MyEnreg As B3DEnreg
Dim aBuffer As B3DBuffer
Dim FieldJoin As B3DField

'-> Tester sur la valeur saisie soit OK
If X.ListIndex = -1 Then
    SetZonLockFromCombobox = "ERROR~" & X.Tag
    Exit Function
End If

'-> V�rifier que l'on n'ait pas s�lectionn� la premi�re zone
If X.ListIndex = 0 Then
    aField.ZonLock = ""
    Exit Function
End If

'-> Pointer sur l'objet du catalogue
Set aObj = Catalogues(aField.Field)

'-> R�cup�rer sa table de correspondance
Set aDescro = MasterDescro.Descros(aObj.Table_Correspondance)

'-> Pointer sur la table associ�e
Set aBuffer = FnameJoins("B3D_" & aField.Field)

'-> Pointer sur le bon enreg
Set MyEnreg = aBuffer.Enregs(X.ListIndex)

'-> R�cup�rer le bon champ
aField.ZonLock = MyEnreg.Fields(aDescro.IndexDirect).ZonData

'-> Mettre � jour les jointures
For Each FieldJoin In aField.FieldJoins
    '-> Pointer sur le champ de la jointure
    FieldJoin.ZonLock = MyEnreg.Fields(FieldJoin.Field).ZonData
Next 'Pour tous les champs de jointure

End Function

Private Function SetZonLockFromTextbox(X As TextBox, aField As B3DField, aDescro As B3DFnameDescriptif) As String

'---> Mettre � jour le champ en fonction du textbox et controle de la zone

Dim aObj As B3DObject

'-> Pointer sur l'objet du catalogue associ�
Set aObj = Catalogues(aField.Field)

X.Text = RTrim(X.Text)

'-> v�rifier la nature de la zone
If X.Text <> "" Then
    '-> V�rifier la valeur
    Select Case UCase$(aObj.DataType)
        Case "CHARACTER"
            '-> Faire un rtrim
            aField.ZonLock = RTrim(X.Text)
        Case "DECIMAL", "INTEGER"
            '-> Tester que la zone soit saisie
            If Not IsNumeric(X.Text) Then
                '-> Retourner une valeur d'erreur
                SetZonLockFromTextbox = "ERROR~" & X.Tag
                '-> Quitter la focntion
                Exit Function
            End If
            '-> Mettre � jour la zone
            aField.ZonLock = Trim(X.Text)
        Case "DATE"
            '-> Tester que la zone soit de type date
            If Not IsDate(X.Text) Or Len(X.Text) <> 10 Then
                '-> Retourner une valeur d'erreur
                SetZonLockFromTextbox = "ERROR~" & X.Tag
                '-> Quitter la focntion
                Exit Function
            End If
            '-> Mettre � jour sa zone
            aField.ZonLock = Trim(X.Text)
    End Select 'Selon le type de l'objet
Else
    '-> Si la zone est � blanc est qu'elle est obligatoire : Renvoyer une erreur
    If Entry(5, X.Tag, "~") = "OB" Then
        '-> Retourner une valeur d'erreur
        SetZonLockFromTextbox = "OB~" & X.Tag
        '-> Quitter la focntion
        Exit Function
    End If
    '-> Mettre � jour la zone Lock
    aField.ZonLock = ""
End If 'Si la zone est saisie

End Function


Public Sub LoadScreen(aEnreg As B3DEnreg, BufferRef As String, aFrm As Form, FnameRef As String, FnameJoins As Collection, MasterDescro As B3DFnameDescriptif, TypeAction As String)

'---> Charge tous les objets � partir d'un enreg

Dim X As Control
Dim aObj As B3DObject
Dim aField As B3DField
Dim TypeTag As String
Dim BufferName As String
Dim Fname As String
Dim Field As String
Dim aBuffer As String
Dim aDescro As B3DFnameDescriptif

'-> Analyse de tous les objest
For Each X In aFrm.Controls
    '-> Tester le tag
    If X.Tag = "" Then GoTo NextCtrl
    '-> Get de son param
    TypeTag = UCase$(Trim(Entry(1, X.Tag, "~")))
    '-> Selon le type de tag
    Select Case TypeTag
        Case "FIELD"
            If TypeAction = "CREATE" Then GoTo NextCtrl
            '-> Get des param�tres
            Fname = UCase$(Trim(Entry(2, X.Tag, "~")))
            BufferName = UCase$(Trim(Entry(3, X.Tag, "~")))
            Field = Format(CLng(Entry(4, X.Tag, "~")), "00000")
            '-> V�rifier si on travaille sur la m�me table
            If Fname <> UCase$(Trim(FnameRef)) Then GoTo NextCtrl
            '-> V�rifier que l'on travaille sur le m�me buffer
            If BufferName <> BufferRef Then GoTo NextCtrl
            '-> Pointer sur l'objet du catalogue
            Set aObj = Catalogues(Field)
            '-> Pointer sur le champ de l'enregistrement
            Set aField = aEnreg.Fields(Field)
            '-> Selon le type d'objet
            If TypeOf X Is TextBox Then
                '-> Cas d'un text box
                FillTextBoxFromField aField, aObj, X
            ElseIf TypeOf X Is ListView Then
            
            End If
        Case "FIELDTABLE"
            '-> Ne traiter
            If TypeAction = "CREATE" Then
                X.ListIndex = 0
                GoTo NextCtrl
            End If
            '-> Get des param�tres
            BufferName = UCase$(Trim(Entry(2, X.Tag, "~")))
            Field = Format(CLng(Entry(3, X.Tag, "~")), "00000")
            '-> V�rifier que l'on travaille sur le m�me buffer
            If BufferName <> BufferRef Then GoTo NextCtrl
            '-> Selon le type d'objet
            If TypeOf X Is ComboBox Then
                SetValueComboFieldTable X, aEnreg.Fields(Field).ZonData, Field, MasterDescro, FnameJoins
            End If
    End Select
NextCtrl:
Next

End Sub

Private Sub SetValueComboFieldTable(X As ComboBox, ValueToFind As String, Field As String, MasterDescro As B3DFnameDescriptif, FnameJoins As Collection)

'---> Cette proc�dure positionne la combo sur la valeur sp�cifi�e dans le champ d'origine

Dim aDescro As B3DFnameDescriptif
Dim aBuffer As B3DBuffer
Dim aObj As B3DObject
Dim aEnreg As B3DEnreg
Dim i As Long


'-> Pointer sur le buffer FieldTable associ�
Set aBuffer = FnameJoins("B3D_" & Field)
'-> Pointer sur l'objet du catalogue
Set aObj = Catalogues(Field)
'-> Pointer sur le descriptif associ�
Set aDescro = MasterDescro.Descros(aObj.Table_Correspondance)
'-> Analyser les enreg
i = 1
For Each aEnreg In aBuffer.Enregs
    '-> Comparer par rapport au champ d'indexjointure
    If Trim(UCase$(ValueToFind)) = Trim(UCase$(aEnreg.Fields(aDescro.IndexDirect).ZonData)) Then
        '-> Positionner la combo
        X.ListIndex = i
        '-> Quitter la proc�dure
        Exit Sub
    End If
    i = i + 1
Next 'Pour tous les enregistrements

End Sub

Private Sub FillTextBoxFromField(aField As B3DField, aObj As B3DObject, X As TextBox)

'---> Cette proc�dure charge un controle avec la valeur de l'enreg associ�

Dim ValueToFill As String
Dim FieldJoin As B3DField

'-> Ne pas traiter les champs de type vecteur
If aObj.Sep_Vecteur <> "" Then Exit Sub

'-> Cr�er la valeur de remplissage
If aObj.Table_Correspondance <> "" Then Exit Sub
    
'-> Champ simple : poser sa valeur
Select Case UCase$(aObj.DataType)
    Case "CHARACTER"
        X.Text = RTrim(aField.ZonData)
    Case "DECIMAL", "INTEGER", "DATE"
        X.Text = Trim(aField.ZonData)
End Select


End Sub


Public Sub CreateListViewNaviga(aNaviga As B3DNavigation, ListView As ListView)

'---> Cette proc�dure initialise un listview et afficahe la premi�re page
'Cette proc�dure ne peut �tre appel�e qu'apr�s un initnaviga

Dim aCol As ColumnHeader
Dim aField As B3DField
Dim aBuffer As B3DBuffer
Dim aEnreg As B3DEnreg
Dim i As Integer
Dim aObj As B3DObject

'-> Initialiser le listview
ListView.ColumnHeaders.Clear
ListView.ListItems.Clear

'-> Cr�er les colonnes
For i = 1 To NumEntries(aNaviga.ListeFieldToDisplay, "�")
    '-> Pointer sur le champ
    Set aField = aNaviga.aDescro.Fields(Entry(i, aNaviga.ListeFieldToDisplay, "�"))
    '-> Cr�er une nouvell colonne
    Set aCol = ListView.ColumnHeaders.Add(, "COL|" & aField.Field)
    '-> Positionner son libelle
    aCol.Text = aField.Libel
    '-> Pointer sur l'objet associ�
    Set aObj = Catalogues(aField.Field)
    '-> Alignement interne selon le type de donn�es
    Select Case UCase$(aObj.DataType)
        Case "CHARACTERE", "DATE"
            aCol.Alignment = lvwColumnLeft
        Case "DECIMAL", "INTEGER"
            aCol.Alignment = lvwColumnRight
    End Select
Next 'Pour tous les champs � afficher

'-> Pointer sur la premi�re page s'il y a une page
If aNaviga.PageCours <> 0 Then

    '-> Pointer sur la page
    Set aBuffer = aNaviga.Pages("PAGE|1")

    '-> Afficher les enregistrements de la premi�re page
    For Each aEnreg In aBuffer.Enregs
        '-> Ajouter un enreg dans le listview
        DisplayEnregInListView aEnreg, ListView, aNaviga.aDescro
    Next
End If

'-> Formatter le listview
FormatListView ListView

End Sub
Public Function NavigaGetPage(Connexion As B3DConnexion, aNaviga As B3DNavigation, PageNumber As Integer, aDescro As B3DFnameDescriptif) As Boolean

'---> Cette proc�dure r�cup�rer une page en particulier

Dim aBuffer As B3DBuffer
Dim aPage As B3DBuffer
Dim aEnreg As B3DEnreg
Dim Ligne As String
Dim i As Integer

'-> Intialiser une connexion
If Not Connexion.InitDmd() Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Faire un Naviga First ensuite
Instruction = "B3D_NAVIGA~" & aNaviga.BufferName & "�DIRECT�" & PageNumber & "~" & aNaviga.ListeFieldToGet
If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Initialiser le buffer de retour
Set aBuffer = New B3DBuffer

'-> Envoie de l'instruction
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "NavigaGetPage"
    Exit Function
End If

'-> Analyse de la premi�re ligne
Ligne = aBuffer.Lignes(1)

'-> Tester le retour
If UCase$(Entry(2, Ligne, "~")) <> "OK" Then
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
End If

'-> Ajout du buffer dans l'objet de navigation
Set aPage = New B3DBuffer
aNaviga.Pages.Add aPage, "PAGE|" & Trim(Entry(3, Ligne, "~"))

'-> Positionner la page en cours
aNaviga.PageCours = CInt(Entry(3, Ligne, "~"))

'-> Analyse des lignes de retour
For i = 2 To aBuffer.Lignes.Count
    '-> Lecture de la ligne
    Ligne = aBuffer.Lignes(i)
    '-> Cr�ation d'un nouvel Enregistrement
    Set aEnreg = aDescro.GetEnreg
    '-> Charger avec les valeurs
    exGetData Connexion, Ligne, aEnreg
    '-> Ajout dans la page en cours
    aPage.Enregs.Add aEnreg, aEnreg.NumEnreg
Next 'Pour toutes les lignes

'-> Renvoyer une valeur de succ�s
NavigaGetPage = True

'-> Ecraser les lignes de la matrice
aBuffer.ResetLignes

'-> Supprimer l'objet temporaire
Set aBuffer = Nothing

End Function


Public Sub DisplayPageinListview(aNaviga As B3DNavigation, ListView As ListView, PageNumber As Integer, aDescro As B3DFnameDescriptif)

'---> Afficher la page sp�cifi�e

Dim aBuffer As B3DBuffer
Dim aEnreg As B3DEnreg

'-> Bloquer la mise � jour du listview
LockWindowUpdate ListView.hwnd

'-> Vider le listview
ListView.ListItems.Clear

'-> Pointer sur la page s�lectionn�e
Set aBuffer = aNaviga.Pages("PAGE|" & PageNumber)

'-> Afficher les enregistrements de la premi�re page
For Each aEnreg In aBuffer.Enregs
    '-> Ajouter un enreg dans le listview
    DisplayEnregInListView aEnreg, ListView, aDescro
Next

'-> Formatter le listview
FormatListView ListView

'-> Release du lock �cran
LockWindowUpdate 0

End Sub

Private Function IsListItem(Key As String, ListView As ListView) As Boolean

'---> Cette proc�dure indique si un listitem existe dans un listview

Dim X As ListItem

On Error GoTo GestError

'-> Essayer de pointer dessus
Set X = ListView.ListItems(Key)

'-> Indiquer que l'objet existe
IsListItem = True

'-> Quitter la fonction
Exit Function

GestError:


End Function


Public Sub DisplayEnregInListView(aEnreg As B3DEnreg, ListView As ListView, aDescro As B3DFnameDescriptif)

'---> Cette proc�dure cr�er un nouvel enregistrement dans un listview sp�cifi�

Dim X As ListItem
Dim aCol As ColumnHeader
Dim aField As B3DField
Dim aFieldJoin As B3DField
Dim aObj As B3DObject
Dim i As Integer
Dim Valuefield As String
Dim ToGet As Boolean

'-> V�rifier que l'enregistrement sp�cifi� soit OK
If Not aEnreg.Ok Then Exit Sub

'-> Cr�er un nouvel Enreg dans le listView ou pointer dessus s'il existe
If IsListItem("ENREG|" & aEnreg.NumEnreg, ListView) Then
    Set X = ListView.ListItems("ENREG|" & aEnreg.NumEnreg)
Else
    Set X = ListView.ListItems.Add(, "ENREG|" & aEnreg.NumEnreg)
End If

'-> Amimenter les colonnes
For Each aCol In ListView.ColumnHeaders
    '-> Pointer sur le champ associ�
    Set aField = aEnreg.Fields(Entry(2, aCol.Key, "|"))
    '-> Mettre � jour sa valeur
    Valuefield = aField.ZonData
    '-> Analyse des champs jointures pour l'enregistrement en cours
    For Each aFieldJoin In aField.FieldJoins
        '-> Pointer sur le champ du catalogue
        Set aObj = Catalogues(aFieldJoin.Field)
        '-> Tester si le champ est multilangue
        If aFieldJoin.IsF10 Then
            If aObj.MultiLangue = 0 Then
                ToGet = True
            Else
                If aObj.MultiLangue = aDescro.CodeLangue Then
                    ToGet = True
                Else
                    ToGet = False
                End If
            End If
        Else
            ToGet = False
        End If
        If ToGet Then
            If Trim(Valuefield) = "" Then
                If UCase$(aObj.DataType) = "CHARACTER" Or UCase$(aObj.DataType) = "DATE" Then
                    Valuefield = RTrim(aFieldJoin.ZonData)
                Else
                    Valuefield = Trim(aFieldJoin.ZonData)
                End If
            Else
                If UCase$(aObj.DataType) = "CHARACTER" Or UCase$(aObj.DataType) = "DATE" Then
                    Valuefield = Valuefield & " - " & RTrim(aFieldJoin.ZonData)
                Else
                    Valuefield = Valuefield & " - " & Trim(aFieldJoin.ZonData)
                End If
            End If
        End If
    Next 'Pour toutes les jointures
    '-> Mettre � jour
    If aCol.Position = 1 Then
        X.Text = Valuefield
    Else
        X.SubItems(aCol.Position - 1) = Valuefield
    End If
Next 'Pour toutes les colonnes

End Sub

Public Function FindData(Connexion As B3DConnexion, Ident As String, Fname As String, NumEnreg As String, ToLock As Boolean, GetHisto As Boolean, aDescro As B3DFnameDescriptif, GetJoin As Boolean) As B3DEnreg

'---> Cette proc�dure retourne un buffer apr�s un FIND_DATA et un Find_HISTO

Dim aBuffer As B3DBuffer
Dim Ligne As String
Dim aEnreg As B3DEnreg
Dim aHisto As B3DHisto
Dim i As Long

On Error GoTo GestError

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Poser une instruction
Instruction = "B3D_FIND_DATA~" & Trim(UCase$(Ident)) & "�" & Fname & "�" & NumEnreg & "�" & CStr(Abs(CInt(ToLock)) & "�" & CStr(Abs(CInt(GetHisto)))) & "�" & CStr(Abs(CInt(GetJoin)))
If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Initialiser un buffer de r�ponse
Set aBuffer = New B3DBuffer

'-> Envoyer l'instruction et charger la matrice des champs
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "FindData"
    Exit Function
End If

'-> Lire la premi�re ligne
Ligne = aBuffer.Lignes(1)

'-> Analyse du retour
If Entry(2, Ligne, "~") <> "OK" Then
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
End If

'-> Cr�er un nouvel enreg
Set aEnreg = aDescro.GetEnreg

'-> Positionner son num�ro
aEnreg.NumEnreg = NumEnreg

'-> Lire la seconde ligne
Ligne = aBuffer.Lignes(2)

'-> Faire un explode de la ligne
If Not exFindData(Connexion, Ligne, aEnreg) Then Exit Function
    
'-> Tester les histos
If GetHisto Then
    '-> Lecture de la ligne de retour
    Ligne = aBuffer.Lignes(3)
    '-> Tester si l'acc�s aux histos a march�
    If Entry(2, Ligne, "~") <> "OK" Then
        '-> Ne rien faire si Erreur 90013 -> Pas d'histo
        If Entry(3, Ligne, "~") = "90013" Then GoTo NextHisto
        '-> Positionner le code Erreur
        Connexion.ErrorNumber = Entry(3, Ligne, "~")
        Connexion.ErrorLibel = Entry(4, Ligne, "~")
        Connexion.ErrorParam = Entry(5, Ligne, "~")
        Exit Function
    End If
    '-> Analyse de toutes les lignes
    For i = 4 To aBuffer.Lignes.Count
        '-> Lecture de la ligne
        Ligne = aBuffer.Lignes(i)
        '-> R�cup�rer les num�ro d'enregistrement
        exGetHisto aEnreg, Ligne
    Next 'Pour toutes les lignes
End If 'si On doit r�cup�rer les histos
    
NextHisto:
    
'-> Retourner l'enregistrement
Set FindData = aEnreg

'-> Quitter la fonction
Exit Function

GestError:

    Connexion.ErrorNumber = Err.Number
    Connexion.ErrorLibel = Err.Description
    Connexion.ErrorParam = "FindData"


End Function

Public Sub exGetHisto(aEnreg As B3DEnreg, Ligne As String)

'---> Cette fonction AnaLyse une ligne d'historiques et cr�er les histos pour un champ donn�

Dim aField As B3DField
Dim aHisto As B3DHisto
Dim NumField As String
Dim ZonData As String
Dim Fonction As String
Dim DatHisto As String
Dim HeuHisto As String
Dim Operateur As String
Dim Programme As String

On Error GoTo GestError

'-> ZonData
NumField = Entry(1, Entry(2, Ligne, "~"), "�")
ZonData = Entry(2, Entry(2, Ligne, "~"), "�")
Fonction = Entry(3, Entry(2, Ligne, "~"), "�")
DatHisto = Entry(4, Entry(2, Ligne, "~"), "�")
HeuHisto = Entry(5, Entry(2, Ligne, "~"), "�")
Operateur = Entry(6, Entry(2, Ligne, "~"), "�")
Programme = Entry(7, Entry(2, Ligne, "~"), "�")

'-> Pointer sur le champ associ� � l'enregistrement
Set aField = aEnreg.Fields(NumField)

'-> Cr�er un nouvel enregistrement d'histo
Set aHisto = New B3DHisto

'-> Maj de l'histo
aHisto.ZonHisto = ZonData
aHisto.FonctionHisto = Fonction
aHisto.DatHisto = DatHisto
aHisto.HeuHisto = HeuHisto
aHisto.Operat = Operateur
aHisto.Programme = Programme

'-> Ajouter l'histo
aField.Histos.Add aHisto

'-> Quitter la fonction
Exit Sub

GestError:

    

End Sub

Private Sub ShowBrowseFindData(aEnreg As B3DEnreg, ListView As ListView, Optional ShowHisto As Boolean)


'---> Cette proc�dure cr�er un browse du finddata

Dim aCol As ColumnHeader
Dim aField As B3DField
Dim X As ListItem
Dim aHisto As B3DHisto

On Error GoTo GestError

'-> Cr�er le bon nombre de colonnes
ListView.ColumnHeaders.Clear
ListView.ColumnHeaders.Add , , "Field"
ListView.ColumnHeaders.Add , , "Libel"
ListView.ColumnHeaders.Add , , "Zon data"
ListView.ColumnHeaders.Add , , "Operat"
ListView.ColumnHeaders.Add , , "Program"
ListView.ColumnHeaders.Add , , "Date"
    
'-> Pour tous les champs
For Each aField In aEnreg.Fields
    If Trim(aField.ZonData) = "" And aField.Histos.Count = 0 Then GoTo NextField
    '-> Cr�er un ListItem que s'il y a des donn�es
    Set X = ListView.ListItems.Add(, "FIELD|" & aField.Field, aField.Field)
    X.SubItems(1) = aField.Libel
    X.SubItems(3) = aField.Operat
    X.SubItems(4) = aField.Programme
    X.SubItems(5) = aField.DatHeu
    If aField.DatHeu = "IS_MODIFY" Then X.SubItems(5) = aField.DatHeu & " - " & aField.ZonLock
    '-> Pas de jointure : mettre � jour la donn�es
    X.SubItems(2) = aField.ZonData
    '-> Gestion des historiques
    If ShowHisto Then
        For Each aHisto In aField.Histos
            Set X = ListView.ListItems.Add()
            '-> Mise � jour des zones
            X.SubItems(1) = "Histo : "
            X.SubItems(2) = aHisto.ZonHisto
            X.SubItems(3) = aHisto.Operat
            X.SubItems(4) = aHisto.Programme & " - " & aHisto.FonctionHisto
            X.SubItems(5) = aHisto.DatHisto & " " & aHisto.HeuHisto
        Next 'Pour tous les historiques
    End If 'Si on affiche ou non les histos
NextField:
Next

'-> Justifier
FormatListView ListView

GestError:
    
End Sub
Private Function exFindData(Connexion As B3DConnexion, Ligne As String, aEnreg As B3DEnreg) As Boolean

'---> Cette proc�dure effectue un explode de la ligne pour cr�er un enregistrement

'---> Cette proc�dure cr�er une collection de champ � partir d'un ligne

Dim strParam As String
Dim DefField As String
Dim i As Integer
Dim aField As B3DField

On Error GoTo GestError

'-> Ne rien faire si traitement � blanc
If Trim(Ligne) = "" Then Exit Function

'-> Tester s'il y a eu Erreur
If Entry(2, Ligne, "~") = "ERROR" Then
    '-> Tester le code erreur
    Select Case Entry(3, Ligne, "~")
        Case "90006" 'Lock by Other
            '-> Enreg OK
            aEnreg.Ok = False
            '-> Indiquer que l'enregistrement est lock�
            aEnreg.IsLocked = True
            '-> Par qui
            aEnreg.OpeMaj = Entry(1, Entry(5, Ligne, "~"), "�")
            '-> Quand
            aEnreg.DateHeu = Entry(2, Entry(5, Ligne, "~"), "�")
            '-> Programme
            aEnreg.Programme = Entry(2, Entry(5, Ligne, "~"), "�")
            '-> Quitter la fonction
            Exit Function
        Case Else
            '-> Indiquer que l'on n'a pas eu acc�s � l'enregistrement
            aEnreg.Ok = False
            '-> Positionner la variable d'erreur
            Connexion.ErrorNumber = Entry(3, Ligne, "~")
            Connexion.ErrorLibel = Entry(4, Ligne, "~")
            Connexion.ErrorParam = Entry(5, Ligne, "~")
            '-> Quitter la fonction
            Exit Function
    End Select
End If

'-> R�cup�rer le param�trage
strParam = Entry(3, Ligne, "~")

'-> Pour tous les champs
For i = 1 To NumEntries(strParam, "�")
    '-> Get d'une def
    DefField = Entry(i, strParam, "�")
    '-> Pointer sur le champ sp�cifi�
    Set aField = aEnreg.Fields(Entry(1, DefField, "�"))
    '-> Zon Lock
    aField.ZonLock = RTrim(Entry(2, DefField, "�"))
    '-> Zon Data
    aField.ZonData = RTrim(Entry(3, DefField, "�"))
    '-> Operateur
    aField.Operat = Trim(Entry(5, DefField, "�"))
    '-> Programme
    aField.Programme = Trim(Entry(6, DefField, "�"))
    '-> Date Heure
    aField.DatHeu = Trim(Entry(7, DefField, "�"))
Next 'Pour tous les champs � afficher

'-> Indiquer que l'enreg est Ok
aEnreg.Ok = True
aEnreg.IsLocked = False

'-> Renvoyer l'enregistrement
exFindData = True

'-> Quitter la fonction
Exit Function

'-> Gestion des erreurs
GestError:
    Connexion.ErrorNumber = Err.Number
    Connexion.ErrorLibel = Err.Description
    Connexion.ErrorParam = "exFindData"

End Function


Public Function GetData(Connexion As B3DConnexion, Ident As String, Fname As String, NumEnreg As String, ListeFields As String, ToLock As Boolean, aDescro As B3DFnameDescriptif) As B3DEnreg

'---> Cette proc�dure retourne un objet Enregistrement qui correspond � l'instruction B3D_GET_DATA

Dim aBuffer As B3DBuffer
Dim aEnreg As B3DEnreg
Dim Ligne As String

'-> Tester que la liste des champs soit sp�cifi�e
If Trim(ListeFields) = "" Then Exit Function

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Poser une instruction
Instruction = "B3D_GET_DATA~" & Trim(UCase$(Ident)) & "�" & Fname & "�" & NumEnreg & "�" & CStr(Abs(CInt(ToLock))) & "~" & ListeFields
If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Initialiser un buffer de r�ponse
Set aBuffer = New B3DBuffer

'-> Envoyer l'instruction et charger la matrice des champs
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "SeekJoin"
    Exit Function
End If

'-> Lire la premi�re ligne
Ligne = aBuffer.Lignes(1)

'-> Analyse du retour
If Entry(2, Ligne, "~") <> "OK" Then
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
End If

'-> Lire la seconde ligne
Ligne = aBuffer.Lignes(2)

'-> Cr�er un enregistrement vierge
Set aEnreg = aDescro.GetEnreg
'-> Le remplir avec les donn�es
exGetData Connexion, Ligne, aEnreg

'-> Retourner cet enregistrement
Set GetData = aEnreg

End Function
Public Function exGetData(Connexion As B3DConnexion, Ligne As String, aEnreg As B3DEnreg) As Boolean

'---> Cette fonction eclate une ligne de type GETDATA pour creer unenregistrement

Dim NumEnreg As String
Dim TopError As String
Dim Param As String
Dim aField As B3DField
Dim aFieldJoin As B3DField
Dim ListeJoin As String
Dim NumEnregJoin As String
Dim FnameJoin As String
Dim i As Integer
Dim j As Integer
Dim DefField As String
Dim NumField As String
Dim LibelField As String
Dim Valuefield As String

'-> Tester la ligne
If Trim(Ligne) = "" Then Exit Function

'-> Positionner le num�ro d'enreg
aEnreg.NumEnreg = Entry(1, Ligne, "~")

'-> R�cup�rer le num�ro d'enreg
NumEnreg = Entry(1, Ligne, "~")

'-> Get du top de validit�
TopError = Entry(2, Ligne, "~")

'-> Tester le retour
If TopError = "ERRROR" Then
    '-> Tester le code de l'erreur
    If Entry(3, Ligne, "~") = "90006" Then
        '-> R�cup�rer le param�trage
        Param = Entry(5, Ligne, "~")
        '-> Indiquer que l'on a eu acc�s � l'enregistrement
        aEnreg.Ok = False
        '-> L'enregistrement sp�cifi� est lock�
        aEnreg.IsLocked = True
        '-> Op�rateur de lock
        aEnreg.OpeMaj = Entry(1, Param, "�")
        '-> Date du lock
        aEnreg.DateHeu = Entry(2, Param, "�")
        '-> Programme de lock
        aEnreg.Programme = Entry(3, Param, "�")
        '-> Fin de la fonction
        GoTo EndFunction
    Else
        '-> Indiquer que l'on a pas eu acc�s � l'enregistrement
        aEnreg.Ok = False
        '-> Positionner la variable d'erreur
        Connexion.ErrorNumber = Entry(3, Ligne, "~")
        Connexion.ErrorLibel = Entry(4, Ligne, "~")
        Connexion.ErrorParam = Entry(5, Ligne, "~")
        '-> Quitter la fonction
        Exit Function
    End If
End If


'-> R�cup�rer la valeur de retour
Param = Entry(3, Ligne, "~")

    
'-> Tout est OK , cr�ation des enregistrements et de leur jointure
For i = 1 To NumEntries(Param, "�")
    '-> R�cup�rer une d�finition de champ
    DefField = Entry(i, Param, "�")
    '-> Get de son champ
    NumField = Entry(1, Entry(1, DefField, "�"), "|")
    '-> get de son libelle
    LibelField = Entry(2, Entry(1, DefField, "�"), "|")
    '-> Get de sa valeur
    Valuefield = RTrim(Entry(2, Entry(2, DefField, "�"), "|"))
    '-> Pointer sur le champ
    Set aField = aEnreg.Fields(NumField)
    '-> Positionner sa valeur
    aField.ZonData = Valuefield
    '-> MAJ de zon lock
    aField.ZonLock = Valuefield
    '-> R�cup�rer la valeur des champs de jointure
    ListeJoin = Trim(Entry(3, DefField, "�"))
    '-> Traiter les jointures
    If ListeJoin <> "" Then
        '-> R�cup�rer la table de jointure
        FnameJoin = Entry(1, ListeJoin, "|")
        '-> R�cup�rer le numenreg de jointure
        NumEnregJoin = Entry(2, ListeJoin, "|")
        '-> Analyse de tous les champs de jointure
        For j = 4 To NumEntries(DefField, "�")
            '-> Pointer sur le champ de jointure
            Set aFieldJoin = aField.FieldJoins(Entry(1, Entry(j, DefField, "�"), "|"))
            '-> Numenreg
            aFieldJoin.EnregJoin = NumEnregJoin
            '-> Valeur du champ
            aFieldJoin.ZonData = RTrim(Entry(2, Entry(j, DefField, "�"), "|"))
            '-> Table associ�e
            aFieldJoin.FnameJoin = FnameJoin
        Next 'Pour tous les champs de jointure
    End If 'S'il y a des jointures
Next 'Pour tous les champs retourn�s

'-> Indiquer que tout c'est bien pass�
aEnreg.Ok = True
aEnreg.IsLocked = False
aEnreg.ErrorCode = ""

'-> Renvoyer une valeur de succ�s
exGetData = True


EndFunction:
    '-> Retourner l'enregistrement
    exGetData = False

End Function

Public Function GetFieldTableFromVecteur(Connexion As B3DConnexion, BufferCollection As Collection, Ident As String, ListeTable As String)

'---> Cette proc�dure part d'une liste de table � acc�s direct et cr�er un objet Buffer � chaque table

Dim i As Integer, j As Integer
Dim aBuffer As B3DBuffer
Dim BufferFieldTable As B3DBuffer
Dim Ligne As String
Dim aEnreg As B3DEnreg

On Error GoTo GestError

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Initialiser un buffer
Set aBuffer = New B3DBuffer

'-> Cr�er une instruction par table
For i = 1 To NumEntries(ListeTable, ",")
    '-> Poser une instruction FieldTable
    Instruction = "B3D_FIELD_TABLE~" & Trim(UCase$(Ident)) & "�" & Trim(UCase$(Format(Entry(i, ListeTable, ","), "00000")))
    If Not Connexion.PutInstruction(Instruction) Then
        '-> Positionner le code Erreur
        Connexion.ErrorNumber = "100002"
        Connexion.ErrorLibel = "Error creating instruction"
        Connexion.ErrorParam = Instruction
        Exit Function
    End If
Next 'Pour toutes les tables

'-> Envoyer la demande
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "GetFieldTableFromVecteur"
    Exit Function
End If


'-> Analyse du retour
If Entry(2, aBuffer.Lignes(1), "~") <> "OK" Then
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
End If

'-> Compteur de table � 0
j = 0

'-> Pour chaque ligne
For i = 1 To aBuffer.Lignes.Count
    '-> Lecture de la ligne
    Ligne = aBuffer.Lignes(i)
    '-> Selon le type de ligne
    If UCase$(Trim(Entry(1, Ligne, "~"))) = "B3D_FIELD_TABLE" Then
        '-> Tester le retour de cet acc�s � la table
        If Entry(2, Ligne, "~") = "OK" Then
            '-> Incr�menter le compteur de table
            j = j + 1
            '-> C�er dans tous les cas un nouveua buffer
            Set BufferFieldTable = New B3DBuffer
            '-> Positonner les titres
            BufferFieldTable.Title = Entry(3, Ligne, "~")
            '-> Ajouter le buffer dans la collection des buffers
            BufferCollection.Add BufferFieldTable, "B3D_" & Trim(UCase$(Format(Entry(j, ListeTable, ","), "00000")))
        End If
    Else
        '-> Cr�er un enregistrement
        Set aEnreg = exFieldTable(Ligne, BufferFieldTable.Title)
        '-> Ajouter l'enregistrement dans le buffer
        BufferFieldTable.Enregs.Add aEnreg, aEnreg.NumEnreg
    End If
Next 'Pour toutes les lignes

'-> Quitter la fonction
Exit Function

'-> Gestion des erreurs
GestError:

End Function

Public Function FieldTable(Connexion As B3DConnexion, aBuffer As B3DBuffer, Ident As String, Field As String) As Boolean

'----> Cette proc�dure retourne le contenu d'une table associ�e � un champ

Dim Ligne As String
Dim i As Long
Dim aEnreg As B3DEnreg

On Error GoTo GestError

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If
'-> Poser une instruction
Instruction = "B3D_FIELD_TABLE~" & Trim(UCase$(Ident)) & "�" & Trim(UCase$(Field))
If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Envoyer l'instruction et charger la matrice des champs
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "FieldTable"
    Exit Function
End If

'-> Vider l'objet buffer
Do While aBuffer.Enregs.Count <> 0
    aBuffer.Enregs.Remove (1)
Loop 'Pour tous les enregs
aBuffer.Title = ""

'-> Lire la premi�re ligne de la r�ponse
Ligne = aBuffer.Lignes(1)

'-> Analyse du retour
If Entry(2, aBuffer.Lignes(1), "~") <> "OK" Then
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
Else
    '-> R�cup�rer les libell�s des champs en recherche
    aBuffer.Title = Entry(3, Ligne, "~")
End If

'-> Pour chaque ligne
For i = 2 To aBuffer.Lignes.Count
    '-> Lecture de la ligne
    Ligne = aBuffer.Lignes(i)
    '-> Cr�er un enregistrement
    Set aEnreg = exFieldTable(Ligne, aBuffer.Title)
    '-> Ajouter l'enregistrement dans le buffer
    aBuffer.Enregs.Add aEnreg, aEnreg.NumEnreg
Next

'-> Vider la collection des lignes
aBuffer.ResetLignes

'-> Renvoyer une valeur de succ�s
FieldTable = True

'-> Quitter la fonction
Exit Function

GestError:
    
End Function

Private Function exFieldTable(Ligne As String, Title As String) As B3DEnreg

Dim j As Integer
Dim NumEnreg As String
Dim Valuefield As String
Dim aField As B3DField
Dim aEnreg As B3DEnreg

'-> Cr�er un nouvel Enreg
Set aEnreg = New B3DEnreg

'-> Recup�rer son num�ro d'enreg
NumEnreg = UCase$(Trim(Entry(1, Ligne, "~")))
Valuefield = Entry(2, Ligne, "~")
'-> Cr�er un nouvel Enreg pour chaque ligne
Set aEnreg = New B3DEnreg
'-> Positionner son nulm�ro d'enregistrement
aEnreg.NumEnreg = NumEnreg
'-> Pour tous les champs retourn�s
For j = 1 To NumEntries(Valuefield, "�")
    '-> Cr�er un nouveu champ
    Set aField = New B3DField
    '-> Positionner son code
    aField.Field = Trim(UCase$(Entry(1, Entry(j, Title, "�"), "�")))
    aField.Libel = Trim(UCase$(Entry(2, Entry(j, Title, "�"), "�")))
    '-> Postionner sa valeur
    aField.ZonData = Entry(2, Entry(j, Valuefield, "�"), "�")
    '-> Ajouter le champ sur l'enregistrement en cours
    aEnreg.Fields.Add aField, aField.Field
Next 'Pour tous les champs de la ligne

'-> Renvoyer l'enregistrement
Set exFieldTable = aEnreg

End Function

Public Sub SetFieldTableCombo(aBuffer As B3DBuffer, Combo As ComboBox)

'---> Cette proc�dure charge une combo � partir d'un Buffer issu d'un B3D_FieldTable

Dim Ligne As String
Dim aEnreg As B3DEnreg
Dim aField As B3DField

On Error GoTo GestError

'-> Quitter si le buffer n'est pas sp�cifi�
If aBuffer Is Nothing Then Exit Sub

'-> Quitter si Combo n'est pas initialis�
If Combo Is Nothing Then Exit Sub

'-> Vider la combo
Combo.Clear

'-> Cr�er la liste des enreg
For Each aEnreg In aBuffer.Enregs
    '-> Remettre � "" la variable
    Ligne = ""
    For Each aField In aEnreg.Fields
        If Ligne = "" Then
            Ligne = aField.ZonData
        Else
            Ligne = Ligne & " - " & aField.ZonData
        End If
    Next 'Pour tous les champs
    '-> Ajouter dans la combo
    Combo.AddItem Ligne
Next 'Pour tous les enregistrements

GestError:

End Sub

Public Sub ShowFieldTableBrowse(aBuffer As B3DBuffer, Libel As String)

'---> Cette fonction cr�er un browse � partir d'un buffer alimnent� par une instruction FieldTable

Dim aCol As ColumnHeader
Dim i As Integer
Dim aEnreg As B3DEnreg
Dim aField As B3DField
Dim X As ListItem

On Error GoTo GestError

'-> Quitter si le buffer n'est pas sp�cifi�
If aBuffer Is Nothing Then Exit Sub

'-> Charger une nouvelle instance de la feuille
Load frmFieldTable

'-> Initialisation du browse
frmFieldTable.ListView1.ColumnHeaders.Clear
frmFieldTable.ListView1.ListItems.Clear

'-> Cr�ation des colonnes
For i = 1 To NumEntries(aBuffer.Title, "�")
    '-> Cr�er une nouvelle colonne
    frmFieldTable.ListView1.ColumnHeaders.Add , , Entry(2, Entry(i, aBuffer.Title, "�"), "�")
Next 'Pour toutes les colonnes

'-> Cr�er la liste des enreg
For Each aEnreg In aBuffer.Enregs
    '-> Creer un nouvel item
    Set X = frmFieldTable.ListView1.ListItems.Add(, "ENREG|" & aEnreg.NumEnreg)
    '-> Cr�ation de toutes les colonnes
    For i = 1 To NumEntries(aBuffer.Title, "�")
        '-> Pointer sur le champ
        Set aField = aEnreg.Fields(i)
        '-> Mettre sa valeur
        If i = 1 Then
            X.Text = aField.ZonData
        Else
            X.SubItems(i - 1) = aField.ZonData
        End If
    Next 'Pour toutes les colonnes
Next 'Pour tous les enregistrements

'-> S�lectionner la premi�re ligne
If frmFieldTable.ListView1.ListItems.Count <> 0 Then Set frmFieldTable.ListView1.SelectedItem = frmFieldTable.ListView1.ListItems(1)

'-> Positionner le titre de la feuille
frmFieldTable.Caption = Libel

'-> Formatter le browse
FormatListView frmFieldTable.ListView1

'-> Vider la variable d'�change
strRetour = ""

'-> Afficher la feuille
frmFieldTable.Show vbModal


GestError:
    

End Sub

Public Function GetIdentCatalogue(Connexion As B3DConnexion, Ident As String, ListeObj As String) As Boolean

'---> Cette proc�dure charge le catalogue associ� � un ident

Dim aBuffer As B3DBuffer
Dim aObj As B3DObject
Dim i As Integer
Dim Ligne As String

On Error GoTo GestError

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Demander un descriptif
Instruction = "B3D_SCHEMA_CATALOGUE~1~" & ListeObj
If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Initialiser un buffer de retour
Set aBuffer = New B3DBuffer

'-> Envoyer la demande
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "GetFnameDescriptif"
    Exit Function
End If

'-> Lire la r�ponse
Ligne = aBuffer.Lignes(1)

'-> Tester le retour
If Entry(2, Ligne, "~") <> "OK" Then
     '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
End If

'-> Initialisation du catalogue
Set Catalogues = New Collection
        
'-> Analyse de toutes les lignes de la r�ponse
For i = 2 To aBuffer.Lignes.Count
    '-> Lecture de la ligne
    Ligne = aBuffer.Lignes(i)
    '-> Selon le type de la ligne
    Select Case UCase$(Trim(Entry(1, Ligne, "~")))
        Case "B3D_SCHEMA_CATALOGUE"
            '-> Affecter son descriptif
            Set aObj = SetDOGObject(Ligne)
            '-> Ajouter dans le descritif
            Catalogues.Add aObj, aObj.Num_Field
    End Select
Next 'Pour toutes les lignes
'-> Ecraser la matrice des lignes
aBuffer.ResetLignes

'-> Supprimer le buffer
Set aBuffer = Nothing

'-> Renvoyer une valeur de succ�s
GetIdentCatalogue = True

'-> Quitter la fonction
Exit Function

GestError:
    '-> Positionner le code erreur
    Connexion.ErrorNumber = Err.Number
    Connexion.ErrorLibel = Err.Description
    Connexion.ErrorParam = "GetIdentCatalogue"

End Function

Public Function GetFnameDescriptif(Connexion As B3DConnexion, Ident As String, Fname As String) As B3DFnameDescriptif

'---> Cette proc�dure permet de r�cup�rer le descriptif d'un enregistrement et de cr�er un nouvel Enreg Vierge

Dim aField As B3DField
Dim MasterDescro As B3DFnameDescriptif
Dim aDescro As B3DFnameDescriptif
Dim DescroCours As B3DFnameDescriptif
Dim aObj As B3DObject
Dim Ligne As String
Dim aBuffer As B3DBuffer
Dim i As Integer
Dim Param As String

On Error GoTo GestError

'-> Tester l'ident
If Trim(Ident) = "" Then Exit Function

'-> Tester la table
If Trim(Fname) = "" Then Exit Function

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Demander un descriptif
Instruction = "B3D_SCHEMA_DES~1~1~" & Fname
If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Initialiser un buffer de retour
Set aBuffer = New B3DBuffer

'-> Envoyer la demande
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "GetFnameDescriptif"
    Exit Function
End If

'-> Lire la r�ponse
Ligne = aBuffer.Lignes(1)

'-> Tester le retour
If Entry(2, Ligne, "~") <> "OK" Then
     '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
End If


'-> Initialiser le descriptfif de l'enregistrement
Set aDescro = New B3DFnameDescriptif

'-> lecture s�quentielle des lignes de r�ponse
For i = 2 To aBuffer.Lignes.Count
    '-> Lecture de la ligne
    Ligne = aBuffer.Lignes(i)
    '-> Selon le type de la ligne
    Select Case UCase$(Trim(Entry(1, Ligne, "~")))
        Case "B3D_SCHEMA_FNAME"
            '-> Intialisation de la table maitre ou non
            If Entry(1, Entry(2, Ligne, "~"), "�") = Fname Then
                '-> Cr�ation de la table maitresse
                Set MasterDescro = New B3DFnameDescriptif
                '-> Mise � jour des propri�t�s
                MasterDescro.Designation = Entry(3, Entry(2, Ligne, "~"), "�")
                MasterDescro.Fname = Entry(1, Entry(2, Ligne, "~"), "�")
                MasterDescro.IsReplica = CBool(Entry(4, Entry(2, Ligne, "~"), "�"))
                MasterDescro.IsStat = CBool(Entry(5, Entry(2, Ligne, "~"), "�"))
                MasterDescro.IndexDirect = Entry(6, Entry(2, Ligne, "~"), "�")
                MasterDescro.IsChampRef = Entry(7, Entry(2, Ligne, "~"), "�")
                MasterDescro.IndexJointure = Entry(8, Entry(2, Ligne, "~"), "�")
                MasterDescro.CodeLangue = Connexion.CodeLangue
                '-> Faire pointer sur la table descriptif en cours
                Set DescroCours = MasterDescro
            Else
                '-> On est sur une table secondaire
                Set aDescro = New B3DFnameDescriptif
                '-> Mise � jour des propri�t�s
                aDescro.Designation = Entry(3, Entry(2, Ligne, "~"), "�")
                aDescro.Fname = Entry(1, Entry(2, Ligne, "~"), "�")
                aDescro.IsReplica = CBool(Entry(4, Entry(2, Ligne, "~"), "�"))
                aDescro.IsStat = CBool(Entry(5, Entry(2, Ligne, "~"), "�"))
                aDescro.IndexDirect = Entry(6, Entry(2, Ligne, "~"), "�")
                aDescro.IsChampRef = Entry(7, Entry(2, Ligne, "~"), "�")
                aDescro.IndexJointure = Entry(8, Entry(2, Ligne, "~"), "�")
                aDescro.CodeLangue = Connexion.CodeLangue
                '-> Ajouter dans la collection
                MasterDescro.Descros.Add aDescro, aDescro.Fname
                '-> Faire pointer sur la table descriptif en cours
                Set DescroCours = aDescro
            End If
                            
        Case "B3D_SCHEMA_FIELD"
            '-> Cr�er une nouvelle instance d'un champ
            Set aField = New B3DField
            '-> Positionner son code
            aField.Field = Entry(3, Entry(2, Ligne, "~"), "�")
            '-> Ordre
            aField.Ordre = Entry(2, Entry(2, Ligne, "~"), "�")
            '-> Replica
            aField.IsReplica = CBool(Entry(4, Entry(2, Ligne, "~"), "�"))
            '-> Cryptage
            aField.IsCryptage = CBool(Entry(5, Entry(2, Ligne, "~"), "�"))
            '-> Histo
            aField.IsHisto = CBool(Entry(6, Entry(2, Ligne, "~"), "�"))
            '-> Recherche
            aField.IsF10 = CBool(Entry(7, Entry(2, Ligne, "~"), "�"))
            '-> Modifiable
            aField.IsModifiable = CBool(Entry(8, Entry(2, Ligne, "~"), "�"))
            '-> Ajouter dans le descro en cours du champ
            DescroCours.Fields.Add aField, UCase$(Trim(aField.Field))
    End Select 'Si on r�cup�re un objet du catalogue
Next 'Pour toutes les lignes

'-> Positionner les libell�s des champs de la table maitress
For Each aField In MasterDescro.Fields
    '-> Setting du libell�
    aField.Libel = Catalogues(aField.Field).Fill_Label
Next 'Pour tous les champs de la table maitresse

'-> Postionner le libell� pour tous les champs de toutes les tables jointes
For Each aDescro In MasterDescro.Descros
    '-> Pour tous les champs du descriptif
    For Each aField In aDescro.Fields
        aField.Libel = Catalogues(aField.Field).Fill_Label
    Next 'Pour tous les champs de la table jointe
Next 'Pour tous les descriptifs

'-> Ecraser la matrice des lignes
aBuffer.ResetLignes

'-> Supprimer le buffer
Set aBuffer = Nothing

'-> Renvoyer le descriptif maitre
Set GetFnameDescriptif = MasterDescro

'-> Quitter la fonction
Exit Function

GestError:
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = Err.Number
    Connexion.ErrorLibel = Err.Description
    Connexion.ErrorParam = "GetFnameDescriptif"
    
    
End Function
Public Function UpdateEnreg(Connexion As B3DConnexion, Fname As String, aEnreg As B3DEnreg, UpdateLock As Boolean) As Boolean

'---> Update d'un enreg

Dim ListeField As String
Dim aBuffer As B3DBuffer
Dim Ligne As String
Dim aField As B3DField

'-> Cr�er la liste des champs � mettre � jour
ListeField = CreateListeFieldToValue(aEnreg)

'-> Ne rien faire si pas de modif
If Trim(ListeField) = "" Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "90026"
    Connexion.ErrorLibel = GetMesseprog("90026", "ERRORS")
    Connexion.ErrorParam = "UpdateEnreg"
    Exit Function
End If

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Connexion.ErrorParam = "UpdateEnreg"
    Exit Function
End If

'-> Poser l'instruction
If UpdateLock Then
    Instruction = "B3D_UPDATE_LOCK~" & Connexion.Ident & "�" & Fname & "�" & aEnreg.NumEnreg & "~" & ListeField
    If Not Connexion.PutInstruction(Instruction) Then
        '-> Positionner le code Erreur
        Connexion.ErrorNumber = "100002"
        Connexion.ErrorLibel = "Error creating instruction"
        Connexion.ErrorParam = Instruction
        Exit Function
    End If
Else
    Instruction = "B3D_UPDATE~" & Connexion.Ident & "�" & Fname & "�" & aEnreg.NumEnreg & "~" & ListeField
    If Not Connexion.PutInstruction(Instruction) Then
        '-> Positionner le code Erreur
        Connexion.ErrorNumber = "100002"
        Connexion.ErrorLibel = "Error creating instruction"
        Connexion.ErrorParam = Instruction
        Exit Function
    End If
End If

'-> Initialiser un buffer
Set aBuffer = New B3DBuffer

'-> Envoyer l'ordre de cr�ation
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "UpdateEnreg"
    Exit Function
End If

'-> Get de la r�ponse
Ligne = aBuffer.Lignes(1)

'-> Si tout est ok
If Entry(2, Ligne, "~") <> "OK" Then
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
End If

'-> Basculer les zon Lock dans les zone DATA
SetZonDataFromZonLock aEnreg

'-> Renvoyer une valeur de succ�s
UpdateEnreg = True

End Function

Public Function CreateEnreg(Connexion As B3DConnexion, Fname As String, aEnreg As B3DEnreg) As Boolean

'---> Cette proc�dure effectue une cr�ation d'un nouvel enregistrement en ne cr�ant que les champs <> ""

Dim ListeField As String
Dim aBuffer As B3DBuffer
Dim Ligne As String

'-> Cr�er la liste des champs � mettre � jour
ListeField = CreateListeFieldToValue(aEnreg)

'-> Ne rien faire si pas de modif
If Trim(ListeField) = "" Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "90026"
    Connexion.ErrorLibel = GetMesseprog("90026", "ERRORS")
    Connexion.ErrorParam = "CreateEnreg"
    Exit Function
End If
'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If

'-> Poser l'instruction
Instruction = "B3D_CREATE~" & Connexion.Ident & "�" & Fname & "~" & ListeField
If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If
'-> Initialiser un buffer
Set aBuffer = New B3DBuffer

'-> Envoyer l'ordre de cr�ation
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "CreateEnreg"
    Exit Function
End If

'-> Get de la r�ponse
Ligne = aBuffer.Lignes(1)

'-> Si tout est ok
If Entry(2, Ligne, "~") <> "OK" Then
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
End If

'-> Postionner le numenreg de retour
aEnreg.NumEnreg = Entry(3, Ligne, "~")

'-> Basculer les valeurs dans ZonData
SetZonDataFromZonLock aEnreg

'-> Positionner la valeur sur l'enregistrement
aEnreg.Ok = True

'-> Renvoyer une valeur de succ�s
CreateEnreg = True

End Function



Public Function SetZonLockFromZonData(aEnreg As B3DEnreg)

Dim aField As B3DField
Dim aFieldJoin As B3DField

'-> Pour tous les enreg
For Each aField In aEnreg.Fields
    aField.ZonLock = aField.ZonData
    '-> Idem pour les jointures
    For Each aFieldJoin In aField.FieldJoins
        aField.ZonLock = aField.ZonData
    Next 'Pour tous les champs de jointures
Next 'Pour tous les champs

End Function

Private Function SetZonDataFromZonLock(aEnreg As B3DEnreg)

'---> Cette fonction bascule ZonLock dans ZonData

Dim aField As B3DField
Dim aFieldJoin As B3DField

'-> Pour tous les enreg
For Each aField In aEnreg.Fields
    '-> Basculer le champ
    aField.ZonData = aField.ZonLock
    '-> Idem pour les jointures
    For Each aFieldJoin In aField.FieldJoins
        aFieldJoin.ZonData = aFieldJoin.ZonLock
    Next 'Pour tous les champs de jointures
Next 'Pour tous les champs


End Function


Private Function CreateListeFieldToValue(aEnreg As B3DEnreg) As String

'---> Cette proc�dure analyse la collection des champs et cr�er la liste des champs qui ont chang�s

Dim aField As B3DField
Dim ValueToUpdate As String

'-> Analyse de tous les champs
For Each aField In aEnreg.Fields
    If Trim(UCase$(aField.ZonData)) <> Trim(UCase$(aField.ZonLock)) Then
        '-> Ajouter dans la liste
        If ValueToUpdate = "" Then
            ValueToUpdate = aField.Field & "�" & RTrim(aField.ZonLock)
        Else
            ValueToUpdate = ValueToUpdate & "�" & aField.Field & "�" & RTrim(aField.ZonLock)
        End If
    End If
Next 'Pour tous les champs

'-> Retourner la liste
CreateListeFieldToValue = ValueToUpdate

End Function

Public Function DeleteEnreg(Connexion As B3DConnexion, Fname As String, NumEnreg As String) As Boolean

'---> Cette proc�dure supprime un enregistrement sp�cifi�

Dim aBuffer As B3DBuffer
Dim Ligne As String

'-> Initialiser une demande
If Not Connexion.InitDmd Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100001"
    Connexion.ErrorLibel = "Error cr�ating demand"
    Exit Function
End If


'-> Poser l'instruction
Instruction = "B3D_DELETE~" & Connexion.Ident & "�" & Fname & "�" & NumEnreg
If Not Connexion.PutInstruction(Instruction) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100002"
    Connexion.ErrorLibel = "Error creating instruction"
    Connexion.ErrorParam = Instruction
    Exit Function
End If

'-> Initialiser un buffer
Set aBuffer = New B3DBuffer

'-> Envoyer l'ordre de cr�ation
If Not Connexion.SendDemande(aBuffer) Then
    '-> Positionner le code Erreur
    Connexion.ErrorNumber = "100000"
    Connexion.ErrorLibel = "Error sending exchange"
    Connexion.ErrorParam = "DeleteEnreg"
    Exit Function
End If

'-> Get de la r�ponse
Ligne = aBuffer.Lignes(1)

'-> Si tout est ok
If Entry(2, Ligne, "~") <> "OK" Then
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
End If

'-> Renvoyer une valeur de succ�s
DeleteEnreg = True

End Function

Private Function SetDOGObject(Ligne As String) As B3DObject

'---> Cette fonction eclate une ligne de param�trage de type B3D_SCHEMA_CATALOGUE en un objet Emilie

Dim aObj As B3DObject
Dim LigneDef As String

'On Error GoTo GestError

'-> Cr�er une nouvelle instance de l'objet
Set aObj = New B3DObject

'-> R�cup�rer la d�finition de la ligne
LigneDef = Entry(2, Ligne, "~")

aObj.Num_Field = Trim(Entry(2, LigneDef, "�"))
aObj.Fill_Label = Entry(3, LigneDef, "�")
aObj.Progiciel = Trim(Entry(4, LigneDef, "�"))
aObj.DataType = Trim(Entry(5, LigneDef, "�"))
aObj.WidgetFormat = Trim(Entry(6, LigneDef, "�"))
aObj.Filled_Type = Trim(Entry(7, LigneDef, "�"))
aObj.Filled_Ascii = Trim(Entry(8, LigneDef, "�"))
aObj.Table_Correspondance = Entry(1, Trim(Entry(9, LigneDef, "�")), "�")
aObj.MultiLangue = CInt(Trim(Entry(10, LigneDef, "�")))
aObj.MasterField = Entry(11, LigneDef, "�")
aObj.Sep_Vecteur = Entry(12, LigneDef, "�")

'-> Renvoyer l'objet
Set SetDOGObject = aObj

'-> Quitter la focntion
Exit Function

GestError:

End Function

Public Function DisplayZoom(Connexion As B3DConnexion, Fname As String, NumEnreg As String, GetHisto As Boolean, TopLock As Boolean, aDescro As B3DFnameDescriptif, ParentForm As Object, GetJoin As Boolean)

'---> Cette fonction effectue un zoom sur un enregistrement

On Error GoTo GestError

Dim aEnreg As B3DEnreg

'-> Bloquer l'�cran
ParentForm.Enabled = False
Screen.MousePointer = 11

'-> Effectuer un findData
Set aEnreg = FindData(Connexion, Connexion.Ident, Fname, NumEnreg, False, True, aDescro, True)
    
'-> Tester le retour
If aEnreg Is Nothing Then
    '-> Afficher le message d'erreur
    MsgBox Connexion.ErrorNumber & " - " & Connexion.ErrorLibel & Chr(13) & Connexion.ErrorParam, vbExclamation + vbOKOnly, "Message"
    '-> Fin du programme
    GoTo GestError
End If

'-> Tester si l'enreg est Ok
If Not aEnreg.Ok Then
    '-> Tester si l'enregistrement est lock�
    If aEnreg.IsLocked Then
        MsgBox Connexion.ErrorLibel & Chr(13) & aEnreg.OpeMaj & " - " & aEnreg.DateHeu & " - " & aEnreg.Programme, vbInformation + vbOKOnly, "Zoom"
        '-> Fin du programme
        GoTo GestError
    Else
        '-> Afficher le message d'erreur
        MsgBox Connexion.ErrorNumber & " - " & Connexion.ErrorLibel & Chr(13) & Connexion.ErrorParam, vbExclamation + vbOKOnly, "Message"
        '-> Fin du programme
        GoTo GestError
    End If
End If

'-> Charger la feuille en m�moire
Load frmZoom

'-> Afficher le browse du FindData
ShowBrowseFindData aEnreg, frmZoom.ListView1, True

frmZoom.Caption = "Zoom : " & aEnreg.NumEnreg

'-> D�bloquer l'�cran
ParentForm.Enabled = True
Screen.MousePointer = 0

'-> Afficher la feuille
frmZoom.Show vbModal

'-> Quitter la fonction
Exit Function

GestError:
    '-> D�bloquer l'�cran
    ParentForm.Enabled = True
    Screen.MousePointer = 0



End Function



