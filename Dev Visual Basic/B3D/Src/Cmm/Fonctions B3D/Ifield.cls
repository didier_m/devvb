VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IFIELD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Ident As String
Public Num_Field As String
Public Ordre As Long
Public Cumul As Boolean
Public TypeCondition As Integer
Public ValMini As String
Public ValMaxi As String
Public Num_Iname As String
Public Num_Fname As String
