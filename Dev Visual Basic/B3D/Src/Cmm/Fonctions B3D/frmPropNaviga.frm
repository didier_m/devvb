VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPropNaviga 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Navigation"
   ClientHeight    =   7650
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9855
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   510
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   657
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   1680
      Top             =   3360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPropNaviga.frx":0000
            Key             =   "PAGE"
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Command3 
      Height          =   615
      Left            =   120
      Picture         =   "frmPropNaviga.frx":039A
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "More page"
      Top             =   6960
      Width           =   735
   End
   Begin VB.CommandButton Command2 
      Height          =   615
      Left            =   8160
      Picture         =   "frmPropNaviga.frx":1064
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   6960
      Width           =   735
   End
   Begin VB.CommandButton Command1 
      Height          =   615
      Left            =   9000
      Picture         =   "frmPropNaviga.frx":11AE
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   6960
      Width           =   735
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   6855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9855
      _ExtentX        =   17383
      _ExtentY        =   12091
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmPropNaviga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public aNaviga As B3DNavigation
Public aConnexion As B3DConnexion

Public Sub Init(PageCours As Integer)

'---> Cette proc�dure affiche la liste des pages en cours

On Error Resume Next

Dim i As Long

For i = 1 To aNaviga.PageCount
    Me.ListView1.ListItems.Add , "PAGE|" & i, "Page " & i, "PAGE"
Next

'-> Postionner la page en cours
Set Me.ListView1.SelectedItem = Me.ListView1.ListItems("PAGE|" & PageCours)

End Sub

Private Sub Command1_Click()
Unload Me
End Sub

Private Sub Command3_Click()

'---> Faire un naviga continu
Dim PageCount As Long
Dim i As Long

'-> Bloquer l'�cran
Me.Enabled = False
Screen.MousePointer = 11

'-> R�cup�rer le nombre de pages
PageCount = aNaviga.PageCount

'-> Faire un B3D Continu
If Not aNaviga.NavigaContinu(aConnexion) Then GoTo ErrorNaviga

'-> Ne rien faire si le nombre de page est le m�me
If aNaviga.PageCount = PageCount Then GoTo ReleaseScreen

'-> Ajouter autant de page
For i = PageCount + 1 To aNaviga.PageCount
    Me.ListView1.ListItems.Add , "PAGE|" & i, "Page " & i, "PAGE"
Next

'-> Lib�rer l'�cran
GoTo ReleaseScreen

ErrorNaviga:
    '-> Message d'erreur
    MsgBox aConnexion.ErrorNumber, vbExclamation + vbOKOnly, "Error"
ReleaseScreen:

    '-> D�bloquer l'�cran
    Me.Enabled = True
    Screen.MousePointer = 0


End Sub


Private Sub ListView1_DblClick()

'---> Afficher la page en particulier

'-> V�rifier qu'un objet soit s�lectionn�
If Me.ListView1.SelectedItem Is Nothing Then Exit Sub

'-> Renvoyer la page
strRetour = Me.ListView1.SelectedItem.Key

'-> D�charger la feuille
Unload Me

End Sub
