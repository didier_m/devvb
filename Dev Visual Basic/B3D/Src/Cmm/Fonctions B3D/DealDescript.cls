VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DealDescript"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'Deal Informatique Juillet 2002

'**************************************************************
'* Objet qui permet de r�cup�rer le descriptif d'un objet B3D *
'**************************************************************

'-> Propri�t�s du Descriptif du Mod�le Objet
Public Fnames     As Collection
Public Fields     As Collection
Public Inames      As Collection
Public IFields    As Collection
Public IntNames   As Collection
Public IntFields  As Collection
Public Catalogues As Collection
Public Catalogues_Link As Collection

'-> Nombre de table dans qla collection
Private pError As String

'-> Liste des tables pour la fonction B3D_SCHEMA_DES au s�parateur "�"
Dim pListeFname As String

'-> Liste des Objets du catalogue pour la fonction B3D_CATALOGUE au s�parateur "�"
Dim pListeObj As String

'-> Objet de synchronisation
Public aDcom As DealCom

Public Property Get Error() As String
    Error = pError
End Property

Public Property Get ListeFname() As String
    ListeFname = pListeFname
End Property

Public Property Get ListeObj() As String
    ListeObj = pListeObj
End Property

'-> M�thodes de l'objet
Public Function GetSchemaList(ByVal TypeTri As Integer) As Boolean

Dim i As Long

'-> Initialiser un nouveau buffer
aDcom.InitCom

'-> Setting de la temporisation
aDcom.Delai = 100
aDcom.NbHit = 10000

'-> Settign de l'instruction
aDcom.SetData "B3D_SCHEMA_LIST~" & TypeTri

'-> Envoie de l'information
If Not aDcom.Send Then
    '-> Recup de l'erreur
    pError = aDcom.Error
    '-> Renvoie d'une valeur d'erreur
    GetSchemaList = False
    Exit Function
End If

'-> RAZ des collections
Call RazDom

'-> Cr�tion de la collection des tables
For i = 2 To aDcom.NbLig
    '-> Ajouter dans la collection
    Fnames.Add aDcom.GetLigne(i), Trim(Entry(3, aDcom.GetLigne(i), "�"))
    '-> Concat�ner la chaine
    If pListeFname = "" Then
        pListeFname = Trim(Entry(3, aDcom.GetLigne(i), "�"))
    Else
        pListeFname = pListeFname & "�" & Trim(Entry(3, aDcom.GetLigne(i), "�"))
    End If
Next 'Pour toutes les lignes

'-> Renvoyer une valeur de succ�s
GetSchemaList = True

End Function

Public Function GetF10Fields(Fname As String) As String

'---> Cette proc�dure r�cup�re pour une table donn�e la liste des champs sp�cifi�e _
en recherche et les jointures associ�es si elles existes

Dim aField As Variant
Dim NumField As String
Dim aDes As DealDescript
Dim ReturnValue As String
Dim TempStr As String
Dim aDes2 As DealDescript
Dim JointFname As String

'-> R�cup�rer la description de la table
If Not Me.GetSchemaDes(1, 1, Fname) Then
    GetF10Fields = ""
    Exit Function
End If

'-> Analyse de tous les champs
For Each aField In Me.Fields
    '-> R�cup�rer le num�ro du champ
    NumField = Entry(3, aField, "�")
    
    '-> Tester si le champ est en recherche
    If GetFieldProp(Fname, NumField, "F10") = "-1" Then
        '-> Stocker le num�ro du champ
        If ReturnValue = "" Then
            ReturnValue = NumField & "�0"
        Else
            ReturnValue = ReturnValue & "�" & NumField & "�0"
        End If
    End If
Next 'Pour tous les champs

'-> Renvoyer la valeur
GetF10Fields = ReturnValue

End Function



Private Function IsFname(Fname As String) As Boolean

'---> Cette proc�dure v�rifie si une table donn�e est charg�e dans le descripteur en cous

Dim aVar As Variant

On Error GoTo GestError

aVar = Fnames(Fname)
IsFname = True

Exit Function

GestError:
    IsFname = False

End Function


Public Function GetSchemaDes(ByVal TypeTri As String, TableCorrespondance As Integer, nListFname As String) As Boolean

'---> Cette proc�dure r�cup�re le descriptif d'une table

Dim i As Long
Dim aLigne As String

Dim CurFname As String
Dim CurField As String
Dim CurIname As String
Dim CurIntName As String

Dim KeyDef As String
Dim ValueDef As String

'-> V�rifier le passage d'argument
If Trim(nListFname) = "" Then
    '-> Envoyer une erreur
    pError = "ERROR PARAM LISTE FNAME"
    GetSchemaDes = False
    Exit Function
End If


'-> Initialiser la liste des variables
Call RazDom

'-> Initialiser un nouveau buffer
aDcom.InitCom

'-> Setting de la temporisation
aDcom.Delai = 100
aDcom.NbHit = 10000

'-> Settign de l'instruction
aDcom.SetData "B3D_SCHEMA_DES~" & TypeTri & "~" & TableCorrespondance & "~" & nListFname

'-> Envoie de l'information
If Not aDcom.Send Then
    '-> Recup de l'erreur
    pError = aDcom.Error
    '-> Renvoie d'une valeur d'erreur
    GetSchemaDes = False
    Exit Function
End If

For i = 1 To aDcom.NbLig
    '-> R�cup�rer la ligne
    aLigne = aDcom.GetLigne(i)
    KeyDef = Entry(1, aLigne, "~")
    ValueDef = Entry(2, aLigne, "~")
    '-> Selon la ligne
    Select Case UCase$(KeyDef)
        Case "B3D_SCHEMA_FNAME"
            '-> R�cup�rer le code de la table
            CurFname = Entry(1, ValueDef, "�")
            '-> Ajouter dans la table des Fnames
            Fnames.Add ValueDef, CurFname
        Case "B3D_SCHEMA_FIELD"
            '-> R�cup�rer le code du champ
            CurField = Entry(3, ValueDef, "�")
            '-> Ajouter dans la collection des champs
            Fields.Add ValueDef, CurFname & "|" & CurField
        Case "B3D_SCHEMA_INAME"
            '-> R�cup�rer le code de l'index en cours
            CurIname = Entry(2, ValueDef, "�")
            '-> Ajouter dans la collections des index
            Inames.Add ValueDef, CurFname & "|" & CurIname
        Case "B3D_SCHEMA_IFIELD"
            '-> R�cup�rer le code du champ
            CurField = Entry(4, ValueDef, "�")
            '-> Ajouter dans la collection
            IFields.Add ValueDef, CurFname & "|" & CurIname & "|" & CurField
        Case "B3D_SCHEMA_INTNAME"
            '-> R�cup�rer le code de l'interface
            CurIntName = Entry(2, ValueDef, "�")
            '-> Ajouter dans la collection
            IntNames.Add ValueDef, CurFname & "|" & CurIntName
        Case "B3D_SCHEMA_INTFIELD"
            '-> R�cup�rer le code du champ de l'interface
            CurField = Entry(3, ValueDef, "�")
            '-> Ajouter dans la collection
            IntFields.Add ValueDef, CurFname & "|" & CurIntName & "|" & CurField
        Case "B3D_SCHEMA_CATALOGUE"
            '-> R�cup�rer le code de l'objet Emilie
            CurField = Entry(1, ValueDef, "�")
            '-> Ajouter dans le catalogue
            Catalogues.Add ValueDef, CurField
            '-> Concantainer la liste des champs
            If pListeObj = "" Then
                pListeObj = CurField
            Else
                pListeObj = pListeObj & "�" & CurField
            End If
    End Select
Next

'-> Lib�rer les lignes de l'objet de communication
Call aDcom.ReleaseLig

'-> Renvoyer une valeur de succ�s
GetSchemaDes = True

End Function

Public Function GetCatalogue(ByVal TypeTri As String, ListeField As String) As Boolean

'---> Cette proc�dure r�cup�re les propri�t�s d'un objet Emilie

Dim i As Long
Dim ValueDef As String
Dim aLigne As String

'-> Quitter si la liste des champs est � blanc
If ListeField = "" Then
    pError = "ERROR PARAM LISTE EMILIEOBJ"
    '-> Renvoie d'une valeur d'erreur
    GetCatalogue = False
    Exit Function
End If

'-> RAZ du catalogue
Do While Catalogues.Count <> 0
    Catalogues.Remove (1)
Loop

'-> Initialiser un nouveau buffer
aDcom.InitCom

'-> Setting de la temporisation
aDcom.Delai = 100
aDcom.NbHit = 10000

'-> Settign de l'instruction
aDcom.SetData "B3D_CATALOGUE~" & TypeTri & "~" & ListeField

'-> Envoie de l'information
If Not aDcom.Send Then
    '-> Recup de l'erreur
    pError = aDcom.Error
    '-> Renvoie d'une valeur d'erreur
    GetCatalogue = False
    Exit Function
End If

For i = 1 To aDcom.NbLig
    '-> R�cup�rer la ligne
    aLigne = aDcom.GetLigne(i)
    ValueDef = Entry(2, aLigne, "~")
    Catalogues.Add ValueDef, Entry(1, ValueDef, "�")
Next

'-> Lib�rer les lignes de l'objet de communication
Call aDcom.ReleaseLig

'-> Renvoyer une valeur de succ�s
GetCatalogue = True

End Function

Public Function GetCatalogueLink() As Boolean

'---> Cette proc�dure r�cup�rer la totalit� des liens pour la totalit� du catalogue

Dim i As Long
Dim ValueDef As String
Dim Num_Field As String

'Doit �tre pr�c�d�e de GetCatalogue ou GetSchemaDes pour toutes les tables ( afin de r�cup�rer tout le catalogue )
'-> Quitter si la liste des champs est � blanc

'-> RAZ du catalogue
Do While Catalogues_Link.Count <> 0
    Catalogues_Link.Remove (1)
Loop

'-> Initialiser un nouveau buffer
aDcom.InitCom

'-> Setting de la temporisation
aDcom.Delai = 100
aDcom.NbHit = 10000

'-> Settign de l'instruction
aDcom.SetData "B3D_CATALOGUE_LINK~"

'-> Envoie de l'information
If Not aDcom.Send Then
    '-> Recup de l'erreur
    pError = aDcom.Error
    '-> Renvoie d'une valeur d'erreur
    GetCatalogueLink = False
    Exit Function
End If

For i = 1 To aDcom.NbLig
    '-> get du num�ro du champ
    ValueDef = Entry(2, aDcom.GetLigne(i), "~")
    Catalogues_Link.Add ValueDef
Next

'-> Lib�rer les lignes de l'objet de communication
Call aDcom.ReleaseLig

'-> Renvoyer une valeur de succ�s
GetCatalogueLink = True

End Function


Private Sub Class_Initialize()

'---> Initialisation des collections
Set Fnames = New Collection
Set Fields = New Collection
Set Inames = New Collection
Set IFields = New Collection
Set IntNames = New Collection
Set IntFields = New Collection
Set Catalogues = New Collection
Set Catalogues_Link = New Collection

End Sub

Public Function GetFnameProp(Fname As String, PropertyName As String) As String

'---> Cette proc�dure retourne la valeur d'une propri�t� pour une table

Dim aFname As Variant
Dim ReturnValue As String

On Error GoTo GestError

'-> Pointer sur la table sp�cifi�e
aFname = Fnames(Fname)

'-> Analyse de l'imprimante
Select Case UCase$(PropertyName)
    Case "NUM"
        ReturnValue = Entry(1, aFname, "�")
    Case "KEYWORD"
        ReturnValue = Entry(2, aFname, "�")
    Case "DESIGNATION"
        ReturnValue = Entry(3, aFname, "�")
    Case "REPLICA"
        ReturnValue = Entry(4, aFname, "�")
    Case "STAT"
        ReturnValue = Entry(5, aFname, "�")
    Case "INDEXDIRECT"
        ReturnValue = Entry(6, aFname, "�")
    Case "CHAMP_REF"
        ReturnValue = Entry(7, aFname, "�")
    Case "NUM_INDEX_JOIN"
        ReturnValue = Entry(8, aFname, "�")
    Case Else
        ReturnValue = ""
End Select

GetFnameProp = ReturnValue

Exit Function

GestError:

    GetFnameProp = ""


End Function

Public Function GetInameProp(Fname As String, Iname As String, PropertyName As String)

'---> Cette proc�dure retourne une propri�t� d'un champ

Dim aIname As String
Dim ReturnValue As String

On Error GoTo GestError

'-> Pointer sur le champ
aIname = Inames(Trim(Fname) & "|" & Trim(Iname))

'-> Selon la propri�t�
Select Case UCase$(PropertyName)
    Case "NUM_FNAME"
        ReturnValue = Entry(1, aIname, "�")
    Case "NUM_INAME"
        ReturnValue = Entry(2, aIname, "�")
    Case "MOTCLE"
        ReturnValue = Entry(3, aIname, "�")
    Case "DESIGNATION"
        ReturnValue = Entry(4, aIname, "�")
    Case "UNIQUE"
        ReturnValue = Entry(5, aIname, "�")
    Case Else
        ReturnValue = ""
End Select

'-> Renvoyer la valeur
GetInameProp = ReturnValue

Exit Function

GestError:
    
    GetInameProp = ""
    

End Function

Public Function GetFieldProp(Fname As String, Field As String, PropertyName As String)

'---> Cette proc�dure retourne une propri�t� d'un champ

Dim aField As String
Dim ReturnValue As String

On Error GoTo GestError

'-> Pointer sur le champ
aField = Fields(Trim(Fname) & "|" & Trim(Field))

'-> selon la propri�t�
Select Case UCase$(PropertyName)
    Case "NUM_FNAME"
        ReturnValue = Entry(1, aField, "�")
    Case "ORDRE"
        ReturnValue = Entry(2, aField, "�")
    Case "NUM_FIELD"
        ReturnValue = Entry(3, aField, "�")
    Case "REPLICA"
        ReturnValue = Entry(4, aField, "�")
    Case "CRYPTAGE"
        ReturnValue = Entry(5, aField, "�")
    Case "HISTO"
        ReturnValue = Entry(6, aField, "�")
    Case "F10"
        ReturnValue = Entry(7, aField, "�")
    Case "MODIF"
        ReturnValue = Entry(8, aField, "�")
    Case Else
        ReturnValue = ""
End Select

'-> Renvoyer la valeur
GetFieldProp = ReturnValue

Exit Function

GestError:

    GetFieldProp = ""
    
End Function

Public Function GetCatalogueProp(EmilieObj As String, PropertyName As String) As String

Dim aEmilieObj As String
Dim ReturnValue As String

On Error GoTo GestError

'-> Pointer sur l'objet du catalogue
aEmilieObj = Catalogues(EmilieObj)

'-> Retour de sa prorpri�t�
Select Case PropertyName
    Case "NUM_FIELD"
        ReturnValue = Entry(1, aEmilieObj, "�")
    Case "MOTCLE"
        ReturnValue = Entry(2, aEmilieObj, "�")
    Case "LABEL_F"
        ReturnValue = Entry(3, aEmilieObj, "�")
    Case "LABEL_C"
        ReturnValue = Entry(4, aEmilieObj, "�")
    Case "PROGICIEL"
        ReturnValue = Entry(5, aEmilieObj, "�")
    Case "FAMDICT"
        ReturnValue = Entry(6, aEmilieObj, "�")
    Case "WIDGET"
        ReturnValue = Entry(7, aEmilieObj, "�")
    Case "TYPEDATA"
        ReturnValue = Entry(8, aEmilieObj, "�")
    Case "LARGEUR"
        ReturnValue = Entry(9, aEmilieObj, "�")
    Case "HAUTEUR"
        ReturnValue = Entry(10, aEmilieObj, "�")
    Case "REMPLISSAGE"
        ReturnValue = Entry(11, aEmilieObj, "�")
    Case "TYPE_CTRL"
        ReturnValue = Entry(12, aEmilieObj, "�")
    Case "FORMAT"
        ReturnValue = Entry(13, aEmilieObj, "�")
    Case "CADRAGE"
        ReturnValue = Entry(14, aEmilieObj, "�")
    Case "DEFAUT"
        ReturnValue = Entry(15, aEmilieObj, "�")
    Case "PROPRIETE"
        ReturnValue = Entry(16, aEmilieObj, "�")
    Case "FNAME_ASS"
        ReturnValue = Entry(17, aEmilieObj, "�")
    Case "IMAGE"
        ReturnValue = Entry(18, aEmilieObj, "�")
    Case "SON"
        ReturnValue = Entry(19, aEmilieObj, "�")
    Case "MULTI_LANGUE"
        ReturnValue = Entry(20, aEmilieObj, "�")
End Select

'-> Renvoyer la valeur
GetCatalogueProp = ReturnValue

Exit Function

GestError:

    GetCatalogueProp = ""
    
End Function

Public Function GetIFieldProp(Fname As String, Iname As String, IField As String, PropertyName As String)

'---> Cette proc�dure retourne une propri�t� d'un champ

Dim aIField As String
Dim ReturnValue As String

On Error GoTo GestError

'-> Pointer sur le champ
aIField = IFields(Trim(Fname) & "|" & Trim(Iname) & "|" & Trim(IField))
 
'-> selon la propri�t�
Select Case UCase$(PropertyName)
    Case "NUM_FNAME"
        ReturnValue = Entry(1, aIField, "�")
    Case "NUM_INAME"
        ReturnValue = Entry(2, aIField, "�")
    Case "ORDRE"
        ReturnValue = Entry(3, aIField, "�")
    Case "NUM_FIELD"
        ReturnValue = Entry(4, aIField, "�")
    Case "CUMUL"
        ReturnValue = Entry(5, aIField, "�")
    Case "CONDITION"
        ReturnValue = Entry(6, aIField, "�")
    Case "VALMINI"
        ReturnValue = Entry(7, aIField, "�")
    Case "VALMAXI"
        ReturnValue = Entry(8, aIField, "�")
    Case Else
        ReturnValue = ""
End Select

'-> Renvoyer la valeur
GetIFieldProp = ReturnValue

Exit Function

GestError:

    GetIFieldProp = ""
    
End Function

Private Sub RazDom()

'-> RAZ des variables
Do While Fnames.Count <> 0
    Fnames.Remove (1)
Loop
Do While Fields.Count <> 0
    Fields.Remove (1)
Loop
Do While Inames.Count <> 0
    Inames.Remove (1)
Loop
Do While IFields.Count <> 0
    IFields.Remove (1)
Loop
Do While IntNames.Count <> 0
    IntNames.Remove (1)
Loop
Do While IntFields.Count <> 0
    IntFields.Remove (1)
Loop
Do While Catalogues.Count <> 0
    Catalogues.Remove (1)
Loop
Do While Catalogues_Link.Count <> 0
    Catalogues_Link.Remove (1)
Loop

pListeFname = ""
pListeObj = ""

End Sub
