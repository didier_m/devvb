VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "B3DObject"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Objet descriptif d'un champ propri�t�s attach�es au catalogue
Public Progiciel  As String 'Progiciel de rattachement
Public WidgetFormat As String 'Format de la zone
Public Filled_Type As String 'Cadrage
Public Filled_Ascii As String 'Remplissage
Public MultiLangue As Integer 'Code langue si -> 0 pas multi
Public DataType As String 'Type de donn�e
Public Num_Field As String 'Num�ro du champ
Public Table_Correspondance As String 'Table associ�e
Public MasterField As String '-> Champ maitre pour l'acc�s au soci�t� de r�f�rence
Public Sep_Vecteur As String '-> Indique qu'il faut traiter le champ en vecteur ( lookup )
Public Fill_Label As String '-> Libelle du champ


