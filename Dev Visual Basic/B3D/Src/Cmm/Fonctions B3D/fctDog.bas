Attribute VB_Name = "fctDog"
Option Explicit

Public Function LoadCatalogueB3D(ServeurDOG As clsDOG, FichierCatalogue As String) As Boolean

Dim Ligne As String
Dim i As Integer
Dim aEmilieObj As EmilieObj
Dim hdlFichier As Integer
Dim Ident As String
Dim hdlFile As Long

'On Error GoTo GestError

'-> R�cup�ration d'un handle de fichier et ouverture du fichier
hdlFichier = FreeFile
Open FichierCatalogue For Input As #hdlFichier

'-> Boucle d'analyse
Do While Not EOF(hdlFichier)
    
    '-> Rendre la main � la CPU
    DoEvents
    '-> Lecture de la ligne
    Line Input #hdlFichier, Ligne
    
    If Trim(Ligne) <> "" Then
        '-> Tester si mot cle = ""
        If Trim(Entry(1, Ligne, "�")) <> "" Then
            '-> Cr�ation d'un objet catalogue
            Set aEmilieObj = New EmilieObj
            '-> Charger la ligne dans l'objet
            SetLigne aEmilieObj, Ligne
            '-> Enregistrer l'objet dans la collection
            Ident = UCase$(Trim(Entry(1, Ligne, "�")))
            Ident = Trim(Ident & "|" & Entry(2, Ligne, "�"))
            ServeurDOG.Catalogue.Add aEmilieObj, UCase$(Ident)
            Ident = ""
        End If
    End If ' Si ligne � blanc
    Ligne = ""
Loop

'-> Fermer le fichier
Close #hdlFichier

'-> Renvoyer une valeur de succ�s
LoadCatalogueB3D = True

'-> Quitter la proc�dure
Exit Function

GestError:
    
    
End Function
Public Sub SetLigne(aEmilieObj As EmilieObj, LigneDef As String)

Dim i As Integer
Dim ValeurZone As String

'-> Trapper les erreurs
On Error Resume Next

'-> R�cup�ration des zones
aEmilieObj.Ident = Trim(Entry(1, LigneDef, "�"))
aEmilieObj.Num_Field = Trim(Entry(2, LigneDef, "�"))
ValeurZone = Trim(Entry(3, LigneDef, "�"))
For i = 1 To 10
    aEmilieObj.CodeLangue = i
    aEmilieObj.Fill_Label = Trim(Entry(i, ValeurZone, "�"))
Next
aEmilieObj.Progiciel = Trim(Entry(4, LigneDef, "�"))
aEmilieObj.DataType = Trim(Entry(5, LigneDef, "�"))
aEmilieObj.WidgetFormat = Trim(Entry(6, LigneDef, "�"))
aEmilieObj.Filled_Type = Trim(Entry(7, LigneDef, "�"))
aEmilieObj.Filled_Ascii = Trim(Entry(8, LigneDef, "�"))
aEmilieObj.Table_Correspondance = Trim(Entry(9, LigneDef, "�"))
aEmilieObj.MultiLangue = CInt(Trim(Entry(10, LigneDef, "�")))
aEmilieObj.MasterField = Entry(11, LigneDef, "�")
aEmilieObj.Sep_Vecteur = Entry(12, LigneDef, "�")

'-> Rendre la main � la CPU
DoEvents

End Sub


Public Function LoadLinkB3D(ServeurDOG As clsDOG, FichierLink As String) As Boolean
'---> Chargement du fichier des liens entre le B3D et le catalogue

Dim hdlFile As Integer
Dim aEmilieObj As EmilieObj
Dim Ligne As String
Dim aLinkFname As LinkFname
Dim aLinkIname As LinkIname
Dim aLinkInterface As LinkInterface
Dim AccesKey As String
Dim OrderKey As String
Dim LnkValue As String
Dim ObjValue As String

'On Error GoTo GestError


'-> Ouverture du fichier
hdlFile = FreeFile
Open FichierLink For Input As #hdlFile

'-> Boucle d'analyse du fichier
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    
    '-> Rendre la main � la CPU
    DoEvents

    If Trim(Ligne) <> "" Then
        '-> R�cup�rer les valeurs
        AccesKey = Entry(1, Ligne, "�")
        OrderKey = Entry(2, Ligne, "�")
        LnkValue = Entry(3, Ligne, "�")
        '-> Test de valeurs lues
        If Trim(AccesKey) = "" Or Trim(OrderKey) = "" Or Trim(LnkValue) = "" Then
        Else
            '-> Tous les champs sont renseign�s : Charger l'objet Emilie
            Set aEmilieObj = ServeurDOG.Catalogue(AccesKey)
            
            '-> Selon le type d'ordre
            Select Case UCase$(OrderKey)
                Case "CREATE"
                    '-> Cr�ation du fname
                    Set aLinkFname = New LinkFname
                    aLinkFname.Num_Fname = UCase$(LnkValue)
                    '-> Ajout dans la collection
                    aEmilieObj.fNames.Add aLinkFname, LnkValue
                                        
                Case "DIRECT"
                    '-> Indiquer que c'est un index direct
                    Set aLinkFname = aEmilieObj.fNames(LnkValue)
                    aLinkFname.IsIndexDirect = True
                    
                Case "CHAMPREF"
                    '-> Indiquer que c'est un champ refrence
                    Set aLinkFname = aEmilieObj.fNames(LnkValue)
                    aLinkFname.IsChampRef = True
                                    
                Case "INAME"
                    '-> R�cup�rer le nom de l'index
                    ObjValue = Entry(4, Ligne, "�")
                    If Trim(ObjValue) <> "" Then
                        '-> Pointer sur le Fname associ�
                        Set aLinkFname = aEmilieObj.fNames(LnkValue)
                        
                        '-> Ajouter un lien vers le Iname
                        Set aLinkIname = New LinkIname
                        aLinkIname.Num_Iname = ObjValue
                        aLinkFname.LinkInames.Add aLinkIname, ObjValue
                    
                    End If 'Si lien renseign�
                                                            
                Case "INTERFACE"
                    '-> R�cup�rer le nom de l'interafce
                    ObjValue = Entry(4, Ligne, "�")
                    If Trim(ObjValue) <> "" Then
                        '-> Pointer sur le Fname associ�
                        Set aLinkFname = aEmilieObj.fNames(LnkValue)
                        
                        '-> Ajouter un interface vers le fname
                        Set aLinkInterface = New LinkInterface
                        aLinkInterface.Name = ObjValue
                        aLinkFname.LinkInterfaces.Add aLinkInterface, ObjValue
                    
                    End If 'Si lien renseign�
            End Select
        End If 'Si valeur = ""
    End If 'Si ligne = ""
Loop 'Boucle d'analyse du fichier

'-> Fermer le fichier
Close #hdlFile

'-> Renvoyer une valeur de succ�s
LoadLinkB3D = True

'-> Quitter la proc�dure
Exit Function

GestError:

    
End Function

Public Function LoadSystemParam(ServeurDOG As clsDOG, B3DIniFile As String, Ident As String) As Boolean

'---> Cette proc�dure analyse le fichier Ident.ini � la recherche _
d'une section : [PARAM_ident]

Dim lpBuffer As String
Dim aSys As clsSystemParam
Dim DefParam As String
Dim Key As String
Dim Value As String
Dim i As Integer

On Error GoTo GestError

'-> R�cup�rer le contenu de la section
lpBuffer = DeCrypt(GetIniFileValue("PARAM_" & Ident, "", B3DIniFile, True))
If Trim(lpBuffer) = "" Then
    LoadSystemParam = True
    Exit Function
End If

'-> Cr�er les tables de param�trage
For i = 1 To NumEntries(lpBuffer, Chr(0))
    '-> R�cup�rer une d�finition
    DefParam = DeCrypt(Entry(i, lpBuffer, Chr(0)))
    '-> Quitter si ""
    If Trim(DefParam) = "" Then Exit Function
    '-> R�cup�rer son code
    Key = Trim(Entry(1, DefParam, "="))
    Value = Trim(Entry(2, DefParam, "="))
    '-> Cr�er une nouvelle instance
    Set aSys = New clsSystemParam
    '-> Setting des valeurs
    aSys.Key = Key
    aSys.Value = Value
    '-> Ajout dans la collection
    ServeurDOG.SystemParams.Add aSys, "SYS|" & UCase$(Key)
Next

'-> Renvoyer une valeur de succ�s
LoadSystemParam = True

'-> Quitter la proc�dure
Exit Function

GestError:
    
End Function

Public Function LoadSchemaB3D(ServeurDOG As clsDOG, FichierSchema As String) As Boolean

'---> Fonction qui ouvre le Dico B3D

Dim aFname As Fname
Dim aField As Field
Dim aIname As Iname
Dim aIfield As iField
Dim aIntField As IntField
Dim aInt As Interface
'-> Pour ajouter dans les Inames
Dim aLinkInt As LinkInterface
Dim i As Integer
Dim TempoStr As String
Dim res As Long
Dim Ligne As String
Dim hdlFile As String

'On Error GoTo GestError

'-> R�cup�ration d'un hanlde pour ouverture du fichier
hdlFile = FreeFile
Open FichierSchema For Input As #hdlFile

'-> Lecture du fichier ascii
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> Rendre la main � la CPU
    DoEvents
    Select Case UCase$(Entry(1, Ligne, "�"))
        
        Case "FNAME" 'Charger un fname
            '-> Cr�ation d'un nouvel Fname
            Set aFname = New Fname
            
            '-> Setting des valeurs
            aFname.Name = UCase$(Trim(Entry(2, Ligne, "�")))
            aFname.Ident = UCase$(Trim(Entry(3, Ligne, "�")))
            aFname.IsReplica = CBool(UCase$(Trim(Entry(4, Ligne, "�"))))
            aFname.IsStat = True 'CBool(UCase$(Trim(Entry(5, Ligne, "�"))))
            aFname.IndexDirect = UCase$(Trim(Entry(6, Ligne, "�")))
            aFname.iOrdre = CLng(UCase$(Trim(Entry(7, Ligne, "�"))))
            For i = 1 To 10
                aFname.CodeLangue = i
                aFname.Designation = Trim(Entry(7 + i, Ligne, "�"))
            Next
            aFname.ChampRef = Entry(18, Ligne, "�")
            If NumEntries(Ligne, "�") = 18 Then
                aFname.IsChampRef = False
            Else
                aFname.IsChampRef = CBool(UCase$(Trim(Entry(19, Ligne, "�"))))
            End If
            '-> Num�ro du fname associ�
            aFname.Num_Fname = Entry(20, Ligne, "�")
            '-> Compteur d'index
            aFname.Iname_Count = Entry(21, Ligne, "�")
            '-> Index de jointure
            aFname.IndexJointure = Entry(22, Ligne, "�")
            '-> Ajouter le fname dans la collection
            ServeurDOG.fNames.Add aFname, aFname.Ident & "|" & aFname.Num_Fname
            
        Case "FIELD" 'Champ associ� � un fname
        
            '-> Cr�ation d'un nouveau champ
            Set aField = New Field
            
            '-> Setting des valeurs
            aField.Num_Field = UCase$(Trim(Entry(2, Ligne, "�")))
            aField.Ident = UCase$(Trim(Entry(3, Ligne, "�")))
            aField.Ordre = CLng(UCase$(Trim(Entry(4, Ligne, "�"))))
            aField.IsReplica = CBool(UCase$(Trim(Entry(5, Ligne, "�"))))
            aField.IsCryptage = CBool(UCase$(Trim(Entry(6, Ligne, "�"))))
            aField.IsHisto = CBool(UCase$(Trim(Entry(7, Ligne, "�"))))
            aField.IsIndexDirect = CBool(UCase$(Trim(Entry(8, Ligne, "�"))))
            aField.Num_Fname = UCase$(Trim(Entry(9, Ligne, "�")))
            
            '-> Ajout de la propri�t� champ en recherche si renseign�e
            If NumEntries(Ligne, "�") > 9 Then _
                aField.IsF10 = CBool(UCase$(Trim(Entry(10, Ligne, "�"))))
                
            '-> Ajout de la propri�t� si champ modifiable
            If NumEntries(Ligne, "�") > 10 Then _
                aField.NonModifiable = CBool(UCase$(Trim(Entry(11, Ligne, "�"))))
            
            '-> Ajouter le champ dans les fname
            aFname.Fields.Add aField, aField.Num_Field
        
        Case "INAME" 'Index associ� � un fname
        
            '-> Cr�ation d'un nouveau Iname
            Set aIname = New Iname
        
            '-> Setting des propri�t�s
            aIname.Code = UCase$(Trim(Entry(2, Ligne, "�")))
            aIname.Ident = UCase$(Trim(Entry(3, Ligne, "�")))
            aIname.Num_Fname = UCase$(Trim(Entry(4, Ligne, "�")))
            aIname.iOrdre = CLng(UCase$(Trim(Entry(5, Ligne, "�"))))
            aIname.IsUnique = CBool(UCase$(Trim(Entry(6, Ligne, "�"))))
            For i = 1 To 10
                aIname.CodeLangue = i
                aIname.Designation = Trim(Entry(6 + i, Ligne, "�"))
            Next
            TempoStr = UCase$(Trim(Entry(17, Ligne, "�")))
            '-> Cr�ation des liens dans les champs
            If Trim(TempoStr) <> "" Then
                For i = 1 To NumEntries(TempoStr, "|")
                    Set aLinkInt = New LinkInterface
                    aLinkInt.Name = UCase$(Trim(Entry(1, TempoStr, "|")))
                    '-> Ajouter le lien dans l'index
                    aIname.LinkInterfaces.Add aLinkInt, aLinkInt.Name
                Next
            End If
            aIname.Num_Iname = Trim(Entry(18, Ligne, "�"))
            aIname.Groupe = Trim(Entry(19, Ligne, "�"))
            '-> Ajouter l'index dans le fname
            aFname.Inames.Add aIname, aIname.Num_Iname
                        
        Case "IFIELD" 'Champ associ� a un index
        
            '-> Cr�ation d'un nouveau champ d'index
            Set aIfield = New iField
            
            '-> Setting des propri�t�s
            aIfield.Num_Field = UCase$(Entry(2, Ligne, "�"))
            aIfield.Ident = UCase$(Entry(3, Ligne, "�"))
            aIfield.Num_Fname = UCase$(Entry(4, Ligne, "�"))
            aIfield.Num_Iname = UCase$(Entry(5, Ligne, "�"))
            aIfield.Ordre = CLng(UCase$(Entry(6, Ligne, "�")))
            aIfield.TypeCondition = SetValueProp(UCase$(Entry(7, Ligne, "�")))
            aIfield.Cumul = CBool(UCase$(Entry(8, Ligne, "�")))
            aIfield.ValMini = UCase$(Entry(9, Ligne, "�"))
            aIfield.ValMaxi = UCase$(Entry(10, Ligne, "�"))
            
            '-> Ajouter le champ dans le iname
            aIname.iFields.Add aIfield, aIfield.Num_Field
            
        Case "INTERFACE" 'Interface associ� � un fname
        
            '-> Cr�er un nouvel Interface
            Set aInt = New Interface
            
            '-> Setting des propri�t�s
            aInt.Name = UCase$(Entry(2, Ligne, "�"))
            aInt.Index = UCase$(Entry(3, Ligne, "�"))
            aInt.Intput_Output = CInt(UCase$(Entry(4, Ligne, "�")))
            aInt.Tolerance = UCase$(Entry(5, Ligne, "�"))
            aInt.UseSeparateur = CBool(Entry(6, Ligne, "�"))
            aInt.Separateur = UCase$(Entry(7, Ligne, "�"))
            aInt.UseIndex = CBool(Entry(8, Ligne, "�"))
            For i = 1 To 10
                aInt.CodeLangue = i
                aInt.Designation = Entry(8 + i, Ligne, "�")
            Next
            
            '-> Ajouter l'interface dans le Fname
            aFname.Interfaces.Add aInt, "INT|" & UCase$(aInt.Name)
        
        Case "INTFIELD" 'Champ associ� � un interface

            '-> Cr�er un nouveau Champ d'interface
            Set aIntField = New IntField
            
            '-> Setting des propri�t�s
            aIntField.Num_Field = UCase$(Entry(2, Ligne, "�"))
            aIntField.Position = UCase$(Entry(3, Ligne, "�"))
            aIntField.Longueur = UCase$(Entry(4, Ligne, "�"))
            aIntField.IsModif = CBool(UCase$(Entry(5, Ligne, "�")))
            aIntField.Defaut = UCase$(Entry(6, Ligne, "�"))
            aIntField.Table_Correspondance = UCase$(Entry(7, Ligne, "�"))

            '-> Ajouter le champ dans l'interface
            aInt.Fields.Add aIntField, aIntField.Num_Field
            
    End Select
Loop 'Boucle d'analyse principale

'-> Fermer le fichier ASCII
Close #hdlFile

'-> Renvoyer une valeur de succ�s
LoadSchemaB3D = True

'-> Quitter la proc�dure
Exit Function

GestError:

End Function


Public Function SetValueProp(ByVal LibProp As String) As Integer

Select Case UCase$(LibProp)
    Case ""
        SetValueProp = 0
    Case "EQ"
        SetValueProp = 1
    Case "INC"
        SetValueProp = 2
    Case "NIN"
        SetValueProp = 3
    Case "DIF"
        SetValueProp = 4
    Case "LE"
        SetValueProp = 5
    Case "GE"
        SetValueProp = 6
    Case "LT"
        SetValueProp = 7
    Case "GT"
        SetValueProp = 8
    Case "BEG"
        SetValueProp = 9
    Case "MAT"
        SetValueProp = 10
End Select


End Function

Public Sub SaveCatalogueFile(ServeurDOG As clsDOG, FichierCatalogue As String)

'---> Enregistrement du catalogue
Dim Rep As Integer
Dim hdlFile As Integer
Dim aEmilieObj As EmilieObj
Dim Ligne As String
Dim ValeurZone As String
Dim i As Integer


'-> R�cup�ration du handle d'un fichier
hdlFile = FreeFile

'-> Supprimer ancienne version du fichier
If Dir$(FichierCatalogue, vbNormal) <> "" Then
    Kill FichierCatalogue
End If

'-> Ouverture du fichier
Open FichierCatalogue For Output As #hdlFile

'-> Enregistrer tous les objets
For Each aEmilieObj In ServeurDOG.Catalogue
    '-> Mettre variable � blanc
    Ligne = ""
    ValeurZone = ""
    Ligne = aEmilieObj.Ident & "�" & aEmilieObj.Num_Field
    For i = 1 To 10
        aEmilieObj.CodeLangue = i
        If ValeurZone = "" Then
            ValeurZone = aEmilieObj.Fill_Label
        Else
            ValeurZone = ValeurZone & "�" & aEmilieObj.Fill_Label
        End If
    Next
    Ligne = Ligne & "�" & ValeurZone & "�" & aEmilieObj.Progiciel & _
            "�" & aEmilieObj.DataType & "�" & aEmilieObj.WidgetFormat & _
            "�" & aEmilieObj.Filled_Type & "�" & aEmilieObj.Filled_Ascii & _
            "�" & aEmilieObj.Table_Correspondance & "�" & CInt(aEmilieObj.MultiLangue) & _
            "�" & aEmilieObj.MasterField & "�" & aEmilieObj.Sep_Vecteur
            
    '-> Imprimer la ligne THIERRY CRYPTER
    Print #hdlFile, Ligne
Next 'Pour tous les objets du catalogue

'-> Fermer le fichier
Close #hdlFile


End Sub
Public Sub SaveDictionnaireB3D(ServeurDOG As clsDOG, FichierSchema As String)

'---> Fonction qui enregistre le Dico B3D

Dim aFname As Fname
Dim aField As Field
Dim aIname As Iname
Dim aIfield As iField
Dim Ligne As String
Dim i As Integer
Dim hdlFile As String
Dim aLink As LinkInterface
Dim ListeInterface As String
Dim aInt As Interface
Dim aIntField As IntField


'-> Supprimer ancienne version du fichier
If Dir$(FichierSchema, vbNormal) <> "" Then
    Kill FichierSchema
End If

'-> Ouverture du fichier dictionnaire
hdlFile = FreeFile
Open FichierSchema For Output As #hdlFile

'******************
'* Enreg du FName *
'******************

For Each aFname In ServeurDOG.fNames
    
    '-> Prefix
    Ligne = "FNAME"
    '-> Name
    Ligne = Ligne & "�" & aFname.Name
    '-> Ident
    Ligne = Ligne & "�" & aFname.Ident
    '-> Replication
    Ligne = Ligne & "�" & CInt(aFname.IsReplica)
    '-> Stat
    Ligne = Ligne & "�" & CInt(aFname.IsStat)
    '-> IndexDirect
    Ligne = Ligne & "�" & aFname.IndexDirect
    '-> Iordre
    Ligne = Ligne & "�" & aFname.iOrdre
    '-> Designation
    For i = 1 To 10
        aFname.CodeLangue = i
        Ligne = Ligne & "�" & aFname.Designation
    Next
    '-> Enregistrer le SocReg
    Ligne = Ligne & "�" & aFname.ChampRef
    Ligne = Ligne & "�" & CInt(aFname.IsChampRef)
    Ligne = Ligne & "�" & aFname.Num_Fname
    Ligne = Ligne & "�" & aFname.Iname_Count
    Ligne = Ligne & "�" & aFname.IndexJointure
    
    '-> Enregistrer le fname dans le fichier
    Print #hdlFile, Ligne
    
    '*****************************
    '* Enreg des champs attach�s *
    '*****************************
    
    For Each aField In aFname.Fields
    
        '-> Prefix
        Ligne = "FIELD"
        '-> Name
        Ligne = Ligne & "�" & aField.Num_Field
        '-> Ident
        Ligne = Ligne & "�" & aField.Ident
        '-> Ordre
        Ligne = Ligne & "�" & aField.Ordre
        '-> Replica
        Ligne = Ligne & "�" & CInt(aField.IsReplica)
        '-> Cryptage
        Ligne = Ligne & "�" & CInt(aField.IsCryptage)
        '-> Histo
        Ligne = Ligne & "�" & CInt(aField.IsHisto)
        '-> Index indirect
        Ligne = Ligne & "�" & CInt(aField.IsIndexDirect)
        '-> Fname associ�
        Ligne = Ligne & "�" & aField.Num_Fname
        '-> si le champ est affichable en recherche
        Ligne = Ligne & "�" & CInt(aField.IsF10)
        '-> Si le champ est non modifiable
        Ligne = Ligne & "�" & CInt(aField.NonModifiable)
        
        '-> Enregistrer la ligne
        Print #hdlFile, Ligne
        
    Next 'Pour tous les champs d'un fname
    
    '*****************************
    '* Enreg des index  attach�s *
    '*****************************
    
    '-> Enregistrer les index associ�s
    For Each aIname In aFname.Inames
        '-> Prefix
        Ligne = "INAME"
        '->Code
        Ligne = Ligne & "�" & aIname.Code
        '-> Ident
        Ligne = Ligne & "�" & aIname.Ident
        '-> Fname
        Ligne = Ligne & "�" & aIname.Num_Fname
        '-> Iordre
        Ligne = Ligne & "�" & aIname.iOrdre
        '-> Index Unique
        Ligne = Ligne & "�" & CInt(aIname.IsUnique)
        '-> Designation par langue
        For i = 1 To 10
            aIname.CodeLangue = i
            Ligne = Ligne & "�" & aIname.Designation
        Next
        '-> Enregistrer mes liens vers les interfaces
        ListeInterface = ""
        For Each aLink In aIname.LinkInterfaces
            If ListeInterface = "" Then
                ListeInterface = aLink.Name
            Else
                ListeInterface = ListeInterface & "|" & aLink.Name
            End If
        Next 'Pour tous les interfaces
        Ligne = Ligne & "�" & ListeInterface & "�" & aIname.Num_Iname & "�" & aIname.Groupe
        
        '-> Enregistrer la ligne
        Print #hdlFile, Ligne
        
        '**************************************
        '* Enreg des champ attach�s � l'index *
        '**************************************
        For Each aIfield In aIname.iFields
            '-> Prefix
            Ligne = "IFIELD"
            '-> Name
            Ligne = Ligne & "�" & aIfield.Num_Field
            '-> Ident
            Ligne = Ligne & "�" & aIfield.Ident
            '-> Fname associ�
            Ligne = Ligne & "�" & aIfield.Num_Fname
            '-> Inale associ�
            Ligne = Ligne & "�" & aIfield.Num_Iname
            '-> Ordre
            Ligne = Ligne & "�" & aIfield.Ordre
            '-> Type condition
            Ligne = Ligne & "�" & GetValueProp(aIfield.TypeCondition)
            '-> Cumul
            Ligne = Ligne & "�" & CInt(aIfield.Cumul)
            '-> ValMini
            Ligne = Ligne & "�" & aIfield.ValMini
            '-> Valmaxi
            Ligne = Ligne & "�" & aIfield.ValMaxi
            '-> Enregistrer la ligne
            Print #hdlFile, Ligne
        Next 'Pour tous les champs d'index

    Next 'Pour tous les Inames
    
    '****************************************
    '* Enreg des interfaces li�s � un fname *
    '****************************************
    
    For Each aInt In aFname.Interfaces
        
        '-> Prefix
        Ligne = "INTERFACE"
        '-> Nom 1
        Ligne = Ligne & "�" & aInt.Name
        '-> Index associ� 2
        Ligne = Ligne & "�" & aInt.Index
        '-> Destination 3
        Ligne = Ligne & "�" & aInt.Intput_Output
        '-> Tol�rance 4
        Ligne = Ligne & "�" & aInt.Tolerance
        '-> Si on utilise un S�parateur 5
        Ligne = Ligne & "�" & CInt(aInt.UseSeparateur)
        '-> S�parateur 6
        Ligne = Ligne & "�" & aInt.Separateur
        '-> Si on utilise un index 7
        Ligne = Ligne & "�" & CInt(aInt.UseIndex)
        '-> Designation
        For i = 1 To 10
            aInt.CodeLangue = i
            Ligne = Ligne & "�" & aInt.Designation
        Next
        '-> Impression de la ligne
        Print #hdlFile, Ligne
    
        '****************************************
        '* Enreg champs li�s � un interface     *
        '****************************************
        
        For Each aIntField In aInt.Fields
    
            '-> Prefix
            Ligne = "INTFIELD"
            '-> Champ associ� 1
            Ligne = Ligne & "�" & aIntField.Num_Field
            '-> Position 2
            Ligne = Ligne & "�" & aIntField.Position
            '-> Longueur 3
            Ligne = Ligne & "�" & aIntField.Longueur
            '-> Modification 4
            Ligne = Ligne & "�" & CInt(aIntField.IsModif)
            '-> Defaut 5
            Ligne = Ligne & "�" & aIntField.Defaut
            '-> Table de correspondance 6
            Ligne = Ligne & "�" & aIntField.Table_Correspondance
            '-> Enregistrer la ligne
            Print #hdlFile, Ligne
    
        Next 'Pour tous les champs
    
    Next 'Pour tous les interfaces
       
Next 'Pour tous les fnames

'-> Fermer le fichier
Close #hdlFile


End Sub

Public Sub SaveCatalogueLink(ServeurDOG As clsDOG, FichierLink As String)

'---> Cette proc�dure enregistre les liens entre les objets du catalogue
Dim hdlFile As Integer
Dim aEmilieObj As EmilieObj
Dim Ligne As String
Dim aLinkFname As LinkFname
Dim aLinkIname As LinkIname
Dim aLinkInterface As LinkInterface

'-> R�cup�ration du handle d'un fichier
hdlFile = FreeFile

'-> Supprimer ancienne version du fichier
If Dir$(FichierLink, vbNormal) <> "" Then Kill FichierLink

'-> Ouverture du fichier pour enregistrement des liens du catalogue
Open FichierLink For Output As #hdlFile

'-> Lecture des objets
For Each aEmilieObj In ServeurDOG.Catalogue
    '-> Pour tous les fnames rattach�s
    For Each aLinkFname In aEmilieObj.fNames
        '-> Indiquer qu'il y a un fname
        Ligne = UCase$(Trim(aEmilieObj.Ident) & "|" & Trim(aEmilieObj.Num_Field)) & "�CREATE�" & UCase$(aLinkFname.Num_Fname)
        Print #hdlFile, Ligne
        '-> Tester si le lien est r�f�renc� en tant que champ d'index direct
        If aLinkFname.IsIndexDirect Then
            '-> Le champ r�f�renc� fait partie de l'index direct
            Ligne = UCase$(Trim(aEmilieObj.Ident) & "|" & Trim(aEmilieObj.Num_Field)) & "�DIRECT�" & UCase$(aLinkFname.Num_Fname)
            Print #hdlFile, Ligne
        Else
            '-> Le champ n'est pas r�f�renc� dans un index Direct  : _
            if fait peut �tre partie d'autres inde
            For Each aLinkIname In aLinkFname.LinkInames
                '-> Imprimer le lien
                Ligne = UCase$(Trim(aEmilieObj.Ident) & "|" & Trim(aEmilieObj.Num_Field)) & "�INAME�" & UCase$(aLinkFname.Num_Fname) & "�" & UCase$(aLinkIname.Num_Iname)
                Print #hdlFile, Ligne
            Next
        End If 'S'il y a un index diect dans la Fname associ�
        '-> Tester si le champ est r�f�renc� comme champ direct d'un fname
        If aLinkFname.IsChampRef Then
            '-> Le champ r�f�renc� fait partie de l'index direct
            Ligne = UCase$(Trim(aEmilieObj.Ident) & "|" & Trim(aEmilieObj.Num_Field)) & "�CHAMPREF�" & UCase$(aLinkFname.Num_Fname)
            Print #hdlFile, Ligne
        End If
                            
        '-> Analyse si le champ est r�f�renc� dans un interface
        For Each aLinkInterface In aLinkFname.LinkInterfaces
            '-> Imprimer le lien
            Ligne = UCase$(Trim(aEmilieObj.Ident) & "|" & Trim(aEmilieObj.Num_Field)) & "�INTERFACE�" & UCase$(aLinkFname.Num_Fname) & "�" & UCase$(aLinkInterface.Name)
            Print #hdlFile, Ligne
        Next '-> Pour tous les liens interfaces
    Next 'Pour tous les fnames rattach�s
Next 'Pour tous les objets du catalogue

'-> Fermer le fichier
Close #hdlFile

End Sub


Private Function GetValueProp(ByVal Index As Integer) As String

Select Case Index
    Case 0
        GetValueProp = ""
    Case 1
        GetValueProp = "EQ"
    Case 2
        GetValueProp = "INC"
    Case 3
        GetValueProp = "NIN"
    Case 4
        GetValueProp = "DIF"
    Case 5
        GetValueProp = "LE"
    Case 6
        GetValueProp = "GE"
    Case 7
        GetValueProp = "LT"
    Case 8
        GetValueProp = "GT"
    Case 9
        GetValueProp = "BEG"
    Case 10
        GetValueProp = "MAT"
End Select


End Function




