VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "INAME"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Ident As String
Public Num_Fname As String
Public Code As String
Public IsUnique As Boolean
Public CodeLangue As Integer
Public Num_Iname As String
Public Groupe  As String


Public iOrdre As Long

Private pDesignation(10) As String

'-> Liste des champs attach�s � un Iname
Public iFields As Collection

'-> Liste des interfaces associ�s � un index
Public LinkInterfaces As Collection

Public Property Get Designation() As String
    Designation = pDesignation(CodeLangue)
End Property

Public Property Let Designation(ByVal vNewValue As String)
    pDesignation(CodeLangue) = vNewValue
End Property

Private Sub Class_Initialize()

Set iFields = New Collection
Set LinkInterfaces = New Collection

End Sub
