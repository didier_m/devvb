Attribute VB_Name = "fctInit"
Option Explicit

'-> Variables de param�trage globale
Public IDSession As String '-> Identificateur de session
Public IdAgent As String '-> Index de l'agent B3D
Public IdIdx As String '-> Index de l'agent IDX
Public Operateur As String '-> Code de l'op�rateur
Public CodeLangue As Integer '-> Code langue de l'op�rateur
Public Uc_ID As String '-> Code de l'uc_id de travail
Public HdlCom As Long '-> Handle de communication avec la console
Public DebugMode As Boolean '-> Si on debug ou non
Public TraceMode As Boolean '-> Si on trace ou non les instructions
Public CryptMode As Boolean '-> Si on crypt les �changes
Public VecteurLock As String '-> Erreur pour conflit de temporisation
Public ReplicaUpdate As Boolean '-> Indique si on peut modifier des enregistrements issus de la r�plication
Public AccesDir As Integer '-> Indique le d�lai de temporisation entre 2 scan de r�pertoire

'-> Variables affect�es � l'ident
Public Ident As String '-> Code de l'ident
Public FichierSchema As String '-> Fichier sch�ma
Public FichierCatalogue As String  '-> Fichier Catalogue
Public FichierLink As String '-> Fichier Lien

'-> Variables affect�es � la base
Public NomBase As String '-> Nom de la base
Public ServeurBase As String '-> Serveur de base
Public UserBase As String '-> Pour connecxion ODBC, user
Public PwdBase As String '-> Pour connexion ODBC, mot de passe
Public FileBase As String '-> Nom du fichier base � ouvrir
Public TypeBase As String '->Type de base
Public TempoLock As Long '-> Temporisation sur conflit de lock

'-> Variables de fichier
Public DmdFile As String '-> Fichier Demande
Public RspFile As String '-> Fichier R�ponse
Public DebugFile As String '-> Fichier Debug
Public TraceFile As String '-> Fichier Trace
Public B3DIniFile As String '-> Emplacement du fichier B3D.ini
Public IdentIniFile As String '-> Emplacement du fichier IDENT.INI

'-> Variable de Path
Public IntPath As String '-> Int�gration des interfaces
Public IntSovPath As String  '-> Sauvegarde des fichiers interfaces apr�s int�gration
Public ReplicaInputPath As String '-> Int�gration de r�plication
Public ReplicaOutPutPath As String '-> Export de r�plication
Public ServeurDOGPath As String '-> Emplacement des fichiers Serveurs
Public RspPath As String '-> Emplacement du r�pertoire des r�ponses

'-> Variables internes de travail
Dim hdlDebugFile As Integer '-> Handle du fichier de debug
Dim hdlTraceFile As Integer '-> handle du fichier trace
Dim hdlRetourFile As Integer '-> handle du fichier r�ponse
Public TempFileName As String '-> fichier temporaire de retour

'-> Variables d'acc�s aux bases
Public ADOCn As ADODB.Connection '-> Connexion base ODBC
Public AccessBase As Database '-> Connexion base ACCESS
Public IsAccessBase As Boolean '-> Base est de type ACCESS

'-> Pour cr�ation des requ�tes SQL
Public SQLValue As String

'-> Propri�t�s Modifiables par l'agent B3D
Public Top_Crypt As Boolean '-> Si on doit crypter les �changes ou non
Public Programme As String '-> Programme en cours pour l'agent B3D
Public NumLockSpe As String '-> Pour num�ro de lock particulier
Public UpdateMode As Boolean '-> Indique si l'agent B3D peut faire des mise � jour de base

Public Sub Init(FileToLoad As String)

'---> Cette proc�dure effectue le chargement du fichier Param�trage

On Error Resume Next

'-> Param�tres g�n�raux
IDSession = DeCrypt(GetIniFileValue("PARAM", "IDSESSION", FileToLoad))
IdAgent = DeCrypt(GetIniFileValue("PARAM", "IDAGENT", FileToLoad))
IdIdx = DeCrypt(GetIniFileValue("PARAM", "IDIDX", FileToLoad))
Operateur = DeCrypt(GetIniFileValue("PARAM", "OPERAT", FileToLoad))
CodeLangue = CInt(DeCrypt(GetIniFileValue("PARAM", "CODELANGUE", FileToLoad)))
Uc_ID = DeCrypt(GetIniFileValue("PARAM", "UC_ID", FileToLoad))
HdlCom = CLng(DeCrypt(GetIniFileValue("PARAM", "HDLCOM", FileToLoad)))
DebugMode = CBool(DeCrypt(GetIniFileValue("PARAM", "DEBUG", FileToLoad)))
TraceMode = CBool(DeCrypt(GetIniFileValue("PARAM", "TRACE", FileToLoad)))
CryptMode = CBool(DeCrypt(GetIniFileValue("PARAM", "CRYPT", FileToLoad)))
ReplicaUpdate = CBool(DeCrypt(GetIniFileValue("PARAM", "REPLICAUPDATE", FileToLoad)))
VecteurLock = DeCrypt(GetIniFileValue("PARAM", "LOCK", FileToLoad))
AccesDir = CInt(DeCrypt(GetIniFileValue("PARAM", "ACCESDIR", FileToLoad)))

'-> Variables Affect�es � l'ident
Ident = DeCrypt(GetIniFileValue("IDENT", "CODE", FileToLoad))
FichierSchema = DeCrypt(GetIniFileValue("IDENT", "SCH", FileToLoad))
FichierCatalogue = DeCrypt(GetIniFileValue("IDENT", "CAT", FileToLoad))
FichierLink = DeCrypt(GetIniFileValue("IDENT", "LNK", FileToLoad))

'-> Variables affect�es � la base
NomBase = DeCrypt(GetIniFileValue("BASE", "NOM", FileToLoad))
ServeurBase = DeCrypt(GetIniFileValue("BASE", "SERVEUR", FileToLoad))
UserBase = DeCrypt(GetIniFileValue("BASE", "USER", FileToLoad))
PwdBase = DeCrypt(GetIniFileValue("BASE", "PWD", FileToLoad))
FileBase = DeCrypt(GetIniFileValue("BASE", "FICHIER", FileToLoad))
TypeBase = DeCrypt(GetIniFileValue("BASE", "TYPE", FileToLoad))
TempoLock = CLng(DeCrypt(GetIniFileValue("BASE", "TEMPO", FileToLoad)))

'-> Variables de fichier
DmdFile = DeCrypt(GetIniFileValue("PATH", "DMD", FileToLoad))
RspFile = DeCrypt(GetIniFileValue("PATH", "RSP", FileToLoad))
DebugFile = DeCrypt(GetIniFileValue("PATH", "DEBUG", FileToLoad))
TraceFile = DeCrypt(GetIniFileValue("PATH", "TRACE", FileToLoad))
B3DIniFile = DeCrypt(GetIniFileValue("PATH", "B3DINI", FileToLoad))
IdentIniFile = DeCrypt(GetIniFileValue("PATH", "IDENTINI", FileToLoad))
MessprogIniFile = DeCrypt(GetIniFileValue("PATH", "MESSPROGINIFILE", FileToLoad))

'-> Variable de Path
IntPath = DeCrypt(GetIniFileValue("PATH", "INT", FileToLoad))
IntSovPath = DeCrypt(GetIniFileValue("PATH", "INTSOV", FileToLoad))
ReplicaInputPath = DeCrypt(GetIniFileValue("PATH", "REPLICAIN", FileToLoad))
ReplicaOutPutPath = DeCrypt(GetIniFileValue("PATH", "REPLICAOUT", FileToLoad))
ServeurDOGPath = DeCrypt(GetIniFileValue("PATH", "SERVEURDOGPATH", FileToLoad))
RspPath = DeCrypt(GetIniFileValue("PATH", "RSPPATH", FileToLoad))

'-> Ouverture du fichier Debug
If DebugMode Then Call OpenDebugFile

'-> Ouverture du fichier Trace
If TraceMode Then Call OpenTrace

'-> Si on crypt ou non
If CryptMode Then Top_Crypt = True
    
'-> Indiquer si on est en mode Update ou non ( valable que pour l'agent B3D )
If IdIdx <> "" Then
    UpdateMode = True
    NumLockSpe = "AGENTIDX_" & IdIdx
End If


'-> Supprimer le fichier
'If Dir$(FileToLoad) <> "" Then Kill FileToLoad

End Sub

Public Function GetConflitVecteurLock(Erreur As Long) As Boolean

'---> Cette proc�dure recherche si l'erreur analys�e est une erreur de conflit de base
Dim i As Integer

For i = 1 To NumEntries(VecteurLock, ",")
    If CLng(Trim(Entry(i, VecteurLock, ","))) = Erreur Then
        GetConflitVecteurLock = True
        Exit Function
    End If
Next

End Function

Public Sub OpenReponse()

'---> Cette fonction ouvre le fichier de r�ponse de l'agent B3D ( fichier RSP)

'-> Get d'un handle de fichier
hdlRetourFile = FreeFile

'-> Get d'un fichier temporaire
TempFileName = RspFile & ".tmp"

'-> Ouverure du fichier
Open TempFileName For Output As #hdlRetourFile

End Sub

Public Sub PrintReponse(ByVal ReponseToPrint As String)

'-> Ecrire une ligne dans le fichier Retour (RSP)
If Top_Crypt Then
    Print #hdlRetourFile, Crypt(ReponseToPrint)
Else
    Print #hdlRetourFile, ReponseToPrint
End If
'-> Gestion de la trace des instructions
If TraceMode Then PrintTrace (ReponseToPrint)

End Sub

Public Sub CloseReponse()

'---> Cette fonction ferme le fichier de r�ponse de l'agent b3D (RSP)
Dim strTempo As String
Dim strTempo2 As String

'-> Fermer le fichier de retour
Close #hdlRetourFile

'-> V�rifier qu'un fichier ne traine pas
If Dir$(RspFile) <> "" Then Kill RspFile

'-> Renommer le fichier temporaire en fichier r�ponse
Name TempFileName As RspFile

End Sub

Public Sub ResetReponse()

'---> Cette proc�dure ferme le fichier r�ponse en cours, le d�truit en en ouvre un autre

'-> Fermer le fichier
Close #hdlRetourFile

'-> Le supprimer
If Dir$(TempFileName) <> "" Then Kill TempFileName

'-> Ouvrir un nouveau fichier r�ponse
OpenReponse

End Sub

Public Function OpenDebugFile()

'---> Ouverture du fichier de debug

'-> Get d'un handle et ouverture du fichier associ�
hdlDebugFile = FreeFile
Open DebugFile For Output As #hdlDebugFile

End Function
Public Function PrintDebug(DebugToPrint As String)

'---> Enregistrer dans le journal du debug

'-> Ne pas faire de print si pas necessaire
If DebugMode Then Print #hdlDebugFile, DebugToPrint

End Function

Public Function CloseDebug()

'---> Fermeture du fichier debug

'-> Fermer le fichier
If hdlDebugFile <> 0 Then Close #hdlDebugFile

End Function

Public Function OpenTrace()

'---> Ouverture du fichier trace

'-> Get d'un handle et ouverture du fichier
hdlTraceFile = FreeFile
Open TraceFile For Output As #hdlTraceFile

End Function

Public Function CloseTrace()

'---> Fermeture du fichier trace

'-> Fermer le fichier
If hdlTraceFile <> 0 Then Close #hdlTraceFile

End Function

Public Sub PrintTrace(ByVal TraceToPrint As String)

'---> Impression d'une trace dans le fichier
If TraceMode Then Print #hdlTraceFile, TraceToPrint

End Sub

Public Function OpenConnexionBase()

'---> Cette proc�dure connecte la base en fonction des param�tres

'-> Proc�dure de connexion diff�rente selon le type de base
Select Case UCase$(Trim(TypeBase))
    Case "ACCESS"
        Call ConnectAccesBase
    Case "SQL SERVER"
        Call ConnectODBCBase
End Select

End Function

Public Sub ConnectODBCBase()

'---> Cette proc�dure connecte une base de type ODBC

Dim strConnect As String

On Error GoTo GestError

'-> Cr�ation de la chaine de connexion
strConnect = "driver={" & TypeBase & "};Server=" & ServeurBase & ";uid=" & UserBase & ";pwd=" & PwdBase & ";Database=" & NomBase

'-> Envoie du debug
Call PrintDebug("Connexion � la base ODBC � : " & Now & " par la chaine de connexion : " & strConnect)

'-> Connexion � la base
Set ADOCn = New ADODB.Connection
ADOCn.Open strConnect

'-> Debug
Call PrintDebug("Connexion � la base ODBC ---> OK")

'-> Indiquer que l'on n'est pas sur une base acces
IsAccessBase = False

'-> Quitter la fonction
Exit Sub

GestError:
    '-> Envoyer un mode debug
    Call PrintDebug("Erreur dans la proc�dure de connexion � la base ODBC : " & Err.Number & " - " & Err.Description)
    End


End Sub


Public Sub ConnectAccesBase()

'---> Cette proc�dure connecte une base de type ACCESS

On Error GoTo GestError

'-> Envoie du debug
Call PrintDebug("Connexion � la base ACCESS � " & Now)

'-> V�rification du fichier
Call PrintDebug("Recherche du fichier : " & FileBase)
If Dir$(FileBase) = "" Then
    '-> Envoyer une debug
    Call PrintDebug("Impossible de trouver le fichier : Erreur fatale")
    '-> Fermer tous les fichiers
    Reset
    '-> Fin du prog
    End
End If

'-> Tentative de connexion � la base
Set AccessBase = OpenDatabase(FileBase)

'-> Envoie du mode debug
Call PrintDebug("Connexion � la base r�ussie ---> OK")

'-> Indiquer que l'on est sur une base de type acces
IsAccessBase = True

'-> Quitter la fonction
Exit Sub

GestError:
    '-> Envoyer un mode debug
    Call PrintDebug("Erreur dans la proc�dure de connexion � la base access : " & Err.Number & " - " & Err.Description)
    End

End Sub

Public Sub CreateEnreg(ByVal strQuery As String)

On Error GoTo GestError

'---> Cette proc�dure ex�cute une requete SQL sur une lbase

'-> Mode debug
Call PrintDebug("Proc�dure Cr�atEnreg : Ex�cution de la requete : " & Chr(13) & Chr(10) & strQuery)

'-> Selon le type de base
If Not IsAccessBase Then
    ADOCn.Execute strQuery
Else
    AccessBase.Execute strQuery, dbFailOnError
End If

'-> Quitter la fonction
Exit Sub

GestError:

    '-> En cas de conflit
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Erreur impr�vue
    Call PrintDebug("Proc�dure Cr�ate Enreg Erreur dans la proc�dure : " & strQuery & Chr(13) & Chr(10) & Err.Number & "->" & Err.Description)

End Sub


Public Function SetTable(ByVal strQuery As String) As Object

On Error GoTo Errorhandler

Dim recADO As Object
Dim recDAO As Object

'-> Cas d'attaque de base via ADO ( Chaine de connexion ODBC )
If Not IsAccessBase Then

    '-> Initialiser le recordset
    Set recADO = New ADODB.Recordset
    Set recADO.ActiveConnection = ADOCn
    
    '-> Propri�t�s de LOCK
    recADO.CursorType = adOpenDynamic
    recADO.LockType = adLockPessimistic
    
    '-> Ouvrir le recordset
    recADO.Open strQuery
    
    '-> Renvoyer le recordset de retour
    Set SetTable = recADO

Else 'Cas d'une base access
    
    '-> Pointer vers le recordset
    Set recDAO = AccessBase.OpenRecordset(strQuery)
    
    '-> Retourner le recordset
    Set SetTable = recDAO

End If

Exit Function

Errorhandler:

    '-> Gestion des conflits de lock
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Erreur  impr�vue
    Call PrintDebug("Erreur dans la proc�dure SET TABLE pour la requete : " & Chr(13) & Chr(10) & strQuery & Chr(13) & Chr(10) & Err.Number & " -> " & Err.Description)
    
    '-> Renvoyer une table vide
    Set SetTable = Nothing

End Function

Public Function GetDate() As String

'---> Cette proc�dure retourne la date courrante sous la forme YYYYMMDD

Dim Jour As String
Dim Mois As String
Dim Annee As String

'-> Eclater
Jour = Format(Day(Now), "00")
Mois = Format(Month(Now), "00")
Annee = Format(Year(Now), "0000")

'-> Retourner la valeur
GetDate = Annee & Mois & Jour

End Function

Public Function GetTime() As String

'---> Cette proc�dure retourne l'heure courrante sous la forme HHMMSS

Dim Heure As String
Dim Minutes As String
Dim Seconde As String

'-> Eclater
Heure = Format(Hour(Now), "00")
Minutes = Format(Minute(Time), "00")
Seconde = Format(Second(Now), "00")

'-> Retourner la valeur
GetTime = Heure & Minutes & Seconde

End Function
Public Function SetDate(ByVal strDate As String) As String

'---> Reformate une date dans le format europeen

Dim Annee As String
Dim Mois As String
Dim Jour As String

'-> Get de l'annee
Annee = Mid$(strDate, 1, 4)
Mois = Mid$(strDate, 5, 2)
Jour = Mid$(strDate, 7, 2)

'-> retourner la valeur
SetDate = Jour & "/" & Mois & "/" & Annee

End Function

Public Function SetHeure(ByVal strHeure As String) As String

'---> Reformate une date dans le format europeen

Dim Heure As String
Dim Minute As String
Dim Seconde As String

'-> Get de l'heure
Heure = Mid$(strHeure, 1, 2)
Minute = Mid$(strHeure, 3, 2)
Seconde = Mid$(strHeure, 5, 2)

'-> Retourner la valeur
SetHeure = Heure & ":" & Minute & ":" & Seconde

End Function


Public Function Get_Total_Time() As String

'---> Cette fonction retourne un format de date complet ( Date et heure) sous la forme _
YYYYMMDDHHMMSS

'-> Retourner la valeur
Get_Total_Time = GetDate & GetTime

End Function

Public Function FormatDate(strDate As String, Optional Anglo As Boolean) As String

'---> Formater une date Attention date et heure dans m�me zone si taille = 14

Dim aDate As String
Dim aTime As String
Dim tmpDate As String
Dim tmpTime As String


If Left$(strDate, 3) = "IS_" Then
    '-> Retourner la valeur d'origine
    FormatDate = strDate
    '-> quitter la fonction
    Exit Function
End If
    

If Len(strDate) = 14 Then
    '-> Get de la date et de l'heure
    aDate = Mid$(strDate, 1, 8)
    aTime = Mid$(strDate, 9, 6)
ElseIf Len(strDate) = 6 Then
    '-> Get de l'heure
    aDate = ""
    aTime = strDate
Else
    '-> Get de la date
    aTime = ""
    aDate = strDate
End If

If Anglo Then '-> MM / JJ / AAAA
    '-> Si on doit formatter la date
    If aDate <> "" Then tmpDate = Mid$(aDate, 5, 2) & "/" & Mid$(aDate, 7, 2) & "/" & Mid$(aDate, 1, 4)
Else '-> JJ / MM / AAAA
    '-> Si on doit formatter la date
    If aDate <> "" Then tmpDate = Mid$(aDate, 7, 2) & "/" & Mid$(aDate, 5, 2) & "/" & Mid$(aDate, 1, 4)
End If

'-> Formatter l'heure
If aTime <> "" Then
    tmpTime = Mid$(aTime, 1, 2) & ":" & Mid$(aTime, 3, 2) & ":" & Mid$(aTime, 5, 2)
End If

'->  Retourner
FormatDate = Trim(tmpDate & " " & tmpTime)


End Function



Public Function Get_NUM_ENREG(Id_Fname As String) As String

'---> Cette proc�dure retourne pour une table et un identifiant donn� un num�ro d'enreg B3D

Dim aRec As Object
Dim NEnreg As String
Dim ErrorCode As String

On Error GoTo GestError

'-> Cr�er la requette
SQLValue = "SELECT * FROM [DEALB3D_ENREG] WHERE [ID_FNAME] = '" & Id_Fname & "�" & IdAgent & "' AND [DAT_HISTO] = '99999999'"

'-> Mode DEBUG
Call PrintDebug("Proc�dure Get_Num_Enreg : Ex�cution de la requete : " & Chr(13) & Chr(10) & SQLValue)

'-> Ex�cuter la requete
Set aRec = SetTable(SQLValue)

'-> On teste si la requete a aboutie
If Not aRec Is Nothing Then
    '-> Tester si Il y a un enregistrement
    If aRec.BOF = True Then
        '-> Cr�ation d'une requete
        SQLValue = "INSERT INTO [DEALB3D_ENREG] ( ID_FNAME , NUM_ENREG , DAT_HISTO ) VALUES ( '" & _
                    UCase$(Id_Fname) & "�" & IdAgent & "','" & CStr(Format(1, "0000000000000")) & "','99999999')"
        '-> Mode debug
        Call PrintDebug("Proc�dure Get_Num_Enreg : Premier acc�s. Cr�ation de l'nregistrement de base :" & Chr(13) & Chr(10) & SQLValue)
        
        '-> Ex�cution de la requete
        CreateEnreg SQLValue
                    
        '-> Retourner la valeur
        NEnreg = CStr(Format(1, "0000000000000"))
    Else
        '-> Recup la valeur
        NEnreg = Format(CStr(CInt(aRec.Fields("NUM_ENREG")) + 1), "0000000000000")
        
        '-> Cr�ation d'une requete
        SQLValue = "UPDATE [DEALB3D_ENREG] SET [NUM_ENREG] =  '" & NEnreg & "' " & _
        "WHERE [ID_FNAME] = '" & Id_Fname & "�" & IdAgent & "' AND [DAT_HISTO] = '99999999'"
        
        '-> Mode debug
        Call PrintDebug("Proc�dure Get_Num_Enreg : Enreg existant -> Incr�mentation :" & Chr(13) & Chr(10) & SQLValue)
        
        '-> Ex�cution de la requete
        CreateEnreg SQLValue
    End If
    
    '-> Retourner de la valeur
    Get_NUM_ENREG = Uc_ID & IdAgent & NEnreg

    '-> Fermer le query
    aRec.Close
End If

Exit Function

GestError:

    '-> Gestion des conflits de lock
    If GetConflitVecteurLock(Err.Number) Then
        Sleep (TempoLock)
        Resume
    End If
    
    '-> Erreur impr�vue
    Call PrintDebug("Erreur dans la proc�dure Get_Num_ENREG " & Chr(13) & Chr(10) & Err.Number & Err.Description & Chr(13) & Chr(10) & SQLValue)
    
End Function


Public Sub CloseConnectB3D()

'---> Cette proc�dure ferme une connexion vers une base de donn�es

On Error GoTo GestError

'-> Debug
Call PrintDebug("Proc�dure CloseConnectB3D : Fermeture de la connexion base � : " & Now)

'-> Selon que l'on soit en base access ou non
If IsAccessBase Then
    '-> Fermer la connexion DAO
    If Not AccessBase Is Nothing Then AccessBase.Close
Else
    '-> Fermer la connexion ADO
    If Not ADOCn Is Nothing Then ADOCn.Close
End If

'-> Quitter la proc�dure
Exit Sub

GestError:

    '-> Mode debug
    Call PrintDebug("Erreur lors de la fermeture de la connexion base.")

End Sub

Public Function IsInameChampRef(Fname As String) As Boolean

'---> Analyse la table de correspondance associ�e � un champ pour chercher le champ de soci�t� de r�f�rence

Dim aIfield As Object
Dim aIname As Object
Dim aFname As Object
Dim SocRef As String

On Error GoTo GestError

'-> V�rifier qu'il y ait un param�trage de soci�t� de r�f�rence
SocRef = ServeurDOG.GetSystemTableValue("SOCREF")
If SocRef = "" Then Exit Function

'-> Pointer sur la table
Set aFname = ServeurDOG.fNames(Ident & "|" & Fname)

'-> Se barrer si pas d'index de jointure
If aFname.IndexJointure = "" Then Exit Function

'-> Pointer sur le premier Index
Set aIname = aFname.Inames(aFname.IndexJointure)

'-> Analyser l'index pour voir si on trouve le champ d'acc�s sur groupe de r�f�rence
For Each aIfield In aIname.iFields
    If aIfield.Num_Field = SocRef Then
        IsInameChampRef = True
        Exit Function
    End If
Next 'Pour tous les champs de l'index
    
'-> Pas d'acc�s sur soci�t� de r�f�rence dans cet index
IsInameChampRef = False
    
Exit Function

GestError:

    IsInameChampRef = False

End Function

