VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "B3DNavigation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> D�fintion d'un objet de navigation
Public Ident As String '-> code client
Public Fname As String '-> Table
Public Sql As String '-> Requete SQL
Public ConditionsHorsIndex As String '-> Conditions de tri hors index
Public ListeFieldToGet As String '-> Liste des champs � ramener
Public ListeFieldToDisplay As String '-> Liste des champs � afficher
Public EnregSize As Long '-> Taille du buffer de donn�es
Public PageSize As Long '-> Taille d'une page de donn�e
Public PageCours As Long '-> Page en cours
Public EnregCount As Long '-> Nombre d'enregistrement retenu par la clause select
Public PageCount As Long '-> Nombre de page cr�� sur le serveur par l'agent B3D
Public Pages As Collection '-> Collection des pages dans le buffer
Public BufferName As String '-> Nom du buffer attach� � l'agent B3D
Public aDescro As B3DFnameDescriptif '-> descripif de la nature des enregistrements

Private Sub Class_Initialize()

'-> Initialiser la collection des pages
Set Pages = New Collection

'-> Initialiser la collection des champs
Set Fields = New Collection

End Sub


Private Function IsPage(PageNumber As Integer) As Boolean

'---> Cette proc�dure v�rifie si une page existe ou non

Dim aPage As B3DBuffer

On Error GoTo GestError

'-> Essayer  de pointer sur cette page
Set aPage = Pages("PAGE|" & PageNumber)

'-> Renvoyer une valeur qui indique que cette page existe
IsPage = True

'-> Quitter la fonction
Exit Function

GestError:
    '-> Renvoyer une valeur d'erreur
    IsPage = False

End Function
Public Function NavigaDirect(Connexion As B3DConnexion, ListView As ListView, PageNum As Integer) As Boolean

'---> Faire un B3D_NAVIGA_CONTINU

Dim aPage As B3DBuffer

'-> V�rifier si cette page existe
If Not IsPage(PageNum) Then
    '-> Demander la cr�ation de la page
    If Not NavigaGetPage(Connexion, Me, PageNum, aDescro) Then Exit Function
Else
    '->Positonner le ponteur de page dans l'objet de navigation
    Me.PageCours = PageNum
End If

'-> Pointer sur la page
Set aPage = Pages("PAGE|" & PageNum)

'-> Afficher la page
DisplayPageinListview Me, ListView, PageNum, aDescro

'-> Renvoyer une valeur de succ�s
NavigaDirect = True


End Function


Public Function NavigaFirst(Connexion As B3DConnexion, ListView As ListView) As Boolean

'---> Faire un B3D_NAVIGA_FIRST

Dim PageNum As Integer
Dim aPage As B3DBuffer

'-> Identification de la page en cours
If PageCours = 1 Then
    '-> Renvoyer une valeur de succ�s
    NavigaFirst = True
    Exit Function
End If

'-> V�rifier si cette page existe
If Not IsPage(1) Then
    '-> Demander la cr�ation de la page
    If Not NavigaGetPage(Connexion, Me, 1, aDescro) Then Exit Function
Else
    '->Positonner le ponteur de page dans l'objet de navigation
    Me.PageCours = 1
End If

'-> Pointer sur la page
Set aPage = Pages("PAGE|1")

'-> Afficher la page
DisplayPageinListview Me, ListView, 1, aDescro

'-> Renvoyer une valeur de succ�s
NavigaFirst = True

End Function

Public Function NavigaNext(Connexion As B3DConnexion, ListView As ListView) As Boolean

'---> Faire un B3D_NAVIGA_NEXT

Dim PageNum As Integer
Dim aPage As B3DBuffer

'-> Identification de la page en cours
If PageCours = Me.PageCount Then
    '-> Si on a atteint la derni�re page faire un naviga Continu


    '-> Renvoyer une valeur de succ�s
    NavigaNext = True
    '-> Quitter la fonction
    Exit Function
End If


'-> V�rifier si cette page existe
If Not IsPage(Me.PageCours + 1) Then
    '-> Demander la cr�ation de la page
    If Not NavigaGetPage(Connexion, Me, Me.PageCours + 1, aDescro) Then Exit Function
Else
    '-> Positionner la page en cours
    Me.PageCours = Me.PageCours + 1
End If

'-> Pointer sur la page
Set aPage = Pages("PAGE|" & Me.PageCours)

'-> Afficher la page
DisplayPageinListview Me, ListView, Me.PageCours, aDescro

'-> Renvoyer une valeur de succ�s
NavigaNext = True

End Function

Public Function NavigaPrev(Connexion As B3DConnexion, ListView As ListView) As Boolean

'---> Faire un B3D_NAVIGA_PREV

Dim PageNum As Integer
Dim aPage As B3DBuffer

'-> Identification de la page en cours
If PageCours = 1 Then
    '-> Renvoyer une valeur de succ�s
    NavigaPrev = True
    '-> Quitter la focntion
    Exit Function
End If

'-> V�rifier si cette page existe
If Not IsPage(Me.PageCours - 1) Then
    '-> Demander la cr�ation de la page
    If Not NavigaGetPage(Connexion, Me, Me.PageCours - 1, aDescro) Then Exit Function
Else
    '-> Positionner la page en cours
    Me.PageCours = Me.PageCours - 1
End If

'-> Pointer sur la page
Set aPage = Pages("PAGE|" & Me.PageCours)

'-> Afficher la page
DisplayPageinListview Me, ListView, Me.PageCours, aDescro

'-> Renvoyer une valeur de succ�s
NavigaPrev = True

End Function

Public Function NavigaLast(Connexion As B3DConnexion, ListView As ListView) As Boolean

'---> Faire un B3D_NAVIGA_LAST

Dim PageNum As Integer
Dim aPage As B3DBuffer

'-> Identification de la page en cours
If PageCours = Me.PageCount Then
    '-> Renvoyer une valeur de succ�s
    NavigaLast = True
    '-> Quitter la focntion
    Exit Function
End If

'-> V�rifier si cette page existe
If Not IsPage(Me.PageCount) Then
    '-> Demander la cr�ation de la page
    If Not NavigaGetPage(Connexion, Me, Me.PageCount, aDescro) Then Exit Function
Else
    '-> Positionner la page en cours
    Me.PageCours = Me.PageCount
End If

'-> Pointer sur la page
Set aPage = Pages("PAGE|" & Me.PageCours)

'-> Afficher la page
DisplayPageinListview Me, ListView, Me.PageCours, aDescro

'-> Renvoyer une valeur de succ�s
NavigaLast = True


End Function

Public Function NavigaContinu(Connexion As B3DConnexion) As Boolean

'---> Faire un B3D_NAVIGA_CONTINU

Dim Ligne As String
Dim aBuffer As B3DBuffer

'-> Initialiser une demande
If Not Connexion.InitDmd Then Exit Function

'-> Faire un B3D_CONTINU
If Not Connexion.PutInstruction("B3D_NAVIGA~" & Me.BufferName & "�CONTINU") Then Exit Function

'-> Cr�er un buffer de navigation
Set aBuffer = New B3DBuffer

'-> Envoyer la demande
If Not Connexion.SendDemande(aBuffer) Then Exit Function

'-> Lecture de la ligne de retour
Ligne = aBuffer.Lignes(1)

'-> Tester
If Entry(2, Ligne, "~") <> "OK" Then
    '-> Positionner la variable d'erreur
    Connexion.ErrorNumber = Entry(3, Ligne, "~")
    Connexion.ErrorLibel = Entry(4, Ligne, "~")
    Connexion.ErrorParam = Entry(5, Ligne, "~")
    '-> Quitter la fonction
    Exit Function
End If

'-> Mettre � jour le nombre de page et d'enreg
Me.PageCount = CLng(Entry(1, Entry(3, Ligne, "~"), "�"))
Me.EnregCount = CLng(Entry(2, Entry(3, Ligne, "~"), "�"))
    
'-> Renvoyer une valeur de succ�s
NavigaContinu = True

End Function
