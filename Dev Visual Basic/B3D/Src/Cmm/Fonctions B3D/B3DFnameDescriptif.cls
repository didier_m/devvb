VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "B3DFnameDescriptif"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---> Get du descriptif d'une table

Public Fname As String
Public Designation As String
Public IsStat As Boolean
Public IsReplica As Boolean
Public IndexDirect As String
Public IsChampRef As String
Public IndexJointure As String
Public Descros As Collection
Public Fields As Collection
Public CodeLangue As Integer

Private Sub Class_Initialize()

'---> Initialisation des collections
Set Fields = New Collection
Set Descros = New Collection

End Sub

Public Function GetEnreg() As B3DEnreg

'---> Cette fonction retourne un enregistrement vierge bas� sur le descritpif du champ

Dim aEnreg As B3DEnreg
Dim aField As B3DField
Dim aNewField As B3DField
Dim aFieldJoin As B3DField
Dim aField2 As B3DField
Dim aDescro As B3DFnameDescriptif
Dim aObj As B3DObject

'-> Cr�er un nouvel Enreg
Set aEnreg = New B3DEnreg

'-> Pour tous les enregistrements du descriptif
For Each aField In Me.Fields
    '-> Cr�er un nouveu champ
    Set aNewField = New B3DField
    '-> Positionner son code
    aNewField.Field = aField.Field
    '-> R�cup�rer son libelle
    aNewField.Libel = aField.Libel
    '-> Dupliquer ses propri�t�s
    aNewField.IsCryptage = aField.IsCryptage
    aNewField.IsF10 = aField.IsF10
    aNewField.IsHisto = aField.IsHisto
    aNewField.IsModifiable = aField.IsModifiable
    aNewField.IsReplica = aField.IsReplica
    '-> Ajouter dans l'enregistrement en cours
    aEnreg.Fields.Add aNewField, aNewField.Field
    '-> Gestion des jointures : pointer sur l'objet associ� au champ
    Set aObj = fctB3D.Catalogues(aField.Field)
    '-> Tester s'il y a une table de correspondance associ�e
    If Trim(aObj.Table_Correspondance) <> "" And aField.Field <> Me.IndexDirect Then
        '-> Pointer sur le descro assopoci�
        Set aDescro = Me.Descros(aObj.Table_Correspondance)
        '-> Ajouter comme jointure
        For Each aField2 In aDescro.Fields
            '-> Ne pas cr�er pour le champ de jointure
            If aField2.Field <> aDescro.IndexDirect Then
                '-> Cr�er un nouveau champ
                Set aFieldJoin = New B3DField
                '-> Positionner son libelle et son code
                aFieldJoin.Field = aField2.Field
                '-> R�cup�rer son libelle
                aFieldJoin.Libel = aField2.Libel
                '-> Dupliquer ses propri�t�s
                aFieldJoin.IsCryptage = aField2.IsCryptage
                aFieldJoin.IsF10 = aField2.IsF10
                aFieldJoin.IsHisto = aField2.IsHisto
                aFieldJoin.IsModifiable = aField2.IsModifiable
                aFieldJoin.IsReplica = aField2.IsReplica
                '-> Code de la table de jointure
                aFieldJoin.FnameJoin = aDescro.Fname
                '-> Ajouter dans l'enregistrement en cours
                aNewField.FieldJoins.Add aFieldJoin, aFieldJoin.Field
            End If 'Si on n'est pas sur le champ de jointure
        Next 'Pour tous les enregistrements du descritpif
    End If 's'il ya une table de correspondance
Next 'Pour tous les enregistrements du descritpif


'-> Retourner l'enregistrement
Set GetEnreg = aEnreg

End Function
