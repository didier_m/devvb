VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FIELD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Ident As String
Public Num_Field As String
Public Ordre As Long
Public IsReplica As Boolean
Public IsCryptage As Boolean
Public IsHisto As Boolean
Public IsF10 As Boolean '-> Indique si on affiche le champ en Recherche
Public NonModifiable As Boolean '-> Indique que le champ est non modifiable

'-> Inidique la table rattach�e
Public Num_Fname As String

'-> Indique si le champ fait partie d'un index direct pour ne pas le supprimer
Public IsIndexDirect As Boolean
