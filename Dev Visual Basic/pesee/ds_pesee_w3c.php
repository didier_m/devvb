<?php
/**************************************************************************************** DEAL INFORMATIQUE **!
!&DOC-TECHNIQUE ! dog/siexe/deal/emilie/php/ds_pesee_w3c.php                                                   !
!===============!=============================================================================================!
!&Fiche       Du   ! S.!   Operateur   !                      Motif de l'intervention                         !
!__________________!___!_______________!_____________________________________________________________________-!
!        2024-08-22!   !didier marchal !FRW-1707 [FRAMEWORK][EMILIE] - module de communication pont bascule av!
!        2024-08-07!   !didier marchal !FRW [FRAMEWORK][EMILIE] - ajout module de pesee pour le drivers       !
!_____________________________________________________________________________________________________________!
!                                                        M A I N                                              !
!_____________________________________________________________________________________________________________!
&End                                                                                                          
**************************************************************************************************************/

//WatchDir & "/siexe/deal/emilie/php/pesee_w3c.php?&_action=SET&_IniFile=" & IniFile & "&_AppName=" & AppName & "&_lpKeyName=" & lpKeyName & "&_Value=" & Value, True      'false pour sync
// https://172.20.124.200/siexe/deal/emilie/php/ds_pesee_w3c.php?&_action=SET&_IniFile=D:\deal\v6\dog\partageweb\xcspool\pesee\pesee-vm-1.txt&_AppName=POIDS&_lpKeyName=ASK&_Value=&tick=1179327406
// Cas du setIniString
$i = 0;
start:
if($_GET["_action"] == "SET"){
    if($_GET["_IniFile"] != ""){
        $ini_array = parse_ini_file($_GET["_IniFile"], true);
        if($_GET["_AppName"] != "" && $_GET["_lpKeyName"] != ""){
            if(!isset($ini_array[$_GET["_AppName"]])) $ini_array[$_GET["_AppName"]] = array(); 
            $ini_array[$_GET["_AppName"]][$_GET["_lpKeyName"]] = $_GET["_Value"];
        }
    }
    write_php_ini($ini_array , $_GET["_IniFile"]);
    error_log("SET succes: ".$_GET["_lpKeyName"]."=".$_GET["_Value"]);
    echo "SET succes.";
    exit;
}

if($_GET["_action"] == "GET"){
    if(isset($_GET["_IniFile"]) && $_GET["_IniFile"] != ""){
        $ini_array = parse_ini_file($_GET["_IniFile"], true);
        if(array_key_exists("_AppName", $_GET) && $_GET["_AppName"] != ""){
            if(isset($ini_array[$_GET["_AppName"]]) && isset($ini_array[$_GET["_AppName"]][$_GET["_lpKeyName"]])) {
                //error_log( print_r($ini_array, true));
				echo $ini_array[$_GET["_AppName"]][$_GET["_lpKeyName"]];
                exit;
            } else {
                //si demande en cours ou pas trait�e recommencer
				if(array_key_exists("NET", $ini_array[$_GET["_AppName"]]) && $ini_array[$_GET["_AppName"]]["NET"]=="" && $i < 10){
                    error_log("oupsNet"); $i++;    sleep(1); goto start;
                }
				if(array_key_exists("ASK", $ini_array[$_GET["_AppName"]]) && $ini_array[$_GET["_AppName"]]["ASK"]!="" && $i < 10){
                    error_log("oupsAsk"); $i++;    sleep(1); goto start;
                }
                error_log("GET all.");
                echo json_encode($ini_array[$_GET["_AppName"]]);
                exit;
            }
        }
        echo $_GET["_AppName"] . " - " . $_GET["_lpKeyName"] . " key not found";
    } else{
        echo $_GET["_IniFile"] . " Not found.";
    }
    echo print_r($ini_array, true);
    exit;
}

function write_php_ini($array, $fileName){
    $res = array();
    foreach($array as $key => $val)
    {
        if(is_array($val))
        {
            $res[] = "[$key]";
            foreach($val as $skey => $sval) $res[] = "$skey=".(is_numeric($sval) ? $sval : ''.$sval.'');
        }
        else $res[] = "$key=".(is_numeric($val) ? $val : ''.$val.'');
    }
    safefilerewrite($fileName, implode("\r\n", $res));
}

function safefilerewrite($fileName, $dataToSave){
    if ($fp = fopen($fileName, 'w')){
        $startTime = microtime(TRUE);
        do
        {            $canWrite = flock($fp, LOCK_EX);
           // If lock not obtained sleep for 0 - 100 milliseconds, to avoid collision and CPU load
           if(!$canWrite) usleep(round(rand(0, 100)*1000));
        } while ((!$canWrite)and((microtime(TRUE)-$startTime) < 5));

        //file was locked so now we can store information
        if ($canWrite)
        {            fwrite($fp, $dataToSave);
            flock($fp, LOCK_UN);
        }
        fclose($fp);
    }

}

if($_GET["_action"] == "SCREEN"){?>    
<script src="do-dogexchange_w3c.js"></script>
<script>
<?
$_IniFile_1="D:/deal/v6/dog/partageweb/xcspool/pesee/pesee-vm-1.txt";
if(isset($_GET["_IniFile_1"]))$_IniFile_1 = $_GET["_IniFile_1"];
$_IniFile_2="D:/deal/v6/dog/partageweb/xcspool/pesee/pesee-vm-2.txt";
if(isset($_GET["_IniFile_2"]))$_IniFile_1 = $_GET["_IniFile_2"];
?>
chemin_poids_1 = '<?=$_IniFile_1?>';
chemin_poids_2 = '<?=$_IniFile_2?>';

function poidsAsk(strId,chemin_poids){
    //BRUT|NET|TARE|TICKET|DATE|HEURE|STATUT|BRUT|NETSIGNE|BRUTSIGNE|TARESIGNE
    urlAskPoids = 'ds_pesee_w3c.php';    
    var pl = new SOAPClientParameters();
    var retourInvoke = SOAPClient.invoke(urlAskPoids + encodeURI("?&_action=SET&_IniFile="+chemin_poids+"&_AppName=POIDS&_lpKeyName=ASK&_Value=TRUE&_cachekiller=" + Math.floor(Math.random() * 1001)), "", pl, false);
    setTimeout(function(){poidsGet(strId,chemin_poids);},500);
}

function poidsGet(strId,chemin_poids){
    var pl = new SOAPClientParameters();
    var strGetPoids = SOAPClient.invoke(urlAskPoids + encodeURI("?&_action=GET&_IniFile="+chemin_poids+"&_AppName=POIDS&_lpKeyName=&_cachekiller=" + Math.floor(Math.random() * 1001)), "", pl, false);
    document.getElementById("POIDS").value = strGetPoids;
    jsPoids = JSON.parse(strGetPoids);
    document.getElementById(strId).value = jsPoids.NET;
}

</script>

<body>
    <?=$_IniFile_1?>
    <?=$_IniFile_2?>
    <div >
        <input id="POIDS"/>      
        <p>pdsbrut</p>
        <input id="DICT�pdsbrut"/>      
        <a href="#" id="balancebrut"  title="Lecture du poids sur le pont bascule" onclick="poidsAsk('DICT�pdsbrut',chemin_poids_1)">P1</a>
        <a href="#" id="balancebrut2" title="Lecture du poids sur le pont bascule" onclick="poidsAsk('DICT�pdsbrut',chemin_poids_2)">P2</a>
    </div>
</body>

<?
} else {
    echo "No action provide.";
    echo print_r($_GET, true);
    echo $_GET["_action"];
}