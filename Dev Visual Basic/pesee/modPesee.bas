Attribute VB_Name = "modPesee"
'-> API de lecture des fichiers au format "*.ini"
Public Declare Function GetPrivateProfileSection& Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)
Public Declare Function DeletePrivateProfileSection Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Long, ByVal lpString As Long, ByVal lpFileName As String) As Long

Public Sub Main()

If InStr(1, Command$, "console", vbTextCompare) <> 0 Then
    Load frmSysTray
    PontBascule.Init
Else
    PontBascule.Show
End If
End Sub


Public Function GetSettingIni(AppPathTitle As String, lpKeySection As String, lpKeyName As String, Default As String) As String

Dim Res As Long
Dim lpBuffer As String
Dim nCnt As Integer
Dim cString As String

lpBuffer = Space$(320000)
Res = GetPrivateProfileString("Properties", lpKeyName, "NULL", lpBuffer, Len(lpBuffer), App.Path & "\pesee.ini")
lpBuffer = Entry(1, lpBuffer, Chr(0))
If lpBuffer = "NULL" Then
    GetSettingIni = ""
Else
    lpBuffer = Mid$(lpBuffer, 1, Res)
    GetSettingIni = lpBuffer
End If


End Function

Public Function SaveSettingIni(AppPathTitle As String, lpKeySection As String, lpKeyName As String, Value) As String
'SaveSettingIni App.Path & "\" & App.Title, "Properties", "Settings", PontBascule.MSComm1.Settings
Dim Res As Long

Res = WritePrivateProfileString("Properties", lpKeyName, CStr(Value), App.Path & "\pesee.ini")


End Function


Public Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Integer
Dim PosEnCour As Integer
Dim i As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
End Function

Public Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Integer

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Integer
Dim i As Integer
Dim PosAnalyse As Integer

On Error GoTo GestError

'-< Analyse du vecteur
PosAnalyse = 1
Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1
'-> Quitter la fonction
Exit Function

GestError:
    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
End Function


Public Function FileExist(Fichier As String) As Boolean

On Error GoTo GestError

If Dir$(Fichier) <> "" Then FileExist = True
Exit Function

GestError:
    FileExist = False

End Function

Public Sub Trace(Optional sText As String)
    
    Dim hdlMouchard As Long
    Dim Mouchard As String
    
    On Error GoTo GestError
    Mouchard = App.Path & "\log.txt"
    If FileExist(Mouchard) Then
        hdlMouchard = FreeFile
        If FileLen(Mouchard) > 100000 Then Kill Mouchard
        Open Mouchard For Append As #hdlMouchard
        '-> ecrire la ligne
        If sText = "" Then
            If Err.Description <> "" Then Print #hdlMouchard, Format(Now, "hh:mm:ss") & "Err : " & Err.Description
            Err.Clear
        Else
            Print #hdlMouchard, Format(Now, "hh:mm:ss    ") & sText
        End If
        Close #hdlMouchard
    End If

GestError:
End Sub

