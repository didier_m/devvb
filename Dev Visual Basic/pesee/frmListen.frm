VERSION 5.00
Begin VB.Form frmListen 
   Caption         =   "Form1"
   ClientHeight    =   3015
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   ScaleHeight     =   3015
   ScaleWidth      =   4560
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer tmrUpdate 
      Interval        =   20000
      Left            =   3960
      Top             =   0
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Stop"
      Height          =   975
      Left            =   1800
      TabIndex        =   4
      Top             =   120
      Width           =   1575
   End
   Begin VB.TextBox txtNick 
      Height          =   285
      Left            =   0
      TabIndex        =   3
      Top             =   1560
      Width           =   3615
   End
   Begin VB.TextBox txtin 
      Height          =   975
      Left            =   0
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   1920
      Width           =   3615
   End
   Begin VB.TextBox txtOut 
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   1200
      Width           =   3615
   End
   Begin VB.CommandButton cmdListen 
      Caption         =   "Listen"
      Height          =   975
      Left            =   0
      TabIndex        =   0
      ToolTipText     =   "Listen for others to connect you (SERVER)"
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label lblState 
      AutoSize        =   -1  'True
      Caption         =   "State :"
      Height          =   195
      Left            =   0
      TabIndex        =   5
      Top             =   3000
      Width           =   3585
   End
End
Attribute VB_Name = "frmListen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Public bListen As Boolean
Public sListen As String
Public aListen As Integer

Private Declare Function GetIpAddrTable_API Lib "IpHlpApi" Alias "GetIpAddrTable" (pIPAddrTable As Any, pdwSize As Long, ByVal bOrder As Long) As Long

' Returns an array with the local IP addresses (as strings).
' Author: Christian d'Heureuse, www.source-code.biz
Public Function GetIpAddrTable()
   Dim Buf(0 To 511) As Byte
   Dim BufSize As Long: BufSize = UBound(Buf) + 1
   Dim rc As Long
   rc = GetIpAddrTable_API(Buf(0), BufSize, 1)
   If rc <> 0 Then Err.Raise vbObjectError, , "GetIpAddrTable failed with return value " & rc
   Dim NrOfEntries As Integer: NrOfEntries = Buf(1) * 256 + Buf(0)
   If NrOfEntries = 0 Then GetIpAddrTable = Array(): Exit Function
   ReDim IpAddrs(0 To NrOfEntries - 1) As String
   Dim i As Integer
   For i = 0 To NrOfEntries - 1
      Dim j As Integer, s As String: s = ""
      For j = 0 To 3: s = s & IIf(j > 0, ".", "") & Buf(4 + i * 24 + j): Next
      IpAddrs(i) = s
      Next
   GetIpAddrTable = IpAddrs
End Function

' Test program for GetIpAddrTable.
Public Function myIPs() As String
   Dim IpAddrs
   IpAddrs = GetIpAddrTable
   Dim i As Integer
   For i = LBound(IpAddrs) To UBound(IpAddrs)
      If myIPs = "" Then
        myIPs = IpAddrs(i)
      Else
        myIPs = myIPs & "," & IpAddrs(i)
      End If
   Next
End Function

Sub UpdateState() ' sub for controling sck.state
    'On Error Resume Next

    ' print the state of sck
    Select Case Sck.State
        Case 0
            MDIMain.StatusBar1.Panels(2).Text = "Closed"
        Case 1
            MDIMain.StatusBar1.Panels(2).Text = "Open"
        Case 2
            MDIMain.StatusBar1.Panels(2).Text = "Listening"
        Case 3
            MDIMain.StatusBar1.Panels(2).Text = "ConnectionPending"
        Case 4
            MDIMain.StatusBar1.Panels(2).Text = "ResolvingHost"
        Case 5
            MDIMain.StatusBar1.Panels(2).Text = "HostResolved"
        Case 6
            MDIMain.StatusBar1.Panels(2).Text = "Connecting"
        Case 7
            MDIMain.StatusBar1.Panels(2).Text = "Connected"
        Case 8
            MDIMain.StatusBar1.Panels(2).Text = "Closing"
        Case 9
            MDIMain.StatusBar1.Panels(2).Text = "Error"
            Sck.Close
            Sck.LocalPort = 6001
            Sck.Listen ' listen for others to connect you
            startListen
    End Select
End Sub
Private Sub cmdListen_Click()
    ' set localport for listening ( you can change it whatever you want )
    On Error Resume Next
    If Err.Description <> "" Then
        Trace "Av" & Err.Description
        Err.Clear
    End If
    bListen = True
    Sck.Close
    Sck.LocalPort = 6001
    Sck.Listen ' listen for others to connect you
    UpdateState
    startListen
    If Err.Description <> "" Then
        Trace "Ap" & Err.Description
        Err.Clear
    End If
End Sub

Private Sub Command1_Click()
    bListen = False
    Sck.Close
End Sub


Public Sub getFiles()
    On Error Resume Next
    Dim lFiles As String
    '--> on va demander au serveur si on a des nouveaux spools
    Dim pListen As String
    Dim myMSXML As Object
    pListen = GetIniString("PARAM", "LISTEN", App.Path & "\Turbograph.ini", False)
    pListen = DeCrypt(pListen)
    pListen = Replace(pListen, "::", "€")
    pListen = Entry(2, pListen, "€")
    
    If pListen = "" Then Exit Sub
    MDIMain.StatusBar1.Panels(2).Text = "Reading..."
    Set myMSXML = CreateObject("MSXML2.XMLHTTP.6.0")
    DoEvents
    myMSXML.Open "GET", WatchDir & "/siexe/deal/emilie/php/pesee_w3c.php?&files=" & pListen & "&tick=" & CStr(GetTickCount), True    'false pour sync
    myMSXML.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    myMSXML.Send
    
    Sleep 2000
    MDIMain.StatusBar1.Panels(2).Text = ""
    DoEvents
    lFiles = ""
    lFiles = myMSXML.responseText
    MDIMain.StatusBar1.Panels(2).Text = lFiles
    Trace lFiles
    '--> on provoque l'ouverture/impression des fichiers
    If lFiles <> "" Then traiteListen (lFiles)
    
End Sub

' url encodes a string
Function URLEncode(ByVal str As String) As String
        Dim intLen As Integer
        Dim X As Integer
        Dim curChar As Long
                Dim newStr As String
                intLen = Len(str)
        newStr = ""
                        For X = 1 To intLen
            curChar = Asc(Mid$(str, X, 1))
            
            If (curChar < 48 Or curChar > 57) And _
                (curChar < 65 Or curChar > 90) And _
                (curChar < 97 Or curChar > 122) Then
                                newStr = newStr & "%" & Hex(curChar)
            Else
                newStr = newStr & Chr(curChar)
            End If
        Next X
        
        URLEncode = newStr
End Function

Public Sub filePrint(strFichier As String)
            
        '-> Lancer l'impression
        CurrentPIDProcess = Shell(App.Path & "\TurboGraph.exe " & DeviceName & "~DIRECT~" & NbCopies & "|" & FileToPrint & "|" & RectoVerso & "|" & Assembl & "|" & NoGard & "|" & printFilesJoins, vbNormalFocus)
        
        Do While IsPidRunning(CurrentPIDProcess)
            '-> Libération de la pile des messages
            DoEvents
            Sleep 500
        Loop

End Sub


