Attribute VB_Name = "FctFred"
'-> d�finition des variables ( public ) et des fonctions partag�es
Option Explicit
'Public enregs As Collection
Public DataFile As String

Private Sub Main()

Dim LigneCde As String
Dim Dcomma As Boolean
Dim Dtab As Boolean
Dim Dconsec As Boolean
Dim Dsemi As Boolean
Dim Dspace As Boolean
Dim aExcelApp As Object
Dim aWorkBook As Object
Dim aWorkSheet As Object
Dim DataFeuille As String
Dim Delimiter As String
Dim Separator_Decimal As String
Dim ClasseurXlt As String
Dim FeuilleDef As String
Dim TcdDef As String
Dim FeuilleGraph As String
Dim Macro As String

On Error GoTo GestError

'-> Tester la ligne de commande
LigneCde = Command$
If Trim(LigneCde) = "" Then Exit Sub

'-> Afficher la lfeuille de temporisation
' df_Wait.Show

'-> R�cup�ration de la ligne de commande
DataFile = Trim(Entry(1, LigneCde, "�"))
DataFeuille = Trim(Entry(2, LigneCde, "�"))
Delimiter = Trim(Entry(3, LigneCde, "�"))
Separator_Decimal = Trim(Entry(4, LigneCde, "�"))
ClasseurXlt = Trim(Entry(5, LigneCde, "�"))
FeuilleDef = Trim(Entry(6, LigneCde, "�"))
TcdDef = Trim(Entry(7, LigneCde, "�"))
FeuilleGraph = Trim(Entry(8, LigneCde, "�"))
Macro = Trim(Entry(9, LigneCde, "�"))
    
'MsgBox " DataFile " & DataFile
'MsgBox " Datafeuille " & DataFeuille
'MsgBox " Delimiter " & Delimiter
'MsgBox " Separator_Decimal " & Separator_Decimal
'MsgBox " ClasseurXlt " & ClasseurXlt
'MsgBox " FeuilleDef " & FeuilleDef
'MsgBox " TcdDef " & TcdDef
'MsgBox " FeuilleGraph " & FeuilleGraph
'MsgBox " Macro " & Macro
    
' --> Creer un pointeur vers Excel
Set aExcelApp = CreateObject("excel.application")

'MsgBox " Creation du pointeur vers Excel "

' --> Rendre Excel muet
aExcelApp.DisplayAlerts = False

'MsgBox " Rendre Excel Muet "

' --> Ouvrir ce classeur
Set aWorkBook = aExcelApp.Workbooks.Open(ClasseurXlt)

'MsgBox " Ouvrir le Classeur " & ClasseurXlt

If Delimiter = ";" Then
    Dcomma = False
    Dtab = False
    Dconsec = False
    Dsemi = True
    Dspace = False
Else
    If Delimiter <> "" Then
        Dcomma = False
        Dtab = False
        Dconsec = False
        Dsemi = False
        Dspace = False
    End If
End If

'MsgBox " DComma  " & Dcomma
'MsgBox " DTab    " & Dtab
'MsgBox " Dconsec " & Dconsec
'MsgBox " DSemi   " & Dsemi
'MsgBox " DSpace  " & Dspace

If Dsemi = True Then Delimiter = ""
If Separator_Decimal = "" Then Separator_Decimal = "."

' --> Load du fichier Data dans la feuille
Set aWorkSheet = aWorkBook.Worksheets(DataFeuille)
With aWorkSheet.QueryTables.Add(Connection:="TEXT;" & DataFile, Destination:=aWorkSheet.Range("A1"))
     .Name = "export"
     .FieldNames = True
     .RowNumbers = False
     .FillAdjacentFormulas = False
     .PreserveFormatting = True
     .RefreshOnFileOpen = False
     .RefreshStyle = 1
     .SavePassword = False
     .SaveData = True
     .AdjustColumnWidth = True
     .RefreshPeriod = 0
     .TextFilePromptOnRefresh = False
     .TextFilePlatform = 1252
     .TextFileStartRow = 1
     .TextFileParseType = 1
     .TextFileTextQualifier = 1
     .TextFileConsecutiveDelimiter = Dconsec
     .TextFileTabDelimiter = Dtab
     .TextFileSemicolonDelimiter = Dsemi
     .TextFileCommaDelimiter = Dcomma
     .TextFileSpaceDelimiter = Dspace
     .TextFileOtherDelimiter = Delimiter
     '.TextFileColumnDataTypes = Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
     .TextFileTrailingMinusNumbers = True
     .TextFileDecimalSeparator = Separator_Decimal
     .Refresh BackgroundQuery:=False
End With

'MsgBox " Load du fichier Data " & DataFeuille

' --> Selectionner la totalit� de la feuille de donn�es
aWorkSheet.Activate

'MsgBox " Selection de la totalit� de la feuille " & DataFeuille

' --> Desactiver la fenetre de l'assistant qui par d�faut est � True
aExcelApp.ActiveWorkbook.ShowPivotTableFieldList = False

'MsgBox " Desactiver la fen�tre de l'assistant "

' --> Selectionner la feuille
If Trim(FeuilleDef) <> "" Then
    aWorkBook.Worksheets(FeuilleDef).Select
Else
    aWorkBook.Worksheets(DataFeuille).Select
End If

'MsgBox " Selection de la Feuille " & FeuilleDef

' --> Actualiser les donn�es dans le TCD
If Trim(TcdDef) <> "" Then aExcelApp.ActiveSheet.PivotTables(TcdDef).PivotCache.Refresh

'MsgBox " Actualisation du TCD " & TcdDef

' --> Selectionner la feuille du Graphique
If Trim(FeuilleGraph) <> "" Then aWorkBook.Sheets(FeuilleGraph).Select

'MsgBox " Selection de la Feuille Graphique " & FeuilleGraph

'MsgBox " Avant Rendre visible Excel "
'MsgBox " Avant run de la Macro " & Macro

' --> Rendre visible Excel
aExcelApp.Visible = True

'-> Masquer la feuille
'aWorkSheet.Visible = 0

' --> Simuler l'Apercu avant Impression
If Trim(Macro) <> "" Then aExcelApp.Run Macro

GestError:

' --> Afficher le code erreur
If Err.Number <> 0 Then
    MsgBox Err.Number & " - " & Err.Description
    If Not aExcelApp Is Nothing Then aExcelApp.Quit
End If

' --> Obligatoire : toujours lib�rer le pointeur vers Excel
Set aWorkSheet = Nothing
Set aWorkBook = Nothing
Set aExcelApp = Nothing

' Unload df_Wait
        
End Sub

Private Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Integer

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Integer
Dim i As Integer
Dim PosAnalyse As Integer

On Error GoTo GestError

'-< Analyse du vecteur
PosAnalyse = 1
Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1
'-> Quitter la fonction
Exit Function

GestError:
    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
End Function

Private Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Integer
Dim PosEnCour As Integer
Dim i As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + Len(Separateur) > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + Len(Separateur), Len(Vecteur) - PosEnCour + Len(Separateur))
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + Len(Separateur)
Loop
        
    

End Function
