VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   12360
   ClientLeft      =   48
   ClientTop       =   348
   ClientWidth     =   18612
   LinkTopic       =   "Form1"
   ScaleHeight     =   12360
   ScaleWidth      =   18612
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ListView ListView1 
      Height          =   11292
      Left            =   720
      TabIndex        =   0
      Top             =   1080
      Width           =   17532
      _ExtentX        =   30925
      _ExtentY        =   19918
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ligne As String
Dim indice As Integer
Dim aitem As ListItem

Private Const LVM_SETCOLUMNWIDTH = 4126
Private Declare Function SendMessage& Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any)

Private Sub Form_Load()
    OpenFile ("c:\travail\solbud4.csv")
End Sub

Private Sub OpenFile(Fichier As String)

Dim Nblig As Integer
Dim hdlFile As Integer
hdlFile = FreeFile
Dim a As ListSubItem

Open Fichier For Input As #hdlFile
Do While Not EOF(hdlFile)
    Line Input #hdlFile, ligne
    Nblig = Nblig + 1
    If Nblig = 1 Then
        For indice = 1 To NumEntries(ligne, ";")
             If Entry(indice, ligne, ";") <> "" Then
             Me.ListView1.ColumnHeaders.Add , , Entry(indice, ligne, ";")
             End If
        Next
    Else
        Set aitem = Me.ListView1.ListItems.Add
        aitem.Key = "ENREG|" & CStr(Nblig)
        aitem.Text = Entry(1, ligne, ";")
        For indice = 1 To Me.ListView1.ColumnHeaders.Count - 1
                aitem.SubItems(indice) = Entry(indice + 1, ligne, ";")
                If IsNumeric(aitem.SubItems(indice)) Then
                    Me.ListView1.ColumnHeaders(indice + 1).Alignment = lvwColumnRight
                End If
                
        Next
        If Mid$(aitem, 1, 5) = "Total" Then
            aitem.Bold = True
        End If
    End If
Loop
Close #hdlFile

Call FormatListView(Me.ListView1)

End Sub

'Private Sub ListView1_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

'-> Trier sur les entetes de colonne
'Me.ListView1.SortKey = ColumnHeader.Index - 1
'If Me.ListView1.SortOrder = lvwAscending Then
'    Me.ListView1.SortOrder = lvwDescending
'Else
'    Me.ListView1.SortOrder = lvwAscending
'End If
'Me.ListView1.Sorted = True

'End Sub

Private Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Integer

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Integer
Dim i As Integer
Dim PosAnalyse As Integer

On Error GoTo GestError

'-< Analyse du vecteur
PosAnalyse = 1
Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1
'-> Quitter la fonction
Exit Function

GestError:
    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
End Function

Private Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Integer
Dim PosEnCour As Integer
Dim i As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + Len(Separateur) > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + Len(Separateur), Len(Vecteur) - PosEnCour + Len(Separateur))
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + Len(Separateur)
Loop
        
End Function

Public Sub FormatListView(List As Object)

Dim i As Long
Dim x As Object

'-> Ne rien faire si pas de colonnes
If List.ColumnHeaders.Count = 0 Then Exit Sub

'-> De base toujours cr�er un enregistrement avec les entetes de colonnes
Set x = List.ListItems.Add(, "DEALENREGENTETE")

For i = 0 To List.ColumnHeaders.Count - 1
    '-> Ajouter le libelle de l'entete de la colonne
    If i = 0 Then
        x.Text = List.ColumnHeaders(1).Text
    Else
        x.SubItems(i) = List.ColumnHeaders(i + 1).Text
    End If
    SendMessage List.hwnd, LVM_SETCOLUMNWIDTH, i, 0
Next

'-> Supprimer le premier enregistrement
List.ListItems.Remove ("DEALENREGENTETE")

End Sub


'If IsNumeric(Entry(indice + 1, ligne, ",")) Then
'If CInt(Entry(indice + 1, ligne, ",")) <> 0 Then
    'If indice > 1 Then
    '    aitem.SubItems(indice) = Format(Entry(indice + 1, ligne, ","), "#,###,##0.00")
    '    Me.ListView1.ColumnHeaders(indice + 1).Alignment = lvwColumnRight
    'End If
    'Set a = aitem.ListSubItems.Add
    'If Mid$(aitem, 1, 5) = "Total" Then
    '     a.Bold = True
    '     'a.ForeColor = RGB(0, 255, 0)
    'End If
'End If
'End If




'API
'
'Private Declare Function GetLocaleInfoVB& Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long)
'
'Variables � d�clarer
'
''-> Variables de r�cup�ration des infos locales
'Public SepDec As String 'S�parateur d�cimal
'Public SepMil As String 'S�parateur de millier
'Public FormatDateToApply As String 'Format de date � appliquer
'
'
'Public Function GetLocalInfo()
'
''---> Cette proc�dure r�cup�re les informations locales
'
'Dim lpbuffer As String
'Dim Res As Long
'Dim LOCALE_STHOUSAND As Long
'Dim LOCALE_USER_DEFAULT As Long
'Dim LOCALE_SDECIMAL As Long
'Dim LOCALE_IDATE As Long
'
''-> S�parateur de millier : gestion des variables d'environnement
'LOCALE_STHOUSAND = &HF
'LOCALE_USER_DEFAULT& = &H400
'LOCALE_SDECIMAL = 14
'
'lpbuffer = Space$(10)
'Res = GetLocaleInfoVB(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, lpbuffer, Len(lpbuffer))
'SepDec = Mid$(lpbuffer, 1, Res - 1)
'lpbuffer = Space$(10)
'Res = GetLocaleInfoVB(LOCALE_USER_DEFAULT, LOCALE_STHOUSAND, lpbuffer, Len(lpbuffer))
'SepMil = Mid$(lpbuffer, 1, Res - 1)
'
''-> Format des dates : gestion du format
'LOCALE_IDATE = &H21
'lpbuffer = Space$(10)
'Res = GetLocaleInfoVB(LOCALE_USER_DEFAULT, LOCALE_IDATE, lpbuffer, Len(lpbuffer))
'FormatDateToApply = Entry(1, lpbuffer, Chr(0))
'
'
'End Function
'Public Function Convert(ByVal StrToAnalyse As String) As String
'
'Dim i As Integer
'Dim Tempo As String
'Dim FindSep As Boolean
'
'
'For i = Len(StrToAnalyse) To 1 Step -1
'    If Mid$(StrToAnalyse, i, 1) = "." Then
'        If Not FindSep Then
'            Tempo = SepDec & Tempo
'            FindSep = True
'        End If
'    ElseIf Mid$(StrToAnalyse, i, 1) = "," Then
'        If Not FindSep Then
'            Tempo = SepDec & Tempo
'            FindSep = True
'        End If
'    Else
'        Tempo = Mid$(StrToAnalyse, i, 1) & Tempo
'    End If
'Next 'Pour tous les caract�res � analyser
'
'Convert = Tempo
'
'End Function
'
'If IsNumeric(Convert(Varaible)) Then
'
'
