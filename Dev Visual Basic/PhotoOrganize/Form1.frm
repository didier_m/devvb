VERSION 5.00
Begin VB.Form Form1 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFFFFF&
   Caption         =   "Mes Photos DMZ 2016"
   ClientHeight    =   3915
   ClientLeft      =   225
   ClientTop       =   570
   ClientWidth     =   10350
   FillColor       =   &H00FFC0FF&
   FillStyle       =   0  'Solid
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   NegotiateMenus  =   0   'False
   ScaleHeight     =   3915
   ScaleWidth      =   10350
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Param�tres"
      ForeColor       =   &H00000000&
      Height          =   2895
      Left            =   240
      TabIndex        =   2
      Top             =   120
      Width           =   10095
      Begin VB.CheckBox Check2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Check1"
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   480
         TabIndex        =   9
         Top             =   2400
         Width           =   255
      End
      Begin VB.CheckBox Check1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Check1"
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   480
         TabIndex        =   7
         Top             =   2060
         Width           =   255
      End
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   480
         TabIndex        =   4
         ToolTipText     =   "R�pertoire dont les dates de fichier seront �ventuellement modifi�es"
         Top             =   840
         Width           =   4335
      End
      Begin VB.TextBox Text2 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   480
         TabIndex        =   3
         ToolTipText     =   "R�pertoire servant de base de l'analyse"
         Top             =   1560
         Width           =   4335
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Supprimer les images identiques"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   840
         TabIndex        =   10
         Top             =   2505
         Width           =   3495
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Redater le fichier � partir de la date prise de vue"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   840
         TabIndex        =   8
         Top             =   2160
         Width           =   3495
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Dossier contenant les photos � r�organiser"
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   480
         TabIndex        =   6
         Top             =   600
         Width           =   3975
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "R�pertoire de destination peut �tre identique (@year @month @day)"
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   480
         TabIndex        =   5
         Top             =   1320
         Width           =   5175
      End
      Begin VB.Shape Shape12 
         BorderStyle     =   0  'Transparent
         FillColor       =   &H00FFFFFF&
         FillStyle       =   0  'Solid
         Height          =   1335
         Left            =   6000
         Top             =   720
         Width           =   3735
      End
   End
   Begin VB.CommandButton Command1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FF8080&
      Caption         =   "Lancer le traitement"
      Height          =   495
      Left            =   8040
      MaskColor       =   &H00FFC0C0&
      TabIndex        =   0
      Top             =   3240
      Width           =   2175
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   3360
      Width           =   7680
   End
   Begin VB.Menu LVpopupmenu 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu mnuExportexcel 
         Caption         =   "E&xporter sous Excel"
      End
      Begin VB.Menu empty 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCompare 
         Caption         =   "&Comparer A et B"
      End
      Begin VB.Menu mnuFileOpenAB 
         Caption         =   "Ouvrir A et B"
      End
      Begin VB.Menu mnuExportSuppr 
         Caption         =   "Supprimer de l'export"
      End
      Begin VB.Menu mnuFileA 
         Caption         =   "Fichier(s) A"
         Begin VB.Menu mnuExportA 
            Caption         =   "Exporter"
         End
         Begin VB.Menu mnuHorodatage 
            Caption         =   "Horodatage selon fiche"
         End
         Begin VB.Menu mnuFileOpenA 
            Caption         =   "Ouvrir"
         End
         Begin VB.Menu mnuFilePropertiesA 
            Caption         =   "Propri�t�s"
         End
      End
      Begin VB.Menu mnuFileB 
         Caption         =   "Fichier(s) B"
         Begin VB.Menu mnuExportB 
            Caption         =   "Exporter"
         End
         Begin VB.Menu mnuFileOpenB 
            Caption         =   "Ouvrir"
         End
         Begin VB.Menu mnuFilePropertiesB 
            Caption         =   "Propri�t�s"
         End
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

 Private Const NM_FIRST = &H0& '(0U- 0U)
 Private Const NM_CUSTOMDRAW = (NM_FIRST - 12)
 Private Const WM_NOTIFY = &H4E

 Private Const CDDS_PREPAINT = &H1
 Private Const CDDS_POSTPAINT = &H2
 Private Const CDDS_PREERASE = &H3
 Private Const CDDS_POSTERASE = &H4

 Private Const CDDS_ITEM = &H10000
 Private Const CDDS_ITEMPREPAINT = (CDDS_ITEM Or CDDS_PREPAINT)
 Private Const CDDS_ITEMPOSTPAINT = (CDDS_ITEM Or CDDS_POSTPAINT)
 Private Const CDDS_ITEMPREERASE = (CDDS_ITEM Or CDDS_PREERASE)
 Private Const CDDS_ITEMPOSTERASE = (CDDS_ITEM Or CDDS_POSTERASE)
 Private Const CDDS_SUBITEM = &H20000

 Private Const CDRF_DODEFAULT = &H0
 Private Const CDRF_NEWFONT = &H2
 Private Const CDRF_SKIPDEFAULT = &H4
 Private Const CDRF_NOTIFYPOSTPAINT = &H10
 Private Const CDRF_NOTIFYITEMDRAW = &H20
 Private Const CDRF_NOTIFYSUBITEMDRAW = &H20
 Private Const CDRF_NOTIFYPOSTERASE = &H40

 Private Type RECT
     left As Long
     top As Long
     right As Long
     bottom As Long
 End Type

 Private Type NMHDR
     hwndFrom As Long
     idFrom As Long
     code As Long
 End Type

 Private Type NMCUSTOMDRAW
     hdr As NMHDR
     dwDrawStage As Long
     hdc As Long
     rc As RECT
     dwItemSpec As Long
     uItemState As Long
     lItemlParam As Long
 End Type

 Private Type NMLVCUSTOMDRAW
     nmcd As NMCUSTOMDRAW
     clrText As Long
     clrTextBk As Long
     'Les membres suivants ne sont pas disponibles pour tous les OS
     ' iSubItem As Long
     ' dwItemType As Long
     ' clrFace As Long
     ' iIconEffect As Long
     ' iIconPhase As Long
     ' iPartId As Long
     ' iStateId As Long
     ' rcText As RECT
     ' uAlign As Long
 End Type

 Private Declare Sub CopyMemory _
     Lib "kernel32" _
     Alias "RtlMoveMemory" _
     ( _
     Destination As Any, _
     Source As Any, _
     ByVal Length As Long)

Private Sub Command1_Click()
'-> on fait un minimum des verifs
If Me.Command1.Caption = "STOP" Then
    quit = True
    Me.Command1.Caption = "Lancer le traitement"
    Exit Sub
End If
quit = False

If Trim(Me.Text1.Text) = "" Or Trim(Me.Text2.Text) = "" Then
    MsgBox ("R�pertoires non renseign�s!")
    Me.Text1.SetFocus
    Exit Sub
End If

wdir = Me.Text1.Text
rdir = Me.Text2.Text

If Mid(wdir, Len(wdir), 1) = "/" Or Mid(wdir, Len(wdir), 1) = "\" Then
Else
    wdir = wdir & "\"
End If

If Mid(rdir, Len(wdir), 1) = "/" Or Mid(rdir, Len(rdir), 1) = "\" Then
Else
    rdir = rdir & "\"
End If

rdir = Replace(rdir, "/", "\")
wdir = Replace(wdir, "/", "\")


'-> on lance l'analyse
recursive = True
Me.MousePointer = 11
Me.Command1.Caption = "STOP"

Set colPhoto = New clsPhoto
Set Photos = New Collection
'-
'--> on lance le traitement en se basant sur le r�pertoire de travail (tratement des differences des supprim�s et des identiques)
CreateFileC wdir, rdir, ""
DeleteEmptyFolders wdir
Me.MousePointer = 0
Me.Label4.Caption = "End..."
Me.Command1.Caption = "Lancer le traitement"
End Sub

Private Function FormatFileSize(ByVal Size As Long, FormatType As Integer) As String
'--> cette fonction permet de transformer la taille d'un fichier
Dim sRet As String
Const KB& = 1024
Const MB& = KB * KB

'FormatType = 0 Short String Format
'FormatType = 1 Long String Format
'FormatType = 2 Dual String Format

If Size < KB Then
   sRet = Format(Size, "#,##0") & " Bytes"
Else
   Select Case Size \ KB
      Case Is < 10
         sRet = Format(Size / KB, "0.00") & " KB"
      Case Is < 100
         sRet = Format(Size / KB, "0.0") & " KB"
      Case Is < 1000
         sRet = Format(Size / KB, "0") & " KB"
      Case Is < 10000
         sRet = Format(Size / MB, "0.00") & " MB"
      Case Is < 100000
         sRet = Format(Size / MB, "0.0") & " MB"
      Case Is < 1000000
         sRet = Format(Size / MB, "0") & " MB"
      Case Is < 10000000
         sRet = Format(Size / MB / KB, "0.00") & " GB"
   End Select
   
   Select Case FormatType
     Case 0 'Short
       sRet = sRet
     Case 1 'Long
       sRet = Format(Size, "#,##0") & " Bytes"
     Case 2 'Dual
       sRet = sRet & " (" & Format(Size, "#,##0") & " Bytes)"
   End Select
End If

FormatFileSize = sRet
End Function

Private Sub Form_Unload(Cancel As Integer)
'-> on parcour les controles pour eventuellement les memoriser
Dim aControl

For Each aControl In Me.Controls
    If TypeOf aControl Is TextBox Then
        SaveSetting App.Path & "\" & App.Title, "Properties", aControl.Name, aControl.Text
    End If
    If TypeOf aControl Is CheckBox Then
        SaveSetting App.Path & "\" & App.Title, "Properties", aControl.Name, aControl.value
    End If
Next

End Sub

