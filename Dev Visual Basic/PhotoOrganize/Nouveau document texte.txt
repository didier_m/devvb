1.Qu'est-ce qu'un commit

Un commit permet de prendre une photo d'un d�p�t � un instant 'T', ce qui va cr�er un point de contr�le permettant d'historiser des modifications, afin de suivre les �volutions du d�p�t.
Ce qui permettra de revenir sur cet �tat, et de comparer diff�rents commit. Sous 'git' on stocke une succession de commit qui repr�sentent ainsi des pointeurs vers chaque instantan� (snapshot).
A un commit sont li�s notamment : 
	un SHA-1, somme de contr�le, une sorte de carte d'identit� d'un commit, qui permet de pointer dessus, 
	Un libell� descriptif de la raison du commit
	L'auteur et la date du commit
	
2.� quoi sert la commande git log

Un 'git log' permet de revoir le fil des �v�nements d'un d�p�t en visualisant l'historique de commits.
Il pr�sente en ordre chronologique invers� les commits r�alis�s. Identifi�s par le SHA-1, le nom et l'e-mail de l'auteur, la date et le message du commit. 
En fonction des options il peut pr�senter les 'diff' des fichiers, afin de permettre une rapide revue de code. De naviguer rapidement � travers l'historique des modifications, pour en connaitre la nature et  les auteurs.

3.Qu'est-ce qu'une branche

Une branche est une divergence d'une ligne principale en une ligne secondaire, qui permet de faire des modifications sans affecter la ligne principale. Chose tr�s pratique par exemple pour la cr�ation de nouvelles fonctionnalit�s. 
Git permet de basculer d'une branche � une autre et ainsi de pointer sur des environnements qui ont diverg�s. Git permet �galement de fusionner des branches en ramenant les modifications d'une branche sur une autre branche.
