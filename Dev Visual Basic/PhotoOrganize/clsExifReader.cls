VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsExifReader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Offset2IFD0 As Integer
Private Offset2APP1 As Integer
Private Offset2TIFF As Integer
Private APP1Length As Integer
Private Offset2NextIFD As Integer
Private aryIFDDir() As IFD_Data
Private Offset2ExifSubIFD As Integer
Private strCurrFile As String    'used for debugging
Private IsLoaded As Boolean
Private boRL As Boolean  'if JPG file is Intel R-to-L type
Private Const maxStrLgth As Integer = 60 'may be changed; max length of tag string retrieved

Private Enum ExifFormatEnum
  fmtBYTE = 1
  fmtSTRING = 2
  fmtSHORT = 3
  fmtLONG = 4
  fmtRATIONAL = 5
  fmtSBYTE = 6
  fmtUNDEFINED = 7
  fmtSSHORT = 8
  fmtSLONG = 9
  fmtSRATIONAL = 10
  fmtSINGLE = 11
  fmtDOUBLE = 12
End Enum

Private Type IFD_Data
   Tag_No As ExifTagEnum
   Data_Format As ExifFormatEnum
   TagCount As Integer
   Offset_To_Value As Integer
   Value As String     ' was Object: changed 12/26/06
   TagOffset As Integer  'location of tag within byte array (0 = 1st position)
End Type

Public Enum ExifTagEnum
    'IFD0 Tags
  ImageDescription = &H10E '270
  Make = &H10F '271
  Model = &H110 '272
  Orientation = &H112 '274
  XResolution = &H11A '282
  YResolution = &H11B '283
  ResolutionUnit = &H128 '296
  Software = &H131 '305
  DateTime = &H132 '306
  WhitePoint = &H13E '318
  PrimaryChromaticities = &H13F '319
  YCbCrCoefficients = &H211 '529
  YCbCrPositioning = &H213 '531
  ReferenceBlackWhite = &H214 '532
  Copyright = &H8298 '33432
  ExifOffset = &H8769 '34665
    'ExifSubIFD Tags
  ExposureTime = &H829A '33434
  FNumber = &H829D '33437
  ExposureProgram = &H8822 '34850
  ISOSpeedRatings = &H8827 '34855
  ExifVersion = &H9000 '36864
  DateTimeOriginal = &H9003 '36867
  DateTimeDigitized = &H9004 '36868
  ComponentsConfiguration = &H9101 '37121
  CompressedBitsPerPixel = &H9102 '37122
  ShutterSpeedValue = &H9201 '37377
  ApertureValue = &H9202 '37378
  BrightnessValue = &H9203 '37379
  ExposureBiasValue = &H9204 '37380
  MaxApertureValue = &H9205 '37381
  SubjectDistance = &H9206 '37382
  MeteringMode = &H9207 '37383
  LightSource = &H9208 '37384
  Flash = &H9209 '37385
  FocalLength = &H920A '37386
  MakerNote = &H927C '37500
  UserComment = &H9286 '37510
  SubsecTime = &H9290 '37520
  SubsecTimeOriginal = &H9291 '37521
  SubsecTimeDigitized = &H9292 '37522
  FlashPixVersion = &HA000 '40960
  ColorSpace = &HA001 '40961
  ExifImageWidth = &HA002 '40962
  ExifImageHeight = &HA003 '40963
  RelatedSoundFile = &HA004 '40964
  ExifInteroperabilityOffset = &HA005 '40695
  FocalPlaneXResolution = &HA20E '41486
  FocalPlaneYResolution = &HA20F '41487
  FocalPlaneResolutionUnit = &HA210 '41488
  ExposureIndex = &HA215 '41493
  SensingMethod = &HA217 '41497
  FileSource = &HA300 '41728
  SceneType = &HA301 '41729
  CFAPattern = &HA302 '41730 CFA = Color Filter Array
    'Interoperability IFD Tags
  InteroperabilityIndex = &H1
  InteroperabilityVersion = &H2
  RelatedImageFileFormat = &H1000
  RelatedImageWidth = &H1001
  RelatedImageLength = &H1002
    'IFD1 Tags
  ImageWidth = &H100
  ImageHeight = &H101
  BitsPerSample = &H102
  Compression = &H103
  PhotometricInterpretation = &H106
  StripOffsets = &H111
  SamplePerPixel = &H115
  RowsPerStrip = &H116
  StripByteCounts = &H117
  XResolution2 = &H11A
  YResolution2 = &H11B
  PlanarConfiguration = &H11C
  ResolutionUnit2 = &H128
  JPEGInterchangeFormat = &H201
  JPEGInterchangeFormatLength = &H202
  YCbCrCoeffecients = &H211
  YCbCrSubSampling = &H212
  YCbCrPositioning2 = &H213
  ReferenceBlackWhite2 = &H214
    'Misc Tags
  NewSubfileType = &HFE
  SubfileType = &HFF
  TransferFunction = &H12D
  Artist = &H13B
  Predictor = &H13D
  TileWidth = &H142
  TileLength = &H143
  TileOffsets = &H144
  TileByteCounts = &H145
  SubIFDs = &H14A
  JPEGTables = &H15B
  CFARepeatPatternDim = &H828D
  CFAPattern2 = &H828E
  BatteryLevel = &H828F
  IPTC_NAA = &H83BB '33723
  InterColorProfile = &H8773
  SpectralSensitivity = &H8824
  GPSInfo = &H8825
  OECF = &H8828
  Interlace = &H8829
  TimeZoneOffset = &H882A
  SelfTimerMode = &H882B
  FlashEnergy = &H920B
  SpatialFrequencyResponse = &H920C
  Noise = &H920D
  ImageNumber = &H9211 '37393
  SecurityClassification = &H9212
  ImageHistory = &H9213
  SubjectLocation = &H9214
  ExposureIndex2 = &H9215
  TIFFEPStandardID = &H9216
  FlashEnergy2 = &HA20B
  SpatialFrequencyResponse2 = &HA20C
  SubjectLocation2 = &HA214
End Enum

Public Property Get Tag(Optional ByVal ExifTag As ExifTagEnum = 0) As String    'was Object
    Tag = ""     'initialize to empty string
    If ExifTag = 0 Then Tag = ""  '0 is not a valid tag #

    Dim i As Integer

    For i = 1 To UBound(aryIFDDir)
      If aryIFDDir(i).Tag_No = ExifTag Then
        Tag = aryIFDDir(i).Value
      End If
    Next
End Property

Public Property Get BtAryOffset(Optional ByVal ExifTag As ExifTagEnum = 0) As Integer
  
    BtAryOffset = 0   'initialize to default

    If ExifTag = 0 Then BtAryOffset = 0 '0 is not a valid tag #

    Dim Ix As Integer

    For Ix = 1 To UBound(aryIFDDir)
      If aryIFDDir(Ix).Tag_No = ExifTag Then
        BtAryOffset = aryIFDDir(Ix).TagOffset
      End If
    Next

  
End Property


Public Function ExifLoad(ByVal picFile As String) As Boolean 'returns True if Exif block structure OK

  'Debug.Print("****Begin ExifLoad Routine**** " & Now)
  'Debug.Print("m_file is >" & picFile & "<")

  If InspectByteArray() = False Then 'if inspection fails
    IsLoaded = False
    ExifLoad = False                   ' quit
  Else
    strCurrFile = picFile     'save file name for debugging
  End If

  Call subRtoLTest
  If boRL Then     'get IFD0 offset, R to L if Intel
    Offset2IFD0 = BitConverter.ToInt32(ExifTemp, Offset2APP1 + 14)
  Else
    Offset2IFD0 = fnBA2Nbr(Offset2APP1 + 14, 4, True) 'L to R
  End If

  'Debug.Print("bo-RL (Minolta-Intel) = " & boRL)
  'Debug.Print("Offset to APP1: " & Offset2APP1)
  'Debug.Print("Length of APP1: " & APP1Length)
  'Debug.Print("Offset to IFD0: " & Offset2IFD0)
  'Debug.Print("Offset to TIFF: " & Offset2TIFF)

  ReDim aryIFDDir(-1)       'reset the array: added 1/4/2007
  GetTags (Offset2TIFF + Offset2IFD0)
  IsLoaded = True
  ExifLoad = True

End Function

Private Function InspectByteArray() As Boolean
  'validate that file is jpeg and get offset values
  Dim Ix As Integer

  If "" + ExifTemp(0) <> "&HFFS" And "" + ExifTemp(1) <> "&HD8S" Then
    InspectByteArray = False 'if not a jpeg file
  Else

    For Ix = 2 To UBound(ExifTemp) - 1
      If "" + ExifTemp(Ix) = "&HFFS" And "" + ExifTemp(Ix + 1) = "&HE1S" Then
        Offset2APP1 = Ix 'set APP1 offset value
        Exit For
      End If
    Next

    If Offset2APP1 = 0 Then 'if APP1 offset not found
      InspectByteArray = False
    End If

    Offset2TIFF = Offset2APP1 + 10 'set TIFF offset

    APP1Length = ExifTemp(Offset2APP1 + 2) * 256 + ExifTemp(Offset2APP1 + 3)  'note: value is not used

    If Chr(ExifTemp(Offset2APP1 + 4)) & Chr(ExifTemp(Offset2APP1 + 5)) & _
        Chr(ExifTemp(Offset2APP1 + 6)) & Chr(ExifTemp(Offset2APP1 + 7)) _
        <> "Exif" Then
      InspectByteArray = False
    End If

    InspectByteArray = True

  End If

End Function

Private Sub subRtoLTest()
  If Hex(ExifTemp(Offset2TIFF)) = "49" Then
    boRL = True
  Else
    boRL = False
  End If
End Sub

Private Sub GetTags(ByRef Offset As Integer)

  Dim intEntries As Integer
  Dim Upper_IFDDirectory As Integer
  Dim NewDimensions As Integer
  Dim Processed_ExifSubIFD As Boolean
  Dim BytesPerComponent As Integer
  Dim Ix As Integer 'temp working variable
  Dim Jx As Integer 'temp working variable
  Dim Kx As Integer 'temp working variable
  Dim aryBytTmp(-1) As Byte 'temporary byte array, initially empty

  'Debug.Print("****Begin GetDirectoryEntries**** ")
  'Debug.Print("Offset is: " & Offset)

  On Error GoTo suite
  Do
    If boRL Then 'get number of entries - per IFD block?
      intEntries = ExifTemp(Offset + 1) * 256 + ExifTemp(Offset + 0)
    Else 'if not Intel
      intEntries = ExifTemp(Offset + 0) * 256 + ExifTemp(Offset + 1)
    End If

      'Debug.Print("No_of_Entries = >" & intEntries & "<")
    If UBound(aryIFDDir) >= 0 Then
      Upper_IFDDirectory = UBound(aryIFDDir) 'old array size
    Else
      Upper_IFDDirectory = 0
    End If
      'Debug.Print("Upper_IFDDirectory = " & Upper_IFDDirectory)

    NewDimensions = Upper_IFDDirectory + intEntries
    'boost array size to include new entries
    ReDim Preserve aryIFDDir(NewDimensions)

    For Ix = 1 To intEntries 'read, insert new entries

      With aryIFDDir(Upper_IFDDirectory + Ix)

        .TagOffset = 0        'set default

        If boRL Then
          'Debug.Print("R-to-L Minolta/Intel code section entered")
          .Tag_No = BitConverter.ToUInt16(ExifTemp, (Offset + 2) + ((Ix - 1) * 12))

          .Data_Format = BitConverter.ToUInt16(ExifTemp, (Offset + 2) + ((Ix - 1) * 12) + 2)

          .TagCount = BitConverter.ToInt32(ExifTemp, (Offset + 2) + ((Ix - 1) * 12) + 4)

          Select Case .Data_Format

            Case ExifFormatEnum.fmtBYTE, ExifFormatEnum.fmtSBYTE   'types 1, 6: Bytes, Signed-Bytes
              BytesPerComponent = 1
              If .TagCount * BytesPerComponent <= 4 Then
                .Value = "Hex: " & Hex(ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 8)) & " " & _
                  Hex(ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 9)) & " " & _
                  Hex(ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 10)) & " " & _
                  Hex(ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 11))
              Else
                .Offset_To_Value = BitConverter.ToInt32(ExifTemp, (Offset + 2) + ((Ix - 1) * 12) + 8)
                .Value = "Hex: "
                For Jx = 0 To .TagCount - 1
                  .Value = .Value & Hex(ExifTemp(Offset2TIFF + .Offset_To_Value + Jx)) & " "
                Next
              End If

            Case ExifFormatEnum.fmtSTRING ' type 2: ASCII with 00 terminator
              '(don't need to read backwards for ASCII type)
              If .TagCount <= 4 Then
                .Value = ""
                For Jx = 0 To .TagCount - 2   ' -2 to skip trailing hex 00
                  .Value = .Value & Chr(ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 8 + Jx))
                Next
                .TagOffset = (Offset + 2) + ((Ix - 1) * 12) + 8
              Else
                .Offset_To_Value = BitConverter.ToInt32(ExifTemp, (Offset + 2) + ((Ix - 1) * 12) + 8)
                Kx = .TagCount
                If Kx > maxStrLgth + 1 Then Kx = maxStrLgth + 1
                'limit string length to max
                For Jx = 0 To Kx - 2 ' -2 because possible trailing x00
                  .Value = .Value & Chr(ExifTemp(Offset2TIFF + .Offset_To_Value + Jx))
                Next
                .TagOffset = Offset2TIFF + .Offset_To_Value
                If Asc(Chr(ExifTemp(Offset2TIFF + .Offset_To_Value + Kx - 1))) <> 0 Then
                  .Value = .Value & Chr(ExifTemp(Offset2TIFF + .Offset_To_Value + Kx - 1))
                End If
              End If

            Case ExifFormatEnum.fmtUNDEFINED       'type 7: undefined, varies by tag #
              .Value = ""
              If .TagCount <= 4 Then
                For Jx = 0 To .TagCount - 1
                  .Value = .Value & Chr(ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 8 + Jx))
                Next
                .TagOffset = (Offset + 2) + ((Ix - 1) * 12) + 8
                Select Case .Tag_No
                  Case ExifTagEnum.ComponentsConfiguration
                    If InStr(.Value, Chr(2), CompareMethod.Binary) Then
                      .Value = "Y-Cb-Cr"
                    ElseIf InStr(.Value, Chr(4), CompareMethod.Binary) Then
                      .Value = "R-G-B"
                    Else
                      .Value = "Unknown components config"
                    End If
                  Case ExifTagEnum.FileSource
                     If InStr(.Value, Chr(3), CompareMethod.Binary) Then
                      .Value = "DSC: digital still camera"
                    Else
                      .Value = "Unknown file source"
                    End If
                 Case ExifTagEnum.SceneType
                     If InStr(.Value, Chr(1), CompareMethod.Binary) Then
                      .Value = "Directly photographed image"
                    Else
                      .Value = "Unknown scene type"
                    End If
                End Select

              Else
                .Offset_To_Value = BitConverter.ToInt32(ExifTemp, (Offset + 2) + ((Ix - 1) * 12) + 8)
                Kx = .TagCount
                If Kx > maxStrLgth + 1 Then Kx = maxStrLgth + 1 'limit string length to max
                If .Tag_No = ExifTagEnum.UserComment Then    '1st 8 bytes are ASCII000, etc.
                  For Jx = 8 To Kx - 1
                    .Value = .Value & Chr(ExifTemp(Offset2TIFF + .Offset_To_Value + Jx))
                  Next
                  .TagOffset = Offset2TIFF + .Offset_To_Value + 8
                Else
                  For Jx = 0 To Kx - 1
                    .Value = .Value & Chr(ExifTemp(Offset2TIFF + .Offset_To_Value + Jx))
                  Next
                  .TagOffset = Offset2TIFF + .Offset_To_Value
                End If
              End If

            Case ExifFormatEnum.fmtSHORT, ExifFormatEnum.fmtSSHORT 'types 3, 8
              BytesPerComponent = 2
              If .TagCount * BytesPerComponent <= 4 Then
                .Value = CStr(BitConverter.ToUInt16(ExifTemp, ((Offset + 2) + ((Ix - 1) * 12) + 8)))
              Else
                .Offset_To_Value = BitConverter.ToInt32(ExifTemp, (Offset + 2) + ((Ix - 1) * 12) + 8)
                For Jx = 0 To .TagCount - 1
                  .Value = .Value & ExifTemp(Offset2TIFF + .Offset_To_Value + Jx)
                Next
              End If

            Case ExifFormatEnum.fmtLONG, ExifFormatEnum.fmtSLONG 'types 4, 9
              BytesPerComponent = 4
              If .TagCount * BytesPerComponent <= 4 Then
                .Value = CStr(BitConverter.ToInt32(ExifTemp, (Offset + 2) + ((Ix - 1) * 12) + 8))
              Else
                .Offset_To_Value = BitConverter.ToInt32(ExifTemp, (Offset + 2) + ((Ix - 1) * 12) + 8)
                For Jx = 0 To .TagCount - 1
                  .Value = .Value & ExifTemp(Offset2TIFF + .Offset_To_Value + Jx)
                Next
              End If

            Case ExifFormatEnum.fmtRATIONAL, ExifFormatEnum.fmtSRATIONAL 'types 5, 10: un-, signed fraction
              BytesPerComponent = 8

              .Offset_To_Value = BitConverter.ToInt32(ExifTemp, (Offset + 2) + ((Ix - 1) * 12) + 8)
              .Value = BitConverter.ToInt32(ExifTemp, Offset2TIFF + .Offset_To_Value)
              .Value = .Value & "/" & BitConverter.ToInt32(ExifTemp, Offset2TIFF + .Offset_To_Value + 4)

            Case Else       'if unrecognized tag-type number
              .Value = ""

          End Select

        Else 'if not Intel/Minolta; e.g., Nikon
          'Debug.Print("L-to-R Nikon/Non-Intel code section entered")
          .Tag_No = ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 0) * 256 + _
            ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 1)

          .Data_Format = ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 2) * 256 + _
            ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 3)

          .TagCount = fnBA2Nbr((Offset + 2) + ((Ix - 1) * 12) + 4, 4, True)

          Select Case .Data_Format

            Case ExifFormatEnum.fmtBYTE, ExifFormatEnum.fmtSBYTE  'types 1, 6
              If .TagCount <= 4 Then
                .Value = "Hex: " & Hex(ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 8)) & " " & _
                  Hex(ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 9)) & " " & _
                  Hex(ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 10)) & " " & _
                  Hex(ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 11))
              Else
                .Offset_To_Value = fnBA2Nbr((Offset + 2) + ((Ix - 1) * 12) + 8, 4, True)
                .Value = "Hex: "
                For Jx = 0 To .TagCount - 1
                  .Value = .Value & ExifTemp(Offset2TIFF + .Offset_To_Value + Jx)
                Next
              End If

            Case ExifFormatEnum.fmtSTRING        'type 2: ASCII string with 00 at end
              If .TagCount <= 4 Then
                .Value = ""
                For Jx = 0 To .TagCount - 2   ' -2 to skip trailing hex 00
                  .Value = .Value & Chr(ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 8 + Jx))
                Next
                .TagOffset = (Offset + 2) + ((Ix - 1) * 12) + 8

              Else
                .Offset_To_Value = fnBA2Nbr((Offset + 2) + ((Ix - 1) * 12) + 8, 4, True)
                Kx = .TagCount
                If Kx > maxStrLgth + 1 Then Kx = maxStrLgth + 1
                'limit string length to max
                For Jx = 0 To Kx - 2       '-2 because possible trailing x00
                  .Value = .Value & Chr(ExifTemp(Offset2TIFF + .Offset_To_Value + Jx))
                Next
                .TagOffset = Offset2TIFF + .Offset_To_Value
                If Asc(Chr(ExifTemp(Offset2TIFF + .Offset_To_Value + Kx - 1))) <> 0 Then
                  .Value = .Value & Chr(ExifTemp(Offset2TIFF + .Offset_To_Value + Kx - 1))
                End If
              End If

            Case ExifFormatEnum.fmtUNDEFINED      'type 7: undefined format
              .Value = ""
              If .TagCount <= 4 Then
                For Jx = 0 To .TagCount - 1
                  .Value = .Value & Chr(ExifTemp((Offset + 2) + ((Ix - 1) * 12) + 8 + Jx))
                Next
                .TagOffset = (Offset + 2) + ((Ix - 1) * 12) + 8
                Select Case .Tag_No
                  Case ExifTagEnum.ComponentsConfiguration
                    If InStr(.Value, Chr(2), CompareMethod.Binary) Then
                      .Value = "Y-Cb-Cr"
                    ElseIf InStr(.Value, Chr(4), CompareMethod.Binary) Then
                      .Value = "R-G-B"
                    Else
                      .Value = "Unknown components config"
                    End If
                  Case ExifTagEnum.FileSource
                     If InStr(.Value, Chr(3), CompareMethod.Binary) Then
                      .Value = "DSC: digital still camera"
                    Else
                      .Value = "Unknown file source"
                    End If
                 Case ExifTagEnum.SceneType
                     If InStr(.Value, Chr(1), CompareMethod.Binary) Then
                      .Value = "Directly photographed image"
                    Else
                      .Value = "Unknown scene type"
                    End If
                End Select

              Else
                .Offset_To_Value = fnBA2Nbr((Offset + 2) + ((Ix - 1) * 12) + 8, 4, True)
                Kx = .TagCount
                If Kx > maxStrLgth + 1 Then Kx = maxStrLgth + 1 'limit string length to max
                If .Tag_No = ExifTagEnum.UserComment Then    '1st 8 bytes are ASCII000, etc.
                  For Jx = 8 To Kx - 1
                    .Value = .Value & Chr(ExifTemp(Offset2TIFF + .Offset_To_Value + Jx))
                  Next
                  .TagOffset = Offset2TIFF + .Offset_To_Value + 8
                Else
                  For Jx = 0 To Kx - 1
                    .Value = .Value & Chr(ExifTemp(Offset2TIFF + .Offset_To_Value + Jx))
                  Next
                  .TagOffset = Offset2TIFF + .Offset_To_Value
                End If
              End If

            Case ExifFormatEnum.fmtSHORT, ExifFormatEnum.fmtSSHORT 'types 3, 8
              BytesPerComponent = 2
              If .TagCount * BytesPerComponent <= 4 Then
                .Value = CStr(fnBA2Nbr((Offset + 2) + ((Ix - 1) * 12) + 8, 2, True))
              Else
                .Offset_To_Value = fnBA2Nbr((Offset + 2) + ((Ix - 1) * 12) + 8, 4, True)
                For Jx = .TagCount - 1 To 0 Step -1
                  .Value = .Value & ExifTemp(Offset2TIFF + .Offset_To_Value + Jx)
                Next
              End If

            Case ExifFormatEnum.fmtLONG, ExifFormatEnum.fmtSLONG 'types 4, 9
              BytesPerComponent = 4
              If .TagCount * BytesPerComponent <= 4 Then
                .Value = CStr(fnBA2Nbr((Offset + 2) + ((Ix - 1) * 12) + 8, 4, True))
              Else
                .Offset_To_Value = fnBA2Nbr((Offset + 2) + ((Ix - 1) * 12) + 8, 4, True)
                For Jx = 0 To .TagCount - 1
                  .Value = .Value & ExifTemp(Offset2TIFF + .Offset_To_Value + Jx)
                Next
              End If

            Case ExifFormatEnum.fmtRATIONAL, ExifFormatEnum.fmtSRATIONAL 'types 5, 10: un-, signed fraction
              BytesPerComponent = 8
              .Offset_To_Value = fnBA2Nbr((Offset + 2) + ((Ix - 1) * 12) + 8, 4, True)
              .Value = CStr(fnBA2Nbr(Offset2TIFF + .Offset_To_Value + 0, 4, True)) & "/" & _
                CStr(fnBA2Nbr(Offset2TIFF + .Offset_To_Value + 4, 4, True))

            Case Else       'if unrecognized tag-type number
              .Value = ""

          End Select

        End If

        If .Value Is Not Nothing Then
          Kx = InStr(.Value, Chr(0), CompareMethod.Binary)
          If Kx > 0 Then               'truncate string at first x'00' found
            .Value = VB.left(.Value, Kx - 1)
          End If
        End If

        'Debug.Print(Upper_IFDDirectory + Ix & " Tag#: " & .Tag_No & _
        '    "; Format: " & .Data_Format & _
        '    "; Count: " & .TagCount & _
        '    "; TagOffset: " & .TagOffset & _
        '    "; ValueOffset: " & .Offset_To_Value & _
        '    "; Value: " & .Value)

        If .Tag_No = ExifTagEnum.ExifOffset Then
          Offset2ExifSubIFD = CInt(.Value)
          'Debug.Print("Offset_to_ExifSubIFD: " & Offset2ExifSubIFD)
        End If

      End With

    Next Ix 'parse next entry in IFD block

    If boRL Then 'see if there is another IFD block
      If Not Processed_ExifSubIFD Then
        Offset2NextIFD = BitConverter.ToInt32(ExifTemp, Offset + 2 + (intEntries * 12) + 0)
              'Debug.Print("R-to-L Offset2NextIFD: " & Offset2NextIFD)
      Else
        Offset2NextIFD = 0
      End If
    Else 'if not Intel
      If Not Processed_ExifSubIFD Then
        Offset2NextIFD = _
          ExifTemp(Offset + 2 + (intEntries * 12) + 0) * 256 * 256 * 256 + _
          ExifTemp(Offset + 2 + (intEntries * 12) + 1) * 256 * 256 + _
          ExifTemp(Offset + 2 + (intEntries * 12) + 2) * 256 + _
          ExifTemp(Offset + 2 + (intEntries * 12) + 3)
              'Debug.Print("L-to-R Offset2NextIFD: " & Offset2NextIFD)
      Else
        Offset2NextIFD = 0
      End If
    End If

    If Offset2NextIFD = 0 And Processed_ExifSubIFD = False Then
      Offset2NextIFD = Offset2ExifSubIFD
      Processed_ExifSubIFD = True
    End If

    Offset = Offset2TIFF + Offset2NextIFD

  Loop While Offset2NextIFD <> 0 'exit if no next IFD block
Exit Sub
suite:
    MsgBox ex.Message & vbCrLf & ex.ToString & vbCrLf & "File: " & strCurrFile, "Read Exif Tags", MessageBoxButtons.OK, MessageBoxIcon.Error
    Return

End Sub

Private Function fnBA2Nbr( _
  ByVal Offset As Integer, _
  ByVal NumBytes As Integer, _
  ByVal boLR As Boolean) As Integer
'parse num-bytes # of bytes (2 or 4) into an integer value,
' left-to-right (high-order at left, default) or right-to-left

  Dim bytarData(3) As Byte 'byte array structure
  Dim intData As Integer
  Dim Ix As Integer 'index counter
  Dim ArrayOffset As Integer   'to put #s on right end of array

  On Error GoTo suite
    ArrayOffset = 4 - NumBytes   '0 if 4 bytes to parse, 2 if 2

    If boLR = False Then         'if R-to-L: high-order byte at right (high array index)

      For Ix = 0 To NumBytes - 1
        bytarData(Ix) = ExifTemp(Offset + Ix)
      Next Ix

      MsgBox "Potential error; should not be in this code section" & vbCrLf & "File: " & strCurrFile & vbCrLf & "Offset: " & CStr(Offset) & "   # of bytes: " & CStr(NumBytes) & "   L-to-R: " & boLR & vbCrLf & vbCrLf & "Will try to continue", "Parse Exif Data", MessageBoxButtons.OK, MessageBoxIcon.Warning

      'Debug.Print("ParseRtoL: " & Offset & " " & NumBytes & "   " & _
      'bytarData(0) & " " & bytarData(1) & " " & _
      'bytarData(2) & " " & bytarData(3))      '& "  " & CStr(intData))

    Else            ' - if L-to-R: high-order byte at left (low array index)

      For Ix = 0 To NumBytes - 1
        bytarData(NumBytes + ArrayOffset - 1 - Ix) = ExifTemp(Offset + Ix)
      Next Ix

        'Debug.Print("ParseLtoR: " & Offset & " " & NumBytes & "  " & _
        'bytarData(0) & " " & bytarData(1) & " " & _
        'bytarData(2) & " " & bytarData(3))      '& "  " & CStr(intData))

    End If

    If NumBytes = 4 Then
      intData = BitConverter.ToInt32(bytarData, 0)
    ElseIf NumBytes = 2 Then
      intData = CInt(BitConverter.ToUInt16(bytarData, 2))
    Else
      MsgBox "Internal error: can parse only 2 or 4 bytes, not " & CStr(NumBytes), "fnBA2Nbr", MessageBoxButtons.OK, MessageBoxIcon.Warning
      fnBA2Nbr = 0
    End If

    'Debug.Print("BAParse Result: " & CStr(intData))

    fnBA2Nbr = intData    'leave the function

  'Catch ex As OverflowException
  '  Return 0     'leave the function
Exit Function
suite:
    msgboxex Message & vbCrLf & ex.ToString & "File: " & strCurrFile & vbCrLf & "# of bytes: " & NumBytes & "  L-to-R: " & boLR, "Parse Exif Data", MessageBoxButtons.OK, MessageBoxIcon.Error
    fnBA2Nbr = 0

End Function


