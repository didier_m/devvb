Attribute VB_Name = "m_dattouch"
Option Explicit

Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

Dim ByteFormat As Byte

Private Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
   
Private Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
   
Private Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long

Private Declare Function lstrlen Lib "kernel32" Alias "lstrlenW" (ByVal lpString As Long) As Long

Private Declare Function PathMatchSpec Lib "shlwapi" Alias "PathMatchSpecW" (ByVal pszFileParam As Long, ByVal pszSpec As Long) As Long

Public Declare Function SetFileTime Lib "kernel32" (ByVal hFile As Long, lpcreation As FILETIME, lpLecture As FILETIME, lpLastWriteTime As FILETIME) As Long
Public Declare Function GetFileTime Lib "kernel32" (ByVal hFile As Long, lpCreationTime As FILETIME, lpLastAccessTime As FILETIME, lpLastWriteTime As FILETIME) As Long

Private Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, ByVal NoSecurity As Long, ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long

Private Declare Function SetFileCreatedTime Lib "kernel32" Alias "SetFileTime" (ByVal hFile As Long, lpCreationTime As FILETIME, ByVal NullLastAccessTime As Long, ByVal NullLastWriteTime As Long) As Long
Private Declare Function SetFileAccessTime Lib "kernel32" Alias "SetFileTime" (ByVal hFile As Long, ByVal NullCreationTime As Long, lpLastAccessTime As FILETIME, ByVal NullWriteTime As Long) As Long
Private Declare Function SetFileModifiedTime Lib "kernel32" Alias "SetFileTime" (ByVal hFile As Long, ByVal NullCreationTime As Long, ByVal NullLastAccessTime As Long, lpLastWriteTime As FILETIME) As Long
Private Type FILETIME
    dwLowDateTime As Long
    dwHighDateTime As Long
End Type

Private Const GENERIC_READ = &H80000000
Private Const GENERIC_WRITE = &H40000000
Private Const FILE_SHARE_READ = &H1
Private Const FILE_SHARE_WRITE = &H2
Private Const OPEN_EXISTING = 3

Private Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Private Declare Function FileTimeToLocalFileTime Lib "kernel32" (lpFileTime As FILETIME, lpLocalFileTime As FILETIME) As Long
Private Declare Function LocalFileTimeToFileTime Lib "kernel32" (lpLocalFileTime As FILETIME, lpFileTime As FILETIME) As Long
Private Declare Function FileTimeToSystemTime Lib "kernel32" (lpFileTime As FILETIME, lpSystemTime As SYSTEMTIME) As Long
Private Declare Function SystemTimeToFileTime Lib "kernel32" (lpSystemTime As SYSTEMTIME, lpFileTime As FILETIME) As Long
'-> API de temporisation
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Private Declare Function SetFilePointer Lib "kernel32" _
(ByVal hFile As Long, ByVal lDistanceToMove As Long, lpDistanceToMoveHigh As Long, ByVal dwMoveMethod As Long) As Long
'-> API pour gestion des process
Public Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Public Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long

Private fp As FILE_PARAMS
Private stopSearch As Boolean

Public Enum pTypeLigne
    pEntete
    pColumnHeader
    pData
    pSeparateur
End Enum

Public Type WIN32_FIND_DATA
   dwFileAttributes As Long
   ftCreationTime As FILETIME
   ftLastAccessTime As FILETIME
   ftLastWriteTime As FILETIME
   nFileSizeHigh As Long
   nFileSizeLow As Long
   dwReserved0 As Long
   dwReserved1 As Long
   cFileName As String * 260
   cAlternate As String * 14
End Type

Public Type FILE_PARAMS
   bRecurse As Boolean
   bFindOrExclude As Long
   nCount As Long
   nSearched As Long
   sFileNameExt As String
   sFileRoot As String
End Type

Private Declare Function MultiByteToWideChar Lib "kernel32.dll" ( _
    ByVal CodePage As Long, _
    ByVal dwFlags As Long, _
    ByVal lpMultiByteStr As Long, _
    ByVal cbMultiByte As Long, _
    ByVal lpWideCharStr As Long, _
    ByVal cchWideChar As Long _
) As Long

Public Const SEE_MASK_INVOKEIDLIST = &HC

Private Declare Function WideCharToMultiByte Lib "kernel32.dll" ( _
    ByVal CodePage As Long, _
    ByVal dwFlags As Long, _
    ByVal lpWideCharStr As Long, _
    ByVal cchWideChar As Long, _
    ByVal lpMultiByteStr As Long, _
    ByVal cbMultiByte As Long, _
    ByVal lpDefaultChar As Long, _
    ByVal lpUsedDefaultChar As Long _
) As Long

Public Declare Function ShellExecuteEx Lib "shell32" (lpSEI As SHELLEXECUTEINFO) As Long
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal Hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

'-> API pour affichage d'une page de propri�t�s
Public Type SHELLEXECUTEINFO
             cbSize As Long
             fMask As Long
             Hwnd As Long
             lpVerb As String
             lpFile As String
             lpParameters As String
             lpDirectory As String
             nShow As Long
             hInstApp As Long
             ' Optional fields
             lpIDList As Long
             lpClass As String
             hkeyClass As Long
             dwHotKey As Long
             hIcon As Long
             hProcess As Long
   End Type

Public Const CP_ACP        As Long = 0          ' Default ANSI code page.
Public Const CP_UTF8       As Long = 65001      ' UTF8.
Public Const CP_UTF16_LE   As Long = 1200       ' UTF16 - little endian.
Public Const CP_UTF16_BE   As Long = 1201       ' UTF16 - big endian.
Public Const CP_UTF32_LE   As Long = 12000      ' UTF32 - little endian.
Public Const CP_UTF32_BE   As Long = 12001      ' UTF32 - big endian.

'-> Pour gestion des fichiers
Public Const OFS_MAXPATHNAME& = 128
Public Const OF_EXIST& = &H4000
Public Const OF_READ& = &H0

Public Declare Function GetStdHandle Lib "kernel32" _
(ByVal nStdHandle As Long) As Long

Private Declare Function WriteFile Lib "kernel32" _
(ByVal hFile As Long, _
lpBuffer As Any, _
ByVal nNumberOfBytesToWrite As Long, _
lpNumberOfBytesWritten As Long, _
lpOverlapped As Any) As Long

Public Const STD_OUTPUT_HANDLE = -11&

Private Type COORD
        x As Integer
        y As Integer
End Type

Private Type SMALL_RECT
        left As Integer
        top As Integer
        right As Integer
        bottom As Integer
End Type

Public Declare Function OpenFile& Lib "kernel32" (ByVal lpFileName As String, lpReOpenBuff As OFSTRUCT, ByVal wStyle As Long)
Public Declare Function GetFileSize& Lib "kernel32" (ByVal hFile As Long, lpFileSizeHigh As Long)
Private Declare Function ReadFile Lib "kernel32" _
(ByVal hFile As Long, lpBuffer As Any, ByVal nNumberOfBytesToRead As Long, lpNumberOfBytesRead As Long, ByVal lpOverlapped As Any) As Long
'-> Pour Maj d'une fen�tre
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long

Public Type OFSTRUCT
    cBytes As Byte
    fFixedDisk As Byte
    nErrCode As Integer
    Reserved1 As Integer
    Reserved2 As Integer
    szPathName(OFS_MAXPATHNAME) As Byte
End Type

Private Type CONSOLE_SCREEN_BUFFER_INFO
        dwSize As COORD
        dwCursorPosition As COORD
        wAttributes As Integer
        srWindow As SMALL_RECT
        dwMaximumWindowSize As COORD
End Type
Private Declare Function GetConsoleScreenBufferInfo Lib "kernel32" _
(ByVal hConsoleOutput As Long, _
lpConsoleScreenBufferInfo As CONSOLE_SCREEN_BUFFER_INFO) As Long

Private Declare Function SetConsoleTextAttribute Lib "kernel32" _
(ByVal hConsoleOutput As Long, ByVal wAttributes As Long) As Long

Private Const FOREGROUND_BLUE = &H1     '  text color contains blue.
Private Const FOREGROUND_GREEN = &H2     '  text color contains green.
Private Const FOREGROUND_INTENSITY = &H8     '  text color is intensified.
Private Const FOREGROUND_RED = &H4     '  text color contains red.
Private hOutput             As Long
Private Const vbBackslash = "\"
Private Const ALL_FILES = "*.*"
Private Const FILE_BEGIN = 0

'-> Pour gestion des process m�moire
Public Const PROCESS_QUERY_INFORMATION = &H400
Public Const STILL_ACTIVE = &H103

Public fList As String
Public quiet As Boolean
Public keepdate As Boolean
Public recursive As Boolean
Public version As Boolean
Public help As Boolean
Public wdir As String
Public rdir As String
Public edir As String
Public lfile As String
Public Convert As String
Public ismouchard As Boolean
Public isexport As Boolean
Public Mouchard As String
Public cpt As Integer
Public hdlMouchard As Integer

'-->
Public topDiff As Boolean
Public topAddRef As Boolean
Public topAddOri As Boolean
Public topSame As Boolean

Public topRepSame As Boolean
Public topIdentSuppr As Boolean
Public topIdentPrio As Boolean
Public topHorodatage As Boolean
Public topHorodatageFiche As Boolean
Public ident As String
Public repignore As String
Public repautori As String
Public fileignore As String
Public fileautori As String
Public dateMini As String
Public dateMaxi As String

Public cptFileTot As Long
Public cptFileIdem As Long
Public cptFileRef As Long
Public cptFileWork As Long
Public cptFileRefFiltre As Long
Public cptFileWorkFiltre As Long

Public cptFileCopy As Long
Public cptFileListview As Long
Public cptFileNewer As Long
Public cptFileAbsRef As Long
Public cptFileModify As Long

Public cptFileAnachro As Long

Public quit As Boolean

Public colPhoto As clsPhoto
Public Photos As Collection

Public Sub Main()
    Form1.Show
End Sub

Private Function getFileDate(sFile As String) As String
Dim strTemp As String
Dim strTemp2 As String
Dim strTemp3 As String
    strTemp2 = getExif(sFile)
    strTemp2 = Entry(2, strTemp2, "DateTime = ")
    strTemp2 = Entry(1, strTemp2, ";")
    strTemp2 = Mid(strTemp2, 11)
    strTemp = Entry(1, strTemp2, " ")
    strTemp3 = Entry(3, strTemp, ":") & "/" & Entry(2, strTemp, ":") & "/" & Entry(1, strTemp, ":") & " " & Entry(2, strTemp2, " ")
    strTemp2 = Mid(strTemp3, 1, Len(strTemp3) - 1)
    getFileDate = strTemp2
End Function

Public Function CreateFileC(sRoot As String, refDir As String, sFile As String, Optional sFiltreDirectory As String) As String
    '--> cette fonction charge le listview a partir d'un chemin root
 'Dim DSO As Object
 Dim WFD As WIN32_FIND_DATA
 Dim hFile As Long
 Dim sP As String
 Dim refFile As String
 Dim wFile As String
 Dim exportFile As String
 Dim oriFile As String
 Dim strRep As String
 Dim i As Integer
 Dim cpt As Integer
 
 On Error Resume Next
 
 If quit Then
    Exit Function
 End If
 
 With fp
    .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
    .sFileNameExt = sFile                'fichier (* ? autoris�
    .bRecurse = 1                             'True = recherche recursive
    .bFindOrExclude = 1                       '0=inclure, 1=exclure
 End With
 
 'Set DSO = CreateObject("DSOFile.OleDocumentProperties")
 Dim cD As Date
 Dim aD As Date
 Dim mD As Date
 Dim cD2 As Date
 Dim aD2 As Date
 Dim mD2 As Date
 Dim cFileName As String
 
 hFile = FindFirstFile(sRoot & "*.*", WFD)
 If hFile <> -1 Then
    Do
      DoEvents
      cFileName = Trim(TrimNull(WFD.cFileName))
      If Len(cFileName) > 256 Then
        FileCopy cFileName, Mid(cFileName, 50)
        Kill WFD.cFileName
        GoTo suite
      End If
      If quit Then GoTo suite
      'si c'est un repertoire on boucle
       If (WFD.dwFileAttributes And vbDirectory) Then
          If Asc(cFileName) <> CLng(46) Then
              If fp.bRecurse Then
                  strRep = sRoot & cFileName & vbBackslash
                  Form1.Label4.Caption = strRep
                  DoEvents
                  If recursive Then CreateFileC strRep, refDir, sFile
              End If
          End If
       Else
        '-> doit �tre une image
        If MatchSpec(cFileName, "Thumbs.db") Then Kill cFileName
        If MatchSpec(cFileName, "*.jpg") Or MatchSpec(cFileName, "*.jpeg") Or MatchSpec(cFileName, "*.raw") Or MatchSpec(cFileName, "*.avi") Or MatchSpec(cFileName, "*.mov") Or MatchSpec(cFileName, "*.3gp") Or MatchSpec(cFileName, "*.mp4") Then
            If MatchSpec(cFileName, fp.sFileNameExt) Then
                '-> la photo a analyser
                wFile = sRoot & cFileName
                '-> si on doit remettre la date d'origine
                Dim strTemp As String
                Dim strTemp2 As String
                Dim strTemp3 As String
                Dim dDate As Date
                If Form1.Check1.value = 1 Then
                    strTemp2 = getFileDate(wFile)
                    dDate = CDate(strTemp2)
                    SetFileTimes wFile, dDate, dDate, dDate, False
                End If
                'strTemp = CreateMD5HashFile(wFile)
                '-> on regarde si on a pas deja eu la photo pour eviter les doublons
                If Form1.Check2.value = 1 Then
                    'Form1.Label4.Caption = "Hash..." & wFile
                    Set colPhoto = New clsPhoto
                    colPhoto.FileName = wFile
                    colPhoto.FileSize = FileLen(wFile)
                    strTemp = getFileDate(wFile)
                    If strTemp = "" Then strTemp = CreateMD5HashFile(wFile)
                    colPhoto.FileHash = "p" & strTemp & "|" & colPhoto.FileSize
                    colPhoto.FileDate = strTemp2
                    If Not isCol(colPhoto.FileHash) Then
                        If FileSame(Photos(colPhoto.FileHash).FileDest, wFile) Then
                            Kill wFile
                            GoTo suite
                        End If
                    End If
                End If
                '-> les dates du fichier
                GetFileTimes wFile, cD2, aD2, mD2, False
                Dim strDate As String
                strTemp = Entry(1, mD2, " ")
                Dim sYear As String
                sYear = Entry(3, strTemp, "/")
                Dim sMonth As String
                sMonth = Entry(2, strTemp, "/")
                Dim sDay As String
                sDay = Entry(1, strTemp, "/")
                strDate = Replace(rdir, "@year", sYear)
                strDate = Replace(strDate, "@month", sMonth)
                strDate = Replace(strDate, "@day", sDay)
                'strDate = Entry(3, strTemp, "/") & "_" & Entry(2, strTemp, "/") & "_" & Entry(1, strTemp, "/")
                '-> si le repertoire existe pas sur r�f�rence on le cr��
                strRep = strDate
                strTemp = ""
                For i = 2 To NumEntries(strRep, "\")
                    strTemp = strTemp & "\" & Entry(i, strRep, "\")
                    If Dir(strTemp, vbDirectory) = "" Then
                        MkDir (strTemp)
                    End If
                Next
                '-> le fichier correspondant sur reference
                refFile = strRep & TrimNull(cFileName)
                If FileExist(refFile) Then
                  '-> on a trouv� le fichier sur le r�pertoire de destination
                  GetFileTimes refFile, cD, aD, mD, False
                  If (mD <> mD2) Then
                      '->les fichiers sont differents ont regarde si c'est pas un pb de date
                      If Not FileSame(sRoot & TrimNull(cFileName), refFile) Then
                        '-> le contenu est diff�rent on copie avec un indice
                        If NumEntries(wFile, ".") = 2 Then
                            i = 1
                            strTemp = Entry(1, wFile, ".") & "(" & i & ")" & Entry(2, wFile, ".")
                            FileCopy wFile, strTemp
                            DoEvents
                            Kill wFile
                        End If
                      Else
                        If Trim(UCase(wFile)) <> Trim(UCase(refFile)) Then
                            '-> on bute le fichier en doublon
                            Kill wFile
                        End If
                      End If
                  Else
                    '-> les fichiers sont identiques ont fait rien
                    If Trim(UCase(wFile)) <> Trim(UCase(refFile)) Then
                        '-> on bute le fichier en doublon
                        Kill wFile
                    End If
                  End If
                Else
                    '-> on a pas trouv� le fichier on le met dans le repertoire de destination
                    FileCopy wFile, TrimNull(refFile)
                    If Form1.Check2.value = 1 Then
                        colPhoto.FileDest = TrimNull(refFile)
                        Photos.add colPhoto, colPhoto.FileHash
                    End If
                    DoEvents
                    Kill wFile
                End If
            End If
suite:
        End If
       End If
    Form1.Label4.Caption = wFile
    DoEvents
    Loop While FindNextFile(hFile, WFD)
 End If
suite2:
Call FindClose(hFile)

DoEvents
End Function

Public Sub DeleteEmptyFolders(ByVal strFolderPath As String)
   'Dim fsoSubFolders As Folders
   Dim fsoFolder As Object
   Dim fsoSubFolder As Object
   Dim m_fsoObject As Object
   Dim strPaths()
   Dim lngFolder As Long
   Dim lngSubFolder As Long
      
   DoEvents
   
   Set m_fsoObject = New FileSystemObject
   If Not m_fsoObject.FolderExists(strFolderPath) Then Exit Sub
   
   Set fsoFolder = m_fsoObject.GetFolder(strFolderPath)
   
   On Error Resume Next
   
   'Has sub-folders
   If fsoFolder.SubFolders.Count > 0 Then
        lngFolder = 1
        ReDim strPaths(1 To fsoFolder.SubFolders.Count)
        'Get each sub-folders path and add to an array
        For Each fsoSubFolder In fsoFolder.SubFolders
            strPaths(lngFolder) = fsoSubFolder.Path
            lngFolder = lngFolder + 1
        Next fsoSubFolder
        
        lngSubFolder = 1
        'Recursively call the function for each sub-folder
        Do While lngSubFolder < lngFolder
           Call DeleteEmptyFolders(strPaths(lngSubFolder))
           lngSubFolder = lngSubFolder + 1
        Loop
    End If
   
    'No sub-folders or files
    If fsoFolder.Files.Count = 0 And fsoFolder.SubFolders.Count = 0 Then
        fsoFolder.Delete
    End If
End Sub

Private Function isCol(ByVal hash As String) As Boolean
Dim cPhoto As clsPhoto
On Error GoTo GestError
isCol = False
Set cPhoto = Photos(hash)
Exit Function
GestError:
isCol = True

End Function

Private Function GetFileMD5(ByVal FileName As String) As String
    Dim MD5 As New clsMD5, FF As Integer, Buff() As Byte
    Const BuffSize As Long = 2 ^ 16 ' (64 KBytes)

'    On Error GoTo ErrExit
    FF = FreeFile
    Open FileName For Binary Access Read As FF
        MD5.MD5Init

        Do Until Loc(FF) >= LOF(FF)
            If Loc(FF) + BuffSize > LOF(FF) Then
                ReDim Buff(LOF(FF) - Loc(FF) - 1)
            Else
                ReDim Buff(BuffSize - 1)
            End If
            DoEvents
            Get FF, , Buff
            MD5.MD5Update UBound(Buff) + 1, Buff
        Loop

        MD5.MD5Final
        GetFileMD5 = MD5.GetValues
    Close FF

    Exit Function
ErrExit:
    Err.Clear
    GetFileMD5 = ""
End Function

' Purpose:  Take a string whose bytes are in the byte array <the_abytCPString>, with code page <the_nCodePage>, convert to a VB string.
Private Function FromCPString(ByRef the_abytCPString() As Byte, ByVal the_nCodePage As Long) As String

    Dim sOutput                     As String
    Dim nValueLen                   As Long
    Dim nOutputCharLen              As Long

    ' If the code page says this is already compatible with the VB string, then just copy it into the string. No messing.
    If the_nCodePage = CP_UTF16_LE Then
        FromCPString = the_abytCPString()
    Else

        ' Cache the input length.
        nValueLen = UBound(the_abytCPString) - LBound(the_abytCPString) + 1

        ' See how big the output buffer will be.
        nOutputCharLen = MultiByteToWideChar(the_nCodePage, 0&, VarPtr(the_abytCPString(LBound(the_abytCPString))), nValueLen, 0&, 0&)

        ' Resize output byte array to the size of the UTF-8 string.
        sOutput = Space$(nOutputCharLen)

        ' Make this API call again, this time giving a pointer to the output byte array.
        MultiByteToWideChar the_nCodePage, 0&, VarPtr(the_abytCPString(LBound(the_abytCPString))), nValueLen, StrPtr(sOutput), nOutputCharLen

        ' Return the array.
        FromCPString = sOutput

    End If

End Function

Private Function QualifyPath(sPath As String) As String
   If right$(sPath, 1) <> vbBackslash Then
      QualifyPath = sPath & vbBackslash
   Else
      QualifyPath = sPath
   End If
End Function

Private Function TrimNull(startstr As String) As String
   TrimNull = left$(startstr, lstrlen(StrPtr(startstr)))
End Function

Private Function MatchSpec(sFile As String, sSpec As String) As Boolean
   MatchSpec = PathMatchSpec(StrPtr(sFile), StrPtr(sSpec)) = fp.bFindOrExclude
End Function

Private Function GetFileTimes(ByVal file_name As String, _
    ByRef creation_date As Date, ByRef access_date As Date, _
    ByRef modified_date As Date, ByVal local_time As _
    Boolean) As Boolean
Dim file_handle As Long
Dim creation_filetime As FILETIME
Dim access_filetime As FILETIME
Dim modified_filetime As FILETIME
Dim file_time As FILETIME

    ' Assume something will fail.
    GetFileTimes = True

    ' Open the file.
    file_handle = CreateFile(file_name, GENERIC_READ, _
        FILE_SHARE_READ Or FILE_SHARE_WRITE, _
        0&, OPEN_EXISTING, 0&, 0&)
    If file_handle = 0 Then Exit Function

    ' Get the times.
    If GetFileTime(file_handle, creation_filetime, _
        access_filetime, modified_filetime) = 0 Then
        CloseHandle file_handle
        Exit Function
    End If

    ' Close the file.
    If CloseHandle(file_handle) = 0 Then Exit Function

    ' See if we should convert to the local
    ' file system time.
    If local_time Then
        ' Convert to local file system time.
        FileTimeToLocalFileTime creation_filetime, file_time
        creation_filetime = file_time

        FileTimeToLocalFileTime access_filetime, file_time
        access_filetime = file_time

        FileTimeToLocalFileTime modified_filetime, file_time
        modified_filetime = file_time
    End If

    ' Convert into dates.
    creation_date = FileTimeToDate(creation_filetime)
    access_date = FileTimeToDate(access_filetime)
    modified_date = FileTimeToDate(modified_filetime)

    GetFileTimes = False
End Function

Public Function SetFileTimes(ByVal file_name As String, _
    ByVal creation_date As Date, ByVal access_date As Date, _
    ByVal modified_date As Date, ByVal local_times As _
    Boolean) As Boolean
Dim file_handle As Long
Dim creation_filetime As FILETIME
Dim access_filetime As FILETIME
Dim modified_filetime As FILETIME
Dim file_time As FILETIME

    ' Assume something will fail.
    SetFileTimes = True

    ' Convert the dates into FILETIMEs.
    creation_filetime = DateToFileTime(creation_date)
    access_filetime = DateToFileTime(access_date)
    modified_filetime = DateToFileTime(modified_date)

    ' Convert the file times into system file times.
    If local_times Then
        LocalFileTimeToFileTime creation_filetime, file_time
        creation_filetime = file_time

        LocalFileTimeToFileTime access_filetime, file_time
        access_filetime = file_time

        LocalFileTimeToFileTime modified_filetime, file_time
        modified_filetime = file_time
    End If

    ' Open the file.
    file_handle = CreateFile(file_name, GENERIC_WRITE, _
        FILE_SHARE_READ Or FILE_SHARE_WRITE, _
        0&, OPEN_EXISTING, 0&, 0&)
    If file_handle = 0 Then Exit Function

'creation_date = FileTimeToDate(creation_filetime)

    ' Set the times.
    If SetFileTime(file_handle, creation_filetime, _
        access_filetime, modified_filetime) = 0 Then
        CloseHandle file_handle
        Exit Function
    End If

    ' Close the file.
    If CloseHandle(file_handle) = 0 Then Exit Function

    SetFileTimes = False
End Function

Private Function FileTimeToDate(file_time As FILETIME) As Date
Dim system_time As SYSTEMTIME

    ' Convert the FILETIME into a SYSTEMTIME.
    FileTimeToSystemTime file_time, system_time

    ' Convert the SYSTEMTIME into a Date.
    FileTimeToDate = SystemTimeToDate(system_time)
End Function

Private Function DateToFileTime(ByVal the_date As Date) As FILETIME
Dim system_time As SYSTEMTIME
Dim file_time As FILETIME

    ' Convert the Date into a SYSTEMTIME.
    system_time = DateToSystemTime(the_date)

    ' Convert the SYSTEMTIME into a FILETIME.
    SystemTimeToFileTime system_time, file_time
    DateToFileTime = file_time
End Function

Private Function SystemTimeToDate(system_time As SYSTEMTIME) As Date
    With system_time
        SystemTimeToDate = CDate( _
            Format$(.wDay) & "/" & _
            Format$(.wMonth) & "/" & _
            Format$(.wYear) & " " & _
            Format$(.wHour) & ":" & _
            Format$(.wMinute, "00") & ":" & _
            Format$(.wSecond, "00"))
    End With
End Function
' Convert a Date into a SYSTEMTIME.
Private Function DateToSystemTime(ByVal the_date As Date) As SYSTEMTIME
    With DateToSystemTime
        .wYear = Year(the_date)
        .wMonth = Month(the_date)
        .wDay = Day(the_date)
        .wHour = Hour(the_date)
        .wMinute = Minute(the_date)
        .wSecond = Second(the_date)
    End With
End Function

Public Function FileExist(Fichier As String) As Boolean

On Error GoTo GestError

If Dir$(Fichier) <> "" Then FileExist = True
Exit Function

GestError:
    FileExist = False

End Function

Private Function FileSame(ByVal file1 As String, ByVal file2 As String) As Boolean
'--> cette fonction compare 2 fichiers
FileSame = False
'-> on commence par comparer la taille
If FileLen(file1) <> FileLen(file2) Then
    Exit Function
End If
If OpenFileAPI(file1) = OpenFileAPI(file2) Then FileSame = True

End Function

Public Function OpenFileAPI(ByRef FileName As String) As String
    Dim hOrgFile As Long
    Dim nSize As Long
    Dim Ret As Long
    
    On Error GoTo errorhandler
    
    'R�cup�re un Handle pour manipuler le flux m�moire du fichier
    hOrgFile = CreateFile(FileName, _
                            GENERIC_READ, _
                            FILE_SHARE_READ Or FILE_SHARE_WRITE, _
                            ByVal 0&, OPEN_EXISTING, 0, 0)

    'Taille du fichier
    nSize = GetFileSize(hOrgFile, 0)

    DoEvents

    'Initialise le pointeur sur le fichier
    SetFilePointer hOrgFile, 0, 0, FILE_BEGIN

    OpenFileAPI = Space(nSize)

    'Charge le contenu du fichier dans la variable OpenFileAPI
    ReadFile hOrgFile, ByVal OpenFileAPI, nSize, Ret, ByVal 0&
     
    DoEvents
     
    'Ferme le fichier
    CloseHandle hOrgFile
    
    Exit Function
errorhandler:
    OpenFileAPI = ""
    CloseHandle hOrgFile
End Function

Public Sub Trace(sText As String)
End Sub

Public Function IsPidRunning(lngPid As Long) As Boolean

'---> Cette proc�dure v�rifie si un process sp�cifi� ( lngPid ) est encore actif

Dim hdlProcess As Long
Dim ExitCode As Long

'-> Ne rien faire si process � 0
If lngPid = 0 Then Exit Function
'-> R�cup�rer le handle du process affect� � ce pide
hdlProcess = OpenProcess(PROCESS_QUERY_INFORMATION, False, lngPid)
'-> V�rifier si le PID est toujours valide
GetExitCodeProcess hdlProcess, ExitCode
If ExitCode = STILL_ACTIVE Then IsPidRunning = True

End Function

Private Function H2B(InHex As String) As String ' Conv Hex to Bytes
    Dim i As Long
    
    For i = 1 To Len(InHex) Step 2
        H2B = H2B & Chr$(CLng("&H" & Mid$(InHex, i, 2)))
    Next i
End Function

Private Function B2D(InBytes As String) As Double ' Conv. Bytes to Decimal - Could be > 4 Billion
    Dim i As Long
    Dim tmp As String
    
    For i = 1 To Len(InBytes)
        tmp = tmp & Hex(Format$(Asc(Mid$(InBytes, i, 1)), "00"))
    Next i
    B2D = "&H" & tmp
End Function

Private Function Rev(InBytes As String) As String ' Reverse bytes
    If ByteFormat = 1 Then Exit Function ' Not needed for Motorola format
    
    Dim i As Long
    Dim tmp As String
    
    For i = Len(InBytes) To 1 Step -1
        tmp = tmp & Mid$(InBytes, i, 1)
    Next i
    Rev = tmp
End Function

Private Function GetTagName(TagNum As String) As String
    Select Case TagNum
    Case H2B("010E"): GetTagName = "ImageDescription"
    Case H2B("010F"): GetTagName = "Make"
    Case H2B("0110"): GetTagName = "Model"
    Case H2B("0112"): GetTagName = "Orientation"
    Case H2B("011A"): GetTagName = "XResolution"
    Case H2B("011B"): GetTagName = "YResolution"
    Case H2B("0128"): GetTagName = "ResolutionUnit"
    Case H2B("0131"): GetTagName = "Software"
    Case H2B("0132"): GetTagName = "DateTime"
    Case H2B("013E"): GetTagName = "WhitePoint"
    Case H2B("013F"): GetTagName = "PrimaryChromaticities"
    Case H2B("0211"): GetTagName = "YCbCrCoefficients"
    Case H2B("0213"): GetTagName = "YCbCrPositioning"
    Case H2B("0214"): GetTagName = "ReferenceBlackWhite"
    Case H2B("8298"): GetTagName = "Copyright"
    Case H2B("8769"): GetTagName = "ExifOffset"
    
    Case H2B("829A"): GetTagName = "ExposureTime"
    Case H2B("829D"): GetTagName = "FNumber"
    Case H2B("8822"): GetTagName = "ExposureProgram"
    Case H2B("8827"): GetTagName = "ISOSpeedRatings"
    Case H2B("9000"): GetTagName = "ExifVersion"
    Case H2B("9003"): GetTagName = "DateTimeOriginal"
    Case H2B("9004"): GetTagName = "DateTimeDigitized"
    Case H2B("9101"): GetTagName = "ComponentConfiguration"
    Case H2B("9102"): GetTagName = "CompressedBitsPerPixel"
    Case H2B("9201"): GetTagName = "ShutterSpeedValue"
    Case H2B("9202"): GetTagName = "ApertureValue"
    Case H2B("9203"): GetTagName = "BrightnessValue"
    Case H2B("9204"): GetTagName = "ExposureBiasValue"
    Case H2B("9205"): GetTagName = "MaxApertureValue"
    Case H2B("9206"): GetTagName = "SubjectDistance"
    Case H2B("9207"): GetTagName = "MeteringMode"
    Case H2B("9208"): GetTagName = "LightSource"
    Case H2B("9209"): GetTagName = "Flash"
    Case H2B("920A"): GetTagName = "FocalLength"
    Case H2B("927C"): GetTagName = "MakerNote" ': Stop
    Case H2B("9286"): GetTagName = "UserComment"
    Case H2B("A000"): GetTagName = "FlashPixVersion"
    Case H2B("A001"): GetTagName = "ColorSpace"
    Case H2B("A002"): GetTagName = "ExifImageWidth"
    Case H2B("A003"): GetTagName = "ExifImageHeight"
    Case H2B("A004"): GetTagName = "RelatedSoundFile"
    Case H2B("A005"): GetTagName = "ExifInteroperabilityOffset"
    Case H2B("A20E"): GetTagName = "FocalPlaneXResolution"
    Case H2B("A20F"): GetTagName = "FocalPlaneYResolution"
    Case H2B("A210"): GetTagName = "FocalPlaneResolutionUnit"
    Case H2B("A217"): GetTagName = "SensingMethod"
    Case H2B("A300"): GetTagName = "FileSource"
    Case H2B("A301"): GetTagName = "SceneType"
    
    Case H2B("0100"): GetTagName = "ImageWidth"
    Case H2B("0101"): GetTagName = "ImageLength"
    Case H2B("0102"): GetTagName = "BitsPerSample"
    Case H2B("0103"): GetTagName = "Compression"
    Case H2B("0106"): GetTagName = "PhotometricInterpretation"
    Case H2B("0111"): GetTagName = "StripOffsets"
    Case H2B("0115"): GetTagName = "SamplesPerPixel"
    Case H2B("0116"): GetTagName = "RowsPerStrip"
    Case H2B("0117"): GetTagName = "StripByteConunts"
    Case H2B("011A"): GetTagName = "XResolution"
    Case H2B("011B"): GetTagName = "YResolution"
    Case H2B("011C"): GetTagName = "PlanarConfiguration"
    Case H2B("0128"): GetTagName = "ResolutionUnit"
    Case H2B("0201"): GetTagName = "JpegIFOffset"
    Case H2B("0202"): GetTagName = "JpegIFByteCount"
    Case H2B("0211"): GetTagName = "YCbCrCoefficients"
    Case H2B("0212"): GetTagName = "YCbCrSubSampling"
    Case H2B("0213"): GetTagName = "YCbCrPositioning"
    Case H2B("0214"): GetTagName = "ReferenceBlackWhite"
    
    Case H2B("00FE"): GetTagName = "NewSubfileType"
    Case H2B("00FF"): GetTagName = "SubfileType"
    Case H2B("012D"): GetTagName = "TransferFunction"
    Case H2B("013B"): GetTagName = "Artist"
    Case H2B("013D"): GetTagName = "Predictor"
    Case H2B("0142"): GetTagName = "TileWidth"
    Case H2B("0143"): GetTagName = "TileLength"
    Case H2B("0144"): GetTagName = "TileOffsets"
    Case H2B("0145"): GetTagName = "TileByteCounts"
    Case H2B("014A"): GetTagName = "SubIFDs"
    Case H2B("015B"): GetTagName = "JPEGTables"
    Case H2B("828D"): GetTagName = "CFARepeatPatternDim"
    Case H2B("828E"): GetTagName = "CFAPattern"
    Case H2B("828F"): GetTagName = "BatteryLevel"
    Case H2B("83BB"): GetTagName = "IPTC/NAA"
    Case H2B("8773"): GetTagName = "InterColorProfile"
    Case H2B("8824"): GetTagName = "SpectralSensitivity"
    Case H2B("8825"): GetTagName = "GPSInfo"
    Case H2B("8828"): GetTagName = "OECF"
    Case H2B("8829"): GetTagName = "Interlace"
    Case H2B("882A"): GetTagName = "TimeZoneOffset"
    Case H2B("882B"): GetTagName = "SelfTimerMode"
    Case H2B("920B"): GetTagName = "FlashEnergy"
    Case H2B("920C"): GetTagName = "SpatialFrequencyResponse"
    Case H2B("920D"): GetTagName = "Noise"
    Case H2B("9211"): GetTagName = "ImageNumber"
    Case H2B("9212"): GetTagName = "SecurityClassification"
    Case H2B("9213"): GetTagName = "ImageHistory"
    Case H2B("9214"): GetTagName = "SubjectLocation"
    Case H2B("9215"): GetTagName = "ExposureIndex"
    Case H2B("9216"): GetTagName = "TIFF/EPStandardID"
    Case H2B("9290"): GetTagName = "SubSecTime"
    Case H2B("9291"): GetTagName = "SubSecTimeOriginal"
    Case H2B("9292"): GetTagName = "SubSecTimeDigitized"
    Case H2B("A20B"): GetTagName = "FlashEnergy"
    Case H2B("A20C"): GetTagName = "SpatialFrequencyResponse"
    Case H2B("A214"): GetTagName = "SubjectLocation"
    Case H2B("A215"): GetTagName = "ExposureIndex"
    Case H2B("A302"): GetTagName = "CFAPattern"
    
    Case H2B("0200"): GetTagName = "SpecialMode"
    Case H2B("0201"): GetTagName = "JpegQual"
    Case H2B("0202"): GetTagName = "Macro"
    Case H2B("0203"): GetTagName = "Unknown"
    Case H2B("0204"): GetTagName = "DigiZoom"
    Case H2B("0205"): GetTagName = "Unknown"
    Case H2B("0206"): GetTagName = "Unknown"
    Case H2B("0207"): GetTagName = "SoftwareRelease"
    Case H2B("0208"): GetTagName = "PictInfo"
    Case H2B("0209"): GetTagName = "CameraID"
    Case H2B("0F00"): GetTagName = "DataDump"
    'Case H2B(""): GetTagName = ""
    Case Else: GetTagName = "Unknown"
    End Select
End Function

Private Function TypeOfTag(InDec As Long) As Byte
    'Format Info
    'Value              1               2               3               4               5                   6
    'Format             unsigned byte   ascii Strings   unsigned Short  unsigned long   unsigned rational   signed byte
    'Bytes/component    1               1               2               4               8                   1
    
    'Value              7               8               9               10              11                  12
    'Format             undefined       signed Short    signed long     signed rational single float        double float
    'Bytes/component    1               2               4               8               4                   8
    
    Select Case InDec
    Case 1:  TypeOfTag = 1
    Case 2:  TypeOfTag = 1
    Case 3:  TypeOfTag = 2
    Case 4:  TypeOfTag = 4
    Case 5:  TypeOfTag = 8
    Case 6:  TypeOfTag = 1
    Case 7:  TypeOfTag = 1
    Case 8:  TypeOfTag = 2
    Case 9:  TypeOfTag = 4
    Case 10: TypeOfTag = 8
    Case 11: TypeOfTag = 4
    Case 12: TypeOfTag = 8
    End Select
End Function

Private Function ConvertData2Format(DataFormat As Long, InBytes As String) As String
    ' Read function aboves details
    ' Double check for Motorola format esp. CopyMemory
    Dim tmpInt As Integer
    Dim tmpLng As Long
    Dim tmpSng As Single
    Dim tmpDbl As Double
    Dim tmpVal As Double
    
    Select Case DataFormat
    Case 1, 3, 4: ConvertData2Format = B2D(InBytes)
    Case 2, 7: ConvertData2Format = InBytes
    Case 5 ' Kinda Unsigned Fraction
        ConvertData2Format = CDbl(B2D(Mid$(InBytes, 1, 4))) / CDbl(B2D(Mid$(InBytes, 5, 4)))
    Case 6
        tmpVal = B2D(InBytes)
        If tmpVal > 127 Then ConvertData2Format = -(tmpVal - 127) Else Convert = tmpVal
    Case 8
        'tmpVal = B2D(InBytes)
        'If tmpVal > 32767 Then ConvertData2Format = -(tmpVal - 32767) Else ConvertData2Format = tmpVal
        CopyMemory tmpInt, InBytes, 2
        ConvertData2Format = tmpInt
    Case 9
        CopyMemory tmpLng, InBytes, 4
        ConvertData2Format = tmpLng
    Case 10 ' Kinda Signed Fraction (Lens Apeture?)
        CopyMemory tmpLng, Mid$(InBytes, 1, 4), 4
        ConvertData2Format = tmpLng
        CopyMemory tmpLng, Mid$(InBytes, 5, 4), 4
        ConvertData2Format = ConvertData2Format / tmpLng
    Case 11
        CopyMemory tmpSng, InBytes, 4
        ConvertData2Format = tmpSng
    Case 12
        CopyMemory tmpDbl, InBytes, 8
        Convert = tmpDbl
    End Select
End Function

Public Function getExif(strFile As String) As String
    Dim Cmds As String
    Dim AppDataLen As Double
    Dim ExifDataChunk As String
    Dim FID As Double
    Dim NoOfDirEntries As Double
    
    On Error Resume Next
    Open strFile For Binary Access Read As #1
    Cmds = Input(2, 1)
    If Cmds <> H2B("FFD8") Then getExif = getExif & "Not a JPEG" & ""
    
    Cmds = Input(2, 1) 'FF E0 to FF EF = 'Application Marker'
    If Cmds <> H2B("FFE1") Then getExif = getExif & "Contains no 'Application Marker'"
    
    AppDataLen = B2D(Input(2, 1))   ' Motorola byte
    getExif = getExif & ";Application Data Length = " & AppDataLen  ' (-6 from below)
    
    Cmds = Input(6, 1)
    If Cmds <> "Exif" & H2B("0000") Then getExif = getExif & "Not Exif data"
    
    ExifDataChunk = Input(AppDataLen, 1)
    
    Select Case Mid$(ExifDataChunk, 1, 2)
    Case H2B("4949"): getExif = getExif & ";Intel Header Format": ByteFormat = 0  ' Reverse bytes
    Case H2B("4D4D"): getExif = getExif & ";Motarola Header Format - Might have probs": ByteFormat = 1
    Case Else: getExif = getExif & ";Unknown/Error Header Format"
    End Select
    
    
    FID = B2D(Rev(Mid$(ExifDataChunk, 5, 4)))  'Image File Directory Offset = 8
    getExif = getExif & ";Image File Dir. Offset = " & FID  ' (-8)
    
    NoOfDirEntries = B2D(Rev(Mid$(ExifDataChunk, 9, 2)))
    getExif = getExif & ";No Of Dir Entries = " & NoOfDirEntries
    If NoOfDirEntries > 200 Then NoOfDirEntries = 200
    Dim DataFormat As Long
    Dim tmpStr As String
    Dim NxtExifChunk As Long
    Dim DirEntryInfo As String
    Dim TagName As String
    Dim i As Double
    Dim SizeMultiplier As Double
    Dim LenOfTagData As Long
    
    For i = 0 To NoOfDirEntries - 1
        DirEntryInfo = Mid$(ExifDataChunk, (i * 12) + 11, 12)
        DoEvents
        TagName = GetTagName(Rev(Mid$(DirEntryInfo, 1, 2)))
        
        DataFormat = B2D(Rev(Mid$(DirEntryInfo, 3, 2))) ' Byte, Single, Long...
        SizeMultiplier = B2D(Rev(Mid$(DirEntryInfo, 5, 4)))
        LenOfTagData = CLng(TypeOfTag(DataFormat)) * SizeMultiplier
        
        If TagName = "ExifOffset" Then NxtExifChunk = ConvertData2Format(DataFormat, Rev(right$(Mid$(DirEntryInfo, 9, 4), LenOfTagData)))
        If LenOfTagData <= 4 Then ' No Offset
            getExif = getExif & ";" & TagName & " = " & ConvertData2Format(DataFormat, Rev(right$(Mid$(DirEntryInfo, 9, 4), LenOfTagData)))
        Else ' Offset required
            tmpStr = Mid$(ExifDataChunk, B2D(Rev(Mid$(DirEntryInfo, 9, 4))) + 1, LenOfTagData)
            getExif = getExif & ";" & TagName & " = " & ConvertData2Format(DataFormat, tmpStr)
        End If
        If i > 200 Then Exit For
    Next i
    
    
    Dim NxtIFDO As Double
    NxtIFDO = B2D(Rev(Mid$(ExifDataChunk, (i * 12) + 11, 4)))
    getExif = getExif & ";Next IFD Offset? = " & NxtIFDO  ' Seems incorrect, so its saved above
    If NxtIFDO = 0 Then getExif = getExif & "No more IFD entires?"
    
    
    
    getExif = getExif & ""
    
    NoOfDirEntries = B2D(Rev(Mid$(ExifDataChunk, NxtExifChunk + 1, 2)))
    getExif = getExif & ";No Of Dir Entries = " & NoOfDirEntries
    
    For i = 0 To NoOfDirEntries - 1
        DirEntryInfo = Mid$(ExifDataChunk, (i * 12) + NxtExifChunk + 11 + 4, 12)
        
        TagName = GetTagName(Rev(Mid$(DirEntryInfo, 1, 2)))
        
        DataFormat = B2D(Rev(Mid$(DirEntryInfo, 3, 2))) ' Byte, Single, Long...
        SizeMultiplier = B2D(Rev(Mid$(DirEntryInfo, 5, 4)))
        LenOfTagData = CLng(TypeOfTag(DataFormat)) * SizeMultiplier
        
        If LenOfTagData <= 4 Then ' No Offset
            getExif = getExif & ";" & TagName & " = " & ConvertData2Format(DataFormat, Rev(right$(Mid$(DirEntryInfo, 9, 4), LenOfTagData)))
        Else ' Offset required
            tmpStr = Mid$(ExifDataChunk, B2D(Rev(Mid$(DirEntryInfo, 9, 4))) + 1, LenOfTagData)
            getExif = getExif & ";" & TagName & " = " & ConvertData2Format(DataFormat, tmpStr)
        End If
    Next i
    
    NxtIFDO = B2D(Rev(Mid$(ExifDataChunk, NxtExifChunk + (i * 12), 4)))
    getExif = getExif & ";Next IFD Offset? = " & NxtIFDO  ' Seems incorrect, so its saved above
    If NxtIFDO = 0 Then getExif = getExif & "No more IFD entires?"
    
    
    
    
    
    
    getExif = getExif & ""
    Close #1
End Function

