VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "dc_tds"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'***********************************************************
'* Description de la structure TDS                         *
'***********************************************************

'-> Définition des propriétés
Public TDSfield As String
Public TDSposA As Integer 'position debut dans le TDS
Public TDSposB As Integer 'position fin dans le tds
Public TDSvalue As String 'valeur en dur du tds
Public TDSalign As String 'permet de tronquer des valeurs
