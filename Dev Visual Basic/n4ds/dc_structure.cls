VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "dc_structure"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'***********************************************************
'* Description de la structure de l'affichage des donn�es  *
'***********************************************************
Public Enum pType
    pNumeric
    pstring
    pstringUcase
    pDate
End Enum

Public Enum pStructStatut
    pObligatoire
    pFacultatif
    pConditionnel
End Enum

'-> D�finition des propri�t�s
Public FieldType As String '-> D�finit le type de la donn�e
Public FieldValue As String '-> D�finit la cl� pour le champ
Public FieldDescription As String '-> D�scription du champ
Public FieldNiveau '-> D�finit la profondeur du champ
Public Statut As pStructStatut 'le champ est il obligatoire
Public TypeVal As pType 'type du champ
Public Length As Integer 'longueur du champ
Public VarLength As Boolean
Public Index As Integer
Public Condition As String
Public AutoriValues As String 'liste des valeurs autoris�es
Public ListLibel As String 'liste des libell�s pour le treeview
Public Format As Integer
Public Multiple As Boolean
Public Function IsStructure(strKey As String) As Boolean
'-> cette fonction permet de savoir si un �l�ment appartient � la structure
Dim aStructure As dc_structure

On Error GoTo GestError
Set aStructure = Structures.Item(strKey)
IsStructure = True
Set aStructure = Nothing
Exit Function

GestError:
IsStructure = False
End Function
