Attribute VB_Name = "dm_n4ds"
Option Explicit

Public Enum pStatut
    pModify
    pVisu
    pCreate
    pSaisie
    pSave
End Enum

Public Enum pPosition
    pAvant
    pApres
    pPremierEnfant
    pDernierEnfant
    pParent
End Enum

Public FileName As String
Public StrError As String
Public Structures As Collection
Public StructuresTDS As Collection
Public Types As Collection
Public Fields As Collection
Public aStructure As dc_structure
Public aStructureTDS As dc_tds
Public aType As dc_type
Public curStatut As pStatut
Public FileN4ds As String
Public StrCopyKey As String
Public appTurbo As String
Public lastMaquette As String
Public N4dsExportini
Public SaveInTDS As Boolean

Public Declare Function PostMessage Lib "user32.dll" Alias "PostMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long

Private hdlFile As Integer
Private TempFileName As String
Private Ligne As String

Private Const vbBackslash = "\"
Private Const ALL_FILES = "*.*"

Private Type FILETIME
   dwLowDateTime As Long
   dwHighDateTime As Long
End Type

Private Type WIN32_FIND_DATA
   dwFileAttributes As Long
   ftCreationTime As FILETIME
   ftLastAccessTime As FILETIME
   ftLastWriteTime As FILETIME
   nFileSizeHigh As Long
   nFileSizeLow As Long
   dwReserved0 As Long
   dwReserved1 As Long
   cFileName As String * 260
   cAlternate As String * 14
End Type

Private Type FILE_PARAMS
   bRecurse As Boolean
   bFindOrExclude As Long
   nCount As Long
   nSearched As Long
   sFileNameExt As String
   sFileRoot As String
End Type

Private Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Long) As Long
   
Private Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
   
Private Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long

Private Declare Function lstrlen Lib "kernel32" Alias "lstrlenW" (ByVal lpString As Long) As Long

Private Declare Function PathMatchSpec Lib "shlwapi" Alias "PathMatchSpecW" (ByVal pszFileParam As Long, ByVal pszSpec As Long) As Long

Private fp As FILE_PARAMS
Private stopSearch As Boolean

Sub Main()

'-> on initialise le message d'erreur
StrError = "Une erreur est survenue lors du chargement"
'-> on ouvre la feuille de chargement du N4ds
df_N4ds.init

End Sub

Public Function LoadN4ds(aFilePath As String) As Boolean
Dim hdlFic As Integer
Dim Ligne As String
Dim afield As dc_field
Dim aFieldEmpty As dc_field
Dim aStructure As dc_structure
Dim fileLength As Single 'pour le timer
Dim linelength As Single 'pour le timer
Dim tempTimer As Integer '-> pour le timer
Dim strValue As String
Dim Niveau As Variant
Dim lastNiveau As Integer
Dim lastIndex As Integer
Dim i As Integer
Dim iRubrique As Single
Dim iEnreg As Single
Dim topMessage As Boolean

'On Error GoTo GestError:

fileLength = FileLen(aFilePath)

'-> on affiche le nom du fichier pour info
df_N4ds.Label1.Caption = aFilePath

'-> Ouvrir le fichier binaire source
hdlFic = FreeFile
Open aFilePath For Input As #hdlFic
Line Input #hdlFic, Ligne
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)

'-> on teste la premiere chaine pour v�rifier que l'on est bien sur un fichier N4ds
If Mid(Ligne, 1, 1) <> "S" Then GoTo GestError
    
'-> on recharge le fichier
Close #hdlFic
Open aFilePath For Input As #hdlFic
    
'-> on charge maintenant le fichier de structure
If Not LoadN4dsStructure Then GoTo GestError

'-> on affiche le timer
df_Drloading.Show

'on initialise les champs
Set Fields = New Collection

'-> on se charge les enregistrements du fichier � l'aide de la structure que l'on vient de creer
Do
    '-> Lecture de la ligne
    Line Input #hdlFic, Ligne
    If Ligne = "" Then GoTo Suite
    '-> ce doevents sert a rafraichir le timer
    tempTimer = tempTimer + 1
    linelength = linelength + Len(Ligne) + 2
    If tempTimer = 1000 Then
        tempTimer = 0
        DoEvents
        df_Drloading.Label1.Caption = "Chargement en cours..." & CInt(linelength / fileLength * 100) & "%"
    End If
    Set afield = New dc_field
    '-> la valeur du champ est apres la virgule mais sans les ''
    afield.Value = Uncote(Entry(2, Ligne, ","))
    afield.Statut = pNormal
    strValue = Entry(1, Ligne, ",")
    'on cr�e la structure � blanc
    Set aStructure = New dc_structure
    '-> on g�re ici le cas de l'abscence de la structure dans le fichier de d�finition
    If Not aStructure.IsStructure(strValue) Then
        aStructure.FieldValue = strValue
        '-> on reccupere le type associ�
        For Each aType In Types
            If Entry(1, aType.KeyWord, ".") = Entry(1, aStructure.FieldValue, ".") Then
                aStructure.FieldType = aType.KeyWord
                aStructure.FieldNiveau = aType.Niveau
                DisplayMessage "D�finition absente du fichier structure : " & strValue, dsmCritical, dsmOkOnly, "Fichier de structure � mettre � jour"
                topMessage = True
                afield.Statut = pDelete
                Exit For
            End If
        Next
        Structures.Add aStructure, aStructure.FieldValue
    End If
    
    '-> on pointe sur la structure pour les definitions
    Set aStructure = Structures(strValue)
    
    '-> on regarde si on est sur un champ que l'on doit afficher
    If aStructure.FieldType = aStructure.FieldValue Then afield.IsType = True
    
    '-> on prepare la cle de l'enregistrement qui definit sa position dans le treeview
    If lastNiveau = aStructure.FieldNiveau Then
        '-> si on est sur un nouveau type on incremente le compteur du niveau sinon c'est le niveau 7
        If afield.IsType Then
            Niveau(aStructure.FieldNiveau) = Niveau(aStructure.FieldNiveau) + 1
            Niveau(7) = 0
        Else
            '-> on reste au m�me niveau c'est le compteur de niveau 7 qui est utilis�
            Niveau(7) = Niveau(7) + 1
        End If
    ElseIf lastNiveau < aStructure.FieldNiveau Then
        'on descent d'un niveau
        Niveau(aStructure.FieldNiveau) = Niveau(aStructure.FieldNiveau) + 1
        Niveau(7) = 0
    ElseIf lastNiveau > aStructure.FieldNiveau Then
        Niveau(aStructure.FieldNiveau) = Niveau(aStructure.FieldNiveau) + 1
        'les niveaux descendant sont mis a zero
        For i = aStructure.FieldNiveau + 1 To 7
            Niveau(i) = 0
        Next
    End If
    
    afield.Niveau = aStructure.FieldNiveau
    afield.Struct = aStructure.FieldValue
        
    '-> on definit la cl� de l'enregistrement
    afield.KeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
    '-> on ajoute l'objet que l'on vient de cr�er
    Fields.Add afield, afield.KeyWord
    lastNiveau = aStructure.FieldNiveau
    lastIndex = aStructure.Index
Suite:
Loop While Not EOF(hdlFic)

'-> on affiche le treeview en pointant sur la base
df_N4ds.TreeView1.Nodes.Add , , "|0|0|0|0|0|0|0", "Fichier N4ds", 2
'-> on se creer un field pour l'initialisation
Set afield = New dc_field
afield.KeyWord = "|0|0|0|0|0|0|0"
afield.Niveau = 0
afield.Struct = ""
Fields.Add afield, afield.KeyWord

df_Drloading.Label1.Caption = "Chargement de l'ecran..."
DoEvents
LoadTreeview ("|0|0|0|0|0|0|0")

'-> on charge les entetes du dealgrid
df_N4ds.DealGrid1.SetCellText 0, 0, "Rubrique"
df_N4ds.DealGrid1.SetCellText 1, 0, "Libell�"
df_N4ds.DealGrid1.SetCellText 2, 0, "Valeur"

'-> par defaut on est en visu
curStatut = pVisu

'-> on sauvegarde les compteurs pour v�rification
iRubrique = Fields("|2|0|0|0|0|0|0").Value
iEnreg = Fields("|2|1|0|0|0|0|0").Value
'-> on remet a jour les compteurs
CtrlFieldsCompteur

'-> v�rification des compteurs
If iRubrique <> Fields("|2|0|0|0|0|0|0").Value Or iEnreg <> Fields("|2|1|0|0|0|0|0").Value Then
    If Not topMessage Then DisplayMessage "Le nombre d'enregistrement ne correspond pas � celui enregistr�." & Chr(13) & "Le fichier est peut �tre endommag�.", dsmCritical, dsmOkOnly, "Ouverture de fichier"
    Fields("|2|1|0|0|0|0|0").Statut = pModif
End If

'-> V�rifier que l'on trouve le fichier de structure
N4dsExportini = searchExe("N4dsExport.ini")

'-> on masque le timer
Unload df_Drloading

'-> on ferme le fichier
Close #hdlFic

'-> on affiche la feuille
df_N4ds.Show

'-> on vide les objets
'Set Structures = Nothing
'Set Types = Nothing
Exit Function
GestError:

DisplayMessage StrError, dsmCritical, dsmOkOnly, "Erreur sur l'application"
'-> on ferme le fichier
Close #hdlFic
End
End Function

Public Sub LoadTreeview(strKeyWord As String)
 '--> cette proc�dure permet d'afficher le treeview par portion sinon c'est trop long
Dim aNode As Node
Dim afield As dc_field
Dim curKeyWord As String
Dim tempKeyWord As String
Dim curNiveau As Integer
Dim TopLibel As Integer 'sert de compteur pour les libell�s dans le treeview
Dim Niveau As Variant
Dim strTemp As Variant
Dim strLibel As String
Dim i As Integer
Dim intImage As Integer

On Error Resume Next

Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split(strKeyWord, "|")
For i = 1 To 7
    Niveau(i) = CSng(strTemp(i))
Next

curNiveau = Fields(strKeyWord).Niveau
Niveau(curNiveau + 1) = Niveau(curNiveau + 1) + 1
curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)

Do While Fields(1).IsField(curKeyWord)
    '-> on recherche tous les enregistrement du niveau inf�rieur sur la branche
    Do While Fields(1).IsField(curKeyWord)
        '-> on regarde si on est sur un type
        If Fields(curKeyWord).IsType Then
            '-> on rajoute le libell�
            If Not isNode(curKeyWord) Then
                'strLibel = Fields(curKeyword).Value & " " & Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 1).Value & " " & Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 2).Value
                '-> on lance une fonction qui r�cup�re le label selon ou on est
                strLibel = Mid(Fields(curKeyWord).Struct, 1, 3) & " - " & GetLabel(Fields(curKeyWord).KeyWord, Niveau)
                '-> on met une image selon le niveau
                intImage = GetImage(Fields(curKeyWord).Struct, curNiveau)
                '-> on ajoute
                Set aNode = df_N4ds.TreeView1.Nodes.Add(strKeyWord, 4, curKeyWord, strLibel, intImage)
                '-> on charge les nodes enfants
                LoadTreeview (curKeyWord)
            End If
        End If
        Niveau(7) = Niveau(7) + 1
        curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
    Loop
    Niveau(curNiveau + 1) = Niveau(curNiveau + 1) + 1
    Niveau(7) = 0
    curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
Loop

End Sub

Private Function GetLabel(strKey As String, ByVal Niveau As Variant) As String
'-> cette fonction permet de ramener le libell� pour le treeview selon le niveau d'ou l'on vient
Dim strValue As String
Dim i As Integer

On Error Resume Next
If Structures(Fields(strKey).Struct).ListLibel <> "" Then
    For i = 1 To NumEntries(Structures(Fields(strKey).Struct).ListLibel, "|")
        If Not IsNumeric(Entry(i, Structures(Fields(strKey).Struct).ListLibel, "|")) Then
            strValue = Entry(i, Structures(Fields(strKey).Struct).ListLibel, "|")
        Else
            strValue = Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + Entry(i, Structures(Fields(strKey).Struct).ListLibel, "|")).Value
            Select Case Structures(Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + Entry(i, Structures(Fields(strKey).Struct).ListLibel, "|")).Struct).TypeVal
                Case pNumeric
                    '-> on touche � rien
                Case pDate
                    strValue = Format(Mid(strValue, 1, 2) & "/" & Mid(strValue, 3, 2), "dd mmmm")
                Case pstring, pstringUcase
                    '-> on touche � rien
            End Select
        End If
        If GetLabel = "" Then
            GetLabel = strValue
        Else
            GetLabel = GetLabel & " - " & strValue
        End If
    Next
Else
    Select Case Fields(strKey).Niveau
        Case 1
            Select Case Entry(1, Fields(strKey).Struct, ".")
                Case "S90"
                    GetLabel = Structures(Fields(strKey).Struct).FieldDescription & " : " & Fields(strKey).Value
                Case Else
                    GetLabel = Fields(strKey).Value & " - " & Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 2).Value
            End Select
        Case 2
            Select Case Entry(1, Fields(strKey).Struct, ".")
                Case "S90"
                    GetLabel = "Sous rubriques : " & Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)).Value
                Case Else
                    GetLabel = Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 1).Value
                    GetLabel = GetLabel & " - " & Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 17).Value
                    GetLabel = GetLabel & " - " & Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 18).Value
                End Select
        Case 3
            GetLabel = Fields(strKey).Value & " " & Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 3).Value & " " & Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 2).Value
        Case 4
            Select Case Entry(1, Fields(strKey).Struct, ".")
                Case "S41"
                    strValue = Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)).Value
                    GetLabel = Format(Mid(strValue, 1, 2) & "/" & Mid(strValue, 3, 2), "dd mmmm")
                    strValue = Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 2).Value
                    GetLabel = GetLabel & " au " & Format(Mid(strValue, 1, 2) & "/" & Mid(strValue, 3, 2), "dd mmmm")
                Case "S46"
                    strValue = Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 1).Value
                    GetLabel = Format(Mid(strValue, 1, 2) & "/" & Mid(strValue, 3, 2), "dd mmmm")
                    strValue = Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 2).Value
                    GetLabel = GetLabel & " au " & Format(Mid(strValue, 1, 2) & "/" & Mid(strValue, 3, 2), "dd mmmm")
                Case Else
                    GetLabel = Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 1).Value
                    GetLabel = GetLabel & " - " & Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 17).Value
                    GetLabel = GetLabel & " - " & Fields("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7) + 18).Value
            End Select
        Case 5
        
        Case 6
        
    End Select
End If

End Function

Private Function isNode(strKey As String) As Boolean
'--> cette fonction indique si le node existe dans le treeview
Dim aNode As Node

On Error GoTo GestError
Set aNode = df_N4ds.TreeView1.Nodes(strKey)
Set aNode = Nothing
isNode = True
Exit Function

GestError:
isNode = False
End Function

Private Function Uncote(strValue As String) As String
'--> cette fonction va permettre de sortir les ''
Uncote = Mid(strValue, 2, Len(strValue) - 2)
End Function

Private Function LoadN4dsStructure() As Boolean
'--> cette fonction permet de charger le fichier de structure en lisant un fichier de
'    param�trage contenant les informations : N4dsStruct.ini
Dim curType As String
Dim curNiveau As Integer

'On Error GoTo GestError

'-> on initialise les collections
Set Structures = New Collection
Set Types = New Collection

'-> V�rifier que l'on trouve le fichier de structure
If Dir$(searchExe("N4dsStruct.ini")) = "" Then
    StrError = "Fichier N4dsStruct.ini introuvable."
    Exit Function
End If

'-> Ouvrir le fichier ASCII de r�ponse et charger la matrice des lignes
hdlFile = FreeFile
Open searchExe("N4dsStruct.ini") For Input As #hdlFile

'-> On parcours les lignes du fichier de param�trage
Do While Not EOF(hdlFile)
    '-> on initialise les objet
    Set aStructure = New dc_structure
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> On passe les ligne � blanc ou les lignes de commentaire commencant par #
    If Trim(Ligne) <> "" And Mid(Ligne, 1, 1) <> "#" Then
        '-> Ajouter la d�finition de la structure dans l'objet selon que l'on soit sur un type ou un enregistrement
        If Mid(Ligne, 1, 1) = "T" Then
            '-> on est sur un type
            Set aType = New dc_type
            aType.KeyWord = Entry(2, Ligne, ";")
            curType = aType.KeyWord
            aType.Value = Entry(3, Ligne, ";")
            aType.Niveau = Entry(9, Ligne, ";")
            curNiveau = aType.Niveau
            Types.Add aType, aType.KeyWord
        End If
        '-> on est sur la structure d'un enregistrement
        aStructure.FieldType = curType
        aStructure.FieldValue = Entry(2, Ligne, ";")
        aStructure.FieldDescription = Entry(3, Ligne, ";")
        aStructure.FieldNiveau = curNiveau
        Select Case Entry(4, Ligne, ";")
            Case "O"
                aStructure.Statut = pObligatoire
            Case "F"
                aStructure.Statut = pFacultatif
            Case "C"
                aStructure.Statut = pConditionnel
        End Select
        
        aStructure.Length = Val(Entry(3, Entry(6, Ligne, ";"), "."))
        If NumEntries(Entry(6, Ligne, ";"), ".") = 3 Then aStructure.VarLength = True
        Select Case Entry(5, Ligne, ";")
            Case "N"
                aStructure.TypeVal = pNumeric
            Case "X"
                aStructure.TypeVal = pstring
            Case "X>"
                aStructure.TypeVal = pstringUcase
            Case "D"
                aStructure.TypeVal = pDate
        End Select
        aStructure.Condition = Entry(7, Ligne, ";")
        aStructure.AutoriValues = Entry(8, Ligne, ";")
        aStructure.ListLibel = Entry(10, Ligne, ";")
        If aStructure.TypeVal = pNumeric Then aStructure.Format = Val(Entry(11, Ligne, ";"))
        If Entry(12, Ligne, ";") = "O" Then
            aStructure.Multiple = True
        Else
            aStructure.Multiple = False
        End If
        aStructure.Index = Structures.Count + 1
        Structures.Add aStructure, aStructure.FieldValue
    End If 'Ne pas traiter les lignes <> "" et de commentaire
Loop

Close #hdlFile
'-> valeur de succes
LoadN4dsStructure = True
Exit Function
GestError:
StrError = "Erreur lors de la lecture du fichier de param�trage : " & Ligne
LoadN4dsStructure = False

End Function

Public Sub LoadDealGrid(strKeyWord As String)
'--> cette fonction va permettre de charger le dealgrid
Dim Niveau As Variant
Dim strTemp As Variant
Dim curNiveau As Integer
Dim curKeyWord As String
Dim tempKeyWord As String
Dim i As Integer
Dim curLigne As Integer 'sert a se positionner sur la ligne
Dim posLigne As Integer
Dim aNode As Node

'-> on effectue un test pour savoir si on est pas deja sur cette section

'-> ne pas remplir pour le premier noeud
If strKeyWord = "|0|0|0|0|0|0|0" Then Exit Sub

'-> on va parcourir les differents noeud du treeview
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split(strKeyWord, "|")
For i = 1 To 7
    Niveau(i) = CSng(strTemp(i))
Next
Niveau(7) = 0
tempKeyWord = ""
For i = 1 To 7
    tempKeyWord = tempKeyWord & "|" & Niveau(i)
Next

For Each aNode In df_N4ds.TreeView1.Nodes
    aNode.BackColor = vbWhite
Next
Set aNode = df_N4ds.TreeView1.Nodes(tempKeyWord)
aNode.BackColor = vbYellow

'-> on se met la cle dans un vecteur pour pouvoir travailler dessus
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split(aNode.Key, "|")
For i = 1 To 7
    Niveau(i) = CSng(strTemp(i))
Next
Niveau(7) = 0
tempKeyWord = ""
For i = 1 To 5
    tempKeyWord = tempKeyWord & "|" & Niveau(i)
Next
curKeyWord = tempKeyWord & "|0|" & Niveau(7)
posLigne = 1

'-> on vide le dealgrid
df_N4ds.DealGrid1.ClearRow (False)

'-> on charge le dealgrid en commencant par les informations de plus bas niveau
Do While Fields(1).IsField(curKeyWord)
    '-> on recherche tous les enregistrement sur ce niveau (niveau 7 qui est d�di�)
    Do While Fields(1).IsField(curKeyWord)
        '-> on regarde si la ligne existe
        If Fields(curKeyWord).Statut = pNormal Or Fields(curKeyWord).Statut = pCrea Or Fields(curKeyWord).Statut = pCours Or Fields(curKeyWord).Statut = pModif Then
            '-> on rajoute la ligne
            df_N4ds.DealGrid1.AddRow True, posLigne
            '-> on ecrit les infos sur la ligne
            df_N4ds.DealGrid1.SetCellText 0, posLigne, Structures(Fields(curKeyWord).Struct).FieldValue
            df_N4ds.DealGrid1.SetColAlignement 1, 2
            df_N4ds.DealGrid1.SetCellText 1, posLigne, Structures(Fields(curKeyWord).Struct).FieldDescription
            df_N4ds.DealGrid1.SetColAlignement 2, 2
            df_N4ds.DealGrid1.SetCellText 2, posLigne, Fields(curKeyWord).Value
            '-> on se met a gauche la cle de l'enregistrement
            df_N4ds.DealGrid1.SetCellText 3, posLigne, Fields(curKeyWord).KeyWord
            '-> on est dans le cas d'un s�parateur
            'If Structures(Fields(curKeyWord).Struct).FieldDescription = "S�parateur" Then
            '    df_N4ds.DealGrid1.SetRowHeight df_N4ds.DealGrid1.Row, 2
            'End If
            posLigne = posLigne + 1
        End If
        '-> on regarde si on a pas des enreg crees a ce niveau (niveau 6 d�di�)
        If Fields(1).IsField(tempKeyWord & "|" & (Niveau(6) + 1) & "|" & Niveau(7)) Then
            Niveau(6) = Niveau(6) + 1
            curKeyWord = tempKeyWord & "|" & Niveau(6) & "|" & Niveau(7)
            If curKeyWord = strKeyWord Then
                curLigne = df_N4ds.DealGrid1.Rows - 1
            End If
        Else
            Niveau(6) = 0
            Niveau(7) = Niveau(7) + 1
            curKeyWord = tempKeyWord & "|0|" & Niveau(7)
        End If
    Loop
    '-> on descent d'un niveau en utilisant le keyword de base
    '-> on va parcourir les differents noeud du treeview
    Set aNode = aNode.Parent
    '-> on se met la cle dans un vecteur pour pouvoir travailler dessus
    Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
    '-> on se charge le vecteur a partir de la cl�
    strTemp = Split(aNode.Key, "|")
    For i = 1 To 7
        Niveau(i) = CSng(strTemp(i))
    Next
    Niveau(7) = 0
    tempKeyWord = ""
    For i = 1 To 5
        tempKeyWord = tempKeyWord & "|" & Niveau(i)
    Next
    curKeyWord = tempKeyWord & "|0|" & Niveau(7)
    posLigne = 1
    '-> quitter sur le dernier noeud
    If curKeyWord = "|0|0|0|0|0|0|0" Then Exit Do
Loop
            
For curLigne = 1 To df_N4ds.DealGrid1.Rows - 1
    If df_N4ds.DealGrid1.GetCellText(3, curLigne) = strKeyWord Then
        Exit For
    End If
Next

'-> on se positionne sur la ligne (l'astuce c'est de simuler une saisie pour la rendre visible)
df_N4ds.DealGrid1.SelectRow (curLigne)
df_N4ds.DealGrid1.InitSaisieCell True, FlexSaisieString, ""
df_N4ds.DealGrid1.SetSaisieCell 2, curLigne
SendKeys ("{ESC}")
DoEvents
End Sub

Public Function TextSearchGet() As String

If Fields Is Nothing Then Exit Function

If Fields.Count = 0 Then Exit Function

'-> on recupere la chaine a rechercher
DisplayMessage "Saisissez le texte � rechercher (code, libell�, ou valeur", dsmQuestion, dsmOkCancel, "Recherche de texte", True, InputValue & ""
TextSearchGet = InputValue

End Function

Public Function TextSearch(strText As String, Optional strKeyWord As String) As Boolean
'--> cette procedure permet de rechercher du texte dans les donn�es
'    � partir d'une position (strKeyWord) et de se positionner dessus
'    pour cela on se positionne sur l'entr�e du noeud correspondante
Dim afield As dc_field
Dim aNode As Node
Dim topSearch As Boolean 'sert de marqueur pour savoir quand commencer a chercher
Dim i As Integer

On Error GoTo GestError

'-> si rien de specifier on recherche depuis le debut
If strKeyWord = "" Then topSearch = True

For Each afield In Fields
    '-> si on doit faire la recherche
    If topSearch Then
        If InStr(1, afield.Value, strText, vbTextCompare) Or InStr(1, afield.Struct, strText, vbTextCompare) Or InStr(1, Structures(afield.Struct).FieldDescription, strText, vbTextCompare) Then
            '-> on a trouv� qque chose se positionner dessus a partir du dernier noeud
            '-> on trouve le dernier noeud
            If afield.Statut <> pDelete Then
                strKeyWord = ""
                For i = 2 To 7
                    strKeyWord = strKeyWord & "|" & Entry(i, afield.KeyWord, "|")
                Next
                strKeyWord = strKeyWord & "|0"
                Set aNode = df_N4ds.TreeView1.Nodes(strKeyWord)
                '-> on se positionne sur le treeview
                df_N4ds.TreeView1.SelectedItem = aNode
                aNode.EnsureVisible
                LoadDealGrid (afield.KeyWord)
                '-> quitter la boucle
                GoTo Suite
            End If
        End If
   End If
    '-> on initialise le marqueur
    If afield.KeyWord = strKeyWord Then topSearch = True
   
Next

GestError:
'-> si on est ici c'est que l'on a rien trouv�
DisplayMessage "Fin du document atteinte", dsmInterro, dsmOkOnly, "Recherche de donn�es"

Suite:
End Function

Public Function CtrlFormatZone(ValueSaisie As String, strStruct As String) As Boolean
'--> cette fonction teste la validit� de la saisie en fonction de la d�finition
Dim i As Integer

'-> si la zone est facultative est vide c'est ok quitter
If (Structures(strStruct).Statut = pFacultatif Or Structures(strStruct).Statut = pConditionnel) And ValueSaisie = "" Then
    CtrlFormatZone = True
    Exit Function
End If

'-> selon le type de la donn�e
Select Case Structures(strStruct).TypeVal
    Case pstring, pstringUcase
        '-> on teste si la zone est obligatoire
        If Structures(strStruct).Statut = pObligatoire And ValueSaisie = "" Then
            DisplayMessage "Cette zone doit �tre renseign�e.", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        '-> mettre en majuscule si le type le demande
        If Structures(strStruct).TypeVal = pstringUcase Then ValueSaisie = UCase(ValueSaisie)
        '-> on test la longueur
        If Len(ValueSaisie) > Structures(strStruct).Length Then
            DisplayMessage "La longueur de saisie est limit�e � " & Structures(strStruct).Length & " caract�re(s).", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        '-> la longueur doit elle etre fixe
        If Not Structures(strStruct).VarLength And Len(ValueSaisie) <> Structures(strStruct).Length Then
            DisplayMessage "La longueur de saisie doit �tre de " & Structures(strStruct).Length & " caract�re(s).", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
    Case pDate
        '-> on teste si la zone est obligatoire
        If Structures(strStruct).Statut = pObligatoire And ValueSaisie = "" Then
            DisplayMessage "Cette zone doit �tre renseign�e.", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        '-> la longueur doit elle etre fixe
        If Not Structures(strStruct).VarLength And Len(ValueSaisie) <> Structures(strStruct).Length Then
            DisplayMessage "La date doit �tre sous la forme 'jjmmaaaa'.", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        '-> on regarde si la zone saisie est bien une date
        If InStr(1, "|01|02|03|04|05|06|07|08|09|10|11|12|99", Mid(ValueSaisie, 3, 2)) = 0 Then
            DisplayMessage "Cette zone doit �tre une date valide.", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        If InStr(1, "|01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|99", Mid(ValueSaisie, 1, 2)) = 0 Then
            DisplayMessage "Cette zone doit �tre une date valide.", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        If Not (Val(Mid(ValueSaisie, 5, 4)) > 1900 And Val(Mid(ValueSaisie, 5, 4)) < 2100 And Val(Mid(ValueSaisie, 5, 4)) <> 9999) Then
            DisplayMessage "Cette zone doit �tre une date valide.", dsmWarning, dsmOkOnly, "Erreur de saisie"
            GoTo GestError
        End If
        '-> un dernier test pour les fins de mois et ann�e bix..
        If InStr(1, ValueSaisie, "99") = 0 Then
            If Not IsDate(Mid(ValueSaisie, 1, 2) & "/" & Mid(ValueSaisie, 3, 2) & "/" & Mid(ValueSaisie, 5, 4)) Then
                DisplayMessage "Cette zone doit �tre une date valide.", dsmWarning, dsmOkOnly, "Erreur de saisie"
                GoTo GestError
            End If
        End If
    Case pNumeric
        For i = 1 To Len(ValueSaisie)
            If InStr(1, "0123456789.", Mid(ValueSaisie, i, 1)) = 0 Then
                DisplayMessage "Cette zone doit �tre num�rique.", dsmWarning, dsmOkOnly, "Erreur de saisie"
                GoTo GestError
            End If
            If Not IsNumeric(Replace(ValueSaisie, ".", ",")) And Not IsNumeric(Replace(ValueSaisie, ",", ".")) Then
                DisplayMessage "Cette zone doit �tre num�rique.", dsmWarning, dsmOkOnly, "Erreur de saisie"
                GoTo GestError
            End If
        Next
End Select

CtrlFormatZone = True

GestError:

End Function

Public Function EnregsCreate(strKeyWord As String, strType As String) As String
'--> cette procedure va permettre de creer un enregistrement � blanc
'    on va utiliser le niveau 5 d�di� pour ajouter les enregistrements
'    strkeyword contient la valeur du noeud de base a partir d'ou on pointe
'    retourne la valeur du noeud que l'on a cree

Dim aStructure As dc_structure
Dim Niveau As Variant
Dim strTemp As Variant
Dim i As Integer
Dim curNiveau As Integer
Dim curKeyWord As String
Dim lastKeyWord As String
Dim saveKeyWord As String
Dim lastType As String
Dim afield As dc_field
Dim aNode As Node

'-> on pointe sur le niveau sur lequel on veut faire l'insertion
curNiveau = Structures(strType).FieldNiveau
'-> on doit trouver la prochaine valeur libre pour le noeud concern�
'-> on se met la cle dans un vecteur pour pouvoir travailler dessus
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split(strKeyWord, "|")

'-> on se charge le noeud precedent du noeud sur lequel on pointe
For i = 1 To curNiveau - 1
    If CSng(strTemp(i)) = 0 Then
        Niveau(i) = CSng(strTemp(i)) '1
    Else
        Niveau(i) = CSng(strTemp(i))
    End If
Next
'-> si le noeud precedent a ete cree par definition son sous noeud aussi
Niveau(5) = CSng(strTemp(5))
lastType = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & 0 'strKeyWord

'-> on se charge maintenant le premier noeud hypothetique de notre niveau
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
For i = 1 To curNiveau
    Niveau(i) = CSng(strTemp(i))
Next
Niveau(5) = CSng(strTemp(5))
'-> on cree le premier noeud du niveau sur lequel on veut faire l'insertion
curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)

'-> et on recherche le premier noeud (keyword ) de libre sur la meme profondeur en ordonnant les types
Do While Fields(1).IsField(curKeyWord)
    If Fields(curKeyWord).IsType Then 'en theorie on est tjrs sur un type
        If Fields(curKeyWord).Statut <> pDelete And Structures(Fields(curKeyWord).Struct).Index <= Structures(strType).Index Then lastType = curKeyWord
    End If
    '-> si on a depass� l'index de structure (ceci pour ordonner les types)
    If Structures(Fields(curKeyWord).Struct).Index > Structures(strType).Index Then
        '-> le dernier etait le bon
        If Niveau(curNiveau) <> 0 Then Niveau(curNiveau) = Niveau(curNiveau) - 1
        curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
        If Fields(curKeyWord).IsType Then
            If Fields(curKeyWord).Statut <> pDelete Then lastType = curKeyWord
        End If
        '-> on passe les eventuelles creations existantes (niveau 5) de ce niveau
        Do While Fields(1).IsField(curKeyWord)
            '-> on sauvegarde si on a une entree de noeud
            If Fields(curKeyWord).IsType Then
                If Fields(curKeyWord).Statut <> pDelete Then lastType = curKeyWord
            End If
            'on incremente le compteur des creations
            Niveau(5) = Niveau(5) + 1
            lastKeyWord = curKeyWord
            curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
        Loop
        '-> on a trouv� ou s'arreter avec le futur keyword et on quitte la boucle
        Exit Do
    End If
    Niveau(curNiveau) = Niveau(curNiveau) + 1
    lastKeyWord = curKeyWord
    curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
Loop

'-> on verifie comment inserer l'enreg soit c'est le premier enreg sur le niveau ou pas
If Fields(1).IsField(lastType) Then
    '-> on ins�re l'enregistrement dans le treeview
    If Fields(lastType).Niveau = curNiveau Then
        df_N4ds.TreeView1.Nodes.Add lastType, 2, curKeyWord, Mid(strType, 1, 3) & " - En cours...", GetImage(strType, curNiveau - 1)
        lastKeyWord = lastType
    Else
        'c'est le premier en child
        'on verifie qu'il n'y a pas de child
        If df_N4ds.TreeView1.Nodes(lastType).Children = 0 Then
            df_N4ds.TreeView1.Nodes.Add lastType, 4, curKeyWord, Mid(strType, 1, 3) & " - En cours...", GetImage(strType, curNiveau - 1)
        Else
            'on pointe sur le premier enfant
            Set aNode = df_N4ds.TreeView1.Nodes(lastType).Child
            df_N4ds.TreeView1.Nodes.Add aNode.Key, 0, curKeyWord, Mid(strType, 1, 3) & " - En cours...", GetImage(strType, curNiveau - 1)
        End If
    End If
    Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
    '-> on se charge le vecteur a partir de la cl�
    strTemp = Split(lastKeyWord, "|")
    '-> on se recree le vecteur
    For i = 1 To 7
        Niveau(i) = CSng(strTemp(i)) '1
    Next
    Do While Fields(1).IsField(lastKeyWord)
        Niveau(7) = Niveau(7) + 1
        lastKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
    Loop
    lastKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & (Niveau(7) - 1)
Else
    '-> c'est le premier noeud du niveau
    Niveau(curNiveau) = Niveau(curNiveau) - 1
    lastType = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
    '-> bon maintenant on determine le dernier champ de ce niveau pour savoir ou inserer
    lastKeyWord = lastType
    Do While Fields(1).IsField(lastKeyWord)
        Niveau(7) = Niveau(7) + 1
        lastKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
    Loop
    lastKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & (Niveau(7) - 1)
    df_N4ds.TreeView1.Nodes.Add lastType, 4, curKeyWord, Mid(strType, 1, 3) & " - En cours...", GetImage(strType, curNiveau - 1)
End If

'-> on retourne le noeud que l'on a cree
EnregsCreate = curKeyWord
saveKeyWord = curKeyWord

'-> on se cree l'enregistrement � blanc � blanc
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split(curKeyWord, "|")
'-> on se recree le vecteur
For i = 1 To 7
    Niveau(i) = CSng(strTemp(i)) '1
Next

For Each aStructure In Structures
    '-> on v�rifie que l'on se trouve bien sur le type
    If aStructure.FieldType = strType Then
        '-> on se cr�� l'enregistrement
        Set afield = New dc_field
        afield.Struct = aStructure.FieldValue
        If aStructure.FieldType = aStructure.FieldValue Then
            afield.IsType = True
        End If
        afield.KeyWord = curKeyWord
        afield.Niveau = aStructure.FieldNiveau
        afield.Statut = pCours
        afield.Value = ""
        '-> on ajoute l'enregistrement
        Fields.Add afield, afield.KeyWord, , lastKeyWord
        '-> on se prepare la prochaine cl�
        lastKeyWord = curKeyWord
        Niveau(7) = Niveau(7) + 1
        curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
    End If
Next

'-> on ajoute les valeurs par defaut
CtrlFieldsCreate (saveKeyWord)

'-> on remet a jour les compteurs
CtrlFieldsCompteur

'-> on est en cr�ation on modifie le statut
curStatut = pCreate

End Function

Public Function EnregCreate() As Boolean
'--> cette procedure permet d'ajouter une ligne d'enreg selon ou l'on est positionn�
'-> on recherche les structures non pr�sentes apr�s la ligne selectionn�e
'-> pour les lignes d'enregistrement que l'on cr�� c'est le niveau 6 qui est utilis�

Dim afield As dc_field
Dim strStructure As String
Dim strKeyWord As String
Dim curKeyWord As String
Dim curRow As Integer
Dim curIndex As Integer
Dim i As Integer
Dim Niveau As Variant
Dim strTemp As Variant

curRow = df_N4ds.DealGrid1.Row
strKeyWord = df_N4ds.DealGrid1.GetCellText(3, curRow)
If strKeyWord = "" Then GoTo GestError

'-> on pointe sur la structure de la ligne
strStructure = Fields(strKeyWord).Struct
curIndex = Structures(strStructure).Index

If Structures(curIndex).Multiple = True Then curIndex = curIndex - 1

'-> on regarde si on a la meme structure sur la ligne suivante sinon l'ajouter si la ligne existe
If df_N4ds.DealGrid1.Rows > curRow + 1 Then
    If Structures(curIndex + 1).FieldValue = Fields(df_N4ds.DealGrid1.GetCellText(3, curRow + 1)).Struct Then GoTo GestError
Else
    If Fields(df_N4ds.DealGrid1.GetCellText(3, curRow)).Niveau <> Structures(curIndex + 1).FieldNiveau Then GoTo GestError
End If
'-> on verifie que l'on ne change pas de type
If Structures(curIndex + 1).FieldValue = Structures(curIndex + 1).FieldType Then GoTo GestError

'-> on ajoute une ligne
df_N4ds.DealGrid1.AddRow True, curRow + 1

'-> on insere les valeurs de ligne
df_N4ds.DealGrid1.SetCellText 0, curRow + 1, Structures(curIndex + 1).FieldValue
df_N4ds.DealGrid1.SetCellText 1, curRow + 1, Structures(curIndex + 1).FieldDescription
df_N4ds.DealGrid1.SetColAlignement 2, 2

'-> on cree la cl� de l'enregistrement
'-> on se met la cle dans un vecteur pour pouvoir travailler dessus
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split(strKeyWord, "|")
For i = 1 To 7
    Niveau(i) = CSng(strTemp(i))
Next
Niveau(6) = Niveau(6) + 1
curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
df_N4ds.DealGrid1.SetCellText 3, curRow + 1, curKeyWord
Set afield = New dc_field
afield.KeyWord = curKeyWord
afield.Niveau = Structures(curIndex + 1).FieldNiveau
afield.Struct = Structures(curIndex + 1).FieldValue
afield.Statut = pCours
'-> avant d'ajouter on regarde si le champ n'est pas d�ja existant
If Fields(1).IsField(afield.KeyWord) Then
    Fields(afield.KeyWord).Statut = pCours
Else
    Fields.Add afield, afield.KeyWord, , strKeyWord
End If

EnregCreate = True

Exit Function

GestError:
DisplayMessage "Aucun enregistrement � ajouter apr�s la position courante", dsmQuestion, dsmOkOnly, "Ajout d'un enregistrement"

End Function

Public Function EnregCreateFree() As Boolean
'--> cette procedure permet d'ajouter une ligne ou plusieurs lignes d'enreg selon ou l'on est positionn�
'-> on recherche les structures non pr�sentes apr�s la ligne selectionn�e
'-> pour les lignes d'enregistrement que l'on cr�� c'est le niveau 6 qui est utilis�

Dim afield As dc_field
Dim strStructure As String
Dim strKeyWord As String
Dim curKeyWord As String
Dim curRow As Integer
Dim curIndex As Integer
Dim i As Integer
Dim Niveau As Variant
Dim strTemp As Variant
Dim aItem As ListItem

curRow = df_N4ds.DealGrid1.Row
strKeyWord = df_N4ds.DealGrid1.GetCellText(3, curRow)
If strKeyWord = "" Then GoTo GestError

'-> on pointe sur la structure de la ligne
strStructure = Fields(strKeyWord).Struct
curIndex = Structures(strStructure).Index

df_AjoutLignes.ListView1.ColumnHeaders.Clear
df_AjoutLignes.ListView1.ListItems.Clear
'-> on cree les entete du tableau
df_AjoutLignes.ListView1.ColumnHeaders.Add , , "Rubrique", 100
df_AjoutLignes.ListView1.ColumnHeaders.Add , , "Libell�", 100
df_AjoutLignes.ListView1.ColumnHeaders.Add , , "strKeyword", 0

'-> dans le cas ou la valeur peu etre en doublon on la repropose
If Structures(curIndex).Multiple = True Then curIndex = curIndex - 1

'-> on boucle tant que l'on peut ajouter des enregistrements
Do
    '-> on regarde si on a la meme structure sur la ligne suivante sinon l'ajouter si la ligne existe
    If df_N4ds.DealGrid1.Rows > curRow + 1 Then
        If Structures(curIndex + 1).FieldValue = Fields(df_N4ds.DealGrid1.GetCellText(3, curRow + 1)).Struct Then GoTo Suite
    Else
        If Fields(df_N4ds.DealGrid1.GetCellText(3, curRow)).Niveau <> Structures(curIndex + 1).FieldNiveau Then GoTo Suite
    End If
    '-> on verifie que l'on ne change pas de type
    If Structures(curIndex + 1).FieldValue = Structures(curIndex + 1).FieldType Then GoTo Suite
    '-> on se charge une liste des champs possibles a ajouter
    Set aItem = df_AjoutLignes.ListView1.ListItems.Add(, Structures(curIndex + 1).FieldValue, Structures(curIndex + 1).FieldValue)
    '-> on se met la cle dans un vecteur pour pouvoir travailler dessus
    Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
    '-> on se charge le vecteur a partir de la cl�
    strTemp = Split(strKeyWord, "|")
    For i = 1 To 7
        Niveau(i) = CSng(strTemp(i))
    Next
    Niveau(6) = Niveau(6) + 1
    strKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
    aItem.SubItems(1) = Structures(curIndex + 1).FieldDescription
    aItem.SubItems(2) = strKeyWord
    If strKeyWord = "" Then GoTo GestError
    '-> on pointe sur la structure de la ligne
    curIndex = curIndex + 1
    strStructure = Structures(curIndex).FieldValue
Loop

Suite:

If aItem Is Nothing Then GoTo GestError

If df_AjoutLignes.ListView1.ListItems.Count = 1 Then
    EnregCreate
    Exit Function
End If

FormatListView df_AjoutLignes.ListView1
df_AjoutLignes.ListView1.ColumnHeaders.Item(3).Width = 0

'-> on affiche les lignes
df_AjoutLignes.Show vbModal

'-> au retour selon ce que l'on a selectionn� on ajoute des lignes pour eviter des trous les lignes
' pr�cedentes sont ajout�es a delete

EnregCreateFree = True

Exit Function

GestError:
DisplayMessage "Aucun enregistrement � ajouter apr�s la position courante", dsmQuestion, dsmOkOnly, "Ajout d'un enregistrement"

End Function


Public Function EnregSave(strKeyWord As String, ValueSaisie As String) As Boolean
'--> dans cette proc�dure on effectue des controles de second degr�
Fields(strKeyWord).Value = ValueSaisie

'-> on regarde si on doit faire un controle sur les valeurs autoris�e
If Structures(Fields(strKeyWord).Struct).AutoriValues <> "" Then
    If InStr(1, Structures(Fields(strKeyWord).Struct).AutoriValues, "|" & ValueSaisie & "|") = 0 Then
        DisplayMessage "Valeur non autoris�e", dsmQuestion, dsmOkOnly, "Modification d'un enregistrement"
        GoTo GestError
    End If
End If

Select Case Fields(strKeyWord).Statut
    Case pCours
        Fields(strKeyWord).Statut = pCrea
    Case pNormal
        Fields(strKeyWord).Statut = pModif
End Select

'-> on modifie le statut
curStatut = pModify

EnregSave = True
Exit Function
GestError:

End Function

Public Function EnregDelete(strKeyWord As String, curRow As Integer) As Boolean
'--> cette proc�dure permet de supprimer si on a le droit un  enreg
On Error GoTo GestError
'-> on effectue les controles de suppression
If Structures(Fields(strKeyWord).Struct).Statut = pObligatoire And Fields(strKeyWord).Statut <> pCours Then GoTo GestError

'-> on met l'enreg en suppression
Fields(strKeyWord).Statut = pDelete

'-> on supprime l'enregistrement de l'ecran
df_N4ds.DealGrid1.RemoveRow (curRow)

'-> on modifie le statut
curStatut = pModify

EnregDelete = True
Exit Function
GestError:
DisplayMessage "Suppression non autoris�e", dsmQuestion, dsmOkOnly, "Suppression d'un enregistrement"

End Function

Public Function EnregsDelete(strKeyWord As String) As Boolean
'--> cette proc�dure permet de supprimer si on a le droit tout un noeud d'enregistrement
Dim Niveau As Variant
Dim strTemp As Variant
Dim afield As dc_field
Dim tempKeyWord As String
Dim i As Integer

If strKeyWord = "" Then Exit Function

'-> Demander confirmation sur la suppression des enregistrement
strRetour = DisplayMessage("Supprimer d�finitivement l'enregistrement selectionn� (" & df_N4ds.TreeView1.SelectedItem.Text & ")?", dsmWarning, dsmYesNo, "Confirmation de suppression")
If strRetour = "NON" Then Exit Function

On Error GoTo GestError
'-> on effectue les controles de suppression

'-> on se met la cle dans un vecteur pour pouvoir travailler dessus
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split(strKeyWord, "|")
For i = 1 To 6
    Niveau(i) = CSng(strTemp(i))
Next

'-> on cree la chaine de base du noeud
For i = 1 To Fields(strKeyWord).Niveau
    tempKeyWord = tempKeyWord & "|" & CSng(strTemp(i))
Next

tempKeyWord = "�" & tempKeyWord & "|"

'-> on va mettre le statut a delete de tous les enreg de ce noeud et des noeud inferieurs
For Each afield In Fields
    '-> on verifie que l'enreg appartient bien � notre noeud
    If InStr(1, "�" & afield.KeyWord, tempKeyWord) <> 0 Then
        '-> on met tous les enreg du noeud en suppression
        afield.Statut = pDelete
    End If
Next

'-> on supprime l'enregistrement de l'ecran au niveau du treeview et du dealgrid
df_N4ds.TreeView1.Nodes.Remove (strKeyWord)
LoadDealGrid df_N4ds.TreeView1.SelectedItem.Key

'-> on remet a jour les compteurs
CtrlFieldsCompteur

EnregsDelete = True
Exit Function
GestError:
DisplayMessage "Suppression non autoris�e", dsmQuestion, dsmOkOnly, "Suppression d'enregistrements"

End Function

Public Function EnregsDeleteRubrique(strKeyWord As String) As Boolean
'--> permet de supprimer l'ensemble d'une rubrique sur un noeud
df_Suppr.init (strKeyWord)

End Function

Public Function EnregsReplaceRubrique(strKeyWord As String) As Boolean
'--> permet de supprimer l'ensemble d'une rubrique sur un noeud
df_Replace.init (strKeyWord)

End Function

Public Function EnregsCopy(strKeyWord As String) As Boolean
'--> cette proc�dure permet de copier un enregistrement

On Error GoTo GestError
'-> on effectue les controles de copie

'-> en fait on ne fait que pointer sur la cl� de l'enregistrement
StrCopyKey = strKeyWord

EnregsCopy = True
Exit Function
GestError:
DisplayMessage "Copie non autoris�e", dsmQuestion, dsmOkOnly, "Copie d'enregistrements"

End Function

Public Function GetKeyWord(strKeyWord As String, position As pPosition) As String
Dim topOk As Boolean
Dim topSearch As Boolean
Dim afield As dc_field
Dim lastKeyWord As String

'-> renvoit le rowid par rapport � la position demand�e
Select Case position
    Case pDernierEnfant
        For Each afield In Fields
            If afield.Niveau < Fields(strKeyWord).Niveau Or (afield.IsType And afield.Niveau = Fields(strKeyWord).Niveau) Then
                If topSearch = True Then
                    GetKeyWord = lastKeyWord
                    Exit Function
                End If
            End If
            If afield.KeyWord = strKeyWord Then
                topSearch = True
            End If
            lastKeyWord = afield.KeyWord
        Next
    Case pAvant
        For Each afield In Fields
            If afield.KeyWord = strKeyWord Then
                GetKeyWord = lastKeyWord
            End If
            lastKeyWord = afield.KeyWord
        Next
    Case Else
End Select

End Function


Public Function EnregsPaste(strKeyWord As String) As Boolean
'--> cette proc�dure permet de coller si on a le droit tout un noeud d'enregistrement
Dim Niveau As Variant
Dim curNiveau As Integer
Dim strTemp As Variant
Dim strTemp2 As Variant
Dim translate As Variant
Dim afield As dc_field
Dim curfield As dc_field
Dim tempKeyWord As String
Dim curKeyWord As String
Dim lastKeyWord As String
Dim lastNodeKey As String
Dim lastToAdd As String
Dim i As Integer
Dim topOk As String

On Error GoTo GestError
'-> on effectue les controles de d'ajout

'-> si on est au meme niveau on remonte d'un niveau
If Fields(strKeyWord).Niveau = Fields(StrCopyKey).Niveau Then
    '-> on pointe sur le parent
    strKeyWord = df_N4ds.TreeView1.Nodes(strKeyWord).Parent.Key
End If

'-> strkeyword contient l'adresse du parent ou ajouter les enregistrements

'-> On verifie que l'on a bien un element sauvegard�
If StrCopyKey = "" Then GoTo GestError

'-> on se met la cle dans un vecteur pour pouvoir travailler dessus
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split(StrCopyKey, "|")
For i = 1 To 6
    Niveau(i) = CSng(strTemp(i))
Next
'-> on cr�e la chaine de base du noeud
For i = 1 To Fields(StrCopyKey).Niveau
    tempKeyWord = tempKeyWord & "|" & CSng(strTemp(i))
Next
tempKeyWord = "�" & tempKeyWord & "|"

'-> on se met la cle ou ajouter l'enreg dans un vecteur pour pouvoir travailler dessus
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
strTemp = Split(strKeyWord, "|")
'-> on cr�e la chaine de base du noeud ou l'on va ajouter l'enreg
curNiveau = Fields(strKeyWord).Niveau
For i = 1 To 7
    If i <= curNiveau + 1 Then
        Niveau(i) = CSng(strTemp(i))
        curKeyWord = curKeyWord & "|" & CSng(strTemp(i))
    Else
        Niveau(i) = 0
        curKeyWord = curKeyWord & "|" & 0
    End If
Next

lastNodeKey = curKeyWord

'-> on cherche maintenant l'adresse du premier noeud a creer
'   ne pas s'appuyer sur le treeview car il y a d'autres enregs
Do While Fields(1).IsField(curKeyWord)
    If Fields(curKeyWord).Statut <> pDelete And Structures(Fields(curKeyWord).Struct).Index <= Structures(Fields(StrCopyKey).Struct).Index Then
        lastKeyWord = curKeyWord
    End If
    '-> on regarde si on a depass� le niveau si oui le dernier etait le bon
    If Structures(Fields(curKeyWord).Struct).Index > Structures(Fields(StrCopyKey).Struct).Index Then
        Exit Do
    End If
    
    '-> on regarde si il y a eu d'autres creations de blocs d'enreg niveau 5 dedi�
    curKeyWord = ""
    Niveau(5) = Niveau(5) + 1
    For i = 1 To 7
            curKeyWord = curKeyWord & "|" & Niveau(i)
    Next
    '-> sinon on continue d'explorer
    If Not Fields(1).IsField(curKeyWord) Then
        Niveau(curNiveau + 1) = Niveau(curNiveau + 1) + 1
        Niveau(5) = 0
        curKeyWord = ""
        For i = 1 To 7
            curKeyWord = curKeyWord & "|" & Niveau(i)
        Next
    End If
Loop

'-> on pointe maintenant sur la derniere entree
lastNodeKey = lastKeyWord
lastKeyWord = GetKeyWord(lastKeyWord, pDernierEnfant)

'-> on calcul maintenant la valeur du vecteur de translation
strTemp = Split(StrCopyKey, "|")
strTemp2 = Split(lastNodeKey, "|")
'-> on cr�e un noeud niveau 5 dedi�
strTemp(5) = strTemp(5) - 1
translate = Array(0, 0, 0, 0, 0, 0, 0, 0)
For i = 1 To 7
    translate(i) = strTemp2(i) - strTemp(i)
Next

'-> on parcours les enregistrements contenus dans notre noeud sauvegard� pour les injecter dans la nouvelle position
For Each afield In Fields
    If afield.KeyWord = StrCopyKey Then topOk = "OK"
    If topOk = "OK" Or topOk = "LAST" Then
        If afield.Niveau = Fields(StrCopyKey).Niveau And afield.IsType Then
            '-> on quitte la boucle
            If topOk = "LAST" Then
                Exit For
            Else
                topOk = "LAST"
            End If
        End If
        '-> on verifie que l'enreg appartient bien � notre noeud
        If InStr(1, "�" & afield.KeyWord, tempKeyWord) <> 0 Then
                    
            '-> on cr�e la cl� ou ajouter notre enregistrement
            Niveau = Split(afield.KeyWord, "|")
            curKeyWord = ""
            For i = 1 To 7
                If i <= curNiveau Then
                    curKeyWord = curKeyWord & "|" & strTemp2(i)
                Else
                    curKeyWord = curKeyWord & "|" & (Niveau(i) + translate(i))
                End If
            Next
            '-> on ajoute le nouvel enregistrement
            Set curfield = New dc_field
            curfield.IsType = afield.IsType
            curfield.KeyWord = curKeyWord
            curfield.Niveau = afield.Niveau
            If afield.Statut = pNormal Then
                curfield.Statut = pCrea
            Else
                curfield.Statut = afield.Statut
            End If
            curfield.Struct = afield.Struct
            curfield.Value = afield.Value
            '-> on ajoute l'enregistrement dans l'objet
            Fields.Add curfield, curKeyWord, , lastKeyWord
            '-> on ajoute l'enregistrement � l'ecran au niveau du treeview
            If afield.IsType Then
                '-> on regarde comment et ou ajouter le noeud
                If afield.Niveau = Fields(lastKeyWord).Niveau Then df_N4ds.TreeView1.Nodes.Add lastNodeKey, 2, curKeyWord, df_N4ds.TreeView1.Nodes(afield.KeyWord).Text, df_N4ds.TreeView1.Nodes(afield.KeyWord).Image
                If afield.Niveau > Fields(lastKeyWord).Niveau Then df_N4ds.TreeView1.Nodes.Add lastNodeKey, 4, curKeyWord, df_N4ds.TreeView1.Nodes(afield.KeyWord).Text, df_N4ds.TreeView1.Nodes(afield.KeyWord).Image
                If afield.Niveau < Fields(lastKeyWord).Niveau Then
                    lastKeyWord = df_N4ds.TreeView1.Nodes(afield.KeyWord).Parent.Key
                    df_N4ds.TreeView1.Nodes.Add lastNodeKey, 2, curKeyWord, df_N4ds.TreeView1.Nodes(afield.KeyWord).Text, df_N4ds.TreeView1.Nodes(afield.KeyWord).Image
                End If
                '-> on sauvegarde le dernier noeud
                lastNodeKey = curKeyWord
            End If
            '-> on sauvegarde la cl�
            lastKeyWord = curKeyWord
        End If
    End If
Next


LoadDealGrid df_N4ds.TreeView1.SelectedItem.Key

'-> on remet a jour les compteurs
CtrlFieldsCompteur

EnregsPaste = True
Exit Function
GestError:
DisplayMessage "Coller non autoris�e", dsmQuestion, dsmOkOnly, "Duplication d'enregistrements"

End Function

Public Function GetImage(curKeyWord As String, curNiveau As Integer) As Integer
'--> cette fonction permet de ramener le numero d'image du listimage
Dim intImage As Integer

On Error GoTo GestError

'-> on met une image selon le niveau
Select Case curNiveau
    Case 0
        If Mid(curKeyWord, 1, 3) = "S90" Then
            intImage = 21
        Else
            intImage = 11
        End If
    Case 1
        '-> juste pour le timer
        DoEvents
        If Mid(curKeyWord, 1, 3) = "S90" Then
            intImage = 21
        Else
            intImage = 20
        End If
    Case 2
        Select Case Mid(curKeyWord, 1, 3)
            Case "S80"
                intImage = 19
            Case "S70", "S68"
                intImage = 8
            Case Else
                intImage = 10
        End Select
    Case 3
        Select Case Mid(curKeyWord, 1, 3)
            Case "S46"
                intImage = 18
            Case "S66"
                intImage = 7
            Case "S70", "S68"
                intImage = 8
            Case Else
                intImage = 17
        End Select
    Case Else
        intImage = 11
End Select

GetImage = intImage

Exit Function
GestError:
End Function

Public Function CtrlFieldsDealGrid() As Boolean
'--> cette fonction va permettre de controler tous les champs de la saisie en cours dans le DealGrid
Dim Row As Integer
Dim curKeyWord As String
Dim Niveau As Variant
Dim strTemp As Variant
Dim i As Integer

'-> on parcours les lignes du dealgrid
For Row = 1 To df_N4ds.DealGrid1.Rows - 1
    '-> on reccupere le keyword
    curKeyWord = df_N4ds.DealGrid1.GetCellText(3, Row)
    '-> on effectue des test si on est pas pass� sur la ligne
    If Fields(curKeyWord).Statut = pCours Then
        '-> on effectue les controles avec les valeurs de la zone
        If CtrlFormatZone(df_N4ds.DealGrid1.GetCellText(2, Row), Fields(curKeyWord).Struct) Then
            'on passe le statut � pcrea
            Fields(curKeyWord).Statut = pCrea
        Else
            '-> on est pas ok
            GoTo GestError
        End If
    End If
    '-> si on est sur un noeud du dealgrid on met a jour le texte du treeview
    If Fields(curKeyWord).IsType Then
        Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
        '-> on se charge le vecteur a partir de la cl�
        strTemp = Split(curKeyWord, "|")
        For i = 1 To 7
            Niveau(i) = CSng(strTemp(i))
        Next
        df_N4ds.TreeView1.Nodes(curKeyWord).Text = Mid(Fields(curKeyWord).Struct, 1, 3) & " - " & GetLabel(Fields(curKeyWord).KeyWord, Niveau)
    End If
Next

Exit Function
GestError:
'-> se positionner sur le dealgrid
df_N4ds.DealGrid1.SelectRow Row
End Function

Public Function CtrlFieldsCours() As Boolean
'--> cette fonction va permettre de controler tous les champs
Dim afield As dc_field

'-> premiere �tape on verifie que l'on a pas des enregistrements avec le statut en cours de saisie
For Each afield In Fields
    If afield.Statut = pCours Then
        '-> se positionner sur le noeud
        LoadDealGrid (afield.KeyWord)
        '-> afficher le message
        DisplayMessage "Donn�es en cours de cr�ation", dsmWarning, dsmOkOnly, "V�rification des donn�es"
        Exit Function
    End If
Next

CtrlFieldsCours = True
End Function

Public Function CtrlFieldsConditions() As Boolean
'--> cette fonction va permettre de controler tous les champs
Dim afield As dc_field

'-> premiere �tape on verifie que l'on a pas des enregistrements avec le statut en cours de saisie
For Each afield In Fields

Next

CtrlFieldsConditions = True
End Function

Public Function CtrlFieldsCompteur() As Boolean
'--> cette fonction va permettre de mettre a jour les champs de compteur
Dim afield As dc_field
Dim iRubrique As Single
Dim iEnreg As Single
Dim Niveau As Variant
Dim strTemp As Variant
Dim i As Integer

iRubrique = 0
iEnreg = 0

'-> on parcours les enregistrement pour faire nos compte
For Each afield In Fields
    If afield.Statut <> pDelete And afield.Statut <> pNone Then
        If afield.Struct = "S20.G01.00.001" Then iEnreg = iEnreg + 1
        If Mid(afield.Struct, 1, 3) <> "S90" Then iRubrique = iRubrique + 1
    End If
Next

'-> on met a jour les champ
Fields("|2|0|0|0|0|0|0").Value = iRubrique + 1
Fields("|2|1|0|0|0|0|0").Value = iEnreg

'-> on met a jour le treeview
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split("|2|0|0|0|0|0|0", "|")
For i = 1 To 7
    Niveau(i) = CSng(strTemp(i))
Next
df_N4ds.TreeView1.Nodes("|2|0|0|0|0|0|0").Text = "S90 - " & GetLabel("|2|0|0|0|0|0|0", Niveau)
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split("|2|1|0|0|0|0|0", "|")
For i = 1 To 7
    Niveau(i) = CSng(strTemp(i))
Next
df_N4ds.TreeView1.Nodes("|2|1|0|0|0|0|0").Text = "S90 - " & GetLabel("|2|1|0|0|0|0|0", Niveau)

CtrlFieldsCompteur = True
End Function

Public Function CtrlFieldsSame() As Boolean
'--> cette fonction � pour but de v�rifier qu'il y a eu une modification
Dim afield As dc_field

On Error GoTo GestError

'-> on regarde si on a un statut different de celui de l'ouverture
For Each afield In Fields
    If afield.Statut <> pNormal Then Exit Function
Next

GestError:

CtrlFieldsSame = True
End Function

Public Function CtrlFieldsAjout(strKeyWord As String) As Boolean
'--> cette fonction va permettre de verifier si on peut faire l'ajout

On Error GoTo GestError

'-> on teste si on peut faire un ajout d'ou l'on est
Select Case Mid(Fields(strKeyWord).Struct, 1, 3)
    Case "S90", "S10", ""
        GoTo GestError
    Case Else
    
End Select

CtrlFieldsAjout = True

GestError:

End Function

Public Function CtrlFieldsCreate(strKeyWord As String) As Boolean
'--> cette fonction va permettre lors d'une creation de rajout� les �l�ments par d�faut

On Error GoTo GestError

'-> on teste si on peut faire un ajout d'ou l'on est
Select Case Mid(Fields(strKeyWord).Struct, 1, 3)
    Case "S90", "S10", ""
    Case Else
    
End Select

CtrlFieldsCreate = True

GestError:

End Function

Public Function CtrlFieldsSuppression(strKeyWord As String) As Boolean
'--> cette fonction va permettre de verifier si on peut faire l'ajout

On Error GoTo GestError

'-> on teste si on peut faire une suppression
Select Case Mid(Fields(strKeyWord).Struct, 1, 3)
    Case "S90", "S10", ""
        GoTo GestError
    Case Else
    
End Select

CtrlFieldsSuppression = True

GestError:

End Function

Public Function CtrlFieldsPaste(strKeyWord As String) As Boolean
'--> cette fonction va permettre de verifier si on peut faire l'ajout

On Error GoTo GestError

'-> on teste si on peut faire un coller

'-> d�j� on doit avoir qque chose de copi�
If StrCopyKey = "" Then GoTo GestError

Select Case Mid(Fields(strKeyWord).Struct, 1, 3)
    Case "S90", "S10", ""
        GoTo GestError
    Case Else
    
End Select

'-> on doit �tre sur le niveau identique ou au juste dessus
If Fields(StrCopyKey).Niveau = Fields(strKeyWord).Niveau Then CtrlFieldsPaste = True
If Fields(StrCopyKey).Niveau = Fields(strKeyWord).Niveau + 1 Then CtrlFieldsPaste = True

GestError:

End Function

Public Function CtrlFieldsCopy(strKeyWord As String) As Boolean
'--> cette fonction va permettre de verifier si on peut faire l'ajout

On Error GoTo GestError

'-> on teste si on peut faire un copier
Select Case Mid(Fields(strKeyWord).Struct, 1, 3)
    Case "S90", "S10", "", "S20"
        GoTo GestError
    Case Else
    
End Select

'-> valeur de succes
CtrlFieldsCopy = True

GestError:

End Function

Public Function FileExist(Fichier As String) As Boolean

On Error GoTo GestError

If Dir$(Fichier) <> "" Then FileExist = True
Exit Function

GestError:
    FileExist = False

End Function

Public Function EnregsSave() As Boolean
'--> cette fonction va permettre de reg�n�r� le fichier N4ds ou d'en creer un nouveau
'    on travaille avec un fichier tempo pour moins de risque
Dim afield As dc_field
Dim FileName As String
Dim TempFileName As String
Dim hdlFile As Integer
Dim i As Integer
Dim Niveau As Variant
Dim strTemp As Variant
Dim curKeyWord As String

If Fields Is Nothing Then Exit Function

If Fields.Count = 0 Then Exit Function

'-> on regarde si il y a eu des modifications sur le fichier en cours
If CtrlFieldsSame Then
    If Not SaveInTDS Then
        DisplayMessage "Aucunes modifications sur le fichier en cours", dsmWarning, dsmOkOnly, "Reg�n�ration du fichier"
        Exit Function
    End If
End If

'-> on v�rifie qu'il n'y a pas des donn�es en cours de cr�ation
If Not CtrlFieldsCours Then GoTo GestError

'-> on v�rifie les donn�es conditionnelles
If Not CtrlFieldsConditions Then GoTo GestError

'-> on met � jour les champs de compteur
If Not CtrlFieldsCompteur Then GoTo GestError

'-> demander le nom du fichier on propose l'existant par defaut
If DisplayMessage("Fichier � cr�er", dsmQuestion, dsmOkCancel, "Reg�n�ration du fichier", True, FileN4ds) = "ANNULER" Then
    Exit Function
End If

FileName = InputValue

'-> on �crit le nouveau fichier N4ds dans un fichier temporaire
TempFileName = GetTempFileNameVB("N4ds")
hdlFile = FreeFile
Open TempFileName For Output As #hdlFile

'-> on affiche le timer
df_Drloading.Show
df_Drloading.Label1.Caption = "Enregistrement en cours..."


'-> on parcours les enregistrements pour les ecrire dans le fichier
For Each afield In Fields
    If afield.Statut <> pDelete And afield.Statut <> pNone Then
        '-> on controle si on doit ecrire ou pas a l'aide des tests conditionnels en s'appuyant sur le param�trage
        If CtrlFieldsEcriture(afield.KeyWord, afield.Value) Then
            If afield.Struct <> "" Then
                If afield.Value <> "" Then
                    Print #hdlFile, afield.Struct & ",'" & afield.Value & "'"
                Else
                    Print #hdlFile, afield.Struct & ",' '"
                End If
            End If
        Else
            '-> on decremente le compteur
            Fields("|2|0|0|0|0|0|0").Value = CLng(Fields("|2|0|0|0|0|0|0").Value) - 1
        End If
    End If
    i = i + 1
    If i = 2000 Then
        i = 0
        DoEvents
    End If
Next

'-> fermer le fichier
Close #hdlFile

'-> si on doit alimenter un fichier TDS
If SaveInTDS Then
    If FileExist("temptds.tmp") Then Kill "temptds.tmp"
    ExportTDS ("S10")
    ExportTDS ("S20")
    ExportTDS ("S70")
    ExportTDS ("S80")
    ExportTDS ("S90")
End If

'-> on masque le timer
Unload df_Drloading

'-> si le fichier existe demander si on l'�crase
If FileExist(FileName) Then
    If DisplayMessage("Le fichier" & Chr(13) & FileName & Chr(13) & "existe d�j�." & Chr(13) & "Le remplacer ?", dsmInterro, dsmYesNo, "Reg�n�ration du fichier") = "NON" Then
        Exit Function
    Else
        '-> supprimer le fichier
        Kill FileName
    End If
End If

'-> Cas du fichier TDS
If SaveInTDS Then
    If FileExist(FileName & ".tds") Then
        Kill FileName & ".tds"
    End If
    '-> copier le fichier
    FileCopy "temptds.tmp", FileName & ".tds"
    If Dir$("temptds.tmp") <> "" Then Kill "temptds.tmp"
End If

'-> copier le fichier
FileCopy TempFileName, FileName

'-> on modifie le statut en cours
curStatut = pSave

'-> Supprimer le fichier temporaire
If Dir$(TempFileName) <> "" Then Kill TempFileName

'Call Initialise

'LoadN4ds FileName

GestError:
End Function

Private Function LoadExportTDS(strName As String) As Boolean
'--> cette fonction permet de charger les colonnes du listview en lisant un fichier de
'    param�trage contenant les informations : N4dsTDS.ini
Dim hdlFile As Integer
Dim Ligne As String

'On Error GoTo GestError
strName = Entry(1, strName, "|")

'-> V�rifier que l'on trouve le fichier de structure
If searchExe("N4dsTDS.ini") = "" Then Exit Function

'-> Ouvrir le fichier ASCII de r�ponse et charger la matrice des lignes
hdlFile = FreeFile
Open searchExe("N4dsTDS.ini") For Input As #hdlFile
        
'-> on initialise les collections
Set StructuresTDS = New Collection

'-> On parcours les lignes du fichier de param�trage
Do While Not EOF(hdlFile)
    '-> Lecture de la ligne
    Line Input #hdlFile, Ligne
    '-> On passe les ligne � blanc ou les lignes de commentaire commencant par #
    If Trim(Ligne) <> "" And Mid(Ligne, 1, 1) <> "#" And Mid(Ligne, 1, 1) <> "[" Then
        Ligne = Ligne & ";;;;;;;"
        Set aStructureTDS = New dc_tds
        '-> on verifie que l'on est sur le bon export
        If Trim(Entry(1, Ligne, ";")) = strName Then
            '-> on charge l'entete du listview
            aStructureTDS.TDSfield = Trim(Entry(2, Ligne, ";"))
            aStructureTDS.TDSposA = Entry(3, Ligne, ";")
            aStructureTDS.TDSposB = Entry(4, Ligne, ";")
            aStructureTDS.TDSvalue = Trim(Entry(5, Ligne, ";"))
            aStructureTDS.TDSalign = Trim(Entry(7, Ligne, ";"))
            StructuresTDS.Add aStructureTDS
        End If
    Else
        '-> on reccupere ici la valeur du noeud soit le type
        If Mid(Ligne, 1, 1) = "[" Then
            If Entry(1, Entry(1, Ligne, ";"), "|") = "[" & strName Then
                strRetour = Structures(Entry(3, Ligne, ";")).FieldType
            End If
        End If
    End If 'Ne pas traiter les lignes <> "" et de commentaire
Loop

Close #hdlFile

'-> valeur de succes
LoadExportTDS = True

Exit Function
GestError:
DisplayMessage "Erreur lors de la lecture du fichier de param�trage des exports", dsmCritical, dsmOkOnly, ""

End Function

Private Sub ExportTDS(strKeyWord As String)
'--> lance l'edition sur les salari�s
Dim aItem As ListItem
Dim afield As dc_field
Dim afield2 As dc_field
Dim aCol As ColumnHeader
Dim curNiveau As Integer
Dim curKeyWord As String
Dim Niveau As Variant
Dim strTemp As Variant
Dim signe As Integer
Dim i As Integer
Dim m As Integer
Dim strIndex As String
Dim strLigne As String
Dim hdlFile As Integer
Dim TempFileName As String
Dim topOk As Boolean

On Error Resume Next

TempFileName = "temptds.tmp"
hdlFile = FreeFile
Open TempFileName For Append As #hdlFile

'-> on charge la collection en fonction du param�trage
If Not LoadExportTDS(strKeyWord) Then GoTo GestError
strKeyWord = strRetour
curNiveau = Structures(strKeyWord).FieldNiveau

Set afield2 = New dc_field

'-> on charge le listview en commencant par les informations de plus bas niveau
For Each afield In Fields
    DoEvents
    If afield.Statut <> pDelete Then
        '-> on regarde si on est sur une nouvelle entr�e de ligne
        If afield.Struct = strKeyWord And afield.Statut <> pDelete Then
            If Trim(strLigne) <> "" And topOk = True And strKeyWord <> "S90.G01.00.001" Then
                Print #hdlFile, strLigne
                topOk = False
                strLigne = ""
            End If
            '-> on en profite pour charger les informations transversales de plus haut niveau car
            '   on est d�j� pass� dessus on utilise pour cela l'index
            For Each aStructureTDS In StructuresTDS
                '-> on pointe sur les colonnes de niveau inferieur
                If Structures(aStructureTDS.TDSfield).FieldNiveau < curNiveau And aStructureTDS.TDSvalue = "" Then
                    '-> on se met la cle dans un vecteur pour pouvoir travailler dessus
                    Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
                    '-> on se charge le vecteur a partir de la cl�
                    strTemp = Split(afield.KeyWord, "|")
                    For i = 1 To 7
                        If i <= Structures(aStructureTDS.TDSfield).FieldNiveau Then
                            Niveau(i) = CSng(strTemp(i))
                        Else
                            Niveau(i) = 0
                        End If
                    Next
                    curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
                    '-> on charge la ligne avec la bonne valeur
                    Do While afield.IsField(curKeyWord)
                        If Fields(curKeyWord).Struct = aStructureTDS.TDSfield Then
                            If aStructureTDS.TDSalign = "R" Then
                                strLigne = TDSput(strLigne, aStructureTDS.TDSposA, aStructureTDS.TDSposB, Right(Fields(curKeyWord).Value, aStructureTDS.TDSposB - aStructureTDS.TDSposA + 1))
                            Else
                                strLigne = TDSput(strLigne, aStructureTDS.TDSposA, aStructureTDS.TDSposB, Fields(curKeyWord).Value)
                            End If
                            Exit Do
                        End If
                        '-> on passe a l'enreg suivant
                        Niveau(7) = Niveau(7) + 1
                        curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
                    Loop
                End If
                '-> on inscrit les valeurs fixes de la ligne
                If aStructureTDS.TDSvalue <> "" And aStructureTDS.TDSvalue <> "COUNT" And Left(aStructureTDS.TDSvalue, 1) <> "=" Then
                    If Trim(TDSget(strLigne, aStructureTDS.TDSposA, aStructureTDS.TDSposB)) = "" Then
                        strLigne = TDSput(strLigne, aStructureTDS.TDSposA, aStructureTDS.TDSposB, aStructureTDS.TDSvalue)
                    End If
                End If
            Next
        End If
        '-> on ecrit les infos sur la ligne si la donn�e est presente dans le fichier de d�finition
        If isColTDS(afield.Struct) And aStructureTDS.TDSvalue = "" Then
            If afield.Niveau >= curNiveau Then
                '-> on pointe sur la definition
                For Each aStructureTDS In StructuresTDS
                    '-> on garde la boucle car on peut avoir plusieurs fois la meme valeur
                    If aStructureTDS.TDSfield = afield.Struct Then
                        '-> on regarde si on a un alignement
                        If aStructureTDS.TDSalign = "R" Then
                            '-> on ecrit la valeur
                            strLigne = TDSput(strLigne, aStructureTDS.TDSposA, aStructureTDS.TDSposB, Right(afield.Value, aStructureTDS.TDSposB - aStructureTDS.TDSposA + 1))
                        End If
                        If aStructureTDS.TDSalign = "" Then
                            '-> on ecrit la valeur
                            strLigne = TDSput(strLigne, aStructureTDS.TDSposA, aStructureTDS.TDSposB, afield.Value)
                        End If
                        topOk = True
                    End If
                Next
            End If
        End If
        '-> cas des compteurs
        If isColTDS(afield.Struct) Then
                '-> on pointe sur la definition
                For Each aStructureTDS In StructuresTDS
                    '-> on garde la boucle car on peut avoir plusieurs fois la meme valeur
                    If aStructureTDS.TDSfield = afield.Struct And Trim(aStructureTDS.TDSvalue) = "COUNT" Then
                        '-> on ecrit la valeur par incrementation
                        strLigne = TDSput(strLigne, aStructureTDS.TDSposA, aStructureTDS.TDSposB, Right("00000000000000000000" & (Val(TDSget(strLigne, aStructureTDS.TDSposA, aStructureTDS.TDSposB)) + 1), Val(aStructureTDS.TDSposB) - Val(aStructureTDS.TDSposA) + 1))
                    End If
                Next
        End If
        '-> cas des valeurs conditionnelles
        If isColTDS(afield.Struct) Then
                '-> on pointe sur la definition
                For Each aStructureTDS In StructuresTDS
                    '-> on garde la boucle car on peut avoir plusieurs fois la meme valeur
                    If aStructureTDS.TDSfield = afield.Struct And Entry(2, aStructureTDS.TDSvalue & "==", "=") = afield.Value Then
                        '-> on ecrit la valeur
                        If Trim(Entry(3, aStructureTDS.TDSvalue & "===", "=")) = "" Then
                            strLigne = TDSput(strLigne, aStructureTDS.TDSposA, aStructureTDS.TDSposB, afield.Value)
                        Else
                            If Trim(Entry(3, aStructureTDS.TDSvalue & "===", "=")) = "SUIV" Then
                                Set afield2 = afield.NextField(afield.KeyWord)
                                strLigne = TDSput(strLigne, aStructureTDS.TDSposA, aStructureTDS.TDSposB, Right("00000000000000000000" & afield2.Value, Val(aStructureTDS.TDSposB) - Val(aStructureTDS.TDSposA) + 1))
                            End If
                            If Trim(Entry(3, aStructureTDS.TDSvalue & "===", "=")) = "SOMME" Then
                                Set afield2 = afield.NextField(afield.KeyWord)
                                strLigne = TDSput(strLigne, aStructureTDS.TDSposA, aStructureTDS.TDSposB, Right("00000000000000000000" & (Val(TDSget(strLigne, aStructureTDS.TDSposA, aStructureTDS.TDSposB)) + Val(afield2.Value)), Val(aStructureTDS.TDSposB) - Val(aStructureTDS.TDSposA) + 1))
                            End If
                        End If
                    End If
                Next
        End If
    End If
Next

If Trim(strLigne) <> "" Then Print #hdlFile, strLigne
'-> on ferme le fichier TDS
Close #hdlFile

GestError:

End Sub

Private Function TDSput(Ligne As String, iDebut As Integer, iFin As Integer, strValue As String) As String
'--> cette fonction permet d'ecrire dans la ligne du tds
If Len(Ligne) < 700 Then Ligne = Ligne & Space(700)

TDSput = Mid(Ligne, 1, iDebut - 1) & Mid(strValue & Space(700), 1, iFin - iDebut + 1) & Mid(Ligne, iFin + 1)
TDSput = Mid(TDSput, 1, 672)
TDSput = Replace(TDSput, Chr(9), "")
End Function

Private Function TDSget(Ligne As String, iDebut As Integer, iFin As Integer) As String
'--> cette fonction permet d'ecrire dans la ligne du tds
If Len(Ligne) < 700 Then Ligne = Ligne + Space(700)
TDSget = Mid(Ligne, iDebut, iFin - iDebut + 1)
End Function

Private Function isColTDS(strKey As String) As Boolean
'--> cette fonction permet de savoir si la colonne existe
On Error GoTo GestError

For Each aStructureTDS In StructuresTDS
    If aStructureTDS.TDSfield = strKey Then
        '-> valeur de succes
        isColTDS = True
        Exit For
    End If
Next

Exit Function
GestError:
End Function

Public Function CtrlFieldsEcriture(strKeyWord As String, strValue As String) As Boolean
'--> cette fonction � pour but de v�rifier si on doit �crire dans le fichier la ligne
'    concern�e ceci a l'aide des tests conditionnels en s'appuyant sur le fichier de param�trage
Dim afield As dc_field
Dim i As Integer

On Error GoTo GestError

'-> on regarde les conditions
Select Case Mid(Structures(Fields(strKeyWord).Struct).Condition, 1, 1)
    Case "N" 'Rubrique non g�r�e
        
    Case "I" 'Rubrique interdite
        GoTo GestError
    Case "B" 'Rubrique obligatoire si elle est diff�rente de ''
        If Fields(strKeyWord).Value = "" Then GoTo GestError
    Case "L" 'Rubrique conditionnelle
        '-> on verifie que toutes les conditions sont remplies
        For i = 2 To NumEntries(Structures(Fields(strKeyWord).Struct).Condition, "�")
            '-> on pointe sur le champ de condition
            Set afield = GetFieldAssociateValue(strKeyWord, Entry(i, Structures(Fields(strKeyWord).Struct).Condition, "�"))
            If afield Is Nothing Then GoTo Suite
            '-> on effectue un test recursif
            If CtrlFieldsEcriture(afield.KeyWord, afield.Value) Then CtrlFieldsEcriture = True
            '-> si la valeur du champ condition = '' alors pas d'ecriture
            If afield.Value <> "" Then CtrlFieldsEcriture = True
Suite:
        Next
        '-> dans le cas ou on est li� est que saisie conditionnelle
        If Not (Structures(Fields(strKeyWord).Struct).Statut = pConditionnel And afield.Value = "") Then CtrlFieldsEcriture = True
    Case "F" 'Rubrique facultative
        '-> on ecrit
    Case Else
        
End Select

'-> valeur de succes on doit ecrire
CtrlFieldsEcriture = True

GestError:

End Function
Public Function Initialise() As Boolean
'--> cette fonction a pour but de tout reinitialiser
Dim afield As dc_field
Dim i As Integer
Dim iTaille As Single

'On Error Resume Next

df_Drloading.Show
df_Drloading.Label1.Caption = "Vidage de la m�moire en cours..."
'-> on initialise la variable de copie
StrCopyKey = ""
'-> on vide le treeview
LockWindowUpdate df_N4ds.hwnd
df_N4ds.TreeView1.Nodes.Clear
'-> on vide le dealgrid
df_N4ds.DealGrid1.ClearRow
'-> on vide la zone non de fichier
df_N4ds.Label1.Caption = ""
'-> on vide les objets de travail
Set Structures = Nothing
If Not Fields Is Nothing Then
    iTaille = Fields.Count
    Do While Fields.Count <> 0
        Fields.Remove (1)
        i = i + 1
        If i = 1000 Then
            i = 0
            df_Drloading.Label1.Caption = "Vidage de la m�moire en cours..." & 100 - CInt(Fields.Count / iTaille * 100) & "%"
            DoEvents
        End If
    Loop
End If
Set Types = Nothing
Unload df_Drloading
LockWindowUpdate 0
End Function

Private Function GetFieldAssociateValue(strKeyWord As String, strStruct As String) As dc_field
'--> cette fonction permet de recup�rer la valeur d'un enregistrement � partir de sa valeur de structure
'    en s'appuyant sur les donn�es transversales d'un enreg de reference

Dim afield As dc_field
Dim curNiveau As Integer
Dim structNiveau As Integer
Dim curKeyWord As String
Dim Niveau As Variant
Dim strTemp As Variant
Dim i As Integer

'-> on pointe sur le niveau de la valeur de reference
curNiveau = Structures(Fields(strKeyWord).Struct).FieldNiveau
structNiveau = Structures(strStruct).FieldNiveau

'-> on analyse ou rechercher la valeur
If structNiveau <= curNiveau Then
    '-> on va chercher ici la valeur sur la transversale
    '-> on se met la cle dans un vecteur pour pouvoir travailler dessus
    Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
    '-> on se charge le vecteur a partir de la cl�
    strTemp = Split(strKeyWord, "|")
    For i = 1 To 7
        If i <= structNiveau Then
            Niveau(i) = CSng(strTemp(i))
        Else
            Niveau(i) = 0
        End If
    Next
    curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
    '-> on boucle pour trouver la bonne valeur
    Do While Fields(1).IsField(curKeyWord)
        If Fields(curKeyWord).Struct = strStruct Then
            Set GetFieldAssociateValue = Fields(curKeyWord)
            Exit Function
        End If
        '-> on passe a l'enreg suivant en testant egalement les eventuelles creations (niveau 6)
        If Fields(1).IsField("|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & (Niveau(6) + 1) & "|" & Niveau(7)) Then
            Niveau(6) = Niveau(6) + 1
            curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
        Else
            Niveau(6) = 0
            Niveau(7) = Niveau(7) + 1
            curKeyWord = "|" & Niveau(1) & "|" & Niveau(2) & "|" & Niveau(3) & "|" & Niveau(4) & "|" & Niveau(5) & "|" & Niveau(6) & "|" & Niveau(7)
        End If
    Loop
Else
    DisplayMessage "Il peut y avoir plusieurs donn�es r�pondant � cette requ�te!" & Chr(13) & strKeyWord & " sur " & strStruct, dsmCritical, dsmOkOnly, "GetFieldAssociateValue"
End If

GestError:

End Function

Public Function searchExe(strFile As String) As String
'-> cette fonction va chercher les fichiers en fonction du param�trage
'   diff�rentes possibilit�s
'   rien
'   chemin spe|chemin std
'   gloident
Dim sPath As String
Dim sParam As String

stopSearch = False

If Command$ = "" Then
    sParam = "deal"
Else
    sParam = Command$
End If

If InStr(1, sParam, "|") <> 0 Then
    '-> on lit d'abord dans le spe puis le standard
    If Dir$(Entry(1, sParam, "|") & "\" & strFile) <> "" Then
        searchExe = Entry(1, sParam, "|") & "\" & strFile
    Else
        If Dir$(Entry(1, sParam, "|") & "\" & strFile) <> "" Then
            searchExe = Entry(1, sParam, "|") & "\" & strFile
        Else
            searchExe = App.Path & "\" & strFile
        End If
    End If
Else
    '-> � partir du gloident on va essayer de s'y retrouver!
    '-> on pointe sur le chemin de base
    sPath = App.Path
    '-> pour les test
    'sPath = "R:\V6\tools\exe"
    sPath = Replace(sPath, "/", "\")
    '-> on regarde si on est en v51 ou v52
    If InStr(1, sPath, "\V51\") <> 0 Or InStr(1, sPath, "\V52\") <> 0 Then
        If InStr(1, sPath, "\V51\") <> 0 Then
            sPath = Mid(sPath, 1, InStr(1, sPath, "\V51\", vbTextCompare) + 4)
        Else
            sPath = Mid(sPath, 1, InStr(1, sPath, "\V52\", vbTextCompare) + 4)
        End If
        '-> repertoire v52/sophie de l'ident
        If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\deal\", "\" & Trim(sParam) & "\", , , vbTextCompare) & "\Sophie\", strFile)
        '-> repertoire v52/sophie de deal
        If searchExe = "" Then searchExe = SearchForFiles(sPath & "\Sophie\", strFile)
    Else '-> on regarde si on est en V6
        If InStr(1, sPath, "\dog\", vbTextCompare) <> 0 Or InStr(1, sPath, "\tools\", vbTextCompare) <> 0 Then
            sPath = Replace(sPath, "\tools\", "\dog\", , , vbTextCompare)
            sPath = Mid(sPath, 1, InStr(1, sPath, "\dog\", vbTextCompare) + 4)
            '-> on cherche dans le repertoire maquette du client
            If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\dog\", "\" & Trim(sParam) & "\sophie\", , , vbTextCompare), strFile)
            '-> on cherche dans le repertoire spe du client
            If searchExe = "" Then searchExe = SearchForFiles(sPath & "\" & sParam & "\", strFile)
            '-> on cherche dans le repertoire des maquettes
            If searchExe = "" Then searchExe = SearchForFiles(Replace(sPath, "\dog\", "\maq\gui\sophie\", , , vbTextCompare), strFile)
        End If
    End If
    '-> on regarde dans le repertoire courant
    If searchExe = "" Then searchExe = SearchForFiles(App.Path & "\" & Trim(sParam) & "\", strFile)
    If searchExe = "" Then searchExe = SearchForFiles(App.Path & "\", strFile)
    '-> on ramene le repertoire courant
    If searchExe = "" Then searchExe = App.Path & "\" & strFile
End If

End Function

Private Function SearchForFiles(sRoot As String, sfile As String, Optional sFiltreDirectory As String) As String
    '--> cette fonction cherche des fichiers � partir d'une directorie
   Dim WFD As WIN32_FIND_DATA
   Dim hFile As Long
  
   With fp
      .sFileRoot = QualifyPath(sRoot)      'chemin de d�part
      .sFileNameExt = sfile                'fichier (* ? autoris�
      .bRecurse = 1                             'True = recherche recursive
      .bFindOrExclude = 1                       '0=inclure, 1=exclure
   End With
   
   
   hFile = FindFirstFile(sRoot & "*.*", WFD)
   If hFile <> -1 Then
      Do
        If stopSearch = True Then Exit Function
        DoEvents
        'si c'est un repertoire on boucle
         If (WFD.dwFileAttributes And vbDirectory) Then
            If Asc(WFD.cFileName) <> CLng(46) Then
                If fp.bRecurse Then
                    SearchForFiles = SearchForFiles(sRoot & TrimNull(WFD.cFileName) & vbBackslash, sfile)
                End If
            End If
         Else
           'doit etre un fichier..
            If MatchSpec(WFD.cFileName, fp.sFileNameExt) Then
                If sFiltreDirectory <> "" Then
                    If InStr(1, sRoot & TrimNull(WFD.cFileName), sFiltreDirectory) <> 0 Then
                        SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                        stopSearch = True
                        Exit Do
                    End If
                Else
                    SearchForFiles = sRoot & TrimNull(WFD.cFileName)
                    stopSearch = True
                    Exit Do
                End If
            End If
         End If
      Loop While FindNextFile(hFile, WFD)
   End If
   Call FindClose(hFile)
End Function

Private Function QualifyPath(sPath As String) As String
   If Right$(sPath, 1) <> vbBackslash Then
      QualifyPath = sPath & vbBackslash
   Else
      QualifyPath = sPath
   End If
End Function

Private Function TrimNull(startstr As String) As String
   TrimNull = Left$(startstr, lstrlen(StrPtr(startstr)))
End Function

Private Function MatchSpec(sfile As String, sSpec As String) As Boolean
   MatchSpec = PathMatchSpec(StrPtr(sfile), StrPtr(sSpec)) = fp.bFindOrExclude
End Function

Public Function GetVariableEnv(strVariable As String) As String

Dim Res As Long
Dim lpBuffer As String

lpBuffer = Space$(500)
Res = GetEnvironmentVariable(strVariable, lpBuffer, Len(lpBuffer))
If Res <> 0 Then
    '-> Faire un trim sur le buffer
    lpBuffer = Mid$(lpBuffer, 1, Res)
Else
    lpBuffer = ""
End If

'-> Retouner la valeur
GetVariableEnv = lpBuffer

End Function

Public Sub SkinColorModify(aForm As Form)
'--> ceci est un essai de changement de couleur
Dim Lcolor As Long
Dim aControl As Control
Dim sKinName As String

'-> V�rifier que l'on trouve le fichier d'application du skin
sKinName = searchExe("Skin.ini")
If Dir$(sKinName) = "" Then Exit Sub

'-> Appliquer un skin s'il y en a un
If GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BodyBackColor = Val(GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "BODYFORECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BodyForeColor = Val(GetIniFileValue("SKIN", "BODYFORECOLOR", sKinName))
If GetIniFileValue("SKIN", "BORDERCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.BorderColor = Val(GetIniFileValue("SKIN", "BODYBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERBACKCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderBackColor = Val(GetIniFileValue("SKIN", "HEADERBACKCOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERFORECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderForeColor = Val(GetIniFileValue("SKIN", "HEADERFORECOLOR", sKinName))
If GetIniFileValue("SKIN", "HEADERLINECOLOR", sKinName) <> "" Then aForm.DogSkinObject1.HeaderLineColor = Val(GetIniFileValue("SKIN", "HEADERLINECOLOR", sKinName))
If GetIniFileValue("SKIN", "SHADOWCOLOR", sKinName) <> "" Then aForm.DogSkinObject1.ShadowColor = Val(GetIniFileValue("SKIN", "SHADOWCOLOR", sKinName))

'-> on parcours les differents elements pour faire une variation de couleur
For Each aControl In aForm
    If TypeOf aControl Is Shape Then
        If aControl.BackColor = &HF1CFB1 Or aControl.BackColor = 16708585 Then aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
        If aControl.BorderColor = &HC78D4B Then aControl.BorderColor = aForm.DogSkinObject1.BorderColor
    End If
    If TypeOf aControl Is DealCmdButton Or TypeOf aControl Is PictureBox Or TypeOf aControl Is DealCheckBox Then
        If aControl.BackColor = &HF1CFB1 Or aControl.BackColor = 16579059 Then aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
    End If
    If TypeOf aControl Is DealGrid Then
        If aControl.BackColor = &HFDDFC4 Then aControl.BackColor = aForm.DogSkinObject1.BodyBackColor
        If aControl.BackColorFixed = 16637889 Then aControl.BackColorFixed = aForm.DogSkinObject1.ShadowColor
        If aControl.BackColorSel = 10053171 Then aControl.BackColorSel = aForm.DogSkinObject1.ShadowColor
    End If
Next

End Sub

