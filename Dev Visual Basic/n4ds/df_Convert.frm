VERSION 5.00
Object = "{DAFD85F1-343B-47EE-8170-1C26723F7A5D}#2.0#0"; "dogskin.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form df_Convert 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   Caption         =   "Deal Informatique"
   ClientHeight    =   6915
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10875
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   461
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   725
   StartUpPosition =   1  'CenterOwner
   Begin VB.DriveListBox Drive1 
      Height          =   315
      Left            =   3600
      TabIndex        =   1
      Top             =   120
      Visible         =   0   'False
      Width           =   2775
   End
   Begin MSComctlLib.ListView ListView2 
      Height          =   3615
      Left            =   495
      TabIndex        =   2
      Top             =   1800
      Width           =   10080
      _ExtentX        =   17780
      _ExtentY        =   6376
      SortKey         =   1
      Arrange         =   2
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList2"
      ForeColor       =   10053171
      BackColor       =   16777215
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "FOLDER"
         Object.Width           =   38100
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   38100
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   -600
      Top             =   480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   48
      ImageHeight     =   48
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Convert.frx":0000
            Key             =   "POSTE"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Convert.frx":1CDA
            Key             =   "CDROM"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Convert.frx":39B4
            Key             =   "DDUR"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Convert.frx":568E
            Key             =   "DNET"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Convert.frx":7368
            Key             =   "DISQUETTE"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Convert.frx":9042
            Key             =   "FOLDER"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Convert.frx":AD1C
            Key             =   "OPENFOLDER"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Convert.frx":C9F6
            Key             =   "FILE"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Convert.frx":E6D0
            Key             =   "WINFILE"
         EndProperty
      EndProperty
   End
   Begin DogSkin.DealCmdButton cmdOk 
      Height          =   480
      Left            =   6720
      TabIndex        =   3
      Top             =   5880
      Width           =   2025
      _ExtentX        =   3572
      _ExtentY        =   847
      Caption         =   ""
      ForeColor       =   0
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   0
      ButtonForm      =   0
      BackColor       =   16579059
   End
   Begin DogSkin.DealCmdButton cmdCancel 
      Height          =   480
      Left            =   8760
      TabIndex        =   4
      Top             =   5880
      Width           =   2025
      _ExtentX        =   3572
      _ExtentY        =   847
      Caption         =   ""
      ForeColor       =   0
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   0
      ButtonForm      =   0
      BackColor       =   16579059
   End
   Begin DogSkin.DogSkinObject DogSkinObject1 
      Left            =   0
      Top             =   0
      _ExtentX        =   979
      _ExtentY        =   953
      BorderColor     =   13077835
      BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderForeColor =   10053171
      HeaderBackColor =   16637889
      HeaderLineColor =   15847345
      HeaderUseLine   =   -1  'True
      BeginProperty BodyFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BodyForeColor   =   10053171
      BodyBackColor   =   16579059
      HeaderIco       =   "df_Convert.frx":103AA
      HeaderIcoNa     =   "df_Convert.frx":10A2C
      ShadowColor     =   16637889
   End
   Begin DogSkin.DealCmdButton CmdConvertToBin 
      Height          =   480
      Left            =   4680
      TabIndex        =   5
      Top             =   5880
      Width           =   2025
      _ExtentX        =   3572
      _ExtentY        =   847
      Caption         =   ""
      ForeColor       =   0
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   0
      ButtonForm      =   0
      BackColor       =   16579059
   End
   Begin DogSkin.DealCmdButton CmdConvertToText 
      Height          =   480
      Left            =   2640
      TabIndex        =   6
      Top             =   5880
      Width           =   2025
      _ExtentX        =   3572
      _ExtentY        =   847
      Caption         =   ""
      ForeColor       =   0
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   0
      ButtonForm      =   0
      BackColor       =   16579059
   End
   Begin VB.Image imgAffichage 
      Height          =   525
      Left            =   8040
      MouseIcon       =   "df_Convert.frx":110AE
      MousePointer    =   99  'Custom
      Picture         =   "df_Convert.frx":11D78
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgSearch 
      Height          =   525
      Left            =   6840
      MouseIcon       =   "df_Convert.frx":12D9E
      MousePointer    =   99  'Custom
      Picture         =   "df_Convert.frx":13A68
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgProp 
      Height          =   525
      Left            =   4560
      MouseIcon       =   "df_Convert.frx":14A8E
      MousePointer    =   99  'Custom
      Picture         =   "df_Convert.frx":15758
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgCurrentPath 
      Height          =   525
      Left            =   5040
      MouseIcon       =   "df_Convert.frx":1677E
      MousePointer    =   99  'Custom
      Picture         =   "df_Convert.frx":17448
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgPosteTravail 
      Height          =   525
      Left            =   7440
      MouseIcon       =   "df_Convert.frx":1846E
      MousePointer    =   99  'Custom
      Picture         =   "df_Convert.frx":19138
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgNewFolder 
      Height          =   525
      Left            =   6240
      MouseIcon       =   "df_Convert.frx":1A15E
      MousePointer    =   99  'Custom
      Picture         =   "df_Convert.frx":1AE28
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgPrefolder 
      Height          =   525
      Left            =   5640
      MouseIcon       =   "df_Convert.frx":1BE4E
      MousePointer    =   99  'Custom
      Picture         =   "df_Convert.frx":1CB18
      Top             =   870
      Width           =   525
   End
   Begin VB.Image imgCours 
      Height          =   735
      Left            =   480
      Top             =   960
      Width           =   855
   End
   Begin VB.Label lblCours 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00996633&
      Height          =   615
      Left            =   1440
      TabIndex        =   0
      Top             =   1080
      Width           =   3855
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00996633&
      Height          =   5175
      Left            =   120
      Top             =   600
      Width           =   10575
   End
End
Attribute VB_Name = "df_Convert"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-> R�cup�rer des informations sur un disque
Private Declare Function GetLogicalDriveStrings& Lib "kernel32" Alias "GetLogicalDriveStringsA" (ByVal nBufferLength As Long, ByVal lpBuffer As String)
Private Declare Function GetDriveType& Lib "kernel32" Alias "GetDriveTypeA" (ByVal nDrive As String)
Private Declare Function GetVolumeInformation& Lib "kernel32" Alias "GetVolumeInformationA" (ByVal lpRootPathName As String, ByVal lpVolumeNameBuffer As String, ByVal nVolumeNameSize As Long, lpVolumeSerialNumber As Long, lpMaximumComponentLength As Long, lpFileSystemFlags As Long, ByVal lpFileSystemNameBuffer As String, ByVal nFileSystemNameSize As Long)
Private Declare Function GetDiskFreeSpace& Lib "kernel32" Alias "GetDiskFreeSpaceA" (ByVal lpRootPathName As String, lpSectorsPerCluster As Long, lpBytesPerSector As Long, lpNumberOfFreeClusters As Long, lpTotalNumberOfClusters As Long)

'-> Constante de type de lecteur
Private Const DRIVE_CDROM& = 5
Private Const DRIVE_FIXED& = 3
Private Const DRIVE_REMOTE& = 4
Private Const DRIVE_REMOVABLE& = 2

'-> Indique le mode de mise � jour
Dim ModeMaj As Integer '0-> Open, 1-> Save

'-> Indique le mode de fermeture
Dim ModeClose As Integer '0-> Fermeture normale (defaut), 1-> Ouvre le display

'-> Selon l'extension
Dim Extension As String
Dim DisplaySelectedExtension As Boolean
Dim NiveauDisque As Boolean

'-> Indiquer que l'on affiche que les r�pertoires
Dim DisplayOnlyDirectory As Boolean

Public Sub Init(InitMode As Integer, InitPath As String, Titre As String, DisplayExtentsion As String, Optional InitClose As Integer)

'---> Point d'entr�e de la feuille

Dim aIt As ListItem
Dim RemoteDriveName As String
Dim RemotePath As String
Dim i As Integer


Me.DogSkinObject1.Caption = Titre

'-> Libell�s MESSPROG
If InitMode = 0 Then
    Me.cmdOk.Caption = "Ouvrir"
    Me.CmdConvertToBin.Caption = "Convertir en Bin"
    Me.CmdConvertToText.Caption = "Convertir en Text"
    DisplayOnlyDirectory = False
ElseIf InitMode = 1 Then
    Me.cmdOk.Caption = "Enregistrer"
    DisplayOnlyDirectory = False
Else
    Me.cmdOk.Caption = "R�pertoire"
    DisplayOnlyDirectory = True
End If
Me.cmdCancel.Caption = "Fermer"

Me.imgCurrentPath.ToolTipText = "Emplacement en cours" 'MESSPROG
Me.imgNewFolder.ToolTipText = "Cr�ation d'un nouveau r�pertoire"
Me.imgPosteTravail.ToolTipText = "Poste de travail"
Me.imgPrefolder.ToolTipText = "Pr�c�dent"
Me.imgProp.ToolTipText = "Propri�t�s"
Me.imgSearch.ToolTipText = "Recherche"
Me.imgAffichage.ToolTipText = "Affichage"

'-> Positionner la variable
ModeMaj = InitMode

'-> Gestion des extensions
If Not DisplayOnlyDirectory Then
    '-> Doit on afficher une certaine extension
    If DisplayExtentsion = "" Then
        '-> Ne pas afficher d'extention particuli�re
        DisplaySelectedExtension = True
        Extension = "*.*"
    Else
        '-> Afficher une extentsion particuli�re
        DisplaySelectedExtension = True
        Extension = DisplayExtentsion
    End If

    '-> Poser les matrices des effets
    'Me.DogSkinObject1.SetMatriceEffect "Shape1|Shape2"
    '-> Afficher les zones de saisie
Else
    '-> Masquer les zones
    '-> Poser les matrices des effets
    Me.DogSkinObject1.SetMatriceEffect "Shape1"
End If

'-> Se positionner sur le r�pertoire pass� en argument
If InitPath = "" Then
    '-> Charger la liste des disques
    LoadDrive
    '-> Indiquer que l'on est au niveau disque
    NiveauDisque = True
Else
    '-> Gestion des disques r�seaux
    If Left(InitPath, 2) = "\\" Then
        '-> rechercher dans tous les lecteurs si on trouve la lettre de mapping
        For i = 0 To Me.Drive1.ListCount - 1
            '-> Tester s'il y a des "\\"
            If InStr(1, Me.Drive1.List(i), "\\") <> 0 Then
                '-> Get du nom r�seau
                RemoteDriveName = Entry(2, Me.Drive1.List(i), "[")
                RemoteDriveName = Mid$(RemoteDriveName, 1, Len(RemoteDriveName) - 1)
                '-> Chercher si on trouve la comparaison
                If InStr(1, UCase$(InitPath), UCase$(RemoteDriveName)) <> 0 Then
                    '-> On a trouve donc remplacer par la lettre
                    RemotePath = Mid$(InitPath, Len(RemoteDriveName) + 1, Len(InitPath) - Len(RemoteDriveName))
                    InitPath = UCase$(Trim(Entry(1, Me.Drive1.List(i), "["))) & RemotePath
                End If
            End If
        Next
    End If
    DisplayFileObject InitPath, Extension
End If

'-> Sauvegarder le mode de fermeture
ModeClose = InitClose

End Sub


Private Sub CmdConvertToBin_click()

'-> on initialise le rapport du convert
Mouchard = GetTempFileNameVB("DBG")
hdlMouchard = FreeFile
Open Mouchard For Output As #hdlMouchard

'-> mouchard
Print #hdlMouchard, "Lancement de la conversion � : " & Now & " vers format binaire"

'-> On lance la procedure de conversion des repertoires et sous repertoires
Call ParcoursRepertoire(Me.lblCours.Tag, True)

'-> mouchard
Print #hdlMouchard, "Fin de la conversion � : " & Now

Close #hdlMouchard

'-> ouvrir le mouchard
ShellExecute Me.hwnd, "Open", Mouchard, vbNullString, App.Path, 1
    
End Sub
Private Sub ParcoursRepertoire(strRepertoire As String, ToBin As Boolean)
'-> fonction de parcours de repertoires en r�cursif
Dim aFSO As Object
Dim aRepInit As Object
Dim aSousRep As Object

' Ouverture du fichier qui contiendra la liste
Set aFSO = CreateObject("Scripting.FileSystemObject")

'-> On charge les fichiers du repertoire en cours
Set aRepInit = aFSO.GetFolder(strRepertoire)
    
'-> mouchard
Print #hdlMouchard, "Parcours du repertoire : " & aRepInit.Path
    
'-> on converti, les fichiers du repertoire en cours
ConvertRepertoire aRepInit, ToBin

'-> On boucle sur les sous repertoires en cours
For Each aSousRep In aRepInit.SubFolders
    Call ParcoursRepertoire(aSousRep.Path, ToBin)
Next

'-> reinitialiser les objet

End Sub

Private Sub ConvertRepertoire(aRep As Object, ToBin As Boolean)
'--> cette procedure va convertir les binaires du repertoire
Dim aFile As Object
Dim BinToXml As Boolean
Dim IsError As Boolean
Me.Tag = Me.Caption

'-> on exclu les repertoires menu
If InStr(1, aRep, "menu", vbTextCompare) <> 0 Then
    '-> mouchard
    Print #hdlMouchard, "Repertoire exclu de la conversion"
    Exit Sub
End If

For Each aFile In aRep.Files
    '-> on teste l'extension pour convertir que les bin
    Me.Caption = aFile.Path
    Me.Refresh
    DoEvents
    If UCase(Mid(aFile.Name, Len(aFile.Name) - 3, 4)) = ".BIN" And aFile.Size <> 0 Then
        '-> Lancer le traitement en fonction de la nature du bin
        If ToBin Then
            '-> mouchard
            Print #hdlMouchard, aFile.Name
            '-> on verifie le format du fichier
            If isXml(aFile.Path) Then
                '-> conversion
                ConvertXMLToBin (aFile.Path)
            Else
                '-> mouchard
                Print #hdlMouchard, aFile.Name & " d�ja en binaire"
            End If
        Else
            '-> mouchard
            Print #hdlMouchard, aFile.Name
            '-> on verifie le format du fichier
            If Not isXml(aFile.Path) Then
                '-> conversion
                ConvertBINToXML (aFile.Path)
            Else
                '-> mouchard
                Print #hdlMouchard, aFile.Name & " d�ja en texte"
            End If
        End If
    End If 'Si on doit traiter ou non le fichier
Next

Me.Caption = Me.Tag

'-> on vide les objets
Set aFile = Nothing
End Sub

Private Function isXml(aFilePath As String) As Boolean
'--> on verifie le format du chier
Dim hdlFic As Integer
Dim Ligne As String

'-> Ouvrir le fichier binaire source
hdlFic = FreeFile
Open aFilePath For Input As #hdlFic
Line Input #hdlFic, Ligne

'-> on teste la premiere chaine
If Mid(Ligne, 1, 1) = "<" Then
    isXml = True
Else
    '-> si c'est une fiche
    If Len(Ligne) > 20 Then
        If Mid(Ligne, 1, 15) = "/**************" Then isXml = True
    Else
        isXml = False
    End If
End If

'-> on ferme le fichier
Close #hdlFic

End Function

Private Sub ConvertXMLToBin(aFilePath As String)
'---> Cette proc�dure convertit un fichier fichier XML en binaire
Dim Ligne As String
Dim HdlFile As Integer
Dim hdlBin As Integer
Dim hdlXml As Integer
Dim TempFileName As String
Dim ValueEnreg As Integer
Dim LenEnreg As Integer
Dim ValueBalise As String
Dim IsProc As Boolean
Dim strKeyWord As String
Dim NewKeyWord As String
Dim SaveDictionnary As String
Dim SavedKeyWord As String
Dim isInField As Boolean

On Error GoTo GestError

'-> R�cup�rer un fichier temporaire
TempFileName = GetTempFileNameVB("BIN")

'-> Ouvrir le fichier temporaire de destination
hdlBin = FreeFile
Open TempFileName For Binary As #hdlBin

'-> Ouvrir le fichier binaire source
hdlXml = FreeFile
Open aFilePath For Input As #hdlXml

'-> Lecture s�quentielle du buffer
Do While Not EOF(hdlXml)
    '-> Lecture de la ligne
    Line Input #hdlXml, Ligne
    Ligne = Replace(Ligne, Chr(9), "")
    Ligne = Replace(Ligne, Chr(13), "")
    Ligne = Replace(Ligne, Chr(10), "")
    '-> Selon la ligne
    Select Case UCase$(Trim(Entry(1, Ligne, ">")))
        Case "<KEYWORD/"
            If IsProc Then
                '-> Enregistrer le mot cl�
                SaveBinValue hdlBin, 2, UCase$(Trim(Entry(2, Ligne, ">")))
            Else
                '-> Enregistrer le mot cl�
                SaveBinValue hdlBin, 1000, UCase$(Trim(Entry(2, Ligne, ">")))
            End If
            '-> Mettre le mot cl� de cot� pour la gestion des extents
            SavedKeyWord = UCase$(Trim(Entry(2, Ligne, ">")))
        Case "<DATA-TYPE/"
            '-> Enregistrer son Data Type
            SaveBinValue hdlBin, 1200, UCase$(Trim(Entry(2, Ligne, ">")))
        Case "<CADRAGE/"
            '-> Enregistr� le Cadrage
            SaveBinValue hdlBin, 1201, UCase$(Entry(2, Ligne, ">"))
        Case "<GET/"
            '-> Enregistrer sa fonction GET
            SaveBinValue hdlBin, 1202, Trim(Entry(2, Ligne, ">"))
        Case "<F10/"
            '-> Enregistrer sa fonction F10
            SaveBinValue hdlBin, 1203, Trim(Entry(2, Ligne, ">"))
        Case "<DYNACCESS/"
            '-> Enregistrer sa fonction access dynamique
            SaveBinValue hdlBin, 1204, Trim(Entry(2, Ligne, ">"))
        Case "<LABEL/"
            '-> Enregistrer le mot cl�
            If IsProc Then
                SaveBinValue hdlBin, 3, UCase$(Trim(Entry(2, Ligne, ">")))
            Else
                SaveBinValue hdlBin, 1301, UCase$(Trim(Entry(2, Ligne, ">")))
            End If
        Case "<FIELD/"
            '-> Enregistr� le Dictionnaire
            SaveBinValue hdlBin, 1302, Trim(Entry(2, Ligne, ">"))
        Case "<PROCEDURE"
            IsProc = True
            '-> Cr�ation d'une nouvelle proc�dure
            SaveBinValue hdlBin, 1, ""
        Case "<TITLE/"
            SaveBinValue hdlBin, 26, Entry(2, Trim(Ligne), ">")
        Case "<LOCK/"
            SaveBinValue hdlBin, 4, Entry(2, Trim(Ligne), ">")
        Case "<ROWID/"
            SaveBinValue hdlBin, 5, Entry(2, Trim(Ligne), ">")
        Case "<NAVIGA/"
            SaveBinValue hdlBin, 6, Entry(2, Trim(Ligne), ">")
        Case "<USEINDEX/"
            SaveBinValue hdlBin, 7, "1"  'On utilise un index
        Case "<INDEX"
             '-> G�n�rer un balise d'ouverture d'un index
            SaveBinValue hdlBin, 9, ""
            MoveXmlInputFieldToBin hdlXml, hdlBin
        '**** INPUT *****
        Case "<INPUT"
            SaveBinValue hdlBin, 8, ""
            MoveXmlInputFieldToBin hdlXml, hdlBin
        '***** OUTPUT *****
        Case "<OUTPUT"
            SaveBinValue hdlBin, 19, ""
            MoveXmlOutputFieldToBin hdlXml, hdlBin
        '***** FIN PROCEDURE *****
        Case "</PROCEDURE"
            SaveBinValue hdlBin, 24, ""
        Case "<DICTIONNARY/"
            '-> Enregistr� le Dictionnaire
            SaveBinValue hdlBin, 1100, UCase$(Trim(Entry(2, Ligne, ">")))
            '-> Mettre le dictionnaire de cot� pour la gestion des extents
            SaveDictionnary = UCase$(Trim(Entry(2, Ligne, ">")))
        Case "<SIDE-LABEL/"
            '-> Enregistrer so libelle long
            SaveBinValue hdlBin, 1101, Trim(Entry(2, Ligne, ">"))
        Case "<COLUMN-LABEL/"
            '-> Enregistrer son libelle court
            SaveBinValue hdlBin, 1102, Trim(Entry(2, Ligne, ">"))
        Case "<RETURN-VALUE/"
            '-> Enregistrer son libelle court
            SaveBinValue hdlBin, 1103, Trim(Entry(2, Ligne, ">"))
        Case "<MESSPROG/"
            '-> Enregistr� le Dictionnaire
            SaveBinValue hdlBin, 500, Trim(Entry(2, Ligne, ">"))
        Case Else
            '-> Gestion des extents
            If InStr(1, UCase$(Ligne), "<LABEL_") <> 0 Then
                '-> Enregistrer le mot cl� et r�cup�rer l'extent associ�
                SaveBinValue hdlBin, 1000, SavedKeyWord & "_" & Entry(1, Entry(2, Ligne, "_"), "/")
                '-> Enregistrer le dictionnaire affect�
                SaveBinValue hdlBin, 1100, SaveDictionnary
            End If 'Si on est sur un Extent de label
    End Select
Loop 'Pour tout le buffer

'-> Fermer les 2 fichiers
Close #hdlXml
Close #hdlBin

'-> supprimer le fichier d'origine
Kill aFilePath
FileCopy TempFileName, aFilePath
'-> supprimer le fichier tempo
Kill TempFileName

Exit Sub

'-> gestion des erreurs
GestError:

'-> mouchard
Print #hdlMouchard, "Erreur : " & aFilePath & " " & Err.Description

End Sub

Private Sub ConvertBINToXML(aFilePath As String)
'---> Cette proc�dure convertit un fichier bianire en fichier XML

Dim hdlBin As Integer
Dim hdlXml As Integer
Dim TempFileName As String
Dim ValueEnreg As Integer
Dim LenEnreg As Integer
Dim ValueBalise As String
Dim IsIndex As Boolean
Dim IsDictionnaryPrinted As Boolean
Dim isInField As Boolean
Dim SetForIndex As Boolean
Dim NbTab As Integer

On Error GoTo GestError

'-> R�cup�rer un fichier temporaire
TempFileName = GetTempFileNameVB("BIN")

'-> Ouvrir le fichier temporaire de destination
hdlXml = FreeFile
Open TempFileName For Output As #hdlXml

'-> Ouvrir le fichier binaire source
hdlBin = FreeFile
Open aFilePath For Binary As #hdlBin

'-> Lecture du fichier binaire
'-> Lecture s�quentielle
Do While Not EOF(hdlBin)
    '-> Lecture du code enregistrement
    Get #hdlBin, , ValueEnreg
    '-> Lecture de sa taille
    Get #hdlBin, , LenEnreg
    '-> Selon la valeur de l'enreg
    Select Case ValueEnreg
        '**** HEADER ****
        Case 1001, 1002, 1003
            Call GetAsciiByte(hdlBin, LenEnreg)
        Case 25 'Label de la fonction
            Call GetAsciiByte(hdlBin, LenEnreg)
        Case 26 'Theme associ� � la fonction
            Call GetAsciiByte(hdlBin, LenEnreg)
        Case 1 'New Proc
            Print #hdlXml, "<PROCEDURE>"
        Case 2 'KeyWord
            Print #hdlXml, Chr(9) & "<KEYWORD/>" & GetAsciiByte(hdlBin, LenEnreg)
        Case 3 'Label
            Print #hdlXml, Chr(9) & "<LABEL/>" & GetAsciiByte(hdlBin, LenEnreg)
        Case 4 'Lock
            Print #hdlXml, Chr(9) & "<LOCK/>" & GetAsciiByte(hdlBin, LenEnreg)
        Case 5 'Rowid
            Print #hdlXml, Chr(9) & "<ROWID/>" & GetAsciiByte(hdlBin, LenEnreg)
        Case 6 'Naviga
            Print #hdlXml, Chr(9) & "<NAVIGA/>" & GetAsciiByte(hdlBin, LenEnreg)
        Case 7 'Si on utilise des index
            '-> Indiquer que l'on utilise une balise Index
            IsIndex = True
            '-> Poser la balise dans le fichier
            Print #hdlXml, Chr(9) & "<USEINDEX/>1"
            '-> Lire la valeur m�me si pas utilis�e
            Call GetAsciiByte(hdlBin, LenEnreg)
        Case 9 'Ouverture d'un Index
            Print #hdlXml, Chr(9) & "<INDEX>"
            '-> Lancer l'analyse des champs des index
            SaveProcedureInputFieldsClassFromBinFileToXmlFile hdlXml, hdlBin, True
        Case 10 'Keyword Index
            Print #hdlXml, Chr(9) & Chr(9) & "<KEYWORD/>" & GetAsciiByte(hdlBin, LenEnreg)
        Case 11 'Label Index
            Print #hdlXml, Chr(9) & Chr(9) & "<LABEL/>" & GetAsciiByte(hdlBin, LenEnreg)
        Case 12 'New Field
            If isInField Then Print #hdlXml, String(NbTab, Chr(9)) & "</FIELD>"
            Print #hdlXml, String(NbTab, Chr(9)) & "<FIELD>"
            isInField = True
        Case 13 'Keyword Field
            Print #hdlXml, String(NbTab + 1, Chr(9)) & "<KEYWORD/>" & GetAsciiByte(hdlBin, LenEnreg)
        Case 14 'Multi
            Print #hdlXml, String(NbTab + 1, Chr(9)) & "<MULTI/>" & GetAsciiByte(hdlBin, LenEnreg)
        Case 15 'Defaut
            Print #hdlXml, String(NbTab + 1, Chr(9)) & "<DEFAULT/>" & GetAsciiByte(hdlBin, LenEnreg)
'            Print #HdlXML, String(NbTab, Chr(9)) & "</FIELD>"
        Case 16 'Obli
            Print #hdlXml, String(NbTab + 1, Chr(9)) & "<OBLI/>1" '& GetAsciiByte(hdlbin, LenEnreg)
        Case 17 'Fin Index
            If isInField Then
                Print #hdlXml, String(NbTab, Chr(9)) & "</FIELD>"
                isInField = False
            End If
            Print #hdlXml, Chr(9) & "</INDEX>"
        Case 18 'Fin input
            '-> Tester si on doit poser la balise de fin Input
            If isInField Then
                Print #hdlXml, String(NbTab, Chr(9)) & "</FIELD>"
                isInField = False
            End If
            NbTab = 2
            If Not SetForIndex Then Print #hdlXml, Chr(9) & "</INPUT>"
        Case 19 'Output
            Print #hdlXml, Chr(9) & "<OUTPUT>"
            NbTab = 3
            '-> Lancer l'analyse des champs en output
            SaveProcedureOutputFieldsClassFromBinFileToXmlFile hdlXml, hdlBin
            Print #hdlXml, Chr(9) & "</OUTPUT>"
        Case 24 'Fin de proc�dure
            Print #hdlXml, "</PROCEDURE>"
        Case 500 'MESSAGE
            '-> R�cup�rer sa valeur
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
            '-> Enregistrer la cl� dans le fichier XML
            Print #hdlXml, "<MESSPROG/>" & ValueBalise
        Case 1000 'KEYWORD
            '-> R�cup�rer sa valeur
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
            '-> Tester s'il ya une extentsion ou non
            If InStr(1, ValueBalise, "_") <> 0 Then
                '-> Enregistrer la cl� avec Extend
                Print #hdlXml, "<LABEL_" & Entry(2, ValueBalise, "_") & "/>"
            Else
                '-> Enregistrer la cl� dans le fichier XML
                Print #hdlXml, "<KEYWORD/>" & ValueBalise
            End If
        Case 1001
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
        Case 1002
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
        Case 1003
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
        Case 1100 'DICTIONNARY
            '-> R�cup�rer sa valeur
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
            If Not IsDictionnaryPrinted Then
                '-> Enregistrer la cl� dans le fichier XML
                Print #hdlXml, "<DICTIONNARY/>" & ValueBalise
                '-> Indiquer que l'on a imprimer un dictionnaire
                IsDictionnaryPrinted = True
            End If
        Case 1101 'SIDE-LABEL
            '-> R�cup�rer sa valeur
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
            '-> Enregistrer la cl� dans le fichier XML
            Print #hdlXml, "<SIDE-LABEL/>" & ValueBalise
        Case 1102 'COLUMN-LABEL
            '-> R�cup�rer sa valeur
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
            '-> Enregistrer la cl� dans le fichier XML
            Print #hdlXml, "<COLUMN-LABEL/>" & ValueBalise
         Case 1103 'RETURN-VALUE
            '-> R�cup�rer sa valeur
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
            '-> Enregistrer la cl� dans le fichier XML
            Print #hdlXml, "<RETURN-VALUE/>" & ValueBalise
        Case 1200 'DATA-TYPE
            '-> R�cup�rer sa valeur
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
            '-> Enregistrer la cl� dans le fichier XML
            Print #hdlXml, "<DATA-TYPE/>" & ValueBalise
        Case 1201 'CADRAGE
            '-> R�cup�rer sa valeur
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
            '-> Enregistrer la cl� dans le fichier XML
            Print #hdlXml, "<CADRAGE/>" & ValueBalise
        Case 1202 'FONCTION GET
            '-> R�cup�rer sa valeur
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
            '-> Enregistrer la cl� dans le fichier XML
            Print #hdlXml, "<GET/>" & ValueBalise
        Case 1203 'FONCTION F10
            '-> R�cup�rer sa valeur
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
            '-> Enregistrer la cl� dans le fichier XML
            Print #hdlXml, "<F10/>" & ValueBalise
        Case 1204 'FONCTION DYNACCESS
            '-> R�cup�rer sa valeur
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
            '-> Enregistrer la cl� dans le fichier XML
            Print #hdlXml, "<DYNACCESS/>" & ValueBalise
        Case 1301 'LABEL
            '-> R�cup�rer sa valeur
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
            '-> Enregistrer la cl� dans le fichier XML
            Print #hdlXml, "<LABEL/>" & ValueBalise
        Case 1302 'FIELD
            '-> R�cup�rer sa valeur
            ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
            '-> Enregistrer la cl� dans le fichier XML
            Print #hdlXml, "<FIELD/>" & ValueBalise
        Case 1401
            '-> Header
            Print #hdlXml, "<HEADER>"
        Case 1402
            Print #hdlXml, "</HEADER>"
        Case 1403
            Print #hdlXml, "<PROGRAM>"
        Case 1404
            Print #hdlXml, Chr(9) & "<IN/>" & ValueBalise
        Case 1405
            Print #hdlXml, Chr(9) & "<OUT/>" & ValueBalise
        Case 1406
            Print #hdlXml, Chr(9) & "<TRT/>" & ValueBalise
        Case 1424
            Print #hdlXml, Chr(9) & "<LAST/>" & ValueBalise
        Case 1407
            Print #hdlXml, "</PROGRAM>"
        Case 1408
            Print #hdlXml, "<COMBO>"
        Case 1423
            Print #hdlXml, Chr(9) & "<KEYWORDCOMBO/>" & ValueBalise
        Case 1409
            Print #hdlXml, Chr(9) & "<OPTION/>" & ValueBalise
        Case 1410
            Print #hdlXml, "</COMBO>"
        Case 8
            Print #hdlXml, Chr(9) & "<INPUT>"
            isInField = False
            NbTab = 2
        Case 12
            Print #hdlXml, Chr(9) & "<FIELD>"
        Case 1411
            Print #hdlXml, Chr(9) & Chr(9) & "<FIELDKEYWORD/>" & ValueBalise
        Case 1412
            Print #hdlXml, Chr(9) & Chr(9) & "<HIDDEN/>" & ValueBalise
        Case 1413
            Print #hdlXml, Chr(9) & Chr(9) & "<TYPSEL/>" & ValueBalise
        Case 1414
            Print #hdlXml, Chr(9) & Chr(9) & "<NBSEL/>" & ValueBalise
        Case 16
            Print #hdlXml, Chr(9) & Chr(9) & "<OBLI/>" & ValueBalise
        Case 1415
            Print #hdlXml, Chr(9) & Chr(9) & "<GROUPE/>" & ValueBalise
        Case 14
            Print #hdlXml, Chr(9) & Chr(9) & "<DEFAULT/>" & ValueBalise
        Case 1416
            Print #hdlXml, Chr(9) & "</FIELD>"
        Case 1417
            Print #hdlXml, "<EXTEND>"
        Case 1418
            Print #hdlXml, Chr(9) & "<REF/>" & ValueBalise
        Case 1419
            Print #hdlXml, "</EXTEND>"
        Case 1420
            Print #hdlXml, "<ORDER>"
        Case 1421
            Print #hdlXml, Chr(9) & "<FIELDORDER/>" & ValueBalise
        Case 1422
            Print #hdlXml, "</ORDER>"
        Case 18
            Print #hdlXml, "</INPUT>"
        Case 1425
            Print #hdlXml, "<MAQLISTE>"
        Case 1426
            Print #hdlXml, Chr(9) & "<MAQTYPE/>" & ValueBalise
        Case 1427
            Print #hdlXml, Chr(9) & "<MAQDEF/>" & ValueBalise
        Case 1428
            Print #hdlXml, Chr(9) & "<MAQPROG/>" & ValueBalise
        Case 1429
            Print #hdlXml, "</MAQLISTE>"
        Case Else
            'ValueBalise = GetAsciiByte(hdlBin, LenEnreg)
    End Select
Loop 'Pour tout le fichier

'-> Fermer les 2 fichiers
Close #hdlXml
Close #hdlBin

'-> supprimer le fichier d'origine
Kill aFilePath
FileCopy TempFileName, aFilePath
'-> supprimer le fichier tempo
Kill TempFileName

'-> Quitter la fonction
Exit Sub

GestError:
    '-> Fermer le fichier xml
    If hdlXml <> 0 Then Close #hdlXml
    '-> Fermer le fichier binaire
    If hdlBin <> 0 Then Close #hdlBin
    '-> mouchard
    Print #hdlMouchard, "Erreur : " & aFilePath & " " & Err.Description

End Sub

Private Sub CmdConvertToText_Click()

'-> on initialise le rapport du convert
Mouchard = GetTempFileNameVB("DBG")
hdlMouchard = FreeFile
Open Mouchard For Output As #hdlMouchard

'-> mouchard
Print #hdlMouchard, "Lancement de la conversion � : " & Now & " vers format texte"

'-> On lance la procedure de conversion des repertoires et sous repertoires
Call ParcoursRepertoire(Me.lblCours.Tag, False)

'-> mouchard
Print #hdlMouchard, "Fin de la conversion � : " & Now

Close #hdlMouchard

'-> ouvrir le mouchard
ShellExecute Me.hwnd, "Open", Mouchard, vbNullString, App.Path, 1

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
If KeyAscii = 27 Then Call cmdCancel_Click
End Sub

Private Sub Form_Load()

'-> Init du skin
Me.DogSkinObject1.Initialisation False, "", pMinMaxClose, True

End Sub

Private Sub Form_Resize()

Dim aRect As RECT

On Error Resume Next

'-> Bloquer la mise � jour de la feuille
LockWindowUpdate Me.hwnd

'-> R�cup�rer la taille de la zone cliente
GetClientRect Me.hwnd, aRect

'-> Positionner le fond blanc
Me.Shape1.Width = aRect.Right - 31
Me.Shape1.Height = aRect.Bottom - 97

'-> Liste des fichiers
Me.ListView2.Width = aRect.Right - 87
Me.ListView2.Height = aRect.Bottom - 201

'-> Placer le bouton annuler
Me.cmdCancel.Top = aRect.Bottom - 47
Me.cmdCancel.Left = Me.ScaleWidth - 146

'-> Placer le bouton Ok
Me.cmdOk.Left = Me.cmdCancel.Left - 132
Me.cmdOk.Top = Me.cmdCancel.Top
Me.CmdConvertToBin.Top = Me.cmdOk.Top
Me.CmdConvertToText.Top = Me.cmdOk.Top

'-> Pr�c�dent
Me.imgProp.Left = aRect.Right - 296
'-> Poste de travail
Me.imgCurrentPath.Left = Me.imgProp.Left + 32
'-> Path
Me.imgPrefolder.Left = Me.imgCurrentPath.Left + 32
'-> Information
Me.imgNewFolder.Left = Me.imgPrefolder.Left + 32
'-> Create
Me.imgSearch.Left = Me.imgNewFolder.Left + 32
'-> Search
Me.imgPosteTravail.Left = Me.imgSearch.Left + 32
'-> Affichage
Me.imgAffichage.Left = Me.imgPosteTravail.Left + 32

'-> Lib�rer la mise � jour de la feuille
LockWindowUpdate 0

'-> Lib�rer la CPU
DoEvents

End Sub


Private Sub imgCurrentPath_Click()

'-> Ne rien faire si pas d'icone en cours
If Me.ListView2.SelectedItem Is Nothing Then Exit Sub

'-> Afficher le path en cours
DisplayMessage "Emplacement en cours : " & Chr(13) & Me.lblCours.Tag, 3, 1, "Emplacement en cours"

End Sub

Private Sub imgNewFolder_Click()

'-> Demander le nom du nouveau dossier
Call CreateNewFolder

End Sub

Private Sub imgPosteTravail_Click()
'-> Afficher les disques
LoadDrive
End Sub

Private Sub imgPrefolder_Click()

Dim NewFilename As String
Dim i As Integer

'-> Ne rien faire si on est au niveau du disque
If NiveauDisque Then Exit Sub

'-> Supprimer le dernier slash et remonter d'un niveau
NewFilename = Me.lblCours.Tag
If NumEntries(NewFilename, "\") = 2 Then
    '-> Afficher les disques
    LoadDrive
Else
    '-> Cr�er le path � afficher
    NewFilename = Mid$(Me.lblCours.Tag, 1, Len(Me.lblCours.Tag) - 1)
    i = InStrRev(NewFilename, "\")
    NewFilename = Mid$(NewFilename, 1, i - 1)
    '-> Afficher le nouveau path
    DisplayFileObject NewFilename, Extension
End If


End Sub

Private Sub imgProp_Click()

If Me.ListView2.SelectedItem Is Nothing Then Exit Sub
ShowFileProperties Entry(2, Me.ListView2.SelectedItem.Key, "|"), Me.hwnd

End Sub

Private Sub imgSearch_Click()

Dim DateMini As String
Dim DateMaxi As String

'-> Chaine de retour : FICHIER|DATEMINI|DATEMAXI

'-> Ne rien faire si on est au niveau disque
If NiveauDisque Then Exit Sub

'-> rafraichir
Me.Refresh

'-> Analyse du retour
If strRetour = "" Then Exit Sub

'-> Lancer l'analyse du disque
DisplayFileObject Me.lblCours.Tag, Entry(1, strRetour, "|"), True, True, Entry(2, strRetour, "|"), Entry(3, strRetour, "|"), Entry(4, strRetour, "|")

'-> Vider la variable d'�change
strRetour = ""

End Sub

Private Sub cmdCancel_Click()
strRetour = ""
Unload Me
End Sub

Private Sub cmdOk_Click()

Dim Titre As String
Dim Rep As String

'-> Tester si on est en mode r�pertoire MESSPROG
If DisplayOnlyDirectory Then
    If NiveauDisque Then
        DisplayMessage "Veuillez s�lectionner un r�pertoire", 3, 1, "Erreur"
        Exit Sub
    End If
    '-> Retourner le r�pertoire s�lectionn�
    strRetour = Me.lblCours.Tag
    Unload Me
    Exit Sub
End If

'-> Selon le mode d'ouverture
If ModeMaj = 0 Then
    '-> Titre
    Titre = "Impossible d'ouvrir"
    '-> D�charger la feuille
    If ModeClose = 0 Then
        Unload Me
    End If
Else 'Enregistrement
    '-> Titre
    Titre = "Impossible d'enregistrer"
    '-> Ne rien faire si on est au niveau disque
    If NiveauDisque Then
        DisplayMessage "Veuillez s�lectionner un r�pertoire", 3, 1, "Impossible d'enregistrer"
        Exit Sub
    End If
    Unload Me
End If

Exit Sub

BadFile:
    DisplayMessage "Fichier incorrecte", 3, 1, Titre
    

End Sub


Private Sub LoadDrive()


'-> Charger dans le tree la le poste de travail et les disques connect�s


Dim aIt As ListItem
Dim aDisk As String
Dim Res As Long
Dim lpBuffer As String
Dim i As Integer

Dim DiskInfo As String

'-> Vider la liste des items existants
Me.ListView2.ListItems.Clear

'-> Charger la liste des disques connect�s
lpBuffer = Space$(2000)
Res = GetLogicalDriveStrings&(Len(lpBuffer), lpBuffer)
lpBuffer = Mid$(lpBuffer, 1, Res)

'-> On est au niveau disque
NiveauDisque = True

'-> Analyser la liste des diques connect�s
For i = 1 To NumEntries(lpBuffer, Chr(0))
    '-> D�finition du disque
    aDisk = Entry(i, lpBuffer, Chr(0))
    '-> R�cup�ration des informations
    DiskInfo = GetDriveInformations(aDisk)
    '-> Ne rien faire si pas de donn�es
    If Trim(DiskInfo) = "" Then GoTo NextDisk
    '-> Ajouter l'objet dans le listview
    Set aIt = Me.ListView2.ListItems.Add(, "DRIVE|" & Entry(3, DiskInfo, "|"), Entry(2, DiskInfo, "|"), Entry(1, DiskInfo, "|"))
NextDisk:
Next

Me.ListView2.Arrange = lvwAutoTop
Me.ListView2.Refresh

'-> Indiquer que l'on est sur le poste de travail
Me.imgCours.Picture = Me.ImageList2.ListImages("POSTE").Picture

'-> Libell�
Me.lblCours.Tag = "Poste de travail"
Me.lblCours.Caption = "Poste de travail"

End Sub

Private Function GetDriveInformations(aDisk As String) As String

'-> Cette proc�dure retourne le type de disque


Dim TypeDrive As Long
Dim DiskText As String
Dim Res As Long
Dim lpBuffer As String
Dim i As Integer
Dim VolumeName As String
Dim SerialNumber As Long
Dim ComponentLenght As Long
Dim SystemFlags As Long
Dim SystemNameBuffer As String

If Trim(aDisk) = "" Then Exit Function

'-> R�cup�ration du type de lecteur
TypeDrive = GetDriveType(aDisk)
'-> Recup�rer les informations sur le type de disque
VolumeName = Space$(255)
SystemNameBuffer = Space$(250)
Res = GetVolumeInformation(aDisk, VolumeName, Len(VolumeName), SerialNumber, ComponentLenght, SystemFlags, SystemNameBuffer, Len(SystemNameBuffer))

'-> Afficher le nom du disque s'il est renseign�
If Trim(Entry(1, VolumeName, Chr(0))) <> "" Then
    VolumeName = Trim(Entry(1, VolumeName, Chr(0))) & "(" & aDisk & ")|" & aDisk
Else
    Select Case TypeDrive
        Case DRIVE_CDROM 'CDrom
            VolumeName = "Lecteur CD Rom" & Chr(13) & "(" & aDisk & ")|" & aDisk
        Case DRIVE_FIXED 'disque dur
            VolumeName = "Disque local" & Chr(13) & "(" & aDisk & ")|" & aDisk
        Case DRIVE_REMOTE 'Disque r�seau
            VolumeName = "Disque r�seau" & Chr(13) & "(" & aDisk & ")|" & aDisk
        Case DRIVE_REMOVABLE 'Disquette
            VolumeName = "Lecteur de disquette" & Chr(13) & " (" & aDisk & ")|" & aDisk
    End Select

End If

'-> Afficher la bonne icone
Select Case TypeDrive
    Case DRIVE_CDROM 'CDrom
        strRetour = "CDROM|" & VolumeName
    Case DRIVE_FIXED 'disque dur
        strRetour = "DDUR|" & VolumeName
    Case DRIVE_REMOTE 'Disque r�seau
        strRetour = "DNET|" & VolumeName
    Case DRIVE_REMOVABLE 'Disquette
        strRetour = "DISQUETTE|" & VolumeName
End Select

'-> Retourner
GetDriveInformations = strRetour
strRetour = ""

End Function

Private Sub ListView2_DblClick()

'---> Affichage de la liste des r�pertoires associ�s

'-> Ne rien faire si pas d'objet s�lectionn�
If Me.ListView2.SelectedItem Is Nothing Then Exit Sub

'-> Si on est sur un fichier, retourner le fichier
If Me.ListView2.SelectedItem.Tag = "FILE" Then
    Call cmdOk_Click
    Exit Sub
End If

'-> Indiquer le tag en cours
Me.imgCours.Tag = Me.ListView2.SelectedItem.Tag

'-> Afficher le contenu
Call DisplayFileObject(Entry(2, Me.ListView2.SelectedItem.Key, "|"), Extension)

End Sub

Public Sub DisplayFileObject(aPath As String, FindExtentsion As String, Optional HideFolder As Boolean, Optional CheckDate As Boolean, Optional strDateMini As String, Optional strDateMaxi As String, Optional FindText As String)

Dim FileName As String
Dim i As Integer
Dim ItemX As ListItem
Dim MyExtension As String
Dim ToAdd As Boolean
Dim LenFichierFile As Integer
Dim DateMiniOk As Boolean
Dim DateMaxiOk As Boolean
Dim FileDate As String

On Error GoTo GestError

'-> Bloquer l'�cran
Me.Enabled = False
Me.MousePointer = 11

'-> Positionner le path que l'on a modifi�
SavePath = aPath
'-> supprimer l'anti slash
If Right$(aPath, 1) = "\" Then SavePath = Mid$(SavePath, 1, Len(SavePath) - 1)

'-> Rajouter un "\" � la fin si necessaire
If Right(aPath, 1) <> "\" Then aPath = aPath & "\"

'-> Analyse de tous les fichiers
FileName = Dir$(aPath & "*.*", vbDirectory Or vbNormal)

'-> Positionner le path en cours
Me.lblCours.Caption = aPath 'Entry(NumEntries(aPath, "\") - 1, aPath, "\")

'-> Tester si on veut afficher un disque ou non
If NumEntries(aPath, "\") = 2 Then
    '-> Rajouter le \ du disque
    'Me.lblCours.Caption = Me.lblCours.Caption & "\"
    '-> Positionner l'icone � afficher
    Me.imgCours.Picture = Me.ImageList2.ListImages(Entry(1, GetDriveInformations(aPath), "|")).Picture
Else
    Me.imgCours.Picture = Me.ImageList2.ListImages("OPENFOLDER").Picture
End If

'-> Indiquer dans le tag le path en cours
Me.lblCours.Tag = aPath

'-> Vider le listview
Me.ListView2.ListItems.Clear

'-> Si on doit analyser les dates
If CheckDate Then
    '-> Analyse de la date mini
    If Trim(strDateMini) <> "" Then
        '-> Test si la date est OK
        If IsDate(strDateMini) Then
            '-> Formatter la date
            strDateMini = FormatCompareDate(strDateMini)
            '-> Indiquer que l'on travaille sur cette date
            DateMiniOk = True
        End If 'Si la date est valide
    End If
    '-> Analyse de la date maxi
    If Trim(strDateMaxi) <> "" Then
        '-> Test si la date est OK
        If IsDate(strDateMaxi) Then
            '-> Formatter la date
            strDateMaxi = FormatCompareDate(strDateMaxi)
            '-> Indiquer que l'on travaille sur cette date
            DateMaxiOk = True
        End If 'Si la date est valide
    End If
    '-> Si pas date valide, annuler la recherche en date
    If Not DateMiniOk And Not DateMaxiOk Then CheckDate = False
End If 'Si on doit analyser les dates

'-> Analyse des fichiers et r�pertoires
Do While FileName <> ""
    '-> De base on ajoute
    ToAdd = True
    '-> Ne pas afficher le command.com
    If FileName = "." Or FileName = ".." Or Trim(FileName) = "" Then GoTo NextDir
    '-> Tester si c'est un dossier ou un fichier
    If (GetAttr(aPath & FileName) And vbDirectory) = vbDirectory Then
        '-> Ne pas traiter les r�pertoires
        If HideFolder Then GoTo NextDir
        '-> Indiquer que c'est un r�pertoire
        Set ItemX = Me.ListView2.ListItems.Add(, "FOLDER|" & aPath & FileName, FileName, "FOLDER")
        ItemX.Tag = "FOLDER"
        ItemX.SubItems(1) = "a"
    Else
        '-> Analyse selon l'extension
        i = InStrRev(FileName, ".")
        If i <> 0 Then MyExtension = Mid$(FileName, i + 1, Len(FileName) - i)
        '-> Comparer l'extentsion
        If Trim(FindExtentsion) = "" Then
            ToAdd = False
        Else
            If Trim(FindExtentsion) = "*.*" Then
                ToAdd = True
            Else
                '-> Analyse par le debut
                If Trim(Entry(1, FindExtentsion, ".")) <> "*" Then
                    '-> Attention si * dans la premi�re partie
                    If InStr(1, FindExtentsion, "*") <> 0 Then
                        LenFichierFile = Len(Mid$(FindExtentsion, 1, InStr(1, FindExtentsion, "*") - 1))
                    Else
                        '-> Get du d�but
                        LenFichierFile = Len(Entry(1, FindExtentsion, "."))
                    End If
                    '-> Comparer
                    If UCase$(Trim(Mid$(FileName, 1, LenFichierFile))) = UCase$(Trim(Mid$(FindExtentsion, 1, LenFichierFile))) Then
                        ToAdd = True
                    Else
                        ToAdd = False
                    End If
                End If 'Si on doit comparer sur le d�but de la recherche
                
                '-> Comparer l'extentsion que si necessaire
                If Not ToAdd Then GoTo NextDir
                '-> Ne pas comparer l'extension si = "*"
                If Trim(Entry(2, FindExtentsion, ".")) <> "*" Then
                    If Trim(UCase$(Entry(2, FindExtentsion, "."))) = Trim(UCase$(MyExtension)) Then
                        ToAdd = True
                    Else
                        ToAdd = False
                    End If
                End If
            End If
        End If ' Si on doit tester le debut de recherche sur le nom du fichier
        
        '-> Si on doit ajouter le fichier
        If Not ToAdd Then GoTo NextDir
        
        '-> Analyse sur les dates
        If CheckDate Then
            '-> R�cup�rer la date du fichier
            FileDate = FormatCompareDate(FileDateTime(aPath & FileName))
            
            '-> Analyse selon les crit�res de date
            If DateMiniOk Then
                '-> Si la date maxi est rens�ign�e
                If DateMaxiOk Then
                    '-> Compris entre
                    If CDbl(FileDate) <= CDbl(strDateMaxi) And CDbl(FileDate) >= CDbl(strDateMini) Then
                        ToAdd = True
                    Else
                        ToAdd = False
                    End If
                Else
                    '-> Egalite
                    If CDbl(FileDate) = CDbl(strDateMini) Then
                        ToAdd = True
                    Else
                        ToAdd = False
                    End If
                End If 'Si la date maxi est rens�ign�e
            Else
                '-> La date maxi est forc�ment rens�ign�e : �galit�
                If CDbl(FileDate) = CDbl(strDateMaxi) Then ToAdd = True
            End If 'Si la date mini est rens�ign�e
        End If 'Si on doit analyser les dates
        
        '-> Si on doit ajouter le fichier
        If Not ToAdd Then GoTo NextDir
        
        '-> Analyse sur le text � rechercher DIDIER
        If Trim(FindText) <> "" Then
            '-> on lance la recherche du texte
            If FindSearchText(aPath & FileName, FindText) Then
                ToAdd = True
            Else '-> si on a pas trouv� le texte
                ToAdd = False
            End If
        End If
        
        '-> Si on doit ajouter le fichier
        If Not ToAdd Then GoTo NextDir
                
        '-> Afficher  le fichier test sur extension
        Set ItemX = Me.ListView2.ListItems.Add(, "FILE|" & aPath & FileName, FileName, "WINFILE")
        ItemX.Tag = "FILE"
        ItemX.SubItems(1) = "b"
    End If
    
NextDir:
    '-> Analyser lz prochaine entr�e
    FileName = Dir
Loop

'-> Indiquer que l'on n'est pas au niveau disque
NiveauDisque = False

'-> S�lectionner la premi�re icone
If Me.ListView2.ListItems.Count <> 0 Then Set Me.ListView2.SelectedItem = Me.ListView2.ListItems(1)

'-> Arranger la vue
Me.ListView2.Arrange = lvwAutoTop

'-> Quitter la proc�dure
GoTo EndProg

GestError:
    Select Case Err.Number
        Case 52 'Nom au lecteur incorrecte MESSPROG
            DisplayMessage "Impossible d'acc�der � ce disque ou r�pertoire.", 1, 1, "Erreur"
        Case Else
            DisplayMessage "Erreur dans la recherche des fichiers : " & Chr(13) & Err.Number & " - " & Err.Description, 1, 1, "Erreur"
    End Select
            
EndProg:

    Me.Enabled = True
    Me.MousePointer = 0

End Sub

Private Function FindSearchText(FileName As String, TextSearch As String) As Boolean
Dim HdlFile As Integer
Dim x As ListItem
Dim ValueEnreg As Integer
Dim LenEnreg As Integer
Dim StrValueEnreg As String

On Error Resume Next

'-> Ouvrir le fichier
HdlFile = FreeFile
Open FileName For Binary As #HdlFile

'-> Lecture du fichier
Do While Not EOF(HdlFile)
    '-> Lecture du code enregistrement
    Get #HdlFile, , ValueEnreg
    '-> Lecture de la longueur de l'enregistrement
    Get #HdlFile, , LenEnreg
    '-> Lecture de la valeur de l'enregistrement
    StrValueEnreg = GetAsciiByte(HdlFile, LenEnreg)
    '-> on regarde si on a trouv� la chaine
    If InStr(1, Trim(UCase(StrValueEnreg)), Trim(UCase(TextSearch))) <> 0 Then
        '-> valeur de succes
        FindSearchText = True
        GoTo GestError
    End If
Loop 'Pour tout le fichier

GestError:
'-> Fermer le fichier
Close #HdlFile
     
End Function

Private Sub ListView2_ItemClick(ByVal Item As MSComctlLib.ListItem)

'-> Ne rien faire si pas d'objet s�lectionn�
If Me.ListView2.SelectedItem Is Nothing Then Exit Sub

End Sub

Private Sub ListView2_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then ListView2_DblClick

End Sub

Private Sub CreateNewFolder()

'---> Tentative de suppression d'un r�pertoire ou d'un fichier

Dim Rep As String
Dim ItemX As ListItem

'-> Ne rien faire si on est au niveau disque
If NiveauDisque Then Exit Sub

'-> Demander le nom du r�pertoire
Rep = DisplayMessage("Veuillez saisir le nom du nouveau r�pertoire : ", 1, 1, "Cr�ation d'un nouveau r�pertoire", True)
If Rep = "ANNULER" Then Exit Sub

'-> V�rifier qu'il y ait une valeur
If Trim(InputValue) = "" Then
    DisplayMessage "Nom de r�pertoire incorrecte.", 3, 1, "Cr�ation d'un r�pertoire"
    Exit Sub
End If

'-> Enlever un "\" s'il ya en a un
InputValue = Trim(InputValue)
If Right(InputValue, 1) = "\" Then InputValue = Mid$(InputValue, 1, Len(InputValue) - 1)

'-> Essayer de cr�er le r�pertoire
If Not CreateDirectory(InputValue) Then
    DisplayMessage "Erreur lors de la cr�ation du r�pertoire : " & Chr(13) & Me.lblCours.Tag & Trim(InputValue), 1, 1, "Erreur de cr�ation"
    Exit Sub
End If

'-> Afficher le nouveau r�pertoire cr�er
Set ItemX = Me.ListView2.ListItems.Add(, "FILE|" & Me.lblCours.Tag & Trim(InputValue), Trim(InputValue), "FOLDER")
ItemX.Tag = "FOLDER"
ItemX.SubItems(1) = "b"
ItemX.Selected = True
ItemX.EnsureVisible

End Sub

Private Function CreateDirectory(NewDirectory As String) As Boolean

On Error GoTo GestError

'-> Essayer de cr�er le r�pertoire
MkDir Me.lblCours.Tag & NewDirectory

'-> renvoyer une valeur de succ�s
CreateDirectory = True

Exit Function

GestError:


End Function

