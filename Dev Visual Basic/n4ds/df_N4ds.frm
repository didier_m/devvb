VERSION 5.00
Object = "{DAFD85F1-343B-47EE-8170-1C26723F7A5D}#2.0#0"; "dogskin.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{11746B78-BAE4-4458-BA0E-98F1D1917EFA}#4.0#0"; "dealgrid.ocx"
Begin VB.Form df_N4ds 
   BorderStyle     =   0  'None
   Caption         =   "Deal Informatique"
   ClientHeight    =   11505
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   15960
   Icon            =   "df_N4ds.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   767
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1064
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   6600
      TabIndex        =   5
      Top             =   120
      Visible         =   0   'False
      Width           =   1455
      Begin VB.Label ToolTip 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label2"
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   0
         TabIndex        =   6
         Top             =   0
         Width           =   1455
      End
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   4560
      Picture         =   "df_N4ds.frx":058A
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   2
      Top             =   6840
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   3720
      Picture         =   "df_N4ds.frx":1254
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   1
      Top             =   6720
      Visible         =   0   'False
      Width           =   480
   End
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   10275
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   18124
      _Version        =   393217
      Indentation     =   26
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      BorderStyle     =   1
      Appearance      =   0
      MousePointer    =   99
      OLEDragMode     =   1
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   11760
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   21
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":155E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":2238
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":3F12
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":4BEC
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":58C6
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":65A0
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":727A
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":7F54
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":882E
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":A508
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":ADE2
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":BABC
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":C396
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":CC70
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":D94A
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":E624
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":EEFE
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":10BD8
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":128B2
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":180A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_N4ds.frx":19D7E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin DogSkin.DealPopupMenu DealPopupMenu1 
      Left            =   2400
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
   End
   Begin DealGridProject.DealGrid DealGrid1 
      Height          =   10095
      Left            =   7200
      TabIndex        =   3
      Top             =   600
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   17806
      FixedRows       =   1
      FixedCols       =   2
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorFixed  =   16637889
      BackColor       =   16777215
      BackColorBkg    =   16777215
      BackColorSel    =   16777215
      GridLineColor   =   0
      ForeColor       =   13078868
      ForeColorFixed  =   0
      ForeColorSel    =   16777215
      AllowUserResizing=   0
      ScrollBars      =   2
      SelectionMode   =   0
      MergeCells      =   0
      WordWrap        =   0   'False
      Enabled         =   -1  'True
      AutoSize        =   0   'False
      Col             =   2
      Row             =   0
      Cols            =   5
      Rows            =   1
      Text            =   ""
      ColWidth        =   1995
      RowHeight       =   315
      CellBackColor   =   0
      CellForeColor   =   0
      Saisie          =   0
      FullRowSelect   =   0   'False
      ColValidateRow  =   -1
      SetForDataBase  =   0   'False
      DogMatriceColonne=   ""
      SetLargeurCol   =   "2160|3375|1995|0|255"
   End
   Begin DogSkin.DogSkinObject DogSkinObject1 
      Left            =   0
      Top             =   0
      _ExtentX        =   979
      _ExtentY        =   953
      BorderColor     =   10053171
      BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderForeColor =   10053171
      HeaderBackColor =   16637889
      HeaderLineColor =   15847345
      HeaderUseLine   =   -1  'True
      BeginProperty BodyFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BodyForeColor   =   10053171
      BodyBackColor   =   16579059
      HeaderIco       =   "df_N4ds.frx":1BA58
      HeaderIcoNa     =   "df_N4ds.frx":1C0DA
      ShadowColor     =   12615680
   End
   Begin VB.Image DealCmdButton6 
      Height          =   480
      Left            =   12000
      MousePointer    =   99  'Custom
      Picture         =   "df_N4ds.frx":1C75C
      ToolTipText     =   "Validation des donn�es saisies dans le tableau"
      Top             =   10920
      Width           =   480
   End
   Begin VB.Image DealCmdButton1 
      Height          =   480
      Left            =   11400
      MousePointer    =   99  'Custom
      Picture         =   "df_N4ds.frx":1D026
      ToolTipText     =   "Ajouter une section ou une structure selon le positionnement (F9)"
      Top             =   10920
      Width           =   480
   End
   Begin VB.Image Image2 
      Height          =   480
      Left            =   7920
      MousePointer    =   99  'Custom
      Picture         =   "df_N4ds.frx":1D8F0
      ToolTipText     =   "Ouvre un nouveau fichier"
      Top             =   10920
      Width           =   480
   End
   Begin VB.Image Image3 
      Height          =   480
      Left            =   8520
      MousePointer    =   99  'Custom
      Picture         =   "df_N4ds.frx":1E1BA
      ToolTipText     =   "Annule toutes les modifications et r�ouvre le fichier actuel"
      Top             =   10920
      Width           =   480
   End
   Begin VB.Image DealCmdButton4 
      Height          =   480
      Left            =   9480
      MousePointer    =   99  'Custom
      Picture         =   "df_N4ds.frx":1EA84
      ToolTipText     =   "Exportation des donn�es"
      Top             =   10920
      Width           =   480
   End
   Begin VB.Image DealCmdButton3 
      Height          =   480
      Left            =   10800
      MousePointer    =   99  'Custom
      Picture         =   "df_N4ds.frx":1F34E
      ToolTipText     =   "Recherche d'un enregistrement (Ctrl+F) Recherche suivante (F3)"
      Top             =   10920
      Width           =   480
   End
   Begin VB.Image DealCmdButton2 
      Height          =   480
      Left            =   10200
      MousePointer    =   99  'Custom
      Picture         =   "df_N4ds.frx":1FC18
      ToolTipText     =   "Suppression d'un ou plusieurs enregistrements selon que l'on est positionn� sur une section ou un enregistrement (<-)"
      Top             =   10920
      Width           =   480
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   7320
      MousePointer    =   99  'Custom
      Picture         =   "df_N4ds.frx":204E2
      ToolTipText     =   "Reg�n�re le fichier"
      Top             =   10920
      Width           =   480
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   690
      Width           =   6615
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FEF3E9&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      FillColor       =   &H00FFFFFF&
      FillStyle       =   2  'Horizontal Line
      Height          =   375
      Left            =   120
      Top             =   600
      Width           =   6855
   End
End
Attribute VB_Name = "df_N4ds"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Public haveFocus As String

Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare Function TerminateProcess Lib "kernel32" (ByVal hProcess As Long, ByVal uExitCode As Long) As Long
Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, ByRef lpdwProcessId As Long) As Long
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Private Const SYNCHRONIZE = &H100000
Private Const PROCESS_TERMINATE As Long = &H1

Public Sub TerminateApp(strProg As String)
Dim target_hwnd As Long
Dim target_process_id As Long
Dim target_process_handle As Long

    '-> on pointe sur la fenetre.
    target_hwnd = FindWindow(vbNullString, strProg)
    If target_hwnd = 0 Then
        Exit Sub
    End If

    '-> on reccupere l'ID.
    GetWindowThreadProcessId target_hwnd, target_process_id
    If target_process_id = 0 Then
        Exit Sub
    End If

    ' ouvrir le process.
    target_process_handle = OpenProcess(SYNCHRONIZE Or PROCESS_TERMINATE, ByVal 0&, target_process_id)
    If target_process_handle = 0 Then
        Exit Sub
    End If

    '-> terminer process.
    TerminateProcess target_process_handle, 0&

    '-> fermer le process.
    CloseHandle target_process_handle
End Sub

Private Sub KillApp(strProg As String)
'-> cette proc�dure ferme les programes en cours
Dim Ret As String
Dim hwnd As Long

'-> Pour r�cup�rer le hWnd d'une application dont on connait le nom
'nom EXACT de la fen�tre (gestionnaire des t�ches pour l'obtenir)
hwnd = FindWindow(vbNullString, strProg)
If hwnd <> 0 Then PostMessage hwnd, 16, 0&, 0&    'fin de t�che sur l'application

End Sub

Public Sub init()
'-> on ouvre la fen�tre de selection de fichier le fichier en retour est stock� dans strretour
Dim appDir As String

Me.Show
df_Drloading.Show
df_Drloading.Label1.Caption = "Chargement..."
appDir = GetIniFileValue("PARAM", "OpenDir", searchExe("N4ds.ini"))
If (GetIniFileValue("PARAM", "SaveInTDS", searchExe("N4ds.ini")) = "OUI") Then
    SaveInTDS = True
Else
    SaveInTDS = False
End If
If appDir <> "" Then
    If InStr(1, appDir, "%", vbTextCompare) <> 0 Then
        'MsgBox appDir & GetVariableEnv(Entry(2, appDir, "%"))
        appDir = Entry(1, appDir, "%") & GetVariableEnv(Entry(2, appDir, "%")) & Entry(3, appDir, "%")
    End If
    df_OpenFile.init 0, appDir, "Choix du fichier � importer", "*.txt"
Else
    df_OpenFile.init 0, App.Path, "Choix du fichier � importer", "*.txt"
End If
If GetIniFileValue("PARAM", "OpenDirHide", searchExe("N4ds.ini")) = "1" Then
    df_OpenFile.imgProp.Visible = False
    df_OpenFile.imgNewFolder.Visible = False
    df_OpenFile.imgPosteTravail.Visible = False
    df_OpenFile.imgAffichage.Visible = False
    df_OpenFile.imgSearch.Visible = False
    df_OpenFile.imgPrefolder.Visible = False
    df_OpenFile.imgCurrentPath.Visible = False
End If
Unload df_Drloading
df_OpenFile.Show vbModal
'-> on v�rifie l'existance d'un fichier � charger
If Trim(strRetour) = "" Then Exit Sub
FileN4ds = strRetour

'-> on lance la proc�dure de chargement du treeview
LoadN4ds strRetour

End Sub

Private Sub DealCmdButton1_Click()
'--> on lance la procedure de creation d'un enregistrement vierge selon ou l'on se trouve sur le treeview ou sur le dealgrid
On Error GoTo GestError

If haveFocus = "TreeView1" Then
    '-> on regarde si on peut faire l'ajout sur le treeview
    If CtrlFieldsAjout(Me.TreeView1.SelectedItem.Key) Then df_Ajout.LoadTreeview (Me.TreeView1.SelectedItem.Key)
Else
    '-> ajout d'un enregistrement sur le dealgrid
    If Me.DealGrid1.Row <> 1 Then
        If EnregCreateFree Then
            Me.DealGrid1.SelectRow (Me.DealGrid1.Row + 1)
            DealGrid1_BeforeSaisie 2, Me.DealGrid1.Row
        End If
    End If
End If

GestError:
End Sub

Private Sub DealCmdButton1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
DealCmdButton1.BorderStyle = 1
End Sub

Private Sub DealCmdButton1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
DealCmdButton1.BorderStyle = 0
End Sub

Private Sub DealCmdButton1_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
DealCmdButton1.BorderStyle = 0
End Sub

Private Sub DealCmdButton2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
DealCmdButton2.BorderStyle = 1
End Sub

Private Sub DealCmdButton2_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
DealCmdButton2.BorderStyle = 0
End Sub

Private Sub DealCmdButton3_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
DealCmdButton3.BorderStyle = 1
End Sub

Private Sub DealCmdButton3_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
DealCmdButton3.BorderStyle = 0
End Sub

Private Sub DealCmdButton4_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
DealCmdButton4.BorderStyle = 1
End Sub

Private Sub DealCmdButton4_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
DealCmdButton4.BorderStyle = 0
End Sub

Private Sub DealCmdButton4_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
DealCmdButton4.BorderStyle = 0
End Sub

Private Sub DealCmdButton6_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
DealCmdButton6.BorderStyle = 1
End Sub

Private Sub DealCmdButton6_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
DealCmdButton6.BorderStyle = 0
End Sub

Private Sub DealCmdButton6_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
DealCmdButton6.BorderStyle = 0
End Sub



Private Sub DealCmdButton2_Click()
'-> suppression d'un enreg ou d'un ensemble d'enregs

If Fields Is Nothing Then Exit Sub

If Fields.Count = 0 Then Exit Sub

Select Case haveFocus
    Case "DealGrid1" 'un enreg
        Call EnregDelete(Me.DealGrid1.GetCellText(3, Me.DealGrid1.Row), Me.DealGrid1.Row)
    Case "TreeView1" 'sur le treeview un ensemble d'enreg
        If CtrlFieldsSuppression(Me.TreeView1.SelectedItem.Key) Then
            EnregsDelete (Me.TreeView1.SelectedItem.Key)
        Else
            DisplayMessage "Suppression non autoris�e.", dsmWarning, dsmOkOnly, "Suppression"
        End If
End Select

End Sub

Private Sub DealCmdButton3_Click()
'--> on lance la procedure de recherche de texte dans les donn�es
Dim strText As String

'-> on reccupere le texte a rechercher
strText = TextSearchGet()
'-> lancer la recherche du texte depuis le debut
Call TextSearch(strText)
End Sub

Private Sub DealCmdButton4_Click()

'-> on verifie que l'on a pas des donn�es en cours de creation
If Not CtrlFieldsSame And curStatut <> pVisu And curStatut <> pSave Then
    If DisplayMessage("Des modifications sont en cours sur le fichier actuel." & Chr(13) & "L'�dition de donn�es n'est pas souhaitable." & Chr(13) & "Continuer ?", dsmWarning, dsmYesNo, "Ouverture d'un fichier") = "NON" Then
        Exit Sub
    End If
End If

'-> on ouvre la feuille de selection des editions
df_Edition.LoadComboExcel
df_Edition.SetFocus
End Sub

Private Sub DealCmdButton6_Click()
'--> on valide la saisie du dealgrid
CtrlFieldsDealGrid
 
End Sub

Private Sub DealGrid1_AfterSaisie()
'-> on cache le tooltip
Me.Frame1.Visible = False
End Sub

Private Sub DealGrid1_BeforeSaisie(Col As Integer, Row As Integer)

'If curStatut = pVisu Then Exit Sub
Dim strInfo As String
Dim aStructure As dc_structure

On Error GoTo GestError

'--> cette procedure initialise la saisie de la zone
Select Case Structures(Fields(Me.DealGrid1.GetCellText(3, Row)).Struct).TypeVal
    Case pNumeric
        Me.DealGrid1.InitSaisieCell True, FlexSaisieString, ""
    Case pDate
        Me.DealGrid1.InitSaisieCell True, FlexSaisieNum, ""
    Case pstring, pstringUcase
        '-> on initialise le masque a l'aide de la definition
        Me.DealGrid1.InitSaisieCell True, FlexSaisieString, ""
End Select



Set aStructure = Structures(Me.DealGrid1.GetCellText(0, Me.DealGrid1.Row))
                   
Select Case aStructure.Statut
    Case pObligatoire
        strInfo = "Statut : obligatoire"
    Case pFacultatif
        strInfo = "Statut : facultatif"
    Case pConditionnel
        strInfo = "Statut : Conditionnel"
End Select

strInfo = strInfo & Chr(13)

If aStructure.VarLength Then
    strInfo = strInfo & " Longueur : jusqu'� " & aStructure.Length & " caract�res."
Else
    strInfo = strInfo & " Longueur : " & aStructure.Length & " caract�res."
End If

strInfo = strInfo & Chr(13)

Select Case aStructure.TypeVal
    Case pDate
        strInfo = strInfo & " Type : date de format jjmmaaaa"
    Case pstring
        strInfo = strInfo & " Type : alphanum�rique"
    Case pstringUcase
        strInfo = strInfo & " Type : alphanum�rique en majuscule"
    Case pNumeric
        strInfo = strInfo & " Type : num�rique"
End Select

strInfo = strInfo & Chr(13)

strInfo = Me.DealGrid1.GetCellText(0, Me.DealGrid1.Row) & Chr(13) & Me.DealGrid1.GetCellText(1, Me.DealGrid1.Row) & Chr(13) & Me.DealGrid1.GetCellText(2, Me.DealGrid1.Row) & Chr(13) & strInfo

'-> on determine la position verticale de la fenetre du tooltip
Me.Frame1.Top = 250 'Me.DealGrid1.Top + 5 + Me.ScaleX(Me.DealGrid1.RowHeight * (Me.DealGrid1.Row + 1), vbTwips)
'-> on determine la position horizontale de la fenetre
Me.Frame1.Left = Me.DealGrid1.Left + 50
Me.ToolTip.Caption = strInfo
'-> on affiche le tooltip maison
Me.Frame1.Width = Me.ScaleX(Me.ToolTip.Width, vbTwips)
Me.Frame1.Height = Me.ScaleX(Me.ToolTip.Height, vbTwips)
Me.Frame1.Visible = True

'-> on passe en saisie
curStatut = pSaisie
'-> on selectionne la zone de saisie
SelectZone

GestError:

End Sub

Private Sub DealGrid1_Click()
'Dim strInfo As String
'Dim aStructure As dc_structure
'
'On Error GoTo GestError
'
'Set aStructure = Structures(Me.DealGrid1.GetCellText(0, Me.DealGrid1.Row))
'
'Select Case aStructure.Statut
'    Case pObligatoire
'        strInfo = "  Statut : obligatoire"
'    Case pFacultatif
'        strInfo = "  Statut : facultatif"
'    Case pConditionnel
'        strInfo = "  Statut : Conditionnel"
'End Select
'
'strInfo = strInfo & Chr(13)
'
'If aStructure.VarLength Then
'    strInfo = strInfo & " Longueur : jusqu'� " & aStructure.Length & " caract�res."
'Else
'    strInfo = strInfo & " Longueur : " & aStructure.Length & " caract�res."
'End If
'
'strInfo = strInfo & Chr(13)
'
'Select Case aStructure.TypeVal
'    Case pDate
'        strInfo = strInfo & " Type : date de format jjmmaaaa"
'    Case pstring
'        strInfo = strInfo & " Type : alphanum�rique"
'    Case pstringUcase
'        strInfo = strInfo & " Type : alphanum�rique en majuscule"
'    Case pNumeric
'        strInfo = strInfo & " Type : num�rique"
'End Select
'
'strInfo = strInfo & Chr(13)
'
'Me.DealGrid1.ToolTipText = Me.DealGrid1.GetCellText(0, Me.DealGrid1.Row) & "     " & Me.DealGrid1.GetCellText(1, Me.DealGrid1.Row) & "     " & Me.DealGrid1.GetCellText(2, Me.DealGrid1.Row) & Chr(13) & strInfo
'
'GestError:

End Sub

Private Sub DealGrid1_DblClick()

'--> on initialise la saisie
DealGrid1_BeforeSaisie 2, Me.DealGrid1.Row

End Sub

Private Sub SelectZone()
'--> cette procedure selectionne le texte d'une zone
' utilis� pour le dealgrid
SendKeys "+{HOME}"

End Sub

Private Sub DealGrid1_GotFocus()
haveFocus = "DealGrid1"
End Sub

Private Sub DealGrid1_KeyDown(KeyCode As Integer, Shift As Integer)

'--> on g�re ici l'appui sur les touches
Select Case KeyCode
    Case 13
       DealGrid1_BeforeSaisie 2, Me.DealGrid1.Row
    Case vbKeyF9 '-> ajouter un enregistrement
        '-> on regarde si on peut ajouter un enreg apres la position courante
        If EnregCreateFree Then
            Me.DealGrid1.SelectRow Me.DealGrid1.Row + 1
            DealGrid1_BeforeSaisie 2, Me.DealGrid1.Row
        End If
    Case vbKeyBack
        Call EnregDelete(Me.DealGrid1.GetCellText(3, Me.DealGrid1.Row), Me.DealGrid1.Row)
End Select
End Sub

Private Sub DealGrid1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
Dim curRow As Integer
Dim strStructure As String
Dim strKeyWord As String
Dim curIndex As Integer

'-> Tester si on est sur le bouton droit
If Button <> vbRightButton Then Exit Sub

On Error GoTo GestError

curRow = Me.DealGrid1.Row
strKeyWord = Me.DealGrid1.GetCellText(3, curRow)
If strKeyWord = "" Then GoTo GestError

'-> on pointe sur la structure de la ligne
strStructure = Fields(strKeyWord).Struct
curIndex = Structures(strStructure).Index

'-> on regarde si on doit afficher le menu ajouter
'-> on regarde si on a la meme structure sur la ligne suivante sinon l'ajouter si la ligne existe
Me.DealPopupMenu1.SetEnabledMenu "AJOUTER", True
If df_N4ds.DealGrid1.Rows > curRow + 1 Then
    If Structures(curIndex + 1).FieldValue = Fields(df_N4ds.DealGrid1.GetCellText(3, curRow + 1)).Struct Then Me.DealPopupMenu1.SetEnabledMenu "AJOUTER", False
Else
    If Fields(df_N4ds.DealGrid1.GetCellText(3, curRow)).Niveau <> Structures(curIndex + 1).FieldNiveau Then Me.DealPopupMenu1.SetEnabledMenu "AJOUTER", False
End If
'-> si les doublons sont autoris�s
If Structures(curIndex).Multiple = True Then Me.DealPopupMenu1.SetEnabledMenu "AJOUTER", True

'-> on regarde si on doit afficher le menu supprimer
If Structures(Fields(strKeyWord).Struct).Statut = pObligatoire Then
    Me.DealPopupMenu1.SetEnabledMenu "SUPPRIMER", False
Else
    Me.DealPopupMenu1.SetEnabledMenu "SUPPRIMER", True
End If

'-> Afficher le menu
Me.DealPopupMenu1.PopUp "F10"

'-> Selon le retour menu
Select Case Me.DealPopupMenu1.MenuKey
    Case "AJOUTER" 'ajouter tout un noeud d'enregistrement
    '-> ajout d'un enregistrement sur le dealgrid
    If Me.DealGrid1.Row <> 1 Then
        If EnregCreate Then
            Me.DealGrid1.SelectRow (Me.DealGrid1.Row + 1)
            DealGrid1_BeforeSaisie 2, Me.DealGrid1.Row
        End If
    End If
    Case "SUPPRIMER"
        Call EnregDelete(Me.DealGrid1.GetCellText(3, Me.DealGrid1.Row), Me.DealGrid1.Row)
    Case "EXCEL" 'Export vers Excel du browse en cours
        Call df_Edition.LoadComboExcel
    Case "PRINT" 'Print
        '-> on lance l'impression
        '-> on verifie que l'on a pas des donn�es en cours de creation
        If Not CtrlFieldsSame Then
            If DisplayMessage("Des modifications sont en cours sur le fichier actuel." & Chr(13) & "L'�dition de donn�es n'est pas souhaitable." & Chr(13) & "Continuer ?", dsmWarning, dsmYesNo, "Ouverture d'un fichier") = "NON" Then
                Exit Sub
            End If
        End If
'-> on ouvre la feuille de selection des editions
df_Edition.LoadComboExcel
    Case "RECHERCHE"
        '-> on lance la recherche
        Call DealCmdButton3_Click
    Case "SUIVANT"
        TextSearch InputValue, Me.DealGrid1.GetCellText(3, Me.DealGrid1.Row)
End Select

GestError:
End Sub

Private Sub DealGrid1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
If curStatut <> pSaisie Then
    Me.Frame1.Visible = False
End If
End Sub

Private Sub DealGrid1_ValidateSaisie(Col As Integer, Row As Integer, ValueSaisie As String, CancelValidate As Boolean)

'-> on regarde si il y a eu des modifications dans le cas d'un enregistrement normal
If Fields(Me.DealGrid1.GetCellText(3, Row)).Statut = pNormal Then
    If Me.DealGrid1.GetCellText(Col, Row) = ValueSaisie Then
        curStatut = pVisu
        Exit Sub
    End If
End If

'-> on verifie les donn�es
If Not CtrlFormatZone(ValueSaisie, Fields(Me.DealGrid1.GetCellText(3, Row)).Struct) Then
    CancelValidate = True
    Exit Sub
End If

'-> on enregistre la saisie en effectuant les controles de premier niveau
If EnregSave(Me.DealGrid1.GetCellText(3, Row), ValueSaisie) Then
    Me.DealGrid1.SetCellText Col, Row, ValueSaisie
    curStatut = pModify
Else
    CancelValidate = True
End If

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'--> Gestion des touches de raccourci
'-> gestion des erreurs
On Error GoTo GestError

Select Case KeyCode
    Case vbKeyF 'suivant
        If Shift = 2 Then
            Call DealCmdButton3_Click
        End If
    Case vbKeyF3 'recherche suivant
            TextSearch InputValue, Me.DealGrid1.GetCellText(3, Me.DealGrid1.Row)
    Case vbKeyBack 'suppression
            If Me.ActiveControl.Name = "TreeView1" Then EnregsDelete (Me.TreeView1.SelectedItem.Key)
    Case vbKeyF9 'ajouter un enreg a partir du treeview
            If Me.ActiveControl.Name = "TreeView1" Then
                If CtrlFieldsAjout(Me.TreeView1.SelectedItem.Key) Then df_Ajout.LoadTreeview (Me.TreeView1.SelectedItem.Key)
            End If
    Case vbKeyI 'suivant
        If Shift = 2 Then
            DisplayMessage "N4dsExport.ini :" & N4dsExportini & Chr(13) & "N4dsTurbo.ini :" & searchExe("N4dsTurbo.ini") & Chr(13) & "N4dsStruct.ini :" & searchExe("N4dsStruct.ini") & Chr(13) & "Derni�re maquette utilis�e :" & lastMaquette, dsmQuestion, dsmOkOnly, "Fichiers de param�trage"
        End If
End Select

GestError:

End Sub

Private Sub Form_Load()
SkinColorModify Me
Me.DogSkinObject1.Initialisation False, "Visualisation, correction des donn�es N4ds", pMinMaxClose, True
Me.DogSkinObject1.SetMatriceEffect "Shape1|TreeView1|DealGrid1"

'-> Cr�er le premier niveau de menu
Me.DealPopupMenu1.AddMenu "", "F10", "", "F10"

'-> Initialisation des recherches
'-> Rechercher
Me.DealPopupMenu1.AddMenu "F10", "AJOUTER", "Ajouter", "AJOUTER"
Me.DealPopupMenu1.AddMenu "F10", "COPIER", "Copier", "COPIER"
Me.DealPopupMenu1.AddMenu "F10", "COLLER", "Coller", "COLLER"
Me.DealPopupMenu1.AddMenu "F10", "SUPPRIMER", "Supprimer", "SUPPRIMER"
Me.DealPopupMenu1.AddMenu "F10", "SUPPRIMERRUBRIQUE", "Supprimer toute une rubrique", "SUPPRIMERRUBRIQUE"
Me.DealPopupMenu1.AddMenu "F10", "REPLACE", "Remplacer les valeurs d'une rubrique", "REPLACE"

'-> Separation
Me.DealPopupMenu1.AddMenu "F10", "-2", "-", "-"
'-> Rechercher
Me.DealPopupMenu1.AddMenu "F10", "RECHERCHE", "Rechercher (CTRL+F)", "RECHERCHE"
Me.DealPopupMenu1.AddMenu "F10", "SUIVANT", "Suivant (F3)", "SUIVANT"
'-> Separation
Me.DealPopupMenu1.AddMenu "F10", "-1", "-", "-"
'-> Gestion des exports Excel
Me.DealPopupMenu1.AddMenu "F10", "EXCEL", "Exporter vers Excel", "EXCEL"
'-> Menu d'impression
Me.DealPopupMenu1.AddMenu "F10", "PRINT", "Imprimer", "PRINT"

'-> Initialiser le menu
Me.DealPopupMenu1.InitDealMenu

Image1.MouseIcon = Picture2
Image2.MouseIcon = Picture2
Image3.MouseIcon = Picture2
DealCmdButton1.MouseIcon = Picture2
DealCmdButton2.MouseIcon = Picture2
DealCmdButton3.MouseIcon = Picture2
DealCmdButton4.MouseIcon = Picture2
DealCmdButton6.MouseIcon = Picture2

TreeView1.MouseIcon = Picture2
End Sub


Private Sub Form_Unload(Cancel As Integer)
'-> on regarde si il y a eu des modifications sur le fichier en cours
If Not Fields Is Nothing Then
    If Not CtrlFieldsSame And curStatut <> pSave And curStatut <> pVisu Then
        If DisplayMessage("Des modifications sont en cours sur le fichier actuel." & Chr(13) & "La fermeture du fichier va entrainer la perte de toutes les modifications." & Chr(13) & "Continuer ?", dsmWarning, dsmYesNo, "Ouverture d'un fichier") = "NON" Then
            Cancel = 1
            Exit Sub
        End If
    End If
End If
    
TerminateApp "Editeur N4ds"

End Sub

Private Sub Image1_Click()

'--> regeneration du fichier N4ds
Call EnregsSave

End Sub

Private Sub Image1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
Image1.BorderStyle = 1
End Sub

Private Sub Image1_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
Image1.BorderStyle = 0
End Sub
Private Sub Image2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
Image2.BorderStyle = 1
End Sub

Private Sub Image2_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
Image2.BorderStyle = 0
End Sub
Private Sub Image3_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
Image3.BorderStyle = 1
End Sub

Private Sub Image3_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
Image3.BorderStyle = 0
End Sub

Private Sub Image2_Click()
'--> va charger un fichier
'-> on regarde si il y a eu des modifications sur le fichier en cours
If Not Fields Is Nothing Then
    If Not CtrlFieldsSame And curStatut <> pSave And curStatut <> pVisu Then
        If DisplayMessage("Des modifications sont en cours sur le fichier actuel." & Chr(13) & "L'ouverture d'un nouveau fichier va entrainer la perte de toutes les modifications." & Chr(13) & "Continuer ?", dsmWarning, dsmYesNo, "Ouverture d'un fichier") = "NON" Then
            Exit Sub
        End If
    End If
End If
'-> on reinitialise l'ecran
Call Initialise
'--> on reouvre un nouveau fichier
Me.init

End Sub

Private Sub Image3_Click()
'--> va recharger le fichier actuel

If Fields Is Nothing Then Exit Sub

If Fields.Count = 0 Then Exit Sub

'-> on regarde si il y a eu des modifications sur le fichier en cours
If Not Fields Is Nothing Then
    If Not CtrlFieldsSame Then
        If DisplayMessage("Des modifications sont en cours sur le fichier actuel." & Chr(13) & "Le rechargement du fichier va entrainer la perte de toutes les modifications." & Chr(13) & "Continuer ?", dsmWarning, dsmYesNo, "R�ouverture du fichier") = "NON" Then
            Exit Sub
        End If
    Else
        DisplayMessage "Le fichier en cours n'a pas �t� modifi�." & Chr(13) & "Il est inutile de le recharger.", dsmQuestion, dsmOkOnly, "R�ouverture du fichier"
        Exit Sub
    End If
End If
'-> on reinitialise l'ecran
Call Initialise
'--> on reouvre un nouveau fichier
LoadN4ds FileN4ds

End Sub

Private Sub ToolTip_Click()

Me.Frame1.Visible = False

End Sub

Private Sub TreeView1_GotFocus()
haveFocus = "TreeView1"
End Sub

Private Sub TreeView1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'-> Tester si on est sur le bouton droit
If Button <> vbRightButton Then Exit Sub

'On Error GoTo GestError

'-> on regarde si on doit afficher le menu ajouter
If Not CtrlFieldsAjout(Me.TreeView1.SelectedItem.Key) Then
    Me.DealPopupMenu1.SetEnabledMenu "AJOUTER", False

Else
    Me.DealPopupMenu1.SetEnabledMenu "AJOUTER", True
End If

'-> on regarde si on doit afficher copier
If Not CtrlFieldsCopy(Me.TreeView1.SelectedItem.Key) Then
    Me.DealPopupMenu1.SetEnabledMenu "COPIER", False
Else
    Me.DealPopupMenu1.SetEnabledMenu "COPIER", True
End If

'-> on regarde si on doit afficher coller
If Not CtrlFieldsPaste(Me.TreeView1.SelectedItem.Key) Then
    Me.DealPopupMenu1.SetEnabledMenu "COLLER", False
Else
    Me.DealPopupMenu1.SetEnabledMenu "COLLER", True
End If

'-> on regarde si on doit afficher le menu supprimer
If Not CtrlFieldsSuppression(Me.TreeView1.SelectedItem.Key) Then
    Me.DealPopupMenu1.SetEnabledMenu "SUPPRIMER", False
Else
    Me.DealPopupMenu1.SetEnabledMenu "SUPPRIMER", True
End If

'-> on regarde si on doit afficher le menu supprimer une rubrique
Me.DealPopupMenu1.SetEnabledMenu "SUPPRIMERRUBRIQUE", True

'-> on regarde si on doit afficher le menu supprimer
Me.DealPopupMenu1.SetEnabledMenu "REPLACE", True

'-> Afficher le menu
Me.DealPopupMenu1.PopUp "F10"

'-> Selon le retour menu
Select Case Me.DealPopupMenu1.MenuKey
    Case "AJOUTER" 'ajouter tout un noeud d'enregistrement
        df_Ajout.LoadTreeview (Me.TreeView1.SelectedItem.Key)
    Case "COPIER" 'copier un element
        Call EnregsCopy(Me.TreeView1.SelectedItem.Key)
    Case "COLLER" 'coller un enregistrement
        Call EnregsPaste(Me.TreeView1.SelectedItem.Key)
    Case "SUPPRIMER"
        Call EnregsDelete(Me.TreeView1.SelectedItem.Key)
    Case "SUPPRIMERRUBRIQUE"
        Call EnregsDeleteRubrique(Me.TreeView1.SelectedItem.Key)
    Case "REPLACE"
        Call EnregsReplaceRubrique(Me.TreeView1.SelectedItem.Key)
    Case "EXCEL" 'Export vers Excel du browse en cours
        Call df_Edition.LoadComboExcel
    Case "PRINT" 'Print
        '-> on lance l'impression
    '-> on verifie que l'on a pas des donn�es en cours de creation
    If Not CtrlFieldsSame Then
        If DisplayMessage("Des modifications sont en cours sur le fichier actuel." & Chr(13) & "L'�dition de donn�es n'est pas souhaitable." & Chr(13) & "Continuer ?", dsmWarning, dsmYesNo, "Ouverture d'un fichier") = "NON" Then
            Exit Sub
        End If
    End If

'-> on ouvre la feuille de selection des editions
df_Edition.LoadComboExcel
    Case "RECHERCHE"
        '-> on lance la recherche
        Call DealCmdButton3_Click
    Case "SUIVANT"
        TextSearch InputValue, Me.DealGrid1.GetCellText(3, Me.DealGrid1.Row)
End Select

GestError:
End Sub

Private Sub Form_Resize()
'--> on redessine et repositionne les controles au retaillage de l'ecran
Call ScreenResize
End Sub

Private Sub TreeView1_DblClick()
'--> on va charger le tableau selon d'ou l'on vient
If Me.TreeView1.SelectedItem Is Nothing Then Exit Sub
LoadDealGrid (Me.TreeView1.SelectedItem.Key)
End Sub

Private Sub ScreenResize()
'--> cette procedure g�re la pr�sentation de l'�cran

On Error GoTo GestError

'-> le treeview
Me.TreeView1.Height = Me.ScaleY(Me.Height, 1, 3) - 90

'-> le dealgrid
Me.DealGrid1.Height = Me.ScaleY(Me.Height, 1, 3) - 120
Me.DealGrid1.Width = Me.ScaleX(Me.Width, 1, 3) - Me.TreeView1.Width - 40
'-> on retaille les colonnes
Me.DealGrid1.Col = 0
Me.DealGrid1.ColWidth = 2100
Me.DealGrid1.Col = 1
Me.DealGrid1.ColWidth = (Me.ScaleX(Me.DealGrid1.Width, 3, 1) - 2000) / 2 - 250
Me.DealGrid1.Col = 2
Me.DealGrid1.ColWidth = (Me.ScaleX(Me.DealGrid1.Width, 3, 1) - 2000) / 2 - 100
'-> ne sert que pour la colonne de scroll
Me.DealGrid1.Col = 3
Me.DealGrid1.ColWidth = 0
Me.DealGrid1.Col = 4
Me.DealGrid1.ColWidth = 250

'-> les boutons
Me.DealCmdButton1.Top = Me.ScaleY(Me.Height, 1, 3) - 60
Me.DealCmdButton2.Top = Me.ScaleY(Me.Height, 1, 3) - 60
Me.DealCmdButton3.Top = Me.ScaleY(Me.Height, 1, 3) - 60
Me.DealCmdButton3.Top = Me.ScaleY(Me.Height, 1, 3) - 60
Me.DealCmdButton4.Top = Me.ScaleY(Me.Height, 1, 3) - 60
Me.DealCmdButton6.Top = Me.ScaleY(Me.Height, 1, 3) - 60

Me.DealCmdButton1.Left = Me.ScaleX(Me.Width, 1, 3) - 350
Me.DealCmdButton2.Left = Me.DealCmdButton1.Left + 41
Me.DealCmdButton3.Left = Me.DealCmdButton2.Left + 41
Me.DealCmdButton4.Left = Me.DealCmdButton3.Left + 41
Me.DealCmdButton6.Left = Me.DealCmdButton4.Left + 41


Me.Image1.Top = Me.DealCmdButton1.Top
Me.Image2.Top = Me.DealCmdButton1.Top
Me.Image3.Top = Me.DealCmdButton1.Top

Me.Refresh

GestError:

End Sub

