VERSION 5.00
Object = "{DAFD85F1-343B-47EE-8170-1C26723F7A5D}#2.0#0"; "dogskin.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form df_Suppr 
   BackColor       =   &H00CBB49A&
   BorderStyle     =   0  'None
   Caption         =   "Saisie des selections"
   ClientHeight    =   5700
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7515
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   5700
   ScaleWidth      =   7515
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin DogSkin.DealCmdButton Picture1 
      Height          =   615
      Left            =   6120
      TabIndex        =   0
      Tag             =   "DICTAB�BUTTON-OK"
      Top             =   4980
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   1
      ButtonForm      =   1
      BackColor       =   16579059
   End
   Begin DogSkin.DogSkinObject DogSkinObject1 
      Left            =   0
      Top             =   0
      _ExtentX        =   979
      _ExtentY        =   953
      BorderColor     =   13077835
      BeginProperty HeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderForeColor =   10053171
      HeaderBackColor =   16637889
      HeaderLineColor =   15847345
      HeaderUseLine   =   -1  'True
      BeginProperty BodyFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BodyForeColor   =   10053171
      BodyBackColor   =   16579059
      HeaderIco       =   "df_Suppr.frx":0000
      HeaderIcoNa     =   "df_Suppr.frx":0682
      ShadowColor     =   15847345
   End
   Begin DogSkin.DealCmdButton Picture3 
      Height          =   615
      Left            =   6720
      TabIndex        =   1
      Tag             =   "DICTAB�BUTTON-CANCEL"
      Top             =   4980
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1085
      Caption         =   "DogButton"
      ForeColor       =   10053171
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ButtonType      =   2
      ButtonForm      =   1
      BackColor       =   16579059
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4080
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   21
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":0D04
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":19DE
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":36B8
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":4392
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":506C
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":5D46
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":6A20
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":76FA
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":7FD4
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":9CAE
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":A588
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":B262
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":BB3C
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":C416
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":D0F0
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":DDCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":E6A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":1037E
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":12058
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":1784A
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "df_Suppr.frx":19524
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   4215
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   7435
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00C78D4B&
      Height          =   855
      Left            =   120
      Top             =   600
      Width           =   5655
   End
End
Attribute VB_Name = "df_Suppr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
    Case 113 'F2 validation
    
    Case 115, 27 'F4 ou esc annulation bon
        Unload Me
End Select

End Sub

Private Sub Form_Load()
SkinColorModify Me
'---> Init du skin
Me.DogSkinObject1.Initialisation False, "Supprimer des enregistrements", pCloseOnly, False
Me.DogSkinObject1.SetMatriceEffect "SHAPE1"
'-> on charge la couleur de fond desbouttons
Me.Picture1.BackColor = Me.DogSkinObject1.BodyBackColor
Me.Picture3.BackColor = Me.DogSkinObject1.BodyBackColor

End Sub

Public Sub init(strKeyWord As String)
'--> cette procedure va charger la combo avec la liste des champs dispo
' a partir du noeud ou on est situ�

'--> cette proc�dure permet de supprimer si on a le droit tout un noeud d'enregistrement
Dim Niveau As Variant
Dim strTemp As Variant
Dim afield As dc_field
Dim tempKeyWord As String
Dim i As Integer

If strKeyWord = "" Then Exit Sub

On Error GoTo GestError
'-> on effectue les controles de suppression

'-> on se met la cle dans un vecteur pour pouvoir travailler dessus
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split(strKeyWord, "|")
For i = 1 To 6
    Niveau(i) = CSng(strTemp(i))
Next

'-> on cree la chaine de base du noeud
For i = 1 To Fields(strKeyWord).Niveau
    tempKeyWord = tempKeyWord & "|" & CSng(strTemp(i))
Next

tempKeyWord = "�" & tempKeyWord & "|"

Me.ListView1.ColumnHeaders.Clear
Me.ListView1.ListItems.Clear
'-> on cree les entete du tableau
Me.ListView1.ColumnHeaders.Add , , "Rubrique & libell�", 300


'-> on va mettre le statut a delete de tous les enreg de ce noeud et des noeud inferieurs
For Each afield In Fields
    '-> on verifie que l'enreg appartient bien � notre noeud
    If InStr(1, "�" & afield.KeyWord, tempKeyWord) <> 0 Then
        '-> si c'est pas deja fait on rajoute la ligne de rubrique dans la combo
        On Error Resume Next
        If Me.ListView1.ListItems(afield.Struct) Is Nothing Then
            If Structures(afield.Struct).Statut <> pObligatoire Then
                Me.ListView1.ListItems.Add , afield.Struct, afield.Struct & " - " & Structures(afield.Struct).FieldDescription
            End If
        End If
        On Error GoTo GestError
    End If
Next

df_Suppr.Show vbModal

GestError:

End Sub

Private Sub Picture1_Click()
'--> cette proc�dure permet de supprimer toutes les rubriques a partir d'un noeud
Dim Niveau As Variant
Dim strTemp As Variant
Dim afield As dc_field
Dim tempKeyWord As String
Dim strKeyWord As String
Dim i As Integer
Dim j As Integer
Dim aItem As ListItem

strKeyWord = df_N4ds.TreeView1.SelectedItem.Key
If strKeyWord = "" Then Exit Sub

'-> Demander confirmation sur la suppression des enregistrement
strRetour = DisplayMessage("Supprimer d�finitivement tous les enregistrements � partir de la rubrique selectionn�e (" & df_N4ds.TreeView1.SelectedItem.Text & ")?", dsmWarning, dsmYesNo, "Confirmation de suppression")
If strRetour = "NON" Then Exit Sub

On Error GoTo GestError
'-> on effectue les controles de suppression

'-> on se met la cle dans un vecteur pour pouvoir travailler dessus
Niveau = Array(0, 0, 0, 0, 0, 0, 0, 0)
'-> on se charge le vecteur a partir de la cl�
strTemp = Split(strKeyWord, "|")
For i = 1 To 6
    Niveau(i) = CSng(strTemp(i))
Next

'-> on cree la chaine de base du noeud
For i = 1 To Fields(strKeyWord).Niveau
    tempKeyWord = tempKeyWord & "|" & CSng(strTemp(i))
Next

tempKeyWord = "�" & tempKeyWord & "|"

For Each aItem In Me.ListView1.ListItems
    If aItem.Checked Then
        '-> on va mettre le statut a delete de tous les enreg de ce noeud et des noeud inferieurs
        For Each afield In Fields
            '-> on verifie que l'enreg appartient bien � notre noeud
            If InStr(1, "�" & afield.KeyWord, tempKeyWord) <> 0 And afield.Struct = aItem.Key Then
                '-> on met tous les enreg du noeud en suppression
                If Structures(afield.Struct).Statut = pObligatoire Then GoTo GestError
                afield.Statut = pDelete
                j = j + 1
            End If
        Next
    End If
Next

'-> on supprime l'enregistrement de l'ecran au niveau du treeview et du dealgrid
LoadDealGrid df_N4ds.TreeView1.SelectedItem.Key

'-> on remet a jour les compteurs
CtrlFieldsCompteur


DisplayMessage "Suppression de " & j & " enregistrement(s).", dsmQuestion, dsmOkOnly, "Suppression d'enregistrements"

Exit Sub
GestError:
DisplayMessage "Suppression non autoris�e", dsmQuestion, dsmOkOnly, "Suppression d'enregistrements"

End Sub

Private Sub Picture3_Click()

'-> on quitte sans rien faire
Unload Me

End Sub

Public Sub ComboSaisieAuto(aCombo As ImageCombo, SaisieLibre As Boolean)
'--> cette proc�dure permet la saisie automatique dans une combo
Dim i As Long
Dim sel As Long
Dim aItem As ComboItem

'-> on parcours les elements de la combo
For Each aItem In aCombo.ComboItems
    sel = Len(aCombo.Text)
    '-> si on trouve le texte on l'affiche
    If (StrComp(Left$(aItem.Text, sel), aCombo.Text, vbTextCompare) = 0) And aCombo.Text <> "" Then
        aItem.Selected = True
        aCombo.SelStart = sel
        If Len(aCombo.Text) <> sel Then
            aCombo.SelLength = Len(aCombo.Text) - sel
        Else
            aCombo.SelStart = 0
            aCombo.SelLength = Len(aCombo.Text)
        End If
        GoTo Suite
    End If
Next
'-> on a rien trouv�
If Not SaisieLibre Then
    aCombo.Text = ""
End If
Suite:

End Sub

