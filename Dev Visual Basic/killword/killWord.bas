Attribute VB_Name = "killWord"
Option Explicit

Private Declare Function PostMessage Lib "user32.dll" Alias "PostMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long

Private Declare Function TerminateProcess Lib "kernel32" (ByVal hProcess As Long, ByVal uExitCode As Long) As Long
Public Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, ByRef lpdwProcessId As Long) As Long
Public Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessID As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Public Declare Function EnumWindows Lib "user32" (ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
Private Declare Function GetParent Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function IsWindowVisible Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function GetWindowTextLength Lib "user32" _
    Alias "GetWindowTextLengthA" _
   (ByVal hwnd As Long) As Long
    
Private Declare Function GetWindowText Lib "user32" _
    Alias "GetWindowTextA" _
   (ByVal hwnd As Long, _
    ByVal lpString As String, _
    ByVal cch As Long) As Long
    
Private Declare Function GetClassName Lib "user32" _
    Alias "GetClassNameA" _
   (ByVal hwnd As Long, _
    ByVal lpClassName As String, _
    ByVal nMaxCount As Long) As Long

Private Const LVIF_INDENT As Long = &H10
Private Const LVIF_TEXT As Long = &H1
Private Const LVM_FIRST As Long = &H1000
Private Const LVM_SETITEM As Long = (LVM_FIRST + 6)

Private Type LVITEM
   mask As Long
   iItem As Long
   iSubItem As Long
   state As Long
   stateMask As Long
   pszText As String
   cchTextMax As Long
   iImage As Long
   lParam As Long
   iIndent As Long
End Type

Public Const SYNCHRONIZE = &H100000
Public Const PROCESS_TERMINATE As Long = &H1


Public lstW As String
Public NewCommand As String
Public iWait As Integer

Public Const TH32CS_SNAPPROCESS As Long = 2&
Public Const MAX_PATH As Long = 260

Public Type PROCESSENTRY32
    dwSize As Long
    cntUsage As Long
    th32ProcessID As Long
    th32DefaultHeapID As Long
    th32ModuleID As Long
    cntThreads As Long
    th32ParentProcessID As Long
    pcPriClassBase As Long
    dwFlags As Long
    szExeFile As String * MAX_PATH
End Type
   
Public Declare Function CreateToolhelp32Snapshot Lib "kernel32" (ByVal lFlags As Long, ByVal lProcessID As Long) As Long

Public Declare Function ProcessFirst Lib "kernel32" Alias "Process32First" (ByVal hSnapShot As Long, uProcess As PROCESSENTRY32) As Long

Public Declare Function ProcessNext Lib "kernel32" Alias "Process32Next" (ByVal hSnapShot As Long, uProcess As PROCESSENTRY32) As Long

Private Const GW_HWNDFIRST = 0
Private Const GW_HWNDNEXT = 2
Private Const GW_CHILD = 5

Private Declare Function GetWindow Lib "user32" (ByVal hwnd As Long, ByVal wCmd As Long) As Long
Private Declare Function GetDesktopWindow Lib "user32" () As Long
Private Declare Function BringWindowToTop Lib "user32" (ByVal hwnd As Long) As Long

Public Sub Main()

iWait = Val(ZonChargGet("wait"))
If iWait = 0 Then iWait = 30
If InStr(1, Command$, "console", vbTextCompare) <> 0 Then
    Load frmSysTray
    frmKillWord.Init
Else
    frmKillWord.Show
End If

End Sub


Private Sub KillApp(strProg As String)
Dim target_hwnd As Long
Dim target_process_id As Long
Dim target_process_handle As Long

    '-> on pointe sur la fenetre.
    target_hwnd = FindWindow(vbNullString, strProg)
    If target_hwnd = 0 Then
        Exit Sub
    End If

    '-> on reccupere l'ID.
    GetWindowThreadProcessId target_hwnd, target_process_id
    If target_process_id = 0 Then
        Exit Sub
    End If

    ' ouvrir le process.
    target_process_handle = OpenProcess(SYNCHRONIZE Or PROCESS_TERMINATE, ByVal 0&, target_process_id)
    If target_process_handle = 0 Then
        Exit Sub
    End If

    '-> terminer process.
    TerminateProcess target_process_handle, 0&

    '-> fermer le process.
    CloseHandle target_process_handle
End Sub

Public Sub listApp(strProg As String)
Dim target_hwnd As Long
Dim target_process_id As Long
Dim target_process_handle As Long
Dim i As Integer
Dim t As String

    '-> on pointe sur la fenetre.
    target_hwnd = FindWindow(vbNullString, strProg)
    If target_hwnd = 0 Then
        Exit Sub
    End If

    '-> on reccupere l'ID.
    GetWindowThreadProcessId target_hwnd, target_process_id
    If target_process_id = 0 Then
        Exit Sub
    End If

    '-> si on l'a pas on l'ajoute
    If Not InStr(1, lstW, target_process_id) Then
        lstW = lstW & "," & target_process_id & "|" & Now
    Else
        For i = 1 To i <= NumEntries(lstW, ",")
            t = Entry(2, Entry(i, lstW, ","), "|")
            If Val(t) - Now > 3600 Then
                MsgBox ("kill")
            End If
        Next
    End If

    ' ouvrir le process.
    target_process_handle = OpenProcess(SYNCHRONIZE Or PROCESS_TERMINATE, ByVal 0&, target_process_id)
    If target_process_handle = 0 Then
        Exit Sub
    End If

End Sub

Public Function ZonChargGet(StrZonCharg As String) As String

'--> cette fonction retourne la valeur d'un zoncharge
Dim zcharge As String
Dim ztrav As String
Dim i As Integer

'-> on regarde si on a changer le command$
If NewCommand <> "" Then
    zcharge = NewCommand
Else
    zcharge = Entry(2, Command$, "|")
End If

For i = 2 To NumEntries(zcharge, "_")
  ztrav = UCase$(Trim(Entry(1, Entry(i, zcharge, "_"), "=")))
  Select Case ztrav
    Case UCase(StrZonCharg)
        ZonChargGet = Trim(Entry(2, Entry(i, zcharge, "_"), "="))
        Exit For
  End Select
Next

End Function

Public Function doShowProcessList()
Dim hSnapShot As Long
Dim uProcess As PROCESSENTRY32
Dim success As Long
Dim txtOut As String

Dim nSize As Long
Dim sTitle As String
Dim sClass As String

Dim sIDType As String
Dim itmX As ListItem
Dim nodX As Node
Dim timX As Integer
Dim target_process_id As Long
Dim target_process_handle As Long
Dim aitem As ListItem

On Error Resume Next

    For Each aitem In frmKillWord.ListView1.ListItems
        aitem.Text = ""
    Next

  hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0&)

  If hSnapShot = -1 Then Exit Function
  uProcess.dwSize = Len(uProcess)
  success = ProcessFirst(hSnapShot, uProcess)

  If success = 1 Then
    Do
        txtOut = txtOut & vbCrLf & uProcess.szExeFile
        If InStr(1, uProcess.szExeFile, "winWord", vbTextCompare) <> 0 Then
            'hwnd = GetHwndFromProcess(uProcess.th32ProcessID)
            sTitle = "Winword" 'GetWindowIdentification(target_process_id, sIDType, sClass)
            If Not isKey(CStr(uProcess.th32ProcessID) & "h") Then
                'add to the listview
                Set itmX = frmKillWord.ListView1.ListItems.Add(Text:=sTitle, Key:=CStr(uProcess.th32ProcessID) & "h")
                'itmX.SmallIcon = Form1.ImageList1.ListImages("parent").Key
                itmX.SubItems(1) = CStr(uProcess.th32ProcessID)
                itmX.SubItems(2) = Now
             Else
                frmKillWord.ListView1.ListItems(CStr(uProcess.th32ProcessID) & "h").Text = "Winword"
                timX = DateDiff("n", frmKillWord.ListView1.ListItems(CStr(uProcess.th32ProcessID) & "h").SubItems(2), Now)
                If timX >= iWait Then
                    '-> on reccupere l'ID.
                    target_process_id = uProcess.th32ProcessID
                    If target_process_id = 0 Then
                        Exit Function
                    End If
                    target_process_handle = OpenProcess(SYNCHRONIZE Or PROCESS_TERMINATE, ByVal 0&, target_process_id)
                    If target_process_handle = 0 Then
                        Exit Function
                    End If
                
                    '-> terminer process.
                    TerminateProcess target_process_handle, 0&
                
                    '-> fermer le process.
                    CloseHandle target_process_handle
                Else
                    frmKillWord.ListView1.ListItems(CStr(uProcess.th32ProcessID) & "h").SubItems(3) = timX
                End If
             End If
     End If
    
    
    Loop While ProcessNext(hSnapShot, uProcess)
  End If
    
    For Each aitem In frmKillWord.ListView1.ListItems
        If aitem.Text = "" Then
            frmKillWord.ListView1.ListItems.Remove (aitem.Index)
            Exit For
        End If
    Next

  Call CloseHandle(hSnapShot)
    
    
End Function

Public Function GetHwndFromProcess(p_lngProcessId As Long) As Long
    Dim lngDesktop As Long
    Dim lngChild As Long
    Dim lngChildProcessID As Long
    
    On Error Resume Next
    
    'get the hWnd of the desktop
    lngDesktop = GetDesktopWindow()

    lngChild = GetWindow(lngDesktop, GW_CHILD)
    
    Do While lngChild <> 0
    
        'get the ThreadProcessID of the window
        Call GetWindowThreadProcessId(lngChild, lngChildProcessID)
        
        'if ProcessId matches the parameter passed
        'then return that value
        If lngChildProcessID = p_lngProcessId Then
            GetHwndFromProcess = lngChild
            Exit Do
        End If
        
        'not found, continue enumeration
        lngChild = GetWindow(lngChild, GW_HWNDNEXT)
    Loop
End Function

Public Function EnumWindowProc(ByVal hwnd As Long, _
                               ByVal lParam As Long) As Long
   
  MsgBox ("")
  'working vars
   Dim nSize As Long
   Dim sTitle As String
   Dim sClass As String
   
   Dim sIDType As String
   Dim itmX As ListItem
   Dim nodX As Node
   Dim timX As Integer
   Dim target_process_id As Long
   Dim target_process_handle As Long
   
  'eliminate windows that are not top-level.
   If GetParent(hwnd) = 0& And _
      IsWindowVisible(hwnd) Then
      
     'get the window title / class name
      sTitle = GetWindowIdentification(hwnd, sIDType, sClass)
     If sIDType = "title" And InStr(1, sTitle, "Microsoft Word", vbTextCompare) <> 0 Then
         If Not isKey(CStr(hwnd) & "h") Then
            'add to the listview
            Set itmX = frmKillWord.ListView1.ListItems.Add(Text:=sTitle, Key:=CStr(hwnd) & "h")
            'itmX.SmallIcon = Form1.ImageList1.ListImages("parent").Key
            itmX.SubItems(1) = CStr(hwnd)
            itmX.SubItems(2) = Now
         Else
            timX = DateDiff("n", frmKillWord.ListView1.ListItems(CStr(hwnd) & "h").SubItems(2), Now)
            If timX >= iWait Then
                '-> on reccupere l'ID.
                GetWindowThreadProcessId hwnd, target_process_id
                If target_process_id = 0 Then
                    Exit Function
                End If
                target_process_handle = OpenProcess(SYNCHRONIZE Or PROCESS_TERMINATE, ByVal 0&, target_process_id)
                If target_process_handle = 0 Then
                    Exit Function
                End If
            
                '-> terminer process.
                TerminateProcess target_process_handle, 0&
            
                '-> fermer le process.
                CloseHandle target_process_handle
            Else
                frmKillWord.ListView1.ListItems(CStr(hwnd) & "h").SubItems(3) = timX
            End If
         End If
     End If
   End If
   
  'To continue enumeration, return True
  'To stop enumeration return False (0).
  'When 1 is returned, enumeration continues
  'until there are no more windows left.
   EnumWindowProc = 1
   
End Function


Private Function isKey(sk As String) As Boolean
Dim t As String
On Error GoTo gestError
t = frmKillWord.ListView1.ListItems(sk)
isKey = True
Exit Function
gestError:
isKey = False
End Function

Private Function GetWindowIdentification(ByVal hwnd As Long, _
                                         sIDType As String, _
                                         sClass As String) As String

   Dim nSize As Long
   Dim sTitle As String

  'get the size of the string required
  'to hold the window title
   nSize = GetWindowTextLength(hwnd)
   
  'if the return is 0, there is no title
   If nSize > 0 Then
   
      sTitle = Space$(nSize + 1)
      Call GetWindowText(hwnd, sTitle, nSize + 1)
      sIDType = "title"
      
      sClass = Space$(64)
      Call GetClassName(hwnd, sClass, 64)
   
   Else
   
     'no title, so get the class name instead
      sTitle = Space$(64)
      Call GetClassName(hwnd, sTitle, 64)
      sClass = sTitle
      sIDType = "class"
   
   End If
   
   GetWindowIdentification = TrimNull(sTitle)

End Function

Private Function TrimNull(startstr As String) As String

  Dim pos As Integer

  pos = InStr(startstr, Chr$(0))
  
  If pos Then
      TrimNull = Left$(startstr, pos - 1)
      Exit Function
  End If
  
 'if this far, there was
 'no Chr$(0), so return the string
  TrimNull = startstr
  
End Function

