VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmKillWord 
   BorderStyle     =   0  'None
   Caption         =   "Deal Informatique"
   ClientHeight    =   4755
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5550
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   Moveable        =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   4755
   ScaleWidth      =   5550
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   5000
      Left            =   1320
      Top             =   2880
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   1815
      Left            =   120
      TabIndex        =   3
      Top             =   2400
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   3201
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Label Label6 
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   4320
      Width           =   4095
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Word Killer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   5
      Top             =   360
      Width           =   2535
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4920
      TabIndex        =   4
      Top             =   120
      Width           =   495
   End
   Begin VB.Label Label5 
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   3480
      Width           =   4695
   End
   Begin VB.Label Label3 
      Caption         =   "Quitter"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      MousePointer    =   1  'Arrow
      TabIndex        =   1
      Top             =   4320
      Width           =   975
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   3000
      Width           =   5295
      WordWrap        =   -1  'True
   End
   Begin VB.Image Image1 
      Height          =   2115
      Left            =   120
      Picture         =   "KillWord.frx":0000
      Stretch         =   -1  'True
      Top             =   120
      Width           =   5295
   End
End
Attribute VB_Name = "frmKillWord"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)
Private Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpbuffer As String) As Long
Private Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long
Private Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
'-> API de temporisation
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Private Sub Form_Load()
Init
End Sub

Public Sub Init()
    Dim cmdline As String
    Dim comport As Integer
    Dim setg As String
    Dim Settings
    Dim CommPort
    Dim Handshaking
    Dim Echo
    Dim EOFEnable
    Dim sreg As String
    
    On Error GoTo gestError
       
   With ListView1
      .ColumnHeaders.Add , , "Fen�tre"
      .ColumnHeaders.Add , , "Pid"
      .ColumnHeaders.Add , , "Time"
      .ColumnHeaders.Add , , "Dur�e (mn)"
      .View = lvwReport
   End With
Me.Label6.Caption = "Dur�e maximale (min) : " & iWait
    Exit Sub
    
gestError:
    
End Sub

Private Sub Label3_Click()
    '-> on ferme le programme
    End

End Sub

Private Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Integer
Dim PosEnCour As Integer
Dim i As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
End Function

Private Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Integer

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Integer
Dim i As Integer
Dim PosAnalyse As Integer

On Error GoTo gestError

'-< Analyse du vecteur
PosAnalyse = 1
Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1
'-> Quitter la fonction
Exit Function

gestError:
    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
End Function

Private Sub Label4_Click()
 Me.WindowState = 1
 Load frmSysTray

End Sub

Private Sub Timer1_Timer()
Dim aitem As ListItem
Dim hwnd As Long
Dim target_process_id As Long
Dim target_process_handle As Long

    On Error Resume Next
    Err.Description = ""
    Call doShowProcessList
    'Call EnumWindows(AddressOf EnumWindowProc, &H0)
    For Each aitem In Me.ListView1.ListItems
                target_process_id = aitem.SubItems(1)
                If target_process_id = 0 Then
                    Me.ListView1.ListItems.Remove (aitem.Index)
                End If
                target_process_handle = OpenProcess(SYNCHRONIZE Or PROCESS_TERMINATE, ByVal 0&, target_process_id)
                If target_process_handle = 0 Then
                    Me.ListView1.ListItems.Remove (aitem.Index)
                End If
    
    Next
    
    Me.Label6.Caption = "Timer => " & Err.Description
End Sub

