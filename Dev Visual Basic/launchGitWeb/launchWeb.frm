VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Begin VB.Form frmWeb 
   BorderStyle     =   0  'None
   Caption         =   "Deal Informatique"
   ClientHeight    =   4305
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5535
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   Moveable        =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   4305
   ScaleWidth      =   5535
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   20
      Left            =   4920
      Top             =   2400
   End
   Begin SHDocVwCtl.WebBrowser WebBrowser 
      Height          =   975
      Left            =   4560
      TabIndex        =   0
      Top             =   2760
      Width           =   975
      ExtentX         =   1720
      ExtentY         =   1720
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
   Begin VB.Image Image2 
      Height          =   1725
      Left            =   3480
      Picture         =   "launchWeb.frx":0000
      Top             =   2400
      Width           =   1845
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "GIT Console"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   120
      TabIndex        =   1
      Top             =   2520
      Width           =   3735
   End
   Begin VB.Image Image1 
      Height          =   2115
      Left            =   120
      Picture         =   "launchWeb.frx":9C96
      Stretch         =   -1  'True
      Top             =   120
      Width           =   5295
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "GIT Console"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   735
      Left            =   162
      TabIndex        =   2
      Top             =   2560
      Width           =   3735
   End
End
Attribute VB_Name = "frmWeb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)
Private Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Private Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long
Private Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
'-> API de temporisation
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" _
   (ByVal hwnd As Long, ByVal nIndex As Long) As Long
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" _
   (ByVal hwnd As Long, ByVal nIndex As Long, _
   ByVal dwNewLong As Long) As Long
Private Const GWL_STYLE = (-16)
Private Const GWL_EXSTYLE = (-20)
Private Declare Function SetLayeredWindowAttributes Lib "user32" _
   (ByVal hwnd As Long, ByVal crKey As Long, _
   ByVal bAlpha As Byte, ByVal dwFlags As Long) As Long
Private Const LWA_COLORKEY = &H1
Private Const LWA_ALPHA = &H2
Private Const WS_EX_LAYERED = &H80000


Public Sub Init()
    frmWeb.WebBrowser.Navigate (Command$)
    Dim vReadyState As Integer
    frmWeb.WebBrowser.Silent = True
    Do Until vReadyState = 4
        DoEvents
        vReadyState = frmWeb.WebBrowser.ReadyState
    Loop
    frmWeb.WebBrowser.Silent = True
    frmWeb.WebBrowser.Visible = False
    DoEvents
    Sleep 500
    Timer1.Enabled = True
    
End Sub

Public Function fade()
    Dim m_lAlpha As Integer
    Dim lAlpha
    m_lAlpha = 0
    
    Dim lStyle As Long
    lStyle = GetWindowLong(Me.hwnd, GWL_EXSTYLE)
    lStyle = lStyle Or WS_EX_LAYERED
    SetWindowLong Me.hwnd, GWL_EXSTYLE, lStyle
    SetLayeredWindowAttributes Me.hwnd, 0, 0, LWA_ALPHA
    Do
        DoEvents
        Sleep 30
        m_lAlpha = m_lAlpha + 25
        lAlpha = m_lAlpha
        If (lAlpha > 255) Then
           lAlpha = 255
        End If
        SetLayeredWindowAttributes Me.hwnd, 0, lAlpha, LWA_ALPHA
        If (m_lAlpha > 255) Then
           Exit Do
        End If
    Loop
   
    Do
        DoEvents
        Sleep 30
        m_lAlpha = m_lAlpha - 25
        lAlpha = m_lAlpha
        If (lAlpha < 0) Then
           lAlpha = 0
        End If
        SetLayeredWindowAttributes Me.hwnd, 0, lAlpha, LWA_ALPHA
        If (m_lAlpha < 0) Then
           End
        End If
    Loop
End Function

Private Sub Form_Load()
    Init
End Sub

Private Sub Timer1_Timer()
    fade
End Sub
