package excel;

/**
 * <p>Titre : SuiviDeLien</p>
 * <p>Description : Classe permettant de r�cup�rer les liens hypertextes des fichiers HTML</p>
 * <p>Copyright : Copyright (c) 2004</p>
 * <p>Soci�t� : BakaOne</p>
 * @author Indiana_jules
 * @version 1.0
 */

//Importation des packages n�cessaires
import java.awt.Cursor;
import javax.swing.JEditorPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class FollowLink implements HyperlinkListener{
  //Variables globales
  private JEditorPane panneau;
  private Cursor main = new Cursor(Cursor.HAND_CURSOR);
  private Cursor defaut = new Cursor(Cursor.DEFAULT_CURSOR);

  /**Constructeur par d�faut*/
  public FollowLink() {
  }

  /**Constructeur
   * @param panneau Indiquation du JEditorPane de destination*/
  public FollowLink(JEditorPane panneau) {
    this.panneau = panneau;
  }

  /**M�thode retournant le JEditorPane
   * @return Retour du JEditorPane destination*/
  public JEditorPane getPanneau() {
    return panneau;
  }

  /**m�thode permettant d'agir lors d'un clique sur un lien
   * @param evt Gestionnaire d'�v�nement*/
  public void hyperlinkUpdate(HyperlinkEvent evt) {

    /*permet de changer le pointeur de la souris lors du survol d'un lien
        hypertext*/
    if(evt.getEventType()==HyperlinkEvent.EventType.ENTERED) {
      panneau.setCursor(main);
    }
    /*permet de changer le pointeur de la souris en pointeur normal apr�s
        le survol d'un lien hypertext*/
    else {
      panneau.setCursor(defaut);
    }

    //si on clique sur un lien hypertexte, on l'affiche soit dans le JEditorPane
    if(evt.getEventType()==HyperlinkEvent.EventType.ACTIVATED) {
      try {
        panneau.setPage(evt.getURL());
      }
      catch (Exception e) {
      }
    }
  }

  /**M�thode permettant de d�finir le JEditorPane destination
   * @param panneau JEditorPane destination*/
  public void setPanneau(JEditorPane panneau) {
    this.panneau = panneau;
  }
}