package excel;

/**
 * <p>Titre : MyTableModel</p>
 * <p>Description : Classe d�finissant un mod�le de table</p>
 * <p>Copyright : Copyright (c) 2004</p>
 * <p>Soci�t� : BakaOne</p>
 * @author Indiana_jules
 * @version 1.0
 */

//Importation des packages n�cessaires
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

public class MyTableModel extends DefaultTableModel{
  //Variables globales
  private boolean[] editable;

  /**Constructeur par d�faut
   * @param lesTitres Titres des colonnes
   * @param dta Donn�es
   * @param editable Permet de d�signer les colonnes �ditables*/
  public MyTableModel(Vector lesTitres, Vector dta, boolean[] editable) {
    super(dta, lesTitres);

    //Initialisation des �ditions
    this.editable = editable;
  }

  /**M�thode retournant le type de classe dans la colonne
   * @param columnIndex Num�ro de la colonne
   * @return Type de classe*/
  public Class getColumnClass(int columnIndex){
    return getValueAt(0, 0).getClass();
  }

  /**M�thode permettant de savoir si cette cellule est �ditable ou pas
   * @param row Ligne
   * @param col Colonne
   * @return permet de savoir si �ditable ou non*/
  public boolean isCellEditable(int row, int col) {
    try{
      return editable[col];
    }
    catch(Exception e) {
      return false;
    }
  }
}