package excel;

/**
 * <p>Titre : JMyFileChooser</p>
 * <p>Description : Classe permettant d'afficher un explorateur que pour des
 * fichiers excel .xsl</p>
 * <p>Copyright : Copyright (c) 2004</p>
 * <p>Soci�t� : BakaOne</p>
 * @author  Indiana_jules
 * @version 1.0
 */

//Importation des packages n�cessaires
import javax.swing.JFileChooser;

public class JMyFileChooser extends JFileChooser{
  //Constantes
  /**Dialogue d'ouverture*/
  public static int OPEN_DIALOG = 1;
  /**Dialogue de fermeture*/
  public static int SAVE_DIALOG = 2;

  /**Constructeur par d�faut*/
  public JMyFileChooser() {
    super();
    jbinit(OPEN_DIALOG);
  }

  /**Constructeur
   * @parame type Type de JFileChooser*/
  public JMyFileChooser(int type) {
    super();

    if(type == SAVE_DIALOG){
      jbinit(SAVE_DIALOG);
    }
    else{
      jbinit(OPEN_DIALOG);
    }
  }

  /**M�thode permettant d'initialiser la classe
   * @param type Type de JFileChooser*/
  protected void jbinit(int type) {
    //Initialisation
    this.setFileSelectionMode(JFileChooser.FILES_ONLY);

    //D�finition des extensions
      JFileFilter filtre = new JFileFilter();
      filtre.addType("xls");
      filtre.setDescription("Fichiers excel: xls");

    //Configuration selon le type de dialogue
    if(type == OPEN_DIALOG){
      this.setDialogType(JFileChooser.OPEN_DIALOG);
      this.setDialogTitle("Ouvrir un fichier excel");
      this.setApproveButtonText("Ouvrir");
    }
    else{
      this.setDialogType(JFileChooser.SAVE_DIALOG);
      this.setDialogTitle("Sauver dans un fichier excel");
      this.setApproveButtonText("Sauver");
    }

    this.addChoosableFileFilter(filtre);
    this.setMultiSelectionEnabled(false);
    this.setAcceptAllFileFilterUsed(false);
  }
}