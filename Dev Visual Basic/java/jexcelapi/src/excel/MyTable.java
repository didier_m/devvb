package excel;

/**
 * <p>Titre : MyTable</p>
 * <p>Description : Classe d�finissant ma table</p>
 * <p>Copyright : Copyright (c) 2004</p>
 * <p>Soci�t� : BakaOne</p>
 * @author Indiana_jules
 * @version 1.0
 */

//Importation des packages n�cessaires
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

public class MyTable extends JTable{
  //Variables globales
  private MyTableModel model;

  /**Constructeur par d�faut*/
  public MyTable() {
    super();
  }

  /**Constructeur
   * @param data Donn�es*/
  public MyTable(Vector data) {
    super();
    jbinit(data, defaultTitle(data.size()), defaultStatut(data.size()));
  }

  /**Constructeur
   * @param data Donn�es
   * @param titre Titre des colonnes*/
  public MyTable(Vector data, Vector titre) {
    super();
    jbinit(data, titre, defaultStatut(data.size()));
  }

  /**Constructeur
   * @param data Donn�es
   * @param titre Titre des colonnes
   * @param editable Quelle colonne sont �ditables*/
  public MyTable(Vector data, Vector titre, boolean[] editable) {
    super();
    jbinit(data, titre, editable);
  }

  /**M�thode permettant d'ajouter une nouvelle ligne
   * @param newLigne Nouvelle ligne de donn�es*/
  public void addRow(Vector newLigne) {
    model.addRow(newLigne);
    model.fireTableDataChanged();
  }


  /**M�thode permettant d'ajouter une nouvelle ligne
   * @param newLigne Nouvelle ligne de donn�es
   * @param index A quelle ligne*/
  public void addRow(Vector newLigne, int index) {
   model.insertRow(index, newLigne);
   model.fireTableDataChanged();
  }

  /**M�thode permettant d'initialiser par d�faut les statuts des colonnes
   * @param number Nombres de colonnes
   * @return tableau r�sultant*/
  protected boolean[] defaultStatut(int number) {
    boolean[] edit = new boolean[number];
    for(int i = 0; i < number; i++){
      edit[i] = false;
    }
    return edit;
  }


  /**M�thode permettant d'initialiser par d�faut les titres des colonnes
   * @param number Nombres de colonnes
   * @return tableau r�sultant*/
  protected Vector defaultTitle(int number) {
    Vector titre = new Vector();
    for(int i = 0; i < number; i++){
      titre.add("");
    }
    return titre;
  }

  /**M�thode permettant de supprimer une ligne
   * @param index Suppression de ligne*/
  public void deleteRow(int index) {
    model.removeRow(index);
    model.fireTableDataChanged();
  }

  /**M�thode permettant de r�cup�rer les donn�es
   * @return Donn�es*/
  public Vector getData() {
    return model.getDataVector();
  }

  /**M�thode permettant d'initialiser la classe
   * @param data Donn�es
   * @param titre Les titres des colonnes
   * @param editable Quelle colonne sont �ditables*/
  protected void jbinit(Vector data, Vector titre, boolean[] editable) {
    //D�finition du  mod�le
    model = new MyTableModel(titre, data, editable);

    //Initialisation de la table
    this.setModel(model);
    this.setAutoResizeMode(this.AUTO_RESIZE_ALL_COLUMNS);
    this.setRowSelectionAllowed(false);
    this.setRowHeight(15);
  }

  /**M�thode permettant de rafra�chir la table*/
  public void refreshAll() {
    model.fireTableDataChanged();
  }

  /**M�thode permettant d'affecter les donn�es
   * @param data Donn�es*/
  public void setData(Vector data) {
    model.setDataVector(data, defaultTitle(data.size()));
  }

  /**M�thode permettant d'affecter les donn�es
   * @param data Donn�es
   * @param titre Titres des colonnes*/
  public void setData(Vector data, Vector titre) {
    model.setDataVector(data, titre);
  }
}