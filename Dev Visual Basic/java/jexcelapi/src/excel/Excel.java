package excel;

/**
 * <p>Titre : Excel</p>
 * <p>Description : Classe permettant de tester le package jxl</p>
 * <p>Copyright : Copyright (c) 2004</p>
 * <p>Soci�t� : BakaOne</p>
 * @author Indiana_jules
 * @version 1.0
 */

//Importation des packages n�cessaires
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class Excel extends JPanel{
  //Variables globales
  private MyTable table;

  //Constantes
  /**Permet d'indiquer si on ouvre un fichier excel*/
  public static final int OPEN = 1;
  /**Permet d'indiquer si on enregistre un fichier excel*/
  public static final int SAVE = 2;

  /**Constructeur par d�faut*/
  public Excel() {
    super(new BorderLayout());
  }

  /**Constructeur
   * @param typeOp Permet d'indiquer si on veut voir un exemple d'enregistrement
   * ou un exemple de sauvegarde de fichier excel*/
  public Excel(int typeOp) {
    super(new BorderLayout());
    jbinit(typeOp);
  }

  /**M�thode permettant d'initialiser la classe
   * @param typeOp Permet d'indiquer si on veut voir un exemple d'enregistrement
   * ou un exemple de sauvegarde de fichier excel*/
  protected void jbinit(int typeOp) {
    //Variables n�cessaire
    JButton bouton;

    if(typeOp == this.OPEN){ //Nous allons ouvrir un fichier excel
      table = new MyTable(new Vector());

      bouton = new JButton("Ouvrir un fichier excel");
      bouton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          JMyFileChooser choose = new JMyFileChooser(JMyFileChooser.OPEN_DIALOG);
          int valeur = choose.showOpenDialog(null);

          if(valeur == JMyFileChooser.APPROVE_OPTION){
            /*Nous allons v�rifier que le fichier se termine bien par
            l'extension xls*/
            String filename = choose.getSelectedFile().getAbsolutePath();

            if(!filename.endsWith(".xls")){
              filename = filename + ".xls";
            }

            //Nous allons r�cup�rer les donn�es du fichier xls
            Vector data = new Vector();
            try{
              //1�re �tape : nous cr�eons le Workbook
              Workbook workbook = Workbook.getWorkbook(new File(filename));

              //2nd �tape : nous allons r�cup�rons les donn�es de la 1�re page du classeur
              Sheet sheet = workbook.getSheet(0);

              //3�me �tape : nous lisons les cellules et nous les disposons dans la table
              Vector tempo;
              for(int i = 0; i < sheet.getRows(); i++){
                tempo = new Vector();
                for(int j = 0; j < sheet.getColumns(); j++){
                  tempo.add(sheet.getCell(j, i).getContents());
                }

                data.add(tempo);
              }

              table.setData(data);
              table.refreshAll();

              //4�me �tape : fermeture du flux
              workbook.close();
            }
            catch(Exception ex) {
              JOptionPane.showMessageDialog(null, "Impossible d'ouvrir le fichier xls", "Erreur", JOptionPane.ERROR_MESSAGE);
            }
          }
        }
      });
    }
    else{ //Nous allons sauver dans un fichier excel
      //Nous allons cr�er un data
      Vector v = new Vector();
      Vector a = new Vector();
      a.add("1");
      a.add("2");
      a.add("3");
      a.add("4");
      Vector b = new Vector();
      b.add("5");
      b.add("6");
      b.add("7");
      b.add("8");
      Vector c = new Vector();
      c.add("9");
      c.add("10");
      c.add("11");
      c.add("12");
      Vector d = new Vector();
      d.add("13");
      d.add("14");
      d.add("15");
      d.add("16");
      v.add(a);
      v.add(b);
      v.add(c);
      v.add(d);

      table = new MyTable(v);

      bouton = new JButton("Sauver dans un fichier excel");
      bouton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          JMyFileChooser choose = new JMyFileChooser(JMyFileChooser.SAVE_DIALOG);
          int valeur = choose.showSaveDialog(null);

          if(valeur == JMyFileChooser.APPROVE_OPTION){
            //Nous r�cup�rons les donn�es
            Vector data = table.getData();

            /*Nous allons v�rifier que le fichier se termine bien par
           l'extension xls*/
           String filename = choose.getSelectedFile().getAbsolutePath();

           if(!filename.endsWith(".xls")){
             filename = filename + ".xls";
           }

           //Nous allons cr�er le fichier xls
           try{
             //1�re �tape : cr�ation du WritableWorkbook
             WritableWorkbook workbook = Workbook.createWorkbook(new File(
                  filename));

             //2nd �tape : cr�ation de la 1�re page du classeur xls
             WritableSheet sheet = workbook.createSheet("First sheet", 0);

             //3�me �tape : ins�rer les informations de la table dans le classeur
             Label label;
             Vector tempo;

             for(int i = 0; i < data.size(); i++){
               tempo = (Vector)data.get(i);

               for(int j = 0; j < tempo.size(); j++){
                 label = new Label(j, i, tempo.get(j).toString());
                 sheet.addCell(label);
               }
             }

             /*4�me �tape : apr�s avoir ins�rer les cellules, nous cr�eons le fichier
             et nous fermons le flux*/
             workbook.write();
             workbook.close();
           }
           catch(Exception ex) {
             JOptionPane.showMessageDialog(null, "Impossible de cr�er le fichier xls", "Erreur", JOptionPane.ERROR_MESSAGE);
           }
          }
        }
      });
    }

    this.add("Center", new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
    this.add("South", bouton);
  }
}