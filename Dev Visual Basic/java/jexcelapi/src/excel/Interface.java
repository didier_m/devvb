package excel;

/**
 * <p>Titre : Interface</p>
 * <p>Description : Classe permettant de cr�er l'interface graphique</p>
 * <p>Copyright : Copyright (c) 2004</p>
 * <p>Soci�t� : BakaOne</p>
 * @author Indiana_jules
 * @version 1.0
 */

//Importation des packages n�cessaires
import java.awt.Dimension;
import java.io.File;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

public class Interface extends JFrame{
  //D�claration des variables globales
  JScrollPane documentation, gnu, help;
  JTabbedPane tabbedPane;

  /**Constructeur par d�faut*/
  public Interface() {
    //D�finition de la fen�tre
    super("Test du package jexcel");
    this.setSize(700, 550);
    this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
    centerFenetre();

    //Iniatialisation des variables
    tabbedPane = new JTabbedPane();

    try{
      //Initialisation des fichiers d'aide
      File f = new File("./information/tutorial.html");
      File f1 = new File("./information/help.txt");
      File f2 = new File("./information/GNU Lesser General Public License - GNU Project - Free Softw.htm");

      //Initialisation des JEditorPane
      JEditorPane editDocumentation = new JEditorPane();
      JEditorPane editGNU = new JEditorPane();
      JEditorPane editHelp = new JEditorPane();

      //Initialisation des fichiers � afficher
      editDocumentation.setEditable(false);
      editDocumentation.setPage("file:"+f.getAbsolutePath());
      editGNU.setEditable(false);
      editGNU.setPage("file:"+f2.getAbsolutePath());
      editHelp.setEditable(false);
      editHelp.setPage("file:"+f1.getAbsolutePath());

      //Initialisation des gestionnaires d'�v�nements
      editDocumentation.addHyperlinkListener(new FollowLink(editDocumentation));
      editGNU.addHyperlinkListener(new FollowLink(editHelp));
      editHelp.addHyperlinkListener(new FollowLink(editHelp));

      //Initialisation des JScrollPane
      documentation = new JScrollPane(editDocumentation, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      gnu = new JScrollPane(editGNU, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      help = new JScrollPane(editHelp, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    }
    catch(Exception e){

    }

    //Agencement graphique
    tabbedPane.add("Ouvrir", new Excel(Excel.OPEN));
    tabbedPane.add("Sauvegarder", new Excel(Excel.SAVE));
    tabbedPane.add("Tutorial", documentation);
    tabbedPane.add("Licence GNU", gnu);
    tabbedPane.add(" -?- ", help);

    this.getContentPane().add(tabbedPane);

    //On affiche la fen�tre
    this.setVisible(true);
  }

  /**M�thode permettant de center la fen�tre*/
  public void centerFenetre() {
    Dimension them = this.getToolkit().getScreenSize();
    Dimension us = this.getSize();
    this.setLocation((them.width - us.width)/2,(them.height - us.height)/2);
  }
}