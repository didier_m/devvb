/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package deal.export;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.File;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import jxl.*;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.*;

/**
 *
 * @author didier_m
 */
public class exportXLS extends HttpServlet {
   
    /** 
    * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
    * @param request servlet request
    * @param response servlet response
    */
    //On se declare les variable qui sont ramenées en parametre
    private String sFileName; 
    private String sFileNameRel; 
    private String sSheetName;
    private String sXpos;
    private String sYpos;
    private String sValueHeader;
    private String sValueBody;
    private String sBackColorHeader;
    private String sBackColorBody;
    private String sColonnesWidth;
    private String sColonnesAlign;
    private WritableWorkbook workbook = null;
    private int iColor = 10;
    private jxl.format.Colour[] oColor ;
    private String[] sAlign ;
    private String sColor = "";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            // TODO output your page here
            out.println("<script language=javascript>");
            //sFileName = "http://www.google.fr/";
            out.println("window.open('" + sFileNameRel + "','','')");
            out.println("</script>");
            //reponse.sendRedirect(sFileNameRel);
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
    * Handles the HTTP <code>GET</code> method.
    * @param request servlet request
    * @param response servlet response
    */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
    * Handles the HTTP <code>POST</code> method.
    * @param request servlet request
    * @param response servlet response
    */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {        
         try {
            //lecture des données
            setVar(request);
            //construction du fichier tableur
            addTableur();
            //on pointe sur le tableur
            //response.sendRedirect(sFileNameRel);
            //on retourne la reponse
            processRequest(request, response);
       } catch (BiffException ex) {
            Logger.getLogger(exportXLS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (WriteException ex) {
            Logger.getLogger(exportXLS.class.getName()).log(Level.SEVERE, null, ex);
        }
     }

    /** 
    * Returns a short description of the servlet.
    */
    public String getServletInfo() {
        return "Export de données vers un tableur";
    }
    // </editor-fold>

 private void setVar(HttpServletRequest request){
     //-> ici on reccupere toute les variables qui nous viennent de la page web 
     //et qui seront utilisées par la suite pour generer le fichier de type Excel/Open Office
     
     sFileName=request.getParameter("filename").toString(); 
     sFileNameRel=request.getParameter("filenamerel").toString(); 
     sSheetName=request.getParameter("sheetname").toString();
     sXpos=request.getParameter("xpos").toString();
     sYpos=request.getParameter("ypos").toString();
     sValueHeader=request.getParameter("cellvalueheader").toString();
     sValueBody=request.getParameter("cellvaluebody").toString();
     sBackColorHeader=request.getParameter("backcolorheader").toString();
     sBackColorBody=request.getParameter("backcolorbody").toString();
     sColonnesWidth=request.getParameter("colonneswidth").toString();
     sColonnesAlign=request.getParameter("colonnesalign").toString();
     
     System.out.println("DEAL INFORMATIQUE"); //debug
     System.out.println("Export des données vers le tableur"); //debug
     /*System.out.println(sColonnesWidth); //debug
     System.out.println(sFileName); //debug
     System.out.println(sXpos); //debug
     System.out.println(sYpos); //debug
     System.out.println(sBackColorHeader); //debug
     System.out.println(sBackColorBody); //debug
     System.out.println(sValueHeader); //debug
     System.out.println(sValueBody); //debug*/

     //on gere l'absence de valeurs
     if(sFileName == null||sFileName.equals("")) sFileName = this.getServletContext().getRealPath("") + "exportexcel.xls";
     if(sFileNameRel == null||sFileNameRel.equals("")) sFileNameRel = this.getServletContext().getRealPath("") + "exportexcel.xls";
     if(sSheetName == null||sSheetName.equals("")) sSheetName = "Feuille export";
     //if(sValueHeader == null) return;
     //if(sValueBody == null) return;
     if(sBackColorHeader == null||sBackColorHeader.equals("")) sBackColorHeader = "#72A1FF";
     if(sBackColorBody == null||sBackColorBody.equals("")) sBackColorBody = "#FFFFFF";
     if(sXpos == null) sXpos = "0";
     if(sYpos == null) sYpos = "0";
     
 } 
  
 private void addTableur() throws IOException, BiffException, WriteException{
     //-> on initialise le fichier de type Excel selon qu'il existe ou pas
     WritableSheet s = null;
     boolean aBol = true;
     
     //On initialise l'objet fichier
     File txtFile = new File(sFileName); 
     //On se cree la feuille de type Excel en initialisant l'objet s
     if (txtFile.createNewFile()) {
          // on créé le fichier
          WorkbookSettings ws = new WorkbookSettings();
          ws.setLocale(new Locale("fr", "FR"));
          workbook = Workbook.createWorkbook(new File(sFileName), ws);
          //On cree la feuille excel si elle n'existe pas
          s = workbook.createSheet(sSheetName, 0);
     } else {
          // on rappelle le fichier on le charge en memoire
          Workbook workbookOld = Workbook.getWorkbook(new File(sFileName));
          // on créé le fichier
          workbook = Workbook.createWorkbook(new File(sFileName+".tmp"), workbookOld);
          s = workbook.getSheet(sSheetName);
          // on indique que l'on travaille sur un fichier tempo
          aBol = false;
     }
     
     //on converti les valeurs de la position du debut
     int xPos = Integer.parseInt(sXpos); 
     int yPos = Integer.parseInt(sYpos); 

     //-> on se ramene l'alignement des colonnes
     sAlign = sColonnesAlign.split(";");
     
     //-> on initialise la palette des couleur
     oColor = jxl.format.Colour.getAllColours();

     //-> on se cree si besoin l'entete (on incremente la position y)     
     if(sValueHeader!=null&&!sValueHeader.equals("")) yPos = addTableau(xPos, yPos, sValueHeader, s, sBackColorHeader);
     //-> on se cree si besoin le corps
     if(sValueBody!=null&&!sValueBody.equals("")) addTableau(xPos, yPos, sValueBody, s, sBackColorBody);
     //On ferme le fichier
     workbook.write();
     workbook.close();                          
     
     //Si on a travaillé sur un fichier temporaire on le supprime 
     if(!aBol) {
          //On supprime le fichier
          txtFile.delete();
          //On ecrase le fichier d'origine
          File txtFile2 = new File(sFileName+".tmp");
          txtFile2.renameTo(txtFile);         
     }
 } 
  
 private int addTableau(int xPos, int yPos, String sValue,WritableSheet s, String sBackColor) throws WriteException{
      //-> on ajoute le tableau excel en preparant les valeurs recues
      jxl.format.Colour cColor;
      int cc = 0;
      int top = 0;
      //->on fait un replace des delimiteurs de tableau
      sValue = sValue.replaceAll("</TD>", ""); 
      sValue = sValue.replaceAll("</TR>", ""); 
      sValue = sValue.replaceAll("</TBODY>", ""); 
      sValue = sValue.replaceAll("</td>", ""); 
      sValue = sValue.replaceAll("</tr>", ""); 
      sValue = sValue.replaceAll("</tbody>", ""); 
      sValue = sValue.replaceAll("<td", "<TD"); 
      sValue = sValue.replaceAll("<tr", "<TR"); 

      sValue = sValue.replaceAll("[\n\r]+", ""); 
 
      String[] splitLigne = sValue.split("<TR");
      //on travaille sur la largeur des colonnes
      if(splitLigne.length == 1) return xPos;
      String[] splitValue = splitLigne[1].split("<TD");
      
      //-> On se donne une largeur des colonnes par defaut
      for (int x=0; x<splitValue.length-1; x++){
          //-> on met la largeur en automatique par defaut
          s.setColumnView(x,20);                  
      }
      if(sColonnesWidth!=null&&!sColonnesWidth.equals("")){
          //-> si on a passe des valeurs
          String spCol[]=sColonnesWidth.split(";");
          for (int i=0; i<spCol.length;i++){
              if(!spCol[i].equals(""))s.setColumnView(i,Integer.parseInt(spCol[i])/7+2);    
          }
      }
      //->on eclate les couleurs par lignes
      String[] spBackColor = sBackColor.split(";");
      
      //->on eclate le tableau par ligne
      for (int y=1; y<splitLigne.length; y++){
          //-> On se definit la couleur de fond pour la ligne en cours
          String RGBcolor = hexaToRGB(spBackColor[y-1]);
          String[] sRGB = RGBcolor.split(",");
          //on regarde si on a pas deja eu cette couleur
          String[] spColor = sColor.split(";");
          cColor = oColor[11];
          top = 0;
          for(cc=0; cc<spColor.length-1; cc++){
              if(spColor[cc].indexOf(RGBcolor)!=-1){
                  workbook.setColourRGB(oColor[cc+11], Integer.parseInt(sRGB[0]), Integer.parseInt(sRGB[1]), Integer.parseInt(sRGB[2]));
                  cColor = oColor[cc+11];
                  top = 1;
              }
          }
          //si on a rien trouvé on rajoute la couleur
          if(top==0){
              iColor++;
              workbook.setColourRGB(oColor[iColor], Integer.parseInt(sRGB[0]), Integer.parseInt(sRGB[1]), Integer.parseInt(sRGB[2]));
              cColor = oColor[iColor];
              if(sColor.equals(""))
                  sColor = "" + iColor + "|" + RGBcolor;
              else
                  sColor = sColor + ";" + iColor + "|" + RGBcolor;
          }
          //on sort de la balise <TR......   >
          int pos = splitLigne[y].indexOf(">");
          if(pos!=-1)splitLigne[y] = splitLigne[y].substring(pos);
          addLigne(xPos, yPos + y-1, splitLigne[y], s, cColor);
      }
      //on retourne le nombre de lignes que l'on a ajouter
      if(splitLigne.length >= 1) 
          return yPos + splitLigne.length-1;
      else
          return yPos;
 } 
  
 private void addLigne(int xPos, int y ,String splitLigne, WritableSheet s, jxl.format.Colour sBackColor) throws WriteException{
      //-> on eclate le tableau par cellule et on cree les cellules de la ligne
      String[] splitValue = splitLigne.split("<TD");
      for (int x=1; x<splitValue.length; x++){
          //On récupère la valeur cellule du tableau
          String[] cellValue = splitValue[x].split(">");
          String cValue = cellValue[cellValue.length-1];
          //On ajoute la cellule
          addCell(x + xPos -1, y, cValue, s, sBackColor);
      }
 }
 
 private void addCell(int x, int y, String cellValue, WritableSheet s, jxl.format.Colour sBackColor) throws WriteException{
    //-> On créé ici la cellule avec sa valeur et son formatage
    WritableCellFormat sCell = new WritableCellFormat();
    sCell.setBorder(Border.ALL, BorderLineStyle.THIN);
    sCell.setBackground(sBackColor); 
    if(isNumber(cellValue)&&isRightAlign(x)){
        cellValue = cellValue.replaceAll( ""+(char)160, "");
        Number n = new Number(x, y, Double.parseDouble(cellValue.trim()), sCell);
        s.addCell(n);
    } else {
        Label l = new Label(x, y, cellValue, sCell);
        s.addCell(l);
    }
 }
 
private String hexaToRGB(String sHexa){
    //-> Cette fonction retourne la couleur Excel la plus proche (sur les 56) à partir de l'hexa
     //on se donne la valeur rgb des couleurs
     if(sHexa==null) return "255,255,255";
     if(sHexa.equals("transparent")) return "255,255,255";
     if(sHexa.equals("white")) return "255,255,255";
     //dans le cas de firefox la couleur est aussi ramenee 'rgb(r,g,b)'
     if(sHexa.startsWith("rgb")){
        String sTemp = sHexa.replace("rgb(", "");
        sTemp = sTemp.replace(")", "");
        sTemp = sTemp.replace(" ", "");
        return sTemp;
     } else {
         try {
         int R = Integer.parseInt(sHexa.substring(1, 3),16);
         int G = Integer.parseInt(sHexa.substring(3, 5), 16);
         int B = Integer.parseInt(sHexa.substring(5, 7), 16);
         //on retourne les valeurs de la palette
         R=getValuePalette(R);
         G=getValuePalette(G);
         B=getValuePalette(B);

         //par défaut le blanc
         return R+ "," + G + "," + B;
         } catch(Exception e){
             return "255,255,255";
         }    
     }
}

private int getValuePalette(int iValue){
     //on retrouve la valeur la plus proche de la palette par defaut d'excel
     if(iValue<=25)iValue=0;
     if(iValue>25&&iValue<=76)iValue=51;
     if(iValue>76&&iValue<=115)iValue=102;
     if(iValue>115&&iValue<=146)iValue=128;
     if(iValue>146&&iValue<=172)iValue=153;
     if(iValue>172&&iValue<=198)iValue=192;
     if(iValue>198&&iValue<=230)iValue=204;
     if(iValue>230&&iValue<=255)iValue=255;  
     return iValue;
    }

private static boolean isNumber(String s) {
    //on regarde si une chaine est numerique
    //on supprime les blancs
    s = s.trim();
    s = s.replaceAll( ""+(char)160, "");
    try{
        double d = Double.parseDouble(s);
        return true;}
        catch(NumberFormatException nfe) {
    }
    return false;
    }

private boolean isRightAlign(int x) {
    //on regarde si l'alignement est à droite
    try{
        if(sAlign[x].equals("right")) return true;
    }
        catch(Exception e) {
            return true;
    }
    return false;
    }


}
