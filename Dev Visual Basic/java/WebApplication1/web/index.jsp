<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<HEAD>
   <TITLE>Export vers tableur</TITLE>
</HEAD>
    <body >
        <form action="ExportServlet" name="ExportTableur" method="POST">
            <script language=javascript>
                //on se recupere les infos dans la feuille parent
                tabheader = parent.tabheader;
                tabbody = parent.tabbody;
                filename = parent.filename;
                filenamerel = parent.filenamerel;
                aRemplacer = /\"/g;
                //on se cree un vecteur des largeurs de colonne
                sLargeur = "";
                sBackcolorBody = "";
                sBackcolorHeader = "";
                sAlign = "";
                if(parent.document.getElementById(tabheader)!=null){
                    alert("Parent trouvé");
                    try {
                            if(parent.document.getElementById(tabheader).rows.item(0).cells.item(0)) sBackcolorHeader = parent.document.getElementById(tabheader).rows.item(0).cells.item(0).currentStyle.backgroundColor;
                            } catch(ex) {
                            if(parent.document.getElementById(tabheader).rows.item(0).cells.item(0)) sBackcolorHeader = window.getComputedStyle(parent.document.getElementById(tabheader).rows.item(0),'').backgroundColor;
                    }
                    try {
                            if(parent.document.getElementById(tabbody).rows.item(0)!=null) sBackcolorBody = parent.document.getElementById(tabbody).rows.item(0).cells.item(0).currentStyle.backgroundColor;
                            } catch(ex) {
                            if(parent.document.getElementById(tabbody).rows.item(0)!=null) sBackcolorBody = window.getComputedStyle(parent.document.getElementById(tabbody).rows.item(0),'').backgroundColor;
                    }

                    if(parent.document.getElementById(tabheader).rows.item(0)!=null){
                            for(i=0; i<parent.document.getElementById(tabheader).rows.item(0).cells.length;i++){
                                    if(sLargeur!="")
                                            sLargeur = sLargeur + ";" + parent.document.getElementById(tabheader).rows.item(0).cells.item(i).clientWidth;
                                    else
                                            sLargeur = parent.document.getElementById(tabheader).rows.item(0).cells.item(i).clientWidth;
                            }
                    }
                    //on se cree un vecteur pour l'alignement des colonnes

                    if(parent.document.getElementById(tabbody).rows.item(0)!=null){
                            for(i=0; i<parent.document.getElementById(tabbody).rows.item(0).cells.length;i++){
                                    if(sAlign!="")
                                            sAlign = sAlign + ";" + parent.document.getElementById(tabbody).rows.item(0).cells.item(i).align;
                                    else
                                            sAlign = parent.document.getElementById(tabheader).rows.item(0).cells.item(i).align
                            }
                    }

                    document.write("	<input name=colonneswidth VALUE=\"" + sLargeur + "\">");
                    document.write("	<input name=colonnesalign VALUE=\"" + sAlign + "\">");
                    document.write("	<input name=filename VALUE=\"" + "filename\">");
                    document.write("	<input name=filenamerel VALUE=\"" + "filenamerel\">");
                    document.write("	<input name=sheetname VALUE='Feuille de test'>");
                    document.write("	<input name=xpos value='0'>");
                    document.write("	<input name=ypos value='0'>");
                    document.write("	<input name=cellvalueheader value=\"" + parent.document.getElementById(tabheader).innerHTML.replace(aRemplacer,"") + "\">");
                    document.write("	<input name=backcolorheader value=\"" + sBackcolorHeader + "\">");
                    document.write("	<input name=cellvaluebody value=\"" + parent.document.getElementById(tabbody).innerHTML.replace(aRemplacer,"") + "\">");
                    document.write("	<input name=backcolorbody value=\"" + sBackcolorBody + "\">");
                    document.write("	<input type='submit' value='Submit' >");
                    //on lance la génération du fichier
                    document.getElementById('ExportTableur').submit();
                }
            </script> 
        </form>         
    </body>
</html>

