VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.Form fFlashSSCC 
   BorderStyle     =   0  'None
   Caption         =   "Deal Informatique"
   ClientHeight    =   4305
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5535
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   Moveable        =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   4305
   ScaleWidth      =   5535
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   500
      Left            =   3600
      Top             =   3720
   End
   Begin MSCommLib.MSComm MSComm1 
      Left            =   4800
      Top             =   3000
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      CommPort        =   3
      DTREnable       =   -1  'True
      RThreshold      =   1
      RTSEnable       =   -1  'True
      BaudRate        =   1200
      ParitySetting   =   2
      DataBits        =   7
      SThreshold      =   1
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   3000
      Width           =   5295
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label5 
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   3480
      Width           =   4695
   End
   Begin VB.Label Label4 
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   3840
      Width           =   495
   End
   Begin VB.Label Label3 
      Caption         =   "Quitter"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      MousePointer    =   1  'Arrow
      TabIndex        =   2
      Top             =   3840
      Width           =   975
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   2760
      Width           =   5295
      WordWrap        =   -1  'True
   End
   Begin VB.Image Image1 
      Height          =   2115
      Left            =   120
      Picture         =   "fFlashSSCC.frx":0000
      Stretch         =   -1  'True
      Top             =   120
      Width           =   5295
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Waiting data..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   2400
      Width           =   5295
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "fFlashSSCC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)
Private Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Private Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long
Private Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
'-> API de temporisation
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Dim sBuffer
Public strFile As String
Public strEthernet As String
Public strEthernetPort As String

Dim hdlFile As Integer
Dim TempFileName As String
Dim topLecture As Boolean
Public protocole As String
Public strTempo As String
Public ReponseFaite As Boolean

Private Function GetTempFileNameVB(ByVal Id As String, Optional Rep As Boolean) As String

Dim TempPath As String
Dim lpBuffer As String
Dim result As Long
Dim TempFileName As String

'---> Fonction qui d�termine un nom de fichier tempo sous windows

'-> Recherche du r�pertoire temporaire
lpBuffer = Space$(500)
result = GetTempPath(Len(lpBuffer), lpBuffer)
TempPath = Mid$(lpBuffer, 1, result)

'-> Si on ne demande que le r�pertoire de windows
If Rep Then
    GetTempFileNameVB = TempPath
    Exit Function
End If

'-> Cr�ation d'un nom de fichier
TempFileName = Space$(1000)
result = GetTempFileName(TempPath, Id, 0, TempFileName)
TempFileName = Entry(1, TempFileName, Chr(0))

GetTempFileNameVB = TempFileName

End Function

Private Sub Form_Load()
Init
End Sub

Public Sub Init()
    Dim cmdline As String
    Dim comport As Integer
    Dim setg As String
    Dim Settings
    Dim CommPort
    Dim Handshaking
    Dim Echo
    Dim EOFEnable
    
    On Error GoTo GestError
    '-> on cr�e le fichier temporaire de travail
    TempFileName = GetTempFileNameVB("TMP")
    hdlFile = FreeFile
    Settings = GetSetting(App.Title, "Properties", "Settings", "") ' frmTerminal.MSComm1.Settings]\
    If Settings <> "" Then
        MSComm1.Settings = Settings
        If Err Then
            MsgBox Error$, 48
            Exit Sub
        End If
    End If
    strEthernet = GetSetting(App.Title, "Properties", "Ethernet", "")
    strEthernetPort = GetSetting(App.Title, "Properties", "EthernetPort", "")
    protocole = GetSetting(App.Title, "Properties", "Protocole", "")
    If InStr(1, Command$, "console", vbTextCompare) = 0 Then
        Me.Show
        Me.Visible = True
    End If
        
    If strEthernet <> "" Then  ' frmTerminal.MSComm1.CommPort
    Else
        CommPort = GetSetting(App.Title, "Properties", "CommPort", "") ' frmTerminal.MSComm1.CommPort
        If CommPort <> "" Then MSComm1.CommPort = CommPort
        
        Handshaking = GetSetting(App.Title, "Properties", "Handshaking", "") 'frmTerminal.MSComm1.Handshaking
        If Handshaking <> "" Then
            MSComm1.Handshaking = Handshaking
            If Err Then
                MsgBox Error$, 48
                Exit Sub
            End If
        End If
        
        Echo = GetSetting(App.Title, "Properties", "Echo", "") ' Echo
        
        EOFEnable = GetSetting(App.Title, "Properties", "EOFEnable", "") ' frmTerminal.MSComm1.EOFEnable
        If CommPort <> "" Then MSComm1.EOFEnable = EOFEnable
                
        MSComm1.PortOpen = True
    End If
    strFile = GetSetting(App.Title, "Properties", "File", "")
    If strFile = "" Then strFile = "c:\poids.txt"
        
    topLecture = False
    
    Exit Sub
    
GestError:
    
End Sub

Private Sub Label3_Click()
    '-> on ferme le programme
    End

End Sub

Private Sub Label4_Click()
frmProperties.Show
End Sub

Private Sub MSComm1_OnComm()
    Dim receivedbyte As String
    Dim ptr
    Dim EVMsg$
    Dim ERMsg$
    
    Select Case MSComm1.CommEvent
        ' Event messages.
        Case comEvReceive
            topLecture = True
        Case comEvSend
        Case comEvCTS
            EVMsg$ = "Change in CTS Detected"
        Case comEvDSR
            EVMsg$ = "Change in DSR Detected"
        Case comEvCD
            EVMsg$ = "Change in CD Detected"
        Case comEvRing
            EVMsg$ = "The Phone is Ringing"
        Case comEvEOF
            EVMsg$ = "End of File Detected"

        ' Error messages.
        Case comBreak
            ERMsg$ = "Break Received"
        Case comCDTO
            ERMsg$ = "Carrier Detect Timeout"
        Case comCTSTO
            ERMsg$ = "CTS Timeout"
        Case comDCB
            ERMsg$ = "Error retrieving DCB"
        Case comDSRTO
            ERMsg$ = "DSR Timeout"
        Case comFrame
            ERMsg$ = "Framing Error"
        Case comOverrun
            ERMsg$ = "Overrun Error"
        Case comRxOver
            ERMsg$ = "Receive Buffer Overflow"
        Case comRxParity
            ERMsg$ = "Parity Error"
        Case comTxFull
            ERMsg$ = "Transmit Buffer Full"
        Case Else
            ERMsg$ = "Unknown error or event"
    End Select
    
    If Len(EVMsg$) Then
        Me.Label5.Caption = EVMsg$
        Sleep 2000
        Me.Label5.Caption = ""
        ' Enable timer so that the message in the status bar
        ' is cleared after 2 seconds
        
    ElseIf Len(ERMsg$) Then
        Me.Label5.Caption = ERMsg$
        Sleep 2000
        Me.Label5.Caption = ""
        Beep
        'Dim Ret
        'Ret = MsgBox(ERMsg$, 1, "Click Cancel to quit, OK to ignore.")
        
        ' If the user clicks Cancel (2)...
        'If Ret = 2 Then
        '    MSComm1.PortOpen = False    ' Close the port and quit.
        'End If
        
        ' Enable timer so that the message in the status bar
        ' is cleared after 2 seconds
        
    End If
End Sub

Private Sub writePoids(strValue As String)
    Dim arrValue() As String
    Dim i As Integer
    Dim ean128 As String
    Dim SSCC As String
    If protocole = "Standard" Then
        SetIniString "SCAN", "LECTURE", strFile, strValue
        Me.Label1.Caption = Now
        arrValue = Split(strValue, "_2")
        For i = 0 To UBound(arrValue)
            If NumEntries(arrValue(i), ";") = 2 Then
                If Mid(Entry(1, arrValue(i), ";"), 1, 6) <> "NOREAD" Then
                    ean128 = Entry(1, arrValue(i), ";")
                End If
                If Mid(Entry(2, arrValue(i), ";"), 1, 6) <> "NOREAD" Then
                    SSCC = Entry(2, arrValue(i), ";")
                End If
            End If
        Next
        If ean128 <> "" Then
            SetIniString "SCAN", "EAN128", strFile, strValue
        Else
            ean128 = "?"
        End If
        If SSCC <> "" Then
            SetIniString "SCAN", "SSCC", strFile, strValue
        Else
            SSCC = "?"
        End If
        Me.Label2.Caption = "EAN128 : " & ean128
        Me.Label6.Caption = "SSCC : " & SSCC
    Else
    
    End If
End Sub

Public Function GetIniString(AppName As String, lpKeyName As String, IniFile As String, Error As Boolean) As String

Dim Res As Long
Dim lpBuffer As String


lpBuffer = Space$(2000)
Res = GetPrivateProfileString(AppName, lpKeyName, "NULL", lpBuffer, Len(lpBuffer), IniFile)
lpBuffer = Entry(1, lpBuffer, Chr(0))
If lpBuffer = "NULL" Then
    If Error Then
        MsgBox "Impossible de trouver la cl� : " & lpKeyName & _
               " dans la section : " & AppName & " du fichier : " & _
                IniFile, vbCritical + vbOKOnly, "Erreur de lecture"
    End If
    GetIniString = ""
Else
    lpBuffer = Mid$(lpBuffer, 1, Res)
    GetIniString = lpBuffer
End If


End Function

Public Function SetIniString(AppName As String, lpKeyName As String, IniFile As String, Value As String)

Dim Res As Long
Dim lpBuffer As String


Res = WritePrivateProfileString(AppName, lpKeyName, Value, IniFile)

End Function

Private Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Integer
Dim PosEnCour As Integer
Dim i As Integer
Dim CHarDeb As Integer
Dim CharEnd As Integer

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
End Function

Private Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Integer

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Integer
Dim i As Integer
Dim PosAnalyse As Integer

On Error GoTo GestError

'-< Analyse du vecteur
PosAnalyse = 1
Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1
'-> Quitter la fonction
Exit Function

GestError:
    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
End Function

Private Sub Timer1_Timer()
Dim strValue As String

On Error Resume Next

If topLecture Then
    topLecture = False
    Sleep 2000
    'DoEvents
    strValue = MSComm1.Input
    If protocole = "Standard" And strValue <> "" Then
            writePoids strValue
    Else
    
    End If
    topLecture = False
End If

If GetIniString("POIDS", "ASK", strFile, False) = "RESET" Then
    Unload fFlashSSCC
    Load fFlashSSCC
End If

End Sub


