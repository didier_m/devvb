VERSION 5.00
Begin VB.Form frmItoUTF8 
   BorderStyle     =   0  'None
   Caption         =   "Deal Informatique"
   ClientHeight    =   4305
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5535
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   Moveable        =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   4305
   ScaleWidth      =   5535
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   20
      Left            =   4920
      Top             =   2400
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "ItoUTF8"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   2520
      Width           =   3735
   End
   Begin VB.Image Image1 
      Height          =   2115
      Left            =   120
      Picture         =   "frmItoUTF8.frx":0000
      Stretch         =   -1  'True
      Top             =   120
      Width           =   5295
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "ItoUTF8"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   735
      Left            =   162
      TabIndex        =   1
      Top             =   2560
      Width           =   3735
   End
End
Attribute VB_Name = "frmItoUTF8"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)
Private Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Private Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long
Private Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
'-> API de temporisation
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" _
   (ByVal Hwnd As Long, ByVal nIndex As Long) As Long
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" _
   (ByVal Hwnd As Long, ByVal nIndex As Long, _
   ByVal dwNewLong As Long) As Long
Private Const GWL_STYLE = (-16)
Private Const GWL_EXSTYLE = (-20)
Private Declare Function SetLayeredWindowAttributes Lib "user32" _
   (ByVal Hwnd As Long, ByVal crKey As Long, _
   ByVal bAlpha As Byte, ByVal dwFlags As Long) As Long
Private Const LWA_COLORKEY = &H1
Private Const LWA_ALPHA = &H2
Private Const WS_EX_LAYERED = &H80000


Public Sub Init()
    DoEvents
    Sleep 500
    Timer1.Enabled = True
    
End Sub

Public Function fade()
    Dim m_lAlpha As Integer
    Dim lAlpha
    m_lAlpha = 0
    
    Dim lStyle As Long
    lStyle = GetWindowLong(Me.Hwnd, GWL_EXSTYLE)
    lStyle = lStyle Or WS_EX_LAYERED
    SetWindowLong Me.Hwnd, GWL_EXSTYLE, lStyle
    SetLayeredWindowAttributes Me.Hwnd, 0, 0, LWA_ALPHA
    Do
        DoEvents
        Sleep 30
        m_lAlpha = m_lAlpha + 25
        lAlpha = m_lAlpha
        If (lAlpha > 255) Then
           lAlpha = 255
        End If
        SetLayeredWindowAttributes Me.Hwnd, 0, lAlpha, LWA_ALPHA
        If (m_lAlpha > 255) Then
           Exit Do
        End If
    Loop
   
    Do
        DoEvents
        Sleep 30
        m_lAlpha = m_lAlpha - 25
        lAlpha = m_lAlpha
        If (lAlpha < 0) Then
           lAlpha = 0
        End If
        SetLayeredWindowAttributes Me.Hwnd, 0, lAlpha, LWA_ALPHA
        If (m_lAlpha < 0) Then
           End
        End If
    Loop
End Function

Private Sub Form_Load()
    Init
End Sub

Private Sub Timer1_Timer()
    fade
End Sub
