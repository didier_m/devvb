; ==========================================================================
; Nom .....: Ins_n4ds.nsi
; Auteur ..: JP LAMAILLOUX
; Objet ...: Installation de N4DS
; Ver.     Date    Auteur                Objet
; ---    --------  ------ ----------------------------------------
; 1.1.0  10/01/2014  JPLX  Installation initiale
; ==========================================================================

!define PRODUIT "n4ds"
!define DOSSIER "Deal-n4ds"
!define VERSION "1.1.1"
!define COMPANYNAME "Deal Informatique"
!define LEGALCOPYRIGHT "� Deal Informatique"

Name "${PRODUIT} ${VERSION}"

;!include "MUI.nsh"
!include "UMUI.nsh"


;--------------------------------
;Configuration

  ;General
  OutFile ${PRODUIT}_setup.exe
  XPStyle on
  InstallColors 0xFFFFFF 0x00723b
;--------------------------------
;Interface Settings

        !define MUI_ABORTWARNING
	!define MUI_UNABORTWARNING

	!define UMUI_USE_ALTERNATE_PAGE
	!define UMUI_USE_UNALTERNATE_PAGE

        !define UMUI_SKIN "cleargreen"

;--------------------------------
;Page

  !insertmacro MUI_DEFAULT UMUI_HEADERIMAGE_BMP outils\deal-header.bmp
  !insertmacro MUI_DEFAULT UMUI_LEFTIMAGE_BMP outils\deal.bmp

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "License.txt"
;  !insertmacro MUI_PAGE_COMPONENTS  ( Selection des sections )
 !insertmacro MUI_PAGE_DIRECTORY

  ;jpjp Page custom CustomPageA

  !insertmacro MUI_PAGE_INSTFILES
  
        !define MUI_FINISHPAGE_LINK "Site DEAL Informatique"
        !define MUI_FINISHPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro MUI_PAGE_FINISH

        !define UMUI_ABORTPAGE_LINK "Site DEAL Informatique"
	!define UMUI_ABORTPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro UMUI_PAGE_ABORT

  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

	!define MUI_FINISHPAGE_LINK "Site DEAL Informatique"
	!define MUI_FINISHPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro MUI_UNPAGE_FINISH

	!define UMUI_ABORTPAGE_LINK "Site DEAL Informatique"
	!define UMUI_ABORTPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro UMUI_UNPAGE_ABORT


;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "French"

;--------------------------------
; Informations sur l'executable

VIAddVersionKey /LANG=${LANG_FRENCH} "ProductName"     "${PRODUIT}"
VIAddVersionKey /LANG=${LANG_FRENCH} "Comments"        "Programme de gestion n4ds"
VIAddVersionKey /LANG=${LANG_FRENCH} "CompanyName"     "${COMPANYNAME}"
VIAddVersionKey /LANG=${LANG_FRENCH} "LegalCopyright"  "${LEGALCOPYRIGHT}"
VIAddVersionKey /LANG=${LANG_FRENCH} "FileDescription" "${PRODUIT} ${VERSION}"
VIAddVersionKey /LANG=${LANG_FRENCH} "FileVersion"     "${VERSION}"
VIAddVersionKey /LANG=${LANG_FRENCH} "ProductVersion"  "${VERSION}"
VIProductVersion "${VERSION}.0"


; *****************************
;   Repertoire d'installation
; *****************************

;Repertoire par default
  InstallDir $ProgramFiles\${DOSSIER}

;Recup du repertoire d'install depuis le registre si existe
  InstallDirRegKey HKLM "Software\${PRODUIT}" ""

; =============================
;   Installation de N4ds
; =============================

Section "insdeal" SecDeal
ClearErrors

  IfFileExists $INSTDIR\n4ds.ini ficini_exist ficini_existpas
ficini_exist:
 
 CopyFiles $INSTDIR\n4ds.ini $INSTDIR\n4ds.inisov

ficini_existpas:
  SetOutPath $INSTDIR
  File setup\*.*

 ;jp  WriteINIStr $INSTDIR\TurboGraph.ini SITE HTTP "$1"

  ; Inscription du repertoire d'installation
  WriteRegStr HKLM "Software\${PRODUIT}" "" $INSTDIR

  ; Inscription des cles de desinstallation
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}" "DisplayName"     "Deal ${PRODUIT} ${VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}" "DisplayVersion"  "${VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}" "UninstallString" '"$INSTDIR\${PRODUIT}-uninst.exe"'
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}" "URLInfoAbout"    "http://www.deal.fr"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}" "DisplayIcon"     "$INSTDIR\SV-N4ds.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}" "Publisher"       "Deal Informatique"

  ; Creation de l'executable de desinstallation
  WriteUninstaller "$INSTDIR\${PRODUIT}-uninst.exe"

  Exec '"icacls.exe" "$INSTDIR" /Grant :M *S-1-1-0:M /t /c /q'

SectionEnd


; ; ============================================
; ;   D�sinstallation
; ; ============================================
; 
 Section "Uninstall"

  Delete "$INSTDIR\*.*"
  
  RMDir "$INSTDIR"

  DeleteRegKey /ifempty HKLM "Software\${PRODUIT}"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}"


 SectionEnd


 Function .onInit

    ; L'utilisateur doit �tre administrateur dur la machine pour installer ce KIT
    UserInfo::GetName
    Pop $0
    UserInfo::GetAccountType
    Pop $1
    StrCmp $1 "Admin" SuitInit 0
    Messagebox MB_ICONSTOP \
    "Vous ($0) n'�tes pas administrateur de votre machine. Echec de l'installation du ${PRODUIT}. Merci de contacter votre correspondant informatique."
    Abort

SuitInit:

 FunctionEnd
 
