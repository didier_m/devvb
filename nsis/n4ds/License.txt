Copyright � 2000-2014 Deal Informatique

Ce logiciel est fourni tel quel sans aucune forme de garantie, express ou impliqu�e, n'incluant aucune garantie de qualit�,
d'exemption de d�fauts pour une utilisation priv�e. Deal Informatique ne garantie pas toutes vos attentes concernant ce logiciel, ni qu'il fonctionnera de fa�on irr�prochable et continue.
En aucun cas les auteurs ne pourront �tre jug�s responsables de tous les dommages r�sultant de l'utilisation de ce logiciel. 


1. L'origine de ce logiciel ne doit pas �tre mal repr�sent�e ; vous ne devez pas pr�tendre avoir �crit le logiciel original.
   Si vous employez ce logiciel dans un produit, une reconnaissance dans la documentation de produit serait appr�ci�e mais n'est pas exig�e. 
2. Des versions chang�es doivent �tre simplement marqu�es en tant que tels, et ne doivent pas �tre repr�sent�es comme �tant le logiciel original.
3. Cette notification ne peut �tre enlev�e ou chang�e d'aucune distribution. 

www.deal.fr 
postmaster@deal.fr
