; ==========================================================================
; Nom .....: Ins_Turbomaq.nsi
; Auteur ..: JP LAMAILLOUX
; Objet ...: Installation de TurboMaq
; Ver.     Date    Auteur                Objet
; ---    --------  ------ ----------------------------------------
; 2.4.4  10/01/2014  JPLX  Installation initiale
; ==========================================================================

!define PRODUIT "turbomaq"
!define DOSSIER "TurboMaq"
!define VERSION "2.4.8"
!define COMPANYNAME "Deal Informatique"
!define LEGALCOPYRIGHT "� Deal Informatique"

Name "${PRODUIT} ${VERSION}"

;!include "MUI.nsh"
!include "UMUI.nsh"

;--------------------------------
;Configuration

  ;General
  OutFile ${PRODUIT}_setup.exe
  XPStyle on
  InstallColors 0xFFFFFF 0x00723b
;--------------------------------
;Interface Settings

        !define MUI_ABORTWARNING
	!define MUI_UNABORTWARNING

	!define UMUI_USE_ALTERNATE_PAGE
	!define UMUI_USE_UNALTERNATE_PAGE

        ;!define UMUI_SKIN "cleargreen"
         !define UMUI_SKIN "blue"
        !define MUI_BGCOLOR 3C2AFD
;--------------------------------
;Page

  !insertmacro MUI_DEFAULT UMUI_HEADERBGIMAGE_BMP outils\deal-header.bmp
  !insertmacro MUI_DEFAULT UMUI_LEFTIMAGE_BMP outils\deal.bmp

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "License.txt"
;  !insertmacro MUI_PAGE_COMPONENTS  ( Selection des sections )
 !insertmacro MUI_PAGE_DIRECTORY

  ;jpjp Page custom CustomPageA

  !insertmacro MUI_PAGE_INSTFILES
  
        !define MUI_FINISHPAGE_LINK "Site DEAL Informatique"
        !define MUI_FINISHPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro MUI_PAGE_FINISH

        !define UMUI_ABORTPAGE_LINK "Site DEAL Informatique"
	!define UMUI_ABORTPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro UMUI_PAGE_ABORT

  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

	!define MUI_FINISHPAGE_LINK "Site DEAL Informatique"
	!define MUI_FINISHPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro MUI_UNPAGE_FINISH

	!define UMUI_ABORTPAGE_LINK "Site DEAL Informatique"
	!define UMUI_ABORTPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro UMUI_UNPAGE_ABORT


;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "French"

;--------------------------------
; Informations sur l'executable

VIAddVersionKey /LANG=${LANG_FRENCH} "ProductName"     "${PRODUIT}"
VIAddVersionKey /LANG=${LANG_FRENCH} "Comments"        "Programme de gestion des Maquettes"
VIAddVersionKey /LANG=${LANG_FRENCH} "CompanyName"     "${COMPANYNAME}"
VIAddVersionKey /LANG=${LANG_FRENCH} "LegalCopyright"  "${LEGALCOPYRIGHT}"
VIAddVersionKey /LANG=${LANG_FRENCH} "FileDescription" "${DOSSIER} ${VERSION}"
VIAddVersionKey /LANG=${LANG_FRENCH} "FileVersion"     "${VERSION}"
VIAddVersionKey /LANG=${LANG_FRENCH} "ProductVersion"  "${VERSION}"
VIProductVersion "${VERSION}.0"


; *****************************
;   Repertoire d'installation
; *****************************

;Repertoire par default
  InstallDir $ProgramFiles\${DOSSIER}

;Recup du repertoire d'install depuis le registre si existe
  InstallDirRegKey HKLM "Software\${PRODUIT}" ""

; =============================
;   Installation de TurboMaq
; =============================

Section "insdeal" SecDeal
ClearErrors

  IfFileExists $INSTDIR\ini\turbomaq.ini ficini_exist ficini_existpas
ficini_exist:
 
 CopyFiles $INSTDIR\ini\turbomaq.ini $INSTDIR\ini\turbomaq.inisov

ficini_existpas:

  SetOutPath $INSTDIR\exe
  File exe\*.*

  SetOutPath $INSTDIR\ini
  File ini\*.*

  SetOutPath $INSTDIR\mqt
  File mqt\*.*

;WriteINIStr $INSTDIR\turbomaq.ini SITE HTTP "$1"
;Progiciels=D:\testturbo\exe\$IDENT-cs.ini

  IfFileExists $INSTDIR\ini\turbomaq.inisov ficsov_exist ficsov_existpas
ficsov_exist:

 Rename $INSTDIR\ini\turbomaq.ini $INSTDIR\ini\turbomaq.ininew
 Rename $INSTDIR\ini\turbomaq.inisov $INSTDIR\ini\turbomaq.ini

 ficsov_existpas:


  ; Inscription du repertoire d'installation
  WriteRegStr HKLM "Software\${PRODUIT}" "" $INSTDIR

  ; Inscription des cles de desinstallation
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}" "DisplayName"     "Deal ${DOSSIER} ${VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}" "DisplayVersion"  "${VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}" "UninstallString" '"$INSTDIR\${PRODUIT}-uninst.exe"'
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}" "URLInfoAbout"    "http://www.deal.fr"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}" "DisplayIcon"     "$INSTDIR\exe\TurboMaq.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}" "Publisher"       "Deal Informatique"

  ; Creation de l'executable de desinstallation
  WriteUninstaller "$INSTDIR\${PRODUIT}-uninst.exe"

  Exec '"icacls.exe" "$INSTDIR" /Grant :M *S-1-1-0:M /t /c /q'
  Delete "$DESKTOP\Turbomaq.lnk"
  Setoutpath "$INSTDIR\exe"
  CreateShortcut "$DESKTOP\Turbomaq.lnk" "$INSTDIR\exe\Turbomaq.exe" '"$INSTDIR\ini\"' "$INSTDIR\exe\Turbomaq.exe"
SectionEnd


; ; ============================================
; ;   D�sinstallation
; ; ============================================
; 
 Section "Uninstall"

  Delete "$INSTDIR\${PRODUIT}-uninst.exe"

  Delete "$INSTDIR\mqt\*.*"
  RMDir "$INSTDIR\mqt"

  Delete "$INSTDIR\exe\*.*"
  RMDir "$INSTDIR\exe"

  Delete "$INSTDIR\ini\*.*"
  RMDir "$INSTDIR\ini"

  Delete "$INSTDIR\*.*"
  
  RMDir "$INSTDIR"

  DeleteRegKey /ifempty HKLM "Software\${PRODUIT}"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUIT}"


 SectionEnd


 Function .onInit

    ; L'utilisateur doit �tre administrateur dur la machine pour installer ce KIT
    UserInfo::GetName
    Pop $0
    UserInfo::GetAccountType
    Pop $1
    StrCmp $1 "Admin" SuitInit 0
    Messagebox MB_ICONSTOP \
    "Vous ($0) n'�tes pas administrateur de votre machine. Echec de l'installation du ${PRODUIT}. Merci de contacter votre correspondant informatique."
    Abort

SuitInit:

 FunctionEnd
