; ==========================================================================
; Nom .....: Ins_Turbograph.nsi
; Auteur ..: Ezio TORLONE - D MARCHAL
; Objet ...: Installation de Turbograph avec parametrage IE
; Ver.     Date    Auteur                Objet
; ---    --------  ------ ----------------------------------------
; 4.8.9  25/06/2021 Evo
; 4.8.9  25/06/2021 Evo
; 4.9.5  03/11/2021 Evo
; 4.9.6  16/11/2021 Evo
; 4.9.7  23/11/2021 Evo creation rep spool
; 5.0.0  09/03/2022 Evo
; 5.0.1  28/04/2022 Evo Copie des images de deallog dans turo-print\images
; 5.0.4  08/08/2022 Evo 
; 5.1.6  13/11/2023 Evo
; 5.1.7  13/11/2023 Evo
; ==========================================================================

!define PRODUIT "turbograph"
!define DOSSIER "Turbo-print"
!define VERSION "5.3.1"
!define COMPANYNAME "Deal Informatique"
!define LEGALCOPYRIGHT "� Deal Informatique"

Name "${PRODUIT} ${VERSION}"

;!include "MUI.nsh"
!include "UMUI.nsh"


;--------------------------------
;Configuration

  ;General
  OutFile ins_${PRODUIT}.exe
  XPStyle on
  InstallColors 0xFFFFFF 0x0390be
;--------------------------------
;Interface Settings

        !define MUI_ABORTWARNING
	!define MUI_UNABORTWARNING

	!define UMUI_USE_ALTERNATE_PAGE
	!define UMUI_USE_UNALTERNATE_PAGE

;        !define UMUI_SKIN "cleargreen"
        !define UMUI_SKIN "blue"
        !define MUI_BGCOLOR 3C2AFD
; 3C2AFD
;--------------------------------
;Page

 ; !insertmacro MUI_DEFAULT UMUI_HEADERIMAGE_BMP outils\deal-header.bmp
  !insertmacro MUI_DEFAULT UMUI_HEADERBGIMAGE_BMP outils\deal-header.bmp
  !insertmacro MUI_DEFAULT UMUI_LEFTIMAGE_BMP outils\deal.bmp

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "License.txt"
;  !insertmacro MUI_PAGE_COMPONENTS  ( Selection des sections )
 !insertmacro MUI_PAGE_DIRECTORY

   Page custom CustomPageA

  !insertmacro MUI_PAGE_INSTFILES
  
        !define MUI_FINISHPAGE_LINK "Site DEAL Informatique"
        !define MUI_FINISHPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro MUI_PAGE_FINISH

        !define UMUI_ABORTPAGE_LINK "Site DEAL Informatique"
	!define UMUI_ABORTPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro UMUI_PAGE_ABORT

  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

	!define MUI_FINISHPAGE_LINK "Site DEAL Informatique"
	!define MUI_FINISHPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro MUI_UNPAGE_FINISH

	!define UMUI_ABORTPAGE_LINK "Site DEAL Informatique"
	!define UMUI_ABORTPAGE_LINK_LOCATION "http://www.deal.fr/"
  !insertmacro UMUI_UNPAGE_ABORT


;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "French"

;--------------------------------
;Reserve Files

  ;These files should be inserted before other files in the data block
  ;Keep these lines before any File command
  ;Only for solid compression (by default, solid compression is enabled for BZIP2 and LZMA)

  ReserveFile "deal.ini"
  !insertmacro MUI_RESERVEFILE_INSTALLOPTIONS

;--------------------------------
; Informations sur l'executable

VIAddVersionKey /LANG=${LANG_FRENCH} "ProductName" "${PRODUIT}"
VIAddVersionKey /LANG=${LANG_FRENCH} "Comments" "Programme de gestion des �ditions"
VIAddVersionKey /LANG=${LANG_FRENCH} "CompanyName" "${COMPANYNAME}"
VIAddVersionKey /LANG=${LANG_FRENCH} "LegalTrademarks" "${PRODUIT} est une marque d�pos�e de ${COMPANYNAME}"
VIAddVersionKey /LANG=${LANG_FRENCH} "LegalCopyright" "${LEGALCOPYRIGHT}"
VIAddVersionKey /LANG=${LANG_FRENCH} "FileDescription" "${PRODUIT} ${VERSION}"
VIAddVersionKey /LANG=${LANG_FRENCH} "FileVersion" "${VERSION}"
VIAddVersionKey /LANG=${LANG_FRENCH} "ProductVersion"  "${VERSION}"
VIProductVersion "${VERSION}.0"

;======================================================
;Fonction pour connaitre Le Framework .NET est install�
;======================================================
; IsDotNETInstalled
;
; Usage:
;   Call IsDotNETInstalled
;   Pop $0
;   StrCmp $0 1 found.NETFramework no.NETFramework

Function IsDotNETInstalled
   Push $0
   Push $1
   Push $2
   Push $3
   Push $4

   ReadRegStr $4 HKEY_LOCAL_MACHINE \
     "Software\Microsoft\.NETFramework" "InstallRoot"
   # supprime le back slash de fin
   Push $4
   Exch $EXEDIR
   Exch $EXEDIR
   Pop $4
   # si le r�pertoire racine n'existe pas, .NET n'est pas install�
   IfFileExists $4 0 noDotNET

   StrCpy $0 0

   EnumStart:

     EnumRegKey $2 HKEY_LOCAL_MACHINE \
       "Software\Microsoft\.NETFramework\Policy"  $0
     IntOp $0 $0 + 1
     StrCmp $2 "" noDotNET

     StrCpy $1 0

    EnumPolicy:

       EnumRegValue $3 HKEY_LOCAL_MACHINE \
         "Software\Microsoft\.NETFramework\Policy\$2" $1
       IntOp $1 $1 + 1
        StrCmp $3 "" EnumStart
         IfFileExists "$4\$2.$3" foundDotNET EnumPolicy

   noDotNET:
     StrCpy $0 0
     Goto done

   foundDotNET:
     StrCpy $0 1

   done:
     Pop $4
     Pop $3
     Pop $2
     Pop $1
     Exch $0
 FunctionEnd



LangString TEXT_IO_TITLE ${LANG_FRENCH} "URL du site pour les images specifiques"
LangString TEXT_IO_SUBTITLE ${LANG_FRENCH} "Ex: http://www.monsite.fr"

Function CustomPageA

    !insertmacro MUI_HEADER_TEXT "$(TEXT_IO_TITLE)" "$(TEXT_IO_SUBTITLE)"
  !insertmacro MUI_INSTALLOPTIONS_DISPLAY "deal.ini"

    ReadINIStr $0 "$PLUGINSDIR\deal.ini" "Field 2" "State"
    StrCpy $1 $0
    DetailPrint "Info=$1"
  

FunctionEnd

; *****************************
;   Repertoire d'installation
; *****************************

;Repertoire par default
  InstallDir $ProgramFiles\${DOSSIER}

;Recup du repertoire d'install depuis le registre si existe
  InstallDirRegKey HKLM "Software\Turbo-print" ""

; =============================
;   Installation de TurboGraph
; =============================

Section "TurboGraph" SecTurbograph
ClearErrors

  CopyFiles $INSTDIR\TurboGraph.ini $INSTDIR\TurboGraph.inisov

  SetOutPath $INSTDIR\internet
  File html\*.*

  SetOutPath $INSTDIR
  File setup\*.*

  SetOutPath $INSTDIR\images
  File images\*.*

  WriteINIStr $INSTDIR\TurboGraph.ini SITE HTTP "$1"

  ; Inscription du repertoire d'installation
  WriteRegStr HKLM "Software\Turbo-print" "" $INSTDIR

 ; Inscription des cles de desinstallation
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print" "DisplayName"     "Deal ${DOSSIER} ${VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print" "DisplayVersion"  "${VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print" "UninstallString" '"$INSTDIR\tb-uninst.exe"'
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print" "URLInfoAbout"    "http://www.deal.fr"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print" "DisplayIcon"     "$INSTDIR\TurboGraph.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print" "Publisher"       "Deal Informatique"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print" "EstimatedSize"      0x000012c0

  ; Creation de l'executable de desinstallation
  WriteUninstaller "$INSTDIR\tb-uninst.exe"

Exec '"icacls.exe" "$INSTDIR" /Grant :M *S-1-1-0:M /t /c /q'

SectionEnd


; ===============================================
;   Installation de RuntimeVB6 & ocx et autrs DLL
; ===============================================

 !include Library.nsh

 Var ALREADY_INSTALLED

 Section "-Install VB6 runtimes"

   ; Test si Turbograph est deja installe
   
   IfFileExists "$INSTDIR\TurboGraph.exe" 0 new_installation
     StrCpy $ALREADY_INSTALLED 1
   new_installation:

   ; vb6
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msvbvm60.dll" "$SYSDIR\msvbvm60.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_PROTECTED    "dll\oleaut32.dll" "$SYSDIR\oleaut32.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_PROTECTED    "dll\olepro32.dll" "$SYSDIR\olepro32.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_PROTECTED    "dll\comcat.dll"   "$SYSDIR\comcat.dll"   "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_PROTECTED    "dll\asycfilt.dll" "$SYSDIR\asycfilt.dll" "$SYSDIR"
   !insertmacro InstallLib TLB    $ALREADY_INSTALLED REBOOT_PROTECTED    "dll\stdole2.tlb"  "$SYSDIR\stdole2.tlb"  "$SYSDIR"

   ; autres
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\cmct2fr.dll"  "$SYSDIR\cmct2fr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\cmdlgfr.dll"  "$SYSDIR\cmdlgfr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\mscmcfr.dll"  "$SYSDIR\mscmcfr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msmpifr.dll"  "$SYSDIR\msmpifr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\rchtxfr.dll"  "$SYSDIR\rchtxfr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\riched32.dll" "$SYSDIR\riched32.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\tabctfr.dll"  "$SYSDIR\tabctfr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\unzip32.dll"  "$SYSDIR\unzip32.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\vb6fr.dll"    "$SYSDIR\vb6fr.dll"    "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\vb6stkit.dll" "$SYSDIR\vb6stkit.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msvcrt40.dll" "$SYSDIR\msvcrt40.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msvbvm50.dll" "$SYSDIR\msvbvm50.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\dsofile.dll"  "$SYSDIR\dsofile.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\MSSTDFMT.DLL" "$SYSDIR\MSSTDFMT.DLL" "$SYSDIR"

   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msjet35.dll"  "$SYSDIR\msjet35.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\dao350.dll"   "$SYSDIR\dao350.dll"   "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msjint35.dll" "$SYSDIR\msjint35.dll" "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\vbajet32.dll" "$SYSDIR\vbajet32.dll" "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\expsrv.dll"   "$SYSDIR\expsrv.dll"   "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msrd2x35.dll" "$SYSDIR\msrd2x35.dll" "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msrepl35.dll" "$SYSDIR\msrepl35.dll" "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\vb5db.dll"    "$SYSDIR\vb5db.dll"    "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\cmctlfr.dll"  "$SYSDIR\cmctlfr.dll"  "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msch2fr.dll"  "$SYSDIR\msch2fr.dll"  "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\flxgdfr.dll"  "$SYSDIR\flxgdfr.dll"  "$SYSDIR"
   !insertmacro InstallLib DLL    $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msmskfr.dll"  "$SYSDIR\msmskfr.dll"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msexcl35.dll" "$SYSDIR\msexcl35.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\msexch35.dll" "$SYSDIR\msexch35.dll" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "dll\mshfgfr.dll"  "$SYSDIR\mshfgfr.dll"  "$SYSDIR"

   ; ocx
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\comct232.ocx" "$SYSDIR\comct232.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\comdlg32.ocx" "$SYSDIR\comdlg32.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\mscomctl.ocx" "$SYSDIR\mscomctl.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\msmapi32.ocx" "$SYSDIR\msmapi32.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\richtx32.ocx" "$SYSDIR\richtx32.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\tabctl32.ocx" "$SYSDIR\tabctl32.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\dealgrid.ocx" "$SYSDIR\dealgrid.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\flash.ocx"    "$SYSDIR\flash.ocx"    "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\dealskin.ocx" "$SYSDIR\dealskin.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\dogskin.ocx"  "$SYSDIR\dogskin.ocx"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\pstimer.ocx"  "$SYSDIR\pstimer.ocx"  "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\mschrt20.ocx" "$SYSDIR\mschrt20.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\msflxgrd.ocx" "$SYSDIR\msflxgrd.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\msmask32.ocx" "$SYSDIR\msmask32.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\comctl32.ocx" "$SYSDIR\comctl32.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\mshflxgd.ocx" "$SYSDIR\mshflxgd.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\mscomct2.ocx" "$SYSDIR\mscomct2.ocx" "$SYSDIR"
   !insertmacro InstallLib REGDLL $ALREADY_INSTALLED REBOOT_NOTPROTECTED "ocx\MSWINSCK.OCX" "$SYSDIR\MSWINSCK.OCX" "$SYSDIR"

 SectionEnd


 Section "-un.Uninstall VB6 runtimes"

   !insertmacro UnInstallLib REGDLL SHARED NOREMOVE "$SYSDIR\msvbvm60.dll"
   !insertmacro UnInstallLib REGDLL SHARED NOREMOVE "$SYSDIR\oleaut32.dll"
   !insertmacro UnInstallLib REGDLL SHARED NOREMOVE "$SYSDIR\olepro32.dll"
   !insertmacro UnInstallLib REGDLL SHARED NOREMOVE "$SYSDIR\comcat.dll"
   !insertmacro UnInstallLib DLL    SHARED NOREMOVE "$SYSDIR\asycfilt.dll"
   !insertmacro UnInstallLib TLB    SHARED NOREMOVE "$SYSDIR\stdole2.tlb"

 SectionEnd
  
; ===================================================
;   Fin installation de RuntimeVB6 & ocx et autrs DLL
; ===================================================
  
 ; ==================================================================
;   Associaton extension .turboa TurboGraph.exe et ouverture dans ie
; ===================================================================

Section "AssocTurbo" SecAssocTurbo
  
   DeleteRegKey   HKCR .turbo
   DeleteRegKey   HKCR turbofile
   DeleteRegKey   HKCR Applications\TurboGraph.EXE

   DeleteRegKey   HKLM SOFTWARE\turbo-print

   DeleteRegKey   HKLM SOFTWARE\Classes\.turbo
   DeleteRegKey   HKLM SOFTWARE\Classes\turbofile
   DeleteRegKey   HKLM SOFTWARE\Classes\Applications\TurboGraph.EXE

   DeleteRegKey   HKCU SOFTWARE\Classes\Applications\TurboGraph.EXE
   DeleteRegKey   HKCU Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.turbo

   WriteRegStr    HKCR .turbo "" "turbofile"
   WriteRegBin    HKCR .turbo "EditFlags" 0
   WriteRegdword  HKCR .turbo "BrowserFlags" 8
   WriteRegStr    HKCR .turbo\Shell "" ""
   WriteRegStr    HKCR .turbo\Shell\open "" ""
   WriteRegStr    HKCR .turbo\Shell\open\command "" '"$INSTDIR\TurboGraph.exe" "GETPRINTERS~SCREEN|%1%"'

   WriteRegStr    HKCR turbofile "" ""
   WriteRegBin    HKCR turbofile "EditFlags" 0
   WriteRegdword  HKCR turbofile "BrowserFlags" 8
   WriteRegStr    HKCR turbofile\Shell "" ""
   WriteRegStr    HKCR turbofile\Shell\open "" ""
   WriteRegStr    HKCR turbofile\Shell\open\command "" '"$INSTDIR\TurboGraph.exe" "GETPRINTERS~SCREEN|%1%"'

   WriteRegStr    HKCR Applications\TurboGraph.EXE "" ""
   WriteRegStr    HKCR Applications\TurboGraph.EXE\Shell "" ""
   WriteRegStr    HKCR Applications\TurboGraph.EXE\Shell\open "" ""
   WriteRegStr    HKCR Applications\TurboGraph.EXE\Shell\open\command "" '"$INSTDIR\TurboGraph.exe" "GETPRINTERS~SCREEN|%1%"'

   WriteRegStr    HKLM SOFTWARE\Classes\.turbo "" "turbofile"
   WriteRegBin    HKLM SOFTWARE\Classes\.turbo "EditFlags" 0
   WriteRegdword  HKLM SOFTWARE\Classes\.turbo "BrowserFlags" 8
   WriteRegStr    HKLM SOFTWARE\Classes\.turbo\Shell "" ""
   WriteRegStr    HKLM SOFTWARE\Classes\.turbo\Shell\open "" ""
   WriteRegStr    HKLM SOFTWARE\Classes\.turbo\Shell\open\command "" '"$INSTDIR\TurboGraph.exe" "GETPRINTERS~SCREEN|%1%"'

   WriteRegStr    HKLM SOFTWARE\Classes\turbofile "" ""
   WriteRegBin    HKLM SOFTWARE\Classes\turbofile "Edit Flags" 0
   WriteRegdword  HKLM SOFTWARE\Classes\turbofile "Browser Flags" 8
   WriteRegStr    HKLM SOFTWARE\Classes\turbofile\Shell "" ""
   WriteRegStr    HKLM SOFTWARE\Classes\turbofile\Shell\open "" ""
   WriteRegStr    HKLM SOFTWARE\Classes\turbofile\Shell\open\command "" '"$INSTDIR\TurboGraph.exe" "GETPRINTERS~SCREEN|%1%"'

   WriteRegStr    HKLM SOFTWARE\Classes\Applications\TurboGraph.EXE "" ""
   WriteRegStr    HKLM SOFTWARE\Classes\Applications\TurboGraph.EXE\Shell "" ""
   WriteRegStr    HKLM SOFTWARE\Classes\Applications\TurboGraph.EXE\Shell\open "" ""
   WriteRegStr    HKLM SOFTWARE\Classes\Applications\TurboGraph.EXE\Shell\open\command "" '"$INSTDIR\TurboGraph.exe" "GETPRINTERS~SCREEN|%1%"'

   WriteRegStr    HKCU SOFTWARE\Classes\Applications\TurboGraph.EXE "" ""
   WriteRegStr    HKCU SOFTWARE\Classes\Applications\TurboGraph.EXE\Shell "" ""
   WriteRegStr    HKCU SOFTWARE\Classes\Applications\TurboGraph.EXE\Shell\open "" ""
   WriteRegStr    HKCU SOFTWARE\Classes\Applications\TurboGraph.EXE\Shell\open\command "" '"$INSTDIR\TurboGraph.exe" "GETPRINTERS~SCREEN|%1%"'

   WriteRegStr HKCU SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.turbo "Application" "TurboGraph.exe"
   WriteRegStr HKCU SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.turbo\OpenWithList "a" "TurboGraph.exe"
   WriteRegStr HKCU SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.turbo\OpenWithList "MRUList" "a"
   
   WriteRegStr    HKCR .csv "" "csvEdit"
   WriteRegBin    HKCR .csv "EditFlags" 0
   WriteRegdword  HKCR .csv "BrowserFlags" 8
   WriteRegStr    HKCR .csv\Shell "" ""
   WriteRegStr    HKCR .csv\Shell\open "" ""
   WriteRegStr    HKCR .csv\Shell\open\command "" '"$INSTDIR\csvEdit.exe" "%1%"'
   
   WriteRegStr    HKCR csvEdit "" ""
   WriteRegBin    HKCR csvEdit "EditFlags" 0
   WriteRegdword  HKCR csvEdit "BrowserFlags" 8
   WriteRegStr    HKCR csvEdit\Shell "" ""
   WriteRegStr    HKCR csvEdit\Shell\open "" ""
   WriteRegStr    HKCR csvEdit\Shell\open\command "" '"$INSTDIR\csvEdit.exe" "%1%"'

   WriteRegStr    HKLM SOFTWARE\Classes\.csv "" "csvEdit"
   WriteRegBin    HKLM SOFTWARE\Classes\.csv "EditFlags" 0
   WriteRegdword  HKLM SOFTWARE\Classes\.csv "BrowserFlags" 8
   WriteRegStr    HKLM SOFTWARE\Classes\.csv\Shell "" ""
   WriteRegStr    HKLM SOFTWARE\Classes\.csv\Shell\open "" ""

   WriteRegStr    HKLM SOFTWARE\Classes\csvEdit "" ""
   WriteRegBin    HKLM SOFTWARE\Classes\csvEdit "Edit Flags" 0
   WriteRegdword  HKLM SOFTWARE\Classes\csvEdit "Browser Flags" 8
   WriteRegStr    HKLM SOFTWARE\Classes\csvEdit\Shell "" ""
   WriteRegStr    HKLM SOFTWARE\Classes\csvEdit\Shell\open "" ""

   WriteRegStr HKCR Applications\csvEdit.exe "" ""
   WriteRegStr HKCR Applications\csvEdit.exe\Shell "" ""
   WriteRegStr HKCR Applications\csvEdit.exe\Shell\open "" ""

   WriteRegStr HKLM SOFTWARE\Classes\Applications\csvEdit.exe "" ""
   WriteRegStr HKLM SOFTWARE\Classes\Applications\csvEdit.exe\Shell "" ""
   WriteRegStr HKLM SOFTWARE\Classes\Applications\csvEdit.exe\Shell\open "" ""

   WriteRegStr HKLM SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.csv "Application" "csvEdit.exe"
   WriteRegStr HKLM SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.csv\OpenWithList "b" "csvEdit.exe"
   WriteRegStr HKLM SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.csv\OpenWithList "MRUList" "b"

   WriteRegStr HKCU SOFTWARE\Classes\Applications\csvEdit.exe "" ""
   WriteRegStr HKCU SOFTWARE\Classes\Applications\csvEdit.exe\Shell "" ""
   WriteRegStr HKCU SOFTWARE\Classes\Applications\csvEdit.exe\Shell\open "" ""

   WriteRegStr HKCU SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.csv "Application" "csvEdit.exe"
   WriteRegStr HKCU SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.csv\OpenWithList "b" "csvEdit.exe"
   WriteRegStr HKCU SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.csv\OpenWithList "MRUList" "b"

SectionEnd


; ; ============================================
; ;   D�sinstallation
; ; ============================================
; 
 Section "Uninstall"

 ;ADD YOUR OWN FILES HERE...

  Delete "$INSTDIR\Uninstall.exe"

  Delete "$INSTDIR\images\*.*"
  Delete "$INSTDIR\internet\*.*"
  RMDir "$INSTDIR\images"
  RMDir "$INSTDIR\internet"

  Delete "$INSTDIR\*.*"
  
  RMDir "$INSTDIR"

  DeleteRegKey /ifempty HKLM "Software\Turbo-print"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print"


 SectionEnd


 Function .onInit

    ; L'utilisateur doit �tre administrateur dur la machine pour installer ce KIT
    UserInfo::GetName
    Pop $0
    UserInfo::GetAccountType
    Pop $1
    StrCmp $1 "Admin" SuitInit 0
    Messagebox MB_ICONSTOP \
    "Vous ($0) n'�tes pas administrateur de votre machine. Echec de l'installation du TurboGraph. Merci de contacter votre correspondant informatique."
    Abort

SuitInit:

   Call IsDotNETInstalled
   Pop $0
  StrCmp $0 1 found.NETFramework no.NETFramework

found.NETFramework:
  ;Messagebox MB_ICONINFORMATION \
  ;"Le Framework .NET est install� ($0) "

    Goto SuitInit1

no.NETFramework:
;	MessageBox MB_ICONINFORMATION
	MessageBox MB_ICONSTOP  \
        "Echec de l'installation du TurboGraph $\r$\n $\r$\nLe Framwork .NET n'est pas install� sur votre ordinateur .$\r$\n Depuis la version TurboGraph 3.4.7 sa presence est obligatoire $\n$\r $\n$\r Installer le Framework .NET Version 3.0 Minimum .$\n$\r Puis relancer l'installation du TurboGraph."
        Abort
        
SuitInit1:
   !insertmacro MUI_INSTALLOPTIONS_EXTRACT "deal.ini"
   
 FunctionEnd
