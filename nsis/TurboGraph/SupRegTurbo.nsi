; Supp des entr�e registre turbo

!define PRODUIT "SupRegTurbo"
!define DOSSIER "SupRegTurbo"
!define VERSION "1.1.1"
!define COMPANYNAME "Deal Informatique"
!define LEGALCOPYRIGHT "� Deal Informatique"

Name "${PRODUIT} ${VERSION}"

;!include "MUI.nsh"
!include "UMUI.nsh"


;--------------------------------
;Configuration

  ;General
  OutFile ${PRODUIT}.exe
  XPStyle on
  InstallColors 0xFFFFFF 0x00723b
;--------------------------------
;Interface Settings

        !define MUI_ABORTWARNING
;	!define MUI_UNABORTWARNING

;	!define UMUI_USE_ALTERNATE_PAGE
;	!define UMUI_USE_UNALTERNATE_PAGE

        !define UMUI_SKIN "cleargreen"

;--------------------------------
;Page

  !insertmacro MUI_DEFAULT UMUI_HEADERIMAGE_BMP outils\deal-header.bmp
  !insertmacro MUI_DEFAULT UMUI_LEFTIMAGE_BMP outils\deal.bmp

  !insertmacro MUI_PAGE_WELCOME

 !insertmacro MUI_PAGE_FINISH


;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "French"

; ==================================================================
;   Associaton extension .turbo a TurboGraph.exe et ouverture dans ie
; ===================================================================

Section "AssocTurbo" SecAssocTurbo


SectionEnd


 Function .onInit

    UserInfo::GetName
    Pop $0
    UserInfo::GetAccountType
    Pop $1
    StrCmp $1 "Admin" SuitInit 0
    Messagebox MB_ICONSTOP \
    "Vous ($0) n'�tes pas administrateur de votre machine. Echec de l'installation du TurboGraph. Merci de contacter votre correspondant informatique."
    Abort

SuitInit:

   DeleteRegKey HKCR .turbo
   DeleteRegKey HKCR turbofile
   DeleteRegKey HKCR Applications\TurboGraph.EXE

   DeleteRegKey HKLM SOFTWARE\turbo-print

   DeleteRegKey HKLM SOFTWARE\Classes\.turbo
   DeleteRegKey HKLM SOFTWARE\Classes\turbofile
   DeleteRegKey HKLM SOFTWARE\Classes\Applications\TurboGraph.EXE

   DeleteRegKey HKCU SOFTWARE\Classes\Applications\TurboGraph.EXE
   DeleteRegKey HKCU Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.turbo


  DeleteRegKey HKLM Software\Microsoft\Windows\CurrentVersion\Uninstall\Turbo-print


 FunctionEnd
