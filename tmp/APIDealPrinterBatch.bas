Attribute VB_Name = "APIDealPrinterPath"
Option Explicit

'-> API de lecture des fichiers au format "*.ini"
Public Declare Function GetPrivateProfileSection& Lib "kernel32" Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function GetPrivateProfileString& Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String)
Public Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As Any, ByVal lpFileName As String)

'-> Nom de la machine
Public Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long


Public Function SetIniString(AppName As String, lpKeyName As String, IniFile As String, Value As String)

Dim Res As Long
Dim lpBuffer As String


Res = WritePrivateProfileString(AppName, lpKeyName, Value, IniFile)

End Function
Public Function GetIniString(AppName As String, lpKeyName As String, IniFile As String, Error As Boolean) As String

Dim Res As Long
Dim lpBuffer As String


lpBuffer = Space$(2000)
Res = GetPrivateProfileString(AppName, lpKeyName, "NULL", lpBuffer, Len(lpBuffer), IniFile)
lpBuffer = Entry(1, lpBuffer, Chr(0))
If lpBuffer = "NULL" Then
    If Error Then
        MsgBox "Impossible de trouver la cl� : " & lpKeyName & _
               " dans la section : " & AppName & " du fichier : " & _
                IniFile, vbCritical + vbOKOnly, "Erreur de lecture"
    End If
    GetIniString = ""
Else
    lpBuffer = Mid$(lpBuffer, 1, Res)
    GetIniString = lpBuffer
End If


End Function

Public Function Entry(ByVal nEntrie As Integer, ByVal Vecteur As String, ByVal Separateur As String) As String

'---> Fonction qui retourne une entr�e particuli�re d'un vecteur

Dim NbEntries As Long
Dim PosEnCour As Long
Dim i As Long
Dim CHarDeb As Long
Dim CharEnd As Long

'-> V�rifier que l'entr�e sp�cifi�e existe bien
NbEntries = NumEntries(Vecteur, Separateur)
If NbEntries = 1 Or nEntrie > NbEntries Then
    Entry = Vecteur
    Exit Function
End If
    
'-> Tester si on cherche la derni�re entr�e
If nEntrie = NbEntries Then
    'Recherche du dernier s�parateur
    PosEnCour = InStrRev(Vecteur, Separateur)
    If PosEnCour + 1 > Len(Vecteur) Then
        Entry = ""
        Exit Function
    Else
        Entry = Mid$(Vecteur, PosEnCour + 1, Len(Vecteur) - PosEnCour + 1)
        Exit Function
    End If
End If
        
'-> Recherche de l'entr�e
NbEntries = 0
PosEnCour = 1
i = 0
Do
    i = InStr(PosEnCour, Vecteur, Separateur)
    NbEntries = NbEntries + 1
    If NbEntries = nEntrie Then
        'Lecture de la position de d�but
        CHarDeb = PosEnCour
        'Recherche du s�parateur suivant
        CharEnd = i
        If CHarDeb = CharEnd Then
            Entry = ""
        Else
            Entry = Mid$(Vecteur, CHarDeb, CharEnd - CHarDeb)
        End If
        
        Exit Function
    End If
    PosEnCour = i + 1
Loop
        
    

End Function

Public Function NumEntries(ByVal Vecteur As String, ByVal Separateur As String) As Variant

'---> Fonction qui retourne le nombre d'entr�es d'un vecteur pour un s�parateur donn�

Dim NbEntries As Long
Dim i As Long
Dim PosAnalyse As Long

On Error GoTo GestError

PosAnalyse = 1

Do
    i = InStr(PosAnalyse, Vecteur, Separateur)
    If i <> 0 Then
        NbEntries = NbEntries + 1
        PosAnalyse = i + 1
    Else
        Exit Do
    End If
Loop 'Analyse du vecteur

'-> Renvoyer le nombre d'entr�es
NumEntries = NbEntries + 1

Exit Function

GestError:

    '-> Dans ce cas la, renvoyer 1
    NumEntries = 1
    

End Function


Public Function IsLegalName(ByVal NewName As String, Optional Tiret As Boolean) As Boolean

'---> Fonction qui v�rifie le contenu d'un nom pour y supprimer tous les caract�res interdits
' Caract�res interdits : \ / : * " < > | -

Dim FindBad As Boolean


If InStr(1, NewName, "\") <> 0 Then FindBad = True
If InStr(1, NewName, "/") <> 0 Then FindBad = True
If InStr(1, NewName, ":") <> 0 Then FindBad = True
If InStr(1, NewName, "*") <> 0 Then FindBad = True
If InStr(1, NewName, """") <> 0 Then FindBad = True
If InStr(1, NewName, "<") <> 0 Then FindBad = True
If InStr(1, NewName, ">") <> 0 Then FindBad = True
If InStr(1, NewName, "|") <> 0 Then FindBad = True
If Tiret And InStr(1, NewName, "-") <> 0 Then FindBad = True

IsLegalName = Not FindBad


End Function


