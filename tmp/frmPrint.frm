VERSION 5.00
Begin VB.Form frmPrint 
   Caption         =   "Form1"
   ClientHeight    =   1170
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5835
   Icon            =   "frmPrint.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1170
   ScaleWidth      =   5835
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Left            =   1560
      Top             =   240
   End
End
Attribute VB_Name = "frmPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Timer1_Timer()

'-> Variables locales
Dim TurboFile As String
Dim PdfFile As String
Dim KillFile As String
Dim PrinterName As String
Dim NbCopies As Integer
Dim FileName As String
Dim Temp As String
Dim LogLigne As String
Dim CommandLigne As String

On Error Resume Next

'-> V�rifier si on trouve un fichier de fin de travail
If Dir$(ScanDirectory & FilenameStop) <> "" Then
    '-> Butter le fichier de demande de fin
    Kill ScanDirectory & FilenameStop
    '-> Supprimer le fichier de synchronisation
    Kill SynchroDirectory & SynchroFilename
    '-> Insrire la fin du log
    Call PrintLog("****** Arret du Batch " & Now & " *****")
    '-> Fermer toutes les fen�tres
    Unload frmSynchronisation
    Unload Me
    End
End If

'-> Faire une analyse du r�pertoire
FileName = Dir$(ScanDirectory & ScanExtension)
Do While FileName <> ""

    '-> Lecture des informations
    TurboFile = GetIniString("BATCH", "TURBOFILE", ScanDirectory & FileName, False)
    '-> Get du printername
    PrinterName = GetIniString("BATCH", "PRINTERNAME", ScanDirectory & FileName, False)
    '-> Get du nom Pdf
    PdfFile = GetIniString("BATCH", "PDFFILE", ScanDirectory & FileName, False)
    '-> Get du kill
    KillFile = GetIniString("BATCH", "KILLFILE", ScanDirectory & FileName, False)
    '-> R�cup�rer le nombre de copies
    Temp = GetIniString("BATCH", "NBCOPIES", ScanDirectory & FileName, False)
        
    '-> Suppression du fichier
    Kill ScanDirectory & FileName
        
    '-> Raz de la avriable de Log
    Call PrintLog("---> Taitement du fichier : " & ScanDirectory & FileName)
    Call PrintLog("Date : " & Now)
    '-> Test sur le fichier Turbo
    Call PrintLog("Fichier Turbo : " & PathTurboFile & TurboFile)
    '-> Tester que l'on trouve le fichier turbo de r�f�rence
    If Dir$(PathTurboFile & TurboFile) = "" Then
        Call PrintLog("Impossible de trouver le fichier Turbo. Fin du traitement")
        GoTo NextFile
    Else
        Call PrintLog("Fichier trouv�")
    End If
    '-> Test du printername
    Call PrintLog("Printername : " & PrinterName)
    '-> Tester que cela soit un printervalide
    If Not GetPrinter(PrinterName) Then
        Call PrintLog("Imprimante non valide. Fin du traitement")
        GoTo NextFile
    Else
        Call PrintLog("Printer valide")
    End If
    '-> Tester si on est dans le cas d'une imprimante PDF
    If UCase$(Trim(PrinterName)) = "ACROBAT PDFWRITER" Then
        Call PrintLog("Impression PDF : Oui")
        '-> Tester si le nom est sp�cifi�
        If Trim(PdfFile) = "" Then
            PdfFile = PathPdfFile & TurboFile & ".pdf"
        Else
            PdfFile = PathPdfFile & PdfFile
        End If
        '-> Imprimer le Log
        Call PrintLog("PdfFile : " & PdfFile)
        '-> Faire un Tests si = 1
        If KillFile = "1" Then
            Call PrintLog("Suppression du fichier Turbo : Oui")
        Else
            KillFile = "0"
            Call PrintLog("Suppression du fichier Turbo : Non")
        End If
        '-> Cr�ation de la chaine de commande
        CommandLigne = "ACROBAT PDFWRITER~DIRECT~1|" & PathTurboFile & TurboFile & "*" & PdfFile & "*" & KillFile
    Else
        '-> Indiquer que l'on n'est pas sur une impression PDF
        Call PrintLog("Impression PDF : Non")
        If IsNumeric(Temp) Then
            NbCopies = CInt(Temp)
        Else
            NbCopies = 1
        End If
        '-> Cr�ation de la chaine de commande
        CommandLigne = PrinterName & "~DIRECT~" & NbCopies & "|" & PathTurboFile & TurboFile
    End If
    
    '-> Impression
    Call PrintLog("Ligne de commande Turbo : " & CommandLigne)
    '-> Run de l'impression
    Shell App.Path & "\TurboGraph.exe  " & CommandLigne, vbNormalFocus
    
NextFile:
    '-> Analyse de l'entr�e suivante
    FileName = Dir
Loop


End Sub

Private Function GetPrinter(strPrinter As String) As Boolean

Dim x As Printer

On Error GoTo GestError

'-> Essayer de pointer sur le printer
For Each x In Printers
    If UCase$(x.DeviceName) = UCase$(Trim(strPrinter)) Then
        GetPrinter = True
        Exit Function
    End If
Next

GestError:

End Function
