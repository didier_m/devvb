Attribute VB_Name = "fctDealPrinterPath"
Option Explicit

'-> Entete [PARAM]
Public IsSyncho As Boolean 'Indique si on acc�s � la synchro
Public IsLog As Boolean 'Indique si on a acc�s � la gestion des log
Public FilenameStop As String 'Indique le nom du fichier Par d�faut pour arret du batch

'-> Synchronisation [SYNCHRO]
Public SynchroDirectory As String
Public SynchroFilename As String
Public SynchroDelai As Long

'-> Analyse des demandes d'impression [PRINT]
Public ScanDirectory As String
Public ScanExtension As String
Public ScanDelai As Long

'-> Pour gestion des log [LOG]
Public LogDirectory As String
Public LogFilename As String

'-> Pour gestion des paths
Public PathTurboFile As String
Public PathPdfFile As String

'-> Handle du fchier Log
Private hdlFile As Integer


Sub Main()

'---> Point d'entr�e du programme

Dim Temp
Dim IniFileName As String
Dim lpBuffer As String
Dim Res As Long

On Error Resume Next

'-> R�cup�rer le nom de la machine
lpBuffer = Space$(255)
Res = GetComputerName(lpBuffer, Len(lpBuffer))
lpBuffer = Entry(1, lpBuffer, Chr(0))

'-> Composer le nom du fichier Ini � analyser
IniFileName = LCase$(App.Path & "\dpb" & lpBuffer & ".ini")

'-> Tester si on trouve le fichier
If Dir$(IniFileName) = "" Then
    MsgBox "Impossible de trouver le fichier ini de param�trage : " & vbCrLf & IniFileName, vbCritical + vbOKOnly, "Erreur"
    End
End If

'-> Positionnement des valeurs par d�faut
IsSyncho = False
IsLog = False
FilenameStop = "DealPrinterBatch.Stop"
SynchroDelai = 60000  '1 mn
ScanExtension = "*.dpb"
ScanDelai = 10000 '10 secondes
LogDirectory = App.Path
LogFilename = "DealPrinterBatch.Log"

'**********
'* Entete *
'**********

'-> Synchro
Temp = GetIniString("PARAM", "SYNCHRO", IniFileName, False)
If Temp = "1" Then IsSyncho = True

'-> Log
Temp = GetIniString("PARAM", "LOG", IniFileName, False)
If Temp = "1" Then IsLog = True

'-> FilenameStop
Temp = GetIniString("PARAM", "FILENAMESTOP", IniFileName, False)
If Temp <> "" And IsLegalName(Temp) Then FilenameStop = Temp

'-> Path des fichiers Turbo
Temp = GetIniString("PARAM", "PATHTURBOFILE", IniFileName, False)
If Trim(Temp) <> "" Then
    If (GetAttr(Temp) And vbDirectory) = vbDirectory Then PathTurboFile = Temp
End If 'Si on a sp�cifi� un path pour les spools Turbo

'-> Path des fichiers Pdf
Temp = GetIniString("PARAM", "PATHPDFFILE", IniFileName, False)
If Trim(Temp) <> "" Then
    If (GetAttr(Temp) And vbDirectory) = vbDirectory Then PathPdfFile = Temp
End If 'Si on a sp�cifi� un path pour les spools Turbo

'*******************
'* Synchronisation *
'*******************

'-> R�pertoire de synchronisation
Temp = GetIniString("SYNCHRO", "DIRECTORY", IniFileName, False)
If Trim(Temp) = "" Then
    IsSyncho = False
Else
    '-> V�rifier si on trouve le r�pertoire
    If (GetAttr(Temp) And vbDirectory) <> vbDirectory Then
        IsSyncho = False
    Else
        SynchroDirectory = Temp
        If Right$(SynchroDirectory, 1) <> "\" Then SynchroDirectory = SynchroDirectory & "\"
    End If
End If
        
'-> Fichier de synchronisation
Temp = GetIniString("SYNCHRO", "FILENAME", IniFileName, False)
If Trim(Temp) = "" Then
    IsSyncho = False
Else
    If Not IsLegalName(Temp) Then
        IsSyncho = False
    Else
        SynchroFilename = Temp
    End If
End If

'-> Delai de synchronisation
Temp = GetIniString("SYNCHRO", "DELAI", IniFileName, False)
If IsNumeric(Temp) Then SynchroDelai = CLng(Temp)


'********
'* Scan *
'********

'-> R�pertoire d'impression
Temp = GetIniString("SCAN", "DIRECTORY", IniFileName, False)
If Trim(Temp) = "" Then
    MsgBox "R�pertoire d'analyse des fichiers non renseign�", vbCritical + vbOKOnly, "Erreur"
    End
End If
'-> V�rifier si on trouve le r�pertoire
If (GetAttr(Temp) And vbDirectory) <> vbDirectory Then
    MsgBox "R�pertoire d'analyse des fichiers non valide : " & Chr(13) & Temp, vbCritical + vbOKOnly, "Erreur"
    End
End If
'-> Affecter la variable
ScanDirectory = Temp
If Right$(ScanDirectory, 1) <> "\" Then ScanDirectory = ScanDirectory & "\"


'-> Extension
Temp = GetIniString("SCAN", "EXTENSION", IniFileName, False)
If Trim(Temp) <> "" Then ScanExtension = Trim(Temp)

'-> Delai analyse
Temp = GetIniString("SCAN", "DELAI", IniFileName, False)
If IsNumeric(Temp) Then ScanDelai = CInt(Temp)
        
'*******
'* Log *
'*******

'-> R�pertoire d'�criture des log
Temp = GetIniString("LOG", "DIRECTORY", IniFileName, False)
If Trim(Temp) = "" Then
    IsLog = False
Else
    '-> V�rifier si on trouve le r�pertoire
    If (GetAttr(Temp) And vbDirectory) <> vbDirectory Then
        IsLog = False
    Else
        LogDirectory = Temp
        If Right$(LogDirectory, 1) <> "\" Then LogDirectory = LogDirectory & "\"
    End If
End If
        
'-> Fichier des log
Temp = GetIniString("LOG", "FILENAME", IniFileName, False)
If Trim(Temp) <> "" Then
    If Not IsLegalName(Temp) Then
        IsLog = False
    Else
        LogFilename = Temp
    End If
End If

'-> Ouvrir un fichier LOG si necessaire
If IsLog Then
    '-> Ouverture
    hdlFile = FreeFile
    Open LogDirectory & LogFilename For Append As #hdlFile
    '-> Imprimer l'entete
    Print #hdlFile, "****** Debut de session du programme DealPrintBatch ******"
    Print #hdlFile, "Date : " & Now
    Print #hdlFile, "****** Etat des variables ****** "
    Print #hdlFile, "Synchronisation : " & CStr(IsSyncho)
    Print #hdlFile, "Gestion des log : " & CStr(IsLog)
    Print #hdlFile, "FileNameStop : " & FilenameStop
    Print #hdlFile, "PathTurboFile : " & PathTurboFile
    Print #hdlFile, "PathPdfFile : " & PathPdfFile
    Print #hdlFile, "Synchro Directory : " & SynchroDirectory
    Print #hdlFile, "Synchro FileName : " & SynchroFilename
    Print #hdlFile, "Synchro Delai : " & SynchroDelai
    Print #hdlFile, "Scan Directory : " & ScanDirectory
    Print #hdlFile, "Scan Extension : " & ScanExtension
    Print #hdlFile, "Scan Delai : " & ScanDelai
    Print #hdlFile, "Log Directory : " & LogDirectory
    Print #hdlFile, "Log Filename : " & LogFilename
    Print #hdlFile, "****** Traitements ******"
End If

'-> Charger la feuille de synchronisation
If IsSyncho Then
    '-> Cr�er le fichier de  synchronistation
    SetIniString "DEAL", "TIME", SynchroDirectory & SynchroFilename, CStr(Now)
    '-> Lancer la feuille de temporisation
    Load frmSynchronisation
    '->Poser le D�lai de Temporisation
    frmSynchronisation.Timer1.Interval = SynchroDelai
End If

'-> charger la feuille d'analyse
Load frmPrint
frmPrint.Timer1.Interval = ScanDelai

End Sub

Public Sub PrintLog(strToPrint As String)

'---> Mise � jour du fichier Log
If Not IsLog Then Exit Sub

Print #hdlFile, strToPrint

End Sub

